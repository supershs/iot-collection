//
//  AppDelegate.swift
//  RudongTracing
//
//  Created by JAY on 2023/5/17.
//

import UIKit

#if DEBUG // 判断是否在测试环境下
let kWebUrl = "http://114.115.148.40:9077/#/"
//let kWebUrl = "http://192.168.0.46:8080/#/"
#else
let kWebUrl = "http://114.115.148.40:9077/#/"
//let kWebUrl = "http://192.168.0.46:8080/#/"
#endif
let GOTOHOME = "pages/base-index/index?data="
//账号登录
let accountUrl = "http://114.115.148.40:9075/api/1.0/SysLogin/password/login"
//验证码登录
let codeLoginUrl = "http://124.70.188.151:9040/api/1.0/SysLogin/message/login"
//参数url
let paraUrl = "114.115.148.40:9076"
//忘记密码验证码
let CodeURL = "http://114.115.148.40:9075/api/1.0/SysLogin/message/push"
//忘记密码修改成功
let ChangePasswordUrl = "http://114.115.148.40:9075/api/1.0/SysLogin/forget/password"

let agreementURL = "pages/base-login/userAgreement"

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var webVC = HSWebViewController(path: kWebUrl)
    var canAllButUpsideDown: Bool = false
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //设置用户授权显示通知
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.badge,.carPlay,.sound], completionHandler: { (success, error) in
                print("通知授权" + (success ? "成功" : "失败"))
            })
        }
        
        var loginVC = LogOnViewController()
        window = UIWindow(frame: UIScreen.main.bounds)
        let nav = ZXNavigationController (rootViewController: loginVC)
        self.window?.rootViewController = nav
        window!.makeKeyAndVisible()
        
        
//        window = UIWindow(frame: UIScreen.main.bounds)
//
//        window!.rootViewController = webVC
//        window!.makeKeyAndVisible()
//        configUM(application, launchOptions)
//        IFlySpeechUtility.createUtility("appid=\(APPID_VALUE)");
//        wxDelegate.currentVC = webVC
//
        // 强制关闭暗黑模式
        if #available(iOS 13.0, *) {
            self.window?.overrideUserInterfaceStyle = .light
        }
        #if DEBUG // 判断是否在测试环境下
        print("当前环境：DEBUG")
        #else
        print("当前环境：RELEASE")
        #endif
        return true
    }
    

}

