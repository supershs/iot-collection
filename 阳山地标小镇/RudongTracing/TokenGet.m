//
//  TokenGet.m
//  GuangLingWuLian
//
//  Created by 宋海胜 on 2020/10/29.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

#import "TokenGet.h"

@implementation TokenGet

-(NSString *)getTokenStr:(NSData *)deviceToken {
    if (![deviceToken isKindOfClass:[NSData class]]) return @"";
        const unsigned *tokenBytes = [deviceToken bytes];
        NSString *hexToken = [NSString stringWithFormat:@"%08x%08x%08x%08x%08x%08x%08x%08x",
                              ntohl(tokenBytes[0]), ntohl(tokenBytes[1]), ntohl(tokenBytes[2]),
                              ntohl(tokenBytes[3]), ntohl(tokenBytes[4]), ntohl(tokenBytes[5]),
                              ntohl(tokenBytes[6]), ntohl(tokenBytes[7])];
        NSLog(@"deviceToken:%@",hexToken);
    return hexToken;
}

@end
