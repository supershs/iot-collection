//
//  SheYangBigLine_Bridge_Header.h
//  SheYangBigData
//
//  Created by 叁拾叁 on 2020/8/10.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

#ifndef SheYangBigLine_Bridge_Header_h
#define SheYangBigLine_Bridge_Header_h

#import "DGCacheVideoPlayer.h"

//IM相关
//#import <NIMSDK/NIMSDK.h>
//#import "NIMKit.h"
//#import "NIMSessionListViewController.h"
//#import "NSString+NIMKit.h"
//#import "NIMMessageCell.h"
//#import "NIMAvatarImageView.h"
//#import "NIMBadgeView.h"
//#import "NIMSessionAudioContentView.h"
//#import "NIMKitUtil.h"
//#import "UIView+NIM.h"
//#import "NIMSessionConfigurator.h"
//#import "NIMCustomLeftBarView.h"
//#import "NIMMessageMaker.h"
//#import "NIMKitInfoFetchOption.h"
//#import "NIMKitDataProvider.h"

//#import "VGIMOCSessionViewController.h"

#import "UIColor+BBVoiceRecord.h"
//地址选择
#import <BRPickerView.h>
// 视频播放
#import <WMPlayer.h>
#import "SYPlayer.h"
// tableview联动
#import "WMZDialog.h"

#import <RongIMKit/RongIMKit.h>
#import <RongCallKit/RongCallKit.h>

// 日历
//#import "DYRiliViewController.h"
// 上下左右滑动
#import "EScrollPageView.h"
#import "ENestScrollPageView.h"


#import <WechatOpenSDK/WXApi.h>


//讯飞语音
#import "BusinessLine/Web/Xufei/iflyMSC.framework/Headers/IFlyMSC.h"
#import "Definition.h"
#import "ISRDataHelper.h"


// 友盟推送
#import <UMCommon/UMCommon.h>  // 公共组件是所有友盟产品的基础组件，必选
// 友盟分享
//#import <UMSocialWechatHandler.h>
#import <UMPush/UMessage.h>  // Push组件
#import <MLLabel/MLLabel.h>
#import <MLLabel/MLLinkLabel.h>
// iOS10注册APNs所需头文件
#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#import <UserNotifications/UserNotifications.h>
#endif

#import "TokenGet.h"
#import "UPPaymentControl.h"
#import "MyTabBarController.h"

// 身份证识别
//#import "AVCaptureViewController.h"
//#import "IDInfo.h"

// 拖动菜单排序
#import "ZQVariableMenuView.h"
#import "ZQVariableMenuCell.h"
#import "ZQVarableMenuModel.h"
#import <SDWebImage/SDWebImage.h>
#endif /* SheYangBigLine_Bridge_Header_h */
