//
//  SYHomePageHeaderView.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2020/12/31.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit
import SDCycleScrollView

let statusHeight = STATUSBAR_HEIGHT//20.autoWidth()

class SYHomePageHeaderView: SYBaseView, UICollectionViewDelegate, UICollectionViewDataSource {

//    var model: SYHomePageMenuModel!
    var lunboModels: [SYLunboListModel]!
    var collectionView: UICollectionView!
//    var moreImgs: [String] = ["pinpai", "zhuanjia", "zixun", "nongyouquan", "nongjidiaodu", "zhibaodiaodu", "renyang", "gengduo"]
    var moreTitles: [String] = []//["公共品牌", "专家服务", "农业资讯", "农友圈", "农机调度", "植保调度", "认养农业", "更多"]
    var moreImgs: [String] = []
    var moreWebUrls: [String] = []
    var funcModels: [SYFuncModel] = []
    var gengduoBannerUrls: [URL?] = []
    var gengduoRotes: [String] = []
    
    let cycleScrollView: SDCycleScrollView = SDCycleScrollView(frame: CGRect.init(x: 0, y: 0, width: SCREEN_WIDTH, height: 190.0.autoWidth() + statusHeight))
    var dangjianView: SYDangJianView = {
        let v = SYDangJianView()
        v.isHidden = true
        return v
    }()
    
    fileprivate var dataSource: [String] = []
    
    var sepView: UIView = {
        let v = UIView()
        v.backgroundColor = Constant.bgViewColor
        return v
    }()
    
    var secSepView: UIView = {
        let v = UIView()
        v.backgroundColor = Constant.bgViewColor
        v.isHidden = true
        return v
    }()
    
    var thirSepView: UIView = {
        let v = UIView()
        v.backgroundColor = Constant.bgViewColor
        
        return v
    }()
    
    
    let weatherView: SYWeatherView = {
        let v = SYWeatherView()
        
        v.layer.cornerRadius = 5
        
        v.layer.shadowColor = UIColor(hex: 0xB3B3B3).cgColor
        v.layer.shadowOpacity = 0.5//阴影透明度
        v.layer.shadowOffset = CGSize.zero///阴影偏移量,四周
        v.layer.shadowRadius = 5//阴影半径  shadowPathWidth：阴影宽度
        return v
    }()
    
    var jianceBt: UIButton = {
        let v = UIButton()
        v.setImage(UIImage(named: "hp_jiance-1"), for: .normal)
        v.backgroundColor = Constant.bgViewColor
        return v
    }()
    
    var nongjiBt: UIButton = {
        let v = UIButton()
        v.setImage(UIImage(named: "hp_nongji"), for: .normal)
        v.backgroundColor = Constant.bgViewColor
        return v
    }()
    
    func configure(_ models: [SYLunboListModel]) {
        self.lunboModels = models
        let urls = models.map { $0.imgUrl ?? "" }
        let rotes = models.map { $0.dictionaryValue ?? "" }
        self.cycleScrollView.imageURLStringsGroup = urls.map { $0.sy_imageURL() ?? "" }
        NotificationCenter.default.post(name: .Noti_homepageBanner, object: nil, userInfo: ["imgUrls":urls.joined(separator: ","), "rotes":rotes.joined(separator: ",")])
    }
    

    func configure(_ model: [NSAppMenuVOListModel]) {
//        self.model = model
        self.moreTitles = model.map { ($0.name ?? "") }
        self.moreImgs = model.map { (($0.imgUrl ?? "")) }
        self.moreWebUrls = model.map { (($0.url ?? "")) }
        if moreTitles.count == 10 {
            setCollectionView(5)
        } else if moreTitles.count == 8 {
            setCollectionView(4)
        } else {
            setCollectionView(5)
        }
        
        sepView.snp.remakeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.top.equalTo(collectionView.snp.bottom).offset(14.autoWidth())
            make.height.equalTo(10.autoWidth())
        }

        funcModels.removeAll()
        for (i, value) in moreImgs.enumerated() {
            let model: SYFuncModel = SYFuncModel()
            model.title = moreTitles[i]
            model.img = value
            model.url = moreWebUrls[i]
            funcModels.append(model)
        }
        
        let currentVC = self.currentVC as! SYHomePageViewController
        currentVC.topView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: currentVC.topViewHeight + 90.autoWidth())
        
        collectionView.reloadData()
    }
    
    func configure(_ model: SYWeatherModel) {
        weatherView.configure(model)
    }
    
    func configureDangjian(_ model: [SYNewsListModel]) {
        
        let models = model
        let currentVC = self.currentVC as! SYHomePageViewController
        dangjianView.currentVC = self.currentVC
        if models.count == 0 {
            dangjianView.isHidden = true
            secSepView.isHidden = true
            currentVC.topView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: currentVC.topViewHeight)
        } else {
            dangjianView.configure(models)
            dangjianView.isHidden = false
            secSepView.isHidden = false
            currentVC.topView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: currentVC.topViewHeight + 136.autoWidth())
        }
        currentVC.tableView.tableHeaderView = currentVC.topView
    }
    
    override func initViews() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(bannerAction), name: .Noti_homepageBanner, object: nil)
        cycleView()
        
        jianceBt.addAction {[weak self] in
            if let `self` = self {
				self.currentVC.sy_pushWebVC(.nongchanpin)
				
            }
        }
        nongjiBt.addAction {[weak self] in
            if let `self` = self {
                self.currentVC.sy_pushWebVC(.nongji)
            }
        }
        
        addSubview(weatherView)
        addSubview(sepView)
        addSubview(secSepView)
        addSubview(thirSepView)
        addSubview(dangjianView)
        addSubview(jianceBt)
        addSubview(nongjiBt)
        
        weatherView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10.autoWidth())
            make.right.equalToSuperview().offset(-10.autoWidth())
            make.top.equalTo(cycleScrollView.snp.bottom).offset(-41.autoWidth())
            make.height.equalTo(65.autoWidth())
        }
        
        sepView.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.top.equalToSuperview().offset(14.autoWidth())
            make.height.equalTo(10.autoWidth())
        }
        
        dangjianView.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.top.equalTo(sepView.snp.bottom)
            make.height.equalTo(126.autoWidth())
        }
        
        secSepView.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.top.equalTo(dangjianView.snp.bottom)
            make.height.equalTo(10.autoWidth())
        }
        
        thirSepView.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.top.equalTo(sepView.snp.bottom)
            make.height.equalTo(100.autoWidth())
        }
        jianceBt.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10.autoWidth())
            make.width.equalTo(nongjiBt.snp.width)
            make.top.equalTo(sepView.snp.bottom)
            make.height.equalTo(90.autoWidth())
        }
        nongjiBt.snp.makeConstraints { (make) in
            make.left.equalTo(jianceBt.snp.right).offset(10.autoWidth())
            make.width.equalTo(jianceBt.snp.width)
            make.right.equalToSuperview().offset(-10.autoWidth())
            make.top.equalTo(sepView.snp.bottom)
            make.height.equalTo(90.autoWidth())
        }
    }
    
    @objc func bannerAction(noti: Notification) {
        if let urls = noti.userInfo?["imgUrls"] as? String {
            self.gengduoBannerUrls = urls.components(separatedBy: ",").map { $0.sy_imageURL() }
        }
        if let rotes = noti.userInfo?["rotes"] as? String {
            self.gengduoRotes = rotes.components(separatedBy: ",")
        }
    }

    func cycleView() {
        
//        cycleScrollView.placeholderImage = UIImage(named: "banner")
        cycleScrollView.backgroundColor = .white
        cycleScrollView.delegate = self
        self.addSubview(cycleScrollView)

    }
    
    func setCollectionView(_ itemCount: Int) {
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: (SCREEN_WIDTH - 50) / CGFloat(itemCount), height: 71)
        layout.minimumInteritemSpacing = 1
        layout.minimumLineSpacing = 6
        layout.scrollDirection = .vertical
        collectionView = UICollectionView(frame: CGRect(x: 12, y: 216.0.autoWidth() + statusHeight, width: SCREEN_WIDTH - 24, height: 150.0), collectionViewLayout: layout)
        collectionView.backgroundColor = .white
        collectionView.showsHorizontalScrollIndicator = false
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(SYGongnengCell.self, forCellWithReuseIdentifier: NSStringFromClass(SYGongnengCell.self))
        self.addSubview(self.collectionView)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return moreImgs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: SYGongnengCell? = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(SYGongnengCell.self), for: indexPath) as? SYGongnengCell
        if let d = funcModels.objectAtIndex(index: indexPath.row) {
            cell?.configCell(d)
        }
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var url = ""
        if let d = funcModels.objectAtIndex(index: indexPath.row) {
            url = d.url ?? ""
        }
//        switch url {
//
//        case "app_303":// 农友圈
//            let vc = PYQListViewController()
//            self.currentVC.navigationController?.pushViewController(vc, animated: true)
//        case "app_302":// 认养
//            let vc = RYHomeViewController()
//            self.currentVC.navigationController?.pushViewController(vc, animated: true)
//        case "app_301":// 导游
//            let vc = DYDaoLanViewController()
//            self.currentVC.navigationController?.pushViewController(vc, animated: true)
//        case "app_304":// 更多
//            print("app_304")
//            let vc = SYGengduoViewController(bannerUrls: self.gengduoBannerUrls, rotes: self.gengduoRotes)
//            self.currentVC.navigationController?.pushViewController(vc, animated: true)
//        default:
//
//            self.currentVC.sy_pushWebVC(url)
//        }
    }
}


extension SYHomePageHeaderView: SDCycleScrollViewDelegate {
    
    func cycleScrollView(_ cycleScrollView: SDCycleScrollView!, didSelectItemAt index: Int) {
        if let url = lunboModels?.objectAtIndex(index: index)?.dictionaryValue {
//            if url == "tour" {
//                let vc = DYDaoLanViewController()
//                self.currentVC.pushVC(vc)
//            } else {
//                self.currentVC.sy_pushWebVC(url)
//            }
        }
    }
}




class SYWeatherView: SYBaseView {
    
    let imgView: UIImageView = {
        let v = UIImageView()
        v.sy_name("homepage_tianqi")
        return v
    }()
    
    let wendu: UILabel = {
        let v = UILabel()
        
        v.font = 20.autoFontSize()
        v.textColor = UIColor(hex: 0xFF9000)
        return v
    }()
    
    let riqi: UILabel = {
        let v = UILabel()
        
        v.font = 12.autoFontSize()
        v.textColor = UIColor(hex: 0xFF9000)
        return v
    }()
    
    let weizhi: UILabel = {
        let v = UILabel()
        
        v.font = 12.autoFontSize()
        v.textColor = UIColor(hex: 0x666666)
        return v
    }()
    
    let qita: UILabel = {
        let v = UILabel()
        
        v.font = 12.autoFontSize()
        v.textColor = UIColor(hex: 0x666666)
        return v
    }()
    
    func configure(_ model: SYWeatherModel) {
        let date = Date(model.time ?? "")
         
        riqi.text =  date?.sy_toString(format: "YYYY年MM月dd日")
        
        weizhi.text = model.cityInfo?.city
        let forecast = model.data?.forecast?.first
        let high = forecast?.high?.components(separatedBy: " ").last ?? ""
        let low = forecast?.low?.components(separatedBy: " ").last ?? ""
        let h = high.replacingOccurrences(of: "℃", with: "")
        let l = high.replacingOccurrences(of: "℃", with: "")
        let wenduStr = model.data?.wendu ?? ""
        var wenduInt = Int(wenduStr) ?? 0
        let hInt = Int(h) ?? 0
        let lInt = Int(l) ?? 0
        if (wenduInt > hInt) {
            wenduInt = hInt
        }
        if (wenduInt < lInt) {
            wenduInt = lInt
        }
        wendu.text = "\(wenduInt)℃"
        qita.text = "气温：\(low)~\(high)    湿度\(model.data?.shidu ?? "")    空气质量：\(model.data?.quality ?? "")     "
    }
    
    override func initViews() {
        
        self.backgroundColor = UIColor(hex: 0xFFFEF3)
        self.layer.cornerRadius = 10
        addSubview(imgView)
        addSubview(wendu)
        addSubview(riqi)
        addSubview(weizhi)
        addSubview(qita)
        imgView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(-13.autoWidth())
            make.left.equalToSuperview().offset(13.autoWidth())
            make.width.equalTo(41.autoWidth())
            make.height.equalTo(39.autoWidth())
        }
        wendu.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(12.autoWidth())
            make.left.equalToSuperview().offset(62.autoWidth())
            make.height.equalTo(16.autoWidth())
        }
        
        riqi.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(18.autoWidth())
            make.left.equalTo(wendu.snp.right).offset(10.autoWidth())
            make.height.equalTo(12.autoWidth())
        }
        
        weizhi.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(18.autoWidth())
            make.right.equalToSuperview().offset(-13.autoWidth())
            make.height.equalTo(12.autoWidth())
        }
        qita.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(41.autoWidth())
            make.left.equalToSuperview().offset(14.autoWidth())
            make.height.equalTo(12.autoWidth())
        }
    }
}


class SYHomePageSousuoView: SYBaseView {
    
    
    var sousuo: UIButton = {
        let v = UIButton()
        v.setImage(UIImage(named: "homepage_sousuo"), for: .normal)
        return v
    }()
    
    var xiaoxi: UIButton = {
        let v = UIButton()
        v.setImage(UIImage(named: "homepage_xiaoxi"), for: .normal)
        return v
    }()
    
    var bgView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor(white: 1, alpha: 0.25)
        v.layer.cornerRadius = 15.autoWidth()
        v.layer.masksToBounds = true
        return v
    }()
    
    
    let sousuoTf: UITextField = {
        let v:UITextField = UITextField()
        v.textColor = UIColor.white
        v.font = 12.autoFontSize()
        
        let att = NSAttributedString(string: "点击输入你想要的内容", attributes: [NSAttributedString.Key.foregroundColor  : UIColor.white])
        v.attributedPlaceholder = att
        return v
    }()
    
    override func initViews() {
        self.backgroundColor = Constant.greenColor
        xiaoxi.addAction {[weak self] in
            if let `self` = self {
//                let vc = SYIMlistViewController()
//                self.currentVC.sy_push(vc)
            }
        }
        //当文本框内容改变时，将内容输出到控制台上
        sousuoTf.rx.text.orEmpty.asObservable()
            .subscribe(onNext: {
                print("您输入的是：\($0)")
            })
            .disposed(by: disposeBag)
        //按下 return 键
//        sousuoTf.rx.controlEvent(.editingDidEndOnExit).subscribe(onNext: {
//            [weak self] (_) in
//            if let `self` = self {
//                if let c = self.clickChoiceClosure {
//                    c(1)
//                }
//            }
//        }).disposed(by: disposeBag)
        // 编辑完成
        sousuoTf.rx.controlEvent(.editingDidEnd).subscribe(onNext: {
            [weak self] (_) in
            if let `self` = self {
                if let c = self.clickedClosure {
                    c(1)
                }
            }
        }).disposed(by: disposeBag)
        
        addSubview(bgView)
        addSubview(sousuoTf)
        addSubview(sousuo)
        addSubview(xiaoxi)
        
        bgView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(15.autoWidth())
            make.right.equalToSuperview().offset(-50.autoWidth())
            make.top.equalToSuperview().offset(STATUSBAR_HEIGHT + 15.autoWidth())
            make.height.equalTo(30.autoWidth())
        }
        sousuoTf.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(56.autoWidth())
            make.right.equalToSuperview().offset(-50.autoWidth())
            make.centerY.equalTo(bgView)
            make.height.equalTo(30.autoWidth())
        }
        sousuo.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(32.autoWidth())
            make.centerY.equalTo(bgView)
            make.height.equalTo(17.autoWidth())
            make.width.equalTo(17.autoWidth())
        }
        xiaoxi.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-16.autoWidth())
            make.centerY.equalTo(bgView)
            make.width.equalTo(22.autoWidth())
            make.height.equalTo(22.autoWidth())
        }
    }
}
