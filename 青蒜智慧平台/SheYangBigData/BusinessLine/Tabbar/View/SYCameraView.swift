//
//  SYCameraView.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/6/28.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit


/// 扫码区域4个角位置类型
public enum LBXScanViewPhotoframeAngleStyle {
    case Inner // 内嵌，一般不显示矩形框情况下
    case Outer // 外嵌,包围在矩形框的4个角
    case On // 在矩形框的4个角上，覆盖
}

/// 扫码区域动画效果
public enum LBXScanViewAnimationStyle {
    case LineMove // 线条上下移动
    case NetGrid // 网格
    case LineStill // 线条停止在扫码区域中央
    case None // 无动画
}


public struct LBXScanViewStyle {
    
    // MARK: - 中心位置矩形框

    /// 是否需要绘制扫码矩形框，默认YES
    public var isNeedShowRetangle = true

    /// 默认扫码区域为正方形，如果扫码区域不是正方形，设置宽高比
    public var whRatio: CGFloat = 1.0

    /// 矩形框(视频显示透明区)域向上移动偏移量，0表示扫码透明区域在当前视图中心位置，如果负值表示扫码区域下移
    public var centerUpOffset: CGFloat = 44

    /// 矩形框(视频显示透明区)域离界面左边及右边距离，默认60
    public var xScanRetangleOffset: CGFloat = 60

    /// 矩形框线条颜色，默认白色
    public var colorRetangleLine = UIColor.white
    
    /// 矩形框线条宽度，默认1
    public var widthRetangleLine: CGFloat = 1.0

    //MARK: - 矩形框(扫码区域)周围4个角

    /// 扫码区域的4个角类型
    public var photoframeAngleStyle = LBXScanViewPhotoframeAngleStyle.Inner

    /// 4个角的颜色
    public var colorAngle = UIColor.white//UIColor(red: 0.0, green: 167.0 / 255.0, blue: 231.0 / 255.0, alpha: 1.0)

    /// 扫码区域4个角的宽度和高度
    public var photoframeAngleW: CGFloat = 24.0
    public var photoframeAngleH: CGFloat = 24.0
    
    /// 扫码区域4个角的线条宽度,默认6，建议8到4之间
    public var photoframeLineW: CGFloat = 6

    //MARK: - 动画效果

    /// 扫码动画效果:线条或网格
    public var anmiationStyle = LBXScanViewAnimationStyle.LineMove

    /// 动画效果的图像，如线条或网格的图像
    public var animationImage: UIImage?

    //MARK: - 非识别区域颜色, 默认 RGBA (0,0,0,0.5)，范围（0--1）

    public var color_NotRecoginitonArea = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.5)

    public init() { }
    
}



class SYCameraView: SYBaseView {
    
    // 扫码区域各种参数
    var viewStyle = LBXScanViewStyle()
    // 扫码区域
    var scanRetangleRect = CGRect.zero
    
    var bgView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = .clear
        return v
    }()
    
    var secBgView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = UIColor(white: 0, alpha: 0.7)
        v.layer.cornerRadius = 10.autoWidth()
        return v
    }()
    
    lazy var zhengFanBtn: VGBaseTitleImgView = {
        let v: VGBaseTitleImgView = VGBaseTitleImgView(frame: CGRect.zero)
        
        v.textLb.textColor = UIColor(hex: 0xACACAC)
        v.textLb.font = 12.autoFontSize()
        v.imageV.image = UIImage(named: "web_qihuan_camera")
        v.tapClosure = {[weak self] (index, isSelect) in
            if let `self` = self {
                self.clickedClosure?(0)
            }
        }
        return v
    }()
    
    lazy var picsBtn: VGBaseTitleImgView = {
        let v: VGBaseTitleImgView = VGBaseTitleImgView(frame: CGRect.zero, type: .imgTop)
        
        v.textLb.textColor = .white
        v.textLb.font = 12.autoFontSize()
        v.textLb.textAlignment = .center
        v.textLb.text = "图片库"
        v.imageV.image = UIImage(named: "web_tuku")
        v.tapClosure = {[weak self] (index, isSelect) in
            if let `self` = self {
//                self.clickedClosure?(1)
                self.showTukuView()
            }
        }
        return v
    }()
    
    lazy var takeCameraBtn: VGBaseTitleImgView = {
        let v: VGBaseTitleImgView = VGBaseTitleImgView(frame: CGRect.zero, type: .imgTop)
        
        v.textLb.textColor = .white
        v.textLb.font = 12.autoFontSize()
        v.textLb.textAlignment = .center
        v.textLb.text = "拍照识别"
        v.imageV.image = UIImage(named: "web_take_camera")
        v.tapClosure = {[weak self] (index, isSelect) in
            if let `self` = self {
                self.clickedClosure?(2)
            }
        }
        return v
    }()
    
    lazy var historyBtn: VGBaseTitleImgView = {
        let v: VGBaseTitleImgView = VGBaseTitleImgView(frame: CGRect.zero, type: .imgTop)
        
        v.textLb.textColor = .white
        v.textLb.font = 12.autoFontSize()
        v.textLb.textAlignment = .center
        v.textLb.text = "识别历史"
        v.imageV.image = UIImage(named: "web_pic_history")
        v.tapClosure = {[weak self] (index, isSelect) in
            if let `self` = self {
//                self.clickedClosure?(3)
                self.showHistoryView()
            }
        }
        return v
    }()
    
    var quyu: UIImageView = {
        let v = UIImageView()
		v.image = UIImage(named: "web_shibie_quyu")
        return v
    }()
    
    var titleLb: UILabel = {
        let v: UILabel = UILabel()
        v.font = 14.autoFontSize()
        v.textColor = .white
        v.text = "请对准样本进行拍照识别"
        return v
    }()
    
    let back: UIButton = {
        let v: UIButton  = UIButton()
        v.setImage(UIImage(named: "nav_back"), for: .normal)
        return v
    }()
    
    var tukuView: SYPhotoBrowser = {
        let v = SYPhotoBrowser()
        return v
    }()
    
    var historyView: SYPhotoHistroryView = {
        let v = SYPhotoHistroryView()
        return v
    }()
    
    override func initViews() {
        backgroundColor = UIColor.clear
        back.addAction { [weak  self] in
            if let `self` = self {
                self.clickedClosure?(4)
            }
        }
        tukuView.clickedClosure = {[weak self] _ in
            if let `self` = self {
                self.hideTukuView()
            }
        }
        historyView.clickedClosure = {[weak self] _ in
            if let `self` = self {
                self.hideHistoryView()
            }
        }

        addSubview(bgView)
        bgView.isHidden = true
        addSubview(back)
        addSubview(secBgView)
        addSubview(zhengFanBtn)
        secBgView.addSubview(picsBtn)
        secBgView.addSubview(takeCameraBtn)
        secBgView.addSubview(historyBtn)
        addSubview(quyu)
        addSubview(titleLb)
        addSubview(tukuView)
        addSubview(historyView)
        
        back.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.top.equalToSuperview().offset(STATUSBAR_HEIGHT)
            make.height.equalTo(48.autoWidth())
            make.width.equalTo(30.autoWidth())
        }
        bgView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
//        quyu.snp.makeConstraints { (make) in
//            make.centerX.equalToSuperview()
//            make.top.equalToSuperview().offset(STATUSBAR_HEIGHT + 140.autoWidth())
//            make.height.equalTo(250.autoWidth())
//            make.width.equalTo(250.autoWidth())
//        }
        titleLb.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(438.autoWidth() + STATUSBAR_HEIGHT)
            make.centerX.equalToSuperview()
            make.height.equalTo(15.autoWidth())
        }
        
        secBgView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(32.autoWidth())
            make.right.equalToSuperview().offset(-32.autoWidth())
            make.bottom.equalToSuperview().offset(-40.autoWidth() - BOTTOM_SAFE_HEIGHT)
            make.height.equalTo(70.autoWidth())
        }
        
        zhengFanBtn.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-10.autoWidth())
            make.top.equalToSuperview().offset(STATUSBAR_HEIGHT + 20.autoWidth())
            make.height.equalTo(48.autoWidth())
            make.width.equalTo(30.autoWidth())
        }
        
        picsBtn.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10.autoWidth())
            make.bottom.equalToSuperview().offset(-12.autoWidth())
            make.width.equalTo(60.autoWidth())
            make.height.equalTo(50.autoWidth())
        }
        
        takeCameraBtn.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(-12.autoWidth())
            make.width.equalTo(60.autoWidth())
            make.height.equalTo(50.autoWidth())
        }
        
        historyBtn.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-10.autoWidth())
            make.bottom.equalToSuperview().offset(-12.autoWidth())
            make.width.equalTo(60.autoWidth())
            make.height.equalTo(50.autoWidth())
        }
        tukuView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(SCREEN_HEIGHT)
            make.left.right.equalToSuperview()
            make.bottom.equalToSuperview().offset(-SCREEN_HEIGHT)
        }
        historyView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(SCREEN_HEIGHT)
            make.left.right.equalToSuperview()
            make.bottom.equalToSuperview().offset(-SCREEN_HEIGHT)
        }
    }
    
    func showTukuView() {
        UIView.animate(withDuration: 0.3) {
            self.tukuView.bgView.isHidden = false
            self.tukuView.snp.remakeConstraints { (make) in
                make.top.equalToSuperview().offset(0)
                make.left.right.equalToSuperview()
                make.bottom.equalToSuperview().offset(0)
            }
        }
    }
    
    func hideTukuView() {
        UIView.animate(withDuration: 0.3) {
            self.tukuView.bgView.isHidden = true
            self.tukuView.snp.remakeConstraints { (make) in
                make.top.equalToSuperview().offset(SCREEN_HEIGHT)
                make.left.right.equalToSuperview()
                make.bottom.equalToSuperview().offset(-SCREEN_HEIGHT)
            }
        }
    }
    
    func showHistoryView() {
        UIView.animate(withDuration: 0.3) {
            self.historyView.bgView.isHidden = false
            self.historyView.snp.remakeConstraints { (make) in
                make.top.equalToSuperview().offset(0)
                make.left.right.equalToSuperview()
                make.bottom.equalToSuperview().offset(0)
            }
        }
    }
    
    func hideHistoryView() {
        UIView.animate(withDuration: 0.3) {
            self.historyView.bgView.isHidden = true
            self.historyView.snp.remakeConstraints { (make) in
                make.top.equalToSuperview().offset(SCREEN_HEIGHT)
                make.left.right.equalToSuperview()
                make.bottom.equalToSuperview().offset(-SCREEN_HEIGHT)
            }
        }
    }
    
    open override func draw(_ rect: CGRect) {
        drawScanRect()
    }
    
    //MARK: ----- 绘制扫码效果-----
    func drawScanRect() {
        let XRetangleLeft = viewStyle.xScanRetangleOffset
        var sizeRetangle = CGSize(width: frame.size.width - XRetangleLeft * 2.0, height: frame.size.width - XRetangleLeft * 2.0)
        
        if viewStyle.whRatio != 1.0 {
            let w = sizeRetangle.width
            var h = w / viewStyle.whRatio
            h = CGFloat(Int(h))
            sizeRetangle = CGSize(width: w, height: h)
        }
        
        // 扫码区域Y轴最小坐标
        let YMinRetangle = frame.size.height / 2.0 - sizeRetangle.height / 2.0 - viewStyle.centerUpOffset
        let YMaxRetangle = YMinRetangle + sizeRetangle.height
        let XRetangleRight = frame.size.width - XRetangleLeft
        
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        
        // 非扫码区域半透明
        // 设置非识别区域颜色
        context.setFillColor(viewStyle.color_NotRecoginitonArea.cgColor)
        // 填充矩形
        // 扫码区域上面填充
        var rect = CGRect(x: 0, y: 0, width: frame.size.width, height: YMinRetangle)
        context.fill(rect)

        // 扫码区域左边填充
        rect = CGRect(x: 0, y: YMinRetangle, width: XRetangleLeft, height: sizeRetangle.height)
        context.fill(rect)

        // 扫码区域右边填充
        rect = CGRect(x: XRetangleRight, y: YMinRetangle, width: XRetangleLeft, height: sizeRetangle.height)
        context.fill(rect)

        // 扫码区域下面填充
        rect = CGRect(x: 0, y: YMaxRetangle, width: frame.size.width, height: frame.size.height - YMaxRetangle)
        context.fill(rect)
        // 执行绘画
        context.strokePath()
        
        if viewStyle.isNeedShowRetangle {
            // 中间画矩形(正方形)
            context.setStrokeColor(viewStyle.colorRetangleLine.cgColor)
            context.setLineWidth(viewStyle.widthRetangleLine)
            context.addRect(CGRect(x: XRetangleLeft, y: YMinRetangle, width: sizeRetangle.width, height: sizeRetangle.height))

            // CGContextMoveToPoint(context, XRetangleLeft, YMinRetangle);
            // CGContextAddLineToPoint(context, XRetangleLeft+sizeRetangle.width, YMinRetangle);

            context.strokePath()
        }

        scanRetangleRect = CGRect(x: XRetangleLeft, y: YMinRetangle, width: sizeRetangle.width, height: sizeRetangle.height)
        
        
        // 画矩形框4格外围相框角

        // 相框角的宽度和高度
        let wAngle = viewStyle.photoframeAngleW
        let hAngle = viewStyle.photoframeAngleH

        // 4个角的 线的宽度
        let linewidthAngle = viewStyle.photoframeLineW // 经验参数：6和4

        // 画扫码矩形以及周边半透明黑色坐标参数
        var diffAngle = linewidthAngle / 3
        diffAngle = linewidthAngle / 2 // 框外面4个角，与框有缝隙
        diffAngle = linewidthAngle / 2 // 框4个角 在线上加4个角效果
        diffAngle = 0 // 与矩形框重合
        
        switch viewStyle.photoframeAngleStyle {
        case .Outer: diffAngle = linewidthAngle / 3 // 框外面4个角，与框紧密联系在一起
        case .On: diffAngle = 0
        case .Inner: diffAngle = -viewStyle.photoframeLineW / 2
        }
        
        context.setStrokeColor(viewStyle.colorAngle.cgColor)
        context.setFillColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)

        // Draw them with a 2.0 stroke width so they are a bit more visible.
        context.setLineWidth(linewidthAngle)
        
        
        //
        let leftX = XRetangleLeft - diffAngle
        let topY = YMinRetangle - diffAngle
        let rightX = XRetangleRight + diffAngle
        let bottomY = YMaxRetangle + diffAngle

        // 左上角水平线
        context.move(to: CGPoint(x: leftX - linewidthAngle / 2, y: topY))
        context.addLine(to: CGPoint(x: leftX + wAngle, y: topY))
        
        // 左上角垂直线
        context.move(to: CGPoint(x: leftX, y: topY - linewidthAngle / 2))
        context.addLine(to: CGPoint(x: leftX, y: topY + hAngle))
        
        // 左下角水平线
        context.move(to: CGPoint(x: leftX - linewidthAngle / 2, y: bottomY))
        context.addLine(to: CGPoint(x: leftX + wAngle, y: bottomY))
        
        // 左下角垂直线
        context.move(to: CGPoint(x: leftX, y: bottomY + linewidthAngle / 2))
        context.addLine(to: CGPoint(x: leftX, y: bottomY - hAngle))

        // 右上角水平线
        context.move(to: CGPoint(x: rightX + linewidthAngle / 2, y: topY))
        context.addLine(to: CGPoint(x: rightX - wAngle, y: topY))
        
        // 右上角垂直线
        context.move(to: CGPoint(x: rightX, y: topY - linewidthAngle / 2))
        context.addLine(to: CGPoint(x: rightX, y: topY + hAngle))

        // 右下角水平线
        context.move(to: CGPoint(x: rightX + linewidthAngle / 2, y: bottomY))
        context.addLine(to: CGPoint(x: rightX - wAngle, y: bottomY))

        // 右下角垂直线
        context.move(to: CGPoint(x: rightX, y: bottomY + linewidthAngle / 2))
        context.addLine(to: CGPoint(x: rightX, y: bottomY - hAngle))
        
        context.strokePath()
    }
}
