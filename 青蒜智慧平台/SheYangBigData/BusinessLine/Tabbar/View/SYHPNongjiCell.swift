//
//  SYHPNongjiCell.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2021/3/25.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit
import YYText

enum NongjiType {
    case nongji
    case zhibaoji
}

class SYHPNongjiCell: SYBaseCell , UITableViewDelegate, UITableViewDataSource {
    
    var nongjiModels: [SYDroneMachineListModel] = []
    var zhibaojiModels: [SYDroneMachineListModel] = []
    var currentModels: [SYDroneMachineListModel] = []
    var count: Int = 2
    var type: NongjiType = .nongji
    var tableView: UITableView!
    let bgImgView: UIImageView = {
        let v = UIImageView()
        v.sy_name("homepage_nongji_bg")
        return v
    }()
    let gongxuImgView: UIImageView = {
        let v = UIImageView()
        v.sy_name("homepage_nongji_gongxu")
        return v
    }()
    
    let chakanImgView: UIImageView = {
        let v = UIImageView()
        v.sy_name("homepage_nongji_chakan")
        return v
    }()
    
    let tipText: UILabel = {
        let v = UILabel()
        
        v.font = 11.autoFontSize()
        v.textColor = UIColor(hex: 0xFF961D)
        v.backgroundColor = .white
        v.layer.cornerRadius = 11.5
        v.layer.masksToBounds = true
        return v
    }()
    
    var sepView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = Constant.bgViewColor
        return v
    }()
    
    var gongxu: UIButton = {
        let v = UIButton()
        return v
    }()
   
    func configure(_ nongji: [SYDroneMachineListModel], _ zhibaoji: [SYDroneMachineListModel], _ gongxuModel: SYDemandMapModel, _ type: NongjiType) {
        
        self.nongjiModels = nongji
        self.zhibaojiModels = zhibaoji
        
        if let t = gongxuModel.message {
            tipText.text = "  \(t)  "
        } else {
            tipText.text = "  暂无农业供需  "
        }
        
        self.type = type
        if type == .nongji {
            currentModels = nongjiModels
        } else {
            currentModels = zhibaojiModels
        }
        if self.currentModels.count < 2 {
            count = self.currentModels.count
        }
        tableView.reloadData()
        tableView.snp.remakeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.right.equalToSuperview()
            make.height.equalTo(CGFloat(count) * 105.autoWidth())
            make.bottom.equalToSuperview().offset(-10.autoWidth())
        }
    }
    
    override func initViews() {
        
        gongxu.addAction {[weak self] in
            if let `self` = self {
                self.currentVC.sy_pushWebVC(.gongxu)
            }
        }
        self.backgroundColor = .white
        setTableView()
        self.contentView.addSubview(tableView)
        self.contentView.addSubview(sepView)
//        self.contentView.addSubview(bgImgView)
//        self.contentView.addSubview(gongxuImgView)
//        self.contentView.addSubview(chakanImgView)
//        self.contentView.addSubview(tipText)
//        self.contentView.addSubview(gongxu)
        tableView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.right.equalToSuperview()
            make.height.equalTo(105.autoWidth()*CGFloat(count))
            make.bottom.equalToSuperview().offset(-10.autoWidth())
        }
        sepView.snp.makeConstraints { (make) in
            make.top.equalTo(tableView.snp.bottom)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(10.autoWidth())
        }
        /*
        bgImgView.snp.makeConstraints { (make) in
            make.top.equalTo(tableView.snp.bottom).offset(5.autoWidth())
            make.left.equalToSuperview().offset(10.autoWidth())
            make.right.equalToSuperview().offset(-10.autoWidth())
            make.height.equalTo(82.autoWidth())
            make.bottom.equalToSuperview().offset(-10.autoWidth())
        }
        gongxuImgView.snp.makeConstraints { (make) in
            make.top.equalTo(tableView.snp.bottom).offset(29.autoWidth())
            make.centerX.equalToSuperview()
            make.height.equalTo(22.autoWidth())
            make.height.equalTo(94.autoWidth())
        }
        chakanImgView.snp.makeConstraints { (make) in
            make.top.equalTo(tableView.snp.bottom).offset(26.autoWidth())
            make.right.equalToSuperview().offset(-19.autoWidth())
            make.height.equalTo(51.autoWidth())
            make.width.equalTo(51.autoWidth())
        }
        gongxu.snp.makeConstraints { (make) in
            make.top.equalTo(tableView.snp.bottom).offset(26.autoWidth())
            make.right.equalToSuperview().offset(-19.autoWidth())
            make.height.equalTo(51.autoWidth())
            make.width.equalTo(51.autoWidth())
        }
        tipText.snp.makeConstraints { (make) in
            make.top.equalTo(tableView.snp.bottom).offset(55.autoWidth())
            make.left.equalToSuperview().offset(95.autoWidth())
            make.right.equalToSuperview().offset(-95.autoWidth())
            make.width.equalTo(184.autoWidth())
            make.height.equalTo(21.autoWidth())
        }
 */
    }
    
    func setTableView() {
        
        //创建表格视图
        self.tableView = UITableView(frame: CGRect.zero, style:.plain)
        self.tableView.separatorStyle = .none
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.isScrollEnabled = false
        self.tableView.backgroundColor = .white
        self.tableView!.register(SYHPNongjiSubCell.self, forCellReuseIdentifier: "Cell")
        self.addSubview(self.tableView!)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SYHPNongjiSubCell? = tableView.dequeueReusableCell(withIdentifier: "Cell") as? SYHPNongjiSubCell
        cell?.selectionStyle = .none
        cell?.currentVC = self.currentVC
        cell?.configure(self.currentModels[indexPath.row])
        if indexPath.row == currentModels.count - 1 {
            cell?.lineView.isHidden = true
        } else {
            cell?.lineView.isHidden = false
        }
       
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if type == .nongji {
            self.currentVC.sy_pushWebVC(.nongjiDetail(id: self.currentModels[indexPath.row].id ?? ""))
        } else {
            self.currentVC.sy_pushWebVC(.zhibaoDetail(id: self.currentModels[indexPath.row].id ?? ""))
        }
    }
}


class SYHPNongjiSubCell: SYBaseCell {
    
    let title: UILabel = {
        let v = UILabel()
        
        v.font = 15.autoFontSize()
        v.textColor = UIColor(hex: 0x333333)
        return v
    }()
    
    let time: UILabel = {
        let v = UILabel()
        
        v.font = 14.autoFontSize()
        v.textColor = UIColor(hex: 0x333333)
        return v
    }()
    
    let count: UILabel = {
        let v = UILabel()
        v.font = 12.autoFontSize()
        v.textColor = UIColor(hex: 0x666666)
        return v
    }()
    
    let danwei: UILabel = {
        let v = UILabel()
        v.text = "/亩"
        v.font = 14.autoFontSize()
        v.textColor = UIColor(hex: 0x666666)
        return v
    }()
    
    let userName: UILabel = {
        let v = UILabel()
        
        v.font = 14.autoFontSize()
        v.textColor = UIColor(hex: 0x333333)
        return v
    }()
    
    let imgView: UIImageView = {
        let v = UIImageView()
        v.layer.cornerRadius = 3.autoWidth()
        v.layer.masksToBounds = true
        return v
    }()
    
    let userHead: UIImageView = {
        let v = UIImageView()
        v.layer.cornerRadius = 11.autoWidth()
        v.layer.masksToBounds = true
        return v
    }()
    
    let price: YYLabel = {
        let v = YYLabel()
        return v
    }()
    
    var xuxian: UIView = {
        let v: UIView = UIView(frame: CGRect(x: 115.autoWidth(), y: 55.autoWidth(), width: 244.autoWidth(), height: 1))
        return v
    }()
    
    func configure(_ model: SYDroneMachineListModel) {
        
        imgView.sy_setWithUrl(model.machineUrl ?? "")
        userHead.sy_setWithUrl(model.headImg ?? "")
        title.text = model.machineName
        time.text = "\(model.publishDate ?? "")"
		count.text = "好评率\(model.score)% | \(model.orderNum )人付款"
        userName.text = model.trueName
        
//        for i in 0..<5 {
//
//            let v = UIImageView()
//            if model.star > i {
//                v.sy_name("homepage_nongji_wuxing_select")
//            } else {
//                v.sy_name("homepage_nongji_wuxing")
//            }
//            self.contentView.addSubview(v)
//            let width: Double = Double(i)*(1.5+13.0)
//            v.snp.makeConstraints { (make) in
//                make.left.equalToSuperview().offset((116.0+width).autoWidth())
//                make.top.equalToSuperview().offset(28.autoWidth())
//                make.width.height.equalTo(13)
//            }
//        }
        
        let d: Double = Double("\(model.taskPrice ?? 0)") ?? 0.0
        let priceStr = String(format: "%.2f", d)
        let firStr = "￥"
        let totalStr = "￥\(priceStr)"
        let attString = NSMutableAttributedString(string: totalStr)
        attString.yy_setColor(UIColor(hex: 0xF55035), range: NSRange(location: 0, length: firStr.count))
        attString.yy_setFont(12.autoFontSize(), range: NSRange(location: 0, length:firStr.count))
        attString.yy_setColor(UIColor(hex: 0xF55035), range: NSRange(location: firStr.count, length: totalStr.count - firStr.count-2))
        attString.yy_setFont(20.autoFontSize(), range: NSRange(location: firStr.count, length: totalStr.count - firStr.count-2))
        attString.yy_setColor(UIColor(hex: 0xF55035), range: NSRange(location: totalStr.count - firStr.count-2, length: 3))
        attString.yy_setFont(14.autoFontSize(), range: NSRange(location: totalStr.count - firStr.count-2, length: 3))
        price.attributedText = attString
    }
    
    
    override func initViews() {
        self.drawDashLine(lineView: xuxian, strokeColor: UIColor(hex: 0xE4E4E4), lineWidth: 2, lineLength: 2, lineSpacing: 4)
        
        self.contentView.addSubview(title)
        self.contentView.addSubview(time)
        self.contentView.addSubview(count)
        self.contentView.addSubview(danwei)
        self.contentView.addSubview(userName)
        self.contentView.addSubview(imgView)
        self.contentView.addSubview(userHead)
        self.contentView.addSubview(price)
        self.contentView.addSubview(xuxian)
        self.contentView.addSubview(lineView)
        title.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(4.autoWidth())
            make.left.equalToSuperview().offset(116.autoWidth())
            make.height.equalTo(14.autoWidth())
        }
        time.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(4.autoWidth())
            make.right.equalToSuperview().offset(-16.autoWidth())
            make.height.equalTo(14.autoWidth())
        }
        count.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(29.autoWidth())
            make.left.equalToSuperview().offset(116.autoWidth())
            make.height.equalTo(12.autoWidth())
        }
        danwei.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-19.autoWidth())
            make.left.equalTo(price.snp.right).offset(6.autoWidth())
            make.height.equalTo(14.autoWidth())
        }
        userName.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-19.autoWidth())
            make.right.equalToSuperview().offset(-16.autoWidth())
            make.height.equalTo(14.autoWidth())
        }
        imgView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(-15.autoWidth())
            make.left.equalToSuperview().offset(15.autoWidth())
            make.width.height.equalTo(90.autoWidth())
        }
        userHead.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(68.autoWidth())
            make.right.equalTo(userName.snp.left).offset(-7.autoWidth())
            make.width.height.equalTo(22.autoWidth())
        }
        price.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-19.autoWidth())
            make.left.equalToSuperview().offset(117.autoWidth())
            make.height.equalTo(15.autoWidth())
        }
        lineView.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-7.autoWidth())
            make.left.equalToSuperview().offset(15.autoWidth())
            make.right.equalToSuperview().offset(-15.autoWidth())
            make.height.equalTo(0.5.autoWidth())
        }
    }
    
    /// 绘制虚线
        /// - Parameters:
        ///   - lineView: 添加虚线的view
        ///   - strokeColor: 虚线颜色
        ///   - lineWidth: 虚线宽度
        ///   - lineLength: 每段虚线的长度
        ///   - lineSpacing: 每段虚线的间隔
        private func drawDashLine(lineView:UIView,
                                  strokeColor: UIColor,
                                  lineWidth: CGFloat = 0.5,
                                  lineLength: Int = 4,
                                  lineSpacing: Int = 4) {
            let shapeLayer = CAShapeLayer()
            shapeLayer.bounds = lineView.bounds
            shapeLayer.position = CGPoint(x: lineView.frame.size.width/2, y: lineView.frame.size.height/2)
            shapeLayer.fillColor = UIColor.blue.cgColor
            shapeLayer.strokeColor = strokeColor.cgColor
            
            shapeLayer.lineWidth = lineWidth
            shapeLayer.lineJoin = CAShapeLayerLineJoin.round
            shapeLayer.lineDashPhase = 0
            //每一段虚线长度 和 每两段虚线之间的间隔
            shapeLayer.lineDashPattern = [NSNumber(value: lineLength), NSNumber(value: lineSpacing)]
            
            let path = CGMutablePath()
            //起点
            path.move(to: CGPoint(x: lineView.frame.size.width, y: 0))

            //终点
            ///  横向 y = 0
            path.addLine(to: CGPoint(x: 0, y: 0))
            ///纵向 Y = view 的height
//            path.addLine(to: CGPoint(x: lineView.frame.width/2, y: lineView.frame.height))
            shapeLayer.path = path
            lineView.layer.addSublayer(shapeLayer)
        }
}
