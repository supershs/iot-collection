//
//  SYMySetCell.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/7/5.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit

class SYMySetCell: SYBaseCell {
    
    var picNames = ["my_yijian", "my_setting", "my_about"]
    var titles = ["意见反馈", "设置", "关于我们"]

    var bgView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = .white
        v.layer.cornerRadius = 5
        v.layer.masksToBounds = true
        return v
    }()
    
    var titleLb: UILabel = {
        let v: UILabel = UILabel()
        v.font = 14.autoMediumFontSize()
        v.textColor = UIColor(hex: 0x707070)
        return v
    }()
    
    var pic: UIImageView = {
        let v = UIImageView()
        return v
    }()
    
    var arrow: UIImageView = {
        let v = UIImageView()
        v.sy_name("my_row_arrow")
        return v
    }()
    
    func configure(_ index: Int) {
        pic.sy_name(picNames[index])
        titleLb.text = titles[index]
    }
    
    override func initViews() {
        self.backgroundColor = .clear
        contentView.addSubview(bgView)
        bgView.addSubview(titleLb)
        bgView.addSubview(pic)
        bgView.addSubview(arrow)
        bgView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(10.autoWidth())
            make.left.equalToSuperview().offset(15.autoWidth())
            make.right.equalToSuperview().offset(-15.autoWidth())
            make.height.equalTo(44.autoWidth())
            make.bottom.equalToSuperview()
        }
        pic.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(12.autoWidth())
        }
        titleLb.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(41.autoWidth())
            make.height.equalTo(54.autoWidth())
        }
        arrow.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-13.autoWidth())
        }
    }

}
