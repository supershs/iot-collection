//
//  SYNongyeCell.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2021/3/24.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit
import SnapKit
import Kingfisher
import YYText

class SYNongyeCell: SYBaseCell {
    
    var zhongleiHeight: Int = 0
    var news: SYNongyeNewsView = SYNongyeNewsView()
    var product: SYNongyeProductView = SYNongyeProductView()
    let imgView: UIImageView = {
        let v = UIImageView()
        v.sy_name("homepage_renyang_bg")
        return v
    }()
    
    var bgView: UIView = {
        let v = UIView()
        v.backgroundColor = Constant.bgViewColor
        return v
    }()
    
    var bgSecView: UIView = {
        let v = UIView()
        v.backgroundColor = Constant.bgViewColor
        return v
    }()
    
    let title: UILabel = {
        let v = UILabel()
        v.text = "认养推荐"
        v.font = 14.autoFontSize()
        v.textColor = UIColor(hex: 0xE85F11)
        return v
    }()
    
    let subTitle: UILabel = {
        let v = UILabel()
        v.text = "  查看详情  "
        v.font = 9.autoFontSize()
        v.textColor = UIColor(hex: 0xE85F11)
        v.backgroundColor = .white
        v.layer.cornerRadius = 7.autoWidth()
        v.layer.masksToBounds = true
        return v
    }()
    
    var detailBt: UIButton = {
        let v = UIButton()
        return v
    }()
    
    var nongyeZhonglei: SYNongyeZhongleiView = {
        let v = SYNongyeZhongleiView()
        v.isHidden = true
        return v
    }()
    
    func configure(_ newsModel: [SYNewsListModel], _ renyangModel: [SYAdoptListModel], _ zhongleiModel: [SYNewsListModel]) {
        self.news.currentVC = self.currentVC
        self.product.currentVC = self.currentVC
        self.news.configureView(newsModel)
        self.product.configure(renyangModel)
        
        if zhongleiModel.count != 0 {
            nongyeZhonglei.currentVC = self.currentVC
            nongyeZhonglei.configure(zhongleiModel)
            nongyeZhonglei.isHidden = false
            zhongleiHeight = 160
            
        } else {
            
            nongyeZhonglei.isHidden = false
            zhongleiHeight = 0
            
        }
        nongyeZhonglei.snp.remakeConstraints { (make) in
            make.top.equalTo(news.snp.bottom)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(zhongleiHeight.autoWidth())
        }
        imgView.snp.remakeConstraints { (make) in
            make.top.equalToSuperview().offset((180 + zhongleiHeight).autoWidth())
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(150.autoWidth())
            make.bottom.equalToSuperview().offset(-10.autoWidth())
        }
        product.snp.remakeConstraints { (make) in
            make.top.equalToSuperview().offset((190 + zhongleiHeight).autoWidth())
            make.left.equalToSuperview().offset(95.autoWidth())
            make.right.equalToSuperview()
            make.height.equalTo(130.autoWidth())
        }
        bgView.snp.remakeConstraints { (make) in
            make.top.equalToSuperview().offset((170 + zhongleiHeight).autoWidth())
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(10.autoWidth())
        }
        
    }
    
    override func initViews() {
        detailBt.addAction {[weak self] in
            if let `self` = self {
//                let vc = RYHomeViewController()
//                self.currentVC.sy_push(vc)
            }
        }
        self.contentView.addSubview(news)
        self.contentView.addSubview(imgView)
        self.contentView.addSubview(product)
        self.contentView.addSubview(bgView)
        self.contentView.addSubview(bgSecView)
        self.contentView.addSubview(title)
        self.contentView.addSubview(subTitle)
        self.contentView.addSubview(detailBt)
        contentView.addSubview(nongyeZhonglei)
        news.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(170.autoWidth())
        }
        nongyeZhonglei.snp.makeConstraints { (make) in
            make.top.equalTo(news.snp.bottom)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(zhongleiHeight.autoWidth())
        }
        imgView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset((180 + zhongleiHeight).autoWidth())
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(150.autoWidth())
            make.bottom.equalToSuperview().offset(-10.autoWidth())
        }
        product.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset((190 + zhongleiHeight).autoWidth())
            make.left.equalToSuperview().offset(95.autoWidth())
            make.right.equalToSuperview()
            make.height.equalTo(130.autoWidth())
        }
        bgView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset((170 + zhongleiHeight).autoWidth())
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(10.autoWidth())
        }
        bgSecView.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(10.autoWidth())
        }
        title.snp.makeConstraints { (make) in
            make.top.equalTo(imgView).offset(17.autoWidth())
            make.left.equalToSuperview().offset(15.autoWidth())
            make.height.equalTo(14.autoWidth())
        }
        subTitle.snp.makeConstraints { (make) in
            make.top.equalTo(imgView).offset(40.autoWidth())
            make.centerX.equalTo(title)
            make.height.equalTo(14.autoWidth())
        }
        detailBt.snp.makeConstraints { (make) in
            make.top.equalTo(imgView).offset(40.autoWidth())
            make.centerX.equalTo(title)
            make.height.equalTo(64.autoWidth())
            make.height.equalTo(64.autoWidth())
        }
    }
}




class SYNongyeNewsView: SYBaseView , UITableViewDelegate, UITableViewDataSource {
    
    var tableView: UITableView!
    var newsModels:[SYNewsListModel] = []
    var count: Int = 2
    
    public func configureView(_ newsModel: [SYNewsListModel]) {
        
        self.newsModels = newsModel
        self.tableView.reloadData()
        
        if self.newsModels.count < 2 {
            count = self.newsModels.count
        }
        tableView.reloadData()
        tableView.snp.remakeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.right.equalToSuperview()
            make.height.equalTo(CGFloat(count) * 85.autoWidth())
            make.bottom.equalToSuperview()
        }
    }
    
    override func initViews() {
        
        setTableView()
        self.addSubview(tableView)
        
        tableView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.right.equalToSuperview()
            make.height.equalTo(CGFloat(count)*85.0.autoWidth())
            make.bottom.equalToSuperview()
        }
    }
    
    func setTableView() {
        
        self.tableView = UITableView(frame: CGRect.zero, style:.plain)
        self.tableView.separatorStyle = .none
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.isScrollEnabled = false
        self.tableView!.register(SYNongyeNewsCell.self, forCellReuseIdentifier: "Cell")
        self.addSubview(self.tableView!)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SYNongyeNewsCell? = tableView.dequeueReusableCell(withIdentifier: "Cell") as? SYNongyeNewsCell
        cell?.selectionStyle = .none
        cell?.currentVC = self.currentVC
        cell?.configure(self.newsModels[indexPath.row])
        if indexPath.row == newsModels.count - 1 {
            cell?.lineView.isHidden = true
        } else {
            cell?.lineView.isHidden = false
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let category = self.newsModels[indexPath.row].category
        let product = self.newsModels[indexPath.row]
        
        self.currentVC.sy_pushWebVC(.chanyeZhixun(id: product.id ?? "", category: category ?? ""))
//        if category == "94" {// 农业政策
//            self.currentVC.sy_pushWebVC(.chanyeZhixun(id: product.id ?? "", category: product.category ?? ""))
//        } else if category == "95" {// 种植技术
//            self.currentVC.sy_pushWebVC(.zhongzhiJishu(id: product.id ?? ""))
//
//        } else if category == "98" {// 产业资讯
//            self.currentVC.sy_pushWebVC(.chanyeZhixun(id: product.id ?? "", category: product.category ?? ""))
//
//        } else if category == "97" {// 病虫害防治
//            self.currentVC.sy_pushWebVC(.chanyeZhixun(id: product.id ?? "", category: product.category ?? ""))
//        }
    }
}




class  SYNongyeProductView: SYBaseView , UICollectionViewDelegate, UICollectionViewDataSource{
    
    var collectionView: UICollectionView!
    var renyangModels:[SYAdoptListModel] = []
    
    func configure( _ renyangModel: [SYAdoptListModel]) {
        self.renyangModels = renyangModel
        self.collectionView.reloadData()
    }
    
    override func initViews() {
        
        setCollectionView()
        self.addSubview(collectionView)
        
        collectionView.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview()
            make.left.right.top.equalToSuperview()
            make.height.equalTo(130.autoWidth())
        }
    }
    
    func setCollectionView() {
        
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let itemWidth = 90.autoWidth()
        layout.itemSize = CGSize(width: itemWidth, height: 130.autoWidth())
        //        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 10
        layout.scrollDirection = .horizontal
        collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .white
        collectionView.showsHorizontalScrollIndicator = false
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(SYNongyeProductCell.self, forCellWithReuseIdentifier: NSStringFromClass(SYNongyeProductCell.self))
        self.addSubview(self.collectionView)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return renyangModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: SYNongyeProductCell? = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(SYNongyeProductCell.self), for: indexPath) as? SYNongyeProductCell
        cell?.configure(renyangModels[indexPath.row])
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let vc = RYYuViewController(id: Int(renyangModels[indexPath.row].id ?? "0") ?? 0)
//        self.currentVC.navigationController?.pushViewController(vc, animated: true)
    }
}



class SYNongyeNewsCell: SYBaseCell {
    
    let productImgView: UIImageView = {
        let v = UIImageView()
        v.layer.cornerRadius = 3.autoWidth()
        v.layer.masksToBounds = true
        return v
    }()
    
    let watchImgView: UIImageView = {
        let v = UIImageView()
        v.sy_name("homepage_liulan")
        return v
    }()
    
    let titleLb: UILabel = {
        let v = UILabel()
        v.numberOfLines = 0
        v.textColor = UIColor(hex: 0x333333)
        v.font = 16.autoFontSize()
        return v
    }()
    let timeLb: UILabel = {
        let v = UILabel()
        
        v.textColor = UIColor(hex: 0x666666)
        v.font = 12.autoFontSize()
        return v
    }()
    let watchNumLb: UILabel = {
        let v = UILabel()
        
        v.textColor = UIColor(hex: 0x999999)
        v.font = 12.autoFontSize()
        return v
    }()
    
    let typeLb: UILabel = {
        let v = UILabel()
        
        v.textColor = UIColor(hex: 0xACACAC)
        v.font = 12.autoFontSize()
        return v
    }()
    
    func configure(_ model: SYNewsListModel) {
        productImgView.sy_setWithUrl(model.imgUrl ?? "")

        titleLb.text = model.title
        timeLb.text = model.createDate
        watchNumLb.text = model.browse
        typeLb.text = model.classify 
    }
    
    override func initViews() {
        addSubview(productImgView)
        addSubview(titleLb)
        addSubview(timeLb)
        addSubview(watchNumLb)
        addSubview(watchImgView)
        addSubview(typeLb)
        addSubview(lineView)
        
        productImgView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview().offset(15.0.autoWidth())
            make.width.equalTo(90.autoWidth())
            make.height.equalTo(70.autoWidth())
            make.bottom.equalToSuperview().offset(-15.0.autoWidth())
        }
        titleLb.snp.makeConstraints { (make) in
            make.top.equalTo(productImgView).offset(2.0.autoWidth())
            make.left.equalTo(productImgView.snp.right).offset(10.0.autoWidth())
            make.right.equalToSuperview().offset(-15.0.autoWidth())
            make.height.equalTo(20)
        }
        typeLb.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(35.autoWidth())
            make.left.equalTo(titleLb.snp.left)
            make.height.equalTo(10.0.autoWidth())
        }
         timeLb.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(55.autoWidth())
            make.left.equalTo(titleLb.snp.left)
            make.height.equalTo(10.0.autoWidth())
        }
        watchNumLb.snp.makeConstraints { (make) in
            make.centerY.equalTo(watchImgView)
            make.right.equalToSuperview().offset(-15.5.autoWidth())
            make.height.equalTo(10.0.autoWidth())
        }
        watchImgView.snp.makeConstraints { (make) in
            make.centerY.equalTo(timeLb)
            make.right.equalTo(watchNumLb.snp.left).offset(-4.5.autoWidth())
            make.width.equalTo(11.5.autoWidth())
            make.height.equalTo(8.5.autoWidth())
        }
        lineView.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-7.autoWidth())
            make.left.equalToSuperview().offset(15.autoWidth())
            make.right.equalToSuperview().offset(-15.autoWidth())
            make.height.equalTo(0.5.autoWidth())
        }
    }
    
}




class SYNongyeProductCell: SYBaseCollectionViewCell {
    
    let productImgView: UIImageView = {
        let v = UIImageView()
        v.layer.cornerRadius = 3.autoWidth()
        v.layer.masksToBounds = true
        return v
    }()
    
    let tipImgView: UIImageView = {
        let v = UIImageView()
        v.sy_name("homepage_renyang_tip")
        return v
    }()
    
    let titleLb: UILabel = {
        let v = UILabel()
        v.textColor = UIColor(hex: 0x333333)
        v.font = 12.autoFontSize()
        
        return v
    }()
    let yigouLb: UILabel = {
        let v = UILabel()
        
        v.textColor = UIColor(hex: 0x999999)
        v.font = 9.autoFontSize()
        return v
    }()
    
    let price: YYLabel = {
        let v:YYLabel = YYLabel()
        v.textColor = UIColor(hex: 0xF55035)
        v.font = 11.autoFontSize()
        
        return v
    }()
    
    func configure(_ model: SYAdoptListModel) {
        
        productImgView.sy_setWithUrl(model.imgUrl ?? "")

        titleLb.text = model.name
        yigouLb.text = "已购\(model.adoptedPercent ?? "")"
        if let r = model.remarks, r == "热销" {
            tipImgView.isHidden = false
        } else {
            tipImgView.isHidden = true
        }
//        let d: Double = model.adoptPrice
//		var priceStr = String(format: "%f", d)
//        if priceStr.count > 5 {
//            priceStr = String(format: "%.f", d)
//            let firStr = "￥"
//            let totalStr = "￥\(priceStr)"
//            let attString = NSMutableAttributedString(string: totalStr)
//            attString.yy_setColor(UIColor(hex: 0xF55035), range: NSRange(location: 0, length: firStr.count))
//            attString.yy_setFont(9.autoFontSize(), range: NSRange(location: 0, length:firStr.count))
//            attString.yy_setColor(UIColor(hex: 0xF55035), range: NSRange(location: firStr.count, length: totalStr.count - firStr.count-2))
//            attString.yy_setFont(15.autoFontSize(), range: NSRange(location: firStr.count, length: totalStr.count - firStr.count-2))
//            attString.yy_setColor(UIColor(hex: 0xF55035), range: NSRange(location: totalStr.count - firStr.count-2, length: 3))
//            attString.yy_setFont(15.autoFontSize(), range: NSRange(location: totalStr.count - firStr.count-2, length: 3))
//            price.attributedText = attString
//        } else {
            let firStr = "￥"
		let totalStr = "￥\(model.adoptPrice ?? "")"
            let attString = NSMutableAttributedString(string: totalStr)
            attString.yy_setColor(UIColor(hex: 0xF55035), range: NSRange(location: 0, length: firStr.count))
            attString.yy_setFont(9.autoFontSize(), range: NSRange(location: 0, length:firStr.count))
            attString.yy_setColor(UIColor(hex: 0xF55035), range: NSRange(location: firStr.count, length: totalStr.count - firStr.count-2))
            attString.yy_setFont(15.autoFontSize(), range: NSRange(location: firStr.count, length: totalStr.count - firStr.count-2))
            attString.yy_setColor(UIColor(hex: 0xF55035), range: NSRange(location: totalStr.count - firStr.count-2, length: 3))
            attString.yy_setFont(9.autoFontSize(), range: NSRange(location: totalStr.count - firStr.count-2, length: 3))
            price.attributedText = attString
//        }
        
    }
    
    override func initViews() {
        
        addSubview(productImgView)
        addSubview(titleLb)
        addSubview(tipImgView)
        addSubview(yigouLb)
        addSubview(price)
        
        productImgView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview()
            make.width.equalTo(90.autoWidth())
            make.height.equalTo(90.autoWidth())
            make.bottom.equalToSuperview().offset(-40.0.autoWidth())
        }
        tipImgView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(5.autoWidth())
            make.left.equalToSuperview().offset(5.autoWidth())
            make.width.equalTo(40.autoWidth())
            make.height.equalTo(14.autoWidth())
        }
        titleLb.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(97.autoWidth())
            make.left.right.equalToSuperview()
            make.height.equalTo(12.autoWidth())
        }
        yigouLb.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(9.autoWidth())
        }
        price.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalTo(yigouLb.snp.left).offset(-3.autoWidth())
            make.height.equalTo(12.autoWidth())
        }
    }
    
}
