//
//  SYDaibanStateCell.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2021/2/1.
//  Copyright © 2021 叁拾叁. All rights reserved.
//


import UIKit

class SYDaibanStateCell: SYBaseView {

    var bgView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = .white
        v.layer.cornerRadius = 5
        
//        v.layer.shadowColor = UIColor(hex: 0xB3B3B3).cgColor
//        v.layer.shadowOpacity = 0.5//阴影透明度
//        v.layer.shadowOffset = CGSize.zero///阴影偏移量,四周
//        v.layer.shadowRadius = 5//阴影半径  shadowPathWidth：阴影宽度
        return v
    }()
    
    let querenLb: UILabel = {
        let v = UILabel()
        v.font = 10.autoFontSize()
        v.textColor = .white
//        v.backgroundColor = .blue
        v.layer.cornerRadius = 7.autoWidth()
        v.layer.masksToBounds = true
        v.textAlignment = .center
        return v
    }()
    let zuoyeLb: UILabel = {
        let v = UILabel()
        v.font = 10.autoFontSize()
        v.textColor = .white
        v.textAlignment = .center
        return v
    }()
    let jinxingLb: UILabel = {
        let v = UILabel()
        v.font = 10.autoFontSize()
        v.textColor = .white
        v.textAlignment = .center
        return v
    }()
    let jiesuanLb: UILabel = {
        let v = UILabel()
        v.font = 10.autoFontSize()
        v.textColor = .white
        v.textAlignment = .center
        return v
    }()
    let pingjiaLb: UILabel = {
        let v = UILabel()
        v.font = 10.autoFontSize()
        v.textColor = .white
        v.textAlignment = .center
        return v
    }()
    
    lazy var queren: VGBaseTitleImgView = {
        let v: VGBaseTitleImgView = VGBaseTitleImgView(frame: CGRect.zero, type:.imgTop)
        v.textLb.text = "待确认"
        v.textLb.font = 13.autoFontSize()
        v.textLb.textColor = UIColor(hex: 0x333333)
        v.imageV.sy_name("gzt_daiqueren")
        v.tapClosure = {[weak self] (index, isSelect) in
            if let weakSelf = self {
                weakSelf.currentVC.sy_pushWebVC(.myNongjiFuwu(orderStatus: "1"))
            }
        }
        return v
    }()
    
    lazy var zuoye: VGBaseTitleImgView = {
        let v: VGBaseTitleImgView = VGBaseTitleImgView(frame: CGRect.zero, type:.imgTop)
        v.textLb.text = "待作业"
        v.textLb.font = 13.autoFontSize()
        v.textLb.textColor = UIColor(hex: 0x333333)
        v.imageV.sy_name("gzt_daizuoye")
        v.tapClosure = {[weak self] (index, isSelect) in
            if let weakSelf = self {
                weakSelf.currentVC.sy_pushWebVC(.myNongjiFuwu(orderStatus: "3"))
            }
        }
        return v
    }()
    
    lazy var jinxing: VGBaseTitleImgView = {
        let v: VGBaseTitleImgView = VGBaseTitleImgView(frame: CGRect.zero, type:.imgTop)
        v.textLb.text = "进行中"
        v.textLb.font = 13.autoFontSize()
        v.textLb.textColor = UIColor(hex: 0x333333)
        v.imageV.sy_name("gzt_jinxingzhong")
        v.tapClosure = {[weak self] (index, isSelect) in
            if let weakSelf = self {
                weakSelf.currentVC.sy_pushWebVC(.myNongjiFuwu(orderStatus: "4"))
            }
        }
        return v
    }()
    
    lazy var jiesuan: VGBaseTitleImgView = {
        let v: VGBaseTitleImgView = VGBaseTitleImgView(frame: CGRect.zero, type:.imgTop)
        v.textLb.text = "待结算"
        v.textLb.font = 13.autoFontSize()
        v.textLb.textColor = UIColor(hex: 0x333333)
        v.imageV.sy_name("gzt_daijiesuan")
        v.tapClosure = {[weak self] (index, isSelect) in
            if let weakSelf = self {
                weakSelf.currentVC.sy_pushWebVC(.myNongjiFuwu(orderStatus: "5"))
            }
        }
        return v
    }()
    
    lazy var pingjia: VGBaseTitleImgView = {
        let v: VGBaseTitleImgView = VGBaseTitleImgView(frame: CGRect.zero, type:.imgTop)
        v.textLb.text = "待评价"
        v.textLb.font = 13.autoFontSize()
        v.textLb.textColor = UIColor(hex: 0x333333)
        v.imageV.sy_name("gzt_daipingjia")
        v.tapClosure = {[weak self] (index, isSelect) in
            if let weakSelf = self {
                weakSelf.currentVC.sy_pushWebVC(.myNongjiFuwu(orderStatus: "6"))
            }
        }
        return v
    }()
    
    func configure(_ model: SYGongzuotaiModel) {
        if let r = model.orderNum {
            
            querenLb.text = "\(r.confirmCount)"
            zuoyeLb.text = "\(r.serviceCount)"
            jinxingLb.text = "\(r.goCount)"
            jiesuanLb.text = "\(r.payCount)"
            pingjiaLb.text = "\(r.evaluateCount)"
            
//            querenLb.isHidden = (r.confirmCount == 0)
//            zuoyeLb.isHidden = (r.serviceCount == 0)
//            jinxingLb.isHidden = (r.goCount == 0)
//            jiesuanLb.isHidden = (r.payCount == 0)
//            pingjiaLb.isHidden = (r.evaluateCount == 0)
        }
    }

    override func initViews() {
        
        self.backgroundColor = .clear
        self.addSubview(bgView)
        
        bgView.addSubview(self.queren)
        bgView.addSubview(self.zuoye)
        bgView.addSubview(self.jinxing)
        bgView.addSubview(self.jiesuan)
        bgView.addSubview(self.pingjia)
        
        queren.addSubview(querenLb)
        zuoye.addSubview(zuoyeLb)
        jinxing.addSubview(jinxingLb)
        jiesuan.addSubview(jiesuanLb)
        pingjia.addSubview(pingjiaLb)
        
        bgView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(15.autoWidth())
            make.top.equalToSuperview()
            make.right.equalToSuperview().offset(-15.autoWidth())
            make.height.equalTo(109.autoWidth())
            make.bottom.equalToSuperview().offset(-13.5.autoWidth())
        }
        queren.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(20.autoWidth())
            make.left.equalToSuperview().offset(8.autoWidth())
            make.bottom.equalToSuperview().offset(-14.autoWidth())
            make.height.equalTo(60.autoWidth())
            make.width.equalTo(41.autoWidth())
        }
        zuoye.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(20.autoWidth())
            make.left.equalToSuperview().offset(79.autoWidth())
            make.bottom.equalToSuperview().offset(-14.autoWidth())
            make.height.equalTo(60.autoWidth())
            make.width.equalTo(41.autoWidth())
        }
        jinxing.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(20.autoWidth())
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(-14.autoWidth())
            make.height.equalTo(60.autoWidth())
            make.width.equalTo(41.autoWidth())
        }
        jiesuan.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(20.autoWidth())
            make.right.equalToSuperview().offset(-79.autoWidth())
            make.bottom.equalToSuperview().offset(-14.autoWidth())
            make.height.equalTo(60.autoWidth())
            make.width.equalTo(41.autoWidth())
        }
        pingjia.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(20.autoWidth())
            make.right.equalToSuperview().offset(-8.autoWidth())
            make.bottom.equalToSuperview().offset(-14.autoWidth())
            make.height.equalTo(60.autoWidth())
            make.width.equalTo(41.autoWidth())
        }
        querenLb.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(5.autoWidth())
            make.right.equalToSuperview().offset(-7.autoWidth())
            make.width.height.equalTo(14.autoWidth())
        }
        zuoyeLb.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(5.autoWidth())
            make.right.equalToSuperview().offset(-7.autoWidth())
            make.width.height.equalTo(14.autoWidth())
        }
        jinxingLb.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(5.autoWidth())
            make.right.equalToSuperview().offset(-7.autoWidth())
            make.width.height.equalTo(14.autoWidth())
        }
        jiesuanLb.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(5.autoWidth())
            make.right.equalToSuperview().offset(-7.autoWidth())
            make.width.height.equalTo(14.autoWidth())
        }
        pingjiaLb.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(5.autoWidth())
            make.right.equalToSuperview().offset(-7.autoWidth())
            make.width.height.equalTo(14.autoWidth())
        }
    }
}
