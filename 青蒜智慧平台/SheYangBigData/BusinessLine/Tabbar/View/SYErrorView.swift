//
//  SYErrorView.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/4/7.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit

enum ErrorPageType {
    case dataNone
    case netError
    case pageError
}

class SYErrorView: SYBaseView {
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    var imgView: UIImageView = {
        
        let img = UIImageView()
        
        
        
        return img
    }()
    
    var labC: UILabel = {
        
        let lab = UILabel()
        
        lab.textColor = UIColor(hex: 0x999999)
        lab.font = UIFont.systemFont(ofSize: 14)
        return lab
        
    }()
    
    var labE: UILabel = {
        
        let lab = UILabel()
        
        lab.textColor = UIColor(hex: 0x999999)
        lab.font = UIFont.systemFont(ofSize: 10)
        return lab
        
    }()
    
    var backBtn: UIButton = {
        
        let btn = UIButton()
        btn.setTitle("返回首页", for: .normal)
        btn.layer.borderWidth = 0.5
        btn.layer.borderColor = UIColor(hex: 0xCCCCCC)?.cgColor
        btn.layer.cornerRadius = 2
        btn.layer.masksToBounds = true
        btn.setTitleColor(UIColor(hex: 0x7F8389), for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        btn.isHidden = true
        return btn
    }()
//    init(frame: CGRect, loginProtocol: SYLoginProtocol, viewController: UIViewController) {
//        super.init(frame: frame)
//        initViews()
//
//    }
//
//    required init?(coder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//
    func configure(_ type: ErrorPageType) {
        switch type {
        case .dataNone:
            labC.text = "抱歉，暂无内容"
            labE.text = "Sorry, No content."
            imgView.sy_name("yemian_none_")
        case .netError:
            labC.text = "抱歉，服务器拒绝访问"
            labE.text = "Sorry, Server access denied."
            imgView.sy_name("yemian_net_error")
        case .pageError:
            labC.text = "对不起，您访问的页面不存在"
            labE.text = "Sorry, the page you want to access does not exist."
            imgView.sy_name("yemianerror_icon")
        
        }
    }
    
    override func initViews() {
        self.backgroundColor = UIColor(hex: 0xF8F8F8)
        self.configure(.dataNone)
        self.addSubview(imgView)
        self.addSubview(labC)
        self.addSubview(labE)
        self.addSubview(backBtn)
        
        imgView.snp.makeConstraints { (make) in
            
            make.top.equalToSuperview().offset(85)
            make.width.height.equalTo(185)
            make.centerX.equalToSuperview()
        }

        labC.snp.makeConstraints { (make) in
            make.top.equalTo(self.imgView.snp.bottom).offset(85)
            make.height.equalTo(17)
            make.centerX.equalToSuperview()
        }
        
        labE.snp.makeConstraints { (make) in
            make.top.equalTo(self.labC.snp.bottom).offset(0)
            make.height.equalTo(17)
            make.centerX.equalToSuperview()
        }
        
        backBtn.snp.makeConstraints { (make) in
            
            make.top.equalTo(self.labE.snp.bottom).offset(20)
            make.centerX.equalToSuperview()
            make.width.equalTo(95)
            make.height.equalTo(30)
        }
    }
    
}
