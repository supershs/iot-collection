//
//  SYNoDataView.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2020/9/1.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit

class SYNoDataView: SYBaseView {

    var imgHeight: CGFloat = 150
    let imgView: UIImageView = {
        let v = UIImageView()
        v.sy_name("no_data")
        return v
    }()
    
    let tipText: UILabel = {
        let v = UILabel()
        v.text = "这里什么都没有哦"
        v.font = UIFont.systemFont(ofSize: 18)
        v.textColor = UIColor(hex: 0x6F6F6F)
        return v
    }()
    
    let subTipText: UILabel = {
        let v = UILabel()
        v.text = "去首页看看有没有你感兴趣的吧"
        v.font = UIFont.systemFont(ofSize: 14)
        v.textColor = UIColor(hex: 0x808080)
        return v
    }()
    
    let btn: UIButton = {
        let v = UIButton()
        v.backgroundColor = .yellow
        v.setTitle("刷新", for: .normal)
        v.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        v.setTitleColor(.white, for: .normal)
        v.layer.cornerRadius = 5
        v.layer.masksToBounds = true
        return v
    }()
    
    override func initViews() {
        
        addSubview(imgView)
        addSubview(tipText)
        addSubview(subTipText)
        addSubview(btn)
        imgView.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(imgHeight)
        }
        tipText.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(imgView.snp.bottom).offset(10)
            make.left.right.equalToSuperview()
            make.height.equalTo(20)
        }
        subTipText.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(tipText.snp.bottom).offset(10)
            make.left.right.equalToSuperview()
            make.height.equalTo(20)
        }
        btn.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(subTipText.snp.bottom).offset(10)
            make.width.equalTo(200)
            make.height.equalTo(40)
        }
    }

}
