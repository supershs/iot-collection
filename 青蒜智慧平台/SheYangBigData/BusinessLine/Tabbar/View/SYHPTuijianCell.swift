//
//  SYHPTuijianCell.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2021/3/25.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit
import YYText

class SYHPTuijianCell: SYBaseCell , UICollectionViewDelegate, UICollectionViewDataSource{
    
    var models: [SYTourScenicSpotVOListModel] = []
    var collectionView: UICollectionView!
    var count : Int = 2
    var sepView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = Constant.bgViewColor
        return v
    }()
    

    func configure(_ model: [SYTourScenicSpotVOListModel]) {
        self.models = model
        if self.models.count > 1 && self.models.count < 3 {
            count = 1
        } else if self.models.count < 1 {
            count = 0
        }
        collectionView.snp.remakeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview().offset(15.autoWidth())
            make.right.equalToSuperview().offset(-15.autoWidth())
            make.height.equalTo(CGFloat(count)*205.autoWidth()+CGFloat(count-1)*10)
        }
        self.collectionView.reloadData()
    }
    
    override func initViews() {
        

        setCollectionView()
        
        contentView.addSubview(collectionView)
        contentView.addSubview(sepView)
        collectionView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview().offset(15.autoWidth())
            make.right.equalToSuperview().offset(-15.autoWidth())
            make.height.equalTo(CGFloat(count)*205.autoWidth()+CGFloat(count-1)*10)
        }

        sepView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.height.equalTo(10.0.autoWidth())
            make.bottom.equalToSuperview()
            make.top.equalTo(collectionView.snp.bottom).offset(15.autoWidth())
        }
    }
    
    func setCollectionView() {
        
            
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let itemWidth = 167.autoWidth()
        layout.itemSize = CGSize(width: itemWidth, height: 205.autoWidth())
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 10
        layout.scrollDirection = .vertical//.horizontal
        collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .white
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.isScrollEnabled = false // MARK: - tableview滑动时收缩的bug
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(SYHPTuijianSubCell.self, forCellWithReuseIdentifier: NSStringFromClass(SYHPTuijianSubCell.self))
        self.addSubview(self.collectionView)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.models.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: SYHPTuijianSubCell? = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(SYHPTuijianSubCell.self), for: indexPath) as? SYHPTuijianSubCell
        cell?.configure(self.models[indexPath.row])
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let vc = DYJingquDetailViewController(self.models[indexPath.row].id ?? "")
//        self.currentVC.navigationController?.pushViewController(vc, animated: true)
    }
    
}



class SYHPTuijianSubCell: SYBaseCollectionViewCell {
    
    
    var bgView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = Constant.bgViewColor
        v.layer.cornerRadius = 5.autoWidth()
        v.clipsToBounds = true
//        v.layer.shadowColor = UIColor(hex: 0xB3B3B3).cgColor
//        v.layer.shadowOpacity = 0.5//阴影透明度
//        v.layer.shadowOffset = CGSize.zero///阴影偏移量,四周
//        v.layer.shadowRadius = 2.5//阴影半径  shadowPathWidth：阴影宽度
        
        return v
    }()
    
    var locationBgView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = UIColor(white: 0, alpha: 0.5)
        v.layer.cornerRadius = 9.autoWidth()
        v.layer.masksToBounds = true
        
        return v
    }()
    
    let location: UILabel = {
        let v:UILabel = UILabel()
        v.textColor = .white
        v.font = 10.autoFontSize()
        
        v.layer.cornerRadius = 9.autoWidth()
        v.layer.masksToBounds = true
        return v
    }()
    
    let title: UILabel = {
        let v:UILabel = UILabel()
        v.textColor = UIColor(hex: 0x333333)
        v.font = 14.autoFontSize()
        
        return v
    }()
    
    let priceLb: YYLabel = {
        let v:YYLabel = YYLabel()
        v.textColor = UIColor(hex: 0xF55035)
        v.font = 15.autoFontSize()
        
        return v
    }()
    
    let pic: UIImageView = {
        let v: UIImageView = UIImageView()
        return v
    }()
    
    let locationPic: UIImageView = {
        let v: UIImageView = UIImageView()
        v.sy_name("homepage_location")
        return v
    }()
    

    public func configure(_ model: SYTourScenicSpotVOListModel) {
        location.text = model.address
        title.text = model.name
        
        if let r = model.rates, r != "", r != "免费景点" {
            let showStr = "¥\(model.rates ?? "") 起"
            let attString = NSMutableAttributedString(string: showStr)
            attString.yy_setColor(UIColor(hex: 0xF55035), range: NSRange(location: 0, length: 1))
            attString.yy_setFont(12.autoFontSize(), range: NSRange(location: 0, length: 1))
            attString.yy_setColor(UIColor(hex: 0xF55035), range: NSRange(location: 1, length: showStr.count-2))
            attString.yy_setFont(18.autoFontSize(), range: NSRange(location: 1, length: showStr.count-2))
            attString.yy_setColor(UIColor(hex: 0x333333), range: NSRange(location: showStr.count-1, length: 1))
            attString.yy_setFont(10.autoFontSize(), range: NSRange(location: showStr.count-1, length: 1))
            priceLb.attributedText = attString
        } else {
            priceLb.text = "免费景点"
        }
        pic.sy_setWithUrl(model.imgUrl)
    }
    
    override func initViews() {
        
        addSubview(bgView)
        bgView.addSubview(pic)
        bgView.addSubview(locationBgView)
        bgView.addSubview(title)
        bgView.addSubview(location)
        bgView.addSubview(priceLb)
        bgView.addSubview(locationPic)
        bgView.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.top.bottom.equalToSuperview()
        }
        title.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(160.autoWidth())
            make.left.equalToSuperview().offset(10.autoWidth())
            make.right.equalToSuperview().offset(-10.autoWidth())
            make.height.equalTo(16.autoWidth())
        }
        pic.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.right.equalToSuperview()
            make.width.equalTo(167.0.autoWidth())
            make.height.equalTo(150.0.autoWidth())
            make.bottom.equalToSuperview().offset(-55.autoWidth())
        }
        priceLb.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-8.autoWidth())
            make.left.equalToSuperview().offset(10.autoWidth())
            make.right.equalToSuperview().offset(-10.autoWidth())
            make.height.equalTo(18.autoWidth())
        }
        
        locationBgView.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-60.autoWidth())
            make.left.equalToSuperview().offset(5.autoWidth())
            make.right.equalTo(location.snp.right).offset(5.autoWidth())
            make.height.equalTo(18.autoWidth())
        }
        
        location.snp.makeConstraints { (make) in
            make.top.bottom.equalTo(locationBgView)
            make.left.equalTo(locationBgView).offset(21.autoWidth())
            make.right.lessThanOrEqualToSuperview().offset(-10.autoWidth())
        }
        locationPic.snp.makeConstraints { (make) in
            make.centerY.equalTo(locationBgView)
            make.left.equalToSuperview().offset(13.autoWidth())
            make.height.equalTo(9.5.autoWidth())
            make.width.equalTo(8.autoWidth())
        }
    }
}

