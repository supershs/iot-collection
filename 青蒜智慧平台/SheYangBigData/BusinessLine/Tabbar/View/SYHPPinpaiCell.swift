//
//  SYHPPinpaiCell.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2021/3/25.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit


class  SYHPPinpaiCell: SYBaseCell , UICollectionViewDelegate, UICollectionViewDataSource{
    
    var collectionView: UICollectionView!
    
    var models:[SYPublicBrandListModel] = []
    
    var sepView: UIView = {
        let v = UIView()
        v.backgroundColor = Constant.bgViewColor
        return v
    }()

    public func configure(_ model: [SYPublicBrandListModel]) {
        self.models = model
        self.collectionView.reloadData()
    }
    
    override func initViews() {
        
        setCollectionView()
        self.contentView.addSubview(collectionView)
        self.contentView.addSubview(sepView)
        collectionView.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-25.autoWidth())
            make.left.equalToSuperview().offset(15.autoWidth())
            make.right.top.equalToSuperview()
            make.height.equalTo(75.autoWidth())
        }
        sepView.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(10.autoWidth())
        }
    }
    
    func setCollectionView() {
        
            
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let itemWidth = 75.autoWidth()
        layout.itemSize = CGSize(width: itemWidth, height: 75.autoWidth())
//        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 10
        layout.scrollDirection = .horizontal
        collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .white
        collectionView.showsHorizontalScrollIndicator = false
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(SYHPPinpaiSubCell.self, forCellWithReuseIdentifier: NSStringFromClass(SYHPPinpaiSubCell.self))
        self.addSubview(self.collectionView)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return models.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: SYHPPinpaiSubCell? = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(SYHPPinpaiSubCell.self), for: indexPath) as? SYHPPinpaiSubCell
        cell?.configure(models[indexPath.row])
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.currentVC.sy_pushWebVC(.pinpaiDetail(id: self.models[indexPath.row].id ?? ""))
    }
    
  
}


class SYHPPinpaiSubCell: SYBaseCollectionViewCell {

    let productImgView: UIImageView = {
        let v = UIImageView()
        
        v.layer.cornerRadius = 3.autoWidth()
        v.layer.borderWidth = 0.5.autoWidth()
        v.layer.borderColor = Constant.bgViewColor.cgColor
        return v
    }()
    
    let tipImgView: UIImageView = {
        let v = UIImageView()
        v.sy_name("homepage_pinpai_tip")
        return v
    }()
    
    
    func configure(_ model: SYPublicBrandListModel) {
        productImgView.sy_setWithUrl(model.logoUrl ?? "")
        
    }
    
    override func initViews() {
        addSubview(productImgView)
        addSubview(tipImgView)
        
        productImgView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
            make.width.equalTo(75.autoWidth())
            make.height.equalTo(75.autoWidth())
        }
        tipImgView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(3.autoWidth())
            make.left.equalToSuperview()
            make.width.equalTo(46.autoWidth())
            make.height.equalTo(15.autoWidth())
        }
        
    }

}
