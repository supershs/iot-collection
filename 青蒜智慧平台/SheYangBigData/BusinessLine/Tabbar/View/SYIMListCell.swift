//
//  SYIMListCell.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/1/18.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit


class SYIMListCell: SYBaseCell {

    
    let nameLb: UILabel = {
        let v:UILabel = UILabel()
        v.textColor = UIColor(hex: 0x333333)
        v.font = 16.autoFontSize()
        
        return v
    }()
    
    let contentLb: UILabel = {
        let v:UILabel = UILabel()
        v.textColor = UIColor(hex: 0x666666)
        v.font = 12.autoFontSize()
        
        return v
    }()
    
    let timeLb: UILabel = {
        let v:UILabel = UILabel()
        v.textColor =  UIColor(hex: 0x666666)
        v.font = 12.autoFontSize()
        
        return v
    }()
    
    let pic: UIImageView = {
        let v: UIImageView = UIImageView()
        v.layer.cornerRadius = 20.0.autoWidth()
        v.layer.masksToBounds = true
        return v
    }()
    
    let unreadLb: UILabel = {
        let v:UILabel = UILabel()
        v.textColor =  UIColor.white
        v.font = 10.autoFontSize()
        v.textAlignment = .center
        v.layer.cornerRadius = 7.5
        v.layer.masksToBounds = true
        v.backgroundColor = UIColor(hex: 0xFF5400)
        return v
    }()
    

    public func configureView(_ model: SYIMListModel) {
        if true == model.typeName?.contains("预警") {
            nameLb.textColor = Constant.redColor
        } else {
            nameLb.textColor = UIColor(hex: 0x333333)
        }
        nameLb.text = model.typeName
        switch model.info?.type ?? "" {
        case "image":
            contentLb.text = "[图片]"
        case "video":
            contentLb.text = "[视频]"
        case "audio":
            contentLb.text = "[语音]"
        case "text":
            if model.info?.message?.contains("/:") == true {
                contentLb.text = "[表情]"
            } else {
                contentLb.text = model.info?.message
            }
            
        default:
            contentLb.text = model.info?.message
        }
        
        print(model.info?.noReadNum)
        timeLb.text = model.info?.time
        if let n = model.info?.noReadNum, let num = Int(n), num > 0 {
            unreadLb.text = n
            unreadLb.isHidden = false
        } else {
            unreadLb.text = ""
            unreadLb.isHidden = true
        }
        pic.sy_setWithUrl(model.icon)
    }
    
    override func initViews() {
        
        addSubview(pic)
        addSubview(nameLb)
        addSubview(contentLb)
        addSubview(timeLb)
        addSubview(lineView)
        addSubview(unreadLb)
        pic.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(autoWidth(12.0))
            make.bottom.equalToSuperview().offset(autoWidth(-12.0))
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(autoWidth(15.0))
            make.width.height.equalTo(40.0.autoWidth())
        }
        unreadLb.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(autoWidth(8.0))
            make.right.equalTo(pic)
            make.height.equalTo(15.0.autoWidth())
            make.width.greaterThanOrEqualTo(15.autoWidth())
        }
        nameLb.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(autoWidth(13.0))
            make.left.equalTo(pic.snp.right).offset(autoWidth(9))
            make.height.equalTo(16.0.autoWidth())
        }
        contentLb.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(autoWidth(38))
            make.left.equalTo(pic.snp.right).offset(autoWidth(9))
            make.right.equalToSuperview().offset(autoWidth(-15.0))
            make.height.equalTo(12.0.autoWidth())
        }
        timeLb.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(autoWidth(17))
            make.right.equalToSuperview().offset(autoWidth(-15.0))
            make.height.equalTo(12.0.autoWidth())
        }
        lineView.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview()
            make.left.equalTo(pic.snp.right).offset(autoWidth(9))
            make.right.equalToSuperview().offset(autoWidth(-15.0))
            make.height.equalTo(0.5.autoWidth())
        }
    }
}


class SYIMListTopView: SYBaseView {
    
    var bgView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = .white
        v.layer.cornerRadius = 5
        
        v.layer.shadowColor = UIColor(hex: 0xB3B3B3).cgColor
        v.layer.shadowOpacity = 0.5//阴影透明度
        v.layer.shadowOffset = CGSize.zero///阴影偏移量,四周
        v.layer.shadowRadius = 2.5//阴影半径  shadowPathWidth：阴影宽度
        return v
    }()
    
    let hudong: UILabel = {
        let v = UILabel()
        v.text = "互动消息"
        v.font = 14.autoFontSize()
        v.textColor = UIColor(hex: 0x1D1D1D)
        return v
    }()
    
    let zhixun: UILabel = {
        let v = UILabel()
        v.text = "咨询"
        v.font = 14.autoFontSize()
        v.textColor = UIColor(hex: 0x1D1D1D)
        return v
    }()
    
    let tongzhi: UILabel = {
        let v = UILabel()
        v.text = "账户通知"
        v.font = 14.autoFontSize()
        v.textColor = UIColor(hex: 0x1D1D1D)
		v.isHidden = true
        return v
    }()
    
    let hudongImg: UIImageView = {
        let v = UIImageView()
        v.sy_name("xx_hudong")
        return v
    }()
    
    let zhixunImg: UIImageView = {
        let v = UIImageView()
        v.sy_name("xx_zhixun")
        return v
    }()
    
    let tongzhiImg: UIImageView = {
        let v = UIImageView()
        v.sy_name("xx_tongzhi")
		v.isHidden = true
		
        return v
    }()
    
    let hudongBt: UIButton = {
        let v = UIButton()
        return v
    }()
    
    let zhixunBt: UIButton = {
        let v = UIButton()
        return v
    }()
    
    let tongzhiBt: UIButton = {
        let v = UIButton()
		v.isHidden = true
        return v
    }()
    
    override func initViews() {
        self.backgroundColor = .clear
        hudongBt.addAction {[weak self] in
            if let `self` = self {
                    
            }
        }
        zhixunBt.addAction {[weak self] in
            if let `self` = self {
                self.currentVC.sy_pushWebVC(.im_zhixun)
            }
        }
        tongzhiBt.addAction {[weak self] in
            if let `self` = self {
                    
            }
        }
        
        addSubview(bgView)
        addSubview(hudong)
        addSubview(zhixun)
        addSubview(tongzhi)
        addSubview(hudongImg)
        addSubview(zhixunImg)
        addSubview(tongzhiImg)
        addSubview(hudongBt)
        addSubview(zhixunBt)
        addSubview(tongzhiBt)
        
        bgView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview().offset(10.autoWidth())
            make.right.equalToSuperview().offset(-10.autoWidth())
            make.height.equalTo(105.autoWidth())
        }
        hudongImg.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(20.autoWidth())
            make.centerX.equalToSuperview().offset(-68.autoWidth())
            make.width.height.equalTo(42.autoWidth())
        }
        zhixunImg.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(20.autoWidth())
			make.centerX.equalToSuperview().offset(68.autoWidth())
            make.width.height.equalTo(42.autoWidth())
        }
        tongzhiImg.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(20.autoWidth())
            make.centerX.equalToSuperview().offset(117.autoWidth())
            make.width.height.equalTo(42.autoWidth())
        }
        hudong.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(72.autoWidth())
			make.centerX.equalTo(hudongImg)
            make.height.equalTo(13.autoWidth())
        }
        zhixun.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(72.autoWidth())
            make.centerX.equalTo(zhixunImg)
            make.height.equalTo(13.autoWidth())
        }
        tongzhi.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(72.autoWidth())
            make.centerX.equalToSuperview().offset(117.autoWidth())
            make.height.equalTo(13.autoWidth())
        }
        hudongBt.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.centerX.equalTo(hudongImg)
            make.width.equalTo(80.autoWidth())
        }
        zhixunBt.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.centerX.equalTo(zhixunImg)
            make.width.equalTo(80.autoWidth())
        }
        tongzhiBt.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.centerX.equalTo(tongzhiImg)
            make.width.equalTo(80.autoWidth())
        }
    }
}
