//
//  SYjianbianView.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/5/20.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit

class SYjianbianView: SYBaseView {
    
    var bgView: UIView = {
        let v: UIView = UIView()
        return v
    }()
    
    var bt: UIButton = {
        let v: UIButton = UIButton()
        return v
    }()
    
    override func initViews() {
        addSubview(bgView)
        addSubview(bt)
        bgView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        bt.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
}
