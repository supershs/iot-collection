//
//  SYLoginView.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2020/8/21.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import SnapKit

class SYLoginView: UIView {
    
    let minUsernameLength = 1
    let maxUsernameLength = 11
    let minPasswordLength = 1
    let maxPasswordLength = 16
    let disposBag = DisposeBag()
    var loginProtocol: SYLoginProtocol!
    var viewController: UIViewController!
    
    var usernameTF: UITextField = {
        let tf = UITextField()
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.textColor = .black
        tf.placeholder = "请输入账号"
        tf.text = "13382336992"
        return tf
    }()
    var usernameLB: UILabel = {
        let lb = UILabel()
        lb.textColor = .red
        lb.font = UIFont.systemFont(ofSize: 12)
        lb.text = "用户名必须是5-10位的"
        return lb
    }()
    var passwordTF: UITextField = {
        let tf = UITextField()
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.textColor = .black
        tf.placeholder = "请输入密码"
        tf.text = "123456a"
        return tf
    }()
    var passwordLB: UILabel = {
        let lb = UILabel()
        lb.textColor = .red
        lb.font = UIFont.systemFont(ofSize: 12)
        lb.text = "密码必须是5-16位的"
        return lb
    }()
    var loginButton: UIButton = {
        let v = UIButton()
        v.backgroundColor = UIColor.white
        v.layer.cornerRadius = 22.5
        v.layer.masksToBounds = true
        v.titleLabel?.font = UIFont.boldSystemFont(ofSize: 19)
        v.setTitleColor(UIColor.init(hex: 0x39CA8C), for: .normal)
        v.setTitle("登录", for: .normal)
        return v
    }()
    var registerButton: UIButton = {
        let bt = UIButton()
        bt.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        bt.setTitle("注册", for: .normal)
        bt.setTitleColor(UIColor.white, for: .normal)
        return bt
    }()
    var verifiCodeButton: UIButton = {
        let bt = UIButton()
        bt.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        bt.setTitle("验证码登录", for: .normal)
        bt.setTitleColor(UIColor.white, for: .normal)
        return bt
    }()
    var bgImgView: UIImageView = {
        let v = UIImageView()
        v.sy_name("login_bg")
        return v
    }()
    var logoImgView: UIImageView = {
        let v = UIImageView()
        v.sy_name("login_topicon")
        return v
    }()
    var accountImgView: UIImageView = {
        let v = UIImageView()
        v.sy_name("login_account")
        return v
    }()
    var passwordImgView: UIImageView = {
        let v = UIImageView()
        v.sy_name("login_password")
        return v
    }()
    var accountSepView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.init(hex: 0xCBFFEC)
        return v
    }()
    var passwordSepView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.init(hex: 0xCBFFEC)
        return v
    }()
    var thirdLoginLB: UILabel = {
        let lb = UILabel()
        lb.textColor = .white
        lb.font = UIFont.systemFont(ofSize: 13)
        lb.text = "----   第三方登录   ----"
        return lb
    }()
    var thirdLoginBtn: UIButton = {
        let v = UIButton()
        v.setImage(UIImage(named: "login_weixin"), for: .normal)
        return v
    }()
    
    var backBtn: UIButton = {
        let v = UIButton()
        v.setImage(UIImage(named: "nav_back"), for: .normal)
        return v
    }()
    
    
    init(frame: CGRect, loginProtocol: SYLoginProtocol, viewController: UIViewController) {
        super.init(frame: frame)
        self.loginProtocol = loginProtocol
        self.viewController = viewController
        initViews()
        events()
        plugs()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initViews() {
        
        self.addSubview(bgImgView)
        self.addSubview(usernameTF)
        self.addSubview(usernameLB)
        self.addSubview(passwordTF)
        self.addSubview(passwordLB)
        self.addSubview(loginButton)
        self.addSubview(registerButton)
        self.addSubview(logoImgView)
        self.addSubview(accountImgView)
        self.addSubview(passwordImgView)
        self.addSubview(thirdLoginLB)
        self.addSubview(accountSepView)
        self.addSubview(passwordSepView)
        self.addSubview(verifiCodeButton)
        self.addSubview(thirdLoginLB)
        self.addSubview(thirdLoginBtn)
        
        
        bgImgView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        logoImgView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(STATUSBAR_HEIGHT + 20)
            make.centerX.equalToSuperview()
            make.width.equalTo(145)
            make.height.equalTo(180)
        }
        accountImgView.snp.makeConstraints { (make) in
            make.top.equalTo(self.logoImgView.snp.bottom).offset(25)
            make.left.equalToSuperview().offset(26)
            make.width.equalTo(15)
            make.height.equalTo(20)
        }
        passwordImgView.snp.makeConstraints { (make) in
            make.top.equalTo(self.accountImgView.snp.bottom).offset(44)
            make.left.equalTo(self.accountImgView)
            make.width.equalTo(16)
            make.height.equalTo(18)
        }
        usernameTF.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.accountImgView)
            make.left.equalTo(self.accountImgView.snp.right).offset(19)
            make.right.equalToSuperview().offset(-25)
            make.height.equalTo(25)
        }
        usernameLB.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.accountImgView).offset(23)
            make.left.equalTo(self.accountImgView.snp.right).offset(19)
            make.right.equalToSuperview().offset(-25)
            make.height.equalTo(25)
        }
        accountSepView.snp.makeConstraints { (make) in
            make.top.equalTo(self.usernameTF.snp.bottom)
            make.left.right.equalTo(self.usernameTF)
            make.height.equalTo(LINE_HEIGHT)
        }
        passwordTF.snp.makeConstraints { (make) in
            make.centerY.equalTo(passwordImgView)
            make.left.equalTo(passwordImgView.snp.right).offset(19)
            make.right.equalToSuperview().offset(-25)
            make.height.equalTo(25)
        }
        passwordLB.snp.makeConstraints { (make) in
            make.centerY.equalTo(passwordImgView).offset(23)
            make.left.equalTo(passwordImgView.snp.right).offset(19)
            make.right.equalToSuperview().offset(-25)
            make.height.equalTo(25)
        }
        passwordSepView.snp.makeConstraints { (make) in
            make.top.equalTo(passwordTF.snp.bottom)
            make.left.right.equalTo(passwordTF)
            make.height.equalTo(LINE_HEIGHT)
        }
        loginButton.snp.makeConstraints { (make) in
            make.top.equalTo(passwordSepView.snp.bottom).offset(53)
            make.left.equalToSuperview().offset(25)
            make.right.equalToSuperview().offset(-25)
            make.height.equalTo(45)
        }
        registerButton.snp.makeConstraints { (make) in
            make.top.equalTo(loginButton.snp.bottom).offset(21)
            make.centerX.equalToSuperview()
            make.width.equalTo(150)
            make.height.equalTo(25)
        }
        verifiCodeButton.snp.makeConstraints { (make) in
            make.top.equalTo(registerButton.snp.bottom).offset(10)
            make.centerX.equalToSuperview()
            make.width.equalTo(150)
            make.height.equalTo(25)
        }
        thirdLoginLB.snp.makeConstraints { (make) in
            make.bottom.equalTo(thirdLoginBtn.snp.top).offset(-30)
            make.centerX.equalToSuperview()
            make.height.equalTo(15)
        }
        thirdLoginBtn.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-33)
            make.centerX.equalToSuperview()
            make.width.height.equalTo(39)
        }
    }
    
    func events() {
        /*
         *  .map负责对UITextField中的字符进行处理，判断字符长度，是否符合要求，将判断的值返回给usernameValid和passwordValid
         *
         *  shareReplay()是RxSwift提供的一个流操作函数，它是以重播的方式通知自己的订阅者，保证在观察者订阅这个流的时候始终都能回播最后N个，shareReplay(1)表示重播最后一个。
         *  shareReplay 会返回一个新的事件序列，它监听底层序列的事件，并且通知自己的订阅者们。 解决有多个订阅者的情况下，map会被执行多次的问题。
         */
        let usernameValid = usernameTF.rx.text
            .map { $0!.count >= self.minUsernameLength && $0!.count <= self.maxUsernameLength }
            .share(replay: 1)
        
        let passwordValid = passwordTF.rx.text
            .map { $0!.count >= self.minPasswordLength && $0!.count < self.maxPasswordLength }
            .share(replay: 1)
        
        let everythingValid = Observable.combineLatest(usernameValid, passwordValid) { (usernameValid, passwordValid) -> Bool in
            usernameValid && passwordValid
        }
        
        // 或者
        //        let everythingValid = Observable.combineLatest(usernameValid, passwordValid) { $0 && $1 }
        //        .shareReplay(1)
        
        /*  绑定
         *  将usernameValid和passwordTF.rx_enabled绑定，即用usernameValid来控制passwordTF是否可以输入的状态
         *  bindTo就是RxSwfit中用来进行值绑定的函数
         */
        usernameValid
            .bind(to: passwordTF.rx.isEnabled) //username通过验证，passwordTF才可以输入
            .disposed(by: disposBag)
        
        usernameValid
            .bind(to: usernameLB.rx.isHidden)
            .disposed(by: disposBag)
        
        passwordValid
            .bind(to: passwordLB.rx.isHidden)
            .disposed(by: disposBag)
        
        everythingValid
            .bind(to: loginButton.rx.isEnabled) // 用户名密码都通过验证，才可以点击按钮
            .disposed(by: disposBag)
        
        loginButton.rx.tap //绑定button点击事件
            .subscribe(onNext: { [weak self] in
                if let `self` = self {
                    self.loginProtocol.login(loginName: self.usernameTF.text ?? "", password: self.passwordTF.text ?? "")
                }
            })
            .disposed(by: disposBag)
        
        registerButton.rx.tap
            .subscribe(onNext: { [weak self] in
                let vc = SYRegisterViewController()
                self?.viewController.navigationController?.pushViewController(vc, animated: true)
            })
            .disposed(by: disposBag)
        
        verifiCodeButton.rx.tap
            .subscribe(onNext: { [weak self] in
                let vc = SYVerifiCodeLoginViewController()
                self?.viewController.navigationController?.pushViewController(vc, animated: true)
            })
            .disposed(by: disposBag)
        
        thirdLoginBtn.rx.tap
            .subscribe(onNext: { [weak self] in
                self?.loginProtocol.thirdLogin()
            })
            .disposed(by: disposBag)
        
    }
    
    func plugs() {
        switch loginProtocol.loginType {
        case .account:
            print("account")
        case .verifiCode:
            back()
            verifiCodeViews()
        case .register:
            back()
            registerViews()
        }
    }
    
    func back() {
        
        self.addSubview(backBtn)
        backBtn.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(STATUSBAR_HEIGHT + 20)
            make.left.equalToSuperview().offset(30)
        }
        
        backBtn.rx.tap
            .subscribe(onNext: { [weak self] in
                self?.viewController.navigationController?.popViewController(animated: true)
            })
            .disposed(by: disposBag)
        
        registerButton.isHidden = true
        verifiCodeButton.isHidden = true
        thirdLoginBtn.isHidden = true
        thirdLoginLB.isHidden = true
    }
}
