//
//  SYLoginView+Register.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2020/8/21.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit

extension SYLoginView {

    
    func registerViews() {
        
        let passwordSecTF: UITextField = {
            let tf = UITextField()
            tf.font = UIFont.systemFont(ofSize: 14)
            tf.textColor = .black
            tf.placeholder = "请再次输入密码"
            return tf
        }()
        let passwordSecImgView: UIImageView = {
            let v = UIImageView()
            v.sy_name("login_password")
            return v
        }()
        let passwordSecSepView: UIView = {
            let v = UIView()
            v.backgroundColor = UIColor.init(hex: 0xCBFFEC)
            return v
        }()
        let passwordSecLB: UILabel = {
            let lb = UILabel()
            lb.textColor = .red
            lb.font = UIFont.systemFont(ofSize: 12)
            lb.text = "密码必须是5-16位的"
            return lb
        }()
        
        addSubview(passwordSecTF)
        addSubview(passwordSecImgView)
        addSubview(passwordSecSepView)
        addSubview(passwordSecLB)
        
        passwordSecImgView.snp.makeConstraints { (make) in
            make.top.equalTo(self.passwordImgView.snp.bottom).offset(44)
            make.left.equalTo(self.passwordImgView)
            make.width.equalTo(16)
            make.height.equalTo(18)
        }
        passwordSecTF.snp.makeConstraints { (make) in
            make.centerY.equalTo(passwordSecImgView)
            make.left.equalTo(passwordSecImgView.snp.right).offset(19)
            make.right.equalToSuperview().offset(-25)
            make.height.equalTo(25)
        }
        passwordSecLB.snp.makeConstraints { (make) in
            make.centerY.equalTo(passwordSecImgView).offset(23)
            make.left.equalTo(passwordSecImgView.snp.right).offset(19)
            make.right.equalToSuperview().offset(-25)
            make.height.equalTo(25)
        }
        passwordSecSepView.snp.makeConstraints { (make) in
            make.top.equalTo(passwordSecTF.snp.bottom)
            make.left.right.equalTo(passwordSecTF)
            make.height.equalTo(LINE_HEIGHT)
        }
        loginButton.snp.remakeConstraints { (make) in
            make.top.equalTo(passwordSecSepView.snp.bottom).offset(53)
            make.left.equalToSuperview().offset(25)
            make.right.equalToSuperview().offset(-25)
            make.height.equalTo(45)
        }
        let passwordSec = passwordSecTF.rx.text
        .map { $0!.count >= self.minPasswordLength && $0!.count <= self.maxPasswordLength }
        .share(replay: 1)
        
        passwordSec
        .bind(to: passwordSecLB.rx.isHidden)
        .disposed(by: disposBag)
        
        loginButton.setTitle("注册", for: .normal)
        
        usernameTF.placeholder = "请输入账号"
        passwordTF.placeholder = "请输入密码"
    }
}
