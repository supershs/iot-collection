//
//  SYPhotoBrowser.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/6/28.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit
import Kingfisher
import ZLPhotoBrowser
import AVKit
import Photos


class SYPhotoBrowser: SYBaseView, UICollectionViewDelegate, UICollectionViewDataSource {

    var collectionView: UICollectionView!
    var photoNum: Int = 0
    var arrDataSources: [ZLPhotoModel] = []
    var arrSelectedModels: [ZLPhotoModel] = []
    var albumList: ZLAlbumListModel!
    
    var bgBt: UIButton = {
        let v = UIButton()
        v.backgroundColor = .clear
        return v
    }()
    
    var closeBt: VGImageButton = {
        let v = VGImageButton()
        v.imageView.sy_name("tuku_close")
        return v
    }()
    
    var bgView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = .white
        return v
    }()
    
    var collectionBgView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = Constant.bgViewColor
        return v
    }()
    
    var titleLb: UILabel = {
        let v: UILabel = UILabel()
        v.font = 18.autoMediumFontSize()
        v.textColor = UIColor(hex: 0x111111)
        v.text = "图片库"
        v.textAlignment = .center
        return v
    }()
    
    override func layoutSubviews() {
        
        bgView.corner(byRoundingCorners: UIRectCorner(rawValue: UIRectCorner.topRight.rawValue | UIRectCorner.topLeft.rawValue), radii: 15.autoWidth())
    }
    
    override func initViews() {
        
        bgBt.addAction {[weak self] in
            if let `self` = self {
                self.clickedClosure?(0)
            }
        }
        closeBt.btn.addAction {[weak self] in
            if let `self` = self {
                self.clickedClosure?(0)
            }
        }
        
        self.backgroundColor = .clear
        setCollectionView()
        self.addSubview(bgBt)
        addSubview(bgView)
        addSubview(titleLb)
        addSubview(closeBt)
        addSubview(collectionBgView)
        self.addSubview(collectionView)
       
        bgBt.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        bgView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(108.autoWidth() + STATUSBAR_HEIGHT)
            make.right.left.equalToSuperview()
            make.height.equalTo(50.autoWidth())
        }
        titleLb.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(108.autoWidth() + STATUSBAR_HEIGHT)
            make.right.left.equalToSuperview()
            make.height.equalTo(50.autoWidth())
        }
        closeBt.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(118.autoWidth() + STATUSBAR_HEIGHT)
            make.right.equalToSuperview().offset(-15.0.autoWidth())
        }
        collectionBgView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(158.autoWidth() + STATUSBAR_HEIGHT)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        collectionView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(158.autoWidth() + STATUSBAR_HEIGHT)
            make.left.equalToSuperview().offset(15.0.autoWidth())
            make.right.equalToSuperview().offset(-15.0.autoWidth())
            make.bottom.equalToSuperview()
        }
        showThumbnailViewController()
    }

    func setCollectionView() {
        
            
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        
        layout.itemSize = CGSize(width: 110.autoWidth(), height: 110.autoWidth())
        layout.minimumLineSpacing = 6.autoWidth() // 行间距
        layout.minimumInteritemSpacing = 6.autoWidth()
        layout.scrollDirection = .vertical
        collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collectionView.backgroundColor = Constant.bgViewColor
        collectionView.showsHorizontalScrollIndicator = false
//        collectionView.d
        
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(SYImageCell.self, forCellWithReuseIdentifier: NSStringFromClass(SYImageCell.self))
        self.addSubview(self.collectionView)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrDataSources.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: SYImageCell? = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(SYImageCell.self), for: indexPath) as? SYImageCell
        
        let model = self.arrDataSources[indexPath.row]
        cell?.configureView(model)
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell:SYImageCell = collectionView.cellForItem(at: indexPath) as! SYImageCell
        self.passParamsClosure?(cell.imageView.image)
    }
    
    func showThumbnailViewController() {
        self.arrDataSources.removeAll()
        ZLPhotoConfiguration.default().allowSelectVideo = false
        ZLPhotoConfiguration.default().maxSelectCount = 1
        ZLPhotoConfiguration.default().allowTakePhotoInLibrary = false
        ZLPhotoConfiguration.default().sortAscending = false
        ZLPhotoConfiguration.default().maxSelectCount = 3
        ZLPhotoConfiguration.default().showSelectBtnWhenSingleSelect = true
        ZLPhotoManager.getCameraRollAlbum(allowSelectImage: ZLPhotoConfiguration.default().allowSelectImage, allowSelectVideo: ZLPhotoConfiguration.default().allowSelectVideo) { [weak self] (cameraRoll) in
            guard let `self` = self else { return }
            self.albumList = cameraRoll
            self.loadPhotos()
        }
    }
    
    func loadPhotos() {
        
        if self.albumList.models.isEmpty {
//            let hud = ZLProgressHUD(style: ZLPhotoConfiguration.default().HUDStyle)
//            hud.show()
            DispatchQueue.global().async {
                self.albumList.refetchPhotos()
                DispatchQueue.main.async {
                    self.arrDataSources.removeAll()
                    self.arrDataSources.append(contentsOf: self.albumList.models)
                    self.markSelected(source: &self.arrDataSources, selected: &self.arrSelectedModels)
//                    hud.hide()
                    self.collectionView.reloadData()
                }
            }
        } else {
            self.arrDataSources.removeAll()
            self.arrDataSources.append(contentsOf: self.albumList.models)
            self.markSelected(source: &self.arrDataSources, selected: &self.arrSelectedModels)
            self.collectionView.reloadData()
        }
    }
    
    func fetchPhoto(in result: PHFetchResult<PHAsset>, ascending: Bool, allowSelectImage: Bool, allowSelectVideo: Bool, limitCount: Int = .max) -> [ZLPhotoModel] {
        var models: [ZLPhotoModel] = []
        let option: NSEnumerationOptions = ascending ? .init(rawValue: 0) : .reverse
        var count = 1
        
        result.enumerateObjects(options: option) { (asset, index, stop) in
            let m = ZLPhotoModel(asset: asset)
            
            if m.type == .image, !allowSelectImage {
                return
            }
            if m.type == .video, !allowSelectVideo {
                return
            }
            if count == limitCount {
                stop.pointee = true
            }
            
            models.append(m)
            count += 1
        }
        
        return models
    }
    
    func markSelected(source: inout [ZLPhotoModel], selected: inout [ZLPhotoModel]) {
        guard selected.count > 0 else {
            return
        }
        
        var selIds: [String: Bool] = [:]
        var selEditImage: [String: UIImage] = [:]
        var selEditModel: [String: ZLEditImageModel] = [:]
        var selIdAndIndex: [String: Int] = [:]
        
        for (index, m) in selected.enumerated() {
            selIds[m.ident] = true
            selEditImage[m.ident] = m.editImage
            selEditModel[m.ident] = m.editImageModel
            selIdAndIndex[m.ident] = index
        }
        
        source.forEach { (m) in
            if selIds[m.ident] == true {
                m.isSelected = true
                m.editImage = selEditImage[m.ident]
                m.editImageModel = selEditModel[m.ident]
                selected[selIdAndIndex[m.ident]!] = m
            } else {
                m.isSelected = false
            }
        }
    }
    
    
}


class SYImageCell: SYBaseCollectionViewCell {
    
    
    var smallImageRequestID: PHImageRequestID = PHInvalidImageRequestID
    var imageIdentifier: String = ""
    var model: ZLPhotoModel!
    var isVideo: Bool = false {
        didSet {
            playImg.isHidden = !isVideo
        }
    }
    //当前cell索引
    var cellIndexPath: IndexPath?
    
    let imageView: UIImageView = {
        let v: UIImageView = UIImageView()
        v.image = UIImage(named: "add_image")
        v.contentMode = .scaleAspectFill
        v.clipsToBounds = true
        return v
    }()
    
    let deletePic: UIButton = {
        let v: UIButton  = UIButton()
        v.setImage(UIImage(named: "delete_image"), for: .normal)
        v.layer.cornerRadius = 6
        return v
    }()
    
    let playImg: UIImageView = {
        let v = UIImageView()
        v.sy_name("play")
        v.isHidden = true
        return v
    }()

    public func configureView(_ model: ZLPhotoModel) {
        self.model = model
        fetchSmallImage()
    }
    
    public func configureView(_ model: SYRecognizeModel) {
        imageView.sy_setWithUrl(model.imgUrl)
    }
    
    func fetchSmallImage() {
        
        let size: CGSize
        let maxSideLength = self.bounds.width * 1.2
        if self.model.whRatio > 1 {
            let w = maxSideLength * self.model.whRatio
            size = CGSize(width: w, height: maxSideLength)
        } else {
            let h = maxSideLength / self.model.whRatio
            size = CGSize(width: maxSideLength, height: h)
        }
        
        if self.smallImageRequestID > PHInvalidImageRequestID {
            PHImageManager.default().cancelImageRequest(self.smallImageRequestID)
        }
        
        self.imageIdentifier = self.model.ident
        self.imageView.image = nil
        self.smallImageRequestID = ZLPhotoManager.fetchImage(for: self.model.asset, size: size, completion: { [weak self] (image, isDegraded) in
            if self?.imageIdentifier == self?.model.ident {
                self?.imageView.image = image
            }
            if !isDegraded {
                self?.smallImageRequestID = PHInvalidImageRequestID
            }
        })
    }
    
    override func initViews() {
        
        deletePic.addTarget(self, action: #selector(deleteAction), for: .touchUpInside)
        deletePic.isHidden = true
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(tapPicture(tap:)))
        imageView.addGestureRecognizer(tap)
        
        addSubview(imageView)
        addSubview(deletePic)
        addSubview(playImg)
        deletePic.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.right.equalToSuperview()
            make.height.width.equalTo(15.0.autoWidth())
            
        }
        imageView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        playImg.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.height.equalTo(40.autoWidth())
        }
    }
    
    @objc func deleteAction() {
        
        if let c = clickedClosure {
            c(0)
        }
    }
    
    @objc func tapPicture(tap: UITapGestureRecognizer) {
        
//            let animationView: AnimatedImageView = tap.view as! AnimatedImageView
//            if (self.delegate?.responds(to: #selector(PYTableViewCellDelegate.tableViewCell(_:tapImageAction:indexPath:))))! {
//                self.delegate?.tableViewCell(self, tapImageAction: animationView.tag, indexPath: self.cellIndexPath!)
//            }
    }
}

