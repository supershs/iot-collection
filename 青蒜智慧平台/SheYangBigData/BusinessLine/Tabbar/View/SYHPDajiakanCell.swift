//
//  SYHPDajiakanCell.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2021/3/29.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit


class SYHPDajiakanCell: SYBaseCell , UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    var models: [SYTourHotelListModel] = []
    var collectionView: UICollectionView!
    var imgUrls: [String] = []
    var sepView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = Constant.bgViewColor
        return v
    }()
    
    func configure(_ model: [SYTourHotelListModel], _ imgUrls: [String], _ height: Int, _ layout: SGFlowLayout) {
        
        if imgUrls.count == 0 {
            return
        }
        self.models = model
        self.imgUrls = imgUrls
        
        var h = CGFloat(imgUrls.count / 2) * 220.autoWidth()
        h += CGFloat(imgUrls.count % 2) * 220.autoWidth()
        h += CGFloat(imgUrls.count / 2) * 10.autoWidth()
        
        setCollectionView(layout, h)
        self.collectionView.reloadData()
    }
    
    override func initViews() {
        
        
    }
    
    func setCollectionView(_ layout: SGFlowLayout, _ height: CGFloat) {
        
        collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .white
        collectionView.showsHorizontalScrollIndicator = false
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(SYHPDajiakanSubCell.self, forCellWithReuseIdentifier: NSStringFromClass(SYHPDajiakanSubCell.self))
        collectionView.isScrollEnabled = false
        contentView.addSubview(collectionView)
        contentView.addSubview(sepView)
        collectionView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview().offset(15)
            make.right.equalToSuperview().offset(-15)
            make.height.equalTo(height)
        }
        
        sepView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.height.equalTo(10.0.autoWidth())
            make.bottom.equalToSuperview()
            make.top.equalTo(collectionView.snp.bottom).offset(15.autoWidth())
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.imgUrls.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: SYHPDajiakanSubCell? = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(SYHPDajiakanSubCell.self), for: indexPath) as? SYHPDajiakanSubCell
        if indexPath.row == 0 {
            cell?.tipSubTitle.isHidden = false
        } else {
            cell?.tipSubTitle.isHidden = true
        }
        cell?.configure(self.models[indexPath.row], self.imgUrls[indexPath.row], indexPath.row == 0)
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
//            let vc = DYMSListViewController(params: [:])
//            self.currentVC.navigationController?.pushViewController(vc, animated: true)
        } else {
//            let vc = DYZhusuDetailViewController(self.models[indexPath.row].id ?? "")
//            self.currentVC.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}



class SYHPDajiakanSubCell: SYBaseCollectionViewCell {
    
    lazy var darkView: UIView = {
        let v: UIView = UIView()
        v.sy_gradLayerCreatWithFrame(frame: CGRect(x: 0, y:0, width: self.bounds.size.width, height: 60.autoWidth()), topC: UIColor(white: 0, alpha: 0.1), bottomC: UIColor(white: 0, alpha: 0.5))
        return v
    }()
    
    let location: UILabel = {
        let v:UILabel = UILabel()
        v.textColor = .white
        v.font = 10.autoFontSize()
        return v
    }()
    
    let title: UILabel = {
        let v:UILabel = UILabel()
        v.textColor = UIColor.white
        v.font = 14.autoFontSize()
        v.numberOfLines = 1
        return v
    }()
    
    let pic: UIImageView = {
        let v: UIImageView = UIImageView()
        v.layer.cornerRadius = 8.autoWidth()
        v.layer.masksToBounds = true
        v.clipsToBounds = true
        v.contentMode = .scaleAspectFill
        return v
    }()
    
    let locationPic: UIImageView = {
        let v: UIImageView = UIImageView()
        v.sy_name("homepage_location")
        return v
    }()
    
    let firBgImg: UIImageView = {
        let v: UIImageView = UIImageView()
        v.sy_name("hp_meishi_daka_bg")
        v.isHidden = true
        return v
    }()
    
    let firBgDotImg: UIImageView = {
        let v: UIImageView = UIImageView()
        v.sy_name("hp_meishi_daka_icon")
        v.isHidden = true
        return v
    }()
    
    let tip: UILabel = {
        let v:UILabel = UILabel()
        v.textColor = .white
        v.font = 10.autoFontSize()
        v.text = "餐饮·美食"
        return v
    }()
    
    let tipTitle: UILabel = {
        let v:UILabel = UILabel()
        v.textColor = UIColor(hex: 0x333333)
        v.font = 14.autoFontSize()
        v.text = "必打卡的美食之地"
        return v
    }()
    
    let tipSubTitle: UILabel = {
        let v:UILabel = UILabel()
        v.textColor = UIColor(hex: 0x666666)
        v.font = 11.autoFontSize()
        v.text = "美味与实惠为您呈现"
        return v
    }()
    
    
    public func configure(_ model: SYTourHotelListModel, _ imgUrl: String, _ isFirst: Bool) {
        location.text = model.address
        title.text = model.name
        pic.sy_setWithUrl(imgUrl)
        if isFirst {
            locationPic.isHidden = true
            firBgImg.isHidden = false
            firBgDotImg.isHidden = false
            tipTitle.isHidden = false
            tipTitle.isHidden = false
            tip.isHidden = false
        } else {
            locationPic.isHidden = false
            firBgImg.isHidden = true
            firBgDotImg.isHidden = true
            tipTitle.isHidden = true
            tipTitle.isHidden = true
            tip.isHidden = true
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    override func initViews() {
        
        addSubview(pic)
        pic.addSubview(self.darkView)
        addSubview(title)
        addSubview(location)
        addSubview(locationPic)
        addSubview(firBgImg)
        addSubview(firBgDotImg)
        addSubview(tipTitle)
        addSubview(tipSubTitle)
        addSubview(tip)
        
        title.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-10.autoWidth())
            make.left.equalToSuperview().offset(12.autoWidth())
            make.right.equalToSuperview().offset(-12.autoWidth())
        }
        pic.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.left.right.equalToSuperview()
        }
        
        
        darkView.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview()
            make.left.right.equalToSuperview()
            make.height.equalTo(60.autoWidth())
        }
        
        location.snp.makeConstraints { (make) in
            make.bottom.equalTo(title.snp.top).offset(-10.autoWidth())
            make.left.equalToSuperview().offset(28.autoWidth())
            make.right.equalToSuperview().offset(-10.autoWidth())
        }
        locationPic.snp.makeConstraints { (make) in
            make.bottom.equalTo(title.snp.top).offset(-10.autoWidth())
            make.left.equalToSuperview().offset(13.autoWidth())
            make.height.equalTo(9.5.autoWidth())
            make.width.equalTo(8.autoWidth())
        }
        firBgImg.snp.makeConstraints { (make) in
            make.bottom.equalTo(title.snp.top).offset(-5.autoWidth())
            make.left.equalToSuperview().offset(6.autoWidth())
            make.right.equalToSuperview().offset(-6.autoWidth())
            make.height.equalTo(70.autoWidth())
        }
        firBgDotImg.snp.makeConstraints { (make) in
            make.bottom.equalTo(title.snp.top).offset(-32.autoWidth())
            make.left.equalToSuperview().offset(15.autoWidth())
            make.height.equalTo(15.autoWidth())
            make.width.equalTo(12.autoWidth())
        }
        tip.snp.makeConstraints { (make) in
            make.bottom.equalTo(title.snp.top).offset(-60.autoWidth())
            make.left.equalToSuperview().offset(17.autoWidth())
            make.right.equalToSuperview().offset(-10.autoWidth())
            make.height.equalTo(10.autoWidth())
        }
        tipTitle.snp.makeConstraints { (make) in
            make.bottom.equalTo(title.snp.top).offset(-31.autoWidth())
            make.left.equalToSuperview().offset(35.autoWidth())
            make.right.equalToSuperview().offset(-10.autoWidth())
            make.height.equalTo(15.autoWidth())
        }
        tipSubTitle.snp.makeConstraints { (make) in
            make.bottom.equalTo(title.snp.top).offset(-14.autoWidth())
            make.left.equalToSuperview().offset(35.autoWidth())
            make.right.equalToSuperview().offset(-10.autoWidth())
            make.height.equalTo(11.autoWidth())
        }
    }
}

