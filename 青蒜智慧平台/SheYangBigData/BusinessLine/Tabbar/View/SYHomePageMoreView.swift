//
//  SYHomePageMoreView.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2021/3/23.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit

class SYHomePageMoreView: SYBaseView {

    let title: UILabel = {
        let v = UILabel()
        v.font = 16.autoMediumFontSize()
        v.textColor = UIColor(hex: 0x333333)
        return v
    }()
    
    var titleBt: UIButton = {
        let v = UIButton()
        return v
    }()
    
    let imgView: UIImageView = {
        let v = UIImageView()
        v.sy_name("more_bg")
        return v
    }()
    
    var subTitle: UIButton = {
        let v = UIButton()
        v.setTitle("植保机服务", for: .normal)
        v.setTitleColor(UIColor(hex: 0x999999), for: .normal)
        v.titleLabel?.font = 16.autoFontSize()
        v.isHidden = true
        return v
    }()
    
    var more: UIButton = {
        let v = UIButton()
        v.setTitle("更多", for: .normal)
        v.setTitleColor(UIColor(hex: 0x666666), for: .normal)
        v.titleLabel?.font = 12.autoFontSize()
        return v
    }()
    
    override func initViews() {
        
        titleBt.addAction {[weak self] in
            if let `self` = self {
                self.title.textColor = UIColor(hex: 0x333333)
                self.subTitle.setTitleColor(UIColor(hex: 0x999999), for: .normal)
                self.imgView.snp.remakeConstraints { (make) in
                    make.top.equalToSuperview().offset(22.autoWidth())
                    make.left.equalToSuperview().offset(15.autoWidth())
                    make.width.equalTo(63.autoWidth())
                    make.height.equalTo(13.autoWidth())
                }
                self.clickedClosure?(0)
            }
        }
        subTitle.addAction {[weak self] in
            if let `self` = self {
                self.title.textColor = UIColor(hex: 0x999999)
                self.subTitle.setTitleColor(UIColor(hex: 0x333333), for: .normal)
                self.imgView.snp.remakeConstraints { (make) in
                    make.top.equalToSuperview().offset(22.autoWidth())
                    make.left.equalToSuperview().offset(117.autoWidth())
                    make.width.equalTo(63.autoWidth())
                    make.height.equalTo(13.autoWidth())
                }
                self.clickedClosure?(1)
            }
        }
        
        addSubview(imgView)
        addSubview(title)
        addSubview(titleBt)
        addSubview(subTitle)
        addSubview(more)
        title.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(12.autoWidth())
            make.left.equalToSuperview().offset(15.autoWidth())
            make.bottom.equalToSuperview().offset(-17.autoWidth())
            make.height.equalTo(16.autoWidth())
        }
        titleBt.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview().offset(15.autoWidth())
            make.right.equalTo(title)
            make.height.equalTo(40.autoWidth())
        }
        subTitle.snp.makeConstraints { (make) in
            make.centerY.equalTo(title)
            make.left.equalToSuperview().offset(105.autoWidth())
            make.width.equalTo(90.autoWidth())
            make.height.equalTo(40.autoWidth())
        }
        imgView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(22.autoWidth())
            make.left.equalToSuperview().offset(15.autoWidth())
            make.width.equalTo(63.autoWidth())
            make.height.equalTo(13.autoWidth())
        }
        more.snp.makeConstraints { (make) in
            make.centerY.equalTo(title)
            make.right.equalToSuperview().offset(-7.autoWidth())
            make.width.equalTo(60.autoWidth())
            make.height.equalTo(40.autoWidth())
        }
    }
}
