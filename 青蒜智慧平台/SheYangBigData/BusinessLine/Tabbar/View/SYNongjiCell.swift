//
//  SYNongjiCell.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2021/2/1.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit

class SYNongjiCell: SYBaseCell {

    var bgView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = .white
        v.layer.cornerRadius = 5
        
//        v.layer.shadowColor = UIColor(hex: 0xB3B3B3).cgColor
//        v.layer.shadowOpacity = 0.5//阴影透明度
//        v.layer.shadowOffset = CGSize.zero///阴影偏移量,四周
//        v.layer.shadowRadius = 5//阴影半径  shadowPathWidth：阴影宽度
        return v
    }()
    
    let nongjiPic: UIButton = {
        let v = UIButton()
        v.setImage(UIImage(named: "gzt_nongji"), for: .normal)
        return v
    }()
    
    let nongji: UILabel = {
        let v = UILabel()
        
        v.font = 14.autoFontSize()
        v.textColor = .white
        return v
    }()
    
    let weixiuPic: UIButton = {
        let v = UIButton()
        v.setImage(UIImage(named: "gzt_rili"), for: .normal)
        return v
    }()
    
    let weixiu: UILabel = {
        let v = UILabel()
        v.text = "日历维护"
        v.font = 14.autoFontSize()
        v.textColor = .white
        return v
    }()
   

    override func initViews() {
        
        let roleId = UserInstance.roleId
        
        if roleId == "4" {
            nongji.text = "我的植保机机"
            nongjiPic.setImage(UIImage(named: "gzt_zhibao"), for: .normal)

        } else if roleId == "2"  {
            nongji.text = "我的农机"
          nongjiPic.setImage(UIImage(named: "gzt_nongji"), for: .normal)
        }
        nongjiPic.addAction {[weak self] in
            if let `self` = self {
                self.currentVC.sy_pushWebVC(.myNongji)
            }
        }
        weixiuPic.addAction {[weak self] in
            if let `self` = self {
                self.currentVC.sy_pushWebVC(.riliWeihu)
            }
        }
        self.backgroundColor = .clear
        contentView.addSubview(nongjiPic)
        nongjiPic.addSubview(nongji)
        weixiuPic.addSubview(weixiu)
        contentView.addSubview(weixiuPic)
        
        nongjiPic.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(15.autoWidth())
            make.top.equalToSuperview()
            make.right.equalTo(weixiuPic.snp.left).offset(-10.autoWidth())
            make.height.equalTo(80.autoWidth())
            make.bottom.equalToSuperview().offset(-10.autoWidth())
            make.width.equalTo(weixiuPic)
        }
        nongji.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(59.autoWidth())
            make.centerX.equalToSuperview()
            make.height.equalTo(13.autoWidth())
        }
        weixiuPic.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-15.autoWidth())
            make.top.equalToSuperview()
            
            make.height.equalTo(80.autoWidth())
            make.bottom.equalToSuperview().offset(-10.autoWidth())
        }
        weixiu.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(59.autoWidth())
            make.centerX.equalToSuperview()
            make.height.equalTo(13.autoWidth())
        }
        
    }
}
