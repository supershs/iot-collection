//
//  SYMyGongjuCell.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/1/18.
//  Copyright © 2021 叁拾叁. All rights reserved.
//


import UIKit
import SDCycleScrollView

class SYMyGongjuCell: SYBaseCell, UICollectionViewDelegate, UICollectionViewDataSource {

    
    var collectionView: UICollectionView!
    var moreImgs: [String] = []//["my_shangmen", "my_cetu", "my_nongyouquan", "my_bincong", "my_nongji", "my_zhibaoji", "my_renyang", "my_jingqu", "my_canyin", "my_jiudian", "my_zixun", "my_dizhi", "my_yijian", "my_women"]
    var moreTitles: [String] = []//["上门服务", "测土配方", "农友圈", "病虫害识别", "农机服务", "植保机服务", "我的认养", "旅游景区", "餐饮美食", "酒店民宿", "我的咨询", "收货地址", "意见反馈", "关于我们"]
    var moreWebUrls: [String] = []
    var funcModels: [SYFuncModel] = []
    var models: [SYMyGongjuModel] = []
    
    fileprivate var dataSource: [String] = []
    
    var bgView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = .white
        v.layer.cornerRadius = 5
        
        v.layer.shadowColor = UIColor(hex: 0xB3B3B3).cgColor
        v.layer.shadowOpacity = 0.5//阴影透明度
        v.layer.shadowOffset = CGSize.zero///阴影偏移量,四周
        v.layer.shadowRadius = 5//阴影半径  shadowPathWidth：阴影宽度
        return v
    }()
    
    func configure(_ models: [SYMyGongjuModel]) {
        self.models = models
        self.moreTitles = models.map { ($0.name ?? "") }
        self.moreImgs = models.map { (($0.imgUrl ?? "")) }
        self.moreWebUrls = models.map { (($0.url ?? "")) }
        for (i, value) in moreImgs.enumerated() {
            let model: SYFuncModel = SYFuncModel()
            model.title = moreTitles[i]
            model.img = value
            model.url = moreWebUrls[i]
            funcModels.append(model)
        }
        collectionView.reloadData()
    }
    
    override func initViews() {
        setTopView()
        
        
    }
    
    func setTopView() {
        setCollectionView()
        let title: UILabel = UILabel()
        title.text = "推荐工具"
        title.font = 15.autoMediumFontSize()
        title.textColor = UIColor(hex: 0x1D1D1D)
        
            contentView.addSubview(bgView)
        bgView.addSubview(title)
        bgView.addSubview(collectionView)
        
        bgView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(15.autoWidth())
            make.top.equalToSuperview().offset(7.5.autoWidth())
            make.right.equalToSuperview().offset(-15.autoWidth())
            make.height.equalTo(340.autoWidth())
            make.bottom.equalToSuperview().offset(-10.autoWidth())
        }
        
        collectionView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(33.5.autoWidth())
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        title.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(15.5.autoWidth())
            make.top.equalToSuperview().offset(15.5.autoWidth())
            make.height.equalTo(15.5.autoWidth())
        }
    }
    
    func setCollectionView() {
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let itemWidth = (SCREEN_WIDTH - 60.autoWidth()) / 4
        layout.itemSize = CGSize(width: itemWidth, height: 76.5)
        layout.minimumLineSpacing = 1
        layout.scrollDirection = .vertical
        collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .white
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(SYGongjuCell.self, forCellWithReuseIdentifier: NSStringFromClass(SYGongjuCell.self))
        
        collectionView.isScrollEnabled = false
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return moreImgs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: SYGongjuCell? = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(SYGongjuCell.self), for: indexPath) as? SYGongjuCell
        if let d = funcModels.objectAtIndex(index: indexPath.row) {
            cell?.configCell(d)
        }
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var title: String = ""
        var url: String = ""
        if let d = funcModels.objectAtIndex(index: indexPath.row) {
            if let u = d.url {
                url = u
            }
        }
        
        title = models.objectAtIndex(index: indexPath.row)?.name ?? ""
//        switch title {
//            
//        case "我的认养":
//            let vc = RYDingdanListViewController()
//            self.currentVC.navigationController?.pushViewController(vc, animated: true)
//        case "我的游览":
//            let vc = DYMyDingdanViewController()
//            self.currentVC.navigationController?.pushViewController(vc, animated: true)
//        case "收货地址":
//            let vc = RYDizhiViewController(type: "my")
//            self.currentVC.navigationController?.pushViewController(vc, animated: true)
//        case "我的咨询":
//            let vc = DYMyConsultationViewController(isShowSearch: true)
////            let vc = DYZixunViewController(model: zixunModel, listType: .jingqu)
//            self.currentVC.navigationController?.pushViewController(vc, animated: true)
//        
//            
////            HUDUtil.showBlackTextView(text: "暂无")
//        default:
//            self.currentVC.sy_pushWebVC(url)
//        }
    }
    
    @objc func moreAction() {
        
    }
}


//
//  SYGongnengCell.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2020/12/31.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit

class SYGongjuCell: UICollectionViewCell {
    
    fileprivate var img: UIImageView = {
        let v: UIImageView = UIImageView()
        v.contentMode = UIView.ContentMode.scaleAspectFill
        v.clipsToBounds = true
        v.backgroundColor = .white
        return v
    }()
    
    fileprivate var titleLb: UILabel = {
        let v: UILabel = UILabel()
        v.font = 12.autoFontSize()
        v.textColor = UIColor(hex: 0x2A2A2A)
        v.text = "公共品牌"
        return v
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public func configCell(_ model: SYFuncModel) {
//        let m: SYFuncModel = model as! SYFuncModel
        titleLb.text = model.title
//        img.sy_name(model.img!)
        img.sy_setWithUrl(model.img)
    }
    
    fileprivate func initViews() {
        
        contentView.addSubview(img)
        contentView.addSubview(titleLb)
        img.snp.makeConstraints { (make) in
            make.width.height.equalTo(25.0)
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(15.5)
        }
        titleLb.snp.makeConstraints { (make) in
            
            make.centerX.equalToSuperview()
            make.height.equalTo(12.0)
            make.top.equalTo(img.snp.bottom).offset(9.0)
//            make.bottom.equalToSuperview()
        }
    }
}
