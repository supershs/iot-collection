//
//  SYHPZhixunCell.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2021/3/25.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit


class SYHPZhixunCell: SYBaseCell , UITableViewDelegate, UITableViewDataSource {
    
    var tableView: UITableView!
    var models: [SYExpertForumIssueVOListModel] = []
    var count: Int = 2
    var sepView: UIView = {
        let v = UIView()
        v.backgroundColor = Constant.bgViewColor
        return v
    }()
    
    func configure(_ model: [SYExpertForumIssueVOListModel]) {
        self.models = model
        if models.count < 2 {
            count = models.count
        }
        self.tableView.reloadData()
        tableView.snp.remakeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.right.bottom.equalToSuperview()
            make.bottom.equalToSuperview().offset(-10.autoWidth())
            make.height.equalTo(CGFloat(count)*66.0.autoWidth())
        }
        
    }
    
    override func initViews() {
        
        setTableView()
        contentView.addSubview(tableView)
        contentView.addSubview(sepView)
        tableView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.right.equalToSuperview()
            make.bottom.equalToSuperview().offset(-10.autoWidth())
            make.height.equalTo(66*count.autoWidth())
        }
        sepView.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(10.autoWidth())
        }
    }
    
    func setTableView() {
        
        //创建表格视图
        self.tableView = UITableView(frame: CGRect(x: 0, y: NAV_HEIGHT, width: SCREEN_WIDTH, height: SCREEN_HEIGHT-NAV_HEIGHT-BOTTOM_SAFE_HEIGHT), style:.plain)
        self.tableView.separatorStyle = .none
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.isScrollEnabled = false
        self.tableView!.register(SYHPZixunSubCell.self, forCellReuseIdentifier: "Cell")
        self.contentView.addSubview(self.tableView!)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SYHPZixunSubCell? = tableView.dequeueReusableCell(withIdentifier: "Cell") as? SYHPZixunSubCell
        cell?.selectionStyle = .none
        cell?.configure(self.models[indexPath.row])
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.currentVC.sy_pushWebVC(.wendaDetail(id: self.models[indexPath.row].id ?? ""))
    }
    
}


class SYHPZixunSubCell: SYBaseCell {
    
    let wenLb: UILabel = {
        let v:UILabel = UILabel()
        v.textColor = UIColor(hex: 0x333333)
        v.font = 14.autoFontSize()
        return v
    }()
    
    let daLb: UILabel = {
        let v:UILabel = UILabel()
        v.textColor = UIColor(hex: 0x666666)
        v.font = 14.autoFontSize()
        v.numberOfLines = 1
        return v
    }()
    
    let pic: UIImageView = {
        let v: UIImageView = UIImageView()
        v.sy_name("homepage_zhixun_wen")
        return v
    }()
    
    let daPic: UIImageView = {
        let v: UIImageView = UIImageView()
        v.sy_name("homepage_zhixun_da")
        return v
    }()
    
    
    public func configure(_ model: SYExpertForumIssueVOListModel) {
        wenLb.text = model.issue
        if let b = model.forumReplyVO?.reply {
            let bai = b.replacingOccurrences(of: "\n", with: "")
            daLb.text = bai
            
        }
    }
    
    override func initViews() {
        
        addSubview(wenLb)
        addSubview(pic)
        addSubview(daLb)
        addSubview(daPic)
        wenLb.snp.makeConstraints { (make) in
            make.centerY.equalTo(pic)
            make.left.equalTo(pic.snp.right).offset(autoWidth(7.5))
            make.right.equalToSuperview().offset(autoWidth(-15.0))
            make.height.equalTo(15.0.autoWidth())
        }
        pic.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview().offset(autoWidth(15))
            make.width.equalTo(15.autoWidth())
            make.height.equalTo(15.autoWidth())
        }
        daLb.snp.makeConstraints { (make) in
            make.centerY.equalTo(daPic)
            make.left.equalTo(daPic.snp.right).offset(autoWidth(7.5))
            make.right.equalToSuperview().offset(autoWidth(-15.0))
            make.height.equalTo(15.0.autoWidth())
        }
        daPic.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(autoWidth(27))
            make.left.equalToSuperview().offset(autoWidth(15))
            make.width.equalTo(15.autoWidth())
            make.height.equalTo(15.autoWidth())
            make.bottom.equalToSuperview().offset(-23.autoWidth())
        }
    }
}

