//
//  SYGongnengCell.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2020/12/31.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit

class SYGongnengCell: UICollectionViewCell {
    
    fileprivate var img: UIImageView = {
        let v: UIImageView = UIImageView()
        v.contentMode = UIView.ContentMode.scaleAspectFill
        v.clipsToBounds = true
        v.backgroundColor = .white
        return v
    }()
    
    fileprivate var titleLb: UILabel = {
        let v: UILabel = UILabel()
        v.font = 12.autoFontSize()
        v.textColor = UIColor(hex: 0x2A2A2A)
        
        return v
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public func configCell(_ model: SYFuncModel) {

        titleLb.text = model.title
        img.sy_setWithUrl(model.img)
    }
    
    fileprivate func initViews() {
        
        addSubview(img)
        addSubview(titleLb)
        img.snp.makeConstraints { (make) in
            make.width.height.equalTo(44.0)
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(8)
        }
        titleLb.snp.makeConstraints { (make) in
            
            make.centerX.equalToSuperview()
            make.height.equalTo(12.0)
            make.top.equalTo(img.snp.bottom).offset(7)
            make.bottom.equalToSuperview()
        }
    }
}
