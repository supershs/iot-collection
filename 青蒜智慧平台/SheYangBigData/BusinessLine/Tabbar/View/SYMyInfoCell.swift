//
//  SYMyInfoCell.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/1/18.
//  Copyright © 2021 叁拾叁. All rights reserved.
//



import UIKit

class SYMyInfoCell: SYBaseCell {

    var imgBgView: UIImageView = {
        let v: UIImageView = UIImageView()
        v.sy_name("my_top_bg")
        v.isUserInteractionEnabled = true
        return v
    }()
    
    var bgView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = .white
        v.layer.cornerRadius = 5
        v.layer.masksToBounds = true
        return v
    }()
    
    var bianjiBgView: UIImageView = {
        let v: UIImageView = UIImageView()
        v.sy_name("my_edit_bg")
        v.isUserInteractionEnabled = true
        return v
    }()
    
    let head: UIImageView = {
        let v: UIImageView = UIImageView()
        v.layer.cornerRadius = 32.autoWidth()
        v.layer.masksToBounds = true
        v.isUserInteractionEnabled = true
        return v
    }()
    
    var aboveHeadBtn: UIButton = {
        let v: UIButton = UIButton()
        return v
    }()
    

    let nameLb: UILabel = {
        let v:UILabel = UILabel()
        v.textColor = .white
        v.font = 18.autoMediumFontSize()
        return v
    }()
    
    let phoneLb: UILabel = {
        let v:UILabel = UILabel()
        v.textColor = .white
        v.font = 13.autoFontSize()
        return v
    }()
    
//    let jifenNum: UILabel = {
//        let v:UILabel = UILabel()
//        v.textColor = UIColor(hex: 0x414A5D)
//        v.font = 15.autoMediumFontSize()
//        v.text = "0"
//        v.textAlignment = .center
//        return v
//    }()
//
//    let jifen: UILabel = {
//        let v:UILabel = UILabel()
//        v.textColor = UIColor(hex: 0x414A5D)
//        v.font = 13.autoFontSize()
//        v.text = "我的积分"
//        v.textAlignment = .center
//        return v
//    }()
//
//    let quanNum: UILabel = {
//        let v:UILabel = UILabel()
//        v.textColor = UIColor(hex: 0x414A5D)
//        v.font = 15.autoMediumFontSize()
//        v.text = "0"
//        v.textAlignment = .center
//        return v
//    }()
//
//    let quan: UILabel = {
//        let v:UILabel = UILabel()
//        v.textColor = UIColor(hex: 0x414A5D)
//        v.font = 13.autoFontSize()
//        v.text = "优惠券"
//        v.textAlignment = .center
//        return v
//    }()
//
//    let qianbaoNum: UILabel = {
//        let v:UILabel = UILabel()
//        v.textColor = UIColor(hex: 0x414A5D)
//        v.font = 15.autoMediumFontSize()
//        v.text = "0"
//        v.textAlignment = .center
//        return v
//    }()
//
//    var quanbtn: UIButton = {
//        let v = UIButton()
//        return v
//    }()
//    var jifenBtn: UIButton = {
//        let v = UIButton()
//        return v
//    }()
//
//    var qianBtn: UIButton = {
//        let v = UIButton()
//        return v
//    }()
//
//    let qianbao: UILabel = {
//        let v:UILabel = UILabel()
//        v.textColor = UIColor(hex: 0x414A5D)
//        v.font = 13.autoFontSize()
//        v.text = "我的钱包"
//        v.textAlignment = .center
//        return v
//    }()
	
	lazy var bianjiBtn: VGBaseTitleImgView = {
		let v: VGBaseTitleImgView = VGBaseTitleImgView(frame: CGRect.zero, type:.imgTop)
		v.textLb.text = "我的发布"
		v.imageV.sy_name("my_release")
		v.tapClosure = {[weak self] (index, isSelect) in
			if let weakSelf = self {
				weakSelf.currentVC.sy_pushWebVC(.myRelease)
			}
		}
		return v
	}()
	
	lazy var shanchuBtn: VGBaseTitleImgView = {
		let v: VGBaseTitleImgView = VGBaseTitleImgView(frame: CGRect.zero, type:.imgTop)
		v.textLb.text = "我的关注"
		v.imageV.sy_name("my_fork")
		v.tapClosure = {[weak self] (index, isSelect) in
			if let weakSelf = self {
				weakSelf.currentVC.sy_pushWebVC(.myFollow)
			}
		}
		return v
	}()
	
	lazy var wenda: VGBaseTitleImgView = {
		let v: VGBaseTitleImgView = VGBaseTitleImgView(frame: CGRect.zero, type:.imgTop)
		v.textLb.text = "我的问答"
		v.imageV.sy_name("my_wenda")
		v.tapClosure = {[weak self] (index, isSelect) in
			if let weakSelf = self {
				weakSelf.currentVC.sy_pushWebVC(.myQuestion)
			}
		}
		return v
	}()
   

    let bianjiPic: UIImageView = {
        let v: UIImageView = UIImageView()
        v.sy_name("my_edit")
        return v
    }()
    
    let bianjiArrowPic: UIImageView = {
        let v: UIImageView = UIImageView()
        v.sy_name("my_arrow")
        return v
    }()
    
    let bianji: UILabel = {
        let v:UILabel = UILabel()
        v.textColor = .white
        v.font = 12.autoMediumFontSize()
        v.text = "编辑资料"
        return v
    }()
    
    let userKind: UILabel = {
        let v:UILabel = UILabel()
        v.textColor = .white
        v.font = 12.autoFontSize()
        v.text = "普通用户"
        v.layer.cornerRadius = 9.autoWidth()
        v.layer.masksToBounds = true
        v.textAlignment = .center
        v.backgroundColor = UIColor(white: 1, alpha: 0.1)
        return v
    }()
    
    public var firLineView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = UIColor(hex: 0xA7ADB7)
        return v
    }()
    
    public var secLineView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = UIColor(hex: 0xA7ADB7)
        return v
    }()
    
//
//    func configureHead(_ model:HeadMyData) {
//
//        jifenNum.text = model.integral ?? "0"
//        qianbaoNum.text = model.money ?? "0"
//        quanNum.text = model.coupons ?? "0"
//
//    }
//
    func configure(_ model:SYMyInfoModel) {
        nameLb.text = model.nickName
        phoneLb.text = model.mobile
        head.sy_setWithUrl(model.headImg)
		userKind.textAlignment = .center
		userKind.text = String(format: " %@ ",model.roleName ?? "普通用户")

//		userKind.text =
//        quanbtn.addAction {
//            [weak self] in
//            if let `self` = self {
//                let vc = RYyouhuiquanViewController()
//                self.currentVC.sy_push(vc, true)
//            }
//        }
        
//        jifenBtn.addAction {
//            [weak self] in
//            if let weakSelf = self {
//                weakSelf.currentVC.sy_pushWebVC(.myintegral)
//            }
//        }
//        qianBtn.addAction {
//            [weak self] in
//            if let weakSelf = self {
//                weakSelf.currentVC.sy_pushWebVC(.mywallet)
//            }
//        }
    }
    

    override func initViews() {
        
        aboveHeadBtn.addAction {[weak self] in
            if let `self` = self {
                self.currentVC.sy_pushWebVC(.userInfo)
            }
        }
        self.backgroundColor = .clear
        contentView.addSubview(imgBgView)
        contentView.addSubview(bgView)
        imgBgView.addSubview(head)
        imgBgView.addSubview(aboveHeadBtn)
        imgBgView.addSubview(nameLb)
        imgBgView.addSubview(phoneLb)
        imgBgView.addSubview(userKind)
        imgBgView.addSubview(bianjiBgView)
        bianjiBgView.addSubview(bianjiPic)
        bianjiBgView.addSubview(bianjiArrowPic)
        bianjiBgView.addSubview(bianji)
//        bgView.addSubview(jifen)
//        bgView.addSubview(jifenNum)
//        bgView.addSubview(jifenBtn)
//        bgView.addSubview(quan)
//        bgView.addSubview(quanNum)
//        bgView.addSubview(quanbtn)
//        bgView.addSubview(qianbao)
//        bgView.addSubview(qianBtn)
//        bgView.addSubview(qianbaoNum)
		
		bgView.addSubview(bianjiBtn)
		bgView.addSubview(shanchuBtn)
		bgView.addSubview(wenda)
        bgView.addSubview(firLineView)
        bgView.addSubview(secLineView)
        
        imgBgView.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.top.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(190.autoWidth()+STATUSBAR_HEIGHT)
        }
        bgView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(15.autoWidth())
            make.top.equalToSuperview().offset(144.autoWidth()+STATUSBAR_HEIGHT)
            make.right.equalToSuperview().offset(-15.autoWidth())
            make.height.equalTo(110.autoWidth())
            make.bottom.equalToSuperview()
        }
        head.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(15.autoWidth())
            make.top.equalToSuperview().offset(64.autoWidth()+STATUSBAR_HEIGHT)
            make.width.height.equalTo(64.autoWidth())
        }
        aboveHeadBtn.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalToSuperview().offset(64.autoWidth()+STATUSBAR_HEIGHT)
            make.height.equalTo(64.autoWidth())
        }
        nameLb.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(88.autoWidth())
            make.top.equalToSuperview().offset(76.autoWidth()+STATUSBAR_HEIGHT)
            make.height.equalTo(18.autoWidth())
        }
        phoneLb.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(88.autoWidth())
            make.top.equalToSuperview().offset(104.autoWidth()+STATUSBAR_HEIGHT)
            make.height.equalTo(18.autoWidth())
        }
        userKind.snp.makeConstraints { (make) in
			make.left.equalTo(nameLb.snp.right).offset(10.autoWidth())
            make.top.equalToSuperview().offset(76.autoWidth()+STATUSBAR_HEIGHT)
            make.height.equalTo(18.autoWidth())
//            make.width.equalTo(64.autoWidth())
			make.right.lessThanOrEqualToSuperview().offset(-5.autoWidth())
        }
        
        bianjiBgView.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-15.autoWidth())
			make.top.equalTo(nameLb.snp.bottom).offset(5.autoWidth())
            make.width.equalTo(100.autoWidth())
            make.height.equalTo(23.autoWidth())
        }
        bianjiPic.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(12.autoWidth())
            make.width.equalTo(14.autoWidth())
            make.height.equalTo(15.autoWidth())
        }
        bianjiArrowPic.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-10.autoWidth())
            make.width.equalTo(6.autoWidth())
            make.height.equalTo(10.5.autoWidth())
        }
        bianji.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(32.autoWidth())
            make.height.equalTo(15.autoWidth())
        }
        
//        jifenNum.snp.makeConstraints { (make) in
//            make.left.equalToSuperview()
//            make.top.equalToSuperview().offset(40.autoWidth())
//            make.height.equalTo(12.autoWidth())
//            make.width.equalTo(quanNum)
//        }
//        jifen.snp.makeConstraints { (make) in
//            make.left.equalToSuperview()
//            make.top.equalToSuperview().offset(63.autoWidth())
//            make.height.equalTo(14.autoWidth())
//            make.width.equalTo(quanNum)
//        }
//        jifenBtn.snp.makeConstraints { (make) in
//
//            make.left.equalToSuperview()
//            make.top.equalToSuperview()
//            make.width.equalTo(jifenNum)
//            make.bottom.equalToSuperview()
//        }
//        quanNum.snp.makeConstraints { (make) in
//            make.left.equalTo(jifenNum.snp.right)
//            make.right.equalTo(qianbaoNum.snp.left)
//            make.centerX.equalToSuperview()
//            make.top.equalToSuperview().offset(40.autoWidth())
//            make.height.equalTo(12.autoWidth())
//            make.width.equalTo(qianbaoNum)
//        }
//        quan.snp.makeConstraints { (make) in
//            make.centerX.equalToSuperview()
//            make.top.equalToSuperview().offset(63.autoWidth())
//            make.height.equalTo(14.autoWidth())
//            make.width.equalTo(qianbaoNum)
//        }
//        quanbtn.snp.makeConstraints { (make) in
//
//            make.top.equalToSuperview()
//            make.width.equalTo(quanNum)
//            make.bottom.equalToSuperview()
//            make.centerX.equalToSuperview()
//        }
//        qianbaoNum.snp.makeConstraints { (make) in
//            make.right.equalToSuperview()
//            make.top.equalToSuperview().offset(40.autoWidth())
//            make.height.equalTo(12.autoWidth())
//            make.width.equalTo(quanNum)
//        }
//
//        qianbao.snp.makeConstraints { (make) in
//            make.right.equalToSuperview()
//            make.top.equalToSuperview().offset(63.autoWidth())
//            make.height.equalTo(14.autoWidth())
//            make.width.equalTo(quanNum)
//        }
//        qianBtn.snp.makeConstraints { (make) in
//
//            make.right.equalToSuperview()
//            make.top.equalToSuperview()
//            make.width.equalTo(qianbaoNum)
//            make.bottom.equalToSuperview()
//        }
		
		bianjiBtn.snp.makeConstraints { (make) in
//			make.top.equalToSuperview().offset(0.autoWidth())
			make.centerX.equalToSuperview().offset(-110.autoWidth())
			make.centerY.equalToSuperview()
			make.width.height.equalTo(62.autoWidth())
		}
		shanchuBtn.snp.makeConstraints { (make) in
			make.centerX.equalToSuperview()
			make.centerY.equalToSuperview()
			make.width.height.equalTo(62.autoWidth())
		}
		wenda.snp.makeConstraints { (make) in
			make.centerX.equalToSuperview().offset(110.autoWidth())
			make.centerY.equalToSuperview()
			make.width.height.equalTo(62.autoWidth())
		}
//        firLineView.snp.makeConstraints { (make) in
//            make.left.equalTo(bianjiBtn.snp.right).offset(10)
//            make.top.equalToSuperview().offset(49.autoWidth())
//            make.height.equalTo(18.autoWidth())
//            make.width.equalTo(0.5)
//        }
//        secLineView.snp.makeConstraints { (make) in
//            make.left.equalTo(shanchuBtn.snp.right).offset(1)
//            make.top.equalToSuperview().offset(49.autoWidth())
//            make.height.equalTo(18.autoWidth())
//            make.width.equalTo(0.5)
//        }
    }
}
