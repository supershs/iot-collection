//
//  SYMyCaozuoCell.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/1/18.
//  Copyright © 2021 叁拾叁. All rights reserved.
//


import UIKit

class SYMyCaozuoCell: SYBaseCell {

    var bgView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = .white
        v.layer.cornerRadius = 5
        
//        v.layer.shadowColor = UIColor(hex: 0xB3B3B3).cgColor
//        v.layer.shadowOpacity = 0.5//阴影透明度
//        v.layer.shadowOffset = CGSize.zero///阴影偏移量,四周
//        v.layer.shadowRadius = 5//阴影半径  shadowPathWidth：阴影宽度
        return v
    }()
    
    lazy var bianjiBtn: VGBaseTitleImgView = {
        let v: VGBaseTitleImgView = VGBaseTitleImgView(frame: CGRect.zero, type:.imgTop)
        v.textLb.text = "我的发布"
        v.imageV.sy_name("my_release")
        v.tapClosure = {[weak self] (index, isSelect) in
            if let weakSelf = self {
                weakSelf.currentVC.sy_pushWebVC(.myRelease)
            }
        }
        return v
    }()
    
    lazy var shanchuBtn: VGBaseTitleImgView = {
        let v: VGBaseTitleImgView = VGBaseTitleImgView(frame: CGRect.zero, type:.imgTop)
        v.textLb.text = "我的关注"
        v.imageV.sy_name("my_fork")
        v.tapClosure = {[weak self] (index, isSelect) in
            if let weakSelf = self {
                weakSelf.currentVC.sy_pushWebVC(.myFollow)
            }
        }
        return v
    }()
    
    lazy var wenda: VGBaseTitleImgView = {
        let v: VGBaseTitleImgView = VGBaseTitleImgView(frame: CGRect.zero, type:.imgTop)
        v.textLb.text = "我的问答"
        v.imageV.sy_name("my_wenda")
        v.tapClosure = {[weak self] (index, isSelect) in
            if let weakSelf = self {
                weakSelf.currentVC.sy_pushWebVC(.myQuestion)
            }
        }
        return v
    }()
   

    override func initViews() {
        
        self.backgroundColor = .clear
        contentView.addSubview(bgView)
        
        bgView.addSubview(bianjiBtn)
        bgView.addSubview(shanchuBtn)
        bgView.addSubview(wenda)
        
        bgView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(15.autoWidth())
            make.top.equalToSuperview().offset(10.autoWidth())
            make.right.equalToSuperview().offset(-15.autoWidth())
            make.height.equalTo(89.autoWidth())
            make.bottom.equalToSuperview()
        }
        bianjiBtn.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview().offset(-110.autoWidth())
            make.centerY.equalToSuperview()
            make.width.height.equalTo(62.autoWidth())
        }
        shanchuBtn.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
            make.width.height.equalTo(62.autoWidth())
        }
        wenda.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview().offset(110.autoWidth())
            make.centerY.equalToSuperview()
            make.width.height.equalTo(62.autoWidth())
        }
    }
}
