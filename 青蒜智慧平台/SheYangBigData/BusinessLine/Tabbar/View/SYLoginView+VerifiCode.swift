//
//  SYLoginView+VerifiCode.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2020/8/21.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit

extension SYLoginView {
    
    
    func verifiCodeViews() {
        
        let getVerifiCodeBtn: UIButton = {
            let bt = UIButton()
            bt.titleLabel?.font = UIFont.systemFont(ofSize: 13)
            bt.setTitle("获取验证码", for: .normal)
            bt.setTitleColor(UIColor(hex: 0xE3FFB2), for: .normal)
            return bt
        }()
        
        addSubview(getVerifiCodeBtn)
        getVerifiCodeBtn.snp.makeConstraints { (make) in
            make.centerY.equalTo(passwordImgView)
            make.height.equalTo(30)
            make.right.equalTo(passwordTF)
            make.width.equalTo(75)
        }
        
        getVerifiCodeBtn.rx.tap
            .subscribe(onNext: { [weak self] in
                let vc = self?.viewController as! SYVerifiCodeLoginViewController
                vc.getVerifiCode()
            })
            .disposed(by: disposBag)
        
        usernameTF.placeholder = "请输入手机号码"
        passwordTF.placeholder = "请输入验证码"
        
    }
}
