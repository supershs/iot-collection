//
//  SYDaibanCell.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2021/2/1.
//  Copyright © 2021 叁拾叁. All rights reserved.
//


import UIKit

class SYDaibanCell: SYBaseCell, UITableViewDelegate, UITableViewDataSource {
    
    var models: [SYGZTDaibanListModel] = [] {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    var tableView: UITableView!
    var bgView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = .white
        v.layer.cornerRadius = 5
        v.layer.masksToBounds = true
        return v
    }()

    
    
    let nameLb: UILabel = {
        let v:UILabel = UILabel()
        v.textColor = UIColor(hex: 0x333333)
        v.font = 15.autoMediumFontSize()
        v.text = "我的待办"
        return v
    }()
    
    func configure(_ models: [SYGZTDaibanListModel]) {
        self.models = models
        tableView.snp.remakeConstraints { (make) in
            make.top.equalToSuperview().offset(40.autoWidth())
            make.bottom.equalToSuperview().offset(-10.autoWidth())
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(67.autoWidth()*CGFloat(self.models.count))
        }
    }


    override func initViews() {
        
      
        setTableView()
        self.backgroundColor = .clear
        addSubview(bgView)
        bgView.addSubview(nameLb)
        bgView.addSubview(tableView)
        
        bgView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
            make.left.equalToSuperview().offset(15.autoWidth())
            make.right.equalToSuperview().offset(-15.autoWidth())
        }
        nameLb.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(autoWidth(15.0))
            make.left.equalToSuperview().offset(autoWidth(10.0))
            make.right.equalToSuperview().offset(autoWidth(-15.0))
            make.height.equalTo(16.0.autoWidth())
        }
        
        tableView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(40.autoWidth())
            make.bottom.equalToSuperview().offset(-10.autoWidth())
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(66.autoWidth()*10)
        }
    }
    
    func setTableView() {
        
        //创建表格视图
        self.tableView = UITableView(frame: CGRect(x: 0, y: NAV_HEIGHT, width: SCREEN_WIDTH, height: SCREEN_HEIGHT-NAV_HEIGHT-BOTTOM_SAFE_HEIGHT), style:.plain)
        self.tableView.separatorStyle = .none
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.isScrollEnabled = false
        self.tableView!.register(SYDaibanSubCell.self, forCellReuseIdentifier: "Cell")
        self.contentView.addSubview(self.tableView!)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SYDaibanSubCell? = tableView.dequeueReusableCell(withIdentifier: "Cell") as? SYDaibanSubCell
        cell?.selectionStyle = .none
        if models.count > indexPath.row {
            cell?.configure(models[indexPath.row])
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.currentVC.sy_pushWebVC(models[indexPath.row].getOrderState().2)
    }
}



class SYDaibanSubCell: SYBaseCell  {
    
    
    let chepai: UILabel = {
        let v:UILabel = UILabel()
        v.textColor = UIColor(hex: 0x555555)
        v.font = 14.autoFontSize()
        
        return v
    }()
    
    let yuyue: UILabel = {
        let v:UILabel = UILabel()
        v.textColor = UIColor(hex: 0x999999)
        v.font = 13.autoFontSize()
        v.text = "预约时间: "
        return v
    }()
    
    let yuyueTime: UILabel = {
        let v:UILabel = UILabel()
        v.textColor = UIColor(hex: 0x666666)
        v.textAlignment = .left
        v.font = 13.autoFontSize()
        
        return v
    }()
    
    let state: UILabel = {
        let v:UILabel = UILabel()
        v.textColor = UIColor(hex: 0x333333)
        v.font = 13.autoFontSize()
        
        return v
    }()
    
    let imgView: UIImageView = {
        let v = UIImageView()
        v.sy_name("arrow")
        return v
    }()
    
    func configure(_ model: SYGZTDaibanListModel) {
        chepai.text = model.machineName
        yuyueTime.text = model.time
        state.text = model.getOrderState().0
        state.textColor = model.getOrderState().1
    }
    
    override func initViews() {
        
        contentView.addSubview(lineView)
        contentView.addSubview(chepai)
        contentView.addSubview(yuyue)
        contentView.addSubview(yuyueTime)
        contentView.addSubview(state)
        contentView.addSubview(imgView)
        
    
        lineView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(autoWidth(10.0))
            make.right.equalToSuperview().offset(autoWidth(-10.0))
            make.height.equalTo(0.5.autoWidth())
            make.top.equalToSuperview()
        }
        chepai.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(autoWidth(10.0))
            make.height.equalTo(13.0.autoWidth())
            make.top.equalToSuperview().offset(14.autoWidth())
        }
        yuyue.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(autoWidth(10.0))
            make.height.equalTo(13.0.autoWidth())
            make.top.equalToSuperview().offset(40.autoWidth())
            make.width.equalTo(56.autoWidth())
        }
        state.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(autoWidth(-30.0))
            make.height.equalTo(14.0.autoWidth())
            make.centerY.equalToSuperview()
        }
        
        yuyueTime.snp.makeConstraints { (make) in
            make.left.equalTo(yuyue.snp.right).offset(5.autoWidth())
            make.height.equalTo(13.0.autoWidth())
            make.top.equalToSuperview().offset(40.autoWidth())
            make.right.equalTo(state.snp.left).offset(-5.autoWidth())
            make.bottom.equalToSuperview().offset(-14.autoWidth())
        }
        
        imgView.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(autoWidth(-13.0))
            make.height.equalTo(14.0.autoWidth())
            make.width.equalTo(8.0.autoWidth())
//            make.top.equalToSuperview().offset(28.autoWidth())
            make.centerY.equalToSuperview()
        }
    
    }

   
}

