//
//  SYShouyeCell.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2021/2/1.
//  Copyright © 2021 叁拾叁. All rights reserved.
//



import UIKit

class SYShouyeCell: SYBaseCell {

    var bgView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = .white
        v.layer.cornerRadius = 5
        
//        v.layer.shadowColor = UIColor(hex: 0xB3B3B3).cgColor
//        v.layer.shadowOpacity = 0.5//阴影透明度
//        v.layer.shadowOffset = CGSize.zero///阴影偏移量,四周
//        v.layer.shadowRadius = 5//阴影半径  shadowPathWidth：阴影宽度
        return v
    }()
   
    
    lazy var chakan: VGBaseTitleImgView = {
        let v: VGBaseTitleImgView = VGBaseTitleImgView(frame: CGRect.zero, type:.imgRight)
        v.textLb.text = "查看详情 "
        v.textLb.font = 13.autoFontSize()
        v.textLb.textColor = UIColor(hex: 0x999999)
        v.imageV.sy_name("arrow")
        v.tapClosure = {[weak self] (index, isSelect) in
            if let weakSelf = self {
                weakSelf.clickedClosure?(0)
            }
        }
        return v
    }()
    
    let shouyi: UILabel = {
        let v = UILabel()
        v.text = "我的收益"
        v.font = 15.autoFontSize()
        v.textColor = UIColor(hex: 0x333333)
        return v
    }()
    
    let tixian: UILabel = {
        let v = UILabel()
        v.text = "可提现(元)"
        v.font = 14.autoFontSize()
        v.textColor = UIColor(hex: 0x777777)
        return v
    }()
    
    let jine: UILabel = {
        let v = UILabel()
        v.text = "800.00"
        v.font = 14.autoFontSize()
        v.textColor = UIColor(hex: 0x333333)
        return v
    }()
    
    let jinri: UILabel = {
        let v = UILabel()
        v.text = "100"
        v.font = 20.autoFontSize()
        v.textColor = UIColor(hex: 0x367FF6)
        return v
    }()
    
    let jinriT: UILabel = {
        let v = UILabel()
        v.text = "今日"
        v.font = 15.autoFontSize()
        v.textColor = UIColor(hex: 0x333333)
        return v
    }()
    
    let dangyue: UILabel = {
        let v = UILabel()
        v.text = "800.00"
        v.font = 20.autoFontSize()
        v.textColor = UIColor(hex: 0x367FF6)
        return v
    }()
    
    let dangyueT: UILabel = {
        let v = UILabel()
        v.text = "当月"
        v.font = 15.autoFontSize()
        v.textColor = UIColor(hex: 0x333333)
        return v
    }()
    
    let leiji: UILabel = {
        let v = UILabel()
        v.text = "800.00"
        v.font = 20.autoFontSize()
        v.textColor = UIColor(hex: 0x367FF6)
        return v
    }()
    let leijiT: UILabel = {
        let v = UILabel()
        v.text = "累计"
        v.font = 15.autoFontSize()
        v.textColor = UIColor(hex: 0x333333)
        return v
    }()
    
  
    var tixianBt: UIButton = {
        let v = UIButton()
        v.setTitle("立即提现", for: .normal)
        v.setTitleColor(UIColor(hex: 0x00C793), for: .normal)
        v.titleLabel?.font = 14.autoFontSize()
        v.layer.cornerRadius = 3
        v.layer.borderWidth = 1
        v.layer.borderColor = UIColor(hex: 0x00C793)?.cgColor
        return v
    }()
    
    func configure(_ model: SYGongzuotaiModel) {
        if let r = model.myIncome {
            jinri.text = "\(r.todayMoney ?? "")"
            dangyue.text = "\(r.monthMoney ?? "")"
            leiji.text = "\(r.totalMoney ?? "")"
            jine.text = "\(r.ktMoney ?? "")"
            jine.text = "\(r.ktMoney ?? "")"
        }
    }

    override func initViews() {
        
        self.backgroundColor = .clear
        addSubview(bgView)
        
        bgView.addSubview(shouyi)
        bgView.addSubview(chakan)
        bgView.addSubview(tixian)
        bgView.addSubview(jine)
        bgView.addSubview(tixianBt)
        
        bgView.addSubview(jinri)
        bgView.addSubview(jinriT)
        bgView.addSubview(dangyue)
        bgView.addSubview(dangyueT)
        bgView.addSubview(leiji)
        bgView.addSubview(leijiT)
        
        bgView.addSubview(lineView)
        
        bgView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(15.autoWidth())
            make.top.equalToSuperview()
            make.right.equalToSuperview().offset(-15.autoWidth())
            make.height.equalTo(156.autoWidth())
            make.bottom.equalToSuperview().offset(-10.autoWidth())
        }
        lineView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10.autoWidth())
            make.top.equalToSuperview().offset(103.autoWidth())
            make.right.equalToSuperview().offset(-10.autoWidth())
            make.height.equalTo(0.5.autoWidth())
        }
        shouyi.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(15.autoWidth())
            make.left.equalToSuperview().offset(10.autoWidth())
            make.height.equalTo(15.autoWidth())
            
        }
        chakan.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(15.autoWidth())
            make.right.equalToSuperview().offset(-10.autoWidth())
            make.height.equalTo(15.autoWidth())
        }
        tixian.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-15.autoWidth())
            make.left.equalToSuperview().offset(10.autoWidth())
            make.height.equalTo(15.autoWidth())
        }
        jine.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-15.autoWidth())
            make.left.equalToSuperview().offset(88.autoWidth())
            make.height.equalTo(15.autoWidth())
        }
        tixianBt.snp.makeConstraints { (make) in
            make.centerY.equalTo(tixian)
            make.right.equalToSuperview().offset(-10.autoWidth())
            make.height.equalTo(26.autoWidth())
            make.width.equalTo(72.autoWidth())
        }
        
        jinri.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(49.autoWidth())
            make.centerX.equalToSuperview().offset(-130.autoWidth())
            make.height.equalTo(16.autoWidth())
        }
        jinriT.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(74.autoWidth())
            make.centerX.equalToSuperview().offset(-130.autoWidth())
            make.height.equalTo(16.autoWidth())
        }
        dangyue.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(49.autoWidth())
            make.centerX.equalToSuperview()
            make.height.equalTo(16.autoWidth())
        }
        dangyueT.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(74.autoWidth())
            make.centerX.equalToSuperview()
            make.height.equalTo(16.autoWidth())
        }
        leiji.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(49.autoWidth())
            make.centerX.equalToSuperview().offset(130.autoWidth())
            make.height.equalTo(16.autoWidth())
        }
        leijiT.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(74.autoWidth())
            make.centerX.equalToSuperview().offset(130.autoWidth())
            make.height.equalTo(16.autoWidth())
        }
    }
}
