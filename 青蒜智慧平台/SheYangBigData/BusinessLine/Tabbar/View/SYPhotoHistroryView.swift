//
//  SYPhotoHistroryView.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/6/29.
//  Copyright © 2021 叁拾叁. All rights reserved.
//


import UIKit
import Kingfisher
import ZLPhotoBrowser
import AVKit
import Photos


class SYPhotoHistroryView: SYBaseView, UICollectionViewDelegate, UICollectionViewDataSource {

    var collectionView: UICollectionView!
    var photoNum: Int = 0
    var arrDataSources: [[SYRecognizeModel]] = []
    
    var bgBt: UIButton = {
        let v = UIButton()
        v.backgroundColor = .clear
        return v
    }()
    
    var closeBt: VGImageButton = {
        let v = VGImageButton()
        v.imageView.sy_name("tuku_close")
        return v
    }()
    
    var bgView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = .white
        return v
    }()
    
    var collectionBgView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = .white
        return v
    }()
    
    var titleLb: UILabel = {
        let v: UILabel = UILabel()
        v.font = 18.autoMediumFontSize()
        v.textColor = UIColor(hex: 0x111111)
        v.text = "识别历史"
        v.textAlignment = .center
        return v
    }()
    
    var tipLb: UILabel = {
        let v: UILabel = UILabel()
        v.font = 14.autoFontSize()
        v.textColor = UIColor(hex: 0xA6BADC)
        v.text = "您可以点击图片查看识别结果"
        v.textAlignment = .center
        return v
    }()
    
    func configureView(_ models: [SYRecognizeModel]) {
        
        let times = models.map { $0.updateDate?.components(separatedBy: " ").first ?? "" }
        
        times.unique.forEach { (time) in
            var tempArr: [SYRecognizeModel] = []
            models.forEach { (m) in
                if time == m.updateDate?.components(separatedBy: " ").first {
                    tempArr.append(m)
                }
                
            }
            arrDataSources.append(tempArr)
        }
        
        self.collectionView.reloadData()
    }
    
    override func layoutSubviews() {
        
        bgView.corner(byRoundingCorners: UIRectCorner(rawValue: UIRectCorner.topRight.rawValue | UIRectCorner.topLeft.rawValue), radii: 15.autoWidth())
    }
    
    override func initViews() {
        
        bgBt.addAction {[weak self] in
            if let `self` = self {
                self.clickedClosure?(0)
            }
        }
        closeBt.btn.addAction {[weak self] in
            if let `self` = self {
                self.clickedClosure?(0)
            }
        }
        
        self.backgroundColor = .clear
        setCollectionView()
        self.addSubview(bgBt)
        addSubview(bgView)
        addSubview(titleLb)
        addSubview(closeBt)
        addSubview(collectionBgView)
        self.addSubview(collectionView)
        addSubview(tipLb)
       
        bgBt.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        bgView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(108.autoWidth() + STATUSBAR_HEIGHT)
            make.right.left.equalToSuperview()
            make.height.equalTo(50.autoWidth())
        }
        titleLb.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(108.autoWidth() + STATUSBAR_HEIGHT)
            make.right.left.equalToSuperview()
            make.height.equalTo(50.autoWidth())
        }
        tipLb.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(170.autoWidth() + STATUSBAR_HEIGHT)
            make.right.left.equalToSuperview()
            make.height.equalTo(20.autoWidth())
        }
        closeBt.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(118.autoWidth() + STATUSBAR_HEIGHT)
            make.right.equalToSuperview().offset(-15.0.autoWidth())
        }
        collectionBgView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(158.autoWidth() + STATUSBAR_HEIGHT)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        collectionView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(205.autoWidth() + STATUSBAR_HEIGHT)
            make.left.equalToSuperview().offset(15.0.autoWidth())
            make.right.equalToSuperview().offset(-15.0.autoWidth())
            make.bottom.equalToSuperview()
        }
        
    }

    func setCollectionView() {
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 110.autoWidth(), height: 110.autoWidth())
        layout.minimumLineSpacing = 6.autoWidth() // 行间距
        layout.minimumInteritemSpacing = 6.autoWidth()
        layout.headerReferenceSize = CGSize(width: SCREEN_WIDTH, height: 45.autoWidth())
        layout.scrollDirection = .vertical
        collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .white
        collectionView.showsHorizontalScrollIndicator = false
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(SYImageCell.self, forCellWithReuseIdentifier: NSStringFromClass(SYImageCell.self))
        collectionView.register(SYHistoryTitleView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: NSStringFromClass(SYHistoryTitleView.self))
        self.addSubview(self.collectionView)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return arrDataSources.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrDataSources[section].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: SYImageCell? = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(SYImageCell.self), for: indexPath) as? SYImageCell
        
        let model = self.arrDataSources[indexPath.section][indexPath.row]
        cell?.configureView(model)
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let view = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: NSStringFromClass(SYHistoryTitleView.self), for: indexPath) as? SYHistoryTitleView
        if let d = self.arrDataSources[indexPath.section][indexPath.row].updateDate {
            if Date().year == Date(d)?.year {
                let components = NSCalendar.current.dateComponents([.minute], from: Date(d) ?? Date(), to: Date())
                if (components.minute ?? 0) < 5 {
                    view?.titleLb.text = "刚刚"
                } else {
                    view?.titleLb.text = Date(d)?.sy_toString(format: "MM月dd日")
                }
            } else {
                view?.titleLb.text = Date(d)?.sy_toString(format: "yyyy年MM月dd日")
            }
        }
        return view!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell:SYImageCell = collectionView.cellForItem(at: indexPath) as! SYImageCell
        self.passParamsClosure?(cell.imageView.image)
        
    }
    
  
}


class SYHistoryTitleView: UICollectionReusableView {
    
    var titleLb: UILabel = {
        let v: UILabel = UILabel()
        v.font = 14.autoMediumFontSize()
        v.textColor = UIColor(hex: 0x111111)
        return v
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initViews() {
        
        addSubview(titleLb)
        titleLb.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.left.equalToSuperview()
            make.height.equalTo(45.autoWidth())
        }
    }
}
