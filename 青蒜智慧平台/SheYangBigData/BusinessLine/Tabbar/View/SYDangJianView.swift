//
//  SYDangjianView.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2021/3/29.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit

class SYDangJianView: SYBaseView, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var collectionView: UICollectionView!
    var funcModels: [SYNewsListModel] = []
    let bgImgView: UIImageView = {
        let v = UIImageView()
        v.sy_name("hp_dangjian_bg")
        return v
    }()
    
    let imgView: UIImageView = {
        let v = UIImageView()
        v.sy_name("hp_dangjian_gold")
        return v
    }()
    
    func configure(_ models: [SYNewsListModel]) {
        funcModels = models
        self.collectionView.reloadData()
    }
    
    override func initViews() {
        self.backgroundColor = Constant.bgViewColor
        addSubview(bgImgView)
        addSubview(imgView)
        bgImgView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.right.equalToSuperview()
            make.height.equalTo(110.autoWidth())
        }
        imgView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(56.autoWidth())
            make.left.equalToSuperview().offset(15.autoWidth())
            make.right.equalToSuperview().offset(-15.autoWidth())
            make.height.equalTo(70.autoWidth())
            make.bottom.equalToSuperview()
        }
        setCollectionView()
    }
    
    func setCollectionView() {
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: (SCREEN_WIDTH - 56.autoWidth()) / 3, height: 54.autoWidth())
        layout.minimumInteritemSpacing = 6.autoWidth()
//        layout.minimumLineSpacing = 6
        layout.scrollDirection = .vertical
        collectionView = UICollectionView(frame: CGRect(x: 22.autoWidth(), y: 64.autoWidth(), width: SCREEN_WIDTH - 44.autoWidth(), height: 54.autoWidth()), collectionViewLayout: layout)
        collectionView.showsHorizontalScrollIndicator = false
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .clear
        collectionView.register(SYDangJianSubCell.self, forCellWithReuseIdentifier: NSStringFromClass(SYDangJianSubCell.self))
        self.addSubview(self.collectionView)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return funcModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: SYDangJianSubCell? = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(SYDangJianSubCell.self), for: indexPath) as? SYDangJianSubCell
        if let d = funcModels.objectAtIndex(index: indexPath.row) {
            cell?.configCell(d)
        }
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.currentVC.sy_pushWebVC(.lingdaoWuwang)
    }
}


class SYDangJianSubCell: UICollectionViewCell {
    
    fileprivate var img: UIImageView = {
        let v: UIImageView = UIImageView()
        v.sy_name("hp_dangjian_icon")
        return v
    }()
    
    fileprivate var titleLb: UILabel = {
        let v: UILabel = UILabel()
        v.font = 12.autoFontSize()
        v.textColor = UIColor(hex: 0xFF1A2F)
        v.numberOfLines = 0
        return v
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public func configCell(_ model: SYNewsListModel) {
        titleLb.text = model.title
    }
    
    fileprivate func initViews() {
        self.backgroundColor = UIColor(hex: 0xFFF5EA)
        contentView.addSubview(img)
        contentView.addSubview(titleLb)
        img.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(10.autoWidth())
            make.left.equalToSuperview().offset(7.autoWidth())
            make.width.equalTo(26.autoWidth())
            make.height.equalTo(28.autoWidth())
        }
        titleLb.snp.makeConstraints { (make) in
            
            make.top.equalToSuperview().offset(12.autoWidth())
            make.left.equalToSuperview().offset(41.autoWidth())
            make.right.equalToSuperview().offset(-7.autoWidth())
        }
    }
}



class SYNongyeZhongleiView: SYBaseView, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var collectionView: UICollectionView!
    var funcModels: [SYNewsListModel] = []
    
    
    func configure(_ models: [SYNewsListModel]) {
        funcModels = models
        self.collectionView.reloadData()
    }
    
    override func initViews() {
        self.backgroundColor = Constant.bgViewColor
        setCollectionView()
    }
    
    func setCollectionView() {
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: (SCREEN_WIDTH - 55.autoWidth()) / 3, height: 150.autoWidth())
        layout.minimumInteritemSpacing = 10.autoWidth()
//        layout.minimumLineSpacing = 6
        layout.scrollDirection = .vertical
        collectionView = UICollectionView(frame: CGRect(x: 15.autoWidth(), y: 10.autoWidth(), width: SCREEN_WIDTH - 30.autoWidth(), height: 150.autoWidth()), collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        collectionView.showsHorizontalScrollIndicator = false
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(SYNongyeZhongleiCell.self, forCellWithReuseIdentifier: NSStringFromClass(SYNongyeZhongleiCell.self))
        self.addSubview(self.collectionView)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return funcModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: SYNongyeZhongleiCell? = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(SYNongyeZhongleiCell.self), for: indexPath) as? SYNongyeZhongleiCell
        if let d = funcModels.objectAtIndex(index: indexPath.row) {
            cell?.configCell(d)
        }
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
//        let name = (funcModels[indexPath.row].groupName ?? "").getEncodeString + "&id=\(self.funcModels[indexPath.row].id ?? "")"
//        switch indexPath.row {
//        case 0:
//            self.currentVC.sy_pushWebVC(.zhihuiDatian(title: name))
//        case 1:
//            self.currentVC.sy_pushWebVC(.zhihuiDatian(title: name))
//        case 2:
//            self.currentVC.sy_pushWebVC(.zhihuiDatian(title: name))
//            
//        default:
//            break
//        }
    }
}


class SYNongyeZhongleiCell: UICollectionViewCell {
    
    fileprivate var img: UIImageView = {
        let v: UIImageView = UIImageView()
        
        return v
    }()
    
    fileprivate var titleLb: UILabel = {
        let v: UILabel = UILabel()
        v.font = 16.autoFontSize()
        v.textColor = UIColor(hex: 0x212121)
        
        return v
    }()
    
    fileprivate var contentLb: UILabel = {
        let v: UILabel = UILabel()
        v.font = 12.autoFontSize()
        v.textColor = UIColor(hex: 0x666666)
        v.numberOfLines = 0
        return v
    }()
    
    fileprivate var chakanLb: UILabel = {
        let v: UILabel = UILabel()
        v.font = 14.autoFontSize()
        v.textColor = Constant.blueColor
        v.layer.borderWidth = 0.5
        v.layer.borderColor = Constant.blueColor.cgColor
        v.text = "查看"
        v.layer.cornerRadius = 12.5.autoWidth()
        v.layer.masksToBounds = true
        v.textAlignment = .center
        return v
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public func configCell(_ model: SYNewsListModel) {
//        titleLb.text = model.groupName
//        contentLb.text = model.groupContent
        img.sy_setWithUrl(model.imgUrl)
    }
    
    fileprivate func initViews() {
        self.layer.cornerRadius = 5.autoWidth()
        self.layer.masksToBounds = true
        self.backgroundColor = .white
        contentView.addSubview(img)
        contentView.addSubview(titleLb)
        contentView.addSubview(contentLb)
        contentView.addSubview(chakanLb)
        img.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(11.autoWidth())
            make.centerX.equalToSuperview()
            make.width.equalTo(32.autoWidth())
            make.height.equalTo(32.autoWidth())
        }
        titleLb.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(50.autoWidth())
            make.centerX.equalToSuperview()
            make.height.equalTo(16.autoWidth())
        }
        contentLb.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(72.autoWidth())
            make.centerX.equalToSuperview()
            make.height.equalTo(30.autoWidth())
            make.left.equalToSuperview().offset(5.autoWidth())
            make.right.equalToSuperview().offset(-5.autoWidth())
        }
        chakanLb.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-15.autoWidth())
            make.centerX.equalToSuperview()
            make.width.equalTo(60.autoWidth())
            make.height.equalTo(25.autoWidth())
        }
    }
}
