//
//  RYNongchangListVM.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/1/18.
//  Copyright © 2021 叁拾叁. All rights reserved.
//


import Foundation
import UIKit
import RxSwift
import Moya
import Alamofire
import HandyJSON
class RYNongchangListVM: NSObject {
    
    func baseRequest<T: HandyJSON>(disposeBag: DisposeBag,type: SYApiManager, modelClass: T.Type, noHud: Bool = false, noJiance: Bool = false ,notishi: Bool = false, Success:@escaping (T)->(), Error: @escaping ()->()) {
        DispatchQueue.main.async {
            if !noHud {
                HUDUtil.showHud()
            }
        }
        UserDefaults.standard.value(forKey: type.path + UserInstance.userId!)
        ApiManagerProvider.rx.request(type)
            .asObservable()
            .mapHandyJsonModel(type.path, modelClass)
            .subscribe { (event) in
                
                switch event {
                case let .next(data):
                    let dic: Dictionary = UserDefaults.standard.value(forKey: type.path + UserInstance.userId! + REQUEST_DIC) as? Dictionary ?? [:]
                    let code: String = (dic["code"] as? String) ?? ""
                    let message: String = (dic["message"] as? String) ?? ""
                    
                    if code == "10000" {
                        DispatchQueue.main.async {
                            HUDUtil.hideHud()
                        }
                        Success(data)
                    } else {
                        DispatchQueue.main.async {
                            if code == "10001" {//"session失效！"
                                DispatchQueue.main.async {
                                    HUDUtil.hideHud()
                                }
                                if !noJiance {
                                    SYTokenCheck.toLoginVC()
                                }
                            }else if code == "30000" {
                                DispatchQueue.main.async {
                                    HUDUtil.hideHud()
                                }
                                if !noJiance {
                                    SYTokenCheck.toIdcardVC()
                                }
                            }
                            else {
                                if notishi == false {
                                    HUDUtil.showBlackTextView(text: message, detailText:"", delay: 1.5) {
                                        
                                    }
                                }
                                
                            }
                        }
                        
                        print(message)
                        Error()
                        break
                    }
                    
                    break
                case let .error(error):
                    DispatchQueue.main.async {
                        
                        HUDUtil.showBlackTextView(text: error.localizedDescription, detailText:"", delay: 1.5) {
                            
                        }
                        
                        
                    }
					print(error)
                    Error()
                    break
                default:
                    break
                }
            }.disposed(by: disposeBag)
        
    }
    
    func cancel(type: SYApiManager) {
        
    }
    
}
