//
//  RYPayManager.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/5/12.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit
import RxSwift
import HandyJSON

enum PayType: Int {
	case wechatPay = 1
	case aliPay
	case yinlianPay
}
class RYPayManager: NSObject {

    let dispose = DisposeBag()
    var requestVM: RYNongchangListVM = RYNongchangListVM()
    
    static let share = RYPayManager()
    
    func pay(dic: [String: String], payType: PayType, completeClosure: (()->())? = nil) {
        // FIXME:支付fix
        completeClosure?()
        /*
        switch payType {
        case .wechatPay:
            requestVM.baseRequest(disposeBag: dispose, type: .wechatPay(dic: dic), modelClass: SGBaseNoPageListModel<RYKuaidiModel>.self) {[weak self] (res) in
                if let `self` = self {

                }
                completeClosure?()
                
            } Error: {
                completeClosure?()
            }
            
        case .aliPay:
            requestVM.baseRequest(disposeBag: dispose, type: .aliPay(dic: dic), modelClass: SGPayModel.self) {[weak self] (res) in
                if let `self` = self {

                    if let d = res.data {
                        AliPayDelegate.alipay(d)
                    } else {
                        log.info("alipay error")
                    }
                }
                completeClosure?()
                
            } Error: {
                completeClosure?()
            }
            
        case .yinlianPay:
            requestVM.baseRequest(disposeBag: dispose, type: .yinlianPay(dic: dic), modelClass: SGBaseNoPageListModel<RYKuaidiModel>.self) {[weak self] (res) in
                if let `self` = self {
                    
                }
                completeClosure?()
            } Error: {
                completeClosure?()
            }
        }
 */
    }

    func cancelPay(id: String, adoptStatus: String, completeClosure: (()->())? = nil) {
        
        requestVM.baseRequest(disposeBag: dispose, type: .cancelPay(dic: ["id": id, "adoptStatus": adoptStatus]), modelClass: SGBaseNoPageListModel<RYKuaidiModel>.self) {[weak self] (res) in
            if let `self` = self {
//                    self.bottomView.setAlert()
            }
            
            completeClosure?()
        } Error: {
            completeClosure?()
            
        }
    }
}

struct RYKuaidiModel: HandyJSON, Equatable {
	var status: Int?
	var dictionaryName: String?
	var dictionaryDesc: String? //TODO: Specify the type to conforms Codable protocol
	var code: String?
	var parentCode: String?
	var parentName: String? //TODO: Specify the type to conforms Codable protocol
	var updateDate: String?
	var sort: Int?
	var uuid: String?
	var version: Int?
	var id: String?
	var tenantId: Int?
	var createBy: Int?
	var updateBy: Int?
	var parentId: String?
	var dictionaryLevel: Int?
	var list: String? //TODO: Specify the type to conforms Codable protocol
	var dictionaryKey: String? //TODO: Specify the type to conforms Codable protocol
	var remarks: String? //TODO: Specify the type to conforms Codable protocol
	var createDate: String?
	var dictionaryType: String? //TODO: Specify the type to conforms Codable protocol

	
}
