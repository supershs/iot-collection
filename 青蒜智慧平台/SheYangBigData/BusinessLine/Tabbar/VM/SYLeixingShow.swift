//
//  SYLeixingShow.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/1/29.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit

// swift 类型前缀实现方式

/* test
 * UIImageView().SG.setImage(imgUrl: "")
 * UIImageView.SG.imageViewTestFunc()
 */
struct SGWrapper<Base> {
    public let base: Base
    static var baseType: Base.Type {
        return Base.self
    }
    public init(_ base: Base) {
        self.base = base
    }
}

protocol SGCompatible : AnyObject {}

extension SGCompatible {
    public var SG: SGWrapper<Self> {
        return SGWrapper(self)
    }
    
    static var SG: SGWrapper<Self>.Type {
        return SGWrapper<Self>.self
    }
}

extension UIImageView: SGCompatible {}
extension UIButton: SGCompatible {}

extension SGWrapper where Base: UIImageView {
    func setImage(imgUrl: String?, placeHolder: UIImage? = nil) {
        if let urlStr = imgUrl, let url = URL(string: urlStr) {
            self.base.kf.setImage(with: url, placeholder: placeHolder, options: [.transition(.fade(0.2))])
        }
    }
    
    static func imageViewTestFunc() {
        print(baseType)
    }
}

