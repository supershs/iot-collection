//
//  SYDownloadImage.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/1/29.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit
import Kingfisher

class SYDownloadImage: NSObject, ImageDownloaderDelegate {
    
    var currentIndex: Int = 0
    var images:[UIImage] = []
    
    func storeImages() {
        
        // ImageCache，默认是
        let cache = ImageCache.default
        
        // 设置内存缓存的大小，默认是0 pixel表示no limit ，注意它是像素为单位，与我们平时的bytes不同
        //        cache.maxMemoryCost = 10 * 1024 * 1024
        
        // 磁盘缓存大小，默认0 bytes表示no limit （50 * 1024）
        //        cache.maxDiskCacheSize = 50 * 1024 * 1024
        
        // 设置缓存周期 (默认1 week）
//        cache.maxCachePeriodInSecond = 60 * 60 * 24
        
        // 存储一张图片, Key 可用于后期检索资源、删除以及在删除时的一个通知参数
        cache.store(UIImage(named:"test")!, forKey: "test")
        
        // 删除
        cache.removeImage(forKey:"test")
        
        // 检索图片
//        let imgDisk = cache.retrieveImageInDiskCache(forKey:"test")
        
        let imgMemo = cache.retrieveImageInMemoryCache(forKey:"test")
        
        // 异步检索
//        cache.retrieveImage(forKey:"test", options: nil) { (_,_) in
//
//
//
//        }
        
        // 清除
        cache.clearDiskCache()
        
        cache.clearMemoryCache()
        
        cache.clearDiskCache {
            
            
        }
        
        // 清除过期缓存
        cache.cleanExpiredDiskCache()
        
        cache.cleanExpiredDiskCache {
            
        }
        
        cache.backgroundCleanExpiredDiskCache()// 后台清理，但不需要回调
        
        // 判定图片是否存在
        let cached = cache.isCached(forKey:"test")
        
        
        // 监听数据移除
        NotificationCenter.default.addObserver(self, selector:#selector(cleanDiskCache), name: NSNotification.Name.init("KingfisherDidCleanDiskCacheNotification"), object:nil)
    }
    
    // 服务端仅支持依次下载
    func downloadImages(urls: [String], noHud: Bool, complete:@escaping ([UIImage])->()) {
        
        //==========下载图片带进度
        // 图片加载ImageDownloader使用
        
        //  从名字就可以很清楚的知道，这个类就是用来下载图片的，它为我们提供了一些头的设置（比如说你有些图片是需要认证用户才能下载的）；安全设置：我们在下载图片时哪些Host是可信任的；下载超时设置；下载回调等。
        let downloader = ImageDownloader.default
        
        // 设置可信任的Host
        //        let hosts: Set<String> = ["http://xxxxx.com","http://#####.com"]
        //
        //        downloader.trustedHosts = hosts
        
        // 设置sessionConfiguration
        downloader.sessionConfiguration = URLSessionConfiguration.default
        
        // 设置代理，详情参考 ImageDownloaderDelegate
        downloader.delegate = self
        
        // 下载超时设置
        downloader.downloadTimeout = 20
        //        let option: KingfisherOptionsInfo = [.forceRefresh]
        //        let option: KingfisherOptionsInfo = ([.originalCache(.default)])
        // 下载图片
        if !noHud {
            HUDUtil.showHud()
        }
        if let url = URL(string: IP + urls[currentIndex]) {
            
        
            let _ = downloader.downloadImage(with: url, options: nil) { (current, total) in
                print("当前下载image进度\(Float(current)/Float(total))")
            } completionHandler: { (response) in
                switch response {
                case .success(let result):
                    //下载完成
                    DispatchQueue.main.async {
                        
                        print("路径:\(result.url)")
                        self.images.append(result.image)
                        self.currentIndex += 1
                        if urls.count == self.currentIndex {
                            complete(self.images)
                        } else {
                            
                            self.downloadImages(urls: urls, noHud: noHud) { (image) in
                                
                                if urls.count == self.currentIndex {
                                    HUDUtil.hideHud()
                                    complete(self.images)
                                }
                            }
                        }
                        
                    }
                case .failure(let error):
                    print("error:\(error)")
                    if !noHud {
                        HUDUtil.showBlackTextView(text: "下载图片失败")
                    }
                    
                    DispatchQueue.main.async {
                        
                        self.images.append(UIImage(named: "homepage_zhixun_wen") ?? UIImage())
                        self.currentIndex += 1
                        if urls.count == self.currentIndex {
                            complete(self.images)
                        } else {
                            
                            self.downloadImages(urls: urls, noHud: noHud) { (image) in
                                
                                if urls.count == self.currentIndex {
                                    HUDUtil.hideHud()
                                    complete(self.images)
                                }
                            }
                        }
                        
                    }
                    break
                }
            }
        }
        
        // 取消下载
        //        retriveTask?.cancel()
        
    }
    
    @objc func cleanDiskCache() {
        
    }
    
}
