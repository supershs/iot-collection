//
//  SYUserInfo.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2020/9/2.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit
import HandyJSON

class SYUserInfo: NSObject {

    
    static var strand: SYUserInfoModel = SYUserInfoModel()
}


struct SYUserInfoModel: HandyJSON, Equatable
{
    var userId: String = "22"
    var userName: String = "范文涛"
    var userAccount: String?
    var userPhone: String = "17867817454"
}
