//
//  SYMenuListModel.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2021/3/26.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit
import HandyJSON

struct SYMenuListModel : HandyJSON, Equatable {

    /// <#泛型#>
    var createDate: String?
    /// <#泛型#>
    var version: String?
    /// <#泛型#>
    var status: String?
    /// <#泛型#>
    var uuid: String?
    /// <#泛型#>
    var tenantId: String?
    /// <#泛型#>
    var updateDate: String?
    /// <#泛型#>
    var updateBy: String?
    /// <#泛型#>
    var url: String?
    /// <#泛型#>
    var id: String?
    /// <#泛型#>
    var imgUrl: String?
    /// <#泛型#>
    var groupList: String?
    /// <#泛型#>
    var tablePublicList: String?
    /// <#泛型#>
    var sort: String?
    /// <#泛型#>
    var createBy: String?
    /// 推荐频道
    var groupStr: String?
    /// <#泛型#>
    var name: String?
    ///
    var appMenuList: [SYAppMenuFullListModel]?
    /// <#泛型#>
    var remarks: String?
    /// <#泛型#>
    var mold: String?
    /// <#泛型#>
    var roleId: String?

}


struct SYAppMenuFullListModel : HandyJSON, Equatable {

    /// 1
    var version: Int = 0
    ///
    var createDate: String?
    /// 1
    var status: Int = 0
    /// <#泛型#>
    var uuid: String?
    /// <#泛型#>
    var tenantId: String?
    ///
    var updateDate: String?
    /// app_303
    var url: String?
    /// 1
    var updateBy: String?
    /// 4
    var id: String?
    ///
    var imgUrl: String?
    /// 4
    var sort: Int = 0
    /// 1
    var createBy: String?
    /// 农友圈
    var name: String?
    /// 推荐频道
    var groupStr: String?
    /// <#泛型#>
    var remarks: String?
    /// 1
    var mold: Int = 0
    /// ALL
    var roleId: String?

 
}




//
//  NSRootModel.swift
//  SKGenerateModelTool
//
//  Created by SKGenerateModelTool on 2021/07/08.
//  Copyright © 2021 SKGenerateModelTool. All rights reserved.
//
import HandyJSON



struct NSModelDataModel : HandyJSON,Equatable  {

    /// <#泛型#>
    var remarks: String?
    /// <#泛型#>
    var tenantId: String?
    /// <#泛型#>
    var groupStr: String?
    /// <#泛型#>
    var status: String?
    /// <#泛型#>
    var url: String?
    /// <#泛型#>
    var appMenuList: String?
    /// <#泛型#>
    var createBy: String?
    /// <#泛型#>
    var uuid: String?
    /// <#泛型#>
    var version: String?
    /// <#泛型#>
    var sort: String?
    /// <#泛型#>
    var createDate: String?
    /// <#泛型#>
    var updateDate: String?
    /// <#泛型#>
    var name: String?
    /// <#泛型#>
    var id: String?
    /// <#泛型#>
    var imgUrl: String?
    /// <#泛型#>
    var defaultFast: String?
    ///
    var appMenuUser: NSAppMenuUserModel?
    /// <#泛型#>
    var updateBy: String?
    ///
    var appMenuVOList: [NSAppMenuVOListModel]?

}


struct NSAppMenuUserModel : HandyJSON,Equatable {

    /// <#泛型#>
    var updateBy: String?
    /// 15
    var menuIds: String?
    /// <#泛型#>
    var status: String?
    /// <#泛型#>
    var remarks: String?
    /// <#泛型#>
    var updateDate: String?
    /// <#泛型#>
    var id: String?
    /// <#泛型#>
    var createBy: String?
    /// <#泛型#>
    var sort: String?
    /// <#泛型#>
    var createDate: String?
    /// <#泛型#>
    var tenantId: String?
    /// <#泛型#>
    var uuid: String?
    /// <#泛型#>
    var version: String?

}


struct NSAppMenuVOListModel : HandyJSON,Equatable{

    /// <#泛型#>
    var remarks: String?
    /// <#泛型#>
    var tenantId: String?
    /// 供需服务
    var groupStr: String?
    /// <#泛型#>
    var status: String?
    /// <#泛型#>
    var url: String?
    ///
    var appMenuList: [NSAppMenuListModel]?
    /// <#泛型#>
    var createBy: String?
    /// <#泛型#>
    var uuid: String?
    /// <#泛型#>
    var version: String?
    /// <#泛型#>
    var sort: String?
    /// <#泛型#>
    var createDate: String?
    /// <#泛型#>
    var updateDate: String?
    /// <#泛型#>
    var name: String?
    /// <#泛型#>
    var id: String?
    /// <#泛型#>
    var imgUrl: String?
    /// <#泛型#>
    var defaultFast: String?
    /// <#泛型#>
    var appMenuUser: String?
    /// <#泛型#>
    var updateBy: String?
    /// <#泛型#>
    var appMenuVOList: String?

}


struct NSAppMenuListModel : HandyJSON,Equatable  {

    /// <#泛型#>
    var remarks: String?
    /// 1
    var tenantId: String?
    /// 供需服务
    var groupStr: String?
    /// 1
    var status: Int = 0
    /// <#泛型#>
    var url: String?
    /// 1
    var createBy: String?
    /// <#泛型#>
    var uuid: String?
    /// 1
    var version: Int = 0
    /// 15
    var sort: Int = 0
    ///
    var createDate: String?
    ///
    var updateDate: String?
    /// 供需服务
    var name: String?
    /// 15
    var id: String?
    ///
    var imgUrl: String?
    /// <#泛型#>
    var defaultFast: String?
    /// 1
    var updateBy: String?

}
