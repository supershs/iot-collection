//
//  SYGongzuotaiModel.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2021/2/1.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import HandyJSON



class SYGongzuotaiModel : HandyJSON {

    ///
    var orderNum: SYOrderNumModel?
    /// <#泛型#>
    var userInfo: SYGZTUserInfoModel?
    ///
    var myIncome: SYMyIncomeModel?

    required init() {}
}


class SYOrderNumModel : HandyJSON {

    /// 0
    var payCount: Int = 0
    /// 0
    var evaluateCount: Int = 0
    /// 0
    var goCount: Int = 0
    /// 0
    var confirmCount: Int = 0
    /// 0
    var serviceCount: Int = 0

    required init() {}
}


class SYMyIncomeModel : HandyJSON {

    /// 0.0
    var money: String?
    /// 0.0
    var todayMoney: String?
    /// 0.0
    var monthMoney: String?
    /// 0.0
    var totalMoney: String?
    /// 0.00
    var ktMoney: String?
    /// 0.0
    var surplusMoney: String?

    required init() {}
}


class SYGZTUserInfoModel : HandyJSON {

    ///
    var headImg: String?
    /// <#泛型#>
    var enterName: String?
    /// 农机手
    var role: String?
    /// 范文涛
    var userName: String?

    required init() {}
}


