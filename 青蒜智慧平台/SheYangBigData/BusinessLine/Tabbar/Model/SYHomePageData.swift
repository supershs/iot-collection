//
//  SYHomePageData.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2020/8/25.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit
import HandyJSON
import RxSwift

struct SYHomePageData: HandyJSON {
    
    var status: String?
    var code: Int?
    var message: String?
    var date: Date?
    var data: [ListData]?
    class ListData : HandyJSON {
        var id: String?
        var createBy: String? //TODO: Specify the type to conforms Codable protocol
        var uuid: String?
        var version: Int?
        var updateBy: String? //TODO: Specify the type to conforms Codable protocol
        var picName: String?
        var createDate: String?
        var picUrl: String? //TODO: Specify the type to conforms Codable protocol
        var bannerId: String?
        var updateDate: String? //TODO: Specify the type to conforms Codable protocol
        var picId: String?
        var picPath: String?
        var remarks: String? //TODO: Specify the type to conforms Codable protocol
        var sort: String? //TODO: Specify the type to conforms Codable protocol
        var status: Int?
        required init() {
            
        }
    }
}

class SYHomePageListData: HandyJSON {
    
    var status: String?
    var code: Int?
    var message: String?
    var date: Date?
    var data: PListData?
    class PListData: HandyJSON {
        
        var records: [Records]?
        var size: Int?
        var current: Int?
        var pages: Int?
        var total: Int?
        required init() {
            
        }
    }
    required init() {
        
    }
}
struct Records: HandyJSON, Equatable
{
    var deliveryList: String? //TODO: Specify the type to conforms Codable protocol
    var onSelf: Int?
    var surplusAmount: String? //TODO: Specify the type to conforms Codable protocol
    var spList: String? //TODO: Specify the type to conforms Codable protocol
    var remarks: String? //TODO: Specify the type to conforms Codable protocol
    var recommendFlag: Int?
    var id: String?
    var gift: String? //TODO: Specify the type to conforms Codable protocol
    var shoppingCartId: String? //TODO: Specify the type to conforms Codable protocol
    var priceRange: String? //TODO: Specify the type to conforms Codable protocol
    var updateBy: String? //TODO: Specify the type to conforms Codable protocol
    var weight: String? //TODO: Specify the type to conforms Codable protocol
    var createBy: String? //TODO: Specify the type to conforms Codable protocol
    var hotSaleFlag: Int?
    var evaluateAmount: String? //TODO: Specify the type to conforms Codable protocol
    var uuid: String? //TODO: Specify the type to conforms Codable protocol
    var addressMx: String? //TODO: Specify the type to conforms Codable protocol
    var priceFlag: Int?
    var shoppingProductClassfy: String?
    var nowTime: String? //TODO: Specify the type to conforms Codable protocol
    var compStringNo: String? //TODO: Specify the type to conforms Codable protocol
    var status: Int?
    var classId: Int?
    var fileMxIds: String? //TODO: Specify the type to conforms Codable protocol
    var shoppingGiftInformation: String? //TODO: Specify the type to conforms Codable protocol
    var shoppingProductType: String?
    var flag: String? //TODO: Specify the type to conforms Codable protocol
    var productInformationMx: String? //TODO: Specify the type to conforms Codable protocol
    var count: String? //TODO: Specify the type to conforms Codable protocol
    var address: String? //TODO: Specify the type to conforms Codable protocol
    var approvalStatus: Int?
    var createName: String? //TODO: Specify the type to conforms Codable protocol
    var shoppingFlag: String? //TODO: Specify the type to conforms Codable protocol
    var typeFlag: String? //TODO: Specify the type to conforms Codable protocol
    var newProductFlag: Int?
    var userId: String? //TODO: Specify the type to conforms Codable protocol
    var productDescriptions: String? //TODO: Specify the type to conforms Codable protocol
    var price: String? //TODO: Specify the type to conforms Codable protocol
    var modelId: String? //TODO: Specify the type to conforms Codable protocol
    var shoppingDetails: String? //TODO: Specify the type to conforms Codable protocol
    var mxList: String? //TODO: Specify the type to conforms Codable protocol
    var sort: String? //TODO: Specify the type to conforms Codable protocol
    var sendDescription: String? //TODO: Specify the type to conforms Codable protocol
    var deliveryFlag: String? //TODO: Specify the type to conforms Codable protocol
    var updateDate: String? //TODO: Specify the type to conforms Codable protocol
    var depotId: String? //TODO: Specify the type to conforms Codable protocol
    var deliveryDetails: String? //TODO: Specify the type to conforms Codable protocol
    var kind: String? //TODO: Specify the type to conforms Codable protocol
    var shoppingName: String?
    var originalPrice: String? //TODO: Specify the type to conforms Codable protocol
    var shoppingStatus: String? //TODO: Specify the type to conforms Codable protocol
    var shopDepotDescribe: String? //TODO: Specify the type to conforms Codable protocol
    var maintitle: String? //TODO: Specify the type to conforms Codable protocol
    var shoppingPrice: String?
    var createDate: String? //TODO: Specify the type to conforms Codable protocol
    var sum: String? //TODO: Specify the type to conforms Codable protocol
    var destination: String? //TODO: Specify the type to conforms Codable protocol
    var favorableRate: String? //TODO: Specify the type to conforms Codable protocol
    var merchantsName: String?
    var supplyId: String?
    var unUserAmount: String? //TODO: Specify the type to conforms Codable protocol
    var productId: String? //TODO: Specify the type to conforms Codable protocol
    var saleAmount: Int?
    var productNo: String?
    var version: String? //TODO: Specify the type to conforms Codable protocol
    var productNoList: String? //TODO: Specify the type to conforms Codable protocol
    var map: String? //TODO: Specify the type to conforms Codable protocol
    var productNumber: String? //TODO: Specify the type to conforms Codable protocol
    var endTime: String? //TODO: Specify the type to conforms Codable protocol
    var fileIds: String?
    var time: String? //TODO: Specify the type to conforms Codable protocol
    var unit: String? //TODO: Specify the type to conforms Codable protocol
    var modelName: String?
    var specifications: String? //TODO: Specify the type to conforms Codable protocol
    var shopNo: String? //TODO: Specify the type to conforms Codable protocol
    var subtitle: String? //TODO: Specify the type to conforms Codable protocol
    var brandName: String? //TODO: Specify the type to conforms Codable protocol
    var productName: String?
    var classfy: String?
    var maxShoppingPrice: String?
//    required init() {
//        
//    }
}
//struct NewModel: Codable {
//    let date: Date
//    let message: String
//    struct Data: Codable {
//        struct Records: Codable {
//            let deliveryList: Any? //TODO: Specify the type to conforms Codable protocol
//            let onSelf: Int
//            let surplusAmount: Any? //TODO: Specify the type to conforms Codable protocol
//            let spList: Any? //TODO: Specify the type to conforms Codable protocol
//            let remarks: Any? //TODO: Specify the type to conforms Codable protocol
//            let recommendFlag: Int
//            let id: String
//            let gift: Any? //TODO: Specify the type to conforms Codable protocol
//            let shoppingCartId: Any? //TODO: Specify the type to conforms Codable protocol
//            let priceRange: Any? //TODO: Specify the type to conforms Codable protocol
//            let updateBy: Any? //TODO: Specify the type to conforms Codable protocol
//            let weight: Any? //TODO: Specify the type to conforms Codable protocol
//            let createBy: Any? //TODO: Specify the type to conforms Codable protocol
//            let hotSaleFlag: Int
//            let evaluateAmount: Any? //TODO: Specify the type to conforms Codable protocol
//            let uuid: Any? //TODO: Specify the type to conforms Codable protocol
//            let addressMx: Any? //TODO: Specify the type to conforms Codable protocol
//            let priceFlag: Int
//            let shoppingProductClassfy: String
//            let nowTime: Any? //TODO: Specify the type to conforms Codable protocol
//            let companyNo: Any? //TODO: Specify the type to conforms Codable protocol
//            let status: Int
//            let classId: Int
//            let fileMxIds: Any? //TODO: Specify the type to conforms Codable protocol
//            let shoppingGiftInformation: Any? //TODO: Specify the type to conforms Codable protocol
//            let shoppingProductType: String
//            let flag: Any? //TODO: Specify the type to conforms Codable protocol
//            let productInformationMx: Any? //TODO: Specify the type to conforms Codable protocol
//            let count: Any? //TODO: Specify the type to conforms Codable protocol
//            let address: Any? //TODO: Specify the type to conforms Codable protocol
//            let approvalStatus: Int
//            let createName: Any? //TODO: Specify the type to conforms Codable protocol
//            let shoppingFlag: Any? //TODO: Specify the type to conforms Codable protocol
//            let typeFlag: Any? //TODO: Specify the type to conforms Codable protocol
//            let newProductFlag: Int
//            let userId: Any? //TODO: Specify the type to conforms Codable protocol
//            let productDescriptions: Any? //TODO: Specify the type to conforms Codable protocol
//            let price: Any? //TODO: Specify the type to conforms Codable protocol
//            let modelId: Any? //TODO: Specify the type to conforms Codable protocol
//            let shoppingDetails: Any? //TODO: Specify the type to conforms Codable protocol
//            let mxList: Any? //TODO: Specify the type to conforms Codable protocol
//            let sort: Any? //TODO: Specify the type to conforms Codable protocol
//            let sendDescription: Any? //TODO: Specify the type to conforms Codable protocol
//            let deliveryFlag: Any? //TODO: Specify the type to conforms Codable protocol
//            let updateDate: Any? //TODO: Specify the type to conforms Codable protocol
//            let depotId: Any? //TODO: Specify the type to conforms Codable protocol
//            let deliveryDetails: Any? //TODO: Specify the type to conforms Codable protocol
//            let kind: Any? //TODO: Specify the type to conforms Codable protocol
//            let shoppingName: String
//            let originalPrice: Any? //TODO: Specify the type to conforms Codable protocol
//            let shoppingStatus: Any? //TODO: Specify the type to conforms Codable protocol
//            let shopDepotDescribe: Any? //TODO: Specify the type to conforms Codable protocol
//            let maintitle: Any? //TODO: Specify the type to conforms Codable protocol
//            let shoppingPrice: String
//            let createDate: Any? //TODO: Specify the type to conforms Codable protocol
//            let sum: Any? //TODO: Specify the type to conforms Codable protocol
//            let destination: Any? //TODO: Specify the type to conforms Codable protocol
//            let favorableRate: Any? //TODO: Specify the type to conforms Codable protocol
//            let merchantsName: String
//            let supplyId: String
//            let unUserAmount: Any? //TODO: Specify the type to conforms Codable protocol
//            let productId: Any? //TODO: Specify the type to conforms Codable protocol
//            let saleAmount: Int
//            let productNo: String
//            let version: Any? //TODO: Specify the type to conforms Codable protocol
//            let productNoList: Any? //TODO: Specify the type to conforms Codable protocol
//            let map: Any? //TODO: Specify the type to conforms Codable protocol
//            let productNumber: Any? //TODO: Specify the type to conforms Codable protocol
//            let endTime: Any? //TODO: Specify the type to conforms Codable protocol
//            let fileIds: String?
//            let time: Any? //TODO: Specify the type to conforms Codable protocol
//            let unit: Any? //TODO: Specify the type to conforms Codable protocol
//            let modelName: String
//            let specifications: Any? //TODO: Specify the type to conforms Codable protocol
//            let shopNo: Any? //TODO: Specify the type to conforms Codable protocol
//            let subtitle: Any? //TODO: Specify the type to conforms Codable protocol
//            let brandName: Any? //TODO: Specify the type to conforms Codable protocol
//            let productName: String
//            let classfy: String
//            let maxShoppingPrice: String
//        }
//        let records: [Records]
//        let size: Int
//        let current: Int
//        let pages: Int
//        let total: Int
//    }
//    let data: Data
//    let status: String
//    let code: Int
//}
