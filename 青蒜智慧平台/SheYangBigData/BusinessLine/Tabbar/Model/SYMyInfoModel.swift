//
//  SYMyInfoModel.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2021/2/1.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit
import HandyJSON

struct SYMyInfoModel : HandyJSON {

    /// <#泛型#>
    var loginName: String?
    /// 钢铁侠
    var nickName: String?
    /// 钢铁侠
    var trueName: String?
    ///
    var headImg: String?
    /// <#泛型#>
    var createDate: String?
    /// <#泛型#>
    var updateBy: String?
    /// <#泛型#>
    var uuid: String?
    /// <#泛型#>
    var street: String?
    /// <#泛型#>
    var identityCard: String?
    /// <#泛型#>
    var sort: String?
    /// <#泛型#>
    var dynamicImg: String?
    /// <#泛型#>
    var tenantId: String?
    /// <#泛型#>
    var email: String?
    /// <#泛型#>
    var createBy: String?
    /// <#泛型#>
    var status: String?
    /// <#泛型#>
    var sex: String?
    /// <#泛型#>
    var id: String?
    /// <#泛型#>
    var updateDate: String?
    /// <#泛型#>
    var remarks: String?
    /// <#泛型#>
    var roleList: String?
    /// <#泛型#>
    var salt: String?
    /// <#泛型#>
    var roleId: String?
    /// <#泛型#>
    var phone: String?
    /// <#泛型#>
    var password: String?
    /// 133****6992
    var mobile: String?
    /// <#泛型#>
    var version: String?

	var roleName: String?
	
	
}
