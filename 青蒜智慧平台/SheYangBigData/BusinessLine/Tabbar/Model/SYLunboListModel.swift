//
//  SYLunboListModel.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2021/2/1.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit
import HandyJSON


struct SYLunboListModel : HandyJSON {

    /// <#泛型#>
    var number: String?
    /// <#泛型#>
    var unit: String?
    /// <#泛型#>
    var createBy: String?
    /// 271
    var parentId: Int = 0
    /// <#泛型#>
    var remarks: String?
    /// <#泛型#>
    var updateBy: String?
    /// 1
    var status: Int = 0
    /// <#泛型#>
    var taskTime: String?
    /// <#泛型#>
    var title: String?
    /// <#泛型#>
    var uuid: String?
    /// 1
    var sort: Int = 0
    /// <#泛型#>
    var tenantId: String?
    /// <#泛型#>
    var imgUrl: String?
    ///
    var strK: String?
    /// <#泛型#>
    var dictionariesId: String?
    /// <#泛型#>
    var text: String?
    /// <#泛型#>
    var createDate: String?
    /// 272
    var id: String?
    /// <#泛型#>
    var updateDate: String?
    /// <#泛型#>
    var synopsis: String?
    ///
    var dictionaryValue: String?
    /// 0
    var bfb: Int = 0
    /// 1
    var version: Int = 0

    
}
