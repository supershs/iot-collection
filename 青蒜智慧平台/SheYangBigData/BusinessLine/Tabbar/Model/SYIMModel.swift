//
//  SYIMModel.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/2/1.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit
import HandyJSON

struct SYIMListModel : HandyJSON, Equatable {

    ///
    var publishTime: String?
    /// 气象预警
    var typeName: String?
    ///
    var icon: String?
    ///
    var info: SYIMListInfoModel?
    /// 2
    var type: Int = 0
    /// WEATHER_WARN
    var moduleName: String?
    
    var route: String?

    var appUser :PYImListAppUserModel?
}


struct SYIMListInfoModel : HandyJSON, Equatable {

    /// <#泛型#>
    var uuid: String?
    /// 2
    var noReadNum: String?
    /// <#泛型#>
    var relationId: String?
    /// <#泛型#>
    var updateBy: String?
    /// <#泛型#>
    var createBy: String?
    /// <#泛型#>
    var status: String?
    /// <#泛型#>
    var infoType: String?
    /// <#泛型#>
    var id: String?
    /// <#泛型#>
    var tenantId: String?
    /// 20小时前
    var time: String?
    /// <#泛型#>
    var remarks: String?
    /// <#泛型#>
    var updateDate: String?
    /// <#泛型#>
    var sort: String?
    ///
    var message: String?
    ///
    var createDate: String?
    /// <#泛型#>
    var version: String?
    /// WEATHER_WARN
    var modules: String?
    /// <#泛型#>
    var appUserId: String?
    
    var relationInfo: String?

    var type: String?
    
    var route: String?
}



struct PYImListAppUserModel : HandyJSON, Equatable {

    /// <#泛型#>
    var mobile: String?
    /// <#泛型#>
    var updateDate: String?
    /// <#泛型#>
    var remarks: String?
    /// <#泛型#>
    var createBy: String?
    /// <#泛型#>
    var trueName: String?
    /// <#泛型#>
    var loginName: String?
    ///
    var headImg: String?
    /// <#泛型#>
    var uuid: String?
    /// <#泛型#>
    var sort: String?
    /// <#泛型#>
    var version: String?
    /// <#泛型#>
    var salt: String?
    /// <#泛型#>
    var updateBy: String?
    /// <#泛型#>
    var email: String?
    /// <#泛型#>
    var phone: String?
    /// <#泛型#>
    var createDate: String?
    /// <#泛型#>
    var id: String?
    /// <#泛型#>
    var tenantId: String?
    /// <#泛型#>
    var nickName: String?
    /// <#泛型#>
    var dynamicImg: String?
    /// <#泛型#>
    var sex: String?
    /// <#泛型#>
    var identityCard: String?
    /// <#泛型#>
    var street: String?
    /// <#泛型#>
    var password: String?
    /// <#泛型#>
    var status: String?

    
}
