//
//  SYHomePageModel.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2021/3/23.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit
import HandyJSON


struct SYHomePageModel: HandyJSON, Equatable {

     ///
     var adoptList: [SYAdoptListModel]?
     ///
     var tourHotelList: [SYTourHotelListModel]?
     ///
     var agroMachineList: [SYDroneMachineListModel]?
     ///
     var droneMachineList: [SYDroneMachineListModel]?
     ///
     var demandMap: SYDemandMapModel?
     ///
     var newsList: [SYNewsListModel]?
     ///
     var publicBrandList: [SYPublicBrandListModel]?
     ///
     var tourScenicSpotVOList: [SYTourScenicSpotVOListModel]?
     ///
     var expertForumIssueVOList: [SYExpertForumIssueVOListModel]?
    
    var dangjianList: [SYNewsListModel]?
    var appMenuVOList: [NSAppMenuVOListModel]?
 }


struct SYPublicBrandListModel : HandyJSON, Equatable {

     /// <#泛型#>
     var updateDate: String?
     /// <#泛型#>
     var updateBy: String?
     /// <#泛型#>
     var businessLicense: String?
     /// <#泛型#>
     var uuid: String?
     /// <#泛型#>
     var tenantId: String?
     /// <#泛型#>
     var sort: String?
     /// <#泛型#>
     var createDate: String?
     /// <#泛型#>
     var status: String?
     /// <#泛型#>
     var enterpriseName: String?
     /// <#泛型#>
     var createBy: String?
     /// <#泛型#>
     var corporation: String?
     /// <#泛型#>
     var businessScope: String?
     /// <#泛型#>
     var id: String?
     /// <#泛型#>
     var version: String?
     /// <#泛型#>
     var address: String?
     ///
     var logoUrl: String?
     /// <#泛型#>
     var contactNumber: String?
     /// <#泛型#>
     var brand: String?
     /// <#泛型#>
     var remarks: String?
     /// <#泛型#>
     var linkman: String?
     /// <#泛型#>
     var businessHours: String?
 }


 struct SYDroneMachineListModel : HandyJSON, Equatable {

     /// <#泛型#>
     var brandName: String?
     /// <#泛型#>
     var updateDate: String?
     /// <#泛型#>
     var machineType: String?
     /// <#泛型#>
     var cityId: String?
     /// 迪迦
     var trueName: String?
     ///
     var machineUrl: String?
    
     var star: Int = 0
     /// <#泛型#>
     var taskStartTime: String?
     /// 0
     var orderNum: Int = 0
     /// <#泛型#>
     var typeId: String?
     /// <#泛型#>
     var status: String?
     /// <#泛型#>
     var workSpeed: String?
     /// <#泛型#>
     var areaId: String?
     /// <#泛型#>
     var intellectSort: String?
     /// <#泛型#>
     var uuid: String?
     /// <#泛型#>
     var tillageDepth: String?
     /// <#泛型#>
     var typeName: String?
     /// <#泛型#>
     var brandId: String?
     /// <#泛型#>
     var machineContact: String?
     /// <#泛型#>
     var streetId: String?
     /// 拯救者
     var machineName: String?
     /// <#泛型#>
     var userId: String?
     /// <#泛型#>
     var headImg: String?
     /// <#泛型#>
     var remarks: String?
     /// 198
     var taskPrice: Int = 0
     /// 05-21
     var publishDate: String?
     /// <#泛型#>
     var tillageWidth: String?
     /// <#泛型#>
     var connectForm: String?
     /// <#泛型#>
     var dimensions: String?
     /// <#泛型#>
     var area: String?
     /// 0
     var score: Int = 0
     /// <#泛型#>
     var machineStatus: String?
     /// <#泛型#>
     var version: String?
     /// <#泛型#>
     var tenantId: String?
     /// <#泛型#>
     var city: String?
     /// <#泛型#>
     var workYear: String?
     /// <#泛型#>
     var times: String?
     /// <#泛型#>
     var address: String?
     /// 83
     var id: String?
     /// <#泛型#>
     var createBy: String?
     /// <#泛型#>
     var provinceId: String?
     /// <#泛型#>
     var machineModel: String?
     /// <#泛型#>
     var taskEndTime: String?
     /// <#泛型#>
     var carNumber: String?
     /// <#泛型#>
     var updateBy: String?
     /// <#泛型#>
     var sort: String?
     /// <#泛型#>
     var machineContactVO: String?
     /// <#泛型#>
     var createDate: String?
     /// <#泛型#>
     var province: String?
     /// <#泛型#>
     var flyCardUrl: String?
     /// <#泛型#>
     var serviceDate: String?
     /// <#泛型#>
     var street: String?
 }


 struct SYNewsListModel : HandyJSON, Equatable {

     /// 0
     var browse: String?
     /// <#泛型#>
     var updateDate: String?
     /// <#泛型#>
     var tenantId: String?
     /// <#泛型#>
     var uuid: String?
     /// <#泛型#>
     var sort: String?
     ///
     var createDate: String?
     /// <#泛型#>
     var status: String?
     /// <#泛型#>
     var focus: String?
     /// <#泛型#>
     var createBy: String?
     /// <#泛型#>
     var introduction: String?
     ///
     var imgUrl: String?
     /// <#泛型#>
     var category: String?
     /// 83
     var id: String?
     /// <#泛型#>
     var version: String?
     /// 产业资讯
     var structify: String?
     /// <#泛型#>
     var img: String?
     /// <#泛型#>
     var shares: String?
     /// <#泛型#>
     var content: String?
     /// <#泛型#>
     var origin: String?
     ///
     var title: String?
     /// <#泛型#>
     var remarks: String?
     /// <#泛型#>
     var updateBy: String?
    
    var classify: String?
 }


 struct SYTourHotelListModel : HandyJSON, Equatable {

     /// <#泛型#>
     var orderDetail: String?
     /// 22号
     var addressDetail: String?
     /// <#泛型#>
     var cityId: String?
     /// <#泛型#>
     var version: String?
     /// <#泛型#>
     var sort: String?
     /// <#泛型#>
     var status: String?
     /// <#泛型#>
     var remarks: String?
     /// <#泛型#>
     var streetId: String?
     /// <#泛型#>
     var provinceId: String?
     ///
     var address: String?
     /// <#泛型#>
     var latitude: String?
     /// <#泛型#>
     var businessLicense: String?
     /// 酒店
     var name: String?
     /// <#泛型#>
     var orderExplain: String?
     /// <#泛型#>
     var detail: String?
     /// <#泛型#>
     var refundRule: String?
     /// <#泛型#>
     var uuid: String?
     /// <#泛型#>
     var districtId: String?
     /// <#泛型#>
     var contacts: String?
     ///
     var imgUrl: String?
     /// 7311
     var id: String?
     /// <#泛型#>
     var createBy: String?
     /// <#泛型#>
     var type: String?
     /// <#泛型#>
     var createDate: String?
     /// <#泛型#>
     var rates: String?
     /// <#泛型#>
     var longitude: String?
     /// <#泛型#>
     var mold: String?
     /// <#泛型#>
     var tenantId: String?
     /// <#泛型#>
     var car: String?
     /// <#泛型#>
     var updateBy: String?
     /// <#泛型#>
     var mobile: String?
     /// <#泛型#>
     var updateDate: String?
 }


 struct SYDemandMapModel : HandyJSON, Equatable {

     ///
     var message: String?
 }


 struct SYTourScenicSpotVOListModel : HandyJSON, Equatable {

     /// <#泛型#>
     var ticketVOList: String?
     /// <#泛型#>
     var provinceId: String?
     /// 绿松景点
     var name: String?
     /// 82
     var id: String?
     /// <#泛型#>
     var sort: String?
     /// <#泛型#>
     var car: String?
     /// <#泛型#>
     var districtId: String?
     /// <#泛型#>
     var remarks: String?
     /// <#泛型#>
     var tourScenicVisits: String?
     /// <#泛型#>
     var visitVOList: String?
     /// <#泛型#>
     var createBy: String?
     /// <#泛型#>
     var closeVOList: String?
     /// <#泛型#>
     var label: String?
     /// <#泛型#>
     var longitude: String?
     /// <#泛型#>
     var isCollect: String?
     /// <#泛型#>
     var openVOList: String?
     /// <#泛型#>
     var createDate: String?
     /// 22
     var rates: String?
     /// <#泛型#>
     var ticketNotice: String?
     /// <#泛型#>
     var advisoryList: String?
     /// <#泛型#>
     var orderNotice: String?
     /// <#泛型#>
     var phone: String?
     /// <#泛型#>
     var contacts: String?
     /// <#泛型#>
     var officialNotice: String?
     /// <#泛型#>
     var version: String?
     /// <#泛型#>
     var status: String?
     /// <#泛型#>
     var eatHomePlayLabel: String?
     /// <#泛型#>
     var detail: String?
     /// <#泛型#>
     var streetId: String?
     /// <#泛型#>
     var scenicSpotList: String?
     ///
     var address: String?
     /// <#泛型#>
     var refundRule: String?
     /// <#泛型#>
     var scenicOrders: String?
     /// <#泛型#>
     var explainVOList: String?
     /// <#泛型#>
     var hasExplain: String?
     /// 56号
     var addressDetail: String?
     /// <#泛型#>
     var labelIdStr: String?
     ///
     var imgUrl: String?
     /// <#泛型#>
     var tenantId: String?
     /// <#泛型#>
     var cityId: String?
     /// <#泛型#>
     var latitude: String?
     /// <#泛型#>
     var sysFileVOList: String?
     /// <#泛型#>
     var tourHotelVOList: String?
     /// <#泛型#>
     var uuid: String?
     /// <#泛型#>
     var labelStr: String?
     /// <#泛型#>
     var street: String?
     /// <#泛型#>
     var tourScenicExplainVO: String?
     /// <#泛型#>
     var updateDate: String?
     /// <#泛型#>
     var updateBy: String?
 }


 struct SYExpertForumIssueVOListModel : HandyJSON, Equatable {

     /// <#泛型#>
     var hasReply: String?
     /// 1
     var version: Int = 0
     /// <#泛型#>
     var star: String?
     /// 1
     var sort: Int = 0
     /// <#泛型#>
     var expertSysUserVo: String?
     /// 1
     var status: Int = 0
     /// <#泛型#>
     var remarks: String?
     /// 13
     var territoryIds: String?
     /// <#泛型#>
     var comment: String?
     /// <#泛型#>
     var replyCount: String?
     /// <#泛型#>
     var sysFileVOList: String?
     /// <#泛型#>
     var expertInfoVO: String?
     ///
     var uuid: String?
     /// 27
     var id: String?
     /// 138
     var createBy: String?
     /// <#泛型#>
     var sysPublicVO: String?
     ///
     var createDate: String?
     /// <#泛型#>
     var hasAttention: String?
     ///
     var forumReplyVO: SYForumReplyVOModel?
     /// 1
     var tenantId: String?
     /// 138
     var updateBy: String?
     /// <#泛型#>
     var sysUserVO: String?
     /// 2058
     var fileIds: String?
     /// <#泛型#>
     var who: String?
     /// 49
     var expertId: String?
     /// 可以救救我的苹果吗？
     var issue: String?
     ///
     var updateDate: String?
 }


 struct SYForumReplyVOModel : HandyJSON, Equatable {

     /// <#泛型#>
     var uuid: String?
     /// <#泛型#>
     var version: String?
     /// <#泛型#>
     var createDate: String?
     /// <#泛型#>
     var expert: String?
     /// <#泛型#>
     var updateBy: String?
     /// <#泛型#>
     var id: String?
     /// <#泛型#>
     var remarks: String?
     /// <#泛型#>
     var updateDate: String?
     /// <#泛型#>
     var sysUserVO: String?
     /// <#泛型#>
     var sort: String?
     /// 我来试一试救救它
     var reply: String?
     /// <#泛型#>
     var createBy: String?
     /// <#泛型#>
     var issueId: String?
     /// <#泛型#>
     var expertInfoVO: String?
     /// <#泛型#>
     var status: String?
     /// <#泛型#>
     var tenantId: String?
 }


 struct SYAdoptListModel :  HandyJSON, Equatable{

     /// <#泛型#>
     var remainNum: String?
     /// <#泛型#>
     var productionCycleEnd: String?
     /// <#泛型#>
     var productionCycleBegin: String?
     /// <#泛型#>
     var farmId: String?
     /// <#泛型#>
     var landName: String?
     /// <#泛型#>
     var productId: String?
     /// <#泛型#>
     var description: String?
     /// <#泛型#>
     var adoptPeopleNum: String?
     /// <#泛型#>
     var giftName: String?
     /// <#泛型#>
     var updateDate: String?
     /// <#泛型#>
     var adoptPart: String?
     /// <#泛型#>
     var adoptHarvestUnitId: String?
     /// <#泛型#>
     var createDate: String?
     /// <#泛型#>
     var dictionaryId: String?
     /// <#泛型#>
     var traceRecords: String?
     ///
     var imgUrl: String?
     /// <#泛型#>
     var adoptedNum: String?
     /// 84%
     var adoptedPercent: String?
     /// 热销
     var remarks: String?
     /// <#泛型#>
     var typeName: String?
     /// <#泛型#>
     var uuid: String?
     /// <#泛型#>
     var sort: String?
     /// <#泛型#>
     var adoptHarvestNum: String?
     /// <#泛型#>
     var status: String?
     /// <#泛型#>
     var presentNumber: String?
     /// <#泛型#>
     var farmName: String?
     /// <#泛型#>
     var serialNumber: String?
     /// 66.00
	var adoptPrice: String?
     /// 1
     var id: String?
     /// <#泛型#>
     var carouselImgUrl: String?
     /// <#泛型#>
     var version: String?
     /// <#泛型#>
     var present: String?
     /// <#泛型#>
     var unitPrice: String?
     /// <#泛型#>
     var adoptPartUnitId: String?
     /// <#泛型#>
     var detailImgUrl: String?
     /// <#泛型#>
     var purchaseLimit: String?
     /// <#泛型#>
     var attention: String?
     /// <#泛型#>
     var totalNum: String?
     /// <#泛型#>
     var adoptHarvest: String?
     /// <#泛型#>
     var productNumber: String?
     /// <#泛型#>
     var deviceId: String?
     /// <#泛型#>
     var adoptHarvestUnit: String?
     /// <#泛型#>
     var priceDescription: String?
     /// <#泛型#>
     var landId: String?
     /// 矮脚黄
     var name: String?
     /// <#泛型#>
     var presentId: String?
     /// <#泛型#>
     var adoptPartNum: String?
     /// <#泛型#>
     var createBy: String?
     /// <#泛型#>
     var updateBy: String?
     /// <#泛型#>
     var tenantId: String?
     /// <#泛型#>
     var adoptPartUnit: String?
     /// <#泛型#>
     var contact: String?
     /// <#泛型#>
     var productionCycle: String?
     /// <#泛型#>
     var giftNumber: String?
     /// <#泛型#>
     var adoptExplain: String?
     /// <#泛型#>
     var type: String?
     /// <#泛型#>
     var giftRemain: String?
     /// <#泛型#>
     var adoptType: String?
     /// <#泛型#>
     var dictionaryIds: String?
 }


