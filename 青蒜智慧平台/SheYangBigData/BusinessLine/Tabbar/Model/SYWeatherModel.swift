//
//  SYWeatherModel.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2021/3/26.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit
import HandyJSON


struct SYWeatherModel : HandyJSON {

    ///
    var cityInfo: SYCityInfoModel?
    /// 200
    var status: Int = 0
    ///
    var time: String?
    ///
    var message: String?
    ///
    var data: SYWeatherDataModel?
    /// 20210326
    var date: String?

}


struct SYWeatherDataModel : HandyJSON {

    /// 良
    var quality: String?
    /// 21
    var wendu: String?
    /// 68
    var pm10: Int = 0
    /// 41%
    var shidu: String?
    /// 25
    var pm25: Int = 0
    ///
    var forecast: [SYForecastModel]?
    ///
    var yesterday: SYYesterdayModel?
    ///
    var ganmao: String?

}


struct SYCityInfoModel : HandyJSON {

    /// 101190101
    var citykey: String?
    /// 江苏
    var parent: String?
    /// 南京市
    var city: String?
    /// 12:01
    var updateTime: String?

}


struct SYForecastModel : HandyJSON {

    /// 东南风
    var fx: String?
    /// 26
    var date: String?
    /// 2021-03-26
    var ymd: String?
    /// 多云
    var type: String?
    /// 18:20
    var sunset: String?
    /// 阴晴之间,谨防紫外线侵扰
    var notice: String?
    /// 06:01
    var sunrise: String?
    /// 星期五
    var week: String?
    /// 3级
    var fl: String?
    /// 低温 16℃
    var low: String?
    /// 高温 25℃
    var high: String?
    /// 45
    var aqi: Int = 0

}


struct SYYesterdayModel : HandyJSON {

    /// 东南风
    var fx: String?
    /// 25
    var date: String?
    /// 2021-03-25
    var ymd: String?
    /// 晴
    var type: String?
    /// 18:19
    var sunset: String?
    /// 愿你拥有比阳光明媚的心情
    var notice: String?
    /// 06:02
    var sunrise: String?
    /// 星期四
    var week: String?
    /// 2级
    var fl: String?
    /// 低温 12℃
    var low: String?
    /// 高温 23℃
    var high: String?
    /// 80
    var aqi: Int = 0

}
