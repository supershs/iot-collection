//
//  SYLoginModel.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/1/27.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit
import HandyJSON


struct SYLoginModel : HandyJSON,Equatable {

    ///
    var userInfo: NSUserInfoModel?
    ///
    var token: String?
}


struct NSUserInfoModel : HandyJSON,Equatable {

    /// 1
    var sex: Int = 0
    /// 1
    var createBy: Int = 0
    /// 26
    var id: String?
    /// <#泛型#>
    var roleList: String?
    /// 1
    var status: Int = 0
    /// 1
    var updateBy: Int = 0
    /// 1
    var version: Int = 0
    /// <#泛型#>
    var phone: String?
    ///
    var salt: String?
    ///
    var createDate: String?
    /// 1
    var sort: Int = 0
    ///
    var dynamicImg: String?
    ///
    var headImg: String?
    ///
    var password: String?
    /// <#泛型#>
    var roleId: String?
    /// <#泛型#>
    var street: String?
    /// <#泛型#>
    var identityCard: String?
    /// <#泛型#>
    var email: String?
    /// 13915999715
    var mobile: String?
    /// 1
    var tenantId: Int = 0
    ///
    var uuid: String?
    ///
    var updateDate: String?
    /// <#泛型#>
    var loginName: String?
    /// <#泛型#>
    var trueName: String?
    /// <#泛型#>
    var remarks: String?
}
