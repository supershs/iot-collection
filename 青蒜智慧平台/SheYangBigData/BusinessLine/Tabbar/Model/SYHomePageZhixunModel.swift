//
//  SYHomePageZhixunModel.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/2/1.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit
import HandyJSON

struct SYHomePageZhixunModel: HandyJSON,Equatable {
    
    ///
    var createDate: String?
    ///
    var content: String?
    /// <#泛型#>
    var remarks: String?
    /// 1
    var version: Int = 0
    /// <#泛型#>
    var shares: String?
    /// 1
    var sort: Int = 0
    /// 加强农药管理防空安全风险
    var title: String?
    ///
    var imgUrl: String?
    /// 2
    var focus: Int = 0
    ///
    var img: String?
    ///
    var updateDate: String?
    /// 1
    var createBy: Int = 0
    ///
    var uuid: String?
    /// 311
    var category: String?
    /// 4
    var id: String?
    /// <#泛型#>
    var origin: String?
    /// 1
    var tenantId: Int = 0
    /// 1
    var status: Int = 0
    /// 农业基础知识
    var classify: String?
    /// 149
    var browse: Int = 0
    /// 1
    var updateBy: Int = 0

}
