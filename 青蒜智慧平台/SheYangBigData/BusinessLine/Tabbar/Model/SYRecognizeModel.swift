//
//  SYRecognizeModel.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/6/29.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit
import HandyJSON

struct SYRecognizeModel : HandyJSON, Equatable { 

    /// 32391
    var updateBy: String?
    /// 32391
    var createBy: String?
    /// <#泛型#>
    var tenantUrl: String?
    /// <#泛型#>
    var remarks: String?
    /// <#泛型#>
    var idd: String?
    /// 1
    var tenantId: String?
    ///
    var imgUrl: String?
    /// <#泛型#>
    var content: String?
    ///
    var uuid: String?
    /// 粘虫,幼虫
    var name: String?
    /// 251
    var itemId: String?
    /// PESTS
    var discernEnum: String?
    ///
    var ename: String?
    ///
    var createDate: String?
    ///
    var boxPoints: String?
    /// 虫害
    var discernEnumStr: String?
    /// <#泛型#>
    var img: String?
    /// 1
    var version: Int = 0
    ///
    var userImg: String?
    /// 0.7
    var score: Double?
    /// 1
    var sort: Int = 0
    ///
    var updateDate: String?
    /// 1
    var status: Int = 0
}



struct SYRecognizeResultsModel : HandyJSON, Equatable {

    /// success
    var msg: String?
    ///
    var imgUrl: String?
    /// 1
    var code: Int = 0
    /// <#泛型#>
    var totalPage: String?
    ///
    var data: SYRecognizeResultModel?
    /// 285
    var id: String?
}



struct SYRecognizeResultModel : HandyJSON, Equatable {

    ///
    var img: String?
    /// 病害
    var diseaseType: String?
    /// 水稻
    var prodType: String?
    /// 农作物-禾本科
    var prodParentType: String?
    ///
    var userImg: String?
    /// 91
    var id: Int = 0
    /// 稻曲病
    var title: String?
    /// 0.4627808
    var score: Double?
    /// <#泛型#>
    var imgWithoutWatermark: String?
    ///
    var content: String?
	var logId: String?

}
