//
//  SYHomePageMenu.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/2/1.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit
import HandyJSON


class SYHomePageMenuModel : HandyJSON {

    /// <#泛型#>
    var roleId: String?
    /// <#泛型#>
    var url: String?
    /// <#泛型#>
    var mold: String?
    /// <#泛型#>
    var sort: String?
    /// <#泛型#>
    var uuid: String?
    /// <#泛型#>
    var tenantId: String?
    /// <#泛型#>
    var id: String?
    /// <#泛型#>
    var updateDate: String?
    /// <#泛型#>
    var version: String?
    /// <#泛型#>
    var createBy: String?
    /// <#泛型#>
    var createDate: String?
    ///
    var appMenuVOList: [SYAppMenuListModel]?
    /// <#泛型#>
    var remarks: String?
    /// <#泛型#>
    var name: String?
    /// <#泛型#>
    var status: String?
    /// <#泛型#>
    var imgUrl: String?
    /// <#泛型#>
    var updateBy: String?

    required init() {}
}


class SYAppMenuListModel : HandyJSON {

    /// ALL
    var roleId: String?
    ///
    var url: String?
    /// 1
    var mold: Int = 0
    /// 1
    var sort: Int = 0
    /// <#泛型#>
    var uuid: String?
    /// <#泛型#>
    var tenantId: String?
    /// 1
    var itemId: String?
    ///
    var updateDate: String?
    /// 1
    var version: Int = 0
    /// 1
    var createBy: Int = 0
    ///
    var createDate: String?
    /// <#泛型#>
    var remarks: String?
    /// 公共品牌
    var name: String?
    /// 1
    var status: Int = 0
    ///
    var imgUrl: String?
    /// 1
    var updateBy: Int = 0

    required init() {}

    public func mapping(mapper: HelpingMapper) {

        mapper <<< self.itemId <-- "id"

     }
}
