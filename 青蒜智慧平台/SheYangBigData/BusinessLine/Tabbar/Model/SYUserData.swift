//
//  SYUserData.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2020/8/25.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit
import HandyJSON

class SYUserData: SYBaseModel {
    
    var data: BaseData?
    struct BaseData {
        
        var userInfo: UserInfo?
        var socket: Socket?
        var token: String?
        
        struct UserInfo: HandyJSON {
            var cicleBackimg: String? //TODO: Specify the type to conforms Codable protocol
            var identification: String? //TODO: Specify the type to conforms Codable protocol
            var categoryName: String? //TODO: Specify the type to conforms Codable protocol
            var memberName: String?
            var remarks: String?
            var areaCode: String? //TODO: Specify the type to conforms Codable protocol
            var id: String?
            var sex: Int?
            var goodAtField: String? //TODO: Specify the type to conforms Codable protocol
            var updateBy: String? //TODO: Specify the type to conforms Codable protocol
            var createBy: String? //TODO: Specify the type to conforms Codable protocol
            var provinceCode: String? //TODO: Specify the type to conforms Codable protocol
            var registerDate: String? //TODO: Specify the type to conforms Codable protocol
            var uuid: String?
            var currentCount: String? //TODO: Specify the type to conforms Codable protocol
            var salt: String? //TODO: Specify the type to conforms Codable protocol
            var lastLoginTime: String? //TODO: Specify the type to conforms Codable protocol
            var password: String? //TODO: Specify the type to conforms Codable protocol
            var trueName: String?
            var firstLogin: Int?
            var status: Int?
            var count: String? //TODO: Specify the type to conforms Codable protocol
            var roleId: String?
            var headImg: String?
            var addressDetail: String?
            var source: Int?
            var unionId: String?
            var manageName: String? //TODO: Specify the type to conforms Codable protocol
            var userId: String? //TODO: Specify the type to conforms Codable protocol
            var radioOrderType: String? //TODO: Specify the type to conforms Codable protocol
            var roleName: String? //TODO: Specify the type to conforms Codable protocol
            var integExchangeGoods: String? //TODO: Specify the type to conforms Codable protocol
            var nickName: String? //TODO: Specify the type to conforms Codable protocol
            var sort: Int?
            var cityCode: String? //TODO: Specify the type to conforms Codable protocol
            var updateDate: String? //TODO: Specify the type to conforms Codable protocol
            var goodAtType: String? //TODO: Specify the type to conforms Codable protocol
            var searchKey: String? //TODO: Specify the type to conforms Codable protocol
            var memberDesc: String?
            var authState: String? //TODO: Specify the type to conforms Codable protocol
            var workingUnit: String? //TODO: Specify the type to conforms Codable protocol
            var age: Int?
            var createDate: String?
            var goodAtFiled: String? //TODO: Specify the type to conforms Codable protocol
            var psStatus: String? //TODO: Specify the type to conforms Codable protocol
            var varietyName: String? //TODO: Specify the type to conforms Codable protocol
            var content: String? //TODO: Specify the type to conforms Codable protocol
            var level: String? //TODO: Specify the type to conforms Codable protocol
            var version: Int?
            var idCard: String?
            var platUserInfosId: String? //TODO: Specify the type to conforms Codable protocol
            var mobile: String?
            var jsName: String? //TODO: Specify the type to conforms Codable protocol
            var kindName: String? //TODO: Specify the type to conforms Codable protocol
        }
        
        struct Socket: HandyJSON {
            var id: String?
            var createBy: String? //TODO: Specify the type to conforms Codable protocol
            var uuid: String?
            var version: Int?
            var updateBy: String? //TODO: Specify the type to conforms Codable protocol
            var createDate: String?
            var gid: String?
            var password: String?
            var updateDate: String? //TODO: Specify the type to conforms Codable protocol
            var account: String?
            var remarks: String? //TODO: Specify the type to conforms Codable protocol
            var sort: String? //TODO: Specify the type to conforms Codable protocol
            var status: Int?
        }
    }
}

