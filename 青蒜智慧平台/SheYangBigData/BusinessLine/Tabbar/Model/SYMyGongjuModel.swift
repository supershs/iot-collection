//
//  SYMyGongjuModel.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2021/2/1.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit
import HandyJSON



struct SYMyGongjuModel : HandyJSON {

    /// <#泛型#>
    var createDate: String?
    /// 1
    var version: Int = 0
    /// 1
    var sort: Int = 0
    /// <#泛型#>
    var updateBy: String?
    /// <#泛型#>
    var uuid: String?
    /// 1
    var status: Int = 0
    /// <#泛型#>
    var url: String?
    ///
    var imgUrl: String?
    /// <#泛型#>
    var tenantId: String?
    /// ALL
    var roleId: String?
    /// 17
    var itemId: String?
    /// 上门服务
    var name: String?
    /// 2
    var mold: Int = 0
    /// <#泛型#>
    var updateDate: String?
    /// <#泛型#>
    var createBy: String?
    /// <#泛型#>
    var remarks: String?

  
}
struct HeadMyData : HandyJSON {

    
    var money: String?//我的钱包
    var coupons: String?//我的优惠券
    var integral: String?//我的积分


  
}
