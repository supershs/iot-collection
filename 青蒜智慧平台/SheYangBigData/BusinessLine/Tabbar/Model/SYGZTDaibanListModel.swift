//
//  SYGZTDaibanModel.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2021/2/2.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit
import HandyJSON


struct SYGZTDaibanListModel : HandyJSON, Equatable {

    ///
    var serviceAddress: String?
    /// <#泛型#>
    var remarks: String?
    /// <#泛型#>
    var platMoney: String?
    /// <#泛型#>
    var confirmTime: String?
    /// 订单状态（0 待确认 1.农户待付款 2 待服务 3 进行中 4 待结算(农机手改价格比之前多的情况) 5 待评价 6已关闭(待确认或者农户未付钱关闭的情况) 7 农机手关闭 8 客户关闭 9申报故障 10 已完成 11待农户评价 12 待农机手评价 13待付尾款）
    var orderStatus: Int = 0
    ///
    var machineName: String?
    /// <#泛型#>
    var status: String?
    /// <#泛型#>
    var orderRemarks: String?
    /// <#泛型#>
    var trueName: String?
    ///
    var location: String?
    ///
    var orderNo: String?
    /// <#泛型#>
    var earnestTime: String?
    /// <#泛型#>
    var appointmentId: String?
    /// <#泛型#>
    var sort: String?
    /// <#泛型#>
    var headImg: String?
    /// <#泛型#>
    var result: String?
    /// <#泛型#>
    var updateDate: String?
    /// <#泛型#>
    var endTime: String?
    /// 02月02日全天
    var time: String?
    /// <#泛型#>
    var createBy: String?
    /// <#泛型#>
    var uuid: String?
    /// <#泛型#>
    var closeTime: String?
    /// <#泛型#>
    var frontMoneyPayNo: String?
    /// <#泛型#>
    var machineUserId: String?
    /// <#泛型#>
    var payWay: String?
    /// 18596858748
    var userPhone: String?
    /// 49
    var id: String?
    /// <#泛型#>
    var details: String?
    /// <#泛型#>
    var startTime: String?
    ///
    var createDate: String?
    /// <#泛型#>
    var reason: String?
    /// 43
    var childId: String?
    /// <#泛型#>
    var money: String?
    /// <#泛型#>
    var mobile: String?
    /// <#泛型#>
    var list: String?
    /// 82
    var price: Double = 0
    /// 1
    var area: Int = 0
    /// <#泛型#>
    var version: String?
    /// bargainMoney
    var bargainMoney: Double?
    /// 王室
    var userName: String?
    /// <#泛型#>
    var payName: String?
    /// 农机类型(1是农机，2是植保机)
    var machineType: String?
    /// actualMoney
    var actualMoney: Double?
    /// <#泛型#>
    var updateBy: String?
    /// <#泛型#>
    var keepMoney: String?
    /// <#泛型#>
    var newsTime: String?
    ///
    var machineUrl: String?
    /// <#泛型#>
    var machineId: String?
    /// <#泛型#>
    var tenantId: String?
    /// 82
    var orderTotalPayment: Int = 0
    /// <#泛型#>
    var finishTime: String?
    /// <#泛型#>
    var supplementMoney: String?

    func getOrderState() -> (String, UIColor, SYWebApiManager) {
        switch self.orderStatus {
        case 0:
            return ("待确认", UIColor(hex: 0xFC6435), .daiQuerenXiangqing(id: self.id ?? ""))
        case 1:
            return ("待农户支付定金", UIColor(hex: 0xFC6435), .daifuLvyuejin(id: self.id ?? ""))
        case 2:
            return ("待作业", UIColor(hex: 0xFC6435), .daiFuwu(id: self.id ?? ""))
        case 3:
            return ("进行中", UIColor(hex: 0x00C793), .jinxingzhong(id: self.id ?? ""))
        case 4:
            return ("待结算", UIColor(hex: 0x00C793), .daiJiesuan(id: self.id ?? ""))
        case 5:
            return ("待评价", UIColor(hex: 0x00C793), .daiJiesuan(id: self.id ?? ""))
        case 6:
            return ("已关闭", UIColor(hex: 0x333333), .close(id: self.id ?? ""))
        case 7:
            // 农机手关闭
            return ("已关闭", UIColor(hex: 0x333333), .close(id: self.id ?? ""))
        case 8:
            // 客户关闭
            return ("已关闭", UIColor(hex: 0x333333), .close(id: self.id ?? ""))
        case 9:
            return ("申报故障", UIColor(hex: 0xFC6435), .shenqingGuzhang(id: self.id ?? ""))
        case 10:
            return ("已完成", UIColor(hex: 0x333333), .daiJiesuan(id: self.id ?? ""))
        case 11:
            return ("已评价", UIColor(hex: 0x00C793), .daiJiesuan(id: self.id ?? ""))
        case 12:
            return ("待评价", UIColor(hex: 0x00C793), .daiJiesuan(id: self.id ?? ""))
        case 13:
            return ("待支付", UIColor(hex: 0xFC6435), .daiJiesuan(id: self.id ?? ""))
        case 14:
            // 待付履约金
            return ("待我支付", UIColor(hex: 0xFC6435), .daifuLvyuejin(id: self.id ?? ""))
        default:
            return ("", .white, .daiFuwu(id: self.id ?? ""))
        }
    }
}
