//
//  Constant_API.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2020/8/11.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit


let VERSION = "/api/1.0/"

//本地
//var WEBIP = "http://192.168.0.80:8080/#/"



//线上
var WEBIP = "http://119.3.193.20:9223/#/"
//线上
var IP = "http://49.4.60.13:9221"
//线上
var IMGIP = "http://114.115.166.192:9081"

//登录
let logUrl = "http://119.3.193.20:9221/api/1.0/Login/check"
let GOTOHOME = "pages/tabBar/homePage?data="


let paraUrl = "33app.33iot.com"
let testMP3 = "http://downsc.chinaz.net/Files/DownLoad/sound1/201906/11582.mp3"
let testMP4 = "http://static.tripbe.com/videofiles/20121214/9533522808.f4v.mp4"

let REQUEST_DIC = "requestDictionary"
















// MARK: - 认养农业

// 认养首页人气认养显示3条
let RENYANG_HOME_LIST = "AdoptFarmProduct/popularityList/page"
// 认养首页动态接口
let RENYANG_HOME_DYNAMIC =  "AdoptFarmProductOrder/listDynamicOrder"

////首页搜索接口
let HOMEPAGE_SEARCH = "AppMessage/getMessageStatus"
// 农场列表
let NONGCHANG_LIST = "AdoptFarm/list/page"

// 农场产品列表
let NONGCHANG_PRODUCT_LIST = "AdoptFarmProduct/getByFarmId/page"

// 农场产品筛选-获取价格pid=284， 类型pid=264
//let NONGCHANG_CHOICE = "SysDictionary/getListByPid"


// 获取中国省市区行政信息表详情
let DIQU = "SysDistricts/list"

// 农场产品详情
let NONGCHANG_PRODUCT_DETAIL = "AdoptFarmProduct/getDetail"

// 农场产品传感器监测数据
let NONGCHANG_PRODUCT_JIANCE = "AdoptSensorInfo/getRealData"

// 农场产品-立即认养 最大数量
let NONGCHANG_PRODUCT_BUY_MAXNUM = "AdoptFarmProductOrder/getMaxBuyNum"

// 农场产品-立即认养 下单
let NONGCHANG_PRODUCT_BUY = "AdoptFarmProductOrder/addProductOrder"

// 收藏
let RENYANG_SHOUCANG = "AdoptProductConcerns/attention"

//认养详情 是监控还是视频
let RENYANG_ISVIDEO = "AdoptSensorInfo/getVideoList"

// 配送方式、周期 ： 配送方式   290，配送周期   292
//let PEISONG_STYLE = "SysDictionary/getListByPid"

let PEISONG_WAY = "SysPublic/app/indexes"

let NONGCHANG_TYP_SEC = "SysPublic/app/appoint"

//认养默认收货地址
let RYMORENADDRESS = "AdoptShippingAddress/getDefaultAddress"

// 新增地址
let ADD_ADDRESS = "AdoptShippingAddress/add"

// 编辑地址
let EDIT_ADDRESS = "AdoptShippingAddress/edit"

// 收货地址列表
let ADDRESS_LIST = "AdoptShippingAddress/list/page"

// 删除地址
let DELETE_ADDRESS = "AdoptShippingAddress/remove"

// 订单详情
let DINGDAN_DETAIL = "AdoptFarmProductOrder/getOrderDetail"

// 订单列表
let DINGDAN_LIST = "AdoptFarmProductOrder/list/page"


// MARK: - 农友圈
// 农友圈列表
let NONGYOUQUAN_LIST = "Dynamic/listInfo"

// 我的农友圈列表
let MY_LIST = "Dynamic/mySelf"

let ADD_COMMENT = "DynamicComment/app/add"

// 点赞
let DIANZAN = "DynamicDot/dot"

// 点赞列表
let DIANZAN_LIST = "DynamicDot/history"

// 审核列表
let SHENHE_LIST = "DynamicAudit/history"

// 删除用户单个农友圈
//let DELETE_MY = "Dynamic/remove"
let DELETE_MY = "Dynamic/App/clear"


// 用户单个农友圈
let MY_SINGLE = "Dynamic/detail"

// 发布
let RELEASE = "Dynamic/release"

// 上传图片、视频、语音
let UPLOAD_IMAGES = "Dynamic/uploadResource"

// 背景图片替换
let CHANGE_BG_IMAGE = "Dynamic/App/bgChange"


// MARK: - tabbar
// 登录
let LOGIN = "AppLogin/passwordLogin"

let CHIXUDINGWEI = "SysUser/app/online/location"
// 首页、更多页面菜单 首页state = 1, 更多页面state = 0
let HOMEPAGE_MENU = "AppMenu/currentMenu"


let SYMORE_LIST = "AppMenu/app/more"

let SYMODEL_CHANGE = "AppMenuUser/app/change"

// 首页轮播
let HOMEPAGE_BANNER = "SysPublic/app/indexes"

// 首页咨询
let HOMEPAGE_ZHIXUN = "NewsAgricultural/hot"

// 首页信息
let HOMEPAGE_INFO = "AppLogin/app/home"

// 首页天气
let HOMEPAGE_WEATHER = "AppLogin/getFirstPageWeather"

// 我的信息
let USER_INFO = "SysUser/app/baseInfo"

//我的优惠券
let MYYOUHUIQUAN = "CouponsReceive/getAllCouponsByUserId"
let RY_QUAN = "AdoptFarmProductOrder/getAllCoupons"

let RY_MINE  = "IntegralStatistical/app/mine"
let RY_LIVE = "fcloud/getAccessToken"
// 我的咨询

let MY_CONSULTATION  = "TourAdvisory/app/myself"

let MY_ZiXunDetail  = "TourAdvisory/app/my/detail"

// 我的推荐工具
let GONGJU = "AppMenu/recommend"

// 消息列表
let IM_LIST = "AppMessage/getNewMessages"

// 删除消息
let IM_DELETE = "DynamicChatMessage/removeNewMessage"

// 更新消息时间
let IM_UPDATE_READ_TIME = "AppMessageRead/updateReadTime"

// 获取聊天室id
let IM_CREATE_ROOM = "DynamicChat/createRoom"

// 获取聊天记录
let IM_GET_MESSAGES = "DynamicChat/logInfo"

// 发送消息
let IM_SEND_MESSAGE = "DynamicChat/chat"

// 工作台首页
let GONGZUOTAI = "AppMachineOrderInfo/getWorkbenchInfo"

// 工作台代办事项
let GONGZUOTAI_DAIBAN = "AppMachineOrderInfo/machineList/page"


// MARK: - 导游导览
// 首页
let DY_FIRST_PAGE = "TourRestaurant/App/defaultInfo"

// 首页 - 定位
let DY_FIRST_PAGE_LOCATION = "SysDistricts/getAreaInfo"

// 首页 景区筛选信息
let DY_JINGQU_CHOICE_INFO = "TourScenicSpot/App/appoint"

// 首页 景区筛选信息
let DY_CHOICE_INFO = "SysPublic/app/indexes"

// 景区筛选类型
let DY_JINGQU_SELECT = "TourScenicSpot/App/liInfo"

// 景区搜索
let DY_JINGQU_SEARCH = "TourScenicSpot/App/search"

// 景区详情
let DY_JINGQU_DETAIL = "TourScenicSpot/App/detail"
//我的预览 --- 我的关注
let DY_MYGUANGZHU  = "TourCollect/app/list"

// 收藏景区详情
let DY_JINGQU_DETAIL_SHOUCANG = "TourScenicSpot/app/Collect"

// 景区语音讲解更多
let DY_JINGQU_JIANGJIE_MORE = "TourScenicExplain/list/page"

// 景区语音讲解使用次数加1
let DY_JINGQU_JIANGJIE_ADD_COUNT = "TourExplainUse/useAdd"

// 景区游览列表
let DY_JINGQU_YOULAN_LIST = "TourScenicVisit/list/page"

// 景区游览详情
let DY_JINGQU_YOULAN_DETAIL = "TourScenicVisit"

// 景区咨询
let DY_JINGQU_ZIXUN_LIST = "TourScenicSpot/App/advisory"

// 添加咨询
let DY_JINGQU_ZIXUN_ADD = "TourAdvisory/add"


// 景区预约-预约近三天预约日期 + 某天预约详情
let DY_JINGQU_YUYUE = "TourScenicOrder/App/threeDay"

// 景区提交预约订单
let DY_JINGQU_ADD = "TourScenicOrder/App/add"

// 景区人员列表
let DY_JINGQU_PEOPLES = "TourOrderPeople/app/info"

// 景区新增人员
let DY_JINGQU_PEOPLE_ADD = "TourOrderPeople/add"

// 景区人员详情
let DY_JINGQU_PEOPLE_DETAIL = "TourOrderPeople"

// 景区人员编辑
let DY_JINGQU_PEOPLE_EDIT = "TourOrderPeople/edit"

// 景区人员删除
let DY_JINGQU_PEOPLE_DELETE = "TourOrderPeople/remove"

// 景区预约订单详情
let DY_JINGQU_DINGDAN_DELETE = "TourScenicOrder/App/detail"



// MARK: - 导游导览 美食
// 首页 美食筛选信息
let DY_MEISHI_CHOICE_INFO = "TourRestaurant/App/appoint"

// 美食详情
let DY_MEISHI_DETAIL = "TourRestaurant/App/detail"

// 收藏美食详情
let DY_MEISHI_DETAIL_SHOUCANG = "TourRestaurant/app/Collect"

// 美食列表
let DY_MEISHI_LIST = "TourRestaurant/App/search"

// 美食筛选类型
let DY_MEISHI_SELECT = "TourRestaurant/App/liInfo"

// 美食咨询
let DY_MEISHI_ZIXUN_LIST = "TourRestaurant/App/advisory"

// 美食  预订页面各选项 - default/完全预约时间
let DY_MEISHI_YUDING_CHOOSE = "TourRestaurantOrder/app/fifteenDay"

// 美食   预订页面单选项 - 传入日期，返回营业时间段
let DY_MEISHI_YUDING_CHOOSE_TIME = "TourRestaurantOrder/app/hours"

// 美食预约生成订单
let DY_MEISHI_ORDER = "TourRestaurantOrder/App/order"

// 美食预约订单详情
let DY_MEISHI_ORDER_DETAIL = "TourRestaurantOrder/App/detail"


// MARK: - 导游导览 住宿

// 首页 住宿筛选信息
let DY_ZHUSU_CHOICE_INFO = "TourHotel/App/appoint"

// 住宿列表
let DY_ZHUSU_LIST = "TourHotel/App/search"

// 住宿筛选类型
let DY_ZHUSU_SELECT = "TourHotel/App/liInfo"

// 住宿咨询
let DY_ZHUSU_ZIXUN_LIST = "TourHotel/App/advisory"

// 住宿详情
let DY_ZHUSU_DETAIL = "TourHotel/App/detail"

// 住宿详情-房间列表
let DY_ZHUSU_DETAIL_ROOMS = "TourHotelRoom/app/roomList"

// 住宿详情-不可预约时间
let DY_ZHUSU_DETAIL_CAN_NOT_YUYUE = "TourHotel/app/timeList"

// 收藏住宿详情
let DY_ZHUSU_DETAIL_SHOUCANG = "TourHotel/app/Collect"

// 住宿提交订单
let DY_ZHUSU_ADD_DINGDAN = "TourHotelOrder/App/order"

// 住宿订单详情
let DY_ZHUSU_DINGDAN_DETAIL = "TourHotelOrder/App/detail"

// 我的订单
let DY_DINGDANLIST = "TourScenicOrder/App/myOrder"

// 景区取消订单
let DY_JINGQU_DINGDAN_CANCEL = "TourScenicOrder/App/giveUp"

// 住宿取消订单
let DY_ZHUSU_DINGDAN_CANCEL = "TourHotelOrder/App/giveUp"

// 美食取消订单
let DY_MEISHI_CANCEL = "TourRestaurantOrder/app/cancel"

// 删除订单
let DY_DINGDAN_DELETE = "TourScenicOrder/App/remove"

// 景区支付宝支付
let DY_JINGQU_ALI_PAY = "TourScenicOrder/App/zfb/pay"

// 住宿支付宝支付
let DY_ZHUSU_ALI_PAY = "TourHotelOrder/App/zfb/pay"

// MARK: - 支付
// 银联
let YINLIAN_PAY = "union/pay/app/adopt/pay"

// 支付宝
let ALI_PAY = "AdoptFarmProductOrder/App/zfb/pay"

// 微信
let WECHAT_PAY = ""

// 取消订单
let CANCEL_PAY = "AdoptFarmProductOrder/edit"

// 身份识别
let SHENFEN_RECOGNIZE = "SysUser/app/certification"


// MARK: - 病虫害
// 病害图像识别接口
let SHIBIE_BING = "DiseaseDiscernLog/sc/detect/plant"

// 虫害图像识别接口
let SHIBIE_CHONG = "DiseaseDiscernLog/sc/detect/pest"

// 病虫害查询我的识别历史
let SHIBIE_LISHI = "DiseaseDiscernLog/app/myself"
