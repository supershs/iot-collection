//
//  SYAFNManager.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/1/28.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit
import AFNetworking
import HandyJSON

class SYAFNManager: UIView {

    class func postData(url: String, params: Dictionary<String, String>? = nil, header: [String: String], imageDatas: Array<UIImage>, successClosure: @escaping (Any) -> Void, failureClosure: @escaping (Error) -> Void) {
        
        let manager = AFHTTPSessionManager()
        manager.responseSerializer = AFJSONResponseSerializer()
        manager.responseSerializer.acceptableContentTypes = ["application/json","text/html","text/javascript","text/xml"]
        manager.post(url, parameters: params, headers: header, constructingBodyWith: { (formData) in
            
            for (_, value) in imageDatas.enumerated() {
                let imageData = value.jpegData(compressionQuality: 0.5)
                let timeStr: String = Date().sy_toString(format: "yyyy-MM-dd HH:mm:ss")
                let fileName = String(format: "%@.jpg" , timeStr)
                formData.appendPart(withFileData: imageData!, name: "file", fileName: fileName, mimeType: "image/jpg")
            }
            
        },progress: { (progress) in
            
            print("progress: \(progress)")
            
        }, success: { (task, res) in
            
            if let r = res {
                print("progress: \(r)")
                successClosure(r)
            }
            
        }) { (task, error) in
            
            print("progress: \(error)")
            failureClosure(error)
        }
    }
    
    class func postVideo(url: String, params: Dictionary<String, String>? = nil, header: [String: String], videoPath: URL, successClosure: @escaping (Any) -> Void, failureClosure: @escaping (Error) -> Void) {
        
        let manager = AFHTTPSessionManager()
        manager.responseSerializer = AFJSONResponseSerializer()
        manager.responseSerializer.acceptableContentTypes = ["application/json","text/html","text/javascript","text/xml"]
        manager.post(url, parameters: params, headers: header, constructingBodyWith: { (formData) in
            
            let timeStr: String = Date().sy_toString(format: "yyyy-MM-dd HH:mm:ss")
            let fileName = String(format: "%@.mov" , timeStr)
            do {
                let data = try Data.init(contentsOf: videoPath)
                formData.appendPart(withFileData: data, name: "file", fileName: fileName, mimeType: "video/mov")
            } catch {
                print(error)
            }
            
        },progress: { (progress) in
            
            print("progress: \(progress)")
            
        }, success: { (task, res) in
            
            if let r = res {
                print("progress: \(r)")
                successClosure(r)
            }
            
        }) { (task, error) in
            
            print("progress: \(error)")
            failureClosure(error)
        }
    }

}
