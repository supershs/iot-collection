//
//  SYWebUrlConstant.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2021/2/2.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit

// backType 区别是ios进入的web页面
var isIOS = "&backType=1"

//登录页面
var LOGINURL = "pages/login/login"


// MARK: - 首页
//供需
var SUPPLYURL = "pages/supplyAndDemand/index"

//更多资讯
var MORESECKURL = "pages/agriInformation/index"

// 检测
var NONGCHANPIN = "pages/farmProductTest/index"

// 农技耘模块
var NONGJI = "pages/agriculturalTechService/index"

//种植技术
var PLANTURL = "pages/realTimeInfo/plant/detail?id="

//产业资讯
var PRODUCTIONURL = "pages/agriInformation/childrenComponent1Detail?id="

//农机机更多
var NONGJI_MORE = "pages/farmMachinery/index"

//农机详情
var MORENONGJI_DETAIL = "pages/farmMachinery/detail/carDetail?id="

//植保机更多
var ZHIBAOJI_MORE = "pages/plantProtection/index"

//植保详情
var MOREZHIBAO_DETAIL = "pages/plantProtection/detail/plantDetail?id="

//品牌列表
var MOREBRAND_LIST = "pages/publicBrand/list"

//品牌详情
var MOREBRAND_DETAIL = "pages/publicBrand/details?id="

//专家问答列表
var MOREEXPERT_LIST = "pages/expert/index"

//专家问答详情
var MOREEXPERT_DETAIL = "pages/expert/expertDataBase/userConsultation/consultDetail?id="

//领导勿忘使命
var MOREWWSM = "pages/wisdomPartyBuilding/index"

//智慧大田
var MOREZHIHUI_DATIAN = "pages/iot/iotList?title="




// MARK: - 我的
//个人信息
var USRINFOURL = "pages/personalCenter/personalInformation/personalInformation"
//意见反馈
var FEEDBACK = "pages/personalCenter/feedback/feedback"
//设置
var SETTINGURL = "pages/personalCenter/settings/settings"
//我的钱包
var WALLETURL = "pages/cashAdvance/myWallet"

//我的积分

var INTEGRALURL = "pages/integral/integral"
//我的发布
var RELEASEURL = "pages/expert/expertCenter/index"

//我的关注
var FOLLOWURL = "pages/expert/follow/follow"

//我的问答
var MYQUESTION = "pages/personalCenter/myConsult/myConsult"


// MARK: - 工作台
//我的农机
var WORK_AGRICULTURE = "pages/farmMachinery/myCar/index/index"

//日历维护
var WORK_CALENDAR = "pages/farmMachinery/calendar/index/index"

//我的农机服务 orderStatus:  null-全部  1-待确认 2-待支付 3-待作业  4-进行中  5-待结算  6-待评价
var WORK_AGRICULTURESERVICE = "pages/farmMachinery/orderForm/index?orderStatus="

//待确认详情
var WORK_WAITCONFIRMORDERDETAILE = "pages/farmMachinery/orderForm/waitConfirm?id="

//待服务
var WORK_WAITSERVICE = "pages/farmMachinery/orderForm/waitService?id="

//进行中
var WORK_DOING = "pages/farmMachinery/orderForm/conduct?id="

//待结算4， 5，11，12, 10, 13
var WORK_WAITPAY = "pages/farmMachinery/orderForm/waitPay?id="

// 已关闭
var WORK_ALREADY_CLOSE = "pages/farmMachinery/orderForm/close?id="

//申请故障
var WORK_APPLYFAULT = "pages/farmMachinery/orderForm/conduct?id="

//  代付履约金
var WORK_WAITPAYORDERDETAILE = "pages/farmMachinery/orderForm/waitPayDeposit?id="

//气象预警
var NEW_QIXIANGYUJING = "pages/notificationInfo/weatherWarning"

// 设备预警
var NEW_SHEBEIYUJING = "pages/notificationInfo/deviceWarning"

//审核消息
var NEW_SHNEHEXIAOXI = "pages/notificationInfo/auditMessage"

//咨询消息
var NEW_SECKNEWLIST = "pages/notificationInfo/consulting"

//我的收益
var MY_SHOUYI = "pages/cashAdvance/profit"


// 趋势图
var RY_QUSHITU = "pages/adoption/index?ids="

// 消息列表 咨询
var IM_ZHIXUN = "pages/personalCenter/myConsult/myConsult"

// 病虫害识别结果
var SY_SHIBIE_RESULT = "#/pages/insectPest/recognitionResults?id="
