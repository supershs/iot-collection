//
//  SYWebApiManager.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2021/2/2.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit


public protocol WebTargetType {

    /// The target's base `URL`.
    var baseURL: String { get }

    /// The path to be appended to `baseURL` to form the full `URL`.
    var path: String { get }

}


enum SYWebApiManager {
    
    case login
    case userInfo
	case yijianfankui
    case shezhi
    case myRelease
    case mywallet
    case myintegral
    case myFollow
    case myQuestion
    case gongxu
    case gengduoZhixun
    case zhongzhiJishu(id: String)
    case chanyeZhixun(id: String, category: String)
    case myNongji
    case riliWeihu
    case myNongjiFuwu(orderStatus: String)
    case daiQuerenXiangqing(id: String)
    case daiFuwu(id: String)
    case jinxingzhong(id: String)
    case daiJiesuan(id: String)
    case shenqingGuzhang(id: String)
    case daifuLvyuejin(id: String)
    case close(id: String)
    case qixiangYujin
    case shebeiYujin
    case shenheXiaoxi
    case zhixunXiaoxi
    case myShouyi
    case nongchanpin
    case nongji
    case nongjiMore
    case zhibaojiMore
    case pinpaiMore
    case wendaMore
    case nongjiDetail(id: String)
    case zhibaoDetail(id: String)
    case pinpaiDetail(id: String)
    case wendaDetail(id: String)
    case lingdaoWuwang
    case zhihuiDatian(title: String)
    
    case qushitu(id: String, index: String)
    case im_zhixun
    case bingchongShibie(id: String)
}

extension SYWebApiManager: WebTargetType {
    
    var baseURL: String {
        
        switch self {
        case .login:
            return WEBIP
        default:
            return WEBIP
        }
    }
    
    var path: String {
        
        switch self {
        
        case .login: return LOGINURL
        
        case .userInfo: return USRINFOURL
			
		case .yijianfankui : return FEEDBACK
			
        case .shezhi: return SETTINGURL
            
        case .myRelease: return RELEASEURL
        
        case .mywallet: return WALLETURL
        
        case .myintegral: return INTEGRALURL

        case .myFollow: return FOLLOWURL
            
        case .myQuestion: return MYQUESTION
            
        case .gongxu: return SUPPLYURL
			
		case .nongchanpin: return NONGCHANPIN
             
        case .gengduoZhixun: return MORESECKURL
            
        case .zhongzhiJishu(let id): return String(format: "%@%@%@", PLANTURL, id, isIOS)
            
        case .chanyeZhixun(let id, let category): return String(format: "%@%@&category=%@", PRODUCTIONURL, id, category)
            
        case .myNongji: return WORK_AGRICULTURE
            
        case .riliWeihu: return WORK_CALENDAR
            
        case .myNongjiFuwu(let orderStatus): return String(format: "%@%@", WORK_AGRICULTURESERVICE, orderStatus)
            
        case .daiQuerenXiangqing(let id): return String(format: "%@%@", WORK_WAITCONFIRMORDERDETAILE, id)
            
        case .daiFuwu(let id): return String(format: "%@%@", WORK_WAITSERVICE, id)
            
        case .jinxingzhong(let id): return String(format: "%@%@", WORK_DOING, id)
            
        case .daiJiesuan(let id): return String(format: "%@%@", WORK_WAITPAY, id)
            
        case .shenqingGuzhang(let id): return String(format: "%@%@", WORK_APPLYFAULT, id)
        
        case .daifuLvyuejin(let id): return String(format: "%@%@", WORK_WAITPAYORDERDETAILE, id)
            
        case .close(let id): return String(format: "%@%@", WORK_ALREADY_CLOSE, id)
            
        case .qixiangYujin: return NEW_QIXIANGYUJING
            
        case .shebeiYujin: return NEW_SHEBEIYUJING
            
        case .shenheXiaoxi: return NEW_SHNEHEXIAOXI
            
        case .zhixunXiaoxi: return NEW_SECKNEWLIST
            
        case .myShouyi: return MY_SHOUYI
            
            
        
        case .nongjiMore: return NONGJI_MORE
            
        case .zhibaojiMore: return ZHIBAOJI_MORE
            
        case .pinpaiMore: return MOREBRAND_LIST
            
        case .wendaMore: return MOREEXPERT_LIST
            
        case .nongjiDetail(let id): return String(format: "%@%@%@", MORENONGJI_DETAIL, id, isIOS)
            
        case .zhibaoDetail(let id): return String(format: "%@%@%@", MOREZHIBAO_DETAIL, id, isIOS)
            
        case .pinpaiDetail(let id): return String(format: "%@%@", MOREBRAND_DETAIL, id)
            
        case .wendaDetail(let id): return String(format: "%@%@", MOREEXPERT_DETAIL, id)
        
        case .lingdaoWuwang: return MOREWWSM
        
        case .zhihuiDatian(let title): return String(format: "%@%@", MOREZHIHUI_DATIAN, title)
            
        case .qushitu(let id, let idIndex): return String(format: "%@%@&index=%@", RY_QUSHITU, id, idIndex)
            
        case .im_zhixun: return IM_ZHIXUN
    
        case .bingchongShibie(let id): return String(format: "%@%@", SY_SHIBIE_RESULT, id)
            
        case .nongji: return NONGJI
        }
    }
}
