//
//  SYApiManager.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2020/8/11.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import Foundation
import Moya
import HandyJSON
import RxSwift



let ApiManagerProvider = MoyaProvider<SYApiManager>(endpointClosure: endpointMapping, requestClosure: requestTimeoutClosure, plugins:[])

public func endpointMapping<Target: TargetType>(target: Target) -> Endpoint {
    print("\n************************************************** separate *********************************************\n\n请求链接：\(target.baseURL)\(target.path)\n方法：\(target.method.rawValue)\n请求头：\(target.headers ?? [:])\n参数：\(String(describing: target.task.self))")
    //在这里设置你的HTTP头部信息
    return MoyaProvider.defaultEndpointMapping(for: target).adding(newHTTPHeaderFields: target.headers ?? [:])
}

//设置请求超时时间
private let requestTimeoutClosure = { (endpoint: Endpoint, done: @escaping MoyaProvider<SYApiManager>.RequestResultClosure) in
    do {
        var request = try endpoint.urlRequest()
        request.timeoutInterval = 60
print(request)
        done(.success(request))
    } catch {
        return
    }
}

enum SYApiManager {
    
    case login(loginName: String, password: String)
    case loginOut(userId: String)
    
    case nongchangList(page: Int, size: Int, dic: [String: String])
    case nongchangProductList(page: Int, size: Int, dic: [String: String])
    case nongchangrenqiList(page:Int,size:Int,dic:[String: String])
    case nongchangdongtaiList
	case homepage_search
    case nongchangChoice(dic:[String: String])
    case diqu(pid: Int)
    case nongchangProductDetail(id: Int)
    case nongchangProductJiance(id: String)
    case nongchangProductBuyMaxNum(dic: [String: String])
    case nongchangProductBuy(dic: [String: String])
//    case peisongStyle(pid: Int)
    case peisongWay(dic: [String: String])
    case nongchangclass(dic:[String: String])
    case addAddress(dic: [String: String])
    case editAddress(dic: [String: String])
    case addressList(page: Int, size: Int)
    case deleteAddress(id:Int)
    
    case rymorenaddress
    case dingdanDetail(id: Int)
    case dingdanList(page: Int, size: Int, dic:[String: String])
    
    case nongyouquanList(page: Int, size: Int)
    case myList(page: Int, size: Int)
    case dianzan(dic: [String: String])
    case dianzanList(page: Int, size: Int)
    case addCommtent(dic:[String: String])
    case shenheList
    case myDetail(id: String)
    case deleteMy(id: Int)
    case release(dic: [String: String])
    case uploadImages(imgs: [UIImage])
    case uploadVideo(url: URL)
    case uploadAudio(url: URL)
    case uploadData(data: Data)
//    case downloadAudio(url: URL)
    case changeBgImg(dic: [String: String])
    case chixudingwei(dic: [String: String])
    case homepageInfo
    case homepageMenu(state: Int)
    case home_gengduo
    case home_gegduo_change(dic:[String: String])
    case homepageBanner(dic: [String: String])
    case homepageZhixun
    case homepageWeather(cityName: String)
    case myInfo
    case myGongju
    case gongzuotai
    case gongzuotaiDaiban(page: Int, size: Int)
    
    case im_huiHuaList
    case im_huiHuaDelete
    case im_huiHuaUpdateReadTime(dic:[String:String])
    case im_creatRoom(dId: String)
    case im_sendMessages(dic: [String: String])
    case im_getMessage
    
    
    case dy_firstPage
    case dy_firstPageSearch(dic: [String: String])
    case dy_firstPageZSSearch(indexs: String)
    case dy_firstPageLocation(id: String)
    case dy_jingquSearch(page: Int, size: Int, dic: [String: String])
    case dy_select
    case dy_jingquDetail(id: String)
    case dy_guanzhu(page: Int,size: Int)
    case dy_jingquDetailShoucang(id: String)
    case dy_jingquJiangjieMore(page: Int, size: Int, dic: [String: String])
    case dy_jingquJiangjieAddCount(dic: [String: String])
    case dy_jingquYoulanList(page: Int, size: Int, dic: [String: String])
    case dy_jingquZhixunList(page: Int, size: Int, dic: [String: String])
    case dy_jingquZixunAdd(mold: String, dic: [String: String])
    case dy_jingquYoulanDetail(id: String)
    case dy_jingquYuyue(dic: [String: String])
    case dy_jingquPeopleList
    case dy_jingquPeopleAdd(dic: [String: String])
    case dy_jingquPeopleInfo(id: String)
    case dy_jingquPeopleEdit(dic: [String: String])
    case dy_jingquPeopleDelete(id: String)
    case dy_jingquYuyueAdd(dic: [String: Any])
    case dy_jingquDingdanDetail(id: String)
    case dy_jingquChoiceInfo(id: String)
    
    case dy_meishiDetail(id: String)
    case dy_meishiDetailShoucang(id: String)
    case dy_meishiSearch(page: Int, size: Int, dic: [String: String])
    case dy_meishiSelect
    case dy_meishiZhixunList(page: Int, size: Int, dic: [String: String])
    case dy_meishiYudingInfo(id: String)
    case dy_meishiYudingChooseTime(dic: [String: String])
    case dy_meishiOrder(dic: [String: String])
    case dy_meishiOrderDetail(id: String)
    case dy_meishiChoiceInfo(id: String)
    
    case dy_zhusuSearch(page: Int, size: Int, dic: [String: String])
    case dy_zhusuSelect
    case dy_zhusuZhixunList(page: Int, size: Int, dic: [String: String])
    case dy_zhusuDetail(id: String)
    case dy_zhusuDetailShoucang(id: String)
    case dy_zhusuAddDingdan(dic: [String: Any])
    case dy_zhusuDingdanDetail(id: String)
    case dy_zhusuChoiceInfo(id: String)
    case dy_dingdanList(page: Int, size: Int, dic: [String: String])
    case dy_jingquDingdanCancel(dic: [String: String])
    case dy_zhusuDingdanCancel(dic: [String: String])
    case dy_dingdanDelete(dic: [String: String])
    case dy_jingquAliPay(dic: [String: String])
    case dy_zhusuAliPay(dic: [String: String])
    case dy_zhusuDetailCannotYuyue(dic: [String: String])
    case dy_meishiCancel(id: String)
    case dy_zhusuDetailRooms(dic: [String: String])
    
    case renyangshouchang(dic:[String:String])
    case my_youhuiquan
    case ry_quan(dic:[String:String])
    case my_consultation(page:Int,size:Int,dic:[String:String])
    case my_zixundetail(page:Int,size:Int,dic:[String:String])
    case my_mine
    case ry_isvideo(id:Int)
    case ry_live
    case yinlianPay(dic:[String: String])
    case aliPay(dic:[String: String])
    case wechatPay(dic:[String: String])
    case cancelPay(dic: [String: String])
    case shenfenShibie(imgs: [UIImage],dic:[String: String])
    
    case shibieBing(category:String, position: String, img: UIImage)
    case shibieChong(img: UIImage)
    case shibieLishi(page:Int,size:Int)
}

extension SYApiManager: TargetType {
    
    var headers: [String : String]? {
       //园区token临时
        let token: String = UserInstance.accessToken ?? ""
        return ["token": token, "Authorization": token]
    }
    
    var baseURL: URL {
        switch self {
        case .login:
            return URL(string: IP+VERSION)!
        case .homepageWeather(let cityName):
            // 在path中设置的url中的 ？ 会被转义，故放于此
            return  URL(string: String(format: "%@%@%@?cityName=%@", IP, VERSION, HOMEPAGE_WEATHER, cityName.getEncodeString))!
        default:
            return URL(string: IP+VERSION)!
        }
    }
    
    var path: String {
        switch self {
        case .login(_, _): return LOGIN
        case .loginOut(let userId): return "/api/\(userId)"
        case .chixudingwei(_): return CHIXUDINGWEI
        case .nongchangList(let page, let size, _): return  String(format: "%@/%ld/%ld", NONGCHANG_LIST, page, size)
        case .nongchangProductList(let page, let size,_): return String(format: "%@/%ld/%ld", NONGCHANG_PRODUCT_LIST, page, size)
        case .nongchangrenqiList(let page, let size,_): return String(format: "%@/%ld/%ld",RENYANG_HOME_LIST, page, size)
        case .nongchangdongtaiList: return RENYANG_HOME_DYNAMIC
		case .homepage_search: return HOMEPAGE_SEARCH
        case .nongchangChoice(_): return String(format: "%@", PEISONG_WAY)
        case .diqu( _): return DIQU
        case .nongchangProductDetail(let id): return String(format: "%@/%ld", NONGCHANG_PRODUCT_DETAIL, id)
        case .nongchangProductJiance(let id): return String(format: "%@/%@", NONGCHANG_PRODUCT_JIANCE, id)
        case .nongchangProductBuyMaxNum(_): return NONGCHANG_PRODUCT_BUY_MAXNUM
        case .nongchangProductBuy(_): return NONGCHANG_PRODUCT_BUY
//        case .peisongStyle(let pid): return String(format: "%@/%ld", PEISONG_STYLE, pid)
        case .peisongWay(_): return  PEISONG_WAY
        case .nongchangclass(_): return NONGCHANG_TYP_SEC
        case .addAddress(_): return    ADD_ADDRESS
        case .editAddress(_): return    EDIT_ADDRESS
        case .addressList(let page, let size): return  String(format: "%@/%ld/%ld", ADDRESS_LIST, page, size)
        case .deleteAddress(let id) : return String(format: "%@/%ld", DELETE_ADDRESS, id)
        
        case .dingdanDetail(let id): return String(format: "%@/%ld", DINGDAN_DETAIL, id)
        case .dingdanList(let page, let size, _): return  String(format: "%@/%ld/%ld", DINGDAN_LIST, page, size)
            
        case .nongyouquanList(let page, let size) : return  String(format: "%@/%ld/%ld", NONGYOUQUAN_LIST, page, size)
        case .myList(let page, let size) : return  String(format: "%@/%ld/%ld", MY_LIST, page, size)
        case .dianzan(_) : return DIANZAN
        case .addCommtent(_) : return ADD_COMMENT
        case .dianzanList(let page, let size) : return  String(format: "%@/%ld/%ld", DIANZAN_LIST, page, size)
        case .shenheList : return  SHENHE_LIST
        case .myDetail(let id): return String(format: "%@/%@", MY_SINGLE, id)
        case .deleteMy(let id): return String(format: "%@/%ld", DELETE_MY, id)
        case .release(_) : return RELEASE
        case .uploadImages(_) : return UPLOAD_IMAGES
        case .uploadVideo(_) : return UPLOAD_IMAGES
        case .uploadAudio(_) : return UPLOAD_IMAGES
        case .uploadData(_) : return UPLOAD_IMAGES
        case .changeBgImg(_) : return CHANGE_BG_IMAGE
            
        case .homepageInfo: return HOMEPAGE_INFO
        case .homepageMenu(let state): return  String(format: "%@/%ld", HOMEPAGE_MENU, state)
        case .home_gengduo: return SYMORE_LIST
        case .home_gegduo_change(_): return SYMODEL_CHANGE
        case .homepageBanner(_): return  HOMEPAGE_BANNER
            
        case .homepageZhixun: return HOMEPAGE_ZHIXUN
        case .homepageWeather(_): return ""
        case .myInfo: return USER_INFO
        case .myGongju: return GONGJU
        case .im_huiHuaList: return IM_LIST
        case .im_huiHuaDelete: return IM_DELETE
        case .im_huiHuaUpdateReadTime: return IM_UPDATE_READ_TIME
        case .im_creatRoom(let dId): return String(format: "%@/%@", IM_CREATE_ROOM, dId)
        case .im_sendMessages(_): return  IM_SEND_MESSAGE
        case .im_getMessage: return IM_GET_MESSAGES
        case .gongzuotai: return GONGZUOTAI
        case .gongzuotaiDaiban(let page, let size) : return  String(format: "%@/%ld/%ld", GONGZUOTAI_DAIBAN, page, size)
            
        case .dy_firstPage: return DY_FIRST_PAGE
        case .dy_firstPageSearch: return DY_CHOICE_INFO
        case .dy_firstPageZSSearch(let indexs): return String(format: "%@/%@", DY_CHOICE_INFO, indexs)
        case .dy_firstPageLocation(let id): return  String(format: "%@/%@", DY_FIRST_PAGE_LOCATION, id)
        case .dy_jingquSearch(let page, let size, _): return  String(format: "%@/%ld/%ld", DY_JINGQU_SEARCH, page, size)
        case .dy_select: return DY_JINGQU_SELECT
        case .dy_jingquDetail(let id): return  String(format: "%@/%@", DY_JINGQU_DETAIL, id)
        case .dy_guanzhu(let page, let size): return  String(format: "%@/%ld/%ld", DY_MYGUANGZHU, page, size)
        case .dy_jingquDetailShoucang(let id): return String(format: "%@/%@", DY_JINGQU_DETAIL_SHOUCANG, id)
        case .dy_jingquJiangjieMore(let page, let size, _): return  String(format: "%@/%ld/%ld", DY_JINGQU_JIANGJIE_MORE, page, size)
        case .dy_jingquJiangjieAddCount(_): return DY_JINGQU_JIANGJIE_ADD_COUNT
        case .dy_jingquYoulanList(let page, let size, _): return  String(format: "%@/%ld/%ld", DY_JINGQU_YOULAN_LIST, page, size)
        case .dy_jingquZhixunList(let page, let size, _): return  String(format: "%@/%ld/%ld", DY_JINGQU_ZIXUN_LIST, page, size)
        case .dy_jingquZixunAdd(let mold, _): return  String(format: "%@/%@", DY_JINGQU_ZIXUN_ADD, mold)
        case .dy_jingquYoulanDetail(let id): return  String(format: "%@/%@", DY_JINGQU_YOULAN_DETAIL, id)
        case .dy_jingquYuyue(_): return DY_JINGQU_YUYUE
        case .dy_jingquPeopleList: return DY_JINGQU_PEOPLES
        case .dy_jingquPeopleAdd(_): return DY_JINGQU_PEOPLE_ADD
        case .dy_jingquPeopleInfo(let id): return String(format: "%@/%@", DY_JINGQU_PEOPLE_DETAIL, id)
        case .dy_jingquPeopleEdit(_): return DY_JINGQU_PEOPLE_EDIT
        case .dy_jingquPeopleDelete(let id): return  String(format: "%@/%@", DY_JINGQU_PEOPLE_DELETE, id)
        case .dy_jingquYuyueAdd(_): return DY_JINGQU_ADD
        case .dy_jingquDingdanDetail(let id): return  String(format: "%@/%@", DY_JINGQU_DINGDAN_DELETE, id)
        case .dy_jingquChoiceInfo(let id):  return String(format: "%@/%@", DY_JINGQU_CHOICE_INFO, id)
        case .dy_meishiCancel(let id): return String(format: "%@/%@", DY_MEISHI_CANCEL, id)
            
        case .dy_meishiDetail(let id):  return String(format: "%@/%@", DY_MEISHI_DETAIL, id)
        case .dy_meishiDetailShoucang(let id):  return String(format: "%@/%@", DY_MEISHI_DETAIL_SHOUCANG, id)
            
        case .dy_meishiSearch(let page, let size, _): return  String(format: "%@/%ld/%ld", DY_MEISHI_LIST, page, size)
        case .dy_meishiSelect: return DY_MEISHI_SELECT
        case .dy_meishiZhixunList(let page, let size, _): return  String(format: "%@/%ld/%ld", DY_MEISHI_ZIXUN_LIST, page, size)
        case .dy_meishiYudingInfo(let id): return  String(format: "%@/%@", DY_MEISHI_YUDING_CHOOSE, id)
        case .dy_meishiYudingChooseTime(_): return  DY_MEISHI_YUDING_CHOOSE_TIME
        case .dy_meishiOrder(_): return DY_MEISHI_ORDER
        case .dy_meishiOrderDetail(let id):  return String(format: "%@/%@", DY_MEISHI_ORDER_DETAIL, id)
        case .dy_meishiChoiceInfo(let id):  return String(format: "%@/%@", DY_MEISHI_CHOICE_INFO, id)

            
        case .dy_zhusuSearch(let page, let size, _): return  String(format: "%@/%ld/%ld", DY_ZHUSU_LIST, page, size)
        case .dy_zhusuSelect: return DY_ZHUSU_SELECT
        case .dy_zhusuZhixunList(let page, let size, _): return  String(format: "%@/%ld/%ld", DY_ZHUSU_ZIXUN_LIST, page, size)
        case .dy_zhusuDetail(let id): return String(format: "%@/%@", DY_ZHUSU_DETAIL, id)
        case .dy_zhusuDetailShoucang(let id):  return String(format: "%@/%@", DY_ZHUSU_DETAIL_SHOUCANG, id)
        case .dy_zhusuAddDingdan(_): return DY_ZHUSU_ADD_DINGDAN
        case .dy_zhusuDingdanDetail(let id):  return String(format: "%@/%@", DY_ZHUSU_DINGDAN_DETAIL, id)
        case .dy_zhusuChoiceInfo(let id):  return String(format: "%@/%@", DY_ZHUSU_CHOICE_INFO, id)
        case .dy_dingdanList(let page, let size, _): return  String(format: "%@/%ld/%ld", DY_DINGDANLIST, page, size)
        case .dy_jingquDingdanCancel(_):  return DY_JINGQU_DINGDAN_CANCEL
        case .dy_zhusuDingdanCancel(_): return DY_ZHUSU_DINGDAN_CANCEL
        case .dy_dingdanDelete(_): return DY_DINGDAN_DELETE
        case .dy_jingquAliPay(_): return DY_JINGQU_ALI_PAY
        case .dy_zhusuAliPay(_): return DY_ZHUSU_ALI_PAY
        case .dy_zhusuDetailCannotYuyue(_): return DY_ZHUSU_DETAIL_CAN_NOT_YUYUE
        case .dy_zhusuDetailRooms(_): return DY_ZHUSU_DETAIL_ROOMS

        case .renyangshouchang(_): return RENYANG_SHOUCANG
        case .rymorenaddress: return RYMORENADDRESS
        case .my_youhuiquan: return  MYYOUHUIQUAN
        case .ry_quan( _): return  RY_QUAN
        case .my_mine: return RY_MINE
        case .my_consultation(let page, let size, _): return String(format: "%@/%ld/%ld", MY_CONSULTATION,page,size)
        case .my_zixundetail(let page, let size, _): return String(format: "%@/%ld/%ld", MY_ZiXunDetail,page,size)

        case .ry_isvideo(let id):  return String(format: "%@/%ld", RENYANG_ISVIDEO, id)
        case .ry_live: return RY_LIVE
        case .yinlianPay(_): return YINLIAN_PAY
        case .aliPay(_): return ALI_PAY
        case .wechatPay(_): return WECHAT_PAY
        case .cancelPay(_): return CANCEL_PAY
        case .shenfenShibie(_,_): return SHENFEN_RECOGNIZE
            
        case .shibieBing(let category, let position, _): return String(format: "%@/%@/%@", SHIBIE_BING,category,position)
        case .shibieChong(_): return SHIBIE_CHONG
        case .shibieLishi(let page, let size): return String(format: "%@/%ld/%ld", SHIBIE_LISHI,page,size)
        }
    }
    
    var method: Moya.Method {
        switch self {

        case .login, .nongchangList, .nongchangProductList, .diqu, .nongchangProductBuyMaxNum, .nongchangProductBuy, .addAddress, .addressList, .editAddress, .deleteAddress, .dingdanList,.addCommtent,.dianzan, .release, .uploadImages, .uploadVideo, .uploadAudio, .uploadData, .gongzuotaiDaiban, .dy_jingquSearch, .dy_jingquJiangjieMore, .dy_jingquJiangjieAddCount, .dy_jingquYoulanList, .dy_jingquZhixunList, .dy_jingquZixunAdd, .dy_jingquYuyue, .dy_jingquPeopleAdd, .dy_jingquYuyueAdd, .dy_meishiSearch, .dy_zhusuSearch, .dy_meishiZhixunList, .dy_zhusuZhixunList, .dy_meishiYudingChooseTime, .dy_meishiOrder, .dy_zhusuAddDingdan, .im_sendMessages, .changeBgImg,.renyangshouchang,.ry_quan, .yinlianPay, .aliPay, .wechatPay, .cancelPay, .dy_dingdanList, .dy_jingquAliPay, .dy_zhusuAliPay,.im_huiHuaUpdateReadTime,.my_consultation,.chixudingwei,.peisongWay,.nongchangChoice,.nongchangclass, .dy_firstPageSearch, .my_zixundetail, .dy_zhusuDetailRooms,.dy_zhusuDetailCannotYuyue, .homepageBanner,.home_gegduo_change, .shenfenShibie, .nongchangrenqiList, .shibieBing, .shibieChong, .shibieLishi:
            return .post
        case .dy_jingquPeopleEdit, .dy_jingquDingdanCancel, .dy_dingdanDelete, .dy_zhusuDingdanCancel, .dy_meishiCancel:
            return .put
        case .deleteMy, .dy_jingquPeopleDelete:
            return .delete
        default:
            return .get
        }
    }
    
    var task: Task {
        switch self {
        case .login(let loginName, let password):
            return .requestParameters(parameters: ["loginName": loginName, "password": password.md5], encoding: JSONEncoding.default)
            // chao "13915999715"
        // 蜘蛛侠 13382336993
        // 钢铁侠 13382336992
//            return .requestParameters(parameters: ["loginName": "13382336992", "password": "123456"], encoding: JSONEncoding.default) // post请求
        //            return .requestParameters(parameters: ["memberName": "zhaozhiyun", "password": "123456"], encoding: URLEncoding.default) // post请求
        
        //        case .getReward(let type, let cursor, let limit):
        //            return .requestParameters(parameters: ["type": type], encoding: URLEncoding.default) // 其它请求
 
        case .chixudingwei(let dic): return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .yinlianPay(let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .aliPay(let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .wechatPay(let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .cancelPay(let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .homepageBanner(let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
            
        case .home_gegduo_change(let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
            
        case .nongchangProductList( _, _, let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .diqu(let pid):
            return .requestParameters(parameters: ["pid":pid], encoding: JSONEncoding.default)
        case .nongchangProductBuyMaxNum(let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .nongchangProductBuy(let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .addAddress(let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .editAddress(let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .addressList(_,_):
            return .requestParameters(parameters: [:], encoding: JSONEncoding.default) // 该接口必须传空对象
        case .dingdanList(_, _, let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .im_huiHuaUpdateReadTime(let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .dy_zhusuAddDingdan(let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .dianzan(let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .addCommtent(let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .release(let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .im_sendMessages(let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .changeBgImg(let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
            
        case .gongzuotaiDaiban(_, _):
            return .requestParameters(parameters: ["sort": "10"], encoding: JSONEncoding.default)
            
        case .nongchangList(_, _, let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .dy_firstPageSearch(let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .dy_jingquSearch(_, _, let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .dy_jingquJiangjieMore(_, _, let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .dy_jingquYoulanList(_, _, let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .dy_jingquZhixunList(_, _, let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .dy_jingquZixunAdd(_ , let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
            
        case .dy_jingquYuyue(let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .dy_jingquPeopleAdd(let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .dy_jingquPeopleEdit(let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .dy_jingquYuyueAdd(let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
            
        case .dy_meishiSearch(_, _, let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .dy_meishiZhixunList(_, _, let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .dy_meishiYudingChooseTime(let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .dy_meishiOrder(let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
            
        case .dy_zhusuSearch(_, _, let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .dy_zhusuZhixunList(_, _, let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .dy_dingdanList(_, _, let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .dy_jingquDingdanCancel(let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .dy_zhusuDingdanCancel(let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
            
        case .dy_dingdanDelete(let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .dy_jingquAliPay(let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .dy_zhusuAliPay(let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
            
        case .dy_jingquJiangjieAddCount(let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .my_zixundetail(_, _,let dic): return.requestParameters(parameters:dic, encoding: JSONEncoding.default)
        case .dy_zhusuDetailCannotYuyue(let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .dy_zhusuDetailRooms(let dic):
            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
//        case .shenfenShibie(let dic):
//            return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
            
        case .nongchangrenqiList(_, _,let dic): return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .renyangshouchang(let dic): return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .nongchangChoice(let dic): return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .peisongWay(let dic): return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .nongchangclass(let dic): return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .ry_quan(let dic): return .requestParameters(parameters: dic, encoding: JSONEncoding.default)
        case .my_consultation(_, _,let dic): return.requestParameters(parameters:dic, encoding: JSONEncoding.default)
            
        case .shibieBing(_, _, let img):
            return .uploadMultipart(getImagesData(imgs: [img]))
            
        case .shibieChong(let img):
            return .uploadMultipart(getImagesData(imgs: [img]))
        case .shibieLishi(_,_):
            return .requestParameters(parameters: [:], encoding: JSONEncoding.default)
            
        case .uploadImages(let imgs):
            return .uploadMultipart(getImagesData(imgs: imgs))
        case .uploadVideo(let url):
            let now = Date()
            var dateStr: String = now.sy_toString(format: "yyyy-MM-dd HH:mm:ss")
            dateStr = dateStr.appendingFormat("-%i.mov", 0)
            return .uploadMultipart([MultipartFormData(provider: .file(url), name: "file", fileName: dateStr, mimeType: "video/mov")])
        case .uploadAudio(let url):
            let now = Date()
            var dateStr: String = now.sy_toString(format: "yyyy-MM-dd HH:mm:ss")
            dateStr = dateStr.appendingFormat("-%i.amr", 0)
            return .uploadMultipart([MultipartFormData(provider: .file(url), name: "file", fileName: dateStr, mimeType: "audio/amr")])
        case .uploadData(let data):
            return .uploadMultipart([MultipartFormData(provider: .data(data), name: "data")])

//        case .shenfenShibie(let imgs, let dic): return .requestParameters(parameters:dic, encoding: JSONEncoding.default)
//
            
        case .shenfenShibie(let imgs, let dic):
            return .uploadCompositeMultipart((getImagesData(imgs: imgs)), urlParameters: dic)
//        case .downloadAudio(let url):
//            return .downloadDestination { (url, res) -> (destinationURL: URL, options: DownloadRequest.Options) in
//
//            }
        default:
            return .requestPlain
        }
    }
    
    var validate: Bool {
        return false
    }
    var sampleData: Data {
        return "".data(using: String.Encoding.utf8)!
    }
    
    func getImagesData(imgs: [UIImage]) -> [MultipartFormData] {

        var formDataAry:[MultipartFormData] = [MultipartFormData]()
        for (index,image) in imgs.enumerated() {
            //图片转成Data
            let data:Data = image.jpegData(compressionQuality: 0.7)!
            //根据当前时间设置图片上传时候的名字
            let now = Date()
            var dateStr: String = now.sy_toString(format: "yyyy-MM-dd HH:mm:ss")
            //别忘记这里给名字加上图片的后缀哦
            dateStr = dateStr.appendingFormat("-%i.jpeg", index)
            // MARK: - 对应服务端，这里的name必须为 "file"，fileName和mineType必须有值，headers只需要token，不然都会报错，提示未上传文件
            let formData: MultipartFormData = MultipartFormData(provider: .data(data), name: "file", fileName: dateStr, mimeType: "image/jpeg")
            formDataAry.append(formData)
        }
      
        return formDataAry
    }
    
    
    
    
    func getImagesDataID(imgs: [UIImage], trueName: NSString, idCard: NSString,address: NSString) -> [MultipartFormData] {

        var formDataAry:[MultipartFormData] = [MultipartFormData]()
        for (index,image) in imgs.enumerated() {
            //图片转成Data
            let data:Data = image.jpegData(compressionQuality: 0.7)!
            //根据当前时间设置图片上传时候的名字
            let now = Date()
            var dateStr: String = now.sy_toString(format: "yyyy-MM-dd HH:mm:ss")
            //别忘记这里给名字加上图片的后缀哦
            dateStr = dateStr.appendingFormat("-%i.jpeg", index)
            // MARK: - 对应服务端，这里的name必须为 "file"，fileName和mineType必须有值，headers只需要token，不然都会报错，提示未上传文件
            
            let trueData = String(trueName).data(using: String.Encoding.utf8, allowLossyConversion: true)
            let idCardData = String(idCard).data(using: String.Encoding.utf8, allowLossyConversion: true)
            let addressData = String(address).data(using: String.Encoding.utf8, allowLossyConversion: true)

            let formData: MultipartFormData = MultipartFormData(provider: .data(data), name: "file", fileName: dateStr, mimeType: "image/jpeg")
            
            formDataAry.append(formData)
        }
      
        return formDataAry
    }
}



// MARK: 取消所有请求
func cancelAllRequest() {
    //    ApiManagerProvider.manager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
    //        dataTasks.forEach { $0.cancel() }
    //        uploadTasks.forEach { $0.cancel() }
    //        downloadTasks.forEach { $0.cancel() }
    //    }
    //
    //    ApiManagerProvider.manager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
    //        dataTasks.forEach { $0.cancel() }
    //        uploadTasks.forEach { $0.cancel() }
    //        downloadTasks.forEach { $0.cancel() }
    //    }
}


