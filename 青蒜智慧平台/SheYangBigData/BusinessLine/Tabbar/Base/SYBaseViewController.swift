//
//  SYBaseViewController.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2020/8/21.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit
import RxSwift
import MJRefresh
import SwiftDate

class SYBaseViewController: UIViewController {

    public var backClosure: ((Int) -> Void)?
    public var passParamsClosure: ((Any) -> Void)?
    let dispose = DisposeBag()
    let backItem: UIBarButtonItem = UIBarButtonItem()
    
    var requestVM: RYNongchangListVM = RYNongchangListVM()
    var hideNav: Bool = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = hideNav
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.navigationController?.navigationBar.isHidden = false
        self.tabBarController?.tabBar.isHidden = true
        
        let image: UIImage = UIImage(named: "back")!
//        image.renderingMode = .alwaysOriginal
        self.navigationController?.navigationBar.backIndicatorImage = image
//        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = image
        self.navigationItem.backBarButtonItem = backItem
    }
    
    func toShouye(_ animated: Bool = true) {
        self.navigationController?.tabBarController?.hidesBottomBarWhenPushed = false
        self.navigationController?.tabBarController?.selectedIndex = 0 // (第一个控制器)
        self.navigationController?.popToRootViewController(animated: animated)
    }
    
    func popVC(_ animated: Bool = true) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
        self.backClosure?(0)
    }
    
    func pushVC(_ vc: UIViewController) {
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func sy_toast(_ message: String, completion: ((_ didTap: Bool) -> Void)? = nil) {
        
        self.view.makeToast(message, duration: 1.5, position: .center, completion: completion)
    }
    func removeVC(_ vcClass: AnyClass) {
        
        self.navigationController?.children.forEach({ vc in
            if vc.isKind(of: vcClass) {
                self.navigationController?.viewControllers.removeValue(vc)
            }
        })
    }
    
    func sy_pushWebVC(_ webApi: SYWebApiManager) {
        let path = String(format: "%@%@", webApi.baseURL, webApi.path)
        print("\n************************************************** separate *********************************************\n\n webpath: \(path)\n")
        let webvc = HSWebViewController(path: path)
        self.pushVC(webvc)
    }
    
    func sy_pushWebVC(_ webApi: SYWebApiManager, backClosure:((Any)-> Void)?) {
        let path = String(format: "%@%@", webApi.baseURL, webApi.path)
        print("\n************************************************** separate *********************************************\n\n webpath: \(path)\n")
        let webvc = HSWebViewController(path: path)
        webvc.backClosure = {[weak self] value in
            if let `self` = self {
                backClosure?(value)
            }
        }
        self.pushVC(webvc)
    }
    
    func sy_pushWebVC(_ url: String) {
        let path = String(format: "%@%@", WEBIP, url)
        print("\n************************************************** separate *********************************************\n\n webpath: \(path)\n")
        let webvc = HSWebViewController(path: path)
        self.pushVC(webvc)
    }
    
    func backSnp(_ back: UIView) {
        back.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.top.equalToSuperview().offset(STATUSBAR_HEIGHT)
            make.height.equalTo(48.autoWidth())
            make.width.equalTo(30.autoWidth())
        }
    }
    
    func isTodayOrTomorrow(_ date:Date) -> String {

        if date.isToday {
            return "今天"
        } else if date.isTomorrow {
            return "明天"
        } else {
            return ""
        }
    }
    
    func toLoginVC() {
        let loginVC = HSWebViewController(path: kWebUrl, isLoginPage: true)
        self.vg_present(loginVC, animated: true)
    }
    
    func addToolBar() -> UIToolbar {
        let toolBar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 35))
        toolBar.backgroundColor = UIColor.gray
        let spaceBtn = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let barBtn = UIBarButtonItem(title: "完成", style: .plain, target: self, action: #selector(doneNum))
        toolBar.items = [spaceBtn, barBtn]
        toolBar.sizeToFit()
        return toolBar
    }

    @objc func doneNum() {
        self.view.endEditing(false)
    }
    
    public func topMangain(_ tableView: UITableView) {
        // 解决Webview未覆盖顶部状态栏
        if #available(iOS 11.0, *) {
            tableView.contentInsetAdjustmentBehavior = UIScrollView.ContentInsetAdjustmentBehavior.never
        } else {
            self.edgesForExtendedLayout = UIRectEdge.bottom
        }
    }
}

class SYBaseTableViewController: SYBaseViewController {
    
    var page = 1
    var size = 10
    var noDataView = SYNoDataView()
    
    var tableView:UITableView!
    
    var isStatusBarHidden = false {
        didSet {
            self.setNeedsStatusBarAppearanceUpdate()
        }
    }
    override var prefersStatusBarHidden: Bool {
        get {
            return isStatusBarHidden
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
}
