//
//  SYBaseTarBarViewController.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2020/8/21.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit
import SwiftDate
import RxSwift

//class SYBaseTarBarViewController: UITabBarController {
//
//	var requestVM: RYNongchangListVM = RYNongchangListVM()
//	var passParamsClosure: ((Any) -> Void)?
//	var dispose = DisposeBag()
//	var roleId: String?
//	var lastItem: UITabBarItem!
//	var lastTime: Date!
//	var childVcCount: Int = 0
//
//	var homeVC: SYHomePageViewController!
//
////	var renyangVC: RYNongchangListViewController!
////	var nongyouquanVC: PYQListViewController!
////	var daoyouVC: DYDaoLanViewController!
//	var xiaoxiVC: SYIMlistViewController!
//	var moreVC: SYGengduoViewController!
//
//	var myVC: SYMyViewController!
//	var gongzuotaiVC: SYGongzuotaiViewController!
//
//	var zhantingVC: HSWebViewController = HSWebViewController(path: WEBIP + MORESECKURL, hasTab: true)
//	var wulianVC: HSWebViewController = HSWebViewController(path: WEBIP + MORESECKURL, hasTab: true)
//	var dangjianVC: HSWebViewController = HSWebViewController(path: WEBIP + MORESECKURL, hasTab: true)
//
//
//	override func viewWillAppear(_ animated: Bool) {
//		super.viewWillAppear(animated)
//		self.navigationController?.navigationBar.isHidden = true
//	}
//
//	override func viewDidDisappear(_ animated: Bool) {
//		super.viewDidDisappear(animated)
//		self.navigationController?.navigationBar.isHidden = false
//	}
//
//	init(roleId: String?) {
//		super.init(nibName: nil, bundle: nil)
//		self.roleId = roleId
//		setChildVC()
//
//		//        NotificationCenter.default.post(name: .Noti_getUserInfo, object: nil, userInfo: nil)
//		NotificationCenter.default.addObserver(self, selector: #selector(refreshData), name: .Noti_changeRole, object: nil)
//		//        requestUserInfo()
//	}
//
//	required init?(coder: NSCoder) {
//		fatalError("init(coder:) has not been implemented")
//	}
//
//	override func viewDidLoad() {
//		super.viewDidLoad()
//		self.view.backgroundColor = .white
//		tabBar.backgroundColor = .white
//		setupShadow()
//		//        self.middleBtnClick = { [weak self] select in
//		//            if let `self` = self {
//		//                self.selectedIndex = 2
//		//            }
//		//            print("middleBtnClick")
//		//        }
//	}
//
//	func setChildVC() {
//		//        refreshChildVC()
//		homeVC = SYHomePageViewController()
//		addChildVC(childVC: homeVC, title: "首页", image: "tab_homepage",index: 0)
//		xiaoxiVC = SYIMlistViewController(hasTab: true)
//		   addChildVC(childVC: xiaoxiVC, title: "消息", image: "tab_news",index: 1)
//		myVC = SYMyViewController()
//		addChildVC(childVC: myVC, title: "我的", image: "tab_my",index: 2)
//		//        setLayer()
//	}
//	//    func refreshChildVC() {
//	//
//	//        homeVC = SYHomePageViewController()
//	//
//	//        renyangVC = RYNongchangListViewController(hasTab: true)
//	//        nongyouquanVC = PYQListViewController(hasTab: true)
//	//        daoyouVC = DYDaoLanViewController(hasTab: true)
//	//        xiaoxiVC = SYIMlistViewController(hasTab: true)
//	//        moreVC = SYGengduoViewController(hasTab: true)
//	//
//	//        gongzuotaiVC = SYGongzuotaiViewController()
//	//
//	//        addChildVC(childVC: homeVC, title: "首页", image: "tab_homepage",index: 0)
//	////        addChildVC(childVC: renyangVC, title: "认养农业", image: "tab_nongyouquan",index: 1)
//	////        addChildVC(childVC: moreVC, title: "", image: "",index: 2)
//	//        addChildVC(childVC: xiaoxiVC, title: "消息", image: "tab_news",index: 3)
//	//        childVcCount = 1
//	//        /*
//	//        // 1 农户 2农机手 4飞手 5农业专家 6订单农业服务商 7领导
//	//        addChildVC(childVC: homeVC, title: "首页", image: "tab_homepage",index: 0)
//	//        if let id = roleId, id != "null" {
//	//            if id == "2" || id == "4" {
//	//                addChildVC(childVC: gongzuotaiVC, title: "工作台", image: "gongzuotai",index: 1)
//	//                addChildVC(childVC: xiaoxiVC, title: "消息", image: "tab_news",index: 2)
//	//                childVcCount = 2
//	//            } else if id == "8" {
//	//                addChildVC(childVC: zhantingVC, title: "网上展厅", image: "zhanting",index: 1)
//	//                addChildVC(childVC: wulianVC, title: "物联网", image: "wulian",index: 2)
//	//                addChildVC(childVC: dangjianVC, title: "智慧党建", image: "dangjian",index: 3)
//	//            } else {
//	//                addChildVC(childVC: renyangVC, title: "认养农业", image: "tab_renyang",index: 1)
//	//                addChildVC(childVC: nongyouquanVC, title: "农友圈", image: "tab_nongyouquan",index: 2)
//	//                addChildVC(childVC: daoyouVC, title: "导游导览", image: "tab_daoyou",index: 3)
//	//                childVcCount = 3
//	//            }
//	//        } else {
//	//            addChildVC(childVC: renyangVC, title: "认养农业", image: "tab_renyang",index: 1)
//	//            addChildVC(childVC: nongyouquanVC, title: "农友圈", image: "tab_nongyouquan",index: 2)
//	//            addChildVC(childVC: daoyouVC, title: "导游导览", image: "tab_daoyou",index: 3)
//	//            childVcCount = 3
//	//        }
//	// */
//	//    }
//
//	func addChildVC(childVC:UIViewController,title:String,image:String,index: Int) -> Void {
//		childVC.title = title
//
//		var img = UIImage(named: image)
//		img = img?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
//
//		var selectedImg = UIImage(named: image + "_select")
//		selectedImg = selectedImg?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
//
//		childVC.tabBarItem.image = img
//		childVC.tabBarItem.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : Constant.greenColor], for: .highlighted)
//		childVC.tabBarItem.selectedImage = selectedImg
//		let nav = SYBaseNavigationController(rootViewController: childVC)
//
//		if (self.viewControllers?.count ?? 0) > index {
//			self.viewControllers?[index] = nav
//		} else {
//			addChild(nav)
//		}
//	}
//
//	override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
//		if self.selectedIndex == 0, item == self.lastItem, Date() < self.lastTime + 1.seconds {
//			homeVC.toTop()
//		} else {
//			self.lastItem = item
//			self.lastTime = Date()
//		}
//	}
//
//	@objc func refreshData() {
////		refreshChildVC()
//		//        homeVC.refreshData()
//		//        if let id = roleId, id != "null" {
//		//            if id == "2" || id == "4" {
//		//                gongzuotaiVC.refreshData()
//		//                xiaoxiVC.refreshData()
//		//            } else if id == "8" {
//		//                zhantingVC.refreshData()
//		//                wulianVC.refreshData()
//		//                dangjianVC.refreshData()
//		//            } else {
//		//                renyangVC.refreshData()
//		//                nongyouquanVC.refreshData()
//		//                daoyouVC.refreshData()
//		//            }
//		//        } else {
//		//            renyangVC.refreshData()
//		//            nongyouquanVC.refreshData()
//		//            daoyouVC.refreshData()
//		//        }
//		myVC.refreshData()
//	}
//
//	func requestUserInfo() {
//		requestVM.baseRequest(disposeBag: dispose, type: .myInfo, modelClass: SGBaseModel<SYMyInfoModel>.self) {[weak self] (res) in
//			if let `self` = self {
//				let infoModel = res.data
//				UserInstance.userId = infoModel?.id
//				UserInstance.avatar = infoModel?.headImg
//				UserInstance.nickname = infoModel?.nickName
//			}
//
//		} Error: {
//
//		}
//	}
//
//	fileprivate func setLayer() {
//		let bezier = UIBezierPath()
//		bezier.move(to: CGPoint.zero)
//		bezier.addLine(to: CGPoint(x: SCREEN_WIDTH * 0.5 - 22, y: 0))
//
//		bezier.append(UIBezierPath(arcCenter: CGPoint(x: SCREEN_WIDTH * 0.5, y: 5), radius: 23, startAngle: CGFloat.pi * -1/20, endAngle: CGFloat.pi * -19/20, clockwise: false))
//
//		bezier.move(to: CGPoint(x: SCREEN_WIDTH * 0.5 + 22, y: 0))
//		bezier.addLine(to: CGPoint(x: SCREEN_WIDTH, y: 0))
//		let shapeLayer = CAShapeLayer()
//		shapeLayer.path = bezier.cgPath
//		shapeLayer.lineWidth = 1
//		shapeLayer.fillColor = UIColor.white.cgColor
//		shapeLayer.strokeColor = Constant.bgViewColor.cgColor
//		tabBar.backgroundColor = .white
//		tabBar.layer.insertSublayer(shapeLayer, at: 0)
//
//		tabBar.shadowImage = UIImage()
//		tabBar.backgroundImage = UIImage()
//	}
//
//	func setupShadow() {
//		//移除顶部线条
//		self.tabBar.backgroundImage = UIImage();
//		self.tabBar.shadowImage = UIImage();
//
//		//添加阴影
//		self.tabBar.layer.shadowColor = UIColor.lightGray.cgColor;
//		self.tabBar.layer.shadowOffset = CGSize(width: 0, height: -5);
//		self.tabBar.layer.shadowOpacity = 0.3;
//	}
//
//
//}

//extension SYBaseTarBarViewController: NIMConversationManagerDelegate {
//
//}
