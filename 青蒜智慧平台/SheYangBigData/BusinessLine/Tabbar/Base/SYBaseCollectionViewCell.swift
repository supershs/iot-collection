//
//  SYBaseCollectionViewCell.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/1/4.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit

class SYBaseCollectionViewCell: UICollectionViewCell {
    
    
    public var clickedClosure: ((Int) -> Void)?
    override init(frame: CGRect) {
        super.init(frame: frame)

        initViews()
    }
    
 
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func initViews() {

    }
    
    func setPriceLb(_ price: String?) -> NSMutableAttributedString {
        let d: Double = Double(price ?? "0") ?? 0.0
        let priceStr = String(format: "%.2f", d)
        let firStr = "￥"
        let totalStr = "￥\(priceStr)"
        let attString = NSMutableAttributedString(string: totalStr)
        attString.yy_setColor(UIColor(hex: 0xF55035), range: NSRange(location: 0, length: firStr.count))
        attString.yy_setFont(13.autoFontSize(), range: NSRange(location: 0, length:firStr.count))
        attString.yy_setColor(UIColor(hex: 0xF55035), range: NSRange(location: firStr.count, length: totalStr.count - firStr.count))
        attString.yy_setFont(16.autoFontSize(), range: NSRange(location: firStr.count, length: totalStr.count - firStr.count))
        return attString
    }
}
