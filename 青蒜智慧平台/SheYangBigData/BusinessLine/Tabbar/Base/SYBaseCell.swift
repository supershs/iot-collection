//
//  SYBaseCell.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2020/8/26.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit
import RxSwift

class SYBaseCell: UITableViewCell {
    
    let disposeBag = DisposeBag()
    
    weak var currentVC: SYBaseViewController!
    
    public var clickedClosure: ((Int) -> Void)?
    
    public var passParamsClosure: ((Any) -> Void)?
    
    public var lineView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = UIColor(hex: 0xE6E6E6)
        return v
    }()
    
    let inputTf: UITextField = {
        let v:UITextField = UITextField()
        v.textColor = UIColor(hex: 0x333333)
        v.font = 15.autoFontSize()
        return v
    }()
    
    var baseBgView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = .white
        v.layer.cornerRadius = 6
        v.layer.masksToBounds = true
        return v
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func initViews() {
        
    }
    
    func setupYinying() {
        baseBgView.layer.shadowColor = UIColor(hex: 0xB3B3B3).cgColor
        baseBgView.layer.shadowOpacity = 0.5//阴影透明度
        baseBgView.layer.shadowOffset = CGSize(width: 0, height: 0)///阴影偏移量,四周
        baseBgView.layer.shadowRadius = 5//阴影半径  shadowPathWidth：阴影宽度
    }
    
    func result(resClosure:@escaping((String)->Void)) {
        //当文本框内容改变时，将内容输出到控制台上
        inputTf.inputAccessoryView = currentVC.addToolBar()
        inputTf.rx.text.orEmpty.asObservable()
            .subscribe(onNext: {[weak self] in
                if let `self` = self {
                    
                    resClosure($0)
                }
                print("您输入的是：\($0)")
            })
            .disposed(by: disposeBag)
    }
    
    func setPriceLb(_ price: String?) -> NSMutableAttributedString {
        let d: Double = Double(price ?? "0") ?? 0.0
        let priceStr = String(format: "%.2f", d)
        let firStr = "￥"
        let totalStr = "￥\(priceStr)"
        let attString = NSMutableAttributedString(string: totalStr)
        attString.yy_setColor(UIColor(hex: 0xF55035), range: NSRange(location: 0, length: firStr.count))
        attString.yy_setFont(13.autoFontSize(), range: NSRange(location: 0, length:firStr.count))
        attString.yy_setColor(UIColor(hex: 0xF55035), range: NSRange(location: firStr.count, length: totalStr.count - firStr.count))
        attString.yy_setFont(16.autoFontSize(), range: NSRange(location: firStr.count, length: totalStr.count - firStr.count))
        return attString
    }
    
    func setPriceLb(_ price: String?, _ priceFont: UIFont?) -> NSMutableAttributedString {
        let d: Double = Double(price ?? "0") ?? 0.0
        let priceStr = String(format: "%.2f", d)
        let firStr = "￥"
        let totalStr = "￥\(priceStr)"
        let attString = NSMutableAttributedString(string: totalStr)
        attString.yy_setColor(UIColor(hex: 0xF55035), range: NSRange(location: 0, length: firStr.count))
        attString.yy_setFont(13.autoFontSize(), range: NSRange(location: 0, length:firStr.count))
        attString.yy_setColor(UIColor(hex: 0xF55035), range: NSRange(location: firStr.count, length: totalStr.count - firStr.count))
        attString.yy_setFont(priceFont, range: NSRange(location: firStr.count, length: totalStr.count - firStr.count))
        return attString
    }
}
