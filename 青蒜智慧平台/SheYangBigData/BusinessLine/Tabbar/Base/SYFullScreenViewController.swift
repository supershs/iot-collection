//
//  SYFullScreenViewController.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/1/15.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit

class SYFullScreenViewController: UIViewController {

    var syPlayer: SYPlayer?
    
    var originalFrame: CGRect?
    
    var needShuping: Bool = false
    
    var superView: UIView?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    deinit {
        syPlayer?.pause()
        syPlayer = nil
        print("SYFullScreenViewController syPlayer 释放")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.syPlayer.frame = CGRect(x: 0, y: (SCREEN_HEIGHT - SCREEN_WIDTH)/2, width: SCREEN_WIDTH, height: SCREEN_WIDTH*(9.0/16))
        self.view.backgroundColor = .black
        
        
    }
    

    // 横竖屏感应
    open override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {

        if size.width > size.height {// 横屏
            self.syPlayer!.frame = CGRect(x: 0, y: 0, width: SCREEN_HEIGHT, height: SCREEN_WIDTH)
        } else {
            if needShuping {
                
                self.syPlayer!.frame = CGRect(x: 0, y: (SCREEN_HEIGHT - SCREEN_WIDTH*(9.0/16))/2, width: SCREEN_WIDTH, height: SCREEN_WIDTH*(9.0/16))
            } else {
                
            }
        }
        print("横竖屏")
    }
    

    
    func totatingScreen(_ ori: UIInterfaceOrientation) {

        let value = NSNumber(integerLiteral: ori.rawValue)
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    func shupingAction() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.canAllButUpsideDown = true // 开启全屏
        self.syPlayer!.backBtn.isHidden = false
        self.syPlayer!.removeFromSuperview()
        self.view.addSubview(self.syPlayer!)
        self.syPlayer!.frame = CGRect(x: 0, y: (SCREEN_HEIGHT - SCREEN_WIDTH*(9.0/16))/2, width: SCREEN_WIDTH, height: SCREEN_WIDTH*(9.0/16))
        self.totatingScreen(.portrait)
    }

    func fullScreenAction() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.canAllButUpsideDown = true // 开启全屏
        
        self.syPlayer!.removeFromSuperview()
        self.view.addSubview(self.syPlayer!)
        self.syPlayer!.frame = CGRect(x: 0, y: 0, width: SCREEN_HEIGHT, height: SCREEN_WIDTH)
        self.totatingScreen(.landscapeRight)
    }
    func backAction() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.canAllButUpsideDown = false
        
        self.syPlayer!.removeFromSuperview()
        if let s = superView {
            s.addSubview(syPlayer!)
        }
        if let o = originalFrame {
            syPlayer!.frame = o
        }
        self.totatingScreen(.portrait)
        if let nav = self.navigationController {
            nav.popViewController(animated: false)
        } else {
            self.dismiss(animated: false, completion: nil)
        }
    }
}
