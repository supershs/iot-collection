//
//  SYBaseModel.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2020/8/5.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit
import HandyJSON

class SYBaseModel: HandyJSON {
    
    var status: String?
    var code: Int?
    var message: String?
    var date: Date?
    required init() {
        
    }
}


class SYBaseListModel: HandyJSON {
    
    required init() {
        
    }
}

