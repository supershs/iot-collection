//
//  MyTabbarController.h
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/7/3.
//  Copyright © 2021 叁拾叁. All rights reserved.
//


#import <UIKit/UIKit.h>
typedef void(^SYMiddleBtnClick)(BOOL select);

@interface MyTabBarController : UITabBarController
@property(nonatomic, copy) SYMiddleBtnClick middleBtnClick;

@end
