//
//  SYBaseNavigationController.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/1/19.
//  Copyright © 2021 叁拾叁. All rights reserved.
//



import UIKit

class SYBaseNavigationController: UINavigationController {
    //隐藏状态栏
    var isStatusBarHidden = false {
        didSet {
            self.setNeedsStatusBarAppearanceUpdate()
        }
    }
    override var prefersStatusBarHidden: Bool {
        get {
            return isStatusBarHidden
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    


}
