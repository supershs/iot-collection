//
//  SYPlayer.h
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/1/15.
//  Copyright © 2021 叁拾叁. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import "WMPlayerModel.h"
#import "FastForwardView.h"
#import "WMLightView.h"

#import <WMPlayer.h>
//****************************枚举*******************************
//// 播放器的几种状态
//typedef NS_ENUM(NSInteger, WMPlayerState) {
//    WMPlayerStateFailed,        // 播放失败
//    WMPlayerStateBuffering,     // 缓冲中
//    WMPlayerStatePlaying,       // 播放中
//    WMPlayerStateStopped,       //暂停播放
//    WMPlayerStateFinished,      //完成播放
//    WMPlayerStatePause,         // 打断播放
//};
//// playerLayer的填充模式（默认：等比例填充，直到一个维度到达区域边界）
//typedef NS_ENUM(NSInteger, WMPlayerLayerGravity) {
//    WMPlayerLayerGravityResize,           // 非均匀模式。两个维度完全填充至整个视图区域
//    WMPlayerLayerGravityResizeAspect,     // 等比例填充，直到一个维度到达区域边界
//    WMPlayerLayerGravityResizeAspectFill  // 等比例填充，直到填充满整个视图区域，其中一个维度的部分区域会被裁剪
//};
//// 枚举值，包含播放器左上角的返回按钮的类型
//typedef NS_ENUM(NSInteger, BackBtnStyle){
//    BackBtnStyleNone, //什么都没有
//    BackBtnStyleClose,//关闭（X）
//    BackBtnStylePop //pop箭头<-
//};
//
////手势操作的类型
//typedef NS_ENUM(NSUInteger,WMControlType) {
//    WMControlTypeDefault,//无任何操作
//    WMControlTypeProgress,//视频进度调节操作
//    WMControlTypeVoice,//声音调节操作
//    WMControlTypeLight//屏幕亮度调节操作
//} ;



@class SYPlayer;
@protocol SYPlayerDelegate <NSObject>
@optional
//当前缓冲进度
-(void)wmplayer:(float)currentHuanchong;
//当前时间，总时间，总秒数，当前播放百分比
-(void)wmplayer:(NSString *)currentTime totalTime:(NSString *)totalTime totalSecond:(float)totalSecond baifenbi:(float)baifenbi;
//点击播放暂停按钮代理方法
-(void)wmplayer:(SYPlayer *)wmplayer clickedPlayOrPauseButton:(UIButton *)playOrPauseBtn;
//点击关闭按钮代理方法
-(void)wmplayer:(SYPlayer *)wmplayer clickedCloseButton:(UIButton *)backBtn;
//点击全屏按钮代理方法
-(void)wmplayer:(SYPlayer *)wmplayer clickedFullScreenButton:(UIButton *)fullScreenBtn;
//点击锁定🔒按钮的方法
-(void)wmplayer:(SYPlayer *)wmplayer clickedLockButton:(UIButton *)lockBtn;
//单击WMPlayer的代理方法
-(void)wmplayer:(SYPlayer *)wmplayer singleTaped:(UITapGestureRecognizer *)singleTap;
//双击WMPlayer的代理方法
-(void)wmplayer:(SYPlayer *)wmplayer doubleTaped:(UITapGestureRecognizer *)doubleTap;
//WMPlayer的的操作栏隐藏和显示
-(void)wmplayer:(SYPlayer *)wmplayer isHiddenTopAndBottomView:(BOOL )isHidden;
//播放失败的代理方法
-(void)wmplayerFailedPlay:(SYPlayer *)wmplayer WMPlayerStatus:(WMPlayerState)state;
//准备播放的代理方法
-(void)wmplayerReadyToPlay:(SYPlayer *)wmplayer WMPlayerStatus:(WMPlayerState)state;
//播放器已经拿到视频的尺寸大小
-(void)wmplayerGotVideoSize:(SYPlayer *)wmplayer videoSize:(CGSize )presentationSize;
//播放完毕的代理方法
-(void)wmplayerFinishedPlay:(SYPlayer *)wmplayer;
@end


@interface SYPlayer : UIView
/**
 播放器对应的model
 */
@property (nonatomic,strong) WMPlayerModel   *playerModel;
/**
 返回按钮的样式
 */
@property (nonatomic, assign) BackBtnStyle backBtnStyle;
/**
 BOOL值判断当前的状态，开发者要自己维护次BOOL值
 */
@property (nonatomic,assign) BOOL    isFullscreen;
/**
 播放速率(倍速播放，支持0.5、1.0、1.25、1.5、2.0)
 */
@property (nonatomic,assign) CGFloat rate;
/**
 播放器着色
 */
@property (nonatomic,strong) UIColor *tintColor;

@property (nonatomic,assign,readonly) BOOL    prefersStatusBarHidden;
/**
 播放器的代理
 */
@property (nonatomic, weak)id <SYPlayerDelegate> delegate;
/**
 是否开启音量手势
 */
@property (nonatomic,assign) BOOL  enableVolumeGesture;
/**
 是否开启后台播放模式
 */
@property (nonatomic,assign) BOOL  enableBackgroundMode;
/**
 是否开启快进手势
 */
@property (nonatomic,assign) BOOL  enableFastForwardGesture;
/**
 是否静音
 */
@property (nonatomic,assign) BOOL  muted;
/**
 是否循环播放（不循环则意味着需要手动触发第二次播放）
 */
@property (nonatomic,assign) BOOL  loopPlay;


/**
 设置playerLayer的填充模式
 */
@property (nonatomic, assign) WMPlayerLayerGravity    playerLayerGravity;
//是否是锁定屏幕旋转状态
@property (nonatomic,assign,readonly) BOOL isLockScreen;

//返回
@property (nonatomic,strong) UIButton  *backBtn;
/**
 自定义实例化方法初始化方式（-方法）

 @param playerModel 播放model
 @return 播放器实例
 */
-(instancetype)initPlayerModel:(WMPlayerModel *)playerModel;

/**
 自定义类方法+初始化方式（+方法）

 @param playerModel 播放model
 @return 播放器实例
 */
+(instancetype)playerWithModel:(WMPlayerModel *)playerModel;

/**
 播放
 */
- (void)play;

/**
 暂停
 */
- (void)pause;

/**
 获取正在播放的时间点

 @return double的一个时间点
 */
- (double)currentTime;
/**
 获取视频长度
 
 @return double的一个时间点
 */
- (double)duration;
/**
 playOrPauseBtn点击事件
 */
- (void)playOrPause:(UIButton *)sender;
/**
 重置播放器,然后切换下一个播放资源
 */
- (void )resetWMPlayer;


- (void)seekToTimeToPlay:(double)seekTime;
/**
 版本号

 @return 当前版本号
 */
+(NSString *)version;
//获取当前的旋转状态
+(CGAffineTransform)getCurrentDeviceOrientation;
//判断是否为iPhone X系列
+(BOOL)IsiPhoneX;
@end

