//
//  SYGengduoEditViewController.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2021/7/8.
//  Copyright © 2021 叁拾叁. All rights reserved.
//更多编辑页面

import UIKit

import MJRefresh
import RxSwift
import RxDataSources
import Then
import SDCycleScrollView

class SYGengduoEditViewController:SYBaseTableViewController{
    
    var menusView: ZQVariableMenuView!
    var allMenuModels: [NSAppMenuListModel] = []
    var inMenuModels: [NSAppMenuListModel] = []
    var unMenuModels: [NSAppMenuListModel] = []
    var inIds: [String] = []
    var indexPath: IndexPath?
    var targetIndexPath: IndexPath?
    var isShow: Bool = true
    var isEidt: String = "0"
    var count: Int = 1
    var dragingIndexPath: IndexPath?
    var hasTab: Bool = false
    
    init(hasTab: Bool = false) {
        super.init(nibName: nil, bundle: nil)
        self.hasTab = hasTab
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = !hasTab
    }
    
    override func viewDidLoad() {
        self.title = "编辑我的应用"
        self.view.backgroundColor = UIColor(hex: 0xf2f2f2)
        setRightNavItem()
        requestMenu()
    }
    func setRightNavItem() {
        
        let item:UIBarButtonItem = UIBarButtonItem(title: "完成", style: UIBarButtonItem.Style.plain, target: self, action: #selector(rightAction))
        item.tintColor = UIColor(hex: 0x01B865)
        self.navigationItem.rightBarButtonItem=item
    }

    
    @objc func rightAction() {

        requestVM.baseRequest(disposeBag: dispose, type: .home_gegduo_change(dic:["menuIds": inIds.joined(separator: ",")]), modelClass: SGBaseModel<NSModelDataModel>.self) {[weak self] (res) in
            
            if let `self` = self {
                NotificationCenter.default.post(name: .Noti_changeMenus, object: nil, userInfo: nil)
                self.sy_popVC()
            }
        } Error: {
        
        }
    }
    
    func requestMenu() {
        requestVM.baseRequest(disposeBag: dispose, type: .home_gengduo, modelClass: SGBaseModel<NSModelDataModel>.self) {[weak self] (res) in
            if let `self` = self {
                let models: [NSAppMenuVOListModel] = res.data?.appMenuVOList ?? []
                
                /*
                 flatMap 二维数组 -> 一维
                 */
                let yiweiAllMenuModels: [NSAppMenuListModel] = models.flatMap { ($0.appMenuList.map{ $0 } ?? []) }
                self.allMenuModels = yiweiAllMenuModels
                
                let inIdStr = res.data?.appMenuUser?.menuIds ?? ""
                let inIds: [String]  = inIdStr.components(separatedBy: ",")
                self.inIds = inIds
                
                self.getModels(inIds)
                
                self.initViews()
                self.menusView.reloadData()
            }
            
        } Error: {
            
        }
    }
    
    func getModels(_ inIds: [String]) {
        
        var allModels = self.allMenuModels
        inIds.forEach { (id) in
            let ins = self.allMenuModels.filter { $0.id == id }
            allModels = allModels.filter { $0.id != id }
            
            self.inMenuModels.append(ins.first!)
        }
        self.unMenuModels = allModels
        
    }
    
    func reloadCollectionView() {
        if UserInstance.currentMenus?.count == 0 {
            UserInstance.currentMenus = inMenuModels.map { $0.name ?? "" }
        }
    }
    
    func initViews() {
        reloadCollectionView()
        
        menusView = ZQVariableMenuView(frame: CGRect(x: 15, y: 0, width: self.view.frame.size.width - 30, height: self.view.frame.size.height))
        
        var unShowMeunModels: [ZQVarableMenuModel] = []
        var inShowMeunModels: [ZQVarableMenuModel] = []
        for (_, value) in inMenuModels.enumerated() {
            
            let model = ZQVarableMenuModel()
            model.name = value.name ?? ""
            model.imageUrl = (IMGIP + (value.imgUrl ?? "")).getEncodeString
            model.idField = value.id ?? ""
            inShowMeunModels.append(model)
        }
        for (_, value) in unMenuModels.enumerated() {
            
            let model = ZQVarableMenuModel()
            model.name = value.name ?? ""
            model.imageUrl = (IMGIP + (value.imgUrl ?? "")).getEncodeString
            model.idField = value.id ?? ""
            unShowMeunModels.append(model)
        }
        menusView.inUseModels = NSMutableArray(array: inShowMeunModels)
        menusView.unUseModels = NSMutableArray(array: unShowMeunModels)
        menusView.backgroundColor = .clear
        menusView.fixedNum = 2
        self.view.addSubview(menusView)
        menusView.completionBlock = {[weak self] inModels, unModels in
            if let `self` = self {
                self.inIds = inModels!.map { $0.idField }
                self.getModels(self.inIds)
            }
        }
    }
    
}

