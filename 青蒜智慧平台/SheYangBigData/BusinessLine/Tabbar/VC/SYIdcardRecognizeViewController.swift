////
////  SYIdcardRecognizeViewController.swift
////  SheYangBigData
////
////  Created by 宋海胜 on 2021/7/9.
////  Copyright © 2021 叁拾叁. All rights reserved.
////
//
//
//import UIKit
//
//class SYIdcardRecognizeViewController: SYBaseViewController , UITableViewDelegate, UITableViewDataSource{
//    
//    
//    var infoModel: SYMyInfoModel!
//    var headData: HeadMyData!
//    var gongjuModels:[SYMyGongjuModel] = []
//    var idInfos: [String] = []
//    var idImgs: [UIImage] = []
//    
//    var tableView: UITableView!
//    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        self.navigationController?.navigationBar.isHidden = false
//        self.tabBarController?.tabBar.isHidden = true
//        
//    }
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        setTableView()
//        setSearchView()
//        self.title = "身份认证"
//        self.view.backgroundColor = Constant.bgViewColor
//    }
//    
//    func requestUploadImages(imgs:[UIImage], complete:@escaping ([PYUploadImageModel]?)->()) {
//        
//        if idImgs.count < 2 {
//            HUDUtil.showBlackTextView(text: "请上传身份证照片")
//            return
//        }
//        
//        let trueName = idInfos.first ?? ""
//        let idCard = idInfos[1]
//        let address = idInfos.last ?? ""
//        if trueName.isEmpty {
//            HUDUtil.showBlackTextView(text: "请输入姓名")
//            return
//        }
//        if idCard.isEmpty {
//            HUDUtil.showBlackTextView(text: "请输入身份证号")
//            return
//        }
////        if imgUrls.first?.isEmpty == true || imgUrls.last?.isEmpty == true {
////            HUDUtil.showBlackTextView(text: "请上传身份证")
////            return
////        }
//        
//        
//        
//        requestVM.baseRequest(disposeBag: dispose, type: .shenfenShibie(imgs: imgs, dic: ["trueName": trueName, "idCard":idCard,"address": address]), modelClass: SGBaseModel<SYMyInfoModel>.self) {[weak self] (res) in
//                        if let `self` = self {
//                            DispatchQueue.main.async {
//                                self.sy_popVC()
//                            }
//                        }
//            
//                    }Error: {
//            
//        }
//
////        requestVM.baseRequest(disposeBag: dispose, type: .uploadImages(imgs: imgs), modelClass: SGBaseNoPageListModel<PYUploadImageModel>.self) {[weak self] (res) in
////            if let `self` = self {
////                complete(res.data)
////            }
////
////        } Error: {
////
////        }
//        
//        
//        
//    
//    }
//    
//    func requestUpShenfenInfo(_ imgUrls: [String]) {
//        
//   
//        
////        requestVM.baseRequest(disposeBag: dispose, type: .shenfenShibie(dic: ["trueName": trueName, "idCard":idCard, "idCardImg": idCardImg]), modelClass: SGBaseModel<SYMyInfoModel>.self) {[weak self] (res) in
////            if let `self` = self {
////                DispatchQueue.main.async {
////                    self.sy_popVC()
////                }
////            }
////
////        } Error: {
////
////        }
//    }
//    
//    func setSearchView() {
//        
//        let renyangT = UILabel()
//        renyangT.text = "立即认证"
//        renyangT.textColor = .white
//        renyangT.textAlignment = .center
//        renyangT.font = 19.autoMediumFontSize()
//        renyangT.frame = CGRect(x: 15.autoWidth(), y: 6.autoWidth(), width: SCREEN_WIDTH - 30.autoWidth(), height: 38.autoWidth())
//        
//        let renyang = UIView()
//        renyang.vg_gradLayerCreatWithFrame(frame: CGRect(x: 15.autoWidth(), y: 6.autoWidth(), width: SCREEN_WIDTH - 30.autoWidth(), height: 38.autoWidth()), leftC: UIColor(hex: 0x0CB7B9), rightC: UIColor(hex: 0x00B85F))
//        
//        //mark label在button上不用设置该属性
////        renyang.isUserInteractionEnabled = true
//        
//        let zixun: UIButton = UIButton()
//        zixun.backgroundColor = .white
//        zixun.addAction {[weak self] in
//            if let `self` = self {
//                self.requestUploadImages(imgs: self.idImgs) { (models) in
//                    if let m = models {
//                        var urls: [String] = []
//                        m.forEach({ (value) in
//                            let fileName = "\(value.filePath ?? "")\(value.fileName ?? "").\(value.fileSuffix ?? "")"
//                            urls.append(fileName)
//                        })
//                        self.requestUpShenfenInfo(urls)
//                    }
//                }
//            }
//        }
//        view.addSubview(zixun)
//        zixun.addSubview(renyang)
//        renyang.addSubview(renyangT)
//        zixun.snp.makeConstraints { (make) in
//            make.bottom.equalToSuperview().offset(-BOTTOM_SAFE_HEIGHT)
//            make.right.equalToSuperview()
//            make.left.equalToSuperview()
//            make.height.equalTo(50.autoWidth())
//        }
//        
//    }
//
//    func setTableView() {
//        
//        //创建表格视图
//        self.tableView = UITableView(frame: CGRect(x: 0, y:NAV_HEIGHT, width: SCREEN_WIDTH, height: SCREEN_HEIGHT - NAV_HEIGHT - 50.autoWidth() - BOTTOM_SAFE_HEIGHT), style:.plain)
//        self.tableView.separatorStyle = .none
//        self.tableView.delegate = self
//        self.tableView.dataSource = self
//        self.tableView.backgroundColor = .clear
//        self.tableView!.register(SYIdcardInputCell.self, forCellReuseIdentifier: "SYIdcardInputCell")
//        self.tableView!.register(SYIdcardCell.self, forCellReuseIdentifier: "SYIdcardCell")
//        
//        self.view.addSubview(self.tableView!)
//        topMangain(self.tableView!)
//    }
//    
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 5
//    }
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if indexPath.row <= 2  {
//            let cell: SYIdcardInputCell? = tableView.dequeueReusableCell(withIdentifier: "SYIdcardInputCell") as? SYIdcardInputCell
//            cell?.selectionStyle = .none
//            cell?.currentVC = self
//            cell?.configure(indexPath.row, idInfos)
//            return cell!
//        } else {
//            let cell: SYIdcardCell? = tableView.dequeueReusableCell(withIdentifier: "SYIdcardCell") as? SYIdcardCell
//            cell?.selectionStyle = .none
//            cell?.configure(indexPath.row-3, idImgs)
//            return cell!
//        }
//        
//    }
//    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        
//        if indexPath.row == 2 {
//            
//        } else if indexPath.row == 3 {
//            
//            let vc = AVCaptureViewController()
//            vc.isZhengmian = true
//            vc.resultBlock = {[weak self] (info, img) in
//                if let `self` = self {
//                    if let n = info?.name, let id = info?.num , let address = info?.address{
//                        self.idInfos = [n, id,address]
//                        if let i = img {
//                            if self.idImgs.count > 0 {
//                                self.idImgs[0] = i
//                            } else {
//                                self.idImgs.append(i)
//                            }
//                        }
//                        DispatchQueue.main.async {
//                            self.tableView.reloadData()
//                        }
//                    } else {
//                        DispatchQueue.main.async {
//                            HUDUtil.showBlackTextView(text: "请重新识别")
//                        }
//                    }
//                }
//            }
//            DispatchQueue.main.async {
//                self.sy_push(vc)
//            }
//            
//        } else  if indexPath.row == 4 {
//            let vc = AVCaptureViewController()
//            vc.isZhengmian = false
//            vc.resultBlock = {[weak self] (info, img) in
//                if let `self` = self {
//                    if let jiguan = info?.issue, let riqi = info?.valid {
//                        if let i = img {
//                            if self.idImgs.count == 2 {
//                                self.idImgs[1] = i
//                            } else {
//                                self.idImgs.append(i)
//                            }
//                        }
//                        DispatchQueue.main.async {
//                            self.tableView.reloadData()
//                        }
//                    } else {
//                        DispatchQueue.main.async {
//                            HUDUtil.showBlackTextView(text: "请重新识别")
//                        }
//                    }
//                }
//            }
//            DispatchQueue.main.async {
//                self.sy_push(vc)
//            }
//        }
//    }
//}
//
//
//class SYIdcardInputCell: SYBaseCell {
//
//    var titles: [String] = ["姓    名：", "身份证号：", "上传证件："]
//    var subTitles: [String] = ["请输入或拍照识别", "请输入或拍照识别", "身份证"]
//    let nameLb: UILabel = {
//        let v:UILabel = UILabel()
//        v.textColor = UIColor(hex: 0x333333)
//        v.font = 15.autoFontSize()
//        return v
//    }()
//    
//    var aboveBt: UIButton = {
//        let v = UIButton()
//        return v
//    }()
//    
//    
//    func configure(_ index:Int, _ contents: [String]) {
//        
//        nameLb.text = titles[index]
//        inputTf.placeholder = subTitles[index]
//        if contents.count > index {
//            inputTf.text = contents[index]
//        }
//    }
//
//    override func initViews() {
//        
//        inputTf.textColor = UIColor(hex: 0x333333)
//        inputTf.font = 14.autoFontSize()
//        inputTf.textAlignment = .right
//
//        aboveBt.addAction {[weak self] in
//            if let `self` = self {
//                
//            }
//        }
//       
//        self.backgroundColor = .clear
//        contentView.addSubview(baseBgView)
//        baseBgView.addSubview(nameLb)
//        baseBgView.addSubview(inputTf)
//        baseBgView.addSubview(aboveBt)
//        
//        nameLb.snp.makeConstraints { (make) in
//            make.left.equalToSuperview().offset(10.autoWidth())
//            make.width.equalTo(80.autoWidth())
//            make.top.bottom.equalToSuperview()
//            make.height.equalTo(44.autoWidth())
//        }
//        inputTf.snp.makeConstraints { (make) in
//            make.right.equalToSuperview().offset(-10.autoWidth())
//            make.top.bottom.equalToSuperview()
//            make.height.equalTo(44.autoWidth())
//        }
//        baseBgView.snp.makeConstraints { (make) in
//            make.top.equalToSuperview().offset(15.autoWidth())
//            make.left.equalToSuperview().offset(15.autoWidth())
//            make.right.equalToSuperview().offset(-15.autoWidth())
//            make.height.equalTo(44.autoWidth())
//            make.bottom.equalToSuperview()
//        }
//    }
//}
//
//
//class SYIdcardCell: SYBaseCell {
//
//    var titles: [String] = ["上传身份证正面", "上传身份证反面"]
//    var subTitles: [String] = ["请拍照上传您的身份证", "请拍照上传您的身份证"]
//    var imgNames: [String] = ["shibie_shenfen_zhengmian", "shibie_shenfen_fanmian"]
//    let titleLb: UILabel = {
//        let v:UILabel = UILabel()
//        v.textColor = UIColor(hex: 0x0D2510)
//        v.font = 13.autoFontSize()
//        return v
//    }()
//    
//    let subTitleLb: UILabel = {
//        let v:UILabel = UILabel()
//        v.textColor = UIColor(hex: 0x647C66)
//        v.font = 12.autoFontSize()
//        return v
//    }()
//    
//    var paizhao: UIImageView = {
//        let v = UIImageView()
//        v.sy_name("shibie_shenfen_camera")
//        v.isUserInteractionEnabled = true
//        return v
//    }()
//    
//    var bgImg: UIImageView = {
//        let v = UIImageView()
//        v.contentMode = .scaleAspectFill
//        v.isUserInteractionEnabled = true
//        v.clipsToBounds = true
//        return v
//    }()
//    
//    
//    func configure(_ index: Int, _ imgs: [UIImage]) {
//        
//        titleLb.text = titles[index]
//        subTitleLb.text = subTitles[index]
//        bgImg.sy_name(imgNames[index])
//        if imgs.count > index {
//            bgImg.image = imgs[index]
//        }
//    }
//
//    override func initViews() {
//        
//        setupYinying()
//        self.backgroundColor = .clear
//        contentView.addSubview(baseBgView)
//        baseBgView.addSubview(titleLb)
//        baseBgView.addSubview(subTitleLb)
//        baseBgView.addSubview(bgImg)
//        baseBgView.addSubview(paizhao)
//        
//        titleLb.snp.makeConstraints { (make) in
//            make.left.equalToSuperview().offset(50.autoWidth())
//            make.top.equalToSuperview().offset(86.autoWidth())
//            make.height.equalTo(14.autoWidth())
//        }
//        subTitleLb.snp.makeConstraints { (make) in
//            make.left.equalToSuperview().offset(50.autoWidth())
//            make.top.equalToSuperview().offset(108.autoWidth())
//            make.height.equalTo(14.autoWidth())
//        }
//        baseBgView.snp.makeConstraints { (make) in
//            make.left.equalToSuperview().offset(15.autoWidth())
//            make.top.equalToSuperview().offset(15.autoWidth())
//            make.right.equalToSuperview().offset(-15.autoWidth())
//            make.height.equalTo(213.autoWidth())
//            make.bottom.equalToSuperview()
//        }
//        paizhao.snp.makeConstraints { (make) in
//            make.center.equalToSuperview()
//        }
//        bgImg.snp.makeConstraints { (make) in
//            make.left.equalToSuperview().offset(15.autoWidth())
//            make.top.equalToSuperview().offset(15.autoWidth())
//            make.right.equalToSuperview().offset(-15.autoWidth())
//            make.bottom.equalToSuperview().offset(-15.autoWidth())
//        }
//    }
//}
