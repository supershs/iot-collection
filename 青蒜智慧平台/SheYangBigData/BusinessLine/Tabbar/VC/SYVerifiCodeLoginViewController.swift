//
//  SYVerifiCodeLoginViewController.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2020/8/20.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import SnapKit
class SYVerifiCodeLoginViewController: SYBaseViewController, SYLoginProtocol {
    
    
    var loginType: LoginEnum {
        return .verifiCode
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        
        initViews()
        
    }
    
    fileprivate func initViews() {
        
        let loginView = SYLoginView(frame: CGRect.zero, loginProtocol: self, viewController: self)
        self.view.addSubview(loginView)
        loginView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    func getVerifiCode() {
        
    }
    
    func login(loginName: String, password: String) {
        
    }
}
