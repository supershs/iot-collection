//
//  SYMyViewController.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2020/8/21.
//  Copyright © 2020 叁拾叁. All rights reserved.
//




import UIKit

class SYMyViewController: SYBaseViewController , UITableViewDelegate, UITableViewDataSource{
    
    
    var infoModel: SYMyInfoModel!
    var headData: HeadMyData!
    var gongjuModels:[SYMyGongjuModel] = []
    
    var tableView: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.isHidden = false
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setTableView()
        self.title = "我的"
        requestUserInfo()
//        requestGongju()
//        getUserInfo()
        self.view.backgroundColor = Constant.bgViewColor
        NotificationCenter.default.addObserver(self, selector: #selector(refreshData), name: .Noti_myPageRefreshData, object: nil)
    }
    
    @objc func refreshData() {
        requestUserInfo()
//        requestGongju()
    }
    
    func requestUserInfo() {
        requestVM.baseRequest(disposeBag: dispose, type: .myInfo, modelClass: SGBaseModel<SYMyInfoModel>.self) {[weak self] (res) in
            if let `self` = self {
                
                self.infoModel = res.data
				UserInstance.roleId = self.infoModel.roleId
                self.tableView.reloadData()
            }
            
        } Error: {
            
        }
    }
    
    func requestGongju() {
        
        requestVM.baseRequest(disposeBag: dispose, type: .myGongju, modelClass: SGBaseNoPageListModel<SYMyGongjuModel>.self) {[weak self] (res) in
            if let `self` = self {
                
                self.gongjuModels = res.data ?? []
                self.tableView.reloadData()
            }
            
        } Error: {
            
        }
    }
    
    func getUserInfo() {
        
        requestVM.baseRequest(disposeBag: dispose, type: .my_mine, modelClass: SGBaseModel<HeadMyData>.self) {[weak self] (res) in
            if let `self` = self {
                
                print(res)
                
                self.headData = res.data
                
                self.tableView.reloadData()
//                self.gongjuModels = res.data ?? []
            }
            
        } Error: {
            
        }

    }

    func setTableView() {
        
        //创建表格视图
        self.tableView = UITableView(frame: CGRect(x: 0, y:0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT - TABBAR_HEIGHT), style:.grouped)
        self.tableView.separatorStyle = .none
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.backgroundColor = .clear
        self.tableView!.register(SYMyInfoCell.self, forCellReuseIdentifier: "SYMyInfoCell")
        self.tableView!.register(SYMyCaozuoCell.self, forCellReuseIdentifier: "SYMyCaozuoCell")
        self.tableView!.register(SYMySetCell.self, forCellReuseIdentifier: "SYMySetCell")
        
        self.view.addSubview(self.tableView!)
        topMangain(self.tableView!)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell: SYMyInfoCell? = tableView.dequeueReusableCell(withIdentifier: "SYMyInfoCell") as? SYMyInfoCell
            cell?.selectionStyle = .none
            cell?.currentVC = self
            if let m = self.infoModel {
                cell?.configure(m)
            }
//            if let m = self.headData {
//                cell?.configureHead(m)
//            }
            return cell!
        }
//		else if indexPath.row == 1{
//            let cell: SYMyCaozuoCell? = tableView.dequeueReusableCell(withIdentifier: "SYMyCaozuoCell") as? SYMyCaozuoCell
//            cell?.selectionStyle = .none
//            cell?.currentVC = self
//            return cell!
//        }
		else {
            let cell: SYMySetCell? = tableView.dequeueReusableCell(withIdentifier: "SYMySetCell") as? SYMySetCell
            cell?.selectionStyle = .none
            cell?.configure(indexPath.row-1)
            return cell!
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if indexPath.row == 2 {
//
//        }
//		else
		if indexPath.row == 2 {
            
            self.sy_pushWebVC(.shezhi) { (_) in
                DispatchQueue.main.async {
                    SYTokenCheck.toLoginVC()
                }
            }
        }
		else  if indexPath.row == 1 {
			self.sy_pushWebVC(.yijianfankui)
//            let vc = DYMyDingdanViewController()
//            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let v = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 0.01))
        return v
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let v = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 0.01))
        return v
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
}
