//
//  SYIMlistViewController.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/1/18.
//  Copyright © 2021 叁拾叁. All rights reserved.
//



import UIKit
import MJRefresh
import RxSwift
import RxDataSources
import Then
import SDCycleScrollView
import SwiftyJSON
import SwiftDate

//class SYIMlistViewController: SYBaseTableViewController {
//    
//    fileprivate var vm: VGTableViewPageViewModel<Records>!
//    fileprivate var itemDataSouce = [MessageModel]()
//	var bgView: UIImageView = {
//		let v = UIImageView()
//		v.image = UIImage(named: "home_header")
//		return v
//	}()
//    let titleLb: UILabel = {
//        let v = UILabel(frame: CGRect(x: 0, y: 17.autoWidth() + STATUSBAR_HEIGHT, width: SCREEN_WIDTH, height:  17.autoWidth()))
//        v.textAlignment = .center
//        v.text = "消息通知"
//        v.font = 17.autoFontSize()
//        v.textColor = UIColor.white
//        return v
//    }()
//    let back: UIButton = {
//        let v: UIButton  = UIButton()
//        v.setImage(UIImage(named: "nav_back"), for: .normal)
//        return v
//    }()
//    var headerView: SYIMListTopView = SYIMListTopView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 105.autoWidth()))
//    var models: [SYIMListModel]!
//    var timer: Timer!
//    var hasTab: Bool = false
//    init(hasTab: Bool = false) {
//        super.init(nibName: nil, bundle: nil)
//        self.hasTab = hasTab
//    }
//    
//    required init?(coder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//        
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        self.navigationController?.navigationBar.isHidden = true
//        self.tabBarController?.tabBar.isHidden = !hasTab
//    }
//    
//    deinit {
//        stopTimer()
//    }
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        self.view.backgroundColor = .white
//        initViews()
//        setTableView()
//        setNoDataView()
//        
//        requestList()
////        startTimer()
//    }
//    
//    func startTimer() {
//        
//        if timer == nil {
//            timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
//            RunLoop.current.add(timer, forMode: RunLoop.Mode.common)
//            timer.fire()
//        }
//    }
//    
//    func stopTimer() {
//        if self.timer != nil && self.timer.isValid {
//            self.timer.invalidate()
//            self.timer = nil
//        }
//    }
//    
//    @objc func timerAction() {
//        requestList()
//    }
//    
//    func refreshData() {
//        if nil == self.tableView {
//            return
//        }
//        requestList()
//    }
//    
//    func loadDataSuccessed() {
//        DispatchQueue.main.async {
//
//            self.setDataSource()
//        }
//    }
//    
//    func requestList() {
//        
//        requestVM.baseRequest(disposeBag: dispose, type: .im_huiHuaList, modelClass: SGBaseNoPageListModel<SYIMListModel>.self, noHud: true, noJiance: true) {[weak self] (res) in
//            if let `self` = self {
//                
//                self.models = res.data ?? []
////                for (i, listModel) in self.models.enumerated() {
////                    self.models[i].info?.noReadNum = "\(self.calculateNoReadNum(listModel.info?.relationInfo ?? ""))"
////                }
//                self.loadDataSuccessed()
//            }
//            
//        } Error: {
//            
//        }
//    }
//    
//    func creatChatRoom(dId: String, complete:@escaping (PYCreatChatRoomModel?, Bool)->()) {
//        requestVM.baseRequest(disposeBag: dispose, type: .im_creatRoom(dId: dId), modelClass: SGBaseModel<PYCreatChatRoomModel>.self) {[weak self] (res) in
//            if let `self` = self {
//                complete(res.data, true)
//                
//                DispatchQueue.main.async {
//                    
//                }
//            }
//            
//        } Error: {
//            complete(nil,false)
//        }
//    }
//    
//    func calculateNoReadNum(_ roomId: String) -> Int {
//        let result = TSDBManager.shared.prepareTasks(.msgs, roomId, 0, 10000)
//        // 置为已读
//        let noReadRes = result.filter { $0.read == false }
//        return noReadRes.count
//    }
//    
//    func initViews() {
//        back.addAction {[weak self] in
//            if let `self` = self {
//                self.popVC()
//            }
//        }
//        back.isHidden = hasTab
//        view.addSubview(bgView)
//        view.addSubview(back)
//        view.addSubview(titleLb)
//        bgView.snp.makeConstraints { (make) in
//            make.top.left.right.equalToSuperview()
//            make.height.equalTo(100.autoWidth() + STATUSBAR_HEIGHT)
//        }
//        back.snp.makeConstraints { (make) in
//            make.left.equalToSuperview()
//            make.top.equalToSuperview().offset(STATUSBAR_HEIGHT)
//            make.height.equalTo(48.autoWidth())
//            make.width.equalTo(30.autoWidth())
//        }
//    }
//    
//    func setTableView() {
//        
//        //创建表格视图
//        self.tableView = UITableView(frame: CGRect(x: 0, y: STATUSBAR_HEIGHT + 56.autoWidth(), width: SCREEN_WIDTH, height: SCREEN_HEIGHT-STATUSBAR_HEIGHT-56.autoWidth()), style:.plain)
//        self.tableView.separatorStyle = .none
//        self.tableView.backgroundColor = .clear
//        self.tableView!.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
//        
//        headerView.currentVC = self
//        self.tableView.tableHeaderView = headerView
//        self.view.addSubview(self.tableView!)
//        
//        tableView.rx.itemSelected
//            .subscribe(onNext:{ [weak self] indexPath in
//                if let `self` = self {
//                    
////                    self.sy_pushWebVC(.shebeiYujin)
////                    self.sy_pushWebVC(.shebeiYujin)
////                    self.sy_pushWebVC(.qixiangYujin)
////                    self.sy_pushWebVC(.zhixunXiaoxi)
//                    self.requestVM.baseRequest(disposeBag: self.dispose, type:.im_huiHuaUpdateReadTime(dic: ["modules":                         self.models[indexPath.row].moduleName ?? ""]), modelClass:SGBaseModel<SYIMListModel>.self) { (res) in
//                          
//                          
//                      } Error: {
//                          
//                      }
//                    switch self.models[indexPath.row].moduleName {
//                    
//             
//
//                    case "ADOPT":
//                        let vc = RYDingdanListViewController()
//                        self.sy_push(vc)
//                    case "TOUR":
//                        let vc = DYMyDingdanViewController()
//                        self.sy_push(vc)
//                    case "CHAT":
//                        let viewController = TSChatViewController.ts_initFromNib() as! TSChatViewController
//                        let dict = ["":""]
//                        let messageModel = TSMapper<MessageModel>().map(JSON: dict as [String : String])
//                        let chatId: String = self.models[indexPath.row].info?.relationInfo ?? ""
//                        messageModel?.chatId = chatId
//                        messageModel?.nickname = self.models[indexPath.row].typeName
//                        messageModel?.notMeId = ""
//                        messageModel?.notMeHeadUrl = self.models[indexPath.row].icon ?? ""
//                        viewController.messageModel = messageModel
//                        self.ts_pushAndHideTabbar(viewController)
//                    default:
//                        var r = self.models[indexPath.row].info?.route ?? ""
//                        if !r.isEmpty {
//                            r.remove(at: r.startIndex)
//                            self.sy_pushWebVC(r)
//                        } else {
//                            self.sy_toast("url not found")
//                        }
//                        
////                        let session = NIMSession("1", type: .P2P)
////
////                        let vc: VGIMOCSessionViewController = VGIMOCSessionViewController(session: session) {
////                            DispatchQueue.main.async {
////                //                BadgeValueUtils.NEW_IMMESSAGE_COUNT = NIMSDK.shared().conversationManager.allUnreadCount()
////                            }
////                        }
////                        self.navigationController?.pushViewController(vc, animated: true)
//                    }
//                }
//            })
//            .disposed(by:dispose)
//    }
//    
//    func setNoDataView() {
//        noDataView = SYNoDataView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT-NAV_HEIGHT))
//        noDataView.isHidden = true
//        tableView.addSubview(noDataView)
//    }
//    
//    func setDataSource() {
//        //初始化数据
//        let sections = Observable.just([
//            SYIMListSection(header: "", items: self.models)
//        ])
//        
//        //创建数据源
//        let dataSource = RxTableViewSectionedAnimatedDataSource<SYIMListSection>(
//            //设置单元格
//            configureCell: { ds, tv, ip, item in
//                
//                let cell: SYIMListCell = tv.dequeueReusableCell(withIdentifier: "Cell")
//                    as? SYIMListCell ?? SYIMListCell(style: .default, reuseIdentifier: "Cell")
//                cell.configureView(item)
//                cell.selectionStyle = .none
//                
//                return cell
//                
//        },
//            //设置分区头标题
//            titleForHeaderInSection: { ds, index in
//                return ds.sectionModels[index].header
//        })
//        tableView.dataSource = nil
//        //绑定单元格数据
//        sections
//            .bind(to: tableView.rx.items(dataSource: dataSource))
//            .disposed(by: dispose)
//        
//    }
//    
//    
//}



//自定义Section
struct SYIMListSection {
    var header: String
    var items: [Item]
}

extension SYIMListSection : AnimatableSectionModelType {
    typealias Item = SYIMListModel

    var identity: String {
        return header
    }

    init(original: SYIMListSection, items: [Item]) {
        self = original
        self.items = items
    }
}

extension SYIMListModel: IdentifiableType {
    var identity: String {
        return "\(type)"
    }

    typealias Identity = String
}
//自定义Section
struct MySection {
    var header: String
    var items: [Item]
}

extension MySection : AnimatableSectionModelType {
    typealias Item = Records
    
    var identity: String {
        return header
    }
    
    init(original: MySection, items: [Item]) {
        self = original
        self.items = items
    }
}

extension Records: IdentifiableType {
    var identity: String {
        return id!
    }
    
    typealias Identity = String
}
