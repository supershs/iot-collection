//
//  SYGongzuotaiViewController.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/2/1.
//  Copyright © 2021 叁拾叁. All rights reserved.
//




import UIKit

class SYGongzuotaiViewController: SYBaseViewController , UITableViewDelegate, UITableViewDataSource{
    
    fileprivate var vm: VGTableViewPageViewModel<SYGZTDaibanListModel>!
    
    var infoModel: SYGongzuotaiModel!
    var gongjuModels:[SYGZTDaibanListModel] = []
    var headView: SYGongzuotaiHeadView!
    
//    lazy var gongsi: VGTitleImgView = {
//        let v: VGTitleImgView = VGTitleImgView(frame: CGRect.zero)
//
//        v.textLb.textColor = UIColor(hex: 0xACACAC)
//        v.textLb.font = 12.autoFontSize()
//        v.imageV.sy_name("gongzuotai_gongsi")
//        v.tapClosure = {[weak self] (index, isSelect) in
//            if let weakSelf = self {
//
//            }
//        }
//        return v
//    }()
    
    
    var tableView: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setTableView()
        
        setupViewModel()
        self.view.backgroundColor = Constant.bgViewColor
    }
    
    func refreshData() {
        if nil == self.vm {
            return
        }
        requestInfo()
        vm.getList(type: .gongzuotaiDaiban(page: vm.vg_pageNo, size:vm.size), dispose: dispose)
    }
    
    fileprivate func setupViewModel() {
        
        vm = VGTableViewPageViewModel<SYGZTDaibanListModel>()
        vm.tableView = tableView
        vm.emptyViewParams = ("", "", 40.0, 30.0)
        tableView.vg_pageProtocol = vm
        vm.getList(type: .gongzuotaiDaiban(page: vm.vg_pageNo, size:vm.size), dispose: dispose)
        vm.getDataClosure = {[weak self] page in
            if let `self` = self {
                self.vm.getList(type: .gongzuotaiDaiban(page: self.vm.vg_pageNo, size:self.vm.size), dispose: self.dispose)
                self.requestInfo()
            }
        }
        vm.loadDataClosure = {[weak self] success in
            if let `self` = self {
                if success {
                    self.gongjuModels = self.vm.dataSource
                    self.tableView.reloadData()
                    
                }
            }
        }
        requestInfo()
    }
    
    func requestInfo() {
        requestVM.baseRequest(disposeBag: dispose, type: .gongzuotai, modelClass: SGBaseModel<SYGongzuotaiModel>.self) {[weak self] (res) in
            if let `self` = self {
                
                self.infoModel = res.data
                self.headView.configure(res.data ?? SYGongzuotaiModel())
                self.tableView.reloadData()
                
            
                self.tableView.vg_emptyView.isHidden = true

            }
            
        } Error: {
            
        }
        
    }
    

    func setTableView() {
        
        //创建表格视图
        self.tableView = UITableView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT - TABBAR_HEIGHT), style:.grouped)
        self.tableView.separatorStyle = .none
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.backgroundColor = .clear
        self.tableView!.register(SYNongjiCell.self, forCellReuseIdentifier: "SYNongjiCell")
        self.tableView!.register(SYShouyeCell.self, forCellReuseIdentifier: "SYShouyeCell")
        self.tableView!.register(SYDaibanCell.self, forCellReuseIdentifier: "SYDaibanCell")
        self.view.addSubview(self.tableView!)
        
        headView = SYGongzuotaiHeadView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 297.autoWidth() + STATUSBAR_HEIGHT))
        headView.currentVC = self
        self.tableView.tableHeaderView = headView

        
        if #available(iOS 11.0, *) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollView.ContentInsetAdjustmentBehavior.never
        } else {
            self.edgesForExtendedLayout = UIRectEdge.bottom
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell: SYNongjiCell? = tableView.dequeueReusableCell(withIdentifier: "SYNongjiCell") as? SYNongjiCell
            cell?.selectionStyle = .none
            cell?.currentVC = self
            return cell!
            
        } else if indexPath.row == 1{
            let cell: SYShouyeCell? = tableView.dequeueReusableCell(withIdentifier: "SYShouyeCell") as? SYShouyeCell
            cell?.selectionStyle = .none
            cell?.currentVC = self
            if let m = self.infoModel {
                cell?.configure(m)
            }
            return cell!
        } else {
            let cell: SYDaibanCell? = tableView.dequeueReusableCell(withIdentifier: "SYDaibanCell") as? SYDaibanCell
            cell?.selectionStyle = .none
            cell?.currentVC = self
            cell?.configure(self.gongjuModels)
            return cell!
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            
        } else if indexPath.section == 1 {
            self.sy_pushWebVC(.myShouyi)
        } else {
            
        }
        
        print("didSelectRowAt")
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let v = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 0.01))
        return v
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        let v = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 0.01))
//        return v
//    }
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return 0.01
//    }
//
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
            
            let v = UIButton(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 44.autoWidth()))
            v.setTitle("没有更多数据", for: .normal)
            v.setTitleColor(UIColor(hex: 0x999999), for: .normal)
            v.titleLabel?.font = 14.autoFontSize()
            v.addAction {[weak self] in
                if let `self` = self {
//                    let vc = DYJQListViewController(params: [:])
//                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        
            return v
//        } else {
//            let v = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 0.01))
//            return v
        }
//    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        if section == 2 {
            return 44.autoWidth()
//        } else {
//            return 0.01
//        }
    }
}


class SYGongzuotaiHeadView: SYBaseView {
    
    let myBg: UIImageView = {
        let v: UIImageView = UIImageView()
        v.sy_name("gongzuotai_bg")
        v.backgroundColor = Constant.blueColor
        return v
    }()
    
    let headPic: UIImageView = {
        let v: UIImageView = UIImageView()
        
        v.layer.cornerRadius = 41.autoWidth()
        v.layer.masksToBounds = true
        return v
    }()
    
    let tianjia: UIButton = {
        let v: UIButton = UIButton()
        v.setImage(UIImage(named: "tianjia"), for: .normal)
        v.isHidden = true
        return v
    }()
    
    let titleLb: UILabel = {
        let v = UILabel()
        v.text = "工作台"
        v.font = 17.autoMediumFontSize()
        v.textColor = .white
        return v
    }()
    
    let name: UILabel = {
        let v = UILabel()
        v.font = 18.autoMediumFontSize()
        v.textColor = .white
        
        return v
    }()
    
    
    lazy var role: VGTitleImgView = {
        let v: VGTitleImgView = VGTitleImgView(frame: CGRect.zero, imgSuperViewMargin: 10, textSuperViewMargin: 10)
        v.layer.cornerRadius = 15.autoWidth()
        v.backgroundColor = UIColor(hex: 0x414A5D)
        
        v.textLb.textColor = UIColor(hex: 0xD1B176)
        v.textLb.font = 13.autoFontSize()
        v.imageV.sy_name("gongzuotai_role")
        v.tapClosure = {[weak self] (index, isSelect) in
            if let weakSelf = self {
                
            }
        }
        return v
    }()
    
    
    var daibanState = SYDaibanStateCell()
    
    func configure(_ model: SYGongzuotaiModel) {
        self.name.text = model.userInfo?.userName
        self.role.textLb.text = model.userInfo?.role
//        self.headPic.kf.setImage(with: model.userInfo?.headImg?.sy_imageURL())
        self.headPic.sy_setWithUrl(model.userInfo?.headImg)
        self.daibanState.currentVC = self.currentVC
        daibanState.configure(model)
    }
    
    override func initViews() {
        
        tianjia.addAction {[weak self ] in
            if let `self` = self {
                
            }
        }
        
        self.addSubview(myBg)
        myBg.addSubview(tianjia)
        myBg.addSubview(name)
        myBg.addSubview(headPic)
        myBg.addSubview(role)
//        myBg.addSubview(gongsi)
        myBg.addSubview(titleLb)
        self.addSubview(daibanState)
        
        myBg.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.right.equalToSuperview()
            make.height.equalTo(234.autoWidth() + STATUSBAR_HEIGHT)
        }
        tianjia.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(15.autoWidth()+STATUSBAR_HEIGHT)
            make.right.equalToSuperview().offset(-15.autoWidth())
            make.height.width.equalTo(16.autoWidth())
            
        }
        titleLb.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(14.autoWidth()+STATUSBAR_HEIGHT)
            make.centerX.equalToSuperview()
            make.height.equalTo(17.autoWidth())
        }
        name.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(141.autoWidth()+STATUSBAR_HEIGHT)
            make.centerX.equalToSuperview()
            make.height.equalTo(17.autoWidth())
        }
        headPic.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(40.autoWidth()+STATUSBAR_HEIGHT)
            make.centerX.equalToSuperview()
            make.width.height.equalTo(82.autoWidth())
        }
//        gongsi.snp.makeConstraints { (make) in
//            make.top.equalToSuperview().offset(160.autoWidth()+STATUSBAR_HEIGHT)
//            make.centerX.equalToSuperview()
//            make.height.equalTo(22.autoWidth())
//        }
        role.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(44.autoWidth()+STATUSBAR_HEIGHT)
            make.right.equalToSuperview().offset(-15.autoWidth())
//            make.width.equalTo(80.autoWidth())
            make.height.equalTo(30.autoWidth())
        }
        daibanState.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(192.autoWidth()+STATUSBAR_HEIGHT)
            make.left.right.equalToSuperview()
            make.height.equalTo(105.autoWidth())
        }
    }
}
