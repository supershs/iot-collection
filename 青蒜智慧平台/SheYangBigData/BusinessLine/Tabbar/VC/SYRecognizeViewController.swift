//
//  SYRecognizeViewController.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/6/29.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit

class SYRecognizeViewController: SYCameraViewController {

    let cameraView = SYCameraView()
    var category: String!
    var position: String!
    var type: String!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupCamera()
        requestShibieLishi()
    }
    

    func setupCamera() {
        view.addSubview(cameraView)
        cameraView.currentVC = self
        cameraView.clickedClosure = {[weak self] index in
            if let `self` = self {
                switch index {
                case 0:
                    self.toggleCamera()
                case 2:
                    self.photoAction()
                case 4:
                    self.dismissVC()
                default:
                    break
                }
            }
        }
        
        cameraView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.right.bottom.equalToSuperview()
        }
        cameraView.tukuView.passParamsClosure = {[weak self] img in
            if let `self` = self {
                self.requestShibie(img: img as! UIImage)
            }
        }
        cameraView.historyView.passParamsClosure = {[weak self] img in
            if let `self` = self {
                self.requestShibie(img: img as! UIImage)
            }
        }
        self.getImgClosure = {[weak self] img in
            if let `self` = self {
                if let i = img {
                    self.requestShibie(img: i)
                }
            }
        }
        
    }
    
    func requestShibie(img: UIImage) {
        if type == "1" {
            self.requestShibieBing(category: self.category, position: self.position, img: img)
        } else {
            self.requestShibieChong(img: img)
        }
        
    }
    
    func requestShibieBing(category: String, position: String, img: UIImage) {
        
        requestVM.baseRequest(disposeBag: dispose, type: .shibieBing(category: category, position: position, img: img), modelClass: SGBaseModel<SYRecognizeResultsModel>.self) {[weak self] (res) in
            if let `self` = self {
					let id: String = res.data?.id ?? "0"
					self.sy_pushWebVC(.bingchongShibie(id: id))
					self.photoImageview.isHidden = true
				self.removeChildControllers()

            }
            
        } Error: {
            
        }
    }
    
    func requestShibieChong(img: UIImage) {
        
        requestVM.baseRequest(disposeBag: dispose, type: .shibieChong(img: img), modelClass: SGBaseModel<SYRecognizeResultsModel>.self) {[weak self] (res) in
            if let `self` = self {
                let id: String = res.data?.id ?? "0"
                self.sy_pushWebVC(.bingchongShibie(id: id))
                self.photoImageview.isHidden = true
            }
            
        } Error: {
            
        }
    }
    
    func requestShibieLishi() {
        
        requestVM.baseRequest(disposeBag: dispose, type: .shibieLishi(page: 1, size: 1000), modelClass: SGBaseListModel<SYRecognizeModel>.self) {[weak self] (res) in
            if let `self` = self {
                if let s = res.data?.records {
                    self.cameraView.historyView.configureView(s)
                }
            }
            
        } Error: {
            
        }
    }
	
	
	// 移除导航控制器中指定的（单个、多个连续|不连续）控制器
	private func removeChildControllers() {
		// 移除导航控制器中指定的多个连续控制器，⽅法⼀：
		if let tmpControllers = navigationController?.viewControllers {
			var controllers = tmpControllers
			// 从导航控制器的堆栈中连续移除多个维护的控制器，推荐⽤这种⽅法，注意数组越界的问题，因此从最⼤的开始遍历，逐级往最⼩的移除
			for i in (1 ..< (controllers.count-1)).reversed() {
				controllers.remove(at: i)
				navigationController?.viewControllers = controllers
			}
		}
		// 移除导航控制器中指定的多个连续控制器，⽅法⼆：
		//        guard let tmpControllers = navigationController?.viewControllers else { return }
		//        var controllers = tmpControllers
		//
		//        // 从导航控制器的堆栈中连续移除多个维护的控制器，推荐⽤这种⽅法，注意数组越界的问题，因此从最⼤的开始遍历，逐级往最⼩的移除
		//        for i in (1 ..< (controllers.count-1)).reversed() {
		//            controllers.remove(at: i)
		//            navigationController?.viewControllers = controllers
		//        }
		//        // 移除导航控制器中指定的多个不连续控制器，推荐⽤这种⽅法
		//        for (i, controller) in (controllers.enumerated()).reversed() {
		//            if controller.isKind(of: ViewController4.classForCoder()) {
		//                controllers.remove(at: i)
		//                navigationController?.viewControllers = controllers
		//            }
		////
		// 移除导航控制器中指定的（单个、多个连续|不连续）控制器
		//		private func removeChildControllers() {
		//			// 移除导航控制器中指定的多个连续控制器，⽅法⼀：
		//			if let tmpControllers = navigationController?.viewControllers {
		//				var controllers = tmpControllers
		//				// 从导航控制器的堆栈中连续移除多个维护的控制器，推荐⽤这种⽅法，注意数组越界的问题，因此从最⼤的开始遍历，逐级往最⼩的移除
		//				for i in (1 ..< (controllers.count-1)).reversed() {
		//					controllers.remove(at: i)
		//					navigationController?.viewControllers = controllers
		//				}
		//			}
		// 移除导航控制器中指定的多个连续控制器，⽅法⼆：
		//        guard let tmpControllers = navigationController?.viewControllers else { return }
		//        var controllers = tmpControllers
		//
		//        // 从导航控制器的堆栈中连续移除多个维护的控制器，推荐⽤这种⽅法，注意数组越界的问题，因此从最⼤的开始遍历，逐级往最⼩的移除
		//        for i in (1 ..< (controllers.count-1)).reversed() {
		//            controllers.remove(at: i)
		//            navigationController?.viewControllers = controllers
		//        }
		//        // 移除导航控制器中指定的多个不连续控制器，推荐⽤这种⽅法
		//        for (i, controller) in (controllers.enumerated()).reversed() {
		//            if controller.isKind(of: ViewController4.classForCoder()) {
		//                controllers.remove(at: i)
		//                navigationController?.viewControllers = controllers
		//            }
		////
		//	}
	}
}
