//
//  SYHomePageViewController.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2020/8/21.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit
import SDCycleScrollView
import RxSwift
import RxDataSources
import Then
import MJRefresh
import ZLPhotoBrowser
import AVKit
import Photos
import GDPerformanceView_Swift
import MJRefresh
import MAMapKit
import AMapLocationKit

class SYHomePageViewController: SYBaseTableViewController ,AMapLocationManagerDelegate{
    
    
    var infoModels: SYHomePageModel!

    var weatherModel: SYWeatherModel!
    
    var menuModel: SYHomePageMenuModel!
    
    var topView: SYHomePageHeaderView = SYHomePageHeaderView()
    
    var sousuoView: SYHomePageSousuoView = SYHomePageSousuoView()
    
    var mjHeader: MJHeaderView!
    
    var locationManager: AMapLocationManager!
    
    var topBt: UIButton = {
        let v = UIButton()
        v.setImage(UIImage(named: "homepage_top"), for: .normal)
        return v
    }()
    
    var topViewHeight: CGFloat =  160.0 + 240.0.autoWidth() + STATUSBAR_HEIGHT
    
    var nongjiType: NongjiType = .nongji
    
    var nongjiMore = SYHomePageMoreView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 45.autoWidth()))
    
    // 计算大家看cell的高度
    var layout: SGFlowLayout = SGFlowLayout()
    
    var dajiakanHeight = 0
    
    var dajikanImgs:[UIImage] = []
    
    var timer: Timer!
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //性能监测工具
        //        PerformanceMonitor.shared().start()
        self.view.backgroundColor = .white
        
        setTableView()
        setNoDataView()
        
        reloadLists()
        requestUserInfo()
        
        startTimer()
        configLoactionManager()
        NotificationCenter.default.addObserver(self, selector: #selector(refreshMenus), name: .Noti_changeMenus, object: nil)
    }
    
    
    
    
    func configLoactionManager() {
        AMapServices.shared().apiKey = mapKey
        locationManager = AMapLocationManager.init()
        locationManager.delegate = self
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.locatingWithReGeocode = true
        locationManager.distanceFilter = 200
        locationManager.startUpdatingLocation()

    }
    
    
    func amapLocationManager(_ manager: AMapLocationManager!, didFailWithError error: Error!) {
        
        
    }
    
    func amapLocationManager(_ manager: AMapLocationManager!, didUpdate location: CLLocation!, reGeocode: AMapLocationReGeocode!) {
        print("location:{lat:%f; lon:%f; accuracy:%f; reGeocode:%@}", location.coordinate.latitude, location.coordinate.longitude, location.horizontalAccuracy);
        requestDW(longitude: location.coordinate.longitude, latitude: location.coordinate.latitude)
   
        if (reGeocode != nil)
            {
                print("reGeocode:%@", reGeocode);
            }
      let geocoder  = CLGeocoder()
        geocoder.reverseGeocodeLocation(location!) { (placemarks, error) in
        
            print(placemarks)
            
        }
        print(location.coordinate.latitude,location.coordinate.longitude)
    }
    
    
    func startTimer() {
        
        if timer == nil {
            timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(getMsgs), userInfo: nil, repeats: true)
            RunLoop.current.add(timer, forMode: RunLoop.Mode.common)
            timer.fire()
        }
    }
    
    func stopTimer() {
        if self.timer != nil && self.timer.isValid {
            self.timer.invalidate()
            self.timer = nil
        }
    }
    
    @objc func getMsgs() {
//        PYChatManager.share.getMsgs()
        
        
        
    }
    
    //刷新定位
    func requestDW(longitude: Double,latitude: Double) {
        
        requestVM.baseRequest(disposeBag: dispose, type: .chixudingwei(dic:["longitude": "\( longitude)","latitude":"\(latitude)"]), modelClass: SGBaseModel<SYHomePageMenuModel>.self) {[weak self] (res) in
            if let `self` = self {
                
                
//                self.menuModel = res.data ?? SYHomePageMenuModel()
//                DispatchQueue.main.async {
//                    self.topView.configure(self.menuModel)
//                }
//                complete(true)
            }
            
        } Error: {
//            complete(false)
        }
        
    }
    func refreshData() {
        DispatchQueue.main.async {
            self.tableView.mj_header?.beginRefreshing()
        }
    }
    
    func reloadLists() {
        
        // 不是依次执行，只是全部完成后给通知
//        HUDUtil.showBlackIndiView()
        let queue = DispatchQueue(label: "getlist")
        let group  = DispatchGroup()
//        queue.async(group: group, execute: {
//            group.enter()
//            self.requestMenu {[weak self] (result) in
//                if let `self` = self {
//                    DispatchQueue.main.async {
//
//                    }
//                    group.leave()
//                }
//            }
//        })
        queue.async(group: group, execute: {
            group.enter()
            self.requestBanner {[weak self] (result) in
                if let `self` = self {
                    DispatchQueue.main.async {

                    }
                    group.leave()
                }
            }
        })
        
        queue.async(group: group, execute: {
            group.enter()
            self.requestInfo {[weak self] (success) in
                if let `self` = self {
                    DispatchQueue.main.async {
                        
                    }
                    group.leave()
                }
            }
        })
        queue.async(group: group, execute: {
            group.enter()
            self.requestWeather {[weak self] (success) in
                if let `self` = self {
                    DispatchQueue.main.async {
                        
                    }
                    group.leave()
                }
            }
        })
 
        group.notify(queue: queue) {
            DispatchQueue.main.async {
                HUDUtil.hideHud()
                self.tableView.mj_header?.endRefreshing()
                self.loadDataSuccessed()
            }
        }
    }
    

//    func requestMenu( complete:@escaping (Bool)->()) {
//        requestVM.baseRequest(disposeBag: dispose, type: .homepageMenu(state: 1), modelClass: SGBaseModel<SYHomePageMenuModel>.self) {[weak self] (res) in
//            if let `self` = self {
//                self.menuModel = res.data ?? SYHomePageMenuModel()
//                DispatchQueue.main.async {
//                    self.topView.configure(self.menuModel)
//                }
//                complete(true)
//            }
//
//        } Error: {
//            complete(false)
//        }
//
//    }
    
    func requestBanner( complete:@escaping (Bool)->()) {
        requestVM.baseRequest(disposeBag: dispose, type: .homepageBanner(dic: ["indexes":"appFirstPageBanner"]), modelClass: SGBaseNoPageListModel<SYLunboListModel>.self) {[weak self] (res) in
            if let `self` = self {
                DispatchQueue.main.async {
                    self.topView.configure(res.data ?? [])
                }
                complete(true)
            }
            
        } Error: {
            complete(false)
        }
        
    }
    
    //    func requestZhixun( complete:@escaping (Bool)->()) {
    //        requestVM.baseRequest(disposeBag: dispose, type: .homepageZhixun, modelClass: SGBaseNoPageListModel<SYHomePageZhixunModel>.self) {[weak self] (res) in
    //            if let `self` = self {
    //                complete(true)
    //                self.products = res.data ?? []
    //                self.products = [self.products[0]]
    //                self.loadDataSuccessed()
    //            }
    //
    //        } Error: {
    //            complete(false)
    //        }
    //
    //    }
    @objc func refreshMenus() {
        self.requestInfo {[weak self] (success) in
            if let `self` = self {
                
            }
        }
    }
    
    func requestInfo(complete:@escaping (Bool)->()) {
        requestVM.baseRequest(disposeBag: dispose, type: .homepageInfo, modelClass: SGBaseModel<SYHomePageModel>.self) {[weak self] (res) in
            if let `self` = self {
                let model: SYHomePageModel = res.data ?? SYHomePageModel()
                self.infoModels = model
//                let dangjianM = self.infoModels.newsList
                self.topView.configureDangjian([])

//                self.menuModel = model.appMenuVOList
                DispatchQueue.main.async {
                    self.topView.configure(model.appMenuVOList ?? [])
                }
                
//                let dajiakanM = self.infoModels.filter { $0.type == 10 }
//                let dajiakanMs = (dajiakanM.first ?? SYHomePageModel()).info ?? []
//                DispatchQueue.global().async {
//                    self.jisuanDajiakanCellHeight(dajiakanMs)
//                }
                self.setLayout()
                complete(true)
            }
            
        } Error: {
            complete(false)
        }
        
    }
    
    func requestWeather( complete:@escaping (Bool)->()) {
        requestVM.baseRequest(disposeBag: dispose, type: .homepageWeather(cityName: "南京市"), modelClass: SGBaseModel<SYWeatherModel>.self) {[weak self] (res) in
            if let `self` = self {
                self.weatherModel = res.data ?? SYWeatherModel()
                DispatchQueue.main.async {
                    self.topView.configure(self.weatherModel)
                }
                complete(true)
            }
            
        } Error: {
            complete(false)
        }
    }
    
    func requestUserInfo() {
        requestVM.baseRequest(disposeBag: dispose, type: .myInfo, modelClass: SGBaseModel<SYMyInfoModel>.self) {[weak self] (res) in
            if let `self` = self {
                let infoModel = res.data
                UserInstance.userId = infoModel?.id
                UserInstance.avatar = infoModel?.headImg
                UserInstance.nickname = infoModel?.nickName
            }
            
        } Error: {
            
        }
    }
    
    func loadDataSuccessed() {
        DispatchQueue.main.async {
            
            self.setDataSource()
        }
    }
    
    func setNoDataView() {
        noDataView = SYNoDataView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT-NAV_HEIGHT - 200))
        noDataView.isHidden = true
        tableView.addSubview(noDataView)
    }
    
    func setupRefreshHeaderView() {
        
        mjHeader = MJHeaderView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30), refreshText: Constant_Toast.loadingText, height: 30)
        mjHeader.refreshingBlock = {[weak self] in
            if let `self` = self {
                self.reloadLists()
            }
        }
        self.tableView.mj_header = mjHeader
        self.tableView.mj_header?.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30) // 父类有问题 必须要加上，不然顶部会多一块
    }
    
    func setTableView() {
        
        //创建表格视图
        self.tableView = UITableView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT-TABBAR_HEIGHT), style:.grouped)
        self.tableView.separatorStyle = .none
        self.tableView.delegate = self
        self.tableView.backgroundColor = .white
        //        self.tableView.dataSource = self
        topView.currentVC = self
        
        //创建一个重用的单元格
        self.tableView!.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        self.view.addSubview(self.tableView!)
        topMangain(self.tableView!)
        
        tableView.rx.itemSelected
            .subscribe(onNext:{ [weak self] indexPath in
                if let `self` = self {
                    
                }
            })
            .disposed(by:dispose)
        
        sousuoView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: STATUSBAR_HEIGHT + 60.autoWidth())
        sousuoView.isHidden = true
        sousuoView.currentVC = self
        self.view.addSubview(sousuoView)
        setupRefreshHeaderView()
        
        topBt.frame = CGRect(x: SCREEN_WIDTH - 70, y: SCREEN_HEIGHT - 120 - BOTTOM_SAFE_HEIGHT, width: 50, height: 50)
        topBt.isHidden = true
        topBt.addAction {[weak self] in
            if let `self` = self {
                self.toTop()
            }
        }
        self.view.addSubview(topBt)
    }
    
    func setDataSource() {
        
        if nil == infoModels {
            return 
        }
//        let dangjianM = self.infoModels.filter { $0.type == 0 }
        let newsM = self.infoModels.newsList ?? []
//        let wulianM = self.infoModels.newsList
        let productsM = self.infoModels.adoptList ?? []
        let nongjioM = self.infoModels.agroMachineList ?? []
        let zhibaojiM = self.infoModels.droneMachineList ?? []
        let gongxuM = self.infoModels.demandMap ?? SYDemandMapModel()
        let pinpaiM = self.infoModels.publicBrandList ?? []
        let wendaM = self.infoModels.expertForumIssueVOList ?? []
        let tuijianM = self.infoModels.tourScenicSpotVOList ?? []
        let dajiakanM = self.infoModels.tourHotelList ?? []
        
        let listModel = SYNewsListModel()
        
        //初始化数据
        let sections = Observable.just([
            SYNewsListModelSection(header: "", items: [listModel]),
            SYNewsListModelSection(header: "", items: [listModel]),
            SYNewsListModelSection(header: "", items: [listModel]),
//            SYNewsListModelSection(header: "", items: [listModel]),
//            SYNewsListModelSection(header: "", items: [listModel]),
//            SYNewsListModelSection(header: "", items: [listModel])
        ])
        
        //创建数据源
        let dataSource = RxTableViewSectionedAnimatedDataSource<SYNewsListModelSection>(
            //设置单元格
            configureCell: { ds, tv, ip, item in
                switch ip.section {
                case 0:
                    let cell: SYNongyeCell = tv.dequeueReusableCell(withIdentifier: "SYNongyeCell")
                        as? SYNongyeCell ?? SYNongyeCell(style: .default, reuseIdentifier: "SYNongyeCell")
                    cell.currentVC = self
                    cell.configure(newsM, productsM, [])
                    cell.selectionStyle = .none
                    return cell
                case 1:
                    
                    let cell: SYHPNongjiCell = tv.dequeueReusableCell(withIdentifier: "SYHPNongjiCell")
                        as? SYHPNongjiCell ?? SYHPNongjiCell(style: .default, reuseIdentifier: "SYHPNongjiCell")
                    cell.currentVC = self
                    cell.configure(nongjioM, zhibaojiM, gongxuM, self.nongjiType)
                    cell.selectionStyle = .none
                    return cell
                case 2:
                    
//                    let cell: SYHPPinpaiCell = tv.dequeueReusableCell(withIdentifier: "SYHPPinpaiCell")
//                        as? SYHPPinpaiCell ?? SYHPPinpaiCell(style: .default, reuseIdentifier: "SYHPPinpaiCell")
//                    cell.currentVC = self
//                    cell.configure(pinpaiM)
//                    cell.selectionStyle = .none
//                    return cell
//                case 3:
                    
                    let cell: SYHPZhixunCell = tv.dequeueReusableCell(withIdentifier: "SYHPZhixunCell")
                        as? SYHPZhixunCell ?? SYHPZhixunCell(style: .default, reuseIdentifier: "SYHPZhixunCell")
                    cell.currentVC = self
                    cell.configure(wendaM)
                    cell.selectionStyle = .none
                    return cell
                case 4:
                    
                    let cell: SYHPTuijianCell = tv.dequeueReusableCell(withIdentifier: "SYHPTuijianCell")
                        as? SYHPTuijianCell ?? SYHPTuijianCell(style: .default, reuseIdentifier: "SYHPTuijianCell")
                    cell.currentVC = self
                    cell.configure(tuijianM)
                    cell.selectionStyle = .none
                    return cell
                case 5:
                    
                    let cell: SYHPDajiakanCell = tv.dequeueReusableCell(withIdentifier: "SYHPDajiakanCell")
                        as? SYHPDajiakanCell ?? SYHPDajiakanCell(style: .default, reuseIdentifier: "SYHPTuijianCell")
                    cell.currentVC = self
                    cell.configure(dajiakanM, self.getImgUrls(), self.dajiakanHeight, self.layout)
                    cell.selectionStyle = .none
                    return cell
                    
                default:
                    return UITableViewCell()
                }
                
            },
            //设置分区头标题
            titleForHeaderInSection: { ds, index in
                return ds.sectionModels[index].header
            })
        tableView.dataSource = nil
        //绑定单元格数据
        sections
            .bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: dispose)
    }
    
    /*
    // 计算大家看cell的高度
    func jisuanDajiakanCellHeight(_ models: [SYHomePageInfoModel]) {
        
        SYDownloadImage().downloadImages(urls: models.map { ($0.imgUrl?.getEncodeString ?? "") }, noHud: true) {[weak self] (images) in
            if let `self` = self {
                
                self.dajikanImgs = images
                
                let columnCount = 2
                //通过layout的一些参数设置item的宽度
                var ms:[SGFlowLayoutModel] = []
                let itemWidth = (SCREEN_WIDTH - 40) / CGFloat(columnCount)
                self.dajikanImgs.forEach { (img) in
                    let m = SGFlowLayoutModel()
                    let kuanBigao = img.size.width / img.size.height
                    m.itemHeight = itemWidth / kuanBigao
                    ms.append(m)
                }
                
                self.layout.findList = ms
                //设置布局属性
                self.layout.columnCount = columnCount
                self.layout.minimumLineSpacing = 10
                self.layout.minimumInteritemSpacing = 10
                self.layout.scrollDirection = .vertical//.horizontal
                self.layout.maxHeightClosure = {[weak self] height in
                    if let `self` = self {
                        if self.dajiakanHeight != height {
                            
                            self.dajiakanHeight = height
                            DispatchQueue.main.async {
                                
                                self.tableView.reloadSections([5], with: .none)
                            }
                        }
                    }
                }
                DispatchQueue.main.async {
                    self.topView.configure(self.menuModel)
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
            }
        }
    }
 */
    
    func setLayout() {
        
        let dajiakanMs = self.infoModels.tourHotelList ?? []
        
        let columnCount = 2
        //通过layout的一些参数设置item的宽度
        var ms:[SGFlowLayoutModel] = []
        
        for (i, value) in dajiakanMs.enumerated() {
            let m = SGFlowLayoutModel()
            if i != 0 {
                m.itemHeight = CGFloat(220.autoWidth())
            } else {
                m.itemHeight = CGFloat(190.autoWidth())
            }
            ms.append(m)
        }
        
        self.layout.findList = ms
        //设置布局属性
        self.layout.columnCount = columnCount
        self.layout.minimumLineSpacing = 10
        self.layout.minimumInteritemSpacing = 10
        self.layout.scrollDirection = .vertical//.horizontal
    }
    
    func getImgUrls() -> [String] {
        let dajiakanMs = self.infoModels.tourHotelList ?? []
        return dajiakanMs.map { $0.imgUrl ?? "" }
    }
    
    func toTop() {
        self.tableView.contentOffset.y = 0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y > 0 {
            sousuoView.isHidden = false
            topBt.isHidden = false
        } else {
            sousuoView.isHidden = true
            topBt.isHidden = true
        }
    }
}

extension SYHomePageViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
       
		if nil == infoModels {
			return nil
		}
		
		switch section {
        
        case 0:
            let more = SYHomePageMoreView()
            more.title.text = "农业资讯"
            more.more.addAction {[weak self] in
                if let `self` = self {
                    self.sy_pushWebVC(.gengduoZhixun)
                }
            }
            return more
        case 1:
            
//            nongjiMore.subTitle.isHidden = false
            nongjiMore.title.text = "农机服务"
            nongjiMore.more.addAction {[weak self] in
                if let `self` = self {
                    if self.nongjiType == .nongji {
                        self.sy_pushWebVC(.nongjiMore)
                    } else {
                        self.sy_pushWebVC(.zhibaojiMore)
                    }
                }
            }
            nongjiMore.clickedClosure = {[weak self] index in
                if let `self` = self {
                    if index == 0 {
                        self.nongjiType = .nongji
                    } else {
                        self.nongjiType = .zhibaoji
                    }
                    self.tableView.reloadSections([1], with: .none)
                }
            }
            return nongjiMore
        case 2:
            let more = SYHomePageMoreView()
            more.title.text = "公共品牌"
            more.more.addAction {[weak self] in
                if let `self` = self {
                    self.sy_pushWebVC(.pinpaiMore)
                }
            }
            return more
        case 3:
            let more = SYHomePageMoreView()
            more.title.text = "专家问答"
            more.more.addAction {[weak self] in
                if let `self` = self {
                    self.sy_pushWebVC(.wendaMore)
                }
            }
            return more
        case 4:
            let more = SYHomePageMoreView()
//            more.title.text = "景区推荐"
//            more.more.addAction {[weak self] in
//                if let `self` = self {
//                    let vc = DYJQListViewController()
//                    self.pushVC(vc)
//                }
//            }
            return more
        case 5:
            let more = SYHomePageMoreView()
//            more.title.text = "大家都在看"
//            more.more.addAction {[weak self] in
//                if let `self` = self {
//                    let vc = DYZSListViewController(navTitle: "大家都在看")
//                    self.pushVC(vc)
//                }
//            }
            return more
        default:
            return nil
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 45.autoWidth()
        case 1:
            return 45.autoWidth()
        case 2:
            return 45.autoWidth()
        case 3:
            return 45.autoWidth()
        case 4:
            return 45.autoWidth()
        case 5:
            return 45.autoWidth()
        default:
            return 0
            
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 0.01))
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
}

//自定义Section
struct SYNewsListModelSection {
    var header: String
    var items: [Item]
}

extension SYNewsListModelSection : AnimatableSectionModelType {
    typealias Item = SYNewsListModel
    
    var identity: String {
        return header
    }
    
    init(original: SYNewsListModelSection, items: [Item]) {
        self = original
        self.items = items
    }
}

extension SYNewsListModel: IdentifiableType {
    var identity: String {
        return "\(id)"
    }
    
    typealias Identity = String
}


