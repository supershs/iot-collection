//
//  InputView.swift
//  33web
//
//  Created by 宋海胜 on 2020/12/3.
//

import UIKit
import SwiftWeb

class InputView: UIView {

    fileprivate var urlTf: UITextField!
    fileprivate var cancelBtn: UIButton!
    fileprivate var loadBtn: UIButton!
    fileprivate var currentVC:HSWebViewController!
    
    init(frame: CGRect, currentVC: HSWebViewController) {
        super.init(frame: frame)
        self.currentVC = currentVC
        initViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initViews() {
        
        urlTf = UITextField(frame: CGRect(x: 0, y: 0, width: kScreenWidth - 100, height: 50))
        urlTf.placeholder = "前端地址如：\(WEBIP)"
        urlTf.textColor = .black
        urlTf.font = UIFont.systemFont(ofSize: 14)
        urlTf.backgroundColor = UIColor.white
        urlTf.layer.borderWidth = 1
        urlTf.layer.borderColor = UIColor.lightGray.cgColor
        self.addSubview(urlTf)
        cancelBtn = UIButton()
        cancelBtn.frame = CGRect(x: kScreenWidth - 50, y: 0, width: 50, height: 50)
        cancelBtn.backgroundColor = .blue
        cancelBtn.addTarget(self, action: #selector(cancelBtnAction), for: .touchUpInside)
        cancelBtn.setTitle("取消", for: .normal)
        cancelBtn.setTitleColor(.white, for: .normal)
        self.addSubview(cancelBtn)
        loadBtn = UIButton()
        loadBtn.frame = CGRect(x: kScreenWidth - 100, y: 0, width: 50, height: 50)
        loadBtn.backgroundColor = .purple
        loadBtn.addTarget(self, action: #selector(loadBtnAction), for: .touchUpInside)
        loadBtn.setTitle("加载", for: .normal)
        loadBtn.setTitleColor(.white, for: .normal)
        self.addSubview(loadBtn)
    }
    
    @objc func loadBtnAction() {
        WEBIP = self.urlTf.text!
        if let url = URL(string: WEBIP + LOGINURL) {
            currentVC.webView.load(URLRequest(url: url))//load需在addsubview之后
            self.isHidden = true
        } else {
            currentVC.view.makeToast("请输入正确地址", duration: 1.5, position: .center, completion: nil)
        }
        
    }
    
    @objc func cancelBtnAction() {
        self.isHidden = true
    }
}


class APIInputView: InputView {
    
    override func initViews() {
        super.initViews()
        
        urlTf.placeholder = "后台地址如：\(IP)"
        loadBtn.backgroundColor = .orange
        cancelBtn.backgroundColor = .green
    }
    
    override func loadBtnAction() {
        IP = self.urlTf.text!
        IMGIP = self.urlTf.text!
        self.isHidden = true
    }
}
