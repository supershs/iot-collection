//
//  Utils.swift
//  33web
//
//  Created by 宋海胜 on 2020/12/2.
//

import UIKit

class Utils: NSObject {

    class func toPay(_ body: [String: Any]) {
        let payWay = body["payWay"] as? Int ?? 0
        let nextURL = body["nextURL"] as? String ?? ""
        UserDefaults.standard.set(kWebUrl + "/" + nextURL, forKey: "nextURL")
        
        if payWay == 1 {
            let partnerId = body["partnerId"] as? String ?? ""
            let prepayId = body["prepayId"] as? String ?? ""
            let package = body["package"] as? String ?? ""
            let nonceStr = body["nonceStr"] as? String ?? ""
            let timeStamp = body["timeStamp"] as? String ?? ""
            let sign = body["sign"] as? String ?? ""
            print("\(partnerId), \(prepayId), \(package), \(nonceStr), \(timeStamp), \(sign)")
            WXManager.weixinPay(partnerId, prepayId, package, nonceStr, UInt32(timeStamp)!, sign)
        } else {
            let orderInfo = body["orderInfo"] as? String ?? ""
            print(orderInfo)
//            AliPayDelegate.alipay(orderInfo)
        }
    }
    
    class func toPay(paytype: PayType, currentVC: UIViewController? = nil, orderInfo: String? = nil, tn: String? = nil, body: [String: String]? = nil) {
        
        if paytype == .wechatPay {
            self.wxPay(body!)
        } else if paytype == .aliPay {
            print(orderInfo!)
//            AliPayDelegate.alipay(orderInfo!)
        } else {
            YinlianPayDelegate.yinlianPay(tn: tn!, mode: "01", currentVC: currentVC!)
        }
    }
    
    class func wxPay(_ body: [String: String]) {
        let partnerId = body["partnerId"] ?? ""
        let prepayId = body["prepayId"]  ?? ""
        let package = body["package"]  ?? ""
        let nonceStr = body["nonceStr"]  ?? ""
        let timeStamp = body["timeStamp"]  ?? ""
        let sign = body["sign"]  ?? ""
        print("\(partnerId), \(prepayId), \(package), \(nonceStr), \(timeStamp), \(sign)")
        WXManager.weixinPay(partnerId, prepayId, package, nonceStr, UInt32(timeStamp)!, sign)
    }
}
