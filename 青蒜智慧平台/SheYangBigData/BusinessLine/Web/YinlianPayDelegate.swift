//
//  YinlianPayDelegate.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/7/2.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit

class YinlianPayDelegate: NSObject {

    /*
     * tn       交易流水号，商户后台向银联后台提交订单信息后，由银联后台生成并下发给商户后台的交易凭证；
     * mode     "00"代表接入生产环境（正式版本需要）；"01"代表接入开发测试环境（测试版本需要）；
     */
    
    class func yinlianPay(tn: String, mode: String = "01", currentVC: UIViewController) {
        if UPPaymentControl.default()?.isPaymentAppInstalled() == true {
            if !tn.isEmpty {
                UPPaymentControl.default()?.startPay(tn, fromScheme: "", mode: mode, viewController: currentVC)
            }
        }
    }
    
    class func payResult(url: URL) {
        
        UPPaymentControl.default()?.handlePaymentResult(url, complete: { (code, dic) in
            if code == "success" {
                //结果code为成功时，去商户后台查询一下确保交易是成功的再展示成功
                NotificationCenter.default.post(name: .Noti_yinlianPayResult, object: ["paytype": "yl", "result": "0"])
            } else if code == "fail" {
                NotificationCenter.default.post(name: .Noti_yinlianPayResult, object: ["paytype": "yl", "result": "1"])
            } else if code == "cancel" {
                NotificationCenter.default.post(name: .Noti_yinlianPayResult, object: ["paytype": "yl", "result": "2"])
            }
            
        })
    }
}
