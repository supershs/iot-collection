 //
//  TokenGet.h
//  GuangLingWuLian
//
//  Created by 宋海胜 on 2020/10/29.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TokenGet : UIView

-(NSString *)getTokenStr:(NSData *)deviceToken;

@end

NS_ASSUME_NONNULL_END
