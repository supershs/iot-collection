//
//  ZGLiveMiddleView.swift
//  ZhangGuang
//
//  Created by 叁拾叁 on 2020/7/20.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit
import SnapKit
import SwiftWeb

let kIdentifyLength: CGFloat =  30

class ZGLiveMiddleView: UIView, UIScrollViewDelegate, UIGestureRecognizerDelegate {

    var showClosure: (() -> Void)?
    var rotatClosure: ((Int, Double) -> Void)?
    var lastContentOffsetX: CGFloat = 0.0
    var lastContentOffsetY: CGFloat = 0.0
    var isClose: Bool = false
    var location: CGPoint!
    var blanner: UIView! {
        didSet {
            self.aboveView.blanner = blanner
        }
    }
    
    var voiceView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor(white: 0, alpha: 0.3)
        v.layer.cornerRadius = 5
        v.isHidden = true
        return v
    }()
    
    let voiceImage: UIImageView = {
        let v = UIImageView(image: UIImage(named: "voice_default"))
        v.isUserInteractionEnabled = true
        return v
    }()
    
    let voiceLb: UILabel = {
        let v = UILabel()
        let attStr = NSMutableAttributedString(string: "对讲已开启，请说话", attributes: [NSAttributedString.Key.font: UIFont(name: "PingFang SC", size: 15) as Any, NSAttributedString.Key.foregroundColor: UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)])
        v.attributedText = attStr
        v.textAlignment = .center
        v.isUserInteractionEnabled = true
        return v
    }()
    
    let aboveView: ZGLiveScrollView = {
        let v = ZGLiveScrollView()
        v.backgroundColor = UIColor.clear
        v.showsVerticalScrollIndicator = false
        v.showsHorizontalScrollIndicator = false
        v.contentSize = CGSize(width: kScreenHeight * 2, height: kScreenWidth * 2)
        v.isUserInteractionEnabled = true
        v.isMultipleTouchEnabled = true
        v.minimumZoomScale = 1.0;
        v.maximumZoomScale = 4.0;
        return v
    }()
    
    let singleTapGesture: UITapGestureRecognizer = {
        // 单击消失
        let g = UITapGestureRecognizer()
        g.numberOfTapsRequired = 1
        return g
    }()
    
    let doubleTapGesture: UITapGestureRecognizer = {
        // 双击聚焦
        let g = UITapGestureRecognizer()
        g.numberOfTapsRequired = 2
        return g
    }()
    
    let pinchGesture: UIPinchGestureRecognizer = {
        let g = UIPinchGestureRecognizer()
        return g
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        initViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    fileprivate func initViews() {
        
        self.addSubview(voiceView)
        self.voiceView.addSubview(voiceImage)
        self.voiceView.addSubview(voiceLb)
        self.addSubview(aboveView)
        aboveView.addGestureRecognizer(singleTapGesture)
        aboveView.addGestureRecognizer(doubleTapGesture)
        aboveView.addGestureRecognizer(pinchGesture)
        aboveView.restorationIdentifier = "aboveView"
        aboveView.delegate = self
        singleTapGesture.addTarget(self, action: #selector(singleAction))
        doubleTapGesture.addTarget(self, action: #selector(doubleAction))
        pinchGesture.addTarget(self, action: #selector(pinchGestureAction))
        singleTapGesture.delegate = self
        doubleTapGesture.delegate = self
        pinchGesture.delegate = self
        
        
        voiceView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.equalTo(180)
            make.height.equalTo(148)
        }
        
        voiceImage.snp.makeConstraints { (make) in
            
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(21)
            make.height.equalTo(63.5)
            make.width.equalTo(81.5)
        }
        
        voiceLb.snp.makeConstraints { (make) in

            make.centerX.equalToSuperview()
            make.top.equalTo(self.voiceImage.snp.bottom).offset(24)
            make.height.equalTo(20)
            make.width.equalTo(180)
        }
        
        aboveView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        // 只有当没有检测到双击时才执行单击操作
        singleTapGesture.require(toFail: doubleTapGesture)
    }
    
    @objc func singleAction() {
        if let s = showClosure {
            s()
        }
    }
    
    @objc func doubleAction() {
        if isClose {
            if let c = rotatClosure {
                c(11, 0)
            }
            isClose = false
        } else {
            if let c = rotatClosure {
                c(10, 0)
            }
            isClose = true
        }
    }
    
    @objc func pinchGestureAction(_ pinchGesture: UIPinchGestureRecognizer) {
        
        if pinchGesture.state == .ended {
            
            if pinchGesture.scale < 1 {
                if let c = rotatClosure {
                    c(9, 0)
                }
            } else {
                if let c = rotatClosure {
                    c(8, 0)
                }
            }
            print("捏合比列：\(pinchGesture.scale)")
            print("捏合速度\(pinchGesture.velocity)")
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        lastContentOffsetY = scrollView.contentOffset.y;
        lastContentOffsetX = scrollView.contentOffset.x;
    }
    
    // 一次滑动手势，判断只执行一次
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        let y = scrollView.contentOffset.y
        let x = scrollView.contentOffset.x
        let up = lastContentOffsetY - kIdentifyLength
        let down = lastContentOffsetY + kIdentifyLength
        let left = lastContentOffsetX - kIdentifyLength
        let right = lastContentOffsetX + kIdentifyLength
        if let c = rotatClosure {
            if(x < left && y < up){
                
                c(4, getProPortion(x, 0))
                
            }else if(x < left && y > down){
                
                c(5, getProPortion(x, 0))
                
            } else if(x > right && y < up){
                
                c(6, getProPortion(x, 0))
                
            } else if(x > right && y > down){
                
                c(7, getProPortion(x, 0))
                
            }else if(y < up){
                
                c(0, getProPortion(up, y))
                
            } else if(y > down){
                
                c(1, getProPortion(y, down))
                
            } else if(x < left){
                
                c(2, getProPortion(left, x))
                
            }else if(x > right){
                
                c(3, getProPortion(x, right))
            }
        }
    }
    
    func getProPortion(_ max: CGFloat, _ min: CGFloat) -> Double {
        return Double(abs(max - min)/30)
    }
    
    /// scrollViewDelegate
//    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
//        return blanner
//    }
}
