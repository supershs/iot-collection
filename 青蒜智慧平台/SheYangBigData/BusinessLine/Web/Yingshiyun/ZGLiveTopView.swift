//
//  ZGLiveTopView.swift
//  ZhangGuang
//
//  Created by 叁拾叁 on 2020/7/20.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit

class ZGLiveTopView: UIView {

    var backClosure: (() -> Void)?
    
    let topView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor(white: 0, alpha: 0.5)
        return v
    }()
    
    let backImgView: UIImageView = {
        let v = UIImageView()
        v.sy_name("liveback")
        return v
    }()
    
    let backBt: UIButton = {
        let v = UIButton()
        v.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        v.titleEdgeInsets = UIEdgeInsets(top: 1, left: 20, bottom: 0, right: 0)
        return v
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    fileprivate func initViews() {
        
        self.addSubview(topView)
        topView.addSubview(backBt)
        backBt.addSubview(backImgView)
        backBt.addTarget(self, action: #selector(backBtAction), for: .touchUpInside)
        
        topView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.height.equalToSuperview()
        }
        
        backBt.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(10)
            make.left.equalToSuperview().offset(15)
            make.right.equalToSuperview()
            make.height.equalTo(40)
        }
        
        backImgView.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview()
            make.width.equalTo(8)
            make.height.equalTo(14)
        }
    }
    
    @objc func backBtAction() {
        if let b = backClosure {
            b()
        }
    }
}
