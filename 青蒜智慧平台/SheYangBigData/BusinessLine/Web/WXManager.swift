//
//  WXManager.swift
//  SwiftWeb
//
//  Created by 宋海胜 on 2020/12/1.
//

import UIKit

class WXManager: NSObject {
    
    class func weixinPay(_ partnerId: String, _ prepayId: String, _ package: String, _ nonceStr: String, _ timeStamp: UInt32, _ sign: String) {
        let request: PayReq = PayReq()
        //商户号(固定的)
        request.partnerId = partnerId
        //统一下单返回的预支付交易会话ID
        request.prepayId = prepayId
        //扩展字段(固定的)
        request.package = "Sign=WXPay"
        //随机字符串
        request.nonceStr = nonceStr
        //时间戳(10位)
        request.timeStamp = timeStamp
        //签名
        request.sign = sign
//        WXApi.send(request) { (state) in
//            if (state) {
//                print("调起微信成功");
//            } else {
//                print("调起微信失败");
//            }
//        }
        WXApi.send(request)
    }

    class func toMiniProgram(_ appId: String, _ path: String, _ currentVC: UIViewController) {
        
        /*
         参数说明：
         userName：拉起的小程序的username-原始ID，这个在小程序->设置中可以看见，必须 必须 必须 以 （gh） 开头的 重要的事情说3遍
         launchMiniProgramReq.path ="page/index?key1=xxxx";//类似http的url方法来传递参数，如果不传，默认进入小程序首页！
         */
        // 叁拾叁电商 "gh_2384db691736"
        // 盱眙龙虾 "gh_5e848ada2733"
        // WechatOpenSDK 1.8.5的方法
        if #available(iOS 13.0, *){
            miniprogramReq(appId)
        }  else{
            if (WXApi.isWXAppInstalled()) {
                miniprogramReq(appId)
            } else {
                currentVC.view.makeToast("未安装微信", duration: 1.5, position: .center, completion: nil)
            }
        }
        
        /*
         // WechatOpenSDK 1.8.6及其以后的版本用一下方法
         // 如果是iOS13之后用的Universal Links方式跳转 则不需要判断是否安装微信
         let launchMiniProgramReq = WXLaunchMiniProgramReq.object()
         launchMiniProgramReq.userName = "gh_2384db691736"//"gh_5e848ada2733"//appId //拉起的小程序的username-原始ID
         //            launchMiniProgramReq.path = nil //拉起小程序页面的可带参路径，不填默认拉起小程序首页
         launchMiniProgramReq.miniProgramType = WXMiniProgramType.release //拉起小程序的类型
         WXApi.send(launchMiniProgramReq) { (state) in
         print("state:\(state)")
         }
         */
    }
    
    class func miniprogramReq(_ appId: String) {
        
        let launchMiniProgramReq = WXLaunchMiniProgramReq.object()
        launchMiniProgramReq.userName = appId //拉起的小程序的username-原始ID
        //            launchMiniProgramReq.path = nil //拉起小程序页面的可带参路径，不填默认拉起小程序首页
        launchMiniProgramReq.miniProgramType = WXMiniProgramType.release //拉起小程序的类型
        WXApi.send(launchMiniProgramReq)
    }
}
