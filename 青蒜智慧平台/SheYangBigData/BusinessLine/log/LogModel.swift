//
//  LogModel.swift
//  33web
//
//  Created by JAY on 2023/5/9.
//

import UIKit

import HandyJSON


struct NSRootModel<T: HandyJSON> : HandyJSON{
    ///
    var date: String?
    ///
    var data: T?
    /// SUCCESS
    var status: String?
    /// 10000
    var code: Int = 0
    /// 登录成功
    var message: String?

}

struct LoginModel : HandyJSON {
    ///
       var token: String?
       ///
//       var userInfo: NSUserInfoModel?
    var userInfo: NSUserInfoTModel?

}

struct NSUserInfoTModel : HandyJSON {

    /// 16806
    var jobNumber: String? 
    /// <#泛型#>
    var deptId: Any?
    /// <#泛型#>
    var brithday: Any?
    ///
//    var menuList: [Any]?
    ///
    var phone: String?
    /// updateBy
    var updateBy: Int = 0
    /// admin
    var loginName: String?
    ///
    var salt: String?
    ///
    var mobile: String?
    /// <#泛型#>
    var registerDate: Any?
    /// 0
    var sort: Int = 0
    /// 1
    var sex: Int = 0
    /// <#泛型#>
    var idCard: Any?
    /// PC
    var type: String?
    /// <#泛型#>
    var deviceId: Any?
    /// 1
    var status: Int = 0
    /// 1
    var tenantId: Int = 0
    ///
    var updateDate: String?
    /// 前端开发
    var roleName: String?
    ///
    var remarks: String?
    /// <#泛型#>
    var createDate: Any?
    /// <#泛型#>
    var version: Any?
    /// id
    var id: String?
    /// 管理员
    var trueName: String?
    ///
    var headImg: String?
    ///
    var password: String?
    /// <#泛型#>
    var monitorId: Any?
    /// <#泛型#>
    var lastLoginTime: Any?
    ///
    var uuid: String?
    ///
    var email: String?
    /// ADMIN
    var userType: String?
    /// <#泛型#>
    var createBy: Any?
    ///
    var deptName: String?

   
}
