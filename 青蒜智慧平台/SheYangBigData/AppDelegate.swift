//
//  AppDelegate.swift
//  SheYangBigData
//eeee
//  Created by 叁拾叁 on 2020/8/3.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit
import SwiftDate
import CallKit
import RongIMKit
import AFNetworking
import PushKit

let RCIMClientKey = "0vnjpoad0isgz"//"vnroth0kvlt0o"//

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var canAllButUpsideDown: Bool = false
    var wxDelegate = WXDelegate()
//    var loginVC = LogOnViewController()
    var loginVC = JDLoginViewController()
	var navc:SYBaseNavigationController!
	var webLoginVC = HSWebViewController(path: kWebUrl, isLoginPage: false, isFirstLogin: false, hasTab: true)
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // 全局设置时区为设备当前地区
        SwiftDate.defaultRegion = Region.local
        
        window = UIWindow(frame: UIScreen.main.bounds)
		//配置融云Key
		RCIMClient.shared()?.initWithAppKey(RCIMClientKey);
		RCCall.shared()
//		navc =  LogOnViewController (rootViewController: webLoginVC)
        
//        self.checkToken()
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = UINavigationController(rootViewController: loginVC)
        window!.makeKeyAndVisible()
        
        _ = self.sy_application(application, didFinishLaunchingWithOptions: launchOptions)
//		UserDefaults.standard.setValue(ryToken, forKey: "ryToken")
		
	let ryToken	= UserDefaults.standard.object(forKey: "ryToken") as? String ?? ""
		
		if !ryToken.isEmpty {
			
			connectToRy(ryToken)
		}

        return true
    }
	
	func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
		if webLoginVC.canAllButUpsideDown {
			return UIInterfaceOrientationMask.allButUpsideDown
		} else {
			return UIInterfaceOrientationMask.portrait
		}
	}

	func applicationDidEnterBackground(_ application: UIApplication) {
		webLoginVC.liveView.stopLiveVideo()
	}
	
	func applicationWillEnterForeground(_ application: UIApplication) {
		webLoginVC.liveView.startLiveVideo()
	}
	
	
	func watchNetwork() {

		AFNetworkReachabilityManager.shared().startMonitoring()
		AFNetworkReachabilityManager.shared().setReachabilityStatusChange { (status) in
			switch status {
			case .unknown:
				print("watchNetwork: 未知")
				self.webLoginVC.liveView.stopLiveVideo()
				
			case .notReachable:
				print("watchNetwork: 未连接")
				self.webLoginVC.liveView.stopLiveVideo()
				
			case .reachableViaWiFi:
				print("watchNetwork: wifi连接")
				self.webLoginVC.liveView.reloadVideo()
				
			case .reachableViaWWAN:
				print("watchNetwork: wwan连接")
				self.webLoginVC.liveView.reloadVideo()
			}
		}
	}
	
	// 电话状态监听
	func callObserver(_ callObserver: CXCallObserver, callChanged call: CXCall) {
		
		print("电话状态监听: \n      call.isOutgoing: \(call.isOutgoing) \n      call.isOnHold: \(call.isOnHold) \n      call.hasEnded: \(call.hasEnded) \n      call.hasConnected: \(call.hasConnected) \n      call.uuid: \(call.uuid)")
		
		if call.isOutgoing {// 拨打
			if call.hasConnected {
				if call.hasEnded {
					print("watchCallStatus: 拨打->接通->结束")
					webLoginVC.liveView.startLiveVideo()
				} else {
					print("watchCallStatus: 拨打->接通")
				}
			} else if call.hasEnded {
				print("watchCallStatus: 拨打->结束")
				webLoginVC.liveView.startLiveVideo()
			} else {
				print("watchCallStatus: 拨打")
			}
		} else if call.isOnHold {// 待接通
			print("watchCallStatus: 待接通")
		} else if call.hasConnected {// 接通
			if call.hasEnded {// 结束
				webLoginVC.liveView.startLiveVideo()
				print("watchCallStatus: 接通->结束")
			} else {
				print("watchCallStatus: 接通")
			}
		} else if call.hasEnded {// 结束
			webLoginVC.liveView.startLiveVideo()
			print("watchCallStatus: 结束")
		} else {
			print("watchCallStatus: \(call.uuid)")
			webLoginVC.liveView.stopLiveVideo()
		}
	}
	//连接IM 服务
	func connectToRy(_ token: String) {
		if RCIMClient.shared().getConnectionStatus() == .ConnectionStatus_Connected {
					
			RCIMClient.shared().disconnect(false)
				
			print("disconnect")
			  
		}
		
		RCIMClient.shared()!.connect(withToken: token, dbOpened: { (code) in
			print("融云code====\(code.rawValue)")
			if code.rawValue == 34001 {
				print("连接已经存在，不需要重复连接")
			} else if code.rawValue == 0 {

			}
		}, success: { (userId) in
			print("userId: \(userId ?? "")")
		}, error: { (status) in
			print("error: \(status)")
		}) {
			print("tokenIncorrect")
		}
			  
	
	}
	
//    func checkToken() {
//
//        let token = UserInstance.accessToken
//        if !(token ?? "").isEmpty {
//            let roleId = UserInstance.roleId
//            tabbarVC = SYBaseTarBarViewController(roleId: roleId)
//            navc =  SYBaseNavigationController (rootViewController: tabbarVC)
//        } else {
//            navc =  SYBaseNavigationController (rootViewController: webLoginVC)
//        }
//    }
}

