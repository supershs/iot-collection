//
//  Constant.swift
//  vgbox
//
//  Created by wangzhiyuan on 2017/5/24.
//  Copyright © 2017年 Swift Xcode. All rights reserved.
//

import UIKit
import SnapKit


public let SCREEN_HEIGHT: CGFloat = UIScreen.main.bounds.size.height

public let SCREEN_WIDTH: CGFloat = UIScreen.main.bounds.size.width

public let IS_PHONE = Bool(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone)

public let IS_PAD = Bool(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad)

public let IS_PHONEX = Bool(SCREEN_WIDTH >= 375.0 && SCREEN_HEIGHT >= 812.0 && IS_PHONE)

public let NAV_HEIGHT = CGFloat(IS_PHONEX ? 88 : 64)

public let STATUSBAR_HEIGHT = CGFloat(IS_PHONEX ? 44 : 20)

public let TABBAR_HEIGHT = CGFloat(IS_PHONEX ? (49 + 34) : 49)

public let TOP_SAFE_HEIGHT = CGFloat(IS_PHONEX ? 44 : 0)

public let BOTTOM_SAFE_HEIGHT = CGFloat(IS_PHONEX ? 34 : 0)

/// 分割线高度
let LINE_HEIGHT: CGFloat = 1.0 / UIScreen.main.scale

/// 获得距离通过比例
func BY_PROPORTION_GET_WIDTH(width: CGFloat) -> CGFloat {
    let proportion: CGFloat = 375 / width
    return CGFloat(Double(UIScreen.main.bounds.width / proportion))
}

func BY_PROPORTION_GET_HEIGHT(height: CGFloat) -> CGFloat {
    let proportion: CGFloat = 667 / height
    return CGFloat(Double(UIScreen.main.bounds.height / proportion))
}

func BY_PROPORTION_GET_FONTSIZE(font: CGFloat) -> CGFloat {
    return IS_PHONEX ? CGFloat(Double(font * 1.2)) : font
}


///与界面相关的常量
struct Constant {
    
    /// 约束高优先级
    static let HEIGHT_PRIORITY = ConstraintPriority.high
    /// 约束低优先级
    static let LOW_PRIORITY = ConstraintPriority.low
    
    
    // 注意内外变局区分
    /// 距离左边 10
    static let marginToLeft: CGFloat = 10
    /// 距离右边 10
    static let marginToRight: CGFloat = 10
    /// 距离左边内边距
    static let paddingToLeft: CGFloat = 12
    /// 距离右边内边距
    static let paddingToRight: CGFloat = 12
    /// 距离顶部内边距
    static let paddingToTop: CGFloat = 10
    /// 距离底部内边距
    static let paddingToBootom: CGFloat = 10
    
    // MARK: - COLOR -
    /// 线条颜色
    static let lineColor:UIColor = UIColor(hex: 0xF2F2F2)
    
    static let bgViewColor:UIColor = UIColor(hex: 0xF2F2F2)
    
    static let blueColor:UIColor = UIColor(hex: 0x367FF6)
    
    static let greenColor:UIColor = UIColor(hex: 0x01A987)
    
    static let redColor:UIColor = UIColor(hex: 0xF55035)
    
    static let color_33:UIColor = UIColor(hex: 0x333333)
    
    static let color_66:UIColor = UIColor(hex: 0x666666)
    
    static let color_99:UIColor = UIColor(hex: 0x999999)
    
    static let color_1E:UIColor = UIColor(hex: 0x1E1E1E)
    
    static let blackColor :UIColor = UIColor.black
    
    /// 灰色（153）
    static let color_104:UIColor = UIColor(r: 153, g: 153, b: 153, a: 1.0)
	static let  mainColor: UIColor = UIColor(hex: 0x31BC91)

   
    
    // MARK: - FONT -
    // 17 15 14 13 12 18 10 9 16 11
    /// 17
    static let font_1: CGFloat = 17
}

