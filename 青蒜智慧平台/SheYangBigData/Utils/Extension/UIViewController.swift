//
//  UIViewController.swift
//  vgbox
//
//  Created by 宋海胜 on 2019/6/25.
//  Copyright © 2019 Swift Xcode. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func vg_present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        viewControllerToPresent.modalPresentationStyle = UIModalPresentationStyle.fullScreen
        self.present(viewControllerToPresent, animated: flag, completion: completion)
        
    }
    
    /// 设置状态栏颜色
    /// - Parameter style: 传入.default是黑色 lightContent是白色
    func setStatubarStyle(style: UIStatusBarStyle) {
        if style == .default{
            if #available(iOS 13.0, *) {
                UIApplication.shared.setStatusBarStyle(.darkContent, animated: true)
            } else {
                UIApplication.shared.setStatusBarStyle(.default, animated: true)
            }
        }else{
            UIApplication.shared.setStatusBarStyle(style, animated: true)
        }
    }
    
    func dismissViewController() {
        if let _ = navigationController {
            navigationController?.popViewController(animated: true)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
    
    func sy_push(_ vc: UIViewController, _ animated: Bool = true) {
        self.navigationController?.pushViewController(vc, animated: animated)
    }
    
    func sy_popVC() {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
}

extension UINavigationController {
    // 用于获取vc数组中需要的vc
    func fetchVCByOffsetCurrent(offset: Int, classType: AnyClass) -> Any? {
        let vcs = self.viewControllers
        let target = vcs[vcs.count - offset - 1]
        if target.isMember(of: classType) {
            return target
        } else {
            return nil
        }
    }
}

