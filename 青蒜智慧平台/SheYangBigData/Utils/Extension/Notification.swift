//
//  Notification.swift
//  vgbox
//
//  Created by 宋海胜 on 2019/6/26.
//  Copyright © 2019 Swift Xcode. All rights reserved.
//

import Foundation

extension Notification.Name {
    
    /// 跳转到登录页面
    static let Noti_SkipToLoginView = Notification.Name("SkipToLoginView")
    
    /// my页面refresh
    static let Noti_myPageRefreshData = Notification.Name("myPageRefreshData")
    
    /// 登录后获取user info
    static let Noti_getUserInfo = Notification.Name("getUserInfo")
    
    /// change role
    static let Noti_changeRole = Notification.Name("changeRole")
    
    /// get im msgs
    static let Noti_getImMsgs = Notification.Name("getImMsgs")
    
    /// change to login
    static let Noti_changeToLogin = Notification.Name("changeToLogin")
    
    /// aliPayResult
    static let Noti_aliPayResult = Notification.Name("aliPayResult")
    
    /// wxPayResult
    static let Noti_wxPayResult = Notification.Name("wxPayResult")
    
    /// yinlianPayResult
    static let Noti_yinlianPayResult = Notification.Name("yinlianPayResult")
    
    /// refreshPYQCommentData
    static let Noti_refreshPYQCommentData = Notification.Name("refreshPYQCommentData")
    
    /// homepageBanner
    static let Noti_homepageBanner = Notification.Name("homepageBanner")
    
    /// changeMenus
    static let Noti_changeMenus = Notification.Name("changeMenus")
    
    /// 取消订单
    static let Noti_orderChange = Notification.Name("orderChange")
    
}
