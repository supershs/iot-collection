//
//  VGTitleImgView.swift
//  vgbox
//
//  Created by super song on 2019/8/14.
//  Copyright © 2019 Swift Xcode. All rights reserved.
//  图片 + 文字

import UIKit

/// 图片 + 文字
class RYTitleImgView: VGTitleImgView {
    
    
    
    public var isSelected: Bool = false {
        didSet {
            if isSelected {
                clickCount = 1
                imageV.sy_name(selectName)
                selected = true
            } else {
                imageV.sy_name(normalName)
                selected = false
                clickCount = 0
            }
        }
    }
    
    public var selected: Bool = false
    
    var normalName: String = "xuanze"
    var selectName: String = "xuanze_select"
    
    override func tapGesAction() {
        clickCount += 1
        if clickCount%2 == 0 {
            isSelected = false
            super.tapClosure?(self.tag, false)
        } else {
            isSelected = true
            super.tapClosure?(self.tag, true)
        }
    }
}


/// 图片 + 文字
class VGTitleImgView: VGBaseTitleImgView {
    
    // 控制点击状态的值  0 normal, 1 select
    public var clickCount: Int = 0
    
    override func tapGesAction() {
        clickCount += 1
        if clickCount%2 == 0 {
            textLb.textColor = UIColor(hex:0x333333)
            imageV.sy_name("xuanze_select")
            super.tapClosure?(self.tag, true)
        } else {
            textLb.textColor = UIColor(hex:0x999999)
            imageV.sy_name("xuanze")
            super.tapClosure?(self.tag, false)
        }
        
    }
}

enum TitleImgType {
    case imgTop
    case imgLeft
    case imgBottom
    case imgRight
}

enum TitleImgShowType {
    case normal     // 固定frame时用 内容居中
    case byContent // 自适应宽度
}

/// 图片 + 文字
class VGBaseTitleImgView: UIView {
    
    
    
    // 图片 文字之间距离
    public var imgTitleMargin: CGFloat = 4
    
    // 图片 与父视图之间距离
    public var imgSuperViewMargin: CGFloat = 4
    
    public var textSuperViewMargin: CGFloat = 0
    
    // 点击回调
    public var tapClosure: ((Int, Bool)->Void)?
    
    public var textLb: UILabel = {
        let lb: UILabel = UILabel()
        lb.isUserInteractionEnabled = true
        lb.textColor = UIColor(hex:0x999999)
        lb.font = UIFont.systemFont(ofSize: 15)
        return lb
    }()
    
    public var imageV: UIImageView = {
        let img: UIImageView = UIImageView()
        img.isUserInteractionEnabled = true
        return img
    }()
    
    public var bgView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = .white
        return v
    }()
    
    // 显示布局模式
    fileprivate var type: TitleImgType = .imgLeft
    
    // 内容填充模式
    fileprivate var showType: TitleImgShowType = .byContent
    
    fileprivate lazy var tapGes: UITapGestureRecognizer = {
        let ges: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapGesAction))
        return ges
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initViews(frame: frame)
    }
    
    init(frame: CGRect, type: TitleImgType = .imgLeft, showType: TitleImgShowType = .byContent, imgSuperViewMargin: CGFloat = 4, imgTitleMargin: CGFloat = 4, textSuperViewMargin: CGFloat = 0) {
        super.init(frame: frame)
        self.imgSuperViewMargin = imgSuperViewMargin
        self.imgTitleMargin = imgTitleMargin
        self.textSuperViewMargin = textSuperViewMargin
        self.type = type
        self.showType = showType
        initViews(frame: frame)
    }
    
    init(frame: CGRect, type: TitleImgType, showType: TitleImgShowType) {
        super.init(frame: frame)
        self.type = type
        self.showType = showType
        initViews(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    deinit {
        removeGestureRecognizer(tapGes)
    }
    
    public func configView(text: String?, image: String?) {
        if let t = text {
            textLb.text = t
        }
        
        if let img = image {
            imageV.sy_name(img)
        }
    }
    
    public func updateItems() {
        
        addGestureRecognizer(self.tapGes)
        if showType == .byContent {
            addSubview(textLb)
            addSubview(imageV)
        } else {
            addSubview(bgView)
            bgView.addSubview(textLb)
            bgView.addSubview(imageV)
            bgView.snp.updateConstraints { (make) in
                make.center.equalToSuperview()
            }
        }
        
        switch type {
        case .imgTop:
            textLb.textAlignment = .center
            imageV.snp.updateConstraints { (make) in
                make.top.equalToSuperview().offset(imgSuperViewMargin)
                make.centerX.equalToSuperview()
            }
            textLb.snp.updateConstraints { (make) in
                make.bottom.equalToSuperview()
                make.top.equalTo(self.imageV.snp.bottom).offset(imgTitleMargin)
                make.left.right.equalToSuperview()
            }
        case .imgLeft:
            imageV.snp.updateConstraints { (make) in
                make.left.equalToSuperview().offset(imgSuperViewMargin)
                make.centerY.equalToSuperview()
            }
            textLb.snp.updateConstraints { (make) in
                make.right.equalToSuperview().offset(-textSuperViewMargin)
                make.left.equalTo(self.imageV.snp.right).offset(imgTitleMargin)
                make.top.bottom.equalToSuperview()
            }
        case .imgBottom:
            textLb.textAlignment = .center
            imageV.snp.updateConstraints { (make) in
                make.bottom.equalToSuperview().offset(-imgSuperViewMargin)
                make.centerX.equalToSuperview()
            }
            textLb.snp.updateConstraints { (make) in
                make.top.equalToSuperview()
                make.bottom.equalTo(self.imageV.snp.top).offset(-imgTitleMargin)
                make.left.right.equalToSuperview()
            }
        case .imgRight:
            imageV.snp.updateConstraints { (make) in
                make.right.equalToSuperview().offset(-imgSuperViewMargin)
                make.centerY.equalToSuperview()
            }
            textLb.snp.updateConstraints { (make) in
                make.left.equalToSuperview()
                make.right.equalTo(self.imageV.snp.left).offset(-imgTitleMargin)
                make.top.bottom.equalToSuperview()
            }
        }
    }
    
    fileprivate func initViews(frame: CGRect) {
        
        addGestureRecognizer(self.tapGes)
        if showType == .byContent {
            addSubview(textLb)
            addSubview(imageV)
        } else {
            addSubview(bgView)
            bgView.addSubview(textLb)
            bgView.addSubview(imageV)
            bgView.snp.makeConstraints { (make) in
                make.center.equalToSuperview()
            }
        }
        
        switch type {
        case .imgTop:
            imageV.snp.makeConstraints { (make) in
                make.top.equalToSuperview().offset(imgSuperViewMargin)
                make.centerX.equalToSuperview()
            }
            textLb.snp.makeConstraints { (make) in
                make.bottom.equalToSuperview()
                make.top.equalTo(self.imageV.snp.bottom).offset(imgTitleMargin)
                make.left.right.equalToSuperview()
            }
        case .imgLeft:
            imageV.snp.makeConstraints { (make) in
                make.left.equalToSuperview().offset(imgSuperViewMargin)
                make.centerY.equalToSuperview()
            }
            textLb.snp.makeConstraints { (make) in
                make.right.equalToSuperview().offset(-textSuperViewMargin)
                make.left.equalTo(self.imageV.snp.right).offset(imgTitleMargin)
                make.top.bottom.equalToSuperview()
            }
        case .imgBottom:
            imageV.snp.makeConstraints { (make) in
                make.bottom.equalToSuperview().offset(-imgSuperViewMargin)
                make.centerX.equalToSuperview()
            }
            textLb.snp.makeConstraints { (make) in
                make.top.equalToSuperview()
                make.bottom.equalTo(self.imageV.snp.top).offset(-imgTitleMargin)
                make.left.right.equalToSuperview()
            }
        case .imgRight:
            imageV.snp.makeConstraints { (make) in
                make.right.equalToSuperview().offset(-imgSuperViewMargin)
                make.centerY.equalToSuperview()
            }
            textLb.snp.makeConstraints { (make) in
                make.left.equalToSuperview()
                make.right.equalTo(self.imageV.snp.left).offset(-imgTitleMargin)
                make.top.bottom.equalToSuperview()
            }
        }
    }
    
    @objc func tapGesAction() {
        if let closure = tapClosure {
            closure(self.tag, false)
        }
    }
}
