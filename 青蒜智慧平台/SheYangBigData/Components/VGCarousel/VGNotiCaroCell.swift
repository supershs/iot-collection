//
//  VGNotiCaroCell.swift
//  vgbox
//
//  Created by 宋海胜 on 2019/7/10.
//  Copyright © 2019 Swift Xcode. All rights reserved.
//  轮播 首页通知cell

import UIKit


/// 轮播 首页图片cell
class VGPicsCaroCell: UICollectionViewCell, VGCarouselViewCellProtocol {
    
    
    fileprivate var img: UIImageView = {
        let v: UIImageView = UIImageView()
        v.contentMode = UIView.ContentMode.scaleAspectFill
        v.clipsToBounds = true
        return v
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public func configCell(_ model: VGCarouserlModel) {
        let m: VGNotiCaroModel = model as! VGNotiCaroModel
        if let i = m.picImage {
            img.image = i
        } else {
            img.kf.setImage(with: URL(string: m.picUrl))
        }
        print("pic------------\(m.picUrl)")
        img.backgroundColor = m.pageCntColor
    }
    
    fileprivate func initViews() {
        
        addSubview(img)
        
        img.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
}

/// 首页通知 轮播 model
class VGNotiCaroModel: VGCarouserlModel {
    
    var title: String = ""
    var studentName: String = ""
    var orginName: String = ""
    var className: String = ""
    var content: String = ""
    var picUrl: String = ""
    var picImage: UIImage!
    var picHighUrl: String = ""
    var totalPageCount: Int = 5
    var titleColor: UIColor = .white
    var pageCntColor: UIColor = .white
    var pageTotalColor: UIColor = .white
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}
