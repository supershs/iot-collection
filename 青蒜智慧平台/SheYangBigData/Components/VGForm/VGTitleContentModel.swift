//
//  VGTitleContentModel.swift
//  vgbox
//
//  Created by super song on 2019/8/14.
//  Copyright © 2019 Swift Xcode. All rights reserved.
//  表单 title + content 样式

import UIKit

/// 表单 title + content 样式
class VGTitleContentModel: NSObject {
    
    var title: String = ""
    var content: String = ""
    var contents: (String, String) = ("", "")
    var isNessary: Bool = false
    var placeholder: String = ""
    var rightImgName: String = ""
    var rowHeight: CGFloat = 0
    var sepViewHidden: Bool = false
    var selectItemIndex: Int = 0
    var selectItemsIndex: [Int] = []
    var isSelected: String = "" // 00 未选中 01选中
    var isPull:Bool = true //true 展开
}
