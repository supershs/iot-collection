//
//  VGDoubleColorTitleLabel.swift
//  vgbox
//
//  Created by super song on 2019/8/23.
//  Copyright © 2019 Swift Xcode. All rights reserved.
//

import UIKit

/// 双色label
class VGDoubleColorTitleLabel: UIView {

    var text: String? = "" {
        didSet {
            if let t = text, !t.isEmpty {
                titleLb.text = self.text
                titleLb.textColor = UIColor(hex:0x333333)
            }
        }
    }
    
    var placeholder: String? = "" {
        didSet {
            if let p = placeholder, !p.isEmpty {
                titleLb.text = placeholder
                titleLb.textColor = UIColor(hex:0x999999)
            }
        }
    }
    
    public var titleLb: UILabel = {
        let v: UILabel = UILabel()
//        v.text = "放假啊发大发大沙发"
        return v
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    fileprivate func initViews() {
        
        addSubview(titleLb)
        titleLb.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }

}
