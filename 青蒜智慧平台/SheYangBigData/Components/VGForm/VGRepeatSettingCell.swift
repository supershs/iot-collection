//
//  VGRepeatSettingCell.swift
//  vgbox
//
//  Created by super song on 2019/8/14.
//  Copyright © 2019 Swift Xcode. All rights reserved.
//  表单样式 方框选择

import UIKit

///  表单样式 方框选择
class VGRepeatSettingCell: UITableViewCell {
    
    public var clickedClosure: ((Int) -> Void)?
    
    fileprivate var sepView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = Constant.lineColor
        return v
    }()
    
    fileprivate var titleLb: UILabel = {
        let v: UILabel = UILabel()
        v.textColor = UIColor(hex: 0x333333)
        v.font = UIFont.systemFont(ofSize: 15)
        v.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 1000), for: NSLayoutConstraint.Axis.horizontal)
        return v
    }()
    
    fileprivate lazy var neverBoxBtn: VGTitleImgView = {
        let v: VGTitleImgView = VGTitleImgView()
        v.textLb.text = "无"
        v.imageV.sy_name("notselect_yxj")
        v.tapClosure = {[weak self] (index, isSelect) in
            if let weakSelf = self {
                weakSelf.clickedClosure?(0)
            }
        }
        return v
    }()
    
    fileprivate lazy var byWeekBoxBtn: VGTitleImgView = {
        let v: VGTitleImgView = VGTitleImgView()
        v.textLb.text = "每周"
        v.imageV.sy_name("notselect_yxj")
        v.tapClosure = {[weak self] (index, isSelect) in
            if let weakSelf = self {
                weakSelf.clickedClosure?(1)
            }
        }
        return v
    }()
    
    fileprivate lazy var byTwoWeekBoxBtn: VGTitleImgView = {
        let v: VGTitleImgView = VGTitleImgView()
        v.textLb.text = "隔周"
        v.imageV.sy_name("notselect_yxj")
        v.tapClosure = {[weak self] (index, isSelect) in
            if let weakSelf = self {
                weakSelf.clickedClosure?(2)
            }
        }
        return v
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initViews()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func configCell(_ model: Any) {
        let m: VGTitleContentModel = model as! VGTitleContentModel
        titleLb.text = m.title
        sepView.isHidden = m.sepViewHidden
        
        neverBoxBtn.imageV.sy_name("notselect_yxj")
        neverBoxBtn.textLb.textColor = UIColor(hex:0x999999)
        byWeekBoxBtn.imageV.sy_name("notselect_yxj")
        byWeekBoxBtn.textLb.textColor = UIColor(hex:0x999999)
        byTwoWeekBoxBtn.imageV.sy_name("notselect_yxj")
        byTwoWeekBoxBtn.textLb.textColor = UIColor(hex:0x999999)
        switch m.selectItemIndex {
        case 0:
            neverBoxBtn.imageV.sy_name("select_yxj")
            neverBoxBtn.textLb.textColor = UIColor(hex:0x333333)
        case 1:
            byWeekBoxBtn.imageV.sy_name("select_yxj")
            byWeekBoxBtn.textLb.textColor = UIColor(hex:0x333333)
        case 2:
            byTwoWeekBoxBtn.imageV.sy_name("select_yxj")
            byTwoWeekBoxBtn.textLb.textColor = UIColor(hex:0x333333)
        default:
            break
        }
        
        titleLb.snp.updateConstraints { (make) in
            make.height.equalTo(m.rowHeight)
        }
    }
    
    fileprivate func initViews() {
        
        self.selectionStyle = .none
        addSubview(sepView)
        addSubview(titleLb)
        addSubview(neverBoxBtn)
        addSubview(byWeekBoxBtn)
        addSubview(byTwoWeekBoxBtn)
        
        sepView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(1)
        }
        
        titleLb.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(12)
            make.top.bottom.equalToSuperview()
            make.height.equalTo(0)
            make.width.equalTo(65)
        }
        
        neverBoxBtn.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalTo(titleLb.snp.right).offset(2)
            make.height.equalToSuperview()
        }
        
        byWeekBoxBtn.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalTo(neverBoxBtn.snp.right).offset(31)
            make.height.equalToSuperview()
        }
        
        byTwoWeekBoxBtn.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalTo(byWeekBoxBtn.snp.right).offset(30)
            make.height.equalToSuperview()
        }
    }
}
