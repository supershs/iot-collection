//
//  VGTitleContentCell.swift
//  vgbox
//
//  Created by super song on 2019/8/14.
//  Copyright © 2019 Swift Xcode. All rights reserved.
//  表单样式 title - content

import UIKit

///  表单样式 title - content
class VGTitleContentCell: UITableViewCell {
    
    public var clickedClosure: (() -> Void)?
    
    fileprivate var sepView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = Constant.lineColor
        return v
    }()

    fileprivate var nessaryLb: UILabel = {
        let v: UILabel = UILabel()
        v.textColor = UIColor(hex:0xFF4443)
        v.font = UIFont.systemFont(ofSize: 15)
        v.text = "*"
        return v
    }()
    
    public var titleLb: UILabel = {
        let v: UILabel = UILabel()
        v.textColor = UIColor(hex:0x333333)
        v.font = UIFont.systemFont(ofSize: 15)
        v.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 1000), for: NSLayoutConstraint.Axis.horizontal)
        return v
    }()
    
    public lazy var contentTf: VGDoubleColorTitleLabel = {
        let v: VGDoubleColorTitleLabel = VGDoubleColorTitleLabel()
        v.titleLb.textColor = UIColor(hex:0x333333)
        v.titleLb.font = UIFont.systemFont(ofSize: 15)
        return v
    }()
    
    fileprivate var rightImgView: UIImageView = {
        let v: UIImageView = UIImageView()
        v.sy_name("")
        return v
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initViews()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func configCell(_ model: Any) {
        let m: VGTitleContentModel = model as! VGTitleContentModel
        titleLb.text = m.title
        nessaryLb.isHidden = !m.isNessary
        contentTf.placeholder = m.placeholder
        contentTf.text = m.content
        rightImgView.sy_name(m.rightImgName)
        sepView.isHidden = m.sepViewHidden
        
        titleLb.snp.updateConstraints { (make) in
            make.height.equalTo(m.rowHeight)
        }
    }
    
    func configCellNoArrow(_ model: Any) {
        let m: VGTitleContentModel = model as! VGTitleContentModel
        titleLb.text = m.title
        nessaryLb.isHidden = !m.isNessary
        contentTf.placeholder = m.placeholder
        contentTf.text = m.content
        
        sepView.isHidden = m.sepViewHidden
        rightImgView.isHidden = true
        rightImgView.sy_name("")
        
        titleLb.snp.updateConstraints { (make) in
            make.height.equalTo(m.rowHeight)
        }
    }

    fileprivate func initViews() {
        
        self.selectionStyle = .none
        self.backgroundColor = UIColor.white
        addSubview(sepView)
        addSubview(nessaryLb)
        addSubview(titleLb)
        addSubview(self.contentTf)
        addSubview(rightImgView)
        
        sepView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview().offset(12)
            make.right.equalToSuperview()
            make.height.equalTo(0.5)
        }
        
        nessaryLb.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(3)
            make.width.equalTo(8)
            make.top.bottom.equalToSuperview()
        }
        
        titleLb.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(12)
            make.top.bottom.equalToSuperview()
            make.height.equalTo(0)
        }
        
        contentTf.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-27)
            make.left.equalTo(titleLb.snp.right).offset(10)
            make.top.bottom.equalToSuperview()
        }
        
        rightImgView.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-12)
        }
    }

}


///  表单样式 title + 灰色背景
class VGHeaderTitleCell: UITableViewCell {
    
    public var clickedClosure: (() -> Void)?
    
    public var titleLb: UILabel = {
        let v: UILabel = UILabel()
        v.textColor = UIColor(hex:0x999999)
        v.font = UIFont.systemFont(ofSize: 12)
        return v
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initViews()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func configCell(_ model: Any) {
        
        let m: VGTitleContentModel = model as! VGTitleContentModel
        titleLb.text = m.title
        
        titleLb.snp.updateConstraints { (make) in
            make.height.equalTo(m.rowHeight)
        }
    }
    
    fileprivate func initViews() {
        
        self.selectionStyle = .none
        self.backgroundColor = Constant.bgViewColor
        addSubview(titleLb)
        
        titleLb.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(12)
            make.height.equalTo(0)
            make.top.bottom.equalToSuperview()
        }
    }
}



///  表单样式 方框选择
class VGChoiceBoxCell: UITableViewCell {
    
    public var clickedClosure: ((Int) -> Void)?
    
    fileprivate var sepView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = Constant.lineColor
        return v
    }()
    
    fileprivate var nessaryLb: UILabel = {
        let v: UILabel = UILabel()
        v.textColor = UIColor(hex:0xFF4443)
        v.font = UIFont.systemFont(ofSize: 15)
        v.text = "*"
        return v
    }()
    
    fileprivate var titleLb: UILabel = {
        let v: UILabel = UILabel()
        v.textColor = UIColor(hex:0x333333)
        v.font = UIFont.systemFont(ofSize: 15)
        v.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 1000), for: NSLayoutConstraint.Axis.horizontal)
        return v
    }()

    fileprivate var claChoiceBox: UIImageView = {
        let v: UIImageView = UIImageView()
        v.sy_name("notselect_yxj")
        return v
    }()

    fileprivate var stuChoiceBox: UIImageView = {
        let v: UIImageView = UIImageView()
        v.sy_name("notselect_yxj")
        return v
    }()
    
    fileprivate var claBoxLb: UILabel = {
        let v: UILabel = UILabel()
        v.textColor = UIColor(hex:0x999999)
        v.font = UIFont.systemFont(ofSize: 15)
        v.text = "按班级"
//        v.setContentCompressionResistancePriority(1000, for: UILayoutConstraintAxis.horizontal)
        return v
    }()
    
    fileprivate var stuBoxLb: UILabel = {
        let v: UILabel = UILabel()
        v.textColor = UIColor(hex:0x999999)
        v.font = UIFont.systemFont(ofSize: 15)
        v.text = "按学员"
//        v.setContentCompressionResistancePriority(1000, for: UILayoutConstraintAxis.horizontal)
        return v
    }()
    
    fileprivate lazy var claBoxBtn: UIButton = {
        let v: UIButton = UIButton()
        v.addTarget(self, action: #selector(claBoxBtnAction), for: .touchUpInside)
        return v
    }()
    
    fileprivate lazy var stuBoxBtn: UIButton = {
        let v: UIButton = UIButton()
        v.addTarget(self, action: #selector(stuBoxBtnAction), for: .touchUpInside)
        return v
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initViews()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @objc func claBoxBtnAction(sender: UIButton) {
        
//        sender.isSelected = !sender.isSelected
//        if sender.isSelected {
        setDefaultBox(0)
//        } else {
//            setDefaultBox(1)
//        }
        clickedClosure?(0)
    }
    
    @objc func stuBoxBtnAction(sender: UIButton) {
        
//        sender.isSelected = !sender.isSelected
//        if sender.isSelected {
        setDefaultBox(1)
//        } else {
//            setDefaultBox(0)
//        }
        clickedClosure?(1)
    }
    
    public func setDefaultBox(_ type: Int) {
        if type == 0 {
            claChoiceBox.sy_name("select_yxj")
            claBoxLb.textColor = UIColor(hex:0x333333)
            
            stuChoiceBox.sy_name("notselect_yxj")
            stuBoxLb.textColor = UIColor(hex:0x999999)
        } else {
            stuChoiceBox.sy_name("select_yxj")
            stuBoxLb.textColor = UIColor(hex:0x333333)
            
            claChoiceBox.sy_name("notselect_yxj")
            claBoxLb.textColor = UIColor(hex:0x999999)
        }
    }
    
    func configCell(_ model: Any) {
        let m: VGTitleContentModel = model as! VGTitleContentModel
        titleLb.text = m.title
        nessaryLb.isHidden = !m.isNessary
        sepView.isHidden = m.sepViewHidden
        
        titleLb.snp.updateConstraints { (make) in
            make.height.equalTo(m.rowHeight)
        }
    }
    
    fileprivate func initViews() {
        
        self.selectionStyle = .none
        addSubview(sepView)
        addSubview(nessaryLb)
        addSubview(titleLb)
        addSubview(claChoiceBox)
        addSubview(stuChoiceBox)
        addSubview(claBoxLb)
        addSubview(stuBoxLb)
        addSubview(claBoxBtn)
        addSubview(stuBoxBtn)
        
        sepView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview().offset(12)
            make.right.equalToSuperview()
            make.height.equalTo(0.5)
        }
        
        nessaryLb.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(3)
            make.width.equalTo(8)
            make.top.bottom.equalToSuperview()
        }
        
        titleLb.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(12)
            make.top.bottom.equalToSuperview()
            make.height.equalTo(45)
            make.right.equalToSuperview()
        }
        
        claChoiceBox.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalTo(claBoxLb.snp.left).offset(-4)
        }
        
        stuChoiceBox.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalTo(stuBoxLb.snp.left).offset(-4)
        }
        
        claBoxLb.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalTo(stuBoxLb.snp.left).offset(-57)
        }
        
        stuBoxLb.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-30)
        }
        
        claBoxBtn.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalTo(claBoxLb)
            make.left.equalTo(claChoiceBox).offset(-20)
            make.height.equalToSuperview()
        }
        
        stuBoxBtn.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalTo(stuBoxLb)
            make.left.equalTo(stuChoiceBox).offset(-20)
            make.height.equalToSuperview()
        }
    }
}


///  表单样式 - 小时选择
class VGTimeChoiceCell: UITableViewCell {
    
    public var clickedClosure: ((Int) -> Void)?
    
    fileprivate var sepView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = Constant.lineColor
        return v
    }()
    
    fileprivate var titleLb: UILabel = {
        let v: UILabel = UILabel()
        v.textColor = UIColor(hex:0x333333)
        v.font = UIFont.systemFont(ofSize: 15)
        v.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 1000), for: NSLayoutConstraint.Axis.horizontal)
        return v
    }()
    
    fileprivate var middleView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = UIColor(hex:0x333333)
        return v
    }()
    
    fileprivate lazy var startBtn: UIButton = {
        let v: UIButton = UIButton()
        v.addTarget(self, action: #selector(startBtnAction), for: .touchUpInside)
        v.backgroundColor = UIColor(hex:0xF2F2F2)
        v.layer.cornerRadius = 2
        v.layer.masksToBounds = true
        return v
    }()
    
    fileprivate lazy var endBtn: UIButton = {
        let v: UIButton = UIButton()
        v.addTarget(self, action: #selector(endBtnAction), for: .touchUpInside)
        v.backgroundColor = UIColor(hex:0xF2F2F2)
        v.layer.cornerRadius = 2
        v.layer.masksToBounds = true
        return v
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initViews()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @objc func startBtnAction() {
        clickedClosure?(0)
    }
    
    @objc func endBtnAction() {
        clickedClosure?(1)
    }
    
    func configCell(_ model: Any, _ startHour: String, _ endHour: String) {
        
        let m: VGTitleContentModel = model as! VGTitleContentModel
        titleLb.text = m.title
        sepView.isHidden = m.sepViewHidden
        startBtn.setTitle(startHour, for: .normal)
        endBtn.setTitle(endHour, for: .normal)
        if startHour == "请选择" {
            startBtn.setTitleColor(UIColor(hex:0xbbbbbb), for: .normal)
        } else {
            startBtn.setTitleColor(UIColor(hex:0x333333), for: .normal)
        }
        if endHour == "请选择" {
            endBtn.setTitleColor(UIColor(hex:0xbbbbbb), for: .normal)
        } else {
            endBtn.setTitleColor(UIColor(hex:0x333333), for: .normal)
        }
    }
    
    fileprivate func initViews() {
        
        self.selectionStyle = .none
        addSubview(sepView)
        addSubview(titleLb)
        addSubview(middleView)
        addSubview(startBtn)
        addSubview(endBtn)
        
        sepView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview().offset(12)
            make.right.equalToSuperview()
            make.height.equalTo(0.5)
        }
        
        titleLb.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(12)
            make.left.equalToSuperview().offset(12)
            make.height.equalTo(21)
            make.bottom.equalToSuperview().offset(-12)
        }
        
        startBtn.snp.makeConstraints { (make) in
            make.centerY.equalTo(titleLb)
            make.left.equalTo(titleLb.snp.right).offset(12)
            make.width.equalTo(endBtn)
            make.height.equalTo(30)
        }
        
        endBtn.snp.makeConstraints { (make) in
            make.top.bottom.equalTo(startBtn)
            make.left.equalTo(startBtn.snp.right).offset(13)
            make.right.equalToSuperview().offset(-12)
        }
        
        middleView.snp.makeConstraints { (make) in
            make.centerY.equalTo(startBtn)
            make.left.equalTo(startBtn.snp.right).offset(4)
            make.height.equalTo(1)
            make.width.equalTo(5)
        }
    }
}
