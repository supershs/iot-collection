//
//  ZQVarableMenuModel.h
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/8/4.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZQVarableMenuModel : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) NSString *idField;


@end

NS_ASSUME_NONNULL_END
