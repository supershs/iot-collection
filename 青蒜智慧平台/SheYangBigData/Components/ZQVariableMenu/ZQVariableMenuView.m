//
//  ZQVariableMenuView.m
//  ZQVariableMenuDemo
//
//  Created by 肖兆强 on 2017/12/1.
//  Copyright © 2017年 ZQDemo. All rights reserved.
//

#import "ZQVariableMenuView.h"
#import "ZQVariableMenuCell.h"
#import "ZQVariableMenuHeader.h"

#import <JJCollectionViewRoundFlowLayout/JJCollectionViewRoundFlowLayout.h>


@interface ZQVariableMenuView ()<UICollectionViewDelegate,UICollectionViewDataSource>
{
    UICollectionView *_collectionView;
    //被拖拽的item
    ZQVariableMenuCell *_dragingItem;
    //正在拖拽的indexpath
    NSIndexPath *_dragingIndexPath;
    //目标位置
    NSIndexPath *_targetIndexPath;
    CGPoint lastPoint;
}

@end

@implementation ZQVariableMenuView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self buildUI];
    }
    return self;
}

-(void)buildUI{
    
    JJCollectionViewRoundFlowLayout *flowLayout = [[JJCollectionViewRoundFlowLayout alloc]init];
    flowLayout.itemSize = CGSizeMake(self.bounds.size.width/4,80);
    flowLayout.minimumLineSpacing = 0;
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.headerReferenceSize = CGSizeMake(self.bounds.size.width, 56);
    flowLayout.isCalculateHeader = YES;
    
    _collectionView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:flowLayout];
    _collectionView.showsHorizontalScrollIndicator = false;
    _collectionView.backgroundColor = [UIColor clearColor];
    [_collectionView registerClass:[ZQVariableMenuCell class] forCellWithReuseIdentifier:@"ZQVariableMenuCell"];
    [_collectionView registerClass:[ZQVariableMenuHeader class]
        forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"ZQVariableMenuHeader"];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    [self addSubview:_collectionView];
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressMethod:)];
    longPress.minimumPressDuration = 0.3f;
    [_collectionView addGestureRecognizer:longPress];
    
    _dragingItem = [[ZQVariableMenuCell alloc] initWithFrame:CGRectMake(0, 0, 84, 80)];
    _dragingItem.hidden = true;
    [_collectionView addSubview:_dragingItem];
}

#pragma mark -
#pragma mark LongPressMethod
-(void)longPressMethod:(UILongPressGestureRecognizer*)gesture{
    CGPoint point = [gesture locationInView:_collectionView];
    switch (gesture.state) {
        case UIGestureRecognizerStateBegan:
            [self dragBegin:point];
            break;
        case UIGestureRecognizerStateChanged:
            [self dragChanged:point];
            break;
        case UIGestureRecognizerStateEnded:
            [self dragEnd];
            break;
        default:
            break;
    }
}

//拖拽开始 找到被拖拽的item
-(void)dragBegin:(CGPoint)point{
    _dragingIndexPath = [self getDragingIndexPathWithPoint:point];
    if (!_dragingIndexPath) {return;}
    [_collectionView bringSubviewToFront:_dragingItem];
    ZQVariableMenuCell *item = (ZQVariableMenuCell*)[_collectionView cellForItemAtIndexPath:_dragingIndexPath];
    item.isMoving = true;
    item.hidden = YES;
    //更新被拖拽的item
    _dragingItem.hidden = false;
    _dragingItem.frame = item.frame;
    _dragingItem.title = item.title;
    _dragingItem.imageName = item.imageName;
    [_dragingItem setTransform:CGAffineTransformMakeScale(1.1, 1.1)];
}

//正在被拖拽、、、
-(void)dragChanged:(CGPoint)point{
    if (!_dragingIndexPath) {return;}
    _dragingItem.center = point;
    _targetIndexPath = [self getTargetIndexPathWithPoint:point];
    //交换位置 如果没有找到_targetIndexPath则不交换位置
    if (_dragingIndexPath && _targetIndexPath) {
        //更新数据源
        [self rearrangeInUseTitles];
        //更新item位置
        [_collectionView moveItemAtIndexPath:_dragingIndexPath toIndexPath:_targetIndexPath];
        _dragingIndexPath = _targetIndexPath;
        [self getIds];
    }
    lastPoint = point;
}

//拖拽结束
-(void)dragEnd{
    if (!_dragingIndexPath) {return;}
    

    CGRect lastFrame = [_collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:_inUseModels.count - 1 inSection:0]].frame;
    if (lastPoint.y > CGRectGetMaxY(lastFrame)) {
        id obj = [_inUseModels objectAtIndex:_dragingIndexPath.row];
        [_inUseModels removeObject:obj];
        [_unUseModels insertObject:obj atIndex:0];
        [_collectionView moveItemAtIndexPath:_dragingIndexPath toIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
        _dragingIndexPath = [NSIndexPath indexPathForRow:0 inSection:1];
        [self getIds];
    }
    CGRect endFrame = [_collectionView cellForItemAtIndexPath:_dragingIndexPath].frame;
    [_dragingItem setTransform:CGAffineTransformMakeScale(1.0, 1.0)];
    [UIView animateWithDuration:0.3 animations:^{
        _dragingItem.frame = endFrame;
    }completion:^(BOOL finished) {
        _dragingItem.hidden = true;
        ZQVariableMenuCell *item = (ZQVariableMenuCell*)[_collectionView cellForItemAtIndexPath:_dragingIndexPath];
        item.isMoving = false;
        item.hidden = NO;
    }];
    
}

#pragma mark -
#pragma mark 辅助方法

//获取被拖动IndexPath的方法
-(NSIndexPath*)getDragingIndexPathWithPoint:(CGPoint)point{
    NSIndexPath* dragIndexPath = nil;
    //最后剩一个怎不可以排序
    if ([_collectionView numberOfItemsInSection:0] == 1) {return dragIndexPath;}
    
    
    for (NSIndexPath *indexPath in _collectionView.indexPathsForVisibleItems) {
        //下半部分不需要排序
        if (indexPath.section > 0) {continue;}
        //需要固定位置的前几个item不需要排序
        if (indexPath.row<self.fixedNum) {continue;}
        //在上半部分中找出相对应的Item
        if (CGRectContainsPoint([_collectionView cellForItemAtIndexPath:indexPath].frame, point)) {
            if (indexPath.row != 0) {
                dragIndexPath = indexPath;
            }
            break;
        }
    }
    return dragIndexPath;
}

//获取目标IndexPath的方法
-(NSIndexPath*)getTargetIndexPathWithPoint:(CGPoint)point{
    NSIndexPath *targetIndexPath = nil;
    for (NSIndexPath *indexPath in _collectionView.indexPathsForVisibleItems) {
        //如果是自己不需要排序
        if ([indexPath isEqual:_dragingIndexPath]) {continue;}
        
        //在第二组在移动过程中不动
        if (indexPath.section > 0) {continue;}
        //需要固定位置的前几个item不需要排序
        if (indexPath.row<self.fixedNum) {
            continue;
        }
        //在第一组中找出将被替换位置的Item
        if (CGRectContainsPoint([_collectionView cellForItemAtIndexPath:indexPath].frame, point)) {
            if (indexPath.row != 0) {
                targetIndexPath = indexPath;
            }
        }
    }
    return targetIndexPath;
}

#pragma mark -
#pragma mark CollectionViewDelegate&DataSource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return section == 0 ? _inUseModels.count : _unUseModels.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 2;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    ZQVariableMenuHeader *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"ZQVariableMenuHeader" forIndexPath:indexPath];
    if (indexPath.section == 0) {
        headerView.title = @"快捷应用";
        headerView.subTitle = @"拖动可调整应用位置顺序";
    }else if (indexPath.section == 1) {
        headerView.title = @"常用应用";
        headerView.subTitle = @"";
    }
    return headerView;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* cellId = @"ZQVariableMenuCell";
    ZQVariableMenuCell* item = [collectionView dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
    item.title = indexPath.section == 0 ? _inUseModels[indexPath.row].name : _unUseModels[indexPath.row].name;
    item.imageName = indexPath.section == 0 ? _inUseModels[indexPath.row].imageUrl : _unUseModels[indexPath.row].imageUrl;
    
    item.isFixed = indexPath.section == 0 && indexPath.row < _fixedNum;
    return  item;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        //只剩一个的时候不可删除
        if ([_collectionView numberOfItemsInSection:0] == 1) {return;}
        //固定位置的前几个item不可删除
        if (indexPath.row <self.fixedNum) {return;}
        id obj = [_inUseModels objectAtIndex:indexPath.row];
        [_inUseModels removeObject:obj];
        [_unUseModels insertObject:obj atIndex:0];
        [_collectionView moveItemAtIndexPath:indexPath toIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    }else{
        id obj = [_unUseModels objectAtIndex:indexPath.row];
        [_unUseModels removeObject:obj];
        [_inUseModels addObject:obj];
        [_collectionView moveItemAtIndexPath:indexPath toIndexPath:[NSIndexPath indexPathForRow:_inUseModels.count - 1 inSection:0]];
    }
    [self getIds];
}

#pragma mark - JJCollectionViewDelegateRoundFlowLayout
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout borderEdgeInsertsForSectionAtIndex:(NSInteger)section{
    if (section == 0) {
        return UIEdgeInsetsMake(10, 0, 0, 0);
    } else {
        return UIEdgeInsetsMake(10, 0, 5, 0);
    }
}

- (JJCollectionViewRoundConfigModel *)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout configModelForSectionAtIndex:(NSInteger)section{
    JJCollectionViewRoundConfigModel *model = [[JJCollectionViewRoundConfigModel alloc]init];
    model.backgroundColor = [UIColor whiteColor];
    model.cornerRadius = 8;
    return model;
}

#pragma mark -
#pragma mark 刷新方法
//拖拽排序后需要重新排序数据源
-(void)rearrangeInUseTitles
{
    id obj = [_inUseModels objectAtIndex:_dragingIndexPath.row];
    [_inUseModels removeObject:obj];
    [_inUseModels insertObject:obj atIndex:_targetIndexPath.row];
}

-(void)reloadData
{
    [_collectionView reloadData];
}

-(void)getIds {
    NSMutableArray *ids = [NSMutableArray array];
    _completionBlock(_inUseModels, _unUseModels);
}
/**
 16进制颜色转换为UIColor

 @param hexColor 16进制字符串（可以以0x开头，可以以#开头，也可以就是6位的16进制）
 @param opacity 透明度
 @return 16进制字符串对应的颜色
 */
-(UIColor *)colorWithHexString:(NSString *)hexColor alpha:(float)opacity{
    NSString * cString = [[hexColor stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];

    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor blackColor];

    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    if ([cString hasPrefix:@"#"]) cString = [cString substringFromIndex:1];

    if ([cString length] != 6) return [UIColor blackColor];

    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString * rString = [cString substringWithRange:range];

    range.location = 2;
    NSString * gString = [cString substringWithRange:range];

    range.location = 4;
    NSString * bString = [cString substringWithRange:range];

    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];

    return [UIColor colorWithRed:((float)r / 255.0f)
                           green:((float)g / 255.0f)
                            blue:((float)b / 255.0f)
                           alpha:opacity];
}

@end
