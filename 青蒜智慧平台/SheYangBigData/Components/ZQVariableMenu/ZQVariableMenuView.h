//
//  ZQVariableMenuView.h
//  ZQVariableMenuDemo
//
//  Created by 肖兆强 on 2017/12/1.
//  Copyright © 2017年 ZQDemo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZQVarableMenuModel.h"

typedef void(^EditCompletionBlock)(NSArray<ZQVarableMenuModel *> *, NSArray<ZQVarableMenuModel *> *);

@interface ZQVariableMenuView : UIView

@property (nonatomic, strong) NSMutableArray<ZQVarableMenuModel *> *inUseModels;

@property (nonatomic,strong) NSMutableArray<ZQVarableMenuModel *> *unUseModels;

@property (nonatomic,assign) NSInteger fixedNum;

@property (nonatomic, copy) EditCompletionBlock completionBlock;


-(void)reloadData;


@end
