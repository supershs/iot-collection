//
//  VGChoiceNormalCell.swift
//  vgbox
//
//  Created by 宋海胜 on 2019/7/17.
//  Copyright © 2019 Swift Xcode. All rights reserved.
//  选择控件 - 基本样式 1

import UIKit


/// 选择控件 - 基本样式 1 左边title 右边 勾
class VGChoiceNormalCell: VGBaseChoiceCell<VGBaseChoiceOrgModel> {
    
    public var titleLb: UILabel = {
        let v: UILabel = UILabel()
        v.textColor = UIColor(hex:0x333333)
        v.font = UIFont.systemFont(ofSize: 14)
        return v
    }()
    
    public var selectedImg: UIImageView = {
        let v: UIImageView = UIImageView()
        v.sy_name("classImage_duigou_ico")
        v.sizeToFit()
        return v
    }()
    
    public var sepView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = Constant.lineColor
        return v
    }()
    
    required init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func initViews() {
        super.initViews()
        
        addSubview(titleLb)
        addSubview(selectedImg)
        addSubview(sepView)
        
        titleLb.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(12)
            make.right.equalToSuperview().offset(-35)
            make.top.bottom.equalToSuperview()
        }
        selectedImg.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-12)
            make.centerY.equalToSuperview()
        }
        sepView.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.bottom.right.equalToSuperview()
            make.height.equalTo(LINE_HEIGHT)
        }

    }
    
    override func configCell(_ model: Any, _ indexPath: IndexPath, _ dataSource : [Any]) {
        super.configCell(model, indexPath, dataSource)
        
        if let m: VGBaseChoiceModel<VGBaseChoiceOrgModel> = model as? VGBaseChoiceModel<VGBaseChoiceOrgModel> {
            
            if let t = m.cellModel {

                titleLb.text = t.title
                if let font = t.titleFont{
                    titleLb.setFont(font: font)
                }
                sepView.snp.remakeConstraints { (make) in
                    make.left.equalTo(t.lineLeftMargin ?? 0)
                    make.bottom.right.equalToSuperview()
                    make.height.equalTo(LINE_HEIGHT)
                }
            }

            if m.isSelected == true {
                titleLb.textColor = Constant.blueColor
                selectedImg.isHidden = false
            } else {
                titleLb.textColor = UIColor(hex:0x333333)
                selectedImg.isHidden = true
            }
            sepView.isHidden = indexPath.row == dataSource.count - 1
        }
    }
    
}

struct VGBaseChoiceOrgModel: Codable {
    
    // 因为每个模型的title对应的key可能不一样。所以需要把对应的值赋值给title来展示
    var title: String?
    
    var claname: String?
    var classtext: String?
    var claid: String?
    var cilid: String?
    
    var cnt: Int?
    var oname: String?
    var notifyNum: Int?
    var stids: String?
    var orgid: String?

    var titleFont:CGFloat?
    var lineLeftMargin:CGFloat?
}

struct VGCourseLeaveNameNumModel: Codable {
    var num: Int?
    var type: String?
}
