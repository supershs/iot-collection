//
//  VGBaseChoiceModel.swift
//  vgbox
//
//  Created by 宋海胜 on 2019/7/17.
//  Copyright © 2019 Swift Xcode. All rights reserved.
//  选择控件model基础类

import UIKit


/// 选择控件model基础类
class VGBaseChoiceModel<M: Codable>: NSObject {
    
    var isSelected: Bool?
    var canEdit: Bool?
    var canSelected: Bool?
    var cellType: BaseChoiceCellType?
    var cellModel: M?
}


enum BaseChoiceType {
    case normalCancel  // 可滑动的cancel
    case noCancel
    case noBottom      // 没有底部蒙层
}


enum BaseChoiceCellType {
    case normal
    case graySep
    case cancel
}
