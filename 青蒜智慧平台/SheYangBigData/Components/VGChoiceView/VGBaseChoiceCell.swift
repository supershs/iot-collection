//
//  VGBaseChoiceCell.swift
//  vgbox
//
//  Created by 宋海胜 on 2019/7/17.
//  Copyright © 2019 Swift Xcode. All rights reserved.
//  选择控件cell基础类

import UIKit


/// 选择控件cell基础类
class VGBaseChoiceCell<M: Codable>: UITableViewCell, VGBaseListTableViewCellProtocol {

    
    public var cancelLb: UILabel = {
        let v: UILabel = UILabel()
        v.text = "取消"
        v.textColor = UIColor(hex:0x333333)
        v.font = UIFont.systemFont(ofSize: 15)
        v.sizeToFit()
        return v
    }()
    
    public var graySepView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = UIColor(hex:0xF2F3F4)
        return v
    }()
    
    required override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public func initViews() {
        backgroundColor = UIColor.white
        addSubview(cancelLb)
        cancelLb.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
        addSubview(graySepView)
        graySepView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    func configCell(_ model: Any, _ indexPath: IndexPath, _ dataSource : [Any]) {
        
        if let m: VGBaseChoiceModel<M> = model as? VGBaseChoiceModel<M>, let type = m.cellType {
            
            switch type {
            case .normal:
                let _ = self.subviews.map { $0.isHidden = false }
                cancelLb.isHidden = true
                graySepView.isHidden = true
            case .graySep:
                let _ = self.subviews.map { $0.isHidden = true }
                graySepView.isHidden = false
            case .cancel:
                let _ = self.subviews.map { $0.isHidden = true }
                cancelLb.isHidden = false
            }
        }
    }
}


