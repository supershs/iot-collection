//
//  VGChoiceReguCancelView.swift
//  vgbox
//
//  Created by 宋海胜 on 2019/7/19.
//  Copyright © 2019 Swift Xcode. All rights reserved.
//  选择控件 - 取消按钮在底部不动

import UIKit

///  选择控件 - 取消按钮在底部不动
class VGChoiceReguCancelView<M: Codable, C: VGBaseChoiceCell<M>>: UIView {

    public var selectRowClosure: ((Int)->Void)?
    
    public var cancelClosure: (()->Void)?
    
    var choiceView: VGBaseChoiceView<M, C>!

    override init(frame: CGRect) {
        super.init(frame: frame)
        initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @objc func cancelAction() {
        self.isHidden = true
        self.choiceView.showSelf(false)
        cancelClosure?()
    }
    
    public func showSelf(_ show: Bool) {
        self.isHidden = !show
        self.choiceView.showSelf(show)
    }

    public func setUnSelectedIndex(_ index: Int) {
        self.choiceView.unSelectedIndex = index
    }
    
    public func setDataSource(_ lists: [VGBaseChoiceModel<M>]) {
        
        self.choiceView.setDataSource(lists, .noCancel)
    }

    fileprivate func initViews() {
        
        self.isHidden = true
        
        self.choiceView = VGBaseChoiceView(frame: self.bounds)
        let lineView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 10))
        lineView.backgroundColor = Constant.bgViewColor
        let cancelLb: UIButton = UIButton(frame: CGRect(x: 0, y: 10, width: SCREEN_WIDTH, height: 44))
        cancelLb.setTitle("取消", for: .normal)
        cancelLb.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        cancelLb.setTitleColor(UIColor(hex:0x333333), for: .normal)
        cancelLb.addTarget(self, action: #selector(cancelAction), for: .touchUpInside)
        
        cancelLb.backgroundColor = .white
        self.choiceView.bottomBt.addSubview(lineView)
        self.choiceView.bottomBt.addSubview(cancelLb)
        self.addSubview(choiceView)
        
        choiceView.selectRowClosure = { [weak self] index in
            if let weakSelf = self {
                weakSelf.selectRowClosure?(index)
                weakSelf.isHidden = true
                weakSelf.cancelAction()
            }
        }
        
        choiceView.cancelClosure = {[weak self] in
            if let weakSelf = self {
                weakSelf.isHidden = true
                weakSelf.cancelClosure?()
            }
        }
    }
}
