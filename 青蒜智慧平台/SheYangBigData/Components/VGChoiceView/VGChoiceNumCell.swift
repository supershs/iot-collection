//
//  VGChoiceNumCell.swift
//  vgbox
//
//  Created by super song on 2019/8/21.
//  Copyright © 2019 Swift Xcode. All rights reserved.
//

import UIKit

/// 选择控件 - 基本样式 2 左边title 右边num
class VGChoiceNumCell: VGBaseChoiceCell<VGBaseChoiceOrgModel> {
    
    fileprivate var titleLb: UILabel = {
        let v: UILabel = UILabel()
        v.textColor = UIColor(hex:0x333333)
        v.font = UIFont.systemFont(ofSize: 14)
        return v
    }()
    
    fileprivate var contentLb: UILabel = {
        let v: UILabel = UILabel()
        v.textColor = UIColor(hex:0x333333)
        v.font = UIFont.systemFont(ofSize: 14)
        v.textAlignment = .right
        return v
    }()
    
    fileprivate var sepView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = Constant.lineColor
        return v
    }()
    
    required init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func initViews() {
        super.initViews()
        
        addSubview(titleLb)
        addSubview(contentLb)
        addSubview(sepView)
        
        titleLb.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(12)
            make.right.equalToSuperview().offset(-80)
            make.top.bottom.equalToSuperview()
        }
        contentLb.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-12)
            make.centerY.equalToSuperview()
            make.top.bottom.equalToSuperview()
            make.left.equalTo(titleLb.snp.right)
        }
        sepView.snp.makeConstraints { (make) in
            make.left.equalTo(12)
            make.bottom.right.equalToSuperview()
            make.height.equalTo(LINE_HEIGHT)
        }
    }
    
    override func configCell(_ model: Any, _ indexPath: IndexPath, _ dataSource : [Any]) {
        super.configCell(model, indexPath, dataSource)
        
        sepView.isHidden = (indexPath.row == dataSource.count - 1) ? true : false
        
        if let m: VGBaseChoiceModel<VGBaseChoiceOrgModel> = model as? VGBaseChoiceModel<VGBaseChoiceOrgModel> {
            
            if let t = m.cellModel {
                titleLb.text = t.title
            }
            if let t = m.cellModel, let n = t.cnt {
                contentLb.text = "\(n)"
            }
            if m.isSelected == true {
                titleLb.textColor = UIColor(hex:0x1797CE)
                contentLb.textColor = UIColor(hex:0x1797CE)
            } else {
                titleLb.textColor = UIColor(hex:0x333333)
                contentLb.textColor = UIColor(hex:0x333333)
            }
        }
    }
    
}
