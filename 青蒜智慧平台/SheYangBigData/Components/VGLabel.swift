//
//  VGLabel.swift
//  vgbox
//
//  Created by 宋海胜 on 2019/3/27.
//  Copyright © 2019年 Swift Xcode. All rights reserved.
//

import UIKit

class VGLabel: UILabel {
    
    var edgeInsets: UIEdgeInsets!

    override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
        var rect = super.textRect(forBounds: bounds.inset(by: edgeInsets), limitedToNumberOfLines: numberOfLines)
        rect.origin.x -= edgeInsets.left
        rect.origin.y -= edgeInsets.top
        rect.size.width += edgeInsets.left + edgeInsets.right
        rect.size.height += edgeInsets.top + edgeInsets.bottom
        return rect
    }
    
    override func drawText(in rect: CGRect) {
        super.drawText(in: rect.inset(by: edgeInsets))
    }

}
