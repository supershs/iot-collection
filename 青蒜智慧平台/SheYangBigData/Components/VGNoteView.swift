//
//  VGNoteView.swift
//  vgbox
//
//  Created by 宋海胜 on 2019/7/25.
//  Copyright © 2019 Swift Xcode. All rights reserved.
//  便签view

import UIKit
import YYText
import YYImage

///  便签view
class VGNoteView: UIView {
    
    /// 总宽度
    public var width: CGFloat = SCREEN_WIDTH - 60
    
    /// 便签区域上下距离父视图的距离
    public var topMargin: CGFloat = 10
    
    /// 便签区域左右距离父视图的距离
    public var leftMargin: CGFloat = 15
    
    /// 便签高度
    public var height: CGFloat = 16
    
    /// 字体大小 在setupBtns方法之前传入用于计算便签width
    public var font: UIFont = UIFont.systemFont(ofSize: 13)
    
    /// 便签之间的左右间距
    public var marginWidth: CGFloat = 4
    
    /// 便签之间的上下间距
    public var marginHeight: CGFloat = 4
    
    /// 文字距离左右边框的距离
    public var textMargin: CGFloat = 6
    
    /// 文字颜色
    public var textColor: UIColor = UIColor(hex:0xFDBC49)
    
    /// 边框颜色
    public var borderColor: UIColor = UIColor(hex:0x1797CE)

    /// 边框
    public var borderWidth: CGFloat = 0.5

    /// 背景颜色
    public var bgColor: UIColor = UIColor(hex: 0xFFF8E8)

    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    
        //MARK:- 获取标签的高度
    public class func getViewHeight(texts:[String],leftMargin:CGFloat = 12,topMargin:CGFloat = 0,font:CGFloat = 10,width:CGFloat = SCREEN_WIDTH - 24,height:CGFloat = 16,textMargin:CGFloat = 6,marginHeight:CGFloat = 4,marginWidth:CGFloat = 4) -> CGFloat {
        var tagBtnX: CGFloat = leftMargin
        var tagBtnY: CGFloat = topMargin
        
        for (_, value) in texts.enumerated() {
            let tagTextSize: CGSize = value.getSize(font: UIFont.systemFont(ofSize: font), maxSize: CGSize(width: width, height: height))
            if tagBtnX + tagTextSize.width + textMargin * 2 > SCREEN_WIDTH - leftMargin * 2 {
                tagBtnX = leftMargin
                tagBtnY += height + marginHeight
            }
            tagBtnX = tagBtnX + tagTextSize.width + textMargin * 2 + marginWidth // 间距
        }
        return tagBtnY + height + topMargin
    }
    
    public func initViews(texts: [String], singlelLine: Bool = false) -> CGFloat {
        
        removeBtns()
        if texts.count == 0 { return 0 }
        var tagBtnX: CGFloat = leftMargin
        var tagBtnY: CGFloat = topMargin
        for (i, value) in texts.enumerated() {
            let attributeString = NSAttributedString(string: value, attributes: [NSAttributedString.Key.font:font, NSAttributedString.Key.foregroundColor: textColor])
            let attStr = NSMutableAttributedString(attributedString: attributeString)
            var tagTextSize: CGSize = CGSize.zero
            switch i {
            case 0:
                tagTextSize = self.addImage(imageName: "diyi", attStr: attStr)
            case 1:
                tagTextSize = self.addImage(imageName: "dier", attStr: attStr)
            case 2:
                tagTextSize = self.addImage(imageName: "disan", attStr: attStr)
                
            default:
                
                
                tagTextSize = value.getSize(font: self.font, maxSize: CGSize(width: width, height: height))
                
            }

            if tagBtnX + tagTextSize.width + textMargin * 2 > self.width - leftMargin * 2 {
                if !singlelLine {
                    tagBtnX = leftMargin
                    tagBtnY += height + marginHeight
                } else {
                    break
                }
            }
            let tagBtn: YYLabel = YYLabel(frame: CGRect(x: tagBtnX, y: tagBtnY, width: tagTextSize.width + textMargin * 2, height: height))
            tagBtn.numberOfLines = 0
            tagBtn.preferredMaxLayoutWidth = tagTextSize.width
            
            tagBtn.textColor = textColor
            tagBtn.font = self.font

            switch i {
            case 0:
                tagBtn.attributedText = attStr
            case 1:
                tagBtn.attributedText = attStr
            case 2:
                tagBtn.attributedText = attStr
            default:
                tagBtn.text = value
            }
            
            self.addSubview(tagBtn)
            tagBtnX = tagBtn.frame.maxX + marginWidth // 间距
        
        }
        return tagBtnY + height + topMargin
    }
    
    func addImage(imageName: String, attStr: NSMutableAttributedString) -> CGSize {
        
        let first: UIImageView = UIImageView(image: UIImage(named: imageName))
        first.frame = CGRect(x: 0, y: 0, width: 15, height: 15)
        let size = CGSize(width: first.frame.size.width + 10, height: first.frame.size.height)
        let attImagefir = NSMutableAttributedString.yy_attachmentString(withContent: first, contentMode: UIView.ContentMode.scaleAspectFit, attachmentSize: size, alignTo: font, alignment: YYTextVerticalAlignment.center)
        attStr.insert(attImagefir, at: 0)
        
        // 计算文本尺寸
        let maxSize = CGSize(width: width, height: CGFloat(MAXFLOAT))
        let container = YYTextContainer(size: maxSize)
        let layout = YYTextLayout(container: container, text: attStr)
        
        return layout!.textBoundingSize
    }
    
    public func removeBtns() {
        if self.subviews.count > 0 {
            let _ = self.subviews.map { $0.removeFromSuperview() }
        }
    }

    @objc func tagBtnAction(sender: UIButton) {
        print(sender.tag)
        sender.backgroundColor = textColor
        sender.isSelected = true
    }
/*
     YYLabel *contentL = [[YYLabel alloc] init];
     //设置多行
     contentL.numberOfLines = 0;
     //这个属性必须设置，多行才有效
     contentL.preferredMaxLayoutWidth = kScreenWidth -32;

      NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithAttributedString:[OSCBaseCommetView contentStringFromRawString:commentItem.content withFont:24.0]];

     //可以将要插入的图片作为特殊字符处理
     //需要使用 YYAnimatedImageView 控件，直接使用UIImage添加无效。

     YYAnimatedImageView *imageView1= [[YYAnimatedImageView alloc] initWithImage:[UIImage imageNamed:@"ic_quote_left"]];
     imageView1.frame = CGRectMake(0, 0, 16, 16);

     YYAnimatedImageView *imageView2= [[YYAnimatedImageView alloc] initWithImage:[UIImage imageNamed:@"ic_quote_right"]];
     imageView2.frame = CGRectMake(0, 0, 16, 16);
     // attchmentSize 修改，可以处理内边距
     NSMutableAttributedString *attachText1= [NSMutableAttributedString attachmentStringWithContent:imageView1 contentMode:UIViewContentModeScaleAspectFit attachmentSize:imageView1.frame.size alignToFont:[UIFont systemFontOfSize:24] alignment:YYTextVerticalAlignmentCenter];

     NSMutableAttributedString *attachText2= [NSMutableAttributedString attachmentStringWithContent:imageView2 contentMode:UIViewContentModeScaleAspectFit attachmentSize:imageView2.frame.size alignToFont:[UIFont systemFontOfSize:24] alignment:YYTextVerticalAlignmentCenter];

      //插入到开头
     [attri insertAttributedString:attachText1 atIndex:0];
      //插入到结尾
     [attri appendAttributedString:attachText2];

     //用label的attributedText属性来使用富文本
     contentL.attributedText = attri;

     CGSize maxSize = CGSizeMake([UIScreen mainScreen].bounds.size.width - 32, MAXFLOAT);

     //计算文本尺寸
     YYTextLayout *layout = [YYTextLayout layoutWithContainerSize:maxSize text:attri];
     contentL.textLayout = layout;
     CGFloat introHeight = layout.textBoundingSize.height;


     contentL.frame =  commentItem.layoutInfo.contentTextViewFrame;
     contentL.width = maxSize.width;

     contentL.height = introHeight + 50;

     [self addSubview:contentL];
     */
}
