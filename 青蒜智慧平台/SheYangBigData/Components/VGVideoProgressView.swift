//
//  VGVideoProgressView.swift
//  vgbox
//
//  Created by 宋海胜 on 2019/4/13.
//  Copyright © 2019年 Swift Xcode. All rights reserved.
//  播放控制条

import UIKit

class VGVideoProgressView: UIView {
    
    var playOrPauseClosure: (()->Void)?
    var sliderChangeClosure: ((Float)->Void)?
    var fullScreenClosure: (()->Void)?
    
    // 下载进度
    fileprivate var progress: UIProgressView = {
        let pro: UIProgressView = UIProgressView()
        pro.tintColor = .white
        pro.trackTintColor = Constant.color_104
        return pro
    }()
    
    // 播放进度
    fileprivate var slider: UISlider = {
        let slid: UISlider = UISlider()
        slid.setThumbImage(UIImage(named: "icon_control_dot"), for: .normal)
        slid.addTarget(self, action: #selector(sliderAction), for: .valueChanged)
        slid.maximumTrackTintColor = .clear
        return slid
    }()
    
    fileprivate lazy var playOrPauseBt: UIButton = {
        let bt: UIButton = UIButton()
        bt.addTarget(self, action: #selector(playOrPauseAction), for: .touchUpInside)
        bt.setImage(UIImage(named: "icon_play"), for: .normal)
        return bt
    }()
    
    fileprivate lazy var fullScreenBt: UIButton = {
        let bt: UIButton = UIButton()
        bt.addTarget(self, action: #selector(fullScreenAction), for: .touchUpInside)
//        bt.setImage(UIImage(named: "icon_full_screen"), for: .normal)
        return bt
    }()
    
    fileprivate var currentTimeLb: UILabel = {
        let lb: UILabel = UILabel()
        lb.text = "00:00"
        lb.textColor = .white
        lb.font = UIFont.systemFont(ofSize: 13)
        lb.textAlignment = .center
        return lb
    }()
    
    fileprivate var totalTimeLb: UILabel = {
        let lb: UILabel = UILabel()
        lb.text = "00:00"
        lb.textColor = .white
        lb.font = UIFont.systemFont(ofSize: 13)
        lb.textAlignment = .center
        return lb
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @objc func playOrPauseAction(sender: UIButton) {
        self.playState(sender: sender)
        if let closure = playOrPauseClosure {
            closure()
        }
    }
    
    @objc func fullScreenAction(sender: UIButton) {
        if let closure = fullScreenClosure {
            closure()
        }
    }
    
    @objc func sliderAction(sender: UISlider) {
        if let closure = sliderChangeClosure {
            closure(sender.value)
        }
    }
    
    public func setProgress(float: Float) {
        self.progress.setProgress(float, animated: true)
    }
    
    public func setSlider(float: Float) {
        self.slider.setValue(float, animated: true)
    }
    
    public func setPlayState(play: Bool) {
        playOrPauseBt.isSelected = !play
        self.playState(sender: playOrPauseBt)
    }
    
    public func setCurrentTime(time: CGFloat) {
        self.currentTimeLb.text = Date.secondsToMinSec(second: time)
    }
    
    public func setTotalTime(time: CGFloat) {
        self.totalTimeLb.text = Date.secondsToMinSec(second: time)
    }
    
    fileprivate func playState(sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            sender.setImage(UIImage(named: "icon_pause"), for: .normal)
        } else {
            sender.setImage(UIImage(named: "icon_play"), for: .normal)
        }
    }
    
    fileprivate func initViews() {

        self.addSubview(self.playOrPauseBt)
        self.addSubview(self.currentTimeLb)
        self.addSubview(self.fullScreenBt)
        self.addSubview(self.totalTimeLb)
        self.addSubview(self.progress)
        self.addSubview(self.slider)
        
        self.playOrPauseBt.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.left.equalToSuperview().offset(20)
            make.width.equalTo(30)
        }
        
        self.currentTimeLb.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.left.equalTo(self.playOrPauseBt.snp.right)
            make.width.equalTo(45)
        }
        
        self.fullScreenBt.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.right.equalToSuperview().offset(-20)
            make.width.equalTo(30)
        }
        
        self.totalTimeLb.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.right.equalTo(self.fullScreenBt.snp.left)
            make.width.equalTo(45)
        }
        
        self.progress.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalTo(self.currentTimeLb.snp.right)
            make.right.equalTo(self.totalTimeLb.snp.left)
            make.height.equalTo(1)
        }
        
        self.slider.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview().offset(-0.5)
            make.left.equalTo(self.progress.snp.left).offset(-1)
            make.right.equalTo(self.progress.snp.right)
            make.height.equalTo(30)
        }
    }
}


