//
//  VGImageButton.swift
//  vgbox
//
//  Created by super song on 2019/8/21.
//  Copyright © 2019 Swift Xcode. All rights reserved.
//

import UIKit

/// 自适应展示image，并扩大点击区域
class VGImageButton: UIView {

    public var imageView: UIImageView = {
        let v: UIImageView = UIImageView()
        v.isUserInteractionEnabled = true
        return v
    }()
    
    public var btn: UIButton = {
        let v: UIButton = UIButton()
        return v
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    fileprivate func initViews() {
        
        addSubview(imageView)
        addSubview(btn)
        imageView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
        btn.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }

}
