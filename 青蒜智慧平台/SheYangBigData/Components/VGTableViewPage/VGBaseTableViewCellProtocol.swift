//
//  VGBaseListTableViewCellProtocol.swift
//  vgbox
//
//  Created by 宋海胜 on 2019/6/30.
//  Copyright © 2019 宋海胜. All rights reserved.
//

import UIKit

protocol VGBaseListTableViewCellProtocol {
    
    func configCell(_ model: Any, _ indexPath: IndexPath,_ dataSource : [Any]) -> Void
}

protocol VGBaseRequestProtocol : class {
    
    func getUrl() -> String
    func getParams() -> [String : Any]
    func getEmptyPageTitleAndImgUrl() -> (String, String, CGFloat)
    func getRowHeight(indexPath: IndexPath) -> CGFloat
}

// 在扩展中实现的方法为可选方法
extension VGBaseRequestProtocol {
    
    // row高度 为0时->自适应高度
    func getRowHeight(indexPath: IndexPath) -> CGFloat {
        return 0
    }
    
    func getEmptyPageTitleAndImgUrl() -> (String, String, CGFloat) {
        return ("暂无", "enro_mana_list_empty", -30)
    }
}

