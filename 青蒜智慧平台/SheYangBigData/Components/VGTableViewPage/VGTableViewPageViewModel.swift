//
//  VGTableViewPageViewModel.swift
//  vgbox
//
//  Created by a on 2019/12/18.
//  Copyright © 2019年 Swift Xcode. All rights reserved.
//

import UIKit
import HandyJSON
import RxSwift
import Moya
import Alamofire


///  列表页基础类 - ViewModel
class VGTableViewPageViewModel<T: HandyJSON>: NSObject, VGTableViewPageProtocol {
    
    public var vg_pagerModel: SGPagerModel<SGPagerTestModel>?
    
    public var getDataClosure: ((Int) -> Void)?
    
    public var loadDataClosure: ((Bool) -> Void)?
    
    var emptyViewParams: (String, String, CGFloat, CGFloat)!
    
    public var vg_dataCount: Int = 0
    
    public var vg_isLoading: Bool = false
    
    public var vg_pageNo: Int = 1
    
    public var size: Int = 10
    
    public var isNeedCache:Bool = false
    
    public var dataSource: [T] = [] {
        didSet {
            vg_dataCount = dataSource.count
        }
    }
    
    public var model: SGBaseListModel<T>?
    
    public var title: String = "列表页标题"
    
    public var tableView: UITableView!
    
    var type: SYApiManager!
    
    var dispose: DisposeBag!
    
    override init() {
        super.init()
//        getCacheData()
    }
    
    ///解档-取数据
    public func getLocalData(url: String,succeed: (() -> Void)?) {
        dataSource.removeAll()
        model = nil
        
//        let resData:Data? = UserDefaults.standard.value(forKey: url + UserInstance.userId!) as? Data
        let data = UserDefaults.standard.value(forKey: url + UserInstance.userId!)
        let model = data as! SGBaseListModel<T>
        
        self.vg_pagerModel = SGPagerModel<SGPagerTestModel>()//PYQPagerModel()
        self.vg_pagerModel?.total = model.data?.total
        self.vg_pagerModel?.current = model.data?.current
        self.vg_pagerModel?.size = model.data?.size
        self.vg_pagerModel?.pages = model.data?.pages
        
        var s: [T]!
        if let d = model.data?.records {
            s = d
        }
        if let s = s{
            self.dataSource = s
            self.model = model
            succeed?()
        }
        
    }
    
    // 获取缓存
    fileprivate func getCacheData() {
        
        guard isNeedCache else {
            return
        }
        self.getLocalData(url: type.path) {[weak self] in
            DispatchQueue.main.async {
                if let weakSelf = self {
                    weakSelf.tableView.reloadData()
                    weakSelf.tableView.vg_endTableViewRefresh()
                }
            }
        }
    }
    
    public func getList(type: SYApiManager, dispose: DisposeBag) {
        
        vg_isLoading = true
        self.type = type
        self.dispose = dispose
        
        self.baseListRequest(disposeBag: dispose, type: type)
    }
    
    fileprivate func refreshViews(_ success: Bool) {
        if success {
//            tableView.reloadData()
        }
        tableView.vg_endTableViewRefresh()
        self.vg_isLoading = false
    }
    
    // MARK: - VGTableViewPageProtocol
    func vg_tableViewGetList() {
        getDataClosure?(vg_pageNo)
    }
    
    func vg_tableViewGetEmptyPageTitleAndImgUrl() -> (String, String, CGFloat, CGFloat) {
        return self.emptyViewParams
    }
    
    func baseListRequest(disposeBag: DisposeBag, type: SYApiManager) {
        
        ApiManagerProvider.rx.request(type)
            .asObservable()
            .mapHandyJsonModel(type.path, SGBaseListModel<T>.self)
            .subscribe {[weak self] (event) in
                if let `self` = self {
                    
                    switch event {
                    case let .next(data):
                        let dic: Dictionary = UserDefaults.standard.value(forKey: type.path + UserInstance.userId! + REQUEST_DIC) as? Dictionary ?? [:]
                        let code: String = (dic["code"] as? String) ?? ""
                        let message: String = (dic["message"] as? String) ?? ""
                        
                        if code == "10000" {
                            DispatchQueue.main.async {
                                HUDUtil.hideHud()
                            }
                            let model = data
                            self.handleData(url: type.path, data: model)
                            self.loadDataClosure?(true)
                            self.refreshViews(true)
                        } else {
                            DispatchQueue.main.async {
                                if code == "10001" {//"session失效！"
                                    DispatchQueue.main.async {
                                        HUDUtil.hideHud()
                                    }
                                    SYTokenCheck.toLoginVC()
                                } else if code == "30000" {
                                    DispatchQueue.main.async {
                                        HUDUtil.hideHud()
                                    }
                                        SYTokenCheck.toIdcardVC()
                                    
                                }else {
                                    HUDUtil.showBlackTextView(text: message, detailText:"", delay: 1.5) {
                                        
                                    }
                                    self.loadDataClosure?(false)
                                    self.refreshViews(false)
                                }
                            }
                            
                            print(message)
                            break
                        }
                        
                        break
                    case let .error(error):
                        DispatchQueue.main.async {
                            HUDUtil.showBlackTextView(text: error.localizedDescription, detailText:"", delay: 1.5) {
                                
                            }
                            self.loadDataClosure?(false)
                            self.refreshViews(false)
                        }
                        print(error)
                        break
                        
                    default:
                        break
                    }
                }
        }.disposed(by: disposeBag)
        
    }
    
    func handleData(url: String, data: SGBaseListModel<T>?) {
        
        self.vg_pagerModel = SGPagerModel<SGPagerTestModel>()//PYQPagerModel()
        self.vg_pagerModel?.total = data?.data?.total
        self.vg_pagerModel?.current = data?.data?.current
        self.vg_pagerModel?.size = data?.data?.size
        self.vg_pagerModel?.pages = data?.data?.pages
        
        var s: [T]!
        if let d = data?.data?.records {
            s = d
        }
        if self.vg_pageNo == 1 {
            if let s = s {
                self.dataSource = s
                self.model = data
                //归档-第一页数据
//                UserDefaults.standard.setValue(data, forKey: url + UserInstance.userId!)
            }
        } else {
            self.dataSource += s
        }
    }
}

