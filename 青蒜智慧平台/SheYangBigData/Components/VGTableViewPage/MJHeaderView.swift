//
//  MJHeaderView.swift
//  vgbox
//
//  Created by ztsapp on 2017/6/3.
//  Copyright © 2017年 Swift Xcode. All rights reserved.
//

import Foundation
import MJRefresh

class MJHeaderView: MJRefreshHeader {
    
    fileprivate var refreshLab: UILabel!
    private var refreshText: String!
    
    public var leftview: UIView!
    public var rightview: UIView!
    
    init(frame: CGRect, refreshText: String, height: CGFloat = 30) {
        super.init(frame: frame)
        self.refreshText = refreshText
        layoutUI(height)
        self.clipsToBounds = true
        self.pullingPercent = 0
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func layoutUI(_ height: CGFloat) {
        refreshLab = UILabel()
//        refreshLab.backgroundColor = Constant.bgViewColor
        refreshLab.text = refreshText
        refreshLab.font = UIFont.systemFont(ofSize: 13)
        refreshLab.textAlignment = .center
        refreshLab.textColor = UIColor(hex: 0x222222)
        refreshLab.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: height)
        addSubview(refreshLab)
        
        leftview = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width/2, height: 30))
//        leftview.backgroundColor = Constant.bgViewColor
        addSubview(leftview)
        
        rightview = UIView(frame: CGRect(x: self.frame.width/2, y: 0, width: self.frame.width/2, height: 30))
//        rightview.backgroundColor = Constant.bgViewColor
        addSubview(rightview)
    }

}

extension MJHeaderView {
    
    override func beginRefreshing() {
        leftview.frame.origin.x = -self.frame.width / 2
        rightview.frame.origin.x = self.frame.width
        super.beginRefreshing()
    }
    
    override func endRefreshing() {
        UIView.animate(withDuration: 0.25, animations: { [weak self] in
            self?.leftview.frame.origin.x = 0
            self?.rightview.frame.origin.x = (self?.frame.width)! > 700 ? SCREEN_WIDTH / 2 : (self?.frame.width)!/2
        }) { (finsh) in
            super.endRefreshing()
        }
    }
}

extension MJHeaderView {
    
    func setRefreshText(_ text: String) {
        refreshLab.text = text
    }
    
}
