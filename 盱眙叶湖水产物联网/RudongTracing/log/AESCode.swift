//
//  AESCode.swift
//  UnmannedShip
//
//  Created by 宋海胜 on 2022/4/26.
//  Copyright © 2022 宋海胜. All rights reserved.
//

import UIKit
import CryptoSwift


class AESCode: NSObject {

    
    static let key = "1234567890123456"

    static let iv = "1234567890123456"

    static let salt = "1234567890123456"

 
    //MARK: -AES-ECB128加密

    public static func endcode_AES_CBC(strToEncode:String)->String {

        var encodeString = ""
        do {
            let aes =  try AES(key: key.bytes, blockMode: CBC(iv: iv.bytes), padding: .zeroPadding)
            let encoded = try aes.encrypt(strToEncode.bytes)
            encodeString = encoded.toBase64()
            print(encodeString)
        } catch {
            print(error.localizedDescription)
            
        }
        return encodeString
    }

    //  MARK:  -AES-ECB128解密
    public static func decode_AES_ECB(strToDecode:String)->String {

        //decode base64
        let data = NSData(base64Encoded: strToDecode, options: NSData.Base64DecodingOptions.init(rawValue: 0))
        // byte 数组
        var encrypted: [UInt8] = []
        let count = data?.length

        // 把data 转成byte数组
        for i in 0..<count! {
            var temp:UInt8 = 0
            data?.getBytes(&temp, range: NSRange(location: i,length:1))
            encrypted.append(temp)
        }

        // decode AES
        var decrypted: [UInt8] = []
        do {
            decrypted = try AES(key: key.bytes, blockMode: ECB(), padding: .pkcs7).decrypt(encrypted)
        } catch {

        }

        // byte 转换成NSData
        let encoded = Data(decrypted)
        var str = ""
        //解密结果从data转成string
        str = String(bytes: encoded.bytes, encoding: .utf8)!
        return str
    }

    

    //MARK: -MD5 加密
    public static func MD5(codeString: String) -> String {
        // 加盐加密
        let md5String =  (codeString + salt).md5()
        return md5String
    }

}
