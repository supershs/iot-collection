//
//  LodingView.swift
//  33web
//
//  Created by iOS开发 on 2023/8/30.
//

import UIKit

class LodingView: SYBaseView {
    
    var imgHeight: CGFloat = 150
    let imgView: UIImageView = {
        let v = UIImageView()
        v.sy_name("home_headIcon")
        return v
    }()
    
    let title: UILabel = {
        let v = UILabel()
        v.text = "管农场"
        v.font = UIFont.systemFont(ofSize: 18)
        v.textColor = .white
        return v
    }()
    let address: UILabel = {
        let v = UILabel()
        v.text = "玄武区童卫路186号"
        v.font = UIFont.systemFont(ofSize: 15)
        v.textColor = .white
        return v
    }()
    
    let subTipText: UILabel = {
        let v = UILabel()
        v.text = "晴|东风4级|湿度52%"
        v.font = UIFont.systemFont(ofSize: 14)
        v.textColor = .white
        return v
    }()
    let sunImg: UIImageView = {
        let v = UIImageView()
        v.sy_name("sun")
        return v
    }()
    
    let btn: UIButton = {
        let v = UIButton()
        v.backgroundColor = .yellow
        v.setTitle("刷新", for: .normal)
        v.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        v.setTitleColor(.white, for: .normal)
        v.layer.cornerRadius = 5
        v.layer.masksToBounds = true
        return v
    }()
    
    override func initViews() {
        
        addSubview(imgView)
        addSubview(address)
        addSubview(title)
        addSubview(sunImg)
        addSubview(subTipText)
        imgView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(STATUSBAR_HEIGHT + 10)
            make.left.right.equalToSuperview()
            make.height.equalTo(173)
        }
        title.snp.makeConstraints { (make) in
            make.top.equalTo(imgView.snp.bottom).offset(10)
            make.left.right.equalToSuperview()
            make.height.equalTo(20)
        }
        address.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(STATUSBAR_HEIGHT + 41)
            make.left.equalToSuperview().offset(30)
            make.height.equalTo(20)
        }
        sunImg.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(STATUSBAR_HEIGHT + 77)
            make.height.equalTo(46)
            make.width.equalTo(50)
        }
        subTipText.snp.makeConstraints { (make) in
            make.left.equalTo(sunImg.snp.right).offset(5)
            make.centerY.equalTo(sunImg.snp.centerY)
            make.height.equalTo(20)
        }
    
    }

}
