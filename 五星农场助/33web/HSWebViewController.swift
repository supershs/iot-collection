//
//  HSWebViewController.swift
//  33web
//
//  Created by 宋海胜 on 2020/11/25.
//

import UIKit
import WebKit
import SwiftWeb
import MapKit

import RxSwift
import RxAlamofire
import HandyJSON
import MapKit


enum JsMethod {
    case passGPSInfo
    case passVersion
    case passVoiceContent
    case passPayResult
    case identifyContent
    case setSpeechStatusH5//开始播放1 结束 0
    case invokeH5Content//语音识别读取文字
    case updateVoiceSdk//语音识别 区分百度还是讯飞 百度1 讯飞 0
    
}
let kIFlyVoiceWakeuperIsOpen = "IFlyVoiceWakeuperIsOpen"
let  BAIDU_APP_ID = "47982565"
let  BAIDU_API_KEY = "kOHl8t5wSaBigGauKReVF8bP"
let  BAIDU_SECRET_KEY = "ZLXzSYpKp1b1ThCmy50MMQ9KCDNpvOCH"
// 自定义编辑
class HSWebViewController: BaseWebViewController, XYVoiceRecognizeProtocol,BDSClientASRDelegate, BDSSpeechSynthesizerDelegate,IFlySpeechSynthesizerDelegate {
   
    
    
    var iFlySpeechSynthesizer : IFlySpeechSynthesizer?
    var iFlySpeechRecognizer : IFlySpeechRecognizer?
    
    var resultClosure :  ((IFlySpeechError) -> Void)?
    
    var backClosure:((Any)-> Void)?
    var hasTab: Bool = false
    fileprivate var location = LocationManager()
    fileprivate var isOnece = false
    fileprivate var configManager = Tool()
    fileprivate var backImgView = UIImageView()
    fileprivate var lodingView = LodingView()
    fileprivate var voiceView: XYVoiceRecognizeManager?
    public var canAllButUpsideDown = false
    let disposeBag = DisposeBag()
    var asrEventManager: BDSEventManager?
    var IntCount = 0
    var distinguish = 0
    
    deinit {
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "getGPSInfo")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "saveUnreadMsgCnt")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "getVersion")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "passVideoParams")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "leaveVideoPage")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "startVoice")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "stopVoice")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "toMiniProgram")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "toGaodeApp")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "toPay")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "toShare")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "toDownLoad")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "wakeUpIFly")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "stopWakeUpFly")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "syntheticSpeech")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "webGoBack")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "loginOut")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "speechRecognition")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "toHomepage")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "speechText")//开始播报
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "pauseSpeechText")//暂停播报
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "continueSpeechText")//继续播报
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "releaseSpeechText")//停止播放 用于h5那边处于播报时返回
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "updateSpeed")//  停止播放 用于h5那边处于播报时返回
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "startSpeechRecognize")//开始监听
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "stopSpeechRecognize")// 结束监听
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "updateVoiceSdk")//区分百度还是讯飞
        
    }
    
    func onCompleted(_ error: IFlySpeechError!) {
        
        if let re = resultClosure {
                 re(error)
             }
        
        print(error.errorDesc ?? error.errorCode);
        
        print("errorCode == ", error.errorCode);
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.showLoading()
        
        //        self.view.addSubview(self.backImgView)
         
        // 顶部状态栏高度问题解决
        self.configManager.topMangain(webView: webView, vc: self)
        
        // 网络判断，重新加载网页
        judgeNetwork()
        // 经纬度
        location.locationProtocol = self
        // 语音听写、播报
        self.voiceView = XYVoiceRecognizeManager(currentVC: self)
        self.voiceView!.delegate = self
        self.webView.frame = CGRect(x: 0, y: STATUSBAR_HEIGHT, width: SCREEN_WIDTH, height: SCREEN_HEIGHT - STATUSBAR_HEIGHT)
        self.progressView.isHidden = true
        //        IflyVoiceWeakup.wakeUp(self)
        //        self.iflyStartListening()
        //        self.configVoiceRecognitionClient()//百度语音识别
//                self.configureSDK()//百度语音合成
        // 启动图复制图片、web未加载完成时覆盖页面
        self.backImgView = self.configManager.getLanuchboardImage()
        self.backImgView.frame = self.view.bounds
        self.lodingView.frame = self.view.bounds
//        self.view.addSubview(self.lodingView)
        
       
    }
    
   
    
    
    // Mark - implement BDSSpeechSynthesizerDelegate

    func synthesizerSpeechStartSentence(_ SpeakSentence: Int) {
        print(SpeakSentence,"SpeakSentence")
        if SpeakSentence == 0  {
            self.jsAction(param: "1", type: .setSpeechStatusH5)
        }

    }
    
    func synthesizerupdateVoiceSdk(_ updateVoiceSdk: Int) {
        print(updateVoiceSdk,"updateVoiceSdk")
        if updateVoiceSdk == 0  {
            self.jsAction(param: "0", type: .updateVoiceSdk)
        }

    }
    func synthesizerSpeechEndSentence(_ SpeakSentence: Int) {
        print("synthesizerSpeechEndSentence",SpeakSentence)
        if (IntCount-2) == SpeakSentence {
//            [[BDSSpeechSynthesizer sharedInstance] cancel];
            BDSSpeechSynthesizer.sharedInstance().cancel()
            self.jsAction(param: "0", type: .setSpeechStatusH5)
            
        }

    }
    // Mark - implement BDSClientASRDelegate

    func voiceRecognitionClientWorkStatus(_ workStatus: Int32, obj aObj: Any!) {
        
        //        print(workStatus, aObj as! String)
        switch workStatus{
        case 4://持续识别中
            let dic = aObj as! NSDictionary
            print(dic,"4444")
            break
            
        case 5://最终结果
            let dic = aObj as! NSDictionary
            print(dic,dic["results_recognition"])
            let results_recognition = dic["results_recognition"] as!  [String]
            let joinString = results_recognition.joined(separator: "")
            self.jsAction(param: joinString , type: .invokeH5Content)
            print(dic,"55555" )
            break
        case 13://识别结束
            let dic = aObj as! NSString
            print(dic,"133332" )
            break
        case 8://错误
            let error = aObj as!NSError
            print(error)
        default:
            print(workStatus,"erooror",aObj)
            
            break
            
        }
    }
    
    func configVoiceRecognitionClient(){
        self.asrEventManager = BDSEventManager.createEventManager(withName: BDS_ASR_NAME)
        self.asrEventManager?.setDelegate(self)
        self.asrEventManager?.setParameter(EVRDebugLogLevelTrace, forKey: BDS_ASR_DEBUG_LOG_LEVEL)
        self.asrEventManager?.setParameter([BAIDU_API_KEY,BAIDU_SECRET_KEY], forKey: BDS_ASR_API_SECRET_KEYS)
        self.asrEventManager?.setParameter(BAIDU_APP_ID, forKey: BDS_ASR_OFFLINE_APP_CODE)
        self.asrEventManager?.setParameter("1537", forKey: BDS_ASR_PRODUCT_ID)
//        self.configDNNMFE()
        let path = Bundle.main.path(forResource: "bds_easr_mfe_dnn", ofType:"dat")
        self.asrEventManager?.setParameter(path, forKey: BDS_ASR_MFE_DNN_DAT_FILE)
        let cmvn_dnn_filepath = Bundle.main.path(forResource: "bds_easr_mfe_cmvn", ofType:"dat")
        self.asrEventManager?.setParameter(cmvn_dnn_filepath, forKey: BDS_ASR_MFE_CMVN_DAT_FILE)
        self.asrEventManager?.setParameter(false, forKey: BDS_ASR_ENABLE_LONG_SPEECH)
        self.asrEventManager?.sendCommand(BDS_ASR_CMD_START)

    }
    
    func configureSDK() {
        BDSSpeechSynthesizer.setLogLevel(BDS_PUBLIC_LOG_VERBOSE)
        BDSSpeechSynthesizer.sharedInstance().setSynthesizerDelegate(self)
        BDSSpeechSynthesizer.sharedInstance().setApiKey(BAIDU_API_KEY, withSecretKey: BAIDU_SECRET_KEY)
        BDSSpeechSynthesizer.sharedInstance().setSynthParam(1, for: BDS_SYNTHESIZER_PARAM_SPEAKER)
        BDSSpeechSynthesizer.sharedInstance().setSynthParam(7, for: BDS_SYNTHESIZER_PARAM_SPEED)
        
        
    }
    
    
    override func injectSwiftMethod(_ config: WKWebViewConfiguration) {
        
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "getGPSInfo")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "saveUnreadMsgCnt")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "getVersion")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "passVideoParams")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "leaveVideoPage")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "startVoice")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "stopVoice")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "toMiniProgram")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "toGaodeApp")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "toPay")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "toShare")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "toDownLoad")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "wakeUpIFly")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "stopWakeUpFly")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "syntheticSpeech")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "webGoBack")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "loginOut")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "speechRecognition")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "toHomepage")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "speechText")//开始播报
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "pauseSpeechText")//暂停播报
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "continueSpeechText")//  继续播报
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "releaseSpeechText")//  停止播放 用于h5那边处于播报时返回
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "updateSpeed")//  停止播放 用于h5那边处于播报时返回
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "startSpeechRecognize")//  开始监听
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "stopSpeechRecognize")//  结束监听
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "updateVoiceSdk")//  区分百度还是讯飞
        
        
    }
    //加载完成
    override func jsAction() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
         // 延迟 2 秒后执行的代码
         // 在这里关闭 loading
            self.hideLoading()

        }
    }
    
    // 在viewDidLoad中模态不行，此时页面还没加载完成
    fileprivate func judgeNetwork() {
        let myAppdelegate = UIApplication.shared.delegate as! AppDelegate
        myAppdelegate.networkAuthStatus(stateClosure: {[weak self] state in
            if let `self` = self {
                print(state,"state")
                if state {
                    DispatchQueue.main.async {
//                        self.loadWeb()
                        
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        DispatchQueue.main.async {
                            self.loadWeb()
//                            self.lodingView.isHidden = true
                        }
                    }
                }
            }
        })
    }
    
    fileprivate func jsAction(param: String, type: JsMethod) {
        
        var javascript: String = ""
        
        switch type {
            
        case .passGPSInfo:
            javascript = "javascript:passGPSInfo(\"\(param)\")"
            
        case .passVersion:
            javascript = "javascript:passVersion(\"\(param)\")"
            
        case .passVoiceContent:
            javascript = "javascript:passVoiceContent(\"\(param)\")"
            
        case .passPayResult:
            javascript = "javascript:payResult(\"\(param)\")"
            
        case .identifyContent:
            javascript = "javascript:identifyContent(\"\(param)\")"
        case .setSpeechStatusH5:
            javascript = "javascript:setSpeechStatusH5(\"\(param)\")"
            
        case .invokeH5Content:
            javascript = "javascript:invokeH5Content(\"\(param)\")"

        case .updateVoiceSdk:
            javascript = "javascript:updateVoiceSdk(\"\(param)\")"

    }
        
        self.webView.evaluateJavaScript(javascript) { (res, error) in
            print("swift向js传值\n  param: \(param)\n  response: \(res ?? "nil")\n  error: \(error?.localizedDescription ?? "nil")")
        }
    }
        
        override func swiftAction(_ name: String, _ body: Dictionary<String, Any>) {
        
        switch name {
        case "webGoBack":
            if webView.canGoBack {
                webView.goBack()
            } else {
                self.sy_popVC()
            }
        case "loginOut":
            UserInstance.userLogout()
            self.navigationController?.popViewController(animated: false)
        case "getGPSInfo":
            gpsService()
            
        case "saveUnreadMsgCnt":
            let num = body["notReadNum"] as! NSNumber
            UIApplication.shared.applicationIconBadgeNumber = num.intValue > 99 ? 99 : num.intValue
            
        case "getVersion":
            let infoDic = Bundle.main.infoDictionary
            let appVersion = infoDic!["CFBundleShortVersionString"] as! String
            jsAction(param: appVersion, type: .passVersion)
        case "startVoice":
            print(name, "+", body)
            self.voiceView!.startIFlySpeechRecognize()
            
        case "stopVoice":
            self.voiceView!.stopIFlySpeechRecognize()
            
        case "toMiniProgram":
            let appId = body["appId"] as? String ?? ""
            let path = body["path"] as? String ?? ""
            print(appId,path)
        case "toGaodeApp":
            Tool.routePlanning(body)
        case "toDownLoad":
            let url = body["url"] as? String ?? ""
            UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
        case "callSomeOne":
            let phoneNum = body["phoneNum"] as? String ?? ""
            self.configManager.callSomeOne(phoneNum)
        case "wakeUpIFly"://语音唤醒
            IflyVoiceWeakup.wakeUp(self)
            self.iflyStartListening()
        case "stopWakeUpFly"://关闭语音功能
            self.iflyStopListening()
        case  "viewSyntheticSpeech"://有页面语音合成
            let contentSpeech = body["contentSpeech"] as? String ?? ""
            NotificationCenter.default.post(name: NSNotification.Name("contentSpeech"), object: self, userInfo: ["contentSpeech":contentSpeech])
        case "syntheticSpeech"://无页面语音合成
            let contentSpeech = body["contentSpeech"] as? String ?? ""
            SynthesizerModel.shared.startSpeak(speak: contentSpeech )
            SynthesizerModel.shared.resultClosure = { (iFlySpeechError) in
                //语音合成成功后再根据接口，去识别语音
                if iFlySpeechError.errorCode == 0 {
                    ASRConfig.shared.stopIdentify()
                    SynthesizerModel.shared.stopSpeak()
                }
            }
        case "speechRecognition"://语音识别
            print("我是语音识别")
            self.voiceView!.startIFlySpeechRecognize()
        case "stopSpeechRecognition":
            self.voiceView!.stopIFlySpeechRecognize()
                
        case "updateVoiceSdk":
//                let distinguish = body["distinguish"] as! String
                if  distinguish == 0{ //讯飞
                    //获取语音合成单例
                    iFlySpeechSynthesizer = IFlySpeechSynthesizer.sharedInstance()
                    //          //设置协议委托对象
                    iFlySpeechSynthesizer?.delegate = self
                }else{//百度
                    
                    self.configureSDK()
                    
                }
        case "speechText": //播报
                
                let joinedString = body["text"] as! String
                
                if  distinguish == 0{ //讯飞
                    //获取语音合成单例
                    iFlySpeechSynthesizer = IFlySpeechSynthesizer.sharedInstance()
                    //设置协议委托对象
                    iFlySpeechSynthesizer?.delegate = self
                    //设置在线工作方式
                    iFlySpeechSynthesizer?.setParameter(IFlySpeechConstant.type_CLOUD(), forKey: IFlySpeechConstant.engine_TYPE())
                    //设置音量，取值范围 0~100
                    iFlySpeechSynthesizer?.setParameter("50", forKey: IFlySpeechConstant.volume())
                    //发音人，默认为”xiaoyan”，可以设置的参数列表可参考“合成发音人列表”  x4_lingfeizhe_zl xiaoyan
                    iFlySpeechSynthesizer?.setParameter("aisjiuxu", forKey: IFlySpeechConstant.voice_NAME())
                    //保存合成文件名，如不再需要，设置为nil或者为空表示取消，默认目录位于library/cache下
                    iFlySpeechSynthesizer?.setParameter("tts.pcm", forKey: IFlySpeechConstant.tts_AUDIO_PATH())
                    //编码格式
                    iFlySpeechSynthesizer?.setParameter("unicode", forKey: IFlySpeechConstant.text_ENCODING())
                    iFlySpeechSynthesizer?.startSpeaking(joinedString)
                }else{//百度
                    
                self.configureSDK()
                                    
            let joinedString = body["text"] as! String
           
            let newStr = String(joinedString.dropFirst())
            var error: NSError?
            
            let aryString = joinedString.components(separatedBy: "，")
                    //            IntCount = aryString.count
            print(aryString.count)
            print("joinedString===",joinedString)
            print("aryString===",aryString)
            
                for aStr  in aryString {
                 let strID =  BDSSpeechSynthesizer.sharedInstance().speakSentence(aStr, withError: &error)
                   print(strID,"sssssss===",newStr,"dadfafda",aStr,"error====",error as Any)
                    }
                    
                }
                
            
        case "pauseSpeechText"://暂停播放
            print("pauseSpeechText")
                if  distinguish == 0{//讯飞
                    iFlySpeechSynthesizer?.pauseSpeaking()
                    
                }else{//百度
                    print("百度暂停播放")
                    
                    if BDSSpeechSynthesizer.sharedInstance().synthesizerStatus() == BDS_SYNTHESIZER_STATUS_WORKING{
                        BDSSpeechSynthesizer.sharedInstance().pause()}
//                    BDSSpeechSynthesizer.sharedInstance().pause()
                }
                
              
        case "continueSpeechText": //回复播放
            print("continueSpeechText")
               
                if  distinguish == 0{//讯飞
                    iFlySpeechSynthesizer?.resumeSpeaking()
                }else{//百度
                    print("百度恢复播放")
                    BDSSpeechSynthesizer.sharedInstance().resume()
//                    BDSSpeechSynthesizer.sharedInstance().synthesizerStatus()
                }
                

        case "releaseSpeechText":
                
            iFlySpeechSynthesizer?.stopSpeaking()
            BDSSpeechSynthesizer.sharedInstance().cancel()
            BDSSpeechSynthesizer.releaseInstance() 
        case "updateSpeed"://修改语速
            let speed = body["num"] as? Int ?? 7
            BDSSpeechSynthesizer.sharedInstance().setSynthParam(speed, for: BDS_SYNTHESIZER_PARAM_SPEED)
            print(body)
            
        case "startSpeechRecognize":
            self.configVoiceRecognitionClient()
        case "stopSpeechRecognize":
//            [self.asrEventManager sendCommand:BDS_ASR_CMD_STOP];
            self.asrEventManager?.sendCommand(BDS_ASR_CMD_STOP)
        case "toHomepage":
            
            UserInstance.userLogout()
            
            let token = body["token"] as? String ?? ""
            let roleId: String? = body["roleId"] as? String
            
            UserInstance.accessToken = token
            UserInstance.roleId = roleId
            
//            if let vc = UIApplication.shared.keyWindow?.rootViewController, vc.isKind(of: SYBaseTarBarViewController.self) {
//                self.dismiss(animated: true, completion: nil)
//                NotificationCenter.default.post(name: .Noti_changeRole, object: nil, userInfo: nil)
//
//            } else {
                
//                let tabbarVC = SYBaseTarBarViewController(roleId: roleId)
//                UIApplication.shared.keyWindow?.rootViewController = SYBaseNavigationController (rootViewController: tabbarVC)
//            }
            
        default:
            break
        }
    }
    
    public func iflyStartListening() {
        IFlyVoiceWakeuper.sharedInstance()?.startListening()
        UserDefaults.standard.set("true", forKey: kIFlyVoiceWakeuperIsOpen)
    }
    
    public func iflyStopListening() {
        IFlyVoiceWakeuper.sharedInstance()?.stopListening()
        UserDefaults.standard.set("false", forKey: kIFlyVoiceWakeuperIsOpen)
    }
    override func getH5Url(_ url: String) {
        
        if (url.contains("tel://")) {
            let phoneUrl = url.components(separatedBy: "tel://").last
            self.configManager.callSomeOne(phoneUrl!)
        }
    }
    
    override func loadSuccess() {
        UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseInOut, animations: {
            //            self.backImgView.alpha = 0
        }) { (finish) in
            //            self.hideLoading()
            //            self.backImgView.isHidden = true
            //            self.lodingView.isHidden = true
        }
    }
    
    override func loadFail() {
        //        backImgView.isHidden = false
        //        self.configManager.alertToReload(self) {
        //            self.loadWeb()
        //        }
    }
    
    // 定位
    fileprivate func gpsService() {
        
        //只获取一次
        isOnece = true
        self.location.startLocation()
    }
    
    func voiceContent(content: String) {
        self.jsAction(param: content, type: .passVoiceContent)
    }
    
    func payResult() {
        self.jsAction(param: "", type: .passPayResult)
    }
    func showLoading() {
        DispatchQueue.global().async {
            // 在后台线程中执行加载动画的操作
            // 这里可以使用第三方库或者自定义动画视图来实现 loding 动画效果
            
            // 加载完成后，回到主线程更新 UI
            DispatchQueue.main.async {
                // 更新 UI，显示加载动画
                HUDUtil.showHud()
            }
        }
    }
    
    func hideLoading() {
        // 在主线程隐藏 loading 动画
        DispatchQueue.main.async {
            // 隐藏加载动画，恢复界面响应
            HUDUtil.hideHud()
            
        }
    }
}

extension HSWebViewController: LocationProtocol {
    func getLocationSuccess(_ area: String, _ locality: String, _ subLocality: String, _ thoroughfare: String, _ name: String) {
        
    }
     
    func getGPSAuthorizationFailure() {
        let alert = UIAlertController(title: "提示", message: "请打开定位，以便获取您的位置信息", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func getGPSSuccess(latitude: Double, longitude: Double) {
        //		let  customLoc2D: CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude);
        let  customLoc2D: CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
        
        let gcjPt = JZLocationConverter.wgs84(toGcj02: customLoc2D);
        let lat = gcjPt .latitude
        let lng = gcjPt.longitude
        
        location.stopLocation()
        if (self.isOnece) {
            print("lng: \(lng) lat: \(lat)")
            let param = "\(lng),\(lat)"
            self.jsAction(param: param, type: .passGPSInfo)
            self.isOnece = false
        }
    }
    
    func getGPSFailure(error: Error) {
        self.isOnece = false
        print("getMoLocation error: \(error.localizedDescription)")
        if (!self.isOnece) {
            location.stopLocation()
        }
    }
}
