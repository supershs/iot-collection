//
//  AppDelegate.swift
//  33web
//
//  Created by 宋海胜 on 2020/11/25.
//

import UIKit
import CallKit
import CoreTelephony
import IQKeyboardManagerSwift
@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
//    var webVC = HSWebViewController(path: "http://192.168.0.60:8080/#/")
//    var wxDelegate = WXDelegate()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
//        [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
        UIApplication.shared.isIdleTimerDisabled = true
        window = UIWindow(frame: UIScreen.main.bounds)
        let nav = UINavigationController.init(rootViewController:  SYAccountLoginViewController())
//        let nav = UINavigationController.init(rootViewController:  webVC)

        nav.isNavigationBarHidden = true
        window?.rootViewController = nav
        window?.makeKeyAndVisible()
        IQKeyboardManager.shared.enable = true
        // 强制关闭暗黑模式
        if #available(iOS 13.0, *) {
            self.window?.overrideUserInterfaceStyle = .light
        }
        
        IFlySpeechUtility.createUtility("appid=\(APPID_VALUE)")
//        [IFlySetting setLogFile:LVL_NONE]
        IFlySetting.setLogFile(LOG_LEVEL())
        
        //Appid是应用的身份信息，具有唯一性，初始化时必须要传入Appid。
//        NSString *initString = [[NSString alloc] initWithFormat:@"appid=%@", @"YourAppid"];
//        [IFlySpeechUtility createUtility:initString];

        #if DEBUG // 判断是否在测试环境下
        print("当前环境：DEBUG")
        #else
        print("当前环境：RELEASE")
        #endif
        return true
    }
    func networkAuthStatus(stateClosure: @escaping ((Bool) -> Void)) {
        let cellularData = CTCellularData()
        cellularData.cellularDataRestrictionDidUpdateNotifier = {(state) in
            if (state == .restricted) {
                //拒绝
                if let t = UserDefaults.standard.value(forKey: "isFirstInApp") as? Bool, !t {
//                    self.networkSettingAlert()
                }
                stateClosure(false)
            } else if (state == .notRestricted) {
                //允许
                stateClosure(true)
            } else {
                //未知
                if let t = UserDefaults.standard.value(forKey: "isFirstInApp") as? Bool, !t {
//                    self.unknownNetwork()
                }
                stateClosure(false)
            }
        }
    }
}


