//
//  Util.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/1/12.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit

class Util: NSObject {
    
    /// 打电话
    class public func callSomeOne(_ tel: String) {
        let telprompt = "telprompt:\(tel)"
        if let url = URL(string: telprompt) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)//可以写拨打电话的回调
            } else {
                if UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    class public func sy_print(_ items: Any..., separator: String = " ", terminator: String = "\n") {
        print("\(#function) in \(#file)/n\(items)")
    }

    /// GCD定时器倒计时
    ///
    /// - Parameters:
    ///   - timeInterval: 间隔时间
    ///   - repeatCount: 重复次数
    ///   - handler: 循环事件,闭包参数: 1.timer 2.剩余执行次数
    class public func dispatchTimer(timeInterval: Double, repeatCount: Int, handler: @escaping (DispatchSourceTimer?, Int) -> Void) {
        
        if repeatCount <= 0 {
            return
        }
        let timer = DispatchSource.makeTimerSource(flags: [], queue: DispatchQueue.main)
        var count = repeatCount
        timer.schedule(deadline: .now(), repeating: timeInterval)
        timer.setEventHandler {
            count -= 1
            DispatchQueue.main.async {
                
                handler(timer, count)
            }
            if count == 0 {
                timer.cancel()
            }
        }
        timer.resume()
        
    }
    
    /// GCD实现定时器
       ///
       /// - Parameters:
       ///   - timeInterval: 间隔时间
       ///   - handler: 事件
       ///   - needRepeat: 是否重复
    class  func dispatchTimer(timeInterval: Double, handler: @escaping (DispatchSourceTimer?) -> Void, needRepeat: Bool) {
           
           let timer = DispatchSource.makeTimerSource(flags: [], queue: DispatchQueue.main)
           timer.schedule(deadline: .now(), repeating: timeInterval)
           timer.setEventHandler {
               DispatchQueue.main.async {
                   if needRepeat {
                       handler(timer)
                   } else {
                       timer.cancel()
                       handler(nil)
                   }
               }
           }
           timer.resume()
           
       }

    class  func getAppIconName() -> UIImageView {
 //        let tmpLaunchImages = Bundle.main.infoDictionary!["CFBundleIcons"]["CFBundlePrimaryIcon"]["CFBundleIconFiles"] as? [Any]
        let iconImageView = UIImageView()

        if let iconsDict = Bundle.main.infoDictionary?["CFBundleIcons"] as? [String: Any],
           let primaryIconDict = iconsDict["CFBundlePrimaryIcon"] as? [String: Any],
           let iconFiles = primaryIconDict["CFBundleIconFiles"] as? [String],
           let iconName = iconFiles.last {
            print("Icon Name: \(iconName)")
            var imge = UIImage(named: iconName)
            imge = imge?.imageByRemoveWhiteBg()
            iconImageView.image = imge
        }
        return iconImageView

   
     }
}

