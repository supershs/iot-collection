//
//  UIAlertController.swift
//  vgbox
//
//  Created by 宋海胜 on 2019/6/26.
//  Copyright © 2019 Swift Xcode. All rights reserved.
//

import UIKit

extension UIAlertController {

    var titleLabel : UILabel?{
        get{
            if let a = viewArray(root: self.view) {
                if a[1] is UILabel{
                    return a[1] as? UILabel
                }
            }
            return nil
        }
    }
    var messageLabel : UILabel?{
        get{
            if let a = viewArray(root: self.view) {
                if a[2] is UILabel{
                    return a[2] as? UILabel
                }
            }
            return nil
        }
    }
    
    func viewArray(root:UIView) -> [UIView]?{
        
        if let subview1 = root.subviews.first {
            if let subview2 = subview1.subviews.first {
                if let subview3 = subview2.subviews.first {
                    if let subview4 = subview3.subviews.first {
                        if let subview5 = subview4.subviews.first {
                            return subview5.subviews
                        }
                    }
                }
            }
        }
        return nil
    }
    
    /// 消息提示框
    static func vg_showAlert(message: String, confirmTitle: String,confirmActionTitleColor: UIColor? = nil, in viewController: UIViewController, compeletionClosure: ((UIAlertAction) -> Void)?) {
        let alert: UIAlertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        //        alert.addAction(UIAlertAction(title: confirmTitle, style: .cancel, handler: compeletionClosure))
        let saveAction: UIAlertAction = UIAlertAction(title: confirmTitle, style: .default, handler: compeletionClosure)
        alert.addAction(saveAction)
        if let c = confirmActionTitleColor {
            saveAction.setValue(c, forKey: "titleTextColor")
        }
        viewController.vg_present(alert, animated: true)
    }
    
    /// 带标题 消息提示框
    static func vg_showTitleAlert(title : String,message: String, confirmTitle: String,confirmActionTitleColor: UIColor? = nil, in viewController: UIViewController, compeletionClosure: ((UIAlertAction) -> Void)?) {
        let alert: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        //        alert.addAction(UIAlertAction(title: confirmTitle, style: .cancel, handler: compeletionClosure))
//        alert.messageLabel?.textAlignment = .left
        let saveAction: UIAlertAction = UIAlertAction(title: confirmTitle, style: .default, handler: compeletionClosure)
        alert.addAction(saveAction)
        if let c = confirmActionTitleColor {
            saveAction.setValue(c, forKey: "titleTextColor")
        }
        viewController.vg_present(alert, animated: true)
    }
    
    /// 确认框
    static func vg_showConfirm(title: String, message: String?, actionSureTitle: String, actionCancelTitle: String, in selfVC: UIViewController, compeletionClosure: ((UIAlertAction) -> Void)?) {
        let alertController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let saveAction: UIAlertAction = UIAlertAction(title: actionSureTitle, style: .default, handler: compeletionClosure)
        alertController.addAction(saveAction)
        let cancelAction: UIAlertAction = UIAlertAction(title: actionCancelTitle, style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        selfVC.vg_present(alertController, animated: true, completion: nil)
    }
    
    /// 普通 取消、确认 样式
    static func vg_showNoramlCancelSure(title: String?, message: String, cancelTitle: String, sureTitle: String, titleAttribute:NSMutableAttributedString? = nil, actionCancelTitleColor: UIColor?,sureActionTitleColor: UIColor? = nil, in viewController: UIViewController, cancelClosure: ((UIAlertAction) -> Void)?, sureClosure: ((UIAlertAction) -> Void)?) {
        
        let alertController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        //标题字体样式
        if let t = titleAttribute{
            alertController.setValue(t, forKey: "attributedTitle")
        }

        let cancelAction: UIAlertAction = UIAlertAction(title: cancelTitle, style: UIAlertAction.Style.default, handler: cancelClosure)
        if let c = actionCancelTitleColor {
            cancelAction.setValue(c, forKey: "titleTextColor")
        }
        alertController.addAction(cancelAction)
        
        let sureAction: UIAlertAction = UIAlertAction(title: sureTitle, style: UIAlertAction.Style.default, handler: sureClosure)
        alertController.addAction(sureAction)
        
        if let c = sureActionTitleColor {
            sureAction.setValue(c, forKey: "titleTextColor")
        }
//        alertController.messageLabel?.textAlignment = .left
        
        viewController.vg_present(alertController, animated: true, completion: nil)
    }
    
    /// 取消、确认 样式
    static func vg_showCancelSure(title: String, message: String, picStr: String?, cancelTitle: String?, sureTitle: String, in viewController: UIViewController, cancelClosure: ((UIAlertAction) -> Void)?, sureClosure: ((UIAlertAction) -> Void)?) {
        
        let alertController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
//        alertController.messageLabel?.textAlignment = .left
        if cancelTitle != nil {
            let cancelAction: UIAlertAction = UIAlertAction(title: cancelTitle, style: UIAlertAction.Style.cancel, handler: cancelClosure)
            cancelAction.setValue(UIColor.black, forKey: "titleTextColor")
            alertController.addAction(cancelAction)
        }
        if picStr != nil {
            let imageView: UIImageView = UIImageView(frame: CGRect(x: 10, y: 20, width: 20, height: 20))
            imageView.sy_name("time_ico_bm")
            alertController.view.addSubview(imageView)
        }
        let sureAction: UIAlertAction = UIAlertAction(title: sureTitle, style: UIAlertAction.Style.default, handler: sureClosure)
        alertController.addAction(sureAction)
        viewController.vg_present(alertController, animated: true, completion: nil)
    }
}
