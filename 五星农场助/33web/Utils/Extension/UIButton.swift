//
//  UIButton.swift
//  vgbox
//
//  Created by 宋海胜 on 2019/6/25.
//  Copyright © 2019 Swift Xcode. All rights reserved.
//

import UIKit

enum ButtonImagePosition {
    case top
    case bottom
    case left
    case right
}

class AssociateKeys {
    
    static var bkey : String = "BUTTONKEYS"
//    static var buttonKey = {return Unmanaged<AnyObject>.passUnretained(AssociateKeys.bkey as AnyObject).toOpaque()}()
    
    static var vkey : String = "UIVIEWKEYS"
//    static var viewKey = {return Unmanaged<AnyObject>.passUnretained(AssociateKeys.vkey as AnyObject).toOpaque()}()
}

extension UIButton {
    
    var imageWidth: CGFloat {
        return imageView?.frame.width ?? 0.0
    }
    
    var imageHeight: CGFloat {
        return imageView?.frame.height ?? 0.0
    }
    
    var titleWidth: CGFloat {
        return titleLabel?.intrinsicContentSize.width ?? 0.0
    }
    
    var titleHeight: CGFloat {
        return titleLabel?.intrinsicContentSize.height ?? 0.0
    }
    
    var contentWidth: CGFloat {
        return (imageWidth + titleWidth) / 2.0
    }
    
    var contentHeight: CGFloat {
        return (imageHeight + titleHeight) / 2.0
    }
    
    func imagePosition(_ position: ButtonImagePosition, space: CGFloat) {
        var titleTop: CGFloat = 0.0
        var titleLeft: CGFloat = 0.0
        var titleBottom: CGFloat = 0.0
        var titleRight: CGFloat = 0.0
        var imageTop: CGFloat = 0.0
        var imageLeft: CGFloat = 0.0
        var imageBottom: CGFloat = 0.0
        var imageRight: CGFloat = 0.0
        
        switch position {
        case .top:
            imageTop = -(titleHeight / 2.0 + space / 2.0) // 图片上移半个label高度和半个space高度  给label使用
            imageBottom = (titleHeight / 2.0 + space / 2.0)
            imageLeft = titleWidth / 2.0
            imageRight = -titleWidth / 2.0
            
            titleLeft = -imageWidth / 2.0
            titleRight = imageWidth / 2.0
            titleTop = imageHeight / 2.0 + space / 2.0//文字下移半个image高度和半个space高度
            titleBottom = -(imageHeight / 2.0 + space / 2.0)
        case .bottom:
            imageLeft = (imageWidth + titleWidth) / 2.0 - imageWidth / 2.0
            imageRight = -titleWidth / 2.0
            imageBottom = -(titleHeight / 2.0 + space / 2.0)
            imageTop = titleHeight / 2.0 + space / 2.0 //图片下移半个label高度和半个space高度  给label使用
            
            titleTop = -(imageHeight / 2.0 + space / 2.0)
            titleBottom = imageHeight / 2.0 + space / 2.0
            titleLeft = -imageWidth / 2.0
            titleRight = imageWidth / 2.0
        case .left:
            imageTop = 0
            imageBottom = 0
            imageLeft =  -space / 2.0
            imageRight = space / 2.0
            
            titleTop = 0
            titleBottom = 0
            titleLeft = space / 2
            titleRight = -space / 2
        case .right:
            imageTop = 0;
            imageBottom = 0;
            imageRight = -(titleWidth + space / 2.0);
            imageLeft = titleWidth + space / 2.0;
            
            titleTop = 0;
            titleLeft = -(imageWidth + space / 2.0);
            titleBottom = 0;
            titleRight = imageWidth + space / 2.0;
        }
        
        imageEdgeInsets = UIEdgeInsets(top: imageTop,
                                       left: imageLeft,
                                       bottom: imageBottom,
                                       right: imageRight)
        titleEdgeInsets = UIEdgeInsets(top: titleTop,
                                       left: titleLeft,
                                       bottom: titleBottom,
                                       right: titleRight)
    }
}

///按钮点击
extension UIButton {

    func addAction(_ action : (() -> Void)?){
        // 通过objc_setAssociatedObject将闭包保存
        objc_setAssociatedObject(self, &AssociateKeys.bkey , action, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        addTarget(self, action: #selector(itemClick(button:)), for: .touchUpInside)
    }

    @objc private func itemClick(button: UIButton){
        // 点击按钮，通过objc_getAssociatedObject获取之前保存的闭包并调用
        if let block = objc_getAssociatedObject(self, &AssociateKeys.bkey) as? (()->Void){
            block()
        }
    }
}
