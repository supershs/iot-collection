//
//  SYLoginModel.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/1/27.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit
import HandyJSON

struct SYLoginModel:  HandyJSON {
    /// 1
    var tenantId: Int = 0
    /// ADMIN
    var userType: String?
    /// admin
    var loginName: String?
    ///
    var headImg: String?
    /// 管理员
    var trueName: String?
    /// false
    var locked: Bool = false
    /// 管理员
    var nickName: String?
    /// 1
    var userId: String?
    ///
    var token: String?
    ///
    var sysTenantUserVO: NSSysTenantUserVOModel?
}


struct NSSysTenantUserVOModel : HandyJSON {

    /// 1
    var id: String?
    /// 1
    var sort: Int = 0
    /// ADMIN
    var userType: String?
    /// 12
    var version: Int = 0
    /// <#泛型#>
    var remarks: String?
    /// 1
    var updateBy: String?
    /// 165DAA
    var mainColor: String?
    /// <#泛型#>
    var tenantVO: String?
    /// <#泛型#>
    var createDate: String?
    ///
    var loginTime: String?
    /// 1
    var status: Int = 0
    /// false
    var locked: Bool = false
    /// 1
    var userId: String?
    /// 1
    var tenantId: String?
    ///
    var updateDate: String?
    /// SYS_PASSWORD
    var loginType: String?
    /// <#泛型#>
    var userVO: String?
    /// <#泛型#>
    var uuid: String?
    /// 1
    var createBy: String?

  
}
