//
//  SYLoginView.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2020/8/21.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import SnapKit
import SwiftWeb

class SYLoginView: UIView {

    let minUsernameLength = 1
    let maxUsernameLength = 11
    let minPasswordLength = 1
    let maxPasswordLength = 30
    let disposBag = DisposeBag()
    var loginProtocol: SYLoginProtocol!
    var viewController: UIViewController!
    var isOpen:Bool = true
    
    var usernameTF: UITextField = {
        let tf = UITextField()
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.textColor = .black
        tf.placeholder = "请输入账号"
        return tf
    }()
    var usernameLB: UILabel = {
        let lb = UILabel()
        lb.textColor = UIColor(hex: 0x69707F)
        lb.font = UIFont.systemFont(ofSize: 12)
        lb.text = "账号"
        return lb
    }()
    var passwordTF: UITextField = {
        let tf = UITextField()
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.textColor = .black
        tf.placeholder = "请输入密码"
        return tf
    }()
    var passwordLB: UILabel = {
        let lb = UILabel()
        lb.textColor = UIColor(hex: 0x69707F)
        lb.font = UIFont.systemFont(ofSize: 12)
        lb.text = "密码"
        return lb
    }()    
    var openPassword: UIButton = {
        let bt = UIButton(type: .custom)
        bt.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        bt.setImage(UIImage(named: "offeye"), for: .normal)
        bt.setImage(UIImage(named: "openeye"), for: .selected)
        return bt
    }()
    var forgetPasswordButton: UIButton = {
        let bt = UIButton()
        bt.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        bt.setTitle("忘记密码", for: .normal)
        bt.isHidden = true
        bt.setTitleColor(UIColor(hex: 0x4BCA8A), for: .normal)
        return bt
    }()
    var loginButton: UIButton = {
        let v = UIButton()
        v.backgroundColor = UIColor.white
        v.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        v.setTitleColor(UIColor.init(hex: 0xffffff), for: .normal)
        v.setBackgroundImage(UIImage(named: "login_btn"), for: .normal)
        v.setTitle("登录", for: .normal)
        return v
    }()
    var registerView: UIView = {
        let v = UIView()
        v.backgroundColor = .clear
        v.isUserInteractionEnabled = true
        v.isHidden = true
        
        return v
    }()
    var registerLabel:UILabel = {
        let lb = UILabel()
        lb.textColor = UIColor(hex: 0x1D1E2C)
        lb.font = UIFont.systemFont(ofSize: 14.5)
        lb.text = "还没有帐号?"
        lb.isHidden = true
        return lb
    }()
    var registerButton: UIButton = {
        let bt = UIButton()
        bt.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        bt.setTitle("马上注册", for: .normal)
        bt.setTitleColor(UIColor(hex: 0x4BCA8A), for: .normal)
        bt.isHidden = true
        return bt
    }()
    var delegateButton: UIButton = {
        let bt = UIButton()
        bt.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        bt.setImage(UIImage(named: "select_true"), for: .selected)
        bt.setImage(UIImage(named: "select_false"), for: .normal)
        bt.setTitleColor(UIColor(hex: 0x4BCA8A), for: .normal)
//        bt.isHidden = true
        return bt
    }()
    var loginAgreeView:TGSLoginAgreeView!
    var loginMethodButton: UIButton = {
        let bt = UIButton()
        bt.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        bt.setTitle("手机号验证码登录" , for: .normal)
        bt.setTitleColor(UIColor(hex: 0x666666), for: .normal)
        bt.isHidden = true
        return bt
    }()
    var bgImgView: UIImageView = {
        let v = UIImageView()
        v.sy_name("login_bg")
        return v
    }()
    var logoImgView: UIImageView = {
        let v = UIImageView()
        v.sy_name("loginlog")
        return v
    }()
    var logoLoginImgView: UIImageView = {
        let v = UIImageView()
        v.sy_name("login_login")
        return v
    }()
    var titleLabel:UILabel = {
        let lb = UILabel()
        lb.textColor = UIColor(hex: 0x292929)
        lb.font = UIFont.boldSystemFont(ofSize: 25)
        lb.text = "五星.新建智慧农\n 场管理平台"
        lb.numberOfLines = 2
        return lb
    }()
    var accountImgView: UIImageView = {
        let v = UIImageView()
        v.sy_name("login_input")
        return v
    }()
    var passwordImgView: UIImageView = {
        let v = UIImageView()
        v.sy_name("login_input")
        return v
    }()
    var accountSepView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.init(hex: 0xCBFFEC)
        return v
    }()
    var passwordSepView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.init(hex: 0xCBFFEC)
        return v
    }()
    var thirdLoginLB: UILabel = {
        let lb = UILabel()
        lb.textColor = .white
        lb.font = UIFont.systemFont(ofSize: 13)
        lb.text = "----   第三方登录   ----"
        lb.isHidden = true
        return lb
    }()
    var thirdLoginBtn: UIButton = {
        let v = UIButton()
        v.setImage(UIImage(named: "login_weixin"), for: .normal)
        v.isHidden = true
        return v
    }()
    var backBtn: UIButton = {
        let v = UIButton()
        v.setImage(UIImage(named: "nav_back"), for: .normal)
        return v
    }()
    init(frame: CGRect, loginProtocol: SYLoginProtocol, viewController: UIViewController) {
        super.init(frame: frame)
        self.loginProtocol = loginProtocol
        self.viewController = viewController
        initViews()
        events()
        plugs()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initViews() {
        loginAgreeView = TGSLoginAgreeView(frame: .zero)
//        loginAgreeView.isHidden = true
        loginAgreeView.clickHandle = {[weak self] clickType in
            if let `self` = self {
               print(clickType,"clickTypeclickTypeclickType")
                self.viewController.sg_present(HSWebViewController(path: kWebUrl + USERAGREEMENT), animated: true, completion: nil)
            }
        }
        logoImgView = Util.getAppIconName()
        passwordTF.isSecureTextEntry = isOpen
        self.addSubview(bgImgView)
        self.addSubview(forgetPasswordButton)
        self.addSubview(loginButton)
        self.addSubview(registerView)
        registerView.addSubview(registerLabel)
        registerView.addSubview(registerButton)
        self.addSubview(logoLoginImgView)
        self.addSubview(logoImgView)
        self.addSubview(titleLabel)
        self.addSubview(accountImgView)
        self.addSubview(passwordImgView)
        self.addSubview(thirdLoginLB)
        self.addSubview(accountSepView)
        self.addSubview(passwordSepView)
        self.addSubview(loginAgreeView)
        self.addSubview(delegateButton)
        self.addSubview(loginMethodButton)
        self.addSubview(usernameTF)
        self.addSubview(usernameLB)
        self.addSubview(passwordTF)
        self.addSubview(passwordLB)
        self.addSubview(openPassword)
        self.addSubview(thirdLoginLB)
        self.addSubview(thirdLoginBtn)
        
        bgImgView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        logoLoginImgView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(STATUSBAR_HEIGHT + 107)
            make.left.equalToSuperview()
        }
        logoImgView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(STATUSBAR_HEIGHT + 137)
            make.left.equalToSuperview().offset(autoWidth(15))
            make.width.equalTo(55)
            make.height.equalTo(49)
        }
        titleLabel.snp.makeConstraints { make in
            make.centerY.equalTo(logoImgView.snp.centerY)
            make.left.equalTo(logoImgView.snp.right).offset(5)
        }
        accountImgView.snp.makeConstraints { (make) in
            make.top.equalTo(self.logoImgView.snp.bottom).offset(64)
            make.left.equalToSuperview().offset(26)
            make.right.equalToSuperview().offset(-26)
            make.height.equalTo(54)
        }
        passwordImgView.snp.makeConstraints { (make) in
            make.top.equalTo(self.accountImgView.snp.bottom).offset(20)
            make.left.equalToSuperview().offset(26)
            make.right.equalToSuperview().offset(-26)
            make.height.equalTo(54)
        }
        usernameTF.snp.makeConstraints { (make) in
            make.left.equalTo(self.accountImgView.snp.left).offset(17)
            make.right.equalToSuperview().offset(-25)
            make.height.equalTo(25)
            make.bottom.equalTo(self.accountImgView.snp.bottom)
        }
        usernameLB.snp.makeConstraints { (make) in
            make.top.equalTo(accountImgView.snp.top)
            make.left.equalTo(self.accountImgView.snp.left).offset(17)
            make.right.equalToSuperview().offset(-25)
            make.height.equalTo(25)
        }
        accountSepView.snp.makeConstraints { (make) in
            make.top.equalTo(self.usernameTF.snp.bottom)
            make.left.right.equalTo(self.usernameTF)
            make.height.equalTo(LINE_HEIGHT)
        }
        passwordTF.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.passwordImgView.snp.bottom)
            make.left.equalTo(passwordImgView.snp.left).offset(17)
            make.right.equalToSuperview().offset(-50)
            make.height.equalTo(25)
        }
        passwordLB.snp.makeConstraints { (make) in
            make.top.equalTo(passwordImgView.snp.top)
            make.left.equalTo(passwordImgView.snp.left).offset(17)
            make.right.equalToSuperview().offset(-25)
            make.height.equalTo(25)
        }
        openPassword.snp.makeConstraints { make in
            make.right.equalToSuperview().offset(-40)
            make.width.equalTo(25)
            make.height.equalTo(25)
            make.centerY.equalTo(passwordTF.snp.centerY)
        }
        passwordSepView.snp.makeConstraints { (make) in
            make.top.equalTo(passwordTF.snp.bottom)
            make.left.right.equalTo(passwordTF)
            make.height.equalTo(LINE_HEIGHT)
        }
        forgetPasswordButton.snp.makeConstraints { make in
            make.top.equalTo(passwordTF.snp.bottom).offset(10.autoWidth())
            make.right.equalTo(passwordImgView)
            make.height.equalTo(14.autoWidth())
        }
        loginButton.snp.makeConstraints { (make) in
            make.top.equalTo(passwordSepView.snp.bottom).offset(53)
            make.left.equalToSuperview().offset(33)
            make.right.equalToSuperview().offset(-33)
            make.height.equalTo(70)
        }
        registerView.snp.makeConstraints { make in
            make.top.equalTo(loginButton.snp.bottom).offset(21)
            make.centerX.equalToSuperview()
            make.height.equalTo(14.autoWidth())
        }
        registerLabel.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.left.equalToSuperview()
        }
        registerButton.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.left.equalTo(registerLabel.snp.right)
            make.right.equalToSuperview()
        }
       
        loginAgreeView.snp.makeConstraints { (make ) in
            make.top.equalTo(loginButton.snp.bottom).offset(10)
            make.centerX.equalToSuperview()
        }
        delegateButton.snp.makeConstraints { make in
            make.right.equalTo(loginAgreeView.snp.left).offset(-2)
            make.width.height.equalTo(15)
            make.centerY.equalTo(loginAgreeView.snp.centerY)
        }
        loginMethodButton.snp.makeConstraints { (make) in
            make.top.equalTo(loginAgreeView.snp.bottom).offset(10)
            make.centerX.equalToSuperview()
            make.width.equalTo(150)
            make.height.equalTo(25)
        }
        thirdLoginLB.snp.makeConstraints { (make) in
            make.bottom.equalTo(thirdLoginBtn.snp.top).offset(-30)
            make.centerX.equalToSuperview()
            make.height.equalTo(15)
        }
        thirdLoginBtn.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-33)
            make.centerX.equalToSuperview()
            make.width.height.equalTo(39)
        }
    }
    
    func events() {
        /*
         *  .map负责对UITextField中的字符进行处理，判断字符长度，是否符合要求，将判断的值返回给usernameValid和passwordValid
         *
         *  shareReplay()是RxSwift提供的一个流操作函数，它是以重播的方式通知自己的订阅者，保证在观察者订阅这个流的时候始终都能回播最后N个，shareReplay(1)表示重播最后一个。
         *  shareReplay 会返回一个新的事件序列，它监听底层序列的事件，并且通知自己的订阅者们。 解决有多个订阅者的情况下，map会被执行多次的问题。
         */
        let usernameValid = usernameTF.rx.text
            .map { $0!.count >= self.minUsernameLength && $0!.count <= self.maxUsernameLength }
            .share(replay: 1)
        
        let passwordValid = passwordTF.rx.text
            .map { $0!.count >= self.minPasswordLength && $0!.count < self.maxPasswordLength }
            .share(replay: 1)
        
        let everythingValid = Observable.combineLatest(usernameValid, passwordValid) { (usernameValid, passwordValid) -> Bool in
            usernameValid && passwordValid
        }
        
        // 或者
        //        let everythingValid = Observable.combineLatest(usernameValid, passwordValid) { $0 && $1 }
        //        .shareReplay(1)
        
        /*  绑定
         *  将usernameValid和passwordTF.rx_enabled绑定，即用usernameValid来控制passwordTF是否可以输入的状态
         *  bindTo就是RxSwfit中用来进行值绑定的函数
         */
        usernameValid
            .bind(to: passwordTF.rx.isEnabled) //username通过验证，passwordTF才可以输入
            .disposed(by: disposBag)
        delegateButton.rx.tap //绑定button点击事件
            .subscribe(onNext: { [weak self] in
                if let `self` = self {
                    self.delegateButton.isSelected = !self.delegateButton.isSelected
                }
            })
            .disposed(by: disposBag)
        
        everythingValid
            .bind(to: loginButton.rx.isEnabled,openPassword.rx.isEnabled) // 用户名密码都通过验证，才可以点击按钮
            .disposed(by: disposBag)
        //点击登录
        loginButton.rx.tap //绑定button点击事件
            .subscribe(onNext: { [weak self] in
                if let `self` = self {
                    if (self.delegateButton.isSelected == false){
                        self.showMessage("请先勾选用户协议和隐私条款")
                    }else{
                        self.loginProtocol.login(loginName: self.usernameTF.text ?? "", password: self.passwordTF.text ?? "")
                    }
                }
            })
            .disposed(by: disposBag)
        //注册
        registerButton.rx.tap
            .subscribe(onNext: { [weak self] in
                self?.viewController.navigationController?.pushViewController(SYForgetViewController(typeTitle: "注册"), animated: true)
            })
            .disposed(by: disposBag)
        //显示与关闭密码
        openPassword.rx.tap.subscribe(onNext: { [weak self] in
            if let `self` = self {
                self.openPassword.isSelected = self.isOpen
                self.isOpen = !self.isOpen
                self.passwordTF.isSecureTextEntry = self.isOpen
            }
        })
        .disposed(by: disposBag)
        //忘记密码
        forgetPasswordButton.rx.tap.subscribe (onNext: { [weak self] in
            let vc = SYForgetViewController(typeTitle: "忘记密码")
            self?.viewController.navigationController?.pushViewController(vc, animated: true)
        }).disposed(by: disposBag)
        //手机号验证码登录或账号密码登录
        loginMethodButton.rx.tap
            .subscribe(onNext: { [weak self] in
                if self!.loginMethodButton.titleLabel?.text == "手机号验证码登录" {
                    let vc = SYVerifiCodeLoginViewController()
                    self?.viewController.navigationController?.pushViewController(vc, animated: true)
                }else{
                    self?.viewController.sy_popVC()
                }
            })
            .disposed(by: disposBag)
        //三方登录
        thirdLoginBtn.rx.tap
            .subscribe(onNext: { [weak self] in
                self?.loginProtocol.thirdLogin()
            })
            .disposed(by: disposBag)
    }
    
    //根据类型做判断
    func plugs() {
        
        switch loginProtocol.loginType {
        case .account:
            log.info("account")
        case .verifiCode:
            back()
            self.loginMethodButton.setTitle("账号密码登录", for: .normal)
            verifiCodeViews()
        case .register:
            back()
        }
    }
    
    func back() {

        self.addSubview(backBtn)
        backBtn.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(STATUSBAR_HEIGHT + 20)
            make.left.equalToSuperview().offset(30)
        }
        backBtn.rx.tap
            .subscribe(onNext: { [weak self] in
                self?.viewController.navigationController?.popViewController(animated: true)
            })
            .disposed(by: disposBag)
        registerButton.isHidden = true
        forgetPasswordButton.isHidden = true
        registerLabel.isHidden = true
        thirdLoginBtn.isHidden = true
        thirdLoginLB.isHidden = true
    }
    
    // 显示消息提示框
      func showMessage(_ text: String) {
          let alertController = UIAlertController(title: text, message: nil, preferredStyle: .alert)
          let cancelAction = UIAlertAction(title: "确定", style: .cancel, handler: nil)
          alertController.addAction(cancelAction)
          self.viewController.present(alertController, animated: true, completion: nil)
      }
}

