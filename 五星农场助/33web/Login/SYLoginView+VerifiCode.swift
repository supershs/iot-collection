//
//  SYLoginView+VerifiCode.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2020/8/21.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit

extension SYLoginView {
    
    
    func verifiCodeViews() {
        
        let getVerifiCodeBtn: UIButton = {
            let bt = UIButton()
            bt.titleLabel?.font = UIFont.systemFont(ofSize: 13)
            bt.setTitle("获取验证码", for: .normal)
            bt.setTitleColor(UIColor(hex: 0x4BCA8A), for: .normal)
            return bt
        }()
        passwordImgView.isUserInteractionEnabled = true
        passwordImgView.addSubview(getVerifiCodeBtn)
        openPassword.isHidden = true
        getVerifiCodeBtn.snp.makeConstraints { (make) in
            make.centerY.equalTo(passwordImgView)
            make.height.equalTo(30)
            make.right.equalTo(passwordTF).offset(-10)
            make.width.equalTo(85)
        }
        
        getVerifiCodeBtn.rx.tap
            .subscribe(onNext: { [weak self] in
                let vc = self?.viewController as! SYVerifiCodeLoginViewController
                vc.getVerifiCode { bool in
                    if bool == true {
                        HUDUtil.hideHud()

                        Util.dispatchTimer(timeInterval: 1, repeatCount: 60) { timer, index in
                            getVerifiCodeBtn.isUserInteractionEnabled = false
                            getVerifiCodeBtn.setTitle("重新发送\(index)s", for: .normal)
                            if index == 0  {
                                getVerifiCodeBtn.setTitle("获取验证码", for: .normal)
                                getVerifiCodeBtn.isUserInteractionEnabled = true
                            }
                        }
                    }
                }
            })
            .disposed(by: disposBag)
        
        usernameTF.placeholder = "请输入手机号码"
        passwordTF.placeholder = "请输入验证码"
        passwordTF.isSecureTextEntry = false
        
    }
}
