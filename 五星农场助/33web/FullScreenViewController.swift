////
////  FullScreenViewController.swift
////  33web
////
////  Created by 叁拾叁 on 2023/1/11.
////
//
//import UIKit
//import SwiftWeb
//
//class FullScreenViewController: SYBaseViewController {
//
//    internal var liveView = ZGLiveView()
//    public var canAllButUpsideDown = false
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        // 萤石云视频全屏
//        liveView.currentVC = self
//        liveView.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
//
//        // Do any additional setup after loading the view.
//    }
//    func setVideoPage(_ params: Dictionary<String, Any>) {
//        if liveView.player != nil {
//            self.liveView.fullScreenAction()
//            return
//        }
//        Tool.requestAuthorizationPhotoLibary()
//        Tool.requestMicroPhoneAuth()
//        self.canAllButUpsideDown = true
//        self.liveView.configureSDK(params)
//        self.liveView.fullScreenAction()
//    }
//
//    open override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
//
//        if size.width > size.height {// 横屏
//            liveView.blanner!.frame = CGRect(x: 0, y: 0, width: kScreenHeight, height: kScreenWidth)
//            liveView.showView(isFullScreen: true)
//        } else {
//            liveView.blanner!.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenWidth*9/16)
//            liveView.hideAboveViews()
//            self.view.hideToastActivity()
//            liveView.showView(isFullScreen: false)
//        }
//    }
//
//    /*
//    // MARK: - Navigation
//
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        // Get the new view controller using segue.destination.
//        // Pass the selected object to the new view controller.
//    }
//    */
//
//}
