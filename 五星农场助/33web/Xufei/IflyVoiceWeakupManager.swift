//
//  IflyVoiceWeakupManager.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/6/11.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit

let IflyVoiceWeakup = IflyVoiceWeakupManager.sharedInstance

class IflyVoiceWeakupManager: NSObject, IFlyVoiceWakeuperDelegate {
    
    class var sharedInstance : IflyVoiceWeakupManager {
        struct Static {
            static let instance : IflyVoiceWeakupManager = IflyVoiceWeakupManager()
        }
        return Static.instance
    }
    
    var iflyVoiceWakeuper: IFlyVoiceWakeuper?
    var currentVC: UIViewController!
    // ******************************************** 语音识别 start ***********************************************
    func wakeUp(_ currentVC: UIViewController) {
        self.currentVC = currentVC
        
        iflyVoiceWakeuper = IFlyVoiceWakeuper.sharedInstance()
        iflyVoiceWakeuper?.delegate=self;
        iflyVoiceWakeuper?.setParameter("0:1450", forKey: IFlySpeechConstant.ivw_THRESHOLD())
        iflyVoiceWakeuper?.setParameter("wakeup", forKey: IFlySpeechConstant.ivw_SST())
        let path = "\(Bundle.main.resourcePath!)/ivw/\(APPID_VALUE).jet"
        print(path,"------我是wakeup地址")
        
        let ivwResourcePath = IFlyResourceUtil.generateResourcePath(path)
        iflyVoiceWakeuper?.setParameter(ivwResourcePath, forKey: "ivw_res_path")
        iflyVoiceWakeuper?.setParameter("1", forKey: IFlySpeechConstant.keep_ALIVE())
        iflyVoiceWakeuper?.setParameter(IFLY_AUDIO_SOURCE_MIC, forKey: "audio_source")
        iflyVoiceWakeuper?.setParameter("ivw.pcm", forKey: "ivw_audio_path")
    }
    
    func onBeginOfSpeech() {
        print("开始录音")
    }
    
    func onEndOfSpeech() {
        print("结束录音")
    }
    func onVolumeChanged(_ volume: Int32) {
//                print("volume===",volume)
    }
    
    func onCompleted(_ error: IFlySpeechError!) {
        print(error.errorCode,error.errorDesc ?? "ee")
    }
    
    // 模态出半透明视图
    func onResult(_ resultDic: NSMutableDictionary!) {
        
        resultDic.object(forKey: "sst")
        //        print(resultDic);
        let score = resultDic.object(forKey: "score") as! NSInteger
        if score > 1450 {
            self.iflyVoiceWakeuper?.stopListening()
            iflyVoiceWakeuper?.setParameter("", forKey: IFlySpeechConstant.params())
            iflyVoiceWakeuper?.delegate=nil
            iflyVoiceWakeuper?.cancel()
            let avview = AVViewController()
            avview.view.backgroundColor=UIColor(red:0, green:0, blue:0, alpha:0)
            avview.modalPresentationStyle = UIModalPresentationStyle.custom
            avview.restartWakeuper = {
                self.iflyVoiceWakeuper?.startListening()
                self.iflyVoiceWakeuper?.delegate = self
            }
            currentVC.present(avview, animated: true, completion: nil)
//            currentVC?.presentationController(avview)
//            currentVC.navigationController?.presentationController(avview)
//            currentVC.sg_present(avview, animated: true)
        }
    }
    // ******************************************** 语音识别 end ***********************************************
}
