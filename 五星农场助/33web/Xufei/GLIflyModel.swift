//
//  GLIflyModel.swift
//  GuangLingWuLian
//
//  Created by 叁拾叁 on 2020/7/28.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit

class GLBaseModel: Codable {
    
    var status: String?
    var code: Int?
    var date: Int?
    var message: String?
    var data: [GLIflyModel]?
}

class GLIflyModel: Codable {
    
    var controlValue: String?
    var deviceId: String?
    var deviceName: String?
    var sensorId: String?
    var sensorName: String?
    var sort: Int?
}
