//
//  AVViewController.swift
//  GuangLingWuLian
//
//  Created by 杨琴 on 2020/7/20.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit
import SnapKit
import Alamofire
import SwiftWeb
import FLAnimatedImage

class AVViewController: UIViewController{
	
	// MARK: - property
	var animationImg: FLAnimatedImageView = {
		let v = FLAnimatedImageView()
		v.backgroundColor = UIColor .red
		v.layer.cornerRadius = 10
		v.layer.masksToBounds = true
		v.isUserInteractionEnabled = true
		return v
	}()
	var labIdentify: UILabel = {
		let v = UILabel()
		v.text = "在呢！"
		v.textColor = .white
		v.font = UIFont.systemFont(ofSize: 24)
		return v
	}()
	var labLoding: UILabel = {
		let v = UILabel()
		v.text = "加载中..."
		v.numberOfLines = 0
		v.textColor = .white
		v.font = UIFont.systemFont(ofSize: 17)
		return v
	}()
	var typeNumber: NSInteger  = 0
	var btnExit :UIButton = {
		let v = UIButton()
		//        v.setImage(UIImage.init(named: "yuyin_close"), for:.normal)
		return v
	}()
	
	var jiqi: UIImageView = {
		let v = UIImageView()
		v.image = UIImage(named: "yuyin_jiqi")
		return v
	}()
	
	var close: UIImageView = {
		let v = UIImageView()
		v.image = UIImage(named:"yuyin_close")
		return v
	}()
	
	var restartWakeuper :  (() -> Void)?
	var tableView: UITableView?
	var dataModel: GLBaseModel!
	var listData: [Any] = []
	
	// MARK: - init
	override func viewDidLoad() {
		super.viewDidLoad()
		self.modalPresentationStyle = .overCurrentContext

		self.view.backgroundColor = UIColor.white
		NotificationCenter.default.addObserver(self, selector: #selector(test), name: NSNotification.Name(rawValue:"contentSpeech"), object: nil)
		self.setUpUI()
		SynthesizerModel.shared.startSpeak(speak: "在呢！")
		SynthesizerModel.shared.resultClosure = { (iFlySpeechError) in
			//语音合成成功后再根据接口，去识别语音
			if iFlySpeechError.errorCode == 0 {
				self.startIFSpeechRecognition()
                
			}
		}
	}
	//	实现通知监听方法
	@objc func test(nofi : Notification){
		let str :String  = nofi.userInfo!["contentSpeech"] as! String
		self.labIdentify.text = str
		SynthesizerModel.shared.startSpeak(speak: str )
		SynthesizerModel.shared.resultClosure = { (iFlySpeechError) in
			//语音合成成功后再根据接口，去识别语音
			if iFlySpeechError.errorCode == 0 {
                self.disExit()//在页面消失前一定要注销合成识别，否则唤醒不了
				self.dismissViewController()
			}
		}
		print(String(describing: str) + "this notifi")
	}
	//	最后要记得移除通知
	deinit {
		/// 移除通知
		NotificationCenter.default.removeObserver(self)
	}
	// MARK: - private
	func setUpUI() {//语音识别ui
		
		let path: URL = Bundle.main.url(forResource: "001", withExtension: "gif")!
		do {
			let gifData: Data = try Data.init(contentsOf: path)
			let animatedImg: FLAnimatedImage =  FLAnimatedImage.init(gifData: gifData)
			animationImg.animatedImage = animatedImg
		} catch {
			print(error)
		}
		
		self.view.addSubview(animationImg)
		animationImg.addSubview(jiqi)
		animationImg.addSubview(close)
		animationImg.addSubview(labLoding)
		animationImg.addSubview(labIdentify)
		
		btnExit.addTarget(self, action: #selector(disExit), for: .touchUpInside)
		animationImg.addSubview(btnExit)
		
		tableView = UITableView()
		tableView?.delegate = self
		tableView?.dataSource = self
		tableView?.backgroundColor = UIColor.clear
		tableView?.register(UITableViewCell.self, forCellReuseIdentifier: "tableCell")
		self.view .addSubview(tableView!)
		
		animationImg.snp.makeConstraints({ (make) in
			make.bottom.equalToSuperview().offset(-kBottomSafeHeight)
			make.left.equalToSuperview().offset(10)
			make.right.equalToSuperview().offset(-10)
			make.height.equalTo(200)
		})
		
		jiqi.snp.makeConstraints { (make) in
			make.top.equalToSuperview().offset(10)
			make.left.equalToSuperview().offset(10)
		}
		
		btnExit.snp.makeConstraints { (make) in
			make.top.equalToSuperview()
			make.right.equalToSuperview()
			make.height.equalTo(50)
			make.width.equalTo(50)
		}
		
		close.snp.makeConstraints { (make) in
			make.top.equalToSuperview().offset(10)
			make.right.equalToSuperview().offset(-10)
		}
		
		labLoding.snp.makeConstraints({ (make) in
			make.top.equalToSuperview().offset(10)
			make.left.equalToSuperview().offset(40)
			make.right.equalToSuperview().offset(-20)
		})
		
		labIdentify.snp.makeConstraints({ (make) in
			make.centerX.equalTo(animationImg).offset(0)
			make.centerY.equalTo(animationImg).offset(-20)
		})
		
		tableView?.snp.makeConstraints({ (make) in
			make.top.equalTo(self.animationImg.snp.bottom)
			make.leading.equalToSuperview()
			make.trailing.equalToSuperview()
			make.bottom.equalToSuperview()
		})
	}
	
	//语音识别唤起
	func startIFSpeechRecognition() {
		
		ASRConfig.shared.starIdentify()
		ASRConfig.shared.resultClosure = {[weak self] (type, error, res) in
			
			print("strResutl: \(type) %\(String(describing: error)) res:\(res)")
			if let weakSelf = self {
				
				switch type {
					
				case .IFTypeCancel:
					print(":::::::语音识别取消:::::::");
					weakSelf.labLoding.text = "语音识别取消了"
					break;
					
				case .IFTypeiniting:
					print(", 语音正在识别中...");
					weakSelf.labLoding.text = "语音正在识别中..."
					break;
					
				case .IFTypeStart:
					print("%s, 语音识别z开始");
					weakSelf.labLoding.text = "语音识别开始"
					
					break;
					
				case .IFTypeStop:
					print(" 语音识别结束");
					weakSelf.labLoding.text = "语音结束"
					break;
				case .IFTypeEnd:
					do {
						weakSelf.labLoding.text = "语音识别完成。"
						
						if res.contains("退出") {
							self?.disExit()
						}else{
							weakSelf.labIdentify.text = res
							// 如果是空字符串, 自动提示指令语音
							if (weakSelf.YQStringIsEmpty(value: res as AnyObject)) {
								
								weakSelf.typeNumber+=1;
								// 检测回调次数，提示大于3次则自动退出
								if (weakSelf.typeNumber > 2) {
									self?.disExit();
									return;
									
								}else{
									let notice: String = "您好像没有说话"
									weakSelf.labIdentify.text = notice
									SynthesizerModel.shared.startSpeak(speak: notice)
								}
							} else {
								//流程: 识别出结果， 开始请求接口， 并重新计数
								weakSelf.typeNumber = 0;    // 重新计数
								weakSelf.labIdentify.text=res;  // 回显识别的语音文字
								
								NotificationCenter.default.post(name: NSNotification.Name("IdentifyContent"), object: self, userInfo: ["content":res])
								
								//								if let m = weakSelf.dataModel, let arr = m.data, arr.count > 0 {
								//									var hasTargetShebei: Bool = false
								//									for (_,value) in arr.enumerated() {
								//										let targetStr: String = res.sy_getNumberFromStr() ?? ""
								//										let targetNum: Int = Int(targetStr) ?? 0
								//										if (value.sort ?? 0) + 1 == targetNum {
								//											weakSelf.controlShebei(sortContent: [
								//												"sensorId": value.sensorId ?? "",
								//												"deviceId": value.deviceId ?? "",
								//												"controlValue": value.controlValue ?? "",
								//												"sensorName": value.sensorName ?? "",
								//												"sort": "\(value.sort ?? 0)",
								//												"deviceName": value.deviceName ?? ""
								//											])
								//											hasTargetShebei = true
								//										}
								//									}
								//									if !hasTargetShebei {
								//										SynthesizerModel.shared.startSpeak(speak: "请说出正确的设备")
								//									}
								//								} else {
								//									weakSelf.getData(textContent: res);   // 请求接口
								//								}
							}
						}
					}
					break;
				}
			}
		}
	}
	
	func getData(textContent: String?){
		
		if (!self.YQStringIsEmpty(value: textContent as AnyObject)) {
			
			let urlStr = "http://47.101.158.124:19999/api/1.0/voice/controlByVoice"
			let header: HTTPHeaders = HTTPHeaders(["appId": "iot_91c8e4da23ee44b698778748426d2ee8","type":"app"])
			let parameters = ["voiceContent":textContent]
			
            
			let _ = AF.request(urlStr, method: .post, parameters: parameters, encoder: JSONParameterEncoder.default, headers: header, interceptor: nil, requestModifier: nil).responseJSON {[weak self] (data) in
				if let weakSelf = self {
					
					let decoder = JSONDecoder()
					// 蛇形命名转驼峰
					decoder.keyDecodingStrategy = .convertFromSnakeCase
					// 日期解析使用 UNIX 时间戳
					decoder.dateDecodingStrategy = .secondsSince1970
					do {
						let model: GLBaseModel = try decoder.decode(GLBaseModel.self, from: data.data!)
						if model.status == "ERROR" {
							weakSelf.labLoding.text = model.message
							SynthesizerModel.shared.startSpeak(speak: model.message ?? "")
							if model.code == 30000 {
								weakSelf.dataModel = model
							}
						} else {
							SynthesizerModel.shared.startSpeak(speak: model.message ?? "")
							weakSelf.disExit();/// 如果是成功直接退出
						}
						weakSelf.tableView?.reloadData()
						
						print("response:\(data)")
					} catch {
						print(error)
					}
				}
			}
		}
	}
	
	func controlShebei(sortContent: [String: String]?){
		
		let urlStr = "http://47.101.158.124:19999/api/1.0/voice/controlBySensor"
		let header: HTTPHeaders = HTTPHeaders(["appId": "iot_91c8e4da23ee44b698778748426d2ee8","type":"app"])
		
		let _ = AF.request(urlStr, method: .post, parameters: sortContent, encoder: JSONParameterEncoder.default, headers: header, interceptor: nil, requestModifier: nil).responseJSON {[weak self] (data) in
			if let weakSelf = self {
				
				let decoder = JSONDecoder()
				// 蛇形命名转驼峰
				decoder.keyDecodingStrategy = .convertFromSnakeCase
				// 日期解析使用 UNIX 时间戳
				decoder.dateDecodingStrategy = .secondsSince1970
				do {
					let model: GLBaseModel = try decoder.decode(GLBaseModel.self, from: data.data!)
					if model.status == "ERROR" {
						weakSelf.labLoding.text = model.message
						var msg = model.message ?? "未知消息"
						if model.message == "会话不存在" {
							msg = "设备没有连接或没通电"
						}
						SynthesizerModel.shared.startSpeak(speak: msg)
						
					} else {
						SynthesizerModel.shared.startSpeak(speak: model.message ?? "未知消息")
						weakSelf.disExit();/// 如果是成功直接退出
					}
					
					print("response:\(data)")
					
				} catch {
					print(error)
				}
			}
		}
		
		
	}
	
	
	//value 是AnyObject类型是因为有可能所传的值不是String类型，有可能是其他任意的类型。
	func YQStringIsEmpty(value: AnyObject?) -> Bool {
		
		//首先判断是否为nil
		if (nil == value) {
			//对象是nil，直接认为是空串
			return true
		}else{
			//然后是否可以转化为String
			if let myValue  = value as? String{
				//然后对String做判断
				return myValue == "" || myValue == "(null)" || 0 == myValue.count
			}else{
				//字符串都不是，直接认为是空串
				return true
			}
		}
	}
	
	// MARK: - event
	@objc func disExit(){
		
		ASRConfig.shared.stopIdentify()
		SynthesizerModel.shared.stopSpeak()
		if let w = restartWakeuper {
			w()
		}
		self.dismiss(animated: true, completion: nil)
	}
}
// MARK: - UITableViewDelegate
extension AVViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath) as UITableViewCell?
		cell?.backgroundColor = UIColor.white
		let model:GLIflyModel = (self.dataModel.data![indexPath.row])
		cell?.textLabel?.text = "\(model.sensorName!)   \(model.sort! + 1)"
		cell?.textLabel?.textColor = UIColor.black
		cell?.selectionStyle = .none
		return cell!
	}
}

extension AVViewController: UITableViewDataSource {
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if let m = self.dataModel, let d = m.data {
			return d.count
		}
		return 0
	}
	
	func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
		return UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 0.01))
	}
	
	func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
		return 0.01
	}
}
