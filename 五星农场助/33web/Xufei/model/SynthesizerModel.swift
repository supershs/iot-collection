//
//  SynthesizerModel.swift
//  GuangLingWuLian
//
//  Created by 杨琴 on 2020/7/20.
//  Copyright © 2020 叁拾叁. All rights reserved.
//语音合成类

import UIKit


class SynthesizerModel: NSObject, IFlySpeechSynthesizerDelegate {
    var resultClosure :  ((IFlySpeechError) -> Void)?
    
    var iFlySpeechSynthesizer : IFlySpeechSynthesizer?
    
    static let shared = SynthesizerModel()
    
    override init() {
        super.init()
        self.initaceSynthesizer()
        
    }
    
    func initaceSynthesizer()  {
        
        //获取语音合成单例
        iFlySpeechSynthesizer = IFlySpeechSynthesizer.sharedInstance()
        //          //设置协议委托对象
        iFlySpeechSynthesizer?.delegate = self
        //          //设置在线工作方式
        iFlySpeechSynthesizer?.setParameter(IFlySpeechConstant.type_CLOUD(), forKey: IFlySpeechConstant.engine_TYPE())
        //          //设置音量，取值范围 0~100
        iFlySpeechSynthesizer?.setParameter("50", forKey: IFlySpeechConstant.volume())
        //          //发音人，默认为”xiaoyan”，可以设置的参数列表可参考“合成发音人列表”
        iFlySpeechSynthesizer?.setParameter("xiaoyan", forKey: IFlySpeechConstant.voice_NAME())
        //          //保存合成文件名，如不再需要，设置为nil或者为空表示取消，默认目录位于library/cache下
        iFlySpeechSynthesizer?.setParameter("tts.pcm", forKey: IFlySpeechConstant.tts_AUDIO_PATH())
        //编码格式
        iFlySpeechSynthesizer?.setParameter("unicode", forKey: IFlySpeechConstant.text_ENCODING())
        
        
    }
    //开始语音合成
    func startSpeak(speak:String){
        
        iFlySpeechSynthesizer?.startSpeaking(speak)
        
    }
    //结束语音合成
    func stopSpeak() {
        
        iFlySpeechSynthesizer?.stopSpeaking()
        
    }
    
    //合成结束
    func onCompleted(_ error: IFlySpeechError!) {
         
        if let re = resultClosure {
                 re(error)
             }
        
        print(error.errorDesc ?? error.errorCode);
        
        
         
     }
    
     
}
