//
//  Constant_API.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2020/8/11.
//  Copyright © 2020 叁拾叁. All rights reserved.  https://xsxyyc.jsnj33.com:9087/api/1.0/
// https://xsxyyc.jsnj33.com:9087/api/1.0/auth/app/login
// https://xsxyyc.jsnj33.com:9087/api/1.0/auth/login
// https://xsxyyc.jsnj33.com:9087/api/1.0/auth/app/login   tenantUrl loginType


import UIKit

let VERSION = "/api/1.0/"

//let kWebUrl = "http://192.168.0.197:8080/#/"
let kWebUrl = "http://114.115.204.235:9086/#/"
//var IP = "http://114.115.204.235:9000"
var IP = "https://xsxyyc.jsnj33.com:9087"

var IMGIP = IP + VERSION
var sysTenantDTOURL = "http://192.168.0.188:9002"
let testMP3 = "http://downsc.chinaz.net/Files/DownLoad/sound1/201906/11582.mp3"
let testMP4 = "http://static.tripbe.com/videofiles/20121214/9533522808.f4v.mp4"
let REQUEST_DIC = "requestDictionary"
let GOTOHOME = "pages/index/iotManage"
//注册协议
 let USERAGREEMENT = "pages/base-login/login"
//let UGOTOHOME = "pages/enterprise/index" 
// 登录
let LOGIN = "auth/app/login"

//忘记
//忘记密码获取验证码
let CODE = "AppLogin/message/push"
//忘记密码确认修改
let FORGETPASSWORD = "AppLogin/app/changePassword"
//注册获取验证吗
let  REGISTERCODE = "AppLogin/message/push"
//注册
let REGIStER = "AppLogin/app/register"
let CODELOGIN = "SysLogin/message/login"
let USEREDITPERMISSION = "TraceSubject/pc/userEditPermission"

// 身份识别
let SHENFEN_RECOGNIZE = "SysUser/app/certification"

// MARK: - 病虫害
// 病害图像识别接口
let SHIBIE_BING = "DiseaseDiscernLog/sc/detect/plant"

// 虫害图像识别接口
let SHIBIE_CHONG = "DiseaseDiscernLog/sc/detect/pest"

// 病虫害查询我的识别历史
let SHIBIE_LISHI = "DiseaseDiscernLog/app/myself"
