//
//  TSUserManager.swift
//  TSWeChat
//
//  Created by Hilen on 11/6/15.
//  Copyright © 2015 Hilen. All rights reserved.
//

import UIKit
import Foundation
import KeychainAccess
//import SwiftyJSON
//import TimedSilver

let UserInstance = UserManager.sharedInstance

private let kNickname    = "kTS_wechat_username"
private let kAvatar      = "kTS_wechat_avatar"
private let kAccessToken = "kTS_wechat_accessToken"
private let kUserId      = "kTS_wechat_userId"
private let kRoleId      = "kTS_wechat_roleId"
private let kIsLogin     = "kTS_wechat_isLogin"
private let kLoginName   = "kTS_wechat_loginName"
private let kPassword    = "kTS_wechat_password"
private let kCurrentGPS    = "kTS_wechat_currentGPS"
private let kCurrentMenus    = "kTS_wechat_currentMenus"



class UserManager: NSObject {
    class var sharedInstance : UserManager {
        struct Static {
            static let instance : UserManager = UserManager()
        }
        return Static.instance
    }
    
    let TSKeychain = Keychain(service: "com.wechat.Hilen") //keychain
    var accessToken: String? {
        get { return UserDefaults.ts_stringForKey(kAccessToken, defaultValue: "") }
        set (newValue) { UserDefaults.ts_setString(kAccessToken, value: newValue) }
    }
    /// 用户昵称，不是登录名
    var nickname: String? {
        get { return UserDefaults.ts_stringForKey(kNickname, defaultValue: "") }
        set (newValue) { UserDefaults.ts_setString(kNickname, value: newValue) }
    }
    var avatar: String? {
        get { return UserDefaults.ts_stringForKey(kAvatar, defaultValue: "") }
        set (newValue) { UserDefaults.ts_setString(kAvatar, value: newValue) }
    }
    var userId: String? {
        get { return UserDefaults.ts_stringForKey(kUserId, defaultValue: "") }
        set (newValue) { UserDefaults.ts_setString(kUserId, value: newValue) }
    }
    var roleId: String? {
        get { return UserDefaults.ts_stringForKey(kRoleId, defaultValue: "") }
        set (newValue) { UserDefaults.ts_setString(kRoleId, value: newValue) }
    }
    var isLogin: Bool {
        get { return UserDefaults.ts_boolForKey(kIsLogin, defaultValue: false) }
        set (newValue) { UserDefaults.ts_setBool(kIsLogin, value: newValue) }
    }
    var currentGPS: String? {
        get { return UserDefaults.ts_stringForKey(kCurrentGPS, defaultValue: "") }
        set (newValue) { UserDefaults.ts_setString(kCurrentGPS, value: newValue) }
    }
    var currentMenus: [String]? {
        get { return UserDefaults.ts_arrayForKey(kCurrentMenus, defaultValue: []) as? [String]}
        set (newValue) { UserDefaults.ts_setArray(kCurrentMenus, value: (newValue ?? []) as [AnyObject])}
    }
    
    /// 用户手机号 ,存在 keychain
    var loginName: String? {
        get { return  TSKeychain[kLoginName] ?? "" }
        set (newValue) { TSKeychain[kLoginName] = newValue }
    }
    
    ///密码, 存在 keychain
    var password: String?  {
        get { return  TSKeychain[kPassword] ?? "" }
        set (newValue) { TSKeychain[kPassword] = newValue }
    }

    fileprivate override init() {
        super.init()
    }
    
    func readAllData() {
        self.nickname = UserDefaults.ts_stringForKey(kNickname, defaultValue: "")
        self.avatar = UserDefaults.ts_stringForKey(kAvatar, defaultValue: "")
        self.userId = UserDefaults.ts_stringForKey(kUserId, defaultValue: "")
        self.isLogin = UserDefaults.ts_boolForKey(kIsLogin, defaultValue: false)
        self.loginName = TSKeychain[kLoginName] ?? ""
        self.password = TSKeychain[kPassword] ?? ""
    }
    
    /**
     登录成功
     - parameter result: 登录成功后传进来的字典
     */
//    func userLoginSuccess(_ result: JSON) {
//        self.loginName = result["username"].stringValue
//        self.password = result["password"].stringValue
//        self.nickname = result["nickname"].stringValue
//        self.userId = result["user_id"].stringValue
//        self.isLogin = true
//    }
    
    /**
     退出登录
     */
    func userLogout() {
        self.accessToken = ""
        self.loginName = ""
        self.password = ""
        self.nickname = ""
        self.userId = ""
        self.roleId = ""
        self.isLogin = false
    }
    
    func resetAccessToken(_ token: String) {
        UserDefaults.ts_setString(kAccessToken, value: token)
        if token.count > 0 {
//            log.info("token success")
        } else {
            self.userLogout()
        }
    }
}


public var TSUserDefaults = UserDefaults.standard

public extension UserDefaults {
    // MARK: - Getter
    /**
     Get object from NSUserDefaults
     
     - parameter key:          key
     - parameter defaultValue: defaultValue, this can be nil
     
     - returns: AnyObject
     */
    class func ts_objectForKey(_ key: String, defaultValue: AnyObject? = nil) -> AnyObject? {
        if (defaultValue != nil) && ts_objectForKey(key) == nil {
            return defaultValue
        }
        return TSUserDefaults.object(forKey: key) as AnyObject?
    }
    
    /**
     Get integer from NSUserDefaults
     
     - parameter key:          key
     - parameter defaultValue: defaultValue, this can be nil
     
     - returns: AnyObject
     */
    class func ts_integerForKey(_ key: String, defaultValue: Int? = nil) -> Int {
        if (defaultValue != nil) && ts_objectForKey(key) == nil {
            return defaultValue!
        }
        return TSUserDefaults.integer(forKey: key)
    }
    
    /**
     Get bool from NSUserDefaults
     
     - parameter key:          key
     - parameter defaultValue: defaultValue, this can be nil
     
     - returns: AnyObject
     */
    class func ts_boolForKey(_ key: String, defaultValue: Bool? = nil) -> Bool {
        if (defaultValue != nil) && ts_objectForKey(key) == nil {
            return defaultValue!
        }
        return TSUserDefaults.bool(forKey: key)
    }
    
    /**
     Get float from NSUserDefaults
     
     - parameter key:          key
     - parameter defaultValue: defaultValue, this can be nil
     
     - returns: AnyObject
     */
    class func ts_floatForKey(_ key: String, defaultValue: Float? = nil) -> Float {
        if (defaultValue != nil) && ts_objectForKey(key) == nil {
            return defaultValue!
        }
        return TSUserDefaults.float(forKey: key)
    }

    /**
     Get double from NSUserDefaults
     
     - parameter key:          key
     - parameter defaultValue: defaultValue, this can be nil
     
     - returns: AnyObject
     */
    class func ts_doubleForKey(_ key: String, defaultValue: Double? = nil) -> Double {
        if (defaultValue != nil) && ts_objectForKey(key) == nil {
            return defaultValue!
        }
        return TSUserDefaults.double(forKey: key)
    }
    
    /**
     Get string from NSUserDefaults
     
     - parameter key:          key
     - parameter defaultValue: defaultValue, this can be nil
     
     - returns: AnyObject
     */
    class func ts_stringForKey(_ key: String, defaultValue: String? = nil) -> String? {
        if (defaultValue != nil) && ts_objectForKey(key) == nil {
            return defaultValue!
        }
        return TSUserDefaults.string(forKey: key)
    }
    
    /**
     Get NSData from NSUserDefaults
     
     - parameter key:          key
     - parameter defaultValue: defaultValue, this can be nil
     
     - returns: AnyObject
     */
    class func ts_dataForKey(_ key: String, defaultValue: Data? = nil) -> Data? {
        if (defaultValue != nil) && ts_objectForKey(key) == nil {
            return defaultValue!
        }
        return TSUserDefaults.data(forKey: key)
    }
    
    /**
     Get NSURL from NSUserDefaults
     
     - parameter key:          key
     - parameter defaultValue: defaultValue, this can be nil
     
     - returns: AnyObject
     */
    class func ts_URLForKey(_ key: String, defaultValue: URL? = nil) -> URL? {
        if (defaultValue != nil) && ts_objectForKey(key) == nil {
            return defaultValue!
        }
        return TSUserDefaults.url(forKey: key)
    }
    
    /**
     Get array from NSUserDefaults
     
     - parameter key:          key
     - parameter defaultValue: defaultValue, this can be nil
     
     - returns: AnyObject
     */
    class func ts_arrayForKey(_ key: String, defaultValue: [AnyObject]? = nil) -> [AnyObject]? {
        if (defaultValue != nil) && ts_objectForKey(key) == nil {
            return defaultValue!
        }
        return TSUserDefaults.array(forKey: key) as [AnyObject]?
    }
    
    /**
     Get dictionary from NSUserDefaults
     
     - parameter key:          key
     - parameter defaultValue: defaultValue, this can be nil
     
     - returns: AnyObject
     */
    class func ts_dictionaryForKey(_ key: String, defaultValue: [String : AnyObject]? = nil) -> [String : AnyObject]? {
        if (defaultValue != nil) && ts_objectForKey(key) == nil {
            return defaultValue!
        }
        return TSUserDefaults.dictionary(forKey: key) as [String : AnyObject]?
    }

    // MARK: - Setter
    
    /**
     Set object for key
     
     - parameter key:   key
     - parameter value: value
     */
    class func ts_setObject(_ key: String, value: AnyObject?) {
        if value == nil {
            TSUserDefaults.removeObject(forKey: key)
        } else {
            TSUserDefaults.set(value, forKey: key)
        }
        TSUserDefaults.synchronize()
    }
    
    /**
     Set integer for key
     
     - parameter key:   key
     - parameter value: value
     */
    class func ts_setInteger(_ key: String, value: Int) {
        TSUserDefaults.set(value, forKey: key)
        TSUserDefaults.synchronize()
    }
    
    /**
     Set bool for key
     
     - parameter key:   key
     - parameter value: value
     */
    class func ts_setBool(_ key: String, value: Bool) {
        TSUserDefaults.set(value, forKey: key)
        TSUserDefaults.synchronize()
    }
    
    /**
     Set float for key
     
     - parameter key:   key
     - parameter value: value
     */
    class func ts_setFloat(_ key: String, value: Float) {
        TSUserDefaults.set(value, forKey: key)
        TSUserDefaults.synchronize()
    }
    
    /**
     Set string for key
     
     - parameter key:   key
     - parameter value: value
     */
    class func ts_setString(_ key: String, value: String?) {
        if (value == nil) {
            TSUserDefaults.removeObject(forKey: key)
        } else {
            TSUserDefaults.set(value, forKey: key)
        }
        TSUserDefaults.synchronize()
    }
    
    /**
     Set data for key
     
     - parameter key:   key
     - parameter value: value
     */
    class func ts_setData(_ key: String, value: Data) {
        self.ts_setObject(key, value: value as AnyObject?)
    }
    
    /**
     Set array for key
     
     - parameter key:   key
     - parameter value: value
     */
    class func ts_setArray(_ key: String, value: [AnyObject]) {
        self.ts_setObject(key, value: value as AnyObject?)
    }
    
    /**
     Set dictionary for key
     
     - parameter key:   key
     - parameter value: value
     */
    class func ts_setDictionary(_ key: String, value: [String : AnyObject]) {
        self.ts_setObject(key, value: value as AnyObject?)
    }
}
