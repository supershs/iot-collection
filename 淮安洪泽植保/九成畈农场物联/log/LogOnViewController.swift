//
//  LogOnViewController.swift
//  33web
//
//  Created by JAY on 2023/5/10.
//

import UIKit
import SnapKit
import RxSwift
import RxAlamofire
import HandyJSON
import Alamofire
import SwiftyJSON
import KeychainAccess
class LogOnViewController: RootViewController,UITextViewDelegate,UITextFieldDelegate{
    let disposBag = DisposeBag()
//    var TSKeychain : Keychain! = nil
    var TSKeychain: Keychain! = Keychain()

    ///点击类型
    enum ClickLinkType {
        ///用户协议
        case userProtocol
        ///隐私条款
        case privacyPolicy
    }
    
    var showAll : Bool!
    var btnImage : Bool!
    
    ///点击事件
    var clickHandle:((_ clickType:ClickLinkType)->())?
    
    public var imageV : UIImageView = {
        let iconV = UIImageView()
        iconV.image = UIImage(named: "login_bg")
        return iconV
    }()
    
    public var logoV : UIImageView = {
        
        let iconV = UIImageView()
        iconV.image = UIImage(named: "lg")
        return iconV
        
    }()
    
    public var logoVBackV : UIImageView = {
        let iconV = UIImageView()
        iconV.image = UIImage(named: "login")
        return iconV
        
    }()
    
    public var logoLb : UILabel = {
        
        let lb = UILabel()
        lb.textColor = UIColor.black
        lb.font = UIFont.boldSystemFont(ofSize: 25)
        lb.text = "淮安市洪泽区\n病虫检测汇总平台"
        lb.numberOfLines = 0
        lb.textAlignment = .left
        return lb
        
    }()
    
    public var accountView : UIView = {
        let v = UIView()
        v.backgroundColor = UIColor(hex: "f7f7fb")
        v.layer.cornerRadius = 5
        return v
    }()
    
    public var passwordView : UIView = {
        let v = UIView()
        v.backgroundColor = UIColor(hex: "f7f7fb")
        v.layer.cornerRadius = 5
        return v
    }()
    
    public var accountLb : UILabel = {
        let lb = UILabel()
        lb.font = UIFont.systemFont(ofSize: 12.0)
        lb.textColor = UIColor.black
        lb.text = "账号"
        lb.numberOfLines = 0
        lb.textAlignment = .left
        return lb
    }()
    public var passwordLb : UILabel = {
        let lb = UILabel()
        lb.font = UIFont.systemFont(ofSize: 12.0)
        lb.textColor = UIColor.black
        lb.text = "密码"
        lb.numberOfLines = 0
        lb.textAlignment = .left
        return lb
    }()
    
    public var eyeBtn : UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named:"yan"), for: .normal)
        return btn
        
    }()
    
    var usernameTF: UITextField = {
        let tf = UITextField()
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.textColor = .black
       
        tf.placeholder = "请输入账号"
        return tf
    }()
    

    var passwordTF: UITextField = {
        let tf = UITextField()
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.textColor = .black
        tf.placeholder = "请输入密码"
        tf.isSecureTextEntry = true
        return tf
    }()
    
    public var forgetBtn : UIButton = {
        let btn = UIButton()
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        btn.setTitleColor(UIColor(hex: "4bca8a"), for: .normal)
        btn.contentHorizontalAlignment = .right
        btn.setTitle("忘记密码", for: .normal)
        return btn
    }()
    
    public var rememberBtn : UIButton = {
        let btn = UIButton()
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        btn.setTitleColor(UIColor(hex: "#333333"), for: .normal)
        btn.contentHorizontalAlignment = .right
        btn.setTitle("记住密码", for: .normal)
        return btn
    }()
    
    public var rememberButton : UIButton = {
        let bt = UIButton()
        bt.setImage(UIImage(named: "select_false"), for: .normal)
        return bt
    }()
    
    public var logBtn : UIButton = {
        let btn = UIButton()
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        btn.setTitleColor(.white, for: .normal)
        btn.contentHorizontalAlignment = .center
        btn.setTitle("登录", for: .normal)
        btn.setBackgroundImage(UIImage(named: "login_btn"), for: .normal)
        return btn
    }()
    
    public var delegateButton : UIButton = {
        let bt = UIButton()
        bt.setImage(UIImage(named: "select_false"), for: .normal)
        return bt
    }()
    
    
    ///同意View
    private lazy var agreeTextView : UITextView = {
        let textStr = "我已阅读并同意 “用户协议和隐私条款”"
        let textView = UITextView()
        textView.delegate = self
        textView.font =  UIFont.systemFont(ofSize: 16, weight: .regular)
        textView.textColor = UIColor(hex:"666666")
        textView.textAlignment = .center
 
        ///设为true 在代理里面禁掉所有的交互事件
        textView.isEditable = true
         
        textView.autoresizingMask =  UIView.AutoresizingMask.flexibleHeight
        textView.isScrollEnabled = false
        let attStr = NSMutableAttributedString(string: textStr)
         
        //点击超链接
        attStr.addAttribute(NSAttributedString.Key.link, value: "userProtocol://", range: (textStr as NSString).range(of: "用户协议和隐私条款"))
        textView.attributedText = attStr
        ///只能设置一种颜色
        textView.linkTextAttributes =  [
            NSAttributedString.Key.foregroundColor: UIColor(hex: "00B85F")
        ]
    
        return textView
    }()
    
    public var phoneBtn : UIButton = {
        let btn = UIButton()
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        btn.setTitleColor(UIColor(hex: "c9c5c5"), for: .normal)
        btn.contentHorizontalAlignment = .center
        btn.setTitle("手机号验证码登录", for: .normal)

        return btn
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        showAll = false
        btnImage = false

        view.backgroundColor = .white
        view.addSubview(imageV)
        view.addSubview(logoVBackV)
        view.addSubview(logoV)
        view.addSubview(logoLb)
        view.addSubview(accountView)
        view.addSubview(passwordView)
        accountView.addSubview(accountLb)
        passwordView.addSubview(passwordLb)
        accountView.addSubview(usernameTF)
        passwordView.addSubview(passwordTF)
        passwordView.addSubview(eyeBtn)
        
        view.addSubview(forgetBtn)
        view.addSubview(rememberBtn)
        view.addSubview(rememberButton)
        view.addSubview(logBtn)
        view.addSubview(delegateButton)
        view.addSubview(agreeTextView)
        view.addSubview(phoneBtn)
        
        passwordTF.delegate = self
        usernameTF.delegate = self
        
        agreeTextView.font =  UIFont.systemFont(ofSize: 13, weight: .regular)
        
        eyeBtn.addTarget(self, action: #selector(eyeClick), for: .touchUpInside)
        forgetBtn.addTarget(self, action: #selector(forgetClick), for: .touchUpInside)
        rememberButton.addTarget(self, action: #selector(rememberBtnClick), for: .touchUpInside)
        delegateButton.addTarget(self, action: #selector(delegateClick), for: .touchUpInside)
        logBtn.addTarget(self, action: #selector(logClick), for: .touchUpInside)
        phoneBtn.addTarget(self, action: #selector(phoneClick), for: .touchUpInside)
        
        //注册点击事件
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))

        
        imageV.snp.makeConstraints { make in
            make.top.bottom.right.left.equalTo(view)
        }
        
        logoVBackV.snp.makeConstraints { make in
            make.top.equalTo(view).offset(120)
            make.left.equalTo(view)
            make.width.equalTo(190)
            make.height.equalTo(56)
        }
        
        logoV.snp.makeConstraints { make in
            make.top.equalTo(logoVBackV.snp.bottom).offset(-30)
            make.left.equalTo(view).offset(15)
            make.width.equalTo(60)
            make.height.equalTo(60)
        }
        
        logoLb.snp.makeConstraints { make in
            make.top.equalTo(logoV)
            make.left.equalTo(logoV.snp.right).offset(10)
            make.width.equalTo(400)
            make.height.equalTo(60)
        }
    
        accountView.snp.makeConstraints { make in
            make.top.equalTo(logoV.snp.bottom).offset(50)
            make.left.equalTo(imageV).offset(30)
            make.right.equalTo(imageV).offset(-30)
            make.height.equalTo(60)
        }
        passwordView.snp.makeConstraints { make in
            make.top.equalTo(accountView.snp.bottom).offset(20)
            make.left.equalTo(imageV).offset(30)
            make.right.equalTo(imageV).offset(-30)
            make.height.equalTo(60)
        }
        accountLb.snp.makeConstraints { make in
            make.top.equalTo(accountView).offset(5)
            make.left.equalTo(accountView).offset(10)
            make.right.equalTo(accountView).offset(-10)
            make.height.equalTo(25)
        }
        passwordLb.snp.makeConstraints { make in
            make.top.equalTo(passwordView).offset(5)
            make.left.equalTo(passwordView).offset(10)
            make.right.equalTo(passwordView).offset(-10)
            make.height.equalTo(25)
        }
        
        usernameTF.snp.makeConstraints { make in
            make.top.equalTo(accountLb.snp.bottom).offset(0)
            make.left.equalTo(accountView).offset(10)
            make.right.equalTo(accountView).offset(-10)
            make.bottom.equalTo(accountView)
        }
        
        passwordTF.snp.makeConstraints { make in
            make.top.equalTo(passwordLb.snp.bottom).offset(0)
            make.left.equalTo(passwordView).offset(10)
            make.right.equalTo(passwordView).offset(-10)
            make.bottom.equalTo(passwordView)
        }
        
        eyeBtn.snp.makeConstraints { make in
            make.width.equalTo(25)
            make.height.equalTo(25)
            make.right.equalTo(passwordView).offset(-10)
            make.bottom.equalTo(passwordView).offset(-5)
        }
        
        forgetBtn.snp.makeConstraints { make in
            make.top.equalTo(passwordView.snp.bottom).offset(10)
            make.right.equalTo(passwordView.snp.right).offset(0)
            make.width.equalTo(100)
            make.height.equalTo(30)
        }
        
//        rememberBtn.snp.makeConstraints { make in
//            make.top.equalTo(passwordView.snp.bottom).offset(10)
//            make.left.equalTo(passwordView.snp.left).offset(5)
//            make.width.equalTo(70)
//            make.height.equalTo(30)
//        }
        
//        rememberButton.snp.makeConstraints { make in
//            make.centerY.equalTo(rememberBtn)
//            make.right.equalTo(rememberBtn.snp.left).offset(10)
//            make.width.equalTo(15)
//            make.height.equalTo(15)
//        }
        
        logBtn.snp.makeConstraints { make in
            make.top.equalTo(forgetBtn.snp.bottom).offset(20)
            make.right.equalTo(view).offset(-45)
            make.left.equalTo(view).offset(45)
            make.height.equalTo(50)
        }
        
        delegateButton.snp.makeConstraints { make in
            make.top.equalTo(logBtn.snp.bottom).offset(22)
            make.left.equalTo(logBtn).offset(20)
            make.width.equalTo(15)
            make.height.equalTo(15)
        }
        
        agreeTextView.snp.makeConstraints { make in
            make.centerY.equalTo(delegateButton)
            make.left.equalTo(delegateButton.snp.right).offset(5)
            make.width.equalTo(300)
            make.height.equalTo(30)
        }
        

        phoneBtn.snp.makeConstraints { make in
            make.top.equalTo(agreeTextView.snp.bottom).offset(50)
            make.centerX.equalTo(view)
            make.width.equalTo(200)
            make.height.equalTo(30)
        }
        
        
        
//        
    }
    //点击空白处关闭键盘方法
    @objc func handleTap(sender: UITapGestureRecognizer) {
           if sender.state == .ended {
               print("收回键盘")
               self.usernameTF.resignFirstResponder()//username放弃第一响应者
               self.passwordTF.resignFirstResponder()//password放弃第一响应者
           }
           sender.cancelsTouchesInView = false
       }
    //    MARK: -  查看密码
    @objc func eyeClick(){
        
        if showAll{
            showAll = false
            passwordTF.isSecureTextEntry = true
        }else{
            showAll = true
            passwordTF.isSecureTextEntry = false
        }
       
    }
    
    //    MARK: -  验证码登录
    @objc func phoneClick(){
        
        self.navigationController?.pushViewController(HSWebViewController(path: kWebUrl + codeUrl.getEncodeString), animated: true)
    }
    
    //    MARK: -  忘记密码
    @objc func forgetClick(){
        print("忘记密码")
         self.navigationController?.pushViewController(HSWebViewController(path: kWebUrl + forgetUrl.getEncodeString), animated: true)
    }
    
    
    //    MARK: -  登录
    @objc func logClick(){
        
        if usernameTF.text!.isEmpty {
            TSProgressHUD.ts_showWarningWithStatus("请输入账号")
        }else if passwordTF.text!.isEmpty{
            TSProgressHUD.ts_showWarningWithStatus("请输入密码")
        }else if btnImage == false{
            TSProgressHUD.ts_showWarningWithStatus("请先勾选用户协议和隐私条款")
        }else{
//            alamofireRequest()
            self.view?.endEditing(false)
            goLogin(loginName: "", password: "")
            
        }
    
    }
    
    
    //    MARK: -  勾选协议
    @objc func delegateClick(){

        if btnImage{
            btnImage = false
            delegateButton.setImage(UIImage(named: "select_false"), for: .normal)
        }else{
            btnImage = true
            
            delegateButton.setImage(UIImage(named: "select_true"), for: .normal)
        }
        
    }
    
    //    MARK: -  记住密码
    @objc func rememberBtnClick(){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        print("appDelegate === ",appDelegate.record)
        if appDelegate.record == "1"{
            print("忘记密码灰色",appDelegate.record)
            appDelegate.record = "0"
            rememberButton.setImage(UIImage(named: "select_false"), for: .normal)
             
        }else{
            print("忘记密码亮色",appDelegate.record)
            appDelegate.record = "1"
            rememberButton.setImage(UIImage(named: "select_true"), for: .normal)
            
        }
    }
    

    func goLogin(loginName: String, password: String){
        
        TSProgressHUD.ts_showWithStatus("")
        let sysTenantDTO = ["url":paraUrl]
        let parameters  = ["loginName":usernameTF.text!,"password":AESCode.endcode_AES_CBC(strToEncode: passwordTF.text!),"sysTenantDTO":sysTenantDTO] as [String : Any]
        
        print("=====parameters===",parameters)
        print("=====accountUrl===",accountUrl)
        RxAlamofire.requestJSON(.post, URL(string: accountUrl)!, parameters: parameters, encoding: JSONEncoding.default, headers: ["Content-Type":"application/json;charset=UTF-8"])
                  .debug()
        
                  .subscribe(onNext: { (r, json) in
                      
                      
                    if let dict = json as? [String: AnyObject] {
                     
                        let modelA = NSRootModel<LoginModel>.deserialize(from: dict)
                        if modelA?.status == "SUCCESS"  {
                            
                            TSProgressHUD.ts_dismiss()
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            print("appDelegate.record === ",appDelegate.record)
                            if appDelegate.record == "0"{
                                
                                do {
                                    try self.TSKeychain.removeAll()
                                } catch {
                                    print("An error occurred: \(error)")
                                }
                                
                                self.TSKeychain["username"] = self.usernameTF.text!
                            }else{
                                // 将值存储到 Keychain 中
//                                try TSKeychain.removeAll()
                                self.TSKeychain["username"] = self.usernameTF.text!
                                self.TSKeychain["password"] = self.passwordTF.text!
                                print("将值存储到 Keychain 中")
                            }
                            UserDefaults.standard.set(modelA?.data?.token, forKey: "token")
                            UserDefaults.standard.set(self.usernameTF.text!, forKey: "username")
                            UserDefaults.standard.set(self.passwordTF.text!, forKey: "password")
                            print(">>>>>>>>>>>>>>>>>>>自己的打印>>>>>>>>>>>>>>>>>>>>>>")
                            let urlStr =  GOTOHOME  + (modelA?.data?.toJSONString() ?? "")
                            
                            
                            self.navigationController?.pushViewController(HSWebViewController(path: kWebUrl + urlStr.getEncodeString), animated: true)
                            
                        }else{
                            TSProgressHUD.ts_dismiss()
                            TSProgressHUD.ts_showWarningWithStatus(modelA?.message ?? "")
                            
                            }

                        }
                      
                  }, onError: { (error) in
                        print(error,"=====error")
                      print("parameters == ",parameters)
                      TSProgressHUD.ts_dismiss()
                  })
                  .disposed(by: disposBag)
    }
    

    
    //textField点击return关闭键盘  agreementURL
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view?.endEditing(false)
        return true
    }
    
    
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        return false
    }
     
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if URL.scheme  ==  "userProtocol"{
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.record = "1"
            UserDefaults.standard.set(self.usernameTF.text!, forKey: "username")
            UserDefaults.standard.set(self.passwordTF.text!, forKey: "password")
            TSKeychain["username"] = self.usernameTF.text!
            TSKeychain["password"] = self.passwordTF.text!
            
            self.navigationController?.pushViewController(HSWebViewController(path: kWebUrl + agreementURL), animated: true)
        
            return false
        }else if URL.scheme == "privacyPolicy"{
            self.clickHandle?(.privacyPolicy)
            print("111111111")
            return false
        }
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
        
        // 从 Keychain 中检索值
        if let username = TSKeychain["username"] {
            print("Username: \(username)")
            usernameTF.text = username
        } else {
            usernameTF.text = ""
            print("No username stored in Keychain.")
        }
        
        if let password = TSKeychain["password"] {
            print("Username: \(password)")
            passwordTF.text = password
//            rememberButton.setImage(UIImage(named: "select_true"), for: .normal)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.record = "0"
            passwordTF.text = password
        } else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            
            if appDelegate.record == "0"{
                appDelegate.record = "0"
            }else{
                appDelegate.record = "1"
            }
//            let username = TSKeychain["username"]
//            let password = TSKeychain["password"]
            passwordTF.text = ""
//            rememberButton.setImage(UIImage(named: "select_false"), for: .normal)
            print("No username stored in Keychain.")
        }
        
    }
                             
                                     
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
//        self.navigationController?.isNavigationBarHidden = false
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
    }
    
    
}

extension String {
    /// String转encode
        var getEncodeString: String {
            guard self.count != 0 else { return ""}
            if let u = self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                return u
            }
            return ""
        }
}

extension UIColor {
    public convenience init(redIn255: Int, greenIn255: Int, blueIn255: Int, alphaIn100: Int = 100) {
        self.init(red: CGFloat(redIn255)/255, green: CGFloat(greenIn255)/255, blue: CGFloat(blueIn255)/255, alpha: CGFloat(alphaIn100)/100)
    }
}

extension String {
    public var hexInt: Int? {
        let scanner = Scanner(string: self)
        var value: UInt64 = 0
        guard scanner.scanHexInt64(&value) else { return nil}
        return Int(value)
    }
}


extension UIColor {
    public convenience init?(hex: String) {
        var str = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        let startIndex = str.startIndex
        if str.hasPrefix("#") {
            let indexOffsetBy1 = str.index(startIndex, offsetBy: 1)
            str = String(str[indexOffsetBy1...])
        }
        
        guard str.count == 6 else { return nil }
        
        let indexOffsetBy2 = str.index(startIndex, offsetBy: 2)
        let indexOffsetBy4 = str.index(startIndex, offsetBy: 4)

        var red = String(str[..<indexOffsetBy2])
        var green = String(str[indexOffsetBy2..<indexOffsetBy4])
        var blue = String(str[indexOffsetBy4...])
            
        guard let redIn255 = red.hexInt else { return nil }
        guard let greenIn255: Int = green.hexInt else { return nil }
        guard let blueIn255: Int = blue.hexInt else { return nil }
        
        self.init(redIn255: redIn255, greenIn255: greenIn255, blueIn255: blueIn255)
    }
}
