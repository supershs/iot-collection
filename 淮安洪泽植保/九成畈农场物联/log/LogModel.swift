//
//  LogModel.swift
//  33web
//
//  Created by JAY on 2023/5/9.
//

import UIKit

import HandyJSON


struct NSRootModel<T: HandyJSON> : HandyJSON{
    ///
    var date: String?
    ///
    var data: T?
    /// SUCCESS
    var status: String?
    /// 10000
    var code: Int = 0
    /// 登录成功
    var message: String?

}

struct LoginModel : HandyJSON {
    /// <#泛型#>
      var deptPostVOList: Any?
      /// <#泛型#>
      var salt: Any?
      /// 超级管理员
      var trueName: String?
      /// 1
      var id: String?
      /// <#泛型#>
      var sysPostVO: Any?
      /// <#泛型#>
      var sex: Any?
      /// <#泛型#>
      var deptVO: Any?
      /// <#泛型#>
      var password: Any?
      ///
      var token: String?
      /// <#泛型#>
      var createBy: Any?
      /// <#泛型#>
      var educationVOS: Any?
      /// <#泛型#>
      var menuRoleVO: Any?
      /// <#泛型#>
      var nickName: Any?
      /// <#泛型#>
      var appTenantUserVO: Any?
      /// <#泛型#>
      var recentOnLineTime: Any?
      /// <#泛型#>
      var remarks: Any?
      ///
      var tenant: NSTenantModel?
      ///
      var sysTenantUserVO: NSSysTenantUserVOModel?
      ///
      var headImg: String?
      /// <#泛型#>
      var loginTime: Any?
      /// <#泛型#>
      var sort: Any?
      /// <#泛型#>
      var tenantMenuVO: Any?
      /// <#泛型#>
      var birthDayStr: Any?
      /// <#泛型#>
      var uuid: Any?
      /// <#泛型#>
      var tenantId: Any?
      /// <#泛型#>
      var updateBy: Any?
      /// <#泛型#>
      var createDate: Any?
      /// <#泛型#>
      var version: Any?
      /// 13382330001
      var mobile: String?
      /// <#泛型#>
      var sysDeptPostVO: Any?
      /// <#泛型#>
      var userProfileVO: Any?
      /// admin
      var loginName: String?
      /// <#泛型#>
      var updateDate: Any?
      /// <#泛型#>
      var age: Any?
      /// <#泛型#>
      var status: Any?




}

struct NSSysTenantUserVOModel : HandyJSON {

    /// <#泛型#>
       var loginType: Any?
       /// <#泛型#>
       var createDate: Any?
       /// 165DAA
       var mainColor: String?
       /// <#泛型#>
       var menuVOS: Any?
       /// <#泛型#>
       var locked: Any?
       /// <#泛型#>
       var loginTime: Any?
       /// ADMIN
       var userType: String?
       /// <#泛型#>
       var status: Any?
       /// <#泛型#>
       var userVO: Any?
       /// <#泛型#>
       var remarks: Any?
       /// <#泛型#>
       var updateDate: Any?
       /// <#泛型#>
       var tenantVO: Any?
       /// <#泛型#>
       var updateBy: Any?
       /// <#泛型#>
       var sort: Any?
       /// <#泛型#>
       var version: Any?
       /// 1
       var id: String?
       /// <#泛型#>
       var tenantId: Any?
       /// <#泛型#>
       var createBy: Any?
       /// <#泛型#>
       var profileVO: Any?
       /// <#泛型#>
       var userId: Any?
       /// <#泛型#>
       var uuid: Any?


}

struct NSTenantModel : HandyJSON {

    /// ADMIN
    var type: String?
    /// <#泛型#>
    var activate: Any?
    /// <#泛型#>
    var version: Any?
    /// <#泛型#>
    var urlType: Any?
    /// <#泛型#>
    var createDate: Any?
    /// <#泛型#>
    var hasApp: Any?
    /// <#泛型#>
    var appTenantMenuVOS: Any?
    /// <#泛型#>
    var tenantMenuList: Any?
    /// <#泛型#>
    var status: Any?
    /// <#泛型#>
    var applyId: Any?
    /// <#泛型#>
    var remarks: Any?
    /// <#泛型#>
    var adminUserList: Any?
    /// <#泛型#>
    var updateDate: Any?
    /// <#泛型#>
    var updateBy: Any?
    /// <#泛型#>
    var sort: Any?
    /// <#泛型#>
    var name: Any?
    /// <#泛型#>
    var url: Any?
    /// <#泛型#>
    var id: Any?
    /// <#泛型#>
    var uuid: Any?
    /// <#泛型#>
    var current: Any?
    /// <#泛型#>
    var info: Any?
    /// <#泛型#>
    var menuTree: Any?
    /// <#泛型#>
    var createBy: Any?
    /// <#泛型#>
    var tenantId: Any?

}

