//
//  ZGLiveBottomView.swift
//  ZhangGuang
//
//  Created by 叁拾叁 on 2020/7/20.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit

class ZGLiveBottomView: UIView, UIGestureRecognizerDelegate {

    var clickedClosure: ((Int) -> Void)?
    
    var pressClosure: ((UILongPressGestureRecognizer) -> Void)?
    
    let btnCamera: UIButton = {
        let v = UIButton()
        v.setImage(UIImage(named: "camera"), for: .normal)
        v.tag = 1000
        return v
    }()
    
    let btnCloudSpread: UIButton = {
        let v = UIButton()
        v.setImage(UIImage(named: "update"), for: .normal)
        v.tag = 1001
        return v
    }()
    
    let btnMicrophone: UIButton = {
        let v = UIButton()
        v.setImage(UIImage(named: "microphone"), for: .normal)
        return v
    }()
    
    var longPress: UILongPressGestureRecognizer!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    fileprivate func initViews() {
        
        self.addSubview(btnCamera)
        self.addSubview(btnCloudSpread)
        self.addSubview(btnMicrophone)
        btnCamera.addTarget(self, action: #selector(btnAction), for: .touchUpInside)
        btnCloudSpread.addTarget(self, action: #selector(btnAction), for: .touchUpInside)
        longPress = UILongPressGestureRecognizer(target: self, action: #selector(pressesAction))
        btnMicrophone.addGestureRecognizer(longPress)
        longPress.minimumPressDuration = 0.2
        longPress.delegate = self
        
        btnCamera.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalTo(self.btnCloudSpread.snp.left).offset(-15)
            make.width.equalTo(29*2)
            make.height.equalTo(18.5*2)
        }
        
        btnCloudSpread.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalTo(self.btnMicrophone.snp.left).offset(-15)
            make.width.equalTo(29*2)
            make.height.equalTo(18.5*2)
        }
        
        btnMicrophone.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-30)
            make.width.equalTo(13.5*4)
            make.height.equalTo(18.5*4)
        }
    }
    
    @objc func btnAction(button: UIButton) {
        if let c = clickedClosure {
            c(button.tag - 1000)
        }
    }
    
    
    override func pressesChanged(_ presses: Set<UIPress>, with event: UIPressesEvent?) {
        if let c = pressClosure {
            c(longPress)
        }
    }

    @objc func pressesAction(ges: UILongPressGestureRecognizer) {
        if let c = pressClosure {
            c(ges)
        }
    }
}
