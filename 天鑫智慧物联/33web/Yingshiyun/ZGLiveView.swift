//
//  ZGLiveView.swift
//  ZhangGuang
//
//  Created by 叁拾叁 on 2020/7/20.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit
import EZOpenSDKFramework
import TZImagePickerController
import Toast
import SnapKit
import SwiftWeb

class ZGLiveView: UIView, EZPlayerDelegate, TZImagePickerControllerDelegate, ZGAudioRecorderProtocol {
    
    weak var liveProtocol: ZGLiveProtocol!
    weak var currentVC: HSWebViewController!
    let deviceFile = EZDeviceRecordFile()
    var recoder: ZGAudioRecorder = ZGAudioRecorder()
    var isPlayback: Bool = false
    var maxCnt: NSInteger = 2 //失败重新加载最大次数
    var picParams = Dictionary<String, Any>()
    var PTZParams = Dictionary<String, Any>()
    var accessTokenParams = Dictionary<String, Any>()
    var blanner: UIView?
    var player: EZPlayer?//视频播放
    var playerPlayback: EZPlayer?//回放视频播放
    var talkPlayer: EZPlayer?//视频对讲
    var commandType: EZPTZCommand = .up
    var cameraNo: NSInteger = 0
    var deviceSerial = ""
    var canTakePhoto: Bool!
    var middleView: ZGLiveMiddleView? = {
        let v = ZGLiveMiddleView()
        
        return v
    }()
    var topView: ZGLiveTopView? = {
        let v = ZGLiveTopView()
        v.backgroundColor = UIColor(white: 0, alpha: 0.5)
        return v
    }()
    var bottomView: ZGLiveBottomView? = {
        let v = ZGLiveBottomView()
        v.backgroundColor = UIColor(white: 0, alpha: 0.5)
        return v
    }()
    
    var beisuChoiceView: ZGLiveBeisuChoiceView? = {
        let v = ZGLiveBeisuChoiceView()
        v.isHidden = true
        return v
    }()
    var timer: Timer!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        blanner = UIView(frame: self.frame)
        blanner!.backgroundColor = UIColor.black
        blanner!.isHidden = true
        recoder.delegate = self
        startTimer()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func startTimer() {
        
        if timer == nil {
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(getMsgs), userInfo: nil, repeats: true)
            RunLoop.current.add(timer, forMode: RunLoop.Mode.common)
            timer.fire()
        }
    }
    
    func stopTimer() {
        if self.timer != nil && self.timer.isValid {
            self.timer.invalidate()
            self.timer = nil
        }
    }
    
    @objc func getMsgs() {
        if let p = playerPlayback {
            print("currentTime --- \(p.getOSDTime()), currentStreamType: \(p.getStreamFetchType())")
            
        }
    }
    
    func setRotatDirection(_ index: Int, _ length: Double) {
        
        // 操作命令：0-上，1-下，2-左，3-右，4-左上，5-左下，6-右上，7-右下，8-放大，9-缩小，10-近焦距，11-远焦距
        switch index {
        case 0:
            print("向上滑动");
            self.PTZParams["direction"] = 0
        case 1:
            print("向下滑动");
            self.PTZParams["direction"] = 1
        case 2:
            print("向左滑动");
            self.PTZParams["direction"] = 2
        case 3:
            print("向右滑动");
            self.PTZParams["direction"] = 3
        case 4:
            print("左上");
            self.PTZParams["direction"] = 4
        case 5:
            print("左下");
            self.PTZParams["direction"] = 5
        case 6:
            print("右上");
            self.PTZParams["direction"] = 6
        case 7:
            print("右下");
            self.PTZParams["direction"] = 7
        case 8:
            print("放大");
            self.PTZParams["direction"] = 8
        case 9:
            print("缩小");
            self.PTZParams["direction"] = 9
        case 10:
            print("近焦距");
            self.PTZParams["direction"] = 10
        case 11:
            print("远焦距");
            self.PTZParams["direction"] = 11
        
        default:
            break
        }

        
        ZGLiveManager.postGetAccessToken(url: kGetAccessToken, params: self.accessTokenParams, successClosure: { (accessToken) in

            self.PTZParams["accessToken"] = accessToken
            self.PTZParams["speed"] = 1
            ZGLiveManager.postStartPTZ(url: kVideoStartPTZ, params: self.PTZParams, successClosure: { (res) in
                
                if index != 10 && index != 11 {
                    DispatchQueue.main.asyncAfter(deadline: .now() + length) {
                        self.PTZParams.removeValue(forKey: "speed")
                        ZGLiveManager.postStopPTZ(url: kVideoStopPTZ, params: self.PTZParams, successClosure: { (res) in
                            
                        }) { (error) in
                            print(error)
                        }
                    }
                }
                
            }) { (error) in
                print(error)
            }

        }) { (error) in

        }
    }
    
    func initViews() {
        self.currentVC.view.addSubview(self.blanner!)
        
        self.currentVC.view.addSubview(self.middleView!)
        self.currentVC.view.bringSubviewToFront(self.middleView!)
        self.middleView!.blanner = blanner
        self.middleView!.showClosure = {[weak self] in
            if let weakSelf = self {
                weakSelf.aboveBtAction()
            }
        }
        
        self.middleView!.rotatClosure = {[weak self] (index, length) in
            if let weakSelf = self {
                EZOpenSDK.getDeviceInfo(weakSelf.deviceSerial) { (info, error) in
                    if info.isSupportPTZ == true {
                        weakSelf.setRotatDirection(index, length)
                    }
                }
            }
        }
        
        self.currentVC.view.addSubview(self.topView!)
        self.currentVC.view.bringSubviewToFront(self.topView!)
        self.topView!.backClosure = {[weak self] in
            if let weakSelf = self {
                weakSelf.backAction()
            }
        }
        self.topView!.beisuClosure = {[weak self] in
            if let `self` = self {
                self.beisuChoiceView?.isHidden = false
            }
        }
        self.currentVC.view.addSubview(self.beisuChoiceView!)
        self.beisuChoiceView!.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        self.beisuChoiceView!.beisuChoiceClosure = {[weak self] index in
            if let `self` = self {
                /*
                 EZOPENSDK_PLAY_RATE_1_16 = 9,  //以1/16倍速度播放
                 EZOPENSDK_PLAY_RATE_1_8 = 7,   //以1/8倍速度播放
                 EZOPENSDK_PLAY_RATE_1_4 = 5,   //以1/4倍速度播放
                 EZOPENSDK_PLAY_RATE_1_2 = 3,   //以1/2倍速播放
                 EZOPENSDK_PLAY_RATE_1 = 1,     //以正常速度播放
                 EZOPENSDK_PLAY_RATE_2 = 2,     //以2倍速播放
                 EZOPENSDK_PLAY_RATE_4 = 4,     //以4倍速度播放
                 EZOPENSDK_PLAY_RATE_8 = 6,     //以8倍速度播放
                 EZOPENSDK_PLAY_RATE_16 = 8,    //以16倍速度播放
                 EZOPENSDK_PLAY_RATE_32 = 10,   //以32倍速度播放
                 */
                DispatchQueue.main.async {
                    
                    switch index {
                    case 0:
                        let res = self.playerPlayback!.setPlaybackRate(EZPlaybackRate.EZOPENSDK_PLAY_RATE_1_8, mode: 0)
                        print("1/8倍速,\(res)")
                    case 1:
                        let res = self.playerPlayback!.setPlaybackRate(EZPlaybackRate.EZOPENSDK_PLAY_RATE_1_4, mode: 0)
                        print("1/4倍速,\(res)")
                    case 2:
                        let res = self.playerPlayback!.setPlaybackRate(EZPlaybackRate.EZOPENSDK_PLAY_RATE_1_2, mode: 0)
                        print("1/2倍速,\(res)")
                    case 3:
                        let res = self.playerPlayback!.setPlaybackRate(EZPlaybackRate.EZOPENSDK_PLAY_RATE_1, mode: 0)
                        
                        print("1倍速,\(res)")
                    case 4:
                        let res = self.playerPlayback!.setPlaybackRate(EZPlaybackRate.EZOPENSDK_PLAY_RATE_2, mode: 0)
                        print("2倍速,\(res)")
                    case 5:
                        let res = self.playerPlayback!.setPlaybackRate(EZPlaybackRate.EZOPENSDK_PLAY_RATE_4, mode: 0)
                        print("4倍速,\(res)")
                    case 6:
                        let res = self.playerPlayback!.setPlaybackRate(EZPlaybackRate.EZOPENSDK_PLAY_RATE_8, mode: 0)
                        print("8倍速,\(res)")
                    default:
                        break
                    }
                }
                
            }
        }
        
//        self.currentVC.view.addSubview(self.bottomView!)
//        self.currentVC.view.bringSubviewToFront(self.bottomView!)
//        self.bottomView!.clickedClosure = {[weak self] (index) in
//            if let weakSelf = self {
//                weakSelf.hideAboveViews()
//                switch index {
//                case 0:
//                    if let img = weakSelf.screenshot() {
//                        weakSelf.saveImgs(image: img)
//                    }
//                case 1:
//                    weakSelf.choosePhotos()
//                default:
//                    break
//                }
//            }
//        }
//        self.bottomView!.pressClosure = {[weak self] (ges) in
//            
//            if let weakSelf = self {
//                weakSelf.hideAboveViews()
//                if ges.state == UIGestureRecognizer.State.began {
//                    weakSelf.middleView?.voiceView.isHidden = false
//                    weakSelf.talkPlayer!.startVoiceTalk()
//                    weakSelf.recoder.startRecord()
//                } else if ges.state == UIGestureRecognizer.State.ended {
//                    weakSelf.talkPlayer!.stopVoiceTalk()
//                    weakSelf.recoder.stopRecord()
//                    weakSelf.middleView?.voiceView.isHidden = true
//                }
//            }
//        }
        self.showView(isFullScreen: false)
        self.hideAboveViews()
        topView!.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.right.equalToSuperview()
            make.height.equalTo(55)
        }
        middleView!.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
//        bottomView!.snp.makeConstraints { (make) in
//            make.bottom.equalToSuperview()
//            make.left.right.equalToSuperview()
//            make.height.equalTo(55)
//        }
    }
    
    // 实时
    func configureSDK(_ params: Dictionary<String, Any>) {
        
        isPlayback = false
        initViews()
        canTakePhoto = params["canPhoto"] as? Bool ?? false
        self.bottomView?.btnCamera.isHidden = !canTakePhoto
        self.bottomView?.btnCloudSpread.isHidden = !canTakePhoto
        let titleName: String = params["titleName"] as? String ?? ""
        let accessToken: String = params["accessToken"] as! String
        let appKey: String = params["appKey"] as! String
        let appSecret: String = params["appSecret"] as! String
        let cameraNo: String = params["cameraNo"] as! String
        self.cameraNo = Int(cameraNo)!
        let deviceSerial: String = params["deviceSerial"] as! String//"G02683661"//
        self.deviceSerial = deviceSerial
        let videoId: String = params["videoId"] as? String ?? ""
        let monitoryPointId: String = params["monitoryPointId"] as? String ?? ""
        let loginToken: String = params["loginToken"] as? String ?? ""
        
        self.picParams = ["videoId": videoId, "monitoryPointId": monitoryPointId, "description": "不文明行为", "token": loginToken]
        self.PTZParams = ["deviceSerial": deviceSerial, "channelNo": Int(cameraNo)!, "speed": 1]
        self.accessTokenParams = ["appKey": appKey, "appSecret": appSecret]
        self.topView?.backBt.setTitle(titleName, for: .normal)
        
        EZOpenSDK.initLib(withAppKey: appKey)
        EZOpenSDK.setAccessToken(accessToken)
        
        player = EZOpenSDK.createPlayer(withDeviceSerial: deviceSerial, cameraNo: Int(cameraNo)!)
        player!.delegate = self
        player!.setPlayerView(blanner)
        
        talkPlayer = EZOpenSDK.createPlayer(withDeviceSerial: deviceSerial, cameraNo: Int(cameraNo)!)
        talkPlayer!.delegate = self
        talkPlayer!.audioTalkPressed(false)
        self.topView!.beisuBt.isHidden = true
        self.startLiveVideo(true)
    }
    
    // 回放
    func configureSDKPlayback(_ params: Dictionary<String, Any>) {
        
        isPlayback = true
        initViews()
        let appKey: String = params["appKey"] as! String
        let accessToken: String = params["accessToken"] as! String
        let cameraNo: String = params["cameraNo"] as! String
        self.cameraNo = Int(cameraNo)!
        let deviceSerial: String = params["deviceSerial"] as! String//"G02683661"//
        let startDate: String = params["startTime"] as! String
        let stopDate: String = params["stopTime"] as! String
        EZOpenSDK.initLib(withAppKey: appKey)
        EZOpenSDK.setAccessToken(accessToken)
        playerPlayback = EZOpenSDK.createPlayer(withDeviceSerial: deviceSerial, cameraNo: Int(cameraNo)!)
        playerPlayback!.delegate = self
        playerPlayback!.setPlayerView(blanner)
        
        deviceFile.startTime = startDate.toDate(dateFormat: "yyyy-MM-dd HH:mm:ss")
        deviceFile.stopTime = stopDate.toDate(dateFormat: "yyyy-MM-dd HH:mm:ss")
        
        // 该序列号的设备能力集 判断是否支持 倍速回放
//        EZOpenSDK.probeDeviceInfo(deviceSerial, deviceType: nil) { (info, error) in
//            print("DeviceInfo -- \(info), error --- \(error)")
//            if info?.supportExt.contains("support_replay_speed") == true {
//                let dic: [String: Any] = info?.supportExt!.getDictionaryFromJSONString() as! [String : Any]
//                if (dic["support_replay_speed"] as! String) == "1" {
//                    self.topView!.beisuBt.isHidden = false
//                } else {
//                    self.topView!.beisuBt.isHidden = true
//                }
//            } else {
//                self.topView!.beisuBt.isHidden = true
//            }
//        }
        
        self.beisuChoiceView!.lastBeisu = 3
        self.beisuChoiceView!.tableView.reloadData()
        self.startLiveVideo(true)
    }
    
    func fullScreenAction() {
        self.totatingScreen(.landscapeRight)
    }
    func backAction() {
        self.totatingScreen(.portrait)
    }
    
    func hideAboveViews() {
        bottomView!.isHidden = true;
        topView!.isHidden = true;
    }
    
    func choosePhotos() {
        
        let tzvc = TZImagePickerController(maxImagesCount: 9, delegate: self)
        tzvc?.didFinishPickingPhotosHandle = {[weak self] (photos, assets, isOriginal) in
            var photoTimeArr = Array<String>()
            for (_, value) in assets!.enumerated() {
                let currentZoneDate = (value as! PHAsset).creationDate!
                photoTimeArr.append(currentZoneDate.format("yyyy-MM-dd HH:mm:ss"))
            }
            if let weakSelf = self {
                ZGLiveManager.postData(url: kImageUpload, params: nil, imageDatas: photos!, successClosure: { (res) in
                    
                    let resp = res as! Dictionary<String, Any>
                    let arr: Array<Dictionary<String, Any>> = resp["data"] as! Array<Dictionary<String, Any>>
                    var mArr = Array<Dictionary<String, String>>()
                    for (i, value) in arr.enumerated() {
                        var imgDict = Dictionary<String, String>()
                        let filePath: String = value["filePath"] as! String
                        let fileName: String = value["fileName"] as! String
                        imgDict["picPath"] = filePath + fileName
                        imgDict["captureDate"] = photoTimeArr[i]
                        mArr.append(imgDict)
                    }
                    weakSelf.picParams["pictureList"] = mArr
                    
                    ZGLiveManager.postInfo(url: kImageInfoUpload, params: weakSelf.picParams, successClosure: { (res) in
                        print(res)
                        print("上传成功")
                    }) { (error) in
                        print("上传失败")
                    }
                    
                }) { (error) in
                    
                }
                weakSelf.totatingScreen(UIInterfaceOrientation.landscapeRight)
            }
        }
        self.totatingScreen(UIInterfaceOrientation.portrait)
        self.currentVC.sg_present(tzvc!, animated: true)
    }
    
    func totatingScreen(_ ori: UIInterfaceOrientation) {
        
        let value = NSNumber(integerLiteral: ori.rawValue)
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    func aboveBtAction() {
        if self.bottomView!.isHidden {
            UIView.animate(withDuration: 0.3) {
                self.topView!.isHidden = false
                self.bottomView!.isHidden = false
            }
        } else {
            UIView.animate(withDuration: 0.3) {
                self.topView!.isHidden = true
                self.bottomView!.isHidden = true
            }
        }
    }
    
    func leaveVideoPage() {
        self.stopLiveVideo()
        self.currentVC.canAllButUpsideDown = false
        EZOpenSDK.release(player!)
        EZOpenSDK.release(talkPlayer!)
        EZOpenSDK.release(playerPlayback!)
        player = nil
        talkPlayer = nil
        playerPlayback = nil
        self.currentVC.view.hideToastActivity()
    }
    
    func showView(isFullScreen: Bool) {
        
        if !isFullScreen {
            topView!.isHidden = !isFullScreen
            bottomView!.isHidden = !isFullScreen
            if player != nil || playerPlayback != nil {
                leaveVideoPage()
            }
        }
        blanner!.isHidden = !isFullScreen
        middleView!.isHidden = !isFullScreen
        currentVC.webView.isHidden = isFullScreen
    }
    
    func startLiveVideo(_ firstIn: Bool = false) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let vc = self.currentVC, appDelegate.canAllButUpsideDown {
            let currentOri = UIDevice.current.value(forKey: "orientation") as! NSNumber
            if (currentOri == 3 || currentOri == 4 || firstIn) {
                self.currentVC.view.makeToastActivity("CSToastPositionCenter")
            }
            if isPlayback {
                playerPlayback!.startPlayback(fromDevice: deviceFile)
                
            } else {
                player!.startRealPlay()
            }
        }
    }
    
    func stopLiveVideo() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let vc = self.currentVC, appDelegate.canAllButUpsideDown {
            self.currentVC.view.hideToastActivity()
            player!.stopRealPlay()
        }
    }
    
//    func stopLiveVideo() {
//
//        if self.currentVC.canAllButUpsideDown {
//            self.currentVC.view.hideToastActivity()
//            if isPlayback {
//                playerPlayback!.stopPlayback()
//            } else {
//                player!.stopRealPlay()
//            }
//        }
//    }
    
    func reloadVideo() {
        self.stopLiveVideo()
        self.startLiveVideo()
    }
    
    func saveImgs(image: UIImage) {
        ZGCustomPhoto.saveImageInAlbum(image: image, albumName: "章广app") { (result) in
            
            DispatchQueue.main.async {
                switch result{
                case .success:
                    self.currentVC.view.makeToast("抓图成功，已保存至相册")
                case .denied:
                    self.currentVC.view.makeToast("抓图被拒绝")
                case .error:
                    self.currentVC.view.makeToast("抓图失败")
                }
            }
        }
    }
    
    //  播放器播放失败错误回调
    func player(_ player: EZPlayer!, didPlayFailed error: Error!) {
        print("player: \(player!), didPlayFailed: \(error!)")
        
        if self.maxCnt > 0 {
            self.reloadVideo()
            maxCnt -= 1
        } else {
            if (error.localizedDescription != "") {
                self.currentVC.view.makeToast(error.localizedDescription)
            } else {
                self.currentVC.view.makeToast("播放失败,请重试", duration: 1.5, position: CGPoint(x: kScreenHeight/2, y: kScreenWidth/2))
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                self.backAction()
            }
        }
    }
    
    /**
     *  播放器消息回调
     *
     *  @param messageCode 播放器消息码，请对照EZOpenSDK头文件中的EZMessageCode使用
     */
    func player(_ player: EZPlayer!, didReceivedMessage messageCode: Int) {
        print("messageCode: \(messageCode)")
        if messageCode == 1 {//直播开始
            self.currentVC.view.hideToastActivity()
        } else if messageCode == 11 {//回放开始
            self.currentVC.view.hideToastActivity()
        } else if messageCode == 12 {//回放结束
            self.currentVC.view.makeToast("回放结束", duration: 1.5, position: CGPoint(x: kScreenHeight/2, y: kScreenWidth/2))
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                self.backAction()
            }
        } else if messageCode == 21 {//播放器检测到wifi变换过
//            self.reloadVideo()
        }
    }
    
    /**
     *  收到的画面长宽值
     *
     *  @param height 高度
     *  @param width  宽度
     */
    func player(_ player: EZPlayer!, didReceivedDisplayHeight height: Int, displayWidth width: Int) {
        print("height: \(height), width: \(width)")
    }
    
    func getVoiceNum(num: Int) {
        var i = 0
        if num <= 1 {
            i = 0
        } else if num > 1 && num <= 2 {
            i = 1
        } else if num > 2 && num <= 3 {
            i = 2
        } else if num > 3 && num <= 4 {
            i = 3
        } else if num > 4 {
            i = 4
        }
        middleView?.voiceImage.image = UIImage(named: "voice\(i)")
    }
    
    
}

extension String {
    func toDate(dateFormat: String) -> Date? {
        
        let lastformat = DateFormatter()
        lastformat.dateFormat = dateFormat
        return lastformat.date(from: self)
    }
    
    func getDictionaryFromJSONString() -> [AnyHashable : Any] {
        
        let jsonData: Data = self.data(using: .utf8)!
        do {
            let dict = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers)
            guard (dict as? [AnyHashable : Any]) != nil else { return [:] }
            return dict as! [AnyHashable : Any]
        }
        catch(let error) {
            
            return [:]
        }
    }
}
