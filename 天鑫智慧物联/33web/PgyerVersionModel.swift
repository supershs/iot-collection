//
//  PgyerVersionModel.swift
//  33web
//
//  Created by 宋海胜 on 2020/12/4.
//

import UIKit
import HandyJSON

// https://www.pgyer.com/doc/view/api#appUpdate
struct PgyerVersionModel: HandyJSON {
    
    var buildBuildVersion: NSInteger? // 蒲公英生成的用于区分历史版本的build号
    var forceUpdateVersion: String? // 强制更新版本号（未设置强置更新默认为空）
    var forceUpdateVersionNo: String? // 强制更新的版本编号
    
    var needForceUpdate: Bool? // 是否强制更新
    var downloadURL: String? // 应用安装地址
    
    var buildHaveNewVersion: Bool? // 是否有新版本
    var buildVersionNo: String? // 上传包的版本编号，默认为1 (即编译的版本号，一般来说，编译一次会变动一次这个版本号, 在 Android 上叫 Version Code。对于 iOS 来说，是字符串类型；对于 Android 来说是一个整数。例如：1001，28等。)
    
    var buildVersion: String? // 版本号, 默认为1.0 (是应用向用户宣传时候用到的标识，例如：1.1、8.2.1等。)
    var buildShortcutUrl: String? // 应用短链接
    var buildUpdateDescription: String? // 应用更新说明
}

struct BaseModel: HandyJSON {
    var code: NSInteger?
    var message: String?
    var data: PgyerVersionModel?
}
//let jsonString = "{\"doubleOptional\":1.1,\"stringImplicitlyUnwrapped\":\"hello\",\"int\":1}"
//if let object = BasicTypes.deserialize(from: jsonString) {
//    // …
//}
