//
//  JDLoginViewController.swift
//  33web
//
//  Created by iOS开发 on 2023/3/9.
//

import UIKit
import SnapKit
import RxSwift
import RxAlamofire
import HandyJSON
import Toast
import SwiftyJSON
import Alamofire
import KeychainAccess
class JDLoginViewController: UIViewController,UITextFieldDelegate {
//    let disposeBag = DisposeBag()
    let disposBag = DisposeBag()
    var showAll : Bool!
    let minUsernameLength = 1
    let maxUsernameLength = 11
    let minPasswordLength = 1
    let maxPasswordLength = 30
    var TSKeychain: Keychain! = Keychain()
    
    var usernameTF: UITextField = {
        let tf = UITextField()
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.textColor = .black
       
        tf.placeholder = "请输入账号"
        return tf
    }()
    

    var passwordTF: UITextField = {
        let tf = UITextField()
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.textColor = .black
        tf.placeholder = "请输入密码"
        tf.isSecureTextEntry = true
        return tf
    }()
    
    public var rememberBtn : UIButton = {
        let btn = UIButton()
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        btn.setTitleColor(UIColor(hex: "888888"), for: .normal)
        btn.contentHorizontalAlignment = .right
        btn.setTitle("记住密码", for: .normal)
        return btn
    }()
    
    public var rememberButton : UIButton = {
        let bt = UIButton()
        bt.setImage(UIImage(named: "select_false"), for: .normal)
        return bt
    }()
    
  
    var loginButton: UIButton = {
        let v = UIButton()
        v.backgroundColor = UIColor.white
        v.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        v.setTitleColor( .white, for: .normal)
        v.backgroundColor = UIColor(hex: "#1683e9")
        v.setBackgroundImage(UIImage(named: "login_btn"), for: .normal)
        v.setTitle("登录", for: .normal)
        v.layer.cornerRadius = 5
        v.layer.masksToBounds = true
        return v
    }()
    
    var eyeButton: UIButton = {
        let v = UIButton()
        v.setBackgroundImage(UIImage(named: "show"), for: .normal)
        return v
    }()
    
    var logoImgView: UIImageView = {
        let v = UIImageView()
        v.image = UIImage(named: "login_head")
        return v
    }()
    var accountImgView: UIImageView = {
        let v = UIImageView()
        v.image = UIImage(named: "account")

        return v
    }()
    var passwordImgView: UIImageView = {
        let v = UIImageView()
        v.image = UIImage(named: "password")
        return v
    }()
    var accountSepView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.init(hex: "f5f5f5")
        return v
    }()
    var passwordSepView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.init(hex: "f5f5f5")
        return v
    }()
    

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showAll = false
        view.addSubview(logoImgView)
        view.addSubview(accountImgView)
        view.addSubview(passwordImgView)
        view.addSubview(passwordTF)
        view.addSubview(usernameTF)
        view.addSubview(accountSepView)
        view.addSubview(passwordSepView)
        view.addSubview(rememberBtn)
        view.addSubview(rememberButton)
        view.addSubview(loginButton)
        view.addSubview(eyeButton)
        usernameTF.delegate = self
        passwordTF.delegate = self
        loginButton.addTarget(self, action: #selector(logOnClick), for: .touchUpInside)
        eyeButton.addTarget(self, action: #selector(lookBtnClick), for: .touchUpInside)
        rememberButton.addTarget(self, action: #selector(rememberBtnClick), for: .touchUpInside)
        
        //注册点击事件
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
        
        view.backgroundColor = .white
        logoImgView.snp.makeConstraints { make in
            make.top.left.right.equalToSuperview()
            make.height.equalTo(323)
        }

        accountImgView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(25)
            make.top.equalTo(logoImgView.snp.bottom).offset(51)
            make.width.equalTo(18)
            make.height.equalTo(22)
        }
      
        usernameTF.snp.makeConstraints { make in
            make.left.equalTo(accountImgView.snp.right).offset(10)
            make.top.equalTo(logoImgView.snp.bottom).offset(43)
            make.right.equalToSuperview().offset(-25)
            make.height.equalTo(40)
        }
    
        accountSepView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(25)
            make.top.equalTo(usernameTF.snp.bottom).offset(1)
            make.right.equalToSuperview().offset(-25)
            make.height.equalTo(1)
        }
        
        passwordImgView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(25)
            make.top.equalTo(accountSepView.snp.bottom).offset(60)
            make.width.equalTo(18)
            make.height.equalTo(20)
        }
        
        passwordTF.snp.makeConstraints { make in
            make.left.equalTo(passwordImgView.snp.right).offset(10)
            make.top.equalTo(accountSepView.snp.bottom).offset(50)
            make.right.equalToSuperview().offset(-25)
            make.height.equalTo(40)
        }
        
        eyeButton.snp.makeConstraints { make in
            make.right.equalTo(passwordTF.snp.right).offset(10)
            make.centerY.equalTo(passwordTF)
            make.height.equalTo(7)
            make.width.equalTo(15)
        }
     
        passwordSepView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(25)
            make.top.equalTo(passwordTF.snp.bottom).offset(1)
            make.right.equalToSuperview().offset(-25)
            make.height.equalTo(1)
        }
        
        rememberBtn.snp.makeConstraints { make in
            make.top.equalTo(passwordSepView.snp.bottom).offset(10)
            make.left.equalTo(passwordSepView.snp.left).offset(5)
            make.width.equalTo(70)
            make.height.equalTo(30)
        }
        rememberButton.snp.makeConstraints { make in
            make.centerY.equalTo(rememberBtn)
            make.right.equalTo(rememberBtn.snp.left).offset(10)
            make.width.equalTo(15)
            make.height.equalTo(15)
        }
        
        
        loginButton.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(25)
            make.top.equalTo(passwordSepView.snp.bottom).offset(55)
            make.right.equalToSuperview().offset(-25)
            make.height.equalTo(44)
        }
    }
    
    //    MARK: -  记住密码
    @objc func rememberBtnClick(){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        print("appDelegate === ",appDelegate.record)
        if appDelegate.record == "1"{
            print("忘记密码灰色",appDelegate.record)
            appDelegate.record = "0"
            rememberButton.setImage(UIImage(named: "select_false"), for: .normal)
             
        }else{
            print("忘记密码亮色",appDelegate.record)
            appDelegate.record = "1"
            rememberButton.setImage(UIImage(named: "select_true"), for: .normal)
            
        }
    }
    
//    //    MARK: - 登录
    @objc func logOnClick(){

        if usernameTF.text!.isEmpty {
            TSProgressHUD.ts_showWarningWithStatus("请输入手机号")
        }else if passwordTF.text!.isEmpty {
            TSProgressHUD.ts_showWarningWithStatus("请输入密码")
        }else{

            goLogin(loginName: "", password: "")

        }
    }
    
    
    
    
    func goLogin(loginName: String, password: String){
//        let parameters  = ["loginName":usernameTF.text!,"password":AESCode.endcode_AES_CBC(strToEncode: passwordTF.text!),"url":appURL]
        let parameters  = ["loginName":usernameTF.text!,"password":AESCode.endcode_AES_CBC(strToEncode: passwordTF.text!),"url":appURL] as [String : Any]
        RxAlamofire.requestJSON(.post, URL(string: logoURL)!, parameters: parameters, encoding: JSONEncoding.default, headers: ["Content-Type":"application/json;charset=UTF-8"])
                  .debug()
                                                                                                        
                  .subscribe(onNext: { (r, json) in
                    if let dict = json as? [String: AnyObject] {
                        
                        print(">>>>>>>>>>>>>>>>>>>dict>>>>>>>>>>>>>>>>>>>>>>",json)
                     
                        let modelA = NSRootModel<LoginModel>.deserialize(from: dict)
                        if modelA?.status == "SUCCESS"  {
                            
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            if appDelegate.record == "0"{
                                
                                do {
                                    try self.TSKeychain.removeAll()
                                } catch {
                                    print("An error occurred: \(error)")
                                }
                                
                                self.TSKeychain["username"] = self.usernameTF.text!
                            }else{
                                // 将值存储到 Keychain 中
//                                try TSKeychain.removeAll()
                                self.TSKeychain["username"] = self.usernameTF.text!
                                self.TSKeychain["password"] = self.passwordTF.text!
                                print("将值存储到 Keychain 中")
                            }
                            
                            print(">>>>>>>>>>>>>>>>>>>自己的打印>>>>>>>>>>>>>>>>>>>>>>")
                            let urlStr =  GOTOHOME  + (modelA?.data?.toJSONString() ?? "")
                            self.navigationController?.pushViewController(HSWebViewController(path: kWebUrl + urlStr.getEncodeString), animated: true)

                        }else{
                            TSProgressHUD.ts_showWarningWithStatus(modelA?.message ?? "")
                            }

                        }
                      
                  }, onError: { (error) in
                        print(error,"=====error")
                  })
                  .disposed(by: disposBag)
    }
    
    //    MARK: -  查看密码
    @objc func lookBtnClick(){
       
        if showAll{
            showAll = false
            passwordTF.isSecureTextEntry = true
        }else{
            showAll = true
            passwordTF.isSecureTextEntry = false
        }
        
    }
    
    //点击空白处关闭键盘方法
    @objc func handleTap(sender: UITapGestureRecognizer) {
           if sender.state == .ended {
               print("收回键盘")
               self.usernameTF.resignFirstResponder()//username放弃第一响应者
               self.passwordTF.resignFirstResponder()//password放弃第一响应者
           }
           sender.cancelsTouchesInView = false
       }
    
    //textField点击return关闭键盘   
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view?.endEditing(false)
        return true
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if appDelegate.record == "0"{
            usernameTF.text = ""
            passwordTF.text = ""
        }else{
            usernameTF.text = TSKeychain["username"]
            passwordTF.text = TSKeychain["password"]
        }
        
    }
                                     
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
        
    }

}

extension String {
    /// String转encode
        var getEncodeString: String {
            guard self.count != 0 else { return ""}
            if let u = self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                return u
            }
            return ""
        }
}
