//
//  Tool.swift
//  XUYIProject
//
//  Created by 叁拾叁 on 2020/6/24.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit
import WebKit
import AVKit
import Photos
import Alamofire

public let kScreenHeight: CGFloat = UIScreen.main.bounds.size.height

public let kScreenWidth: CGFloat = UIScreen.main.bounds.size.width

public let isPhone = Bool(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone)

public let isPad = Bool(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad)

public let isPhoneX = Bool(kScreenWidth >= 375.0 && kScreenHeight >= 812.0 && isPhone)

public let kNavigationHeight = CGFloat(isPhoneX ? 88 : 64)

public let kStatusBarHeight = CGFloat(isPhoneX ? 44 : 20)

public let kTabBarHeight = CGFloat(isPhoneX ? (49 + 34) : 49)

public let kTopSafeHeight = CGFloat(isPhoneX ? 44 : 0)

public let kBottomSafeHeight = CGFloat(isPhoneX ? 34 : 0)

public class Tool: NSObject {
    
    public func judgeNetworkState(_ vc: UIViewController) -> Bool {
        
        let manager = NetworkReachabilityManager()
        if manager?.isReachable == false {
            let alertView = UIAlertController.init(title: "提示", message: "请到设置中允许APP使用网络", preferredStyle: .alert)
            let okAction = UIAlertAction.init(title:"确定", style: .default) { okAction in

            }
            alertView.addAction(okAction)
            vc.present(alertView, animated: true, completion: nil)
            return false
        } else {
            return true
        }
    }
    
    public func alertToReload(_ vc: UIViewController, okActionClosure: @escaping() -> Void) {
        /// 弹出提示框点击确定返回
        let alertView = UIAlertController.init(title: "提示", message: "加载失败，点击确定重新加载", preferredStyle: .alert)
        let okAction = UIAlertAction.init(title:"确定", style: .default) { okAction in
            okActionClosure()
        }
        alertView.addAction(okAction)
        vc.present(alertView, animated: true, completion: nil)
    }

    /// 打电话
    public func callSomeOne(_ tel: String) {
        let telprompt = "telprompt:\(tel)"
        if let url = URL(string: telprompt) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)//可以写拨打电话的回调
            } else {
                if UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    public func topMangain(webView: WKWebView, vc: UIViewController) {
        // 解决Webview未覆盖顶部状态栏
        if #available(iOS 11.0, *) {
            webView.scrollView.contentInsetAdjustmentBehavior = UIScrollView.ContentInsetAdjustmentBehavior.never
        } else {
            vc.edgesForExtendedLayout = UIRectEdge.bottom
        }
    }
    
    // launchscreen方式获取
    public func getLaunchImage() -> UIImageView {
        let viewSize = UIScreen.main.bounds.size
        let viewOr = "Portrait"//垂直
        var launchImageName = ""
        let tmpLaunchImages = Bundle.main.infoDictionary!["UILaunchImages"] as? [Any]
        for dict in tmpLaunchImages! {
            if let someDict = dict as? [String: Any] {

                let imageSize: CGSize = CGSizeFromString(someDict["UILaunchImageSize"] as! String)
                
                if __CGSizeEqualToSize(viewSize, imageSize) && viewOr == someDict["UILaunchImageOrientation"] as! String {
                    
                    // 因为iphone11、iphonexr 跟 iphonexs max、iphone11pro max 尺寸一样
                    if let size = UIScreen.main.currentMode?.size {
                        
                        if __CGSizeEqualToSize(CGSize(width: 1242, height: 2688), size) {
                            // iPhoneXs Max
                            if let name = someDict["UILaunchImageName"] as? String, name.contains("2688") {
                                launchImageName = someDict["UILaunchImageName"] as! String
                            }
                            
                        } else if __CGSizeEqualToSize(CGSize(width: 1242, height: 2688), size) {
                            // iPHoneXr
                            if let name = someDict["UILaunchImageName"] as? String, name.contains("1792") {
                                launchImageName = someDict["UILaunchImageName"] as! String
                            }
                        } else {
                            launchImageName = someDict["UILaunchImageName"] as! String
                        }
                    }
                }
            }
        }
        let imageView = UIImageView()
        imageView.image = UIImage(named: launchImageName)
        return imageView
    }
    
    public func getLanuchboardImage() -> UIImageView {
        
        let vc: UIViewController = UIStoryboard(name: "LaunchScreen", bundle: Bundle.main).instantiateInitialViewController()!
        let rect = UIScreen.main.bounds
        UIGraphicsBeginImageContextWithOptions(rect.size, true, 0.0)
        let context = UIGraphicsGetCurrentContext()
        vc.view.frame = rect
        vc.view.layer.render(in: context!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let imageV = UIImageView()
        imageV.image = image!
        return imageV
    }
    
    // 请求加入相册权限
    class public func requestAuthorizationPhotoLibary() {
        PHPhotoLibrary.requestAuthorization { (status) in
            
        }
    }
    
    // 请求加入麦克风权限
    class public func requestMicroPhoneAuth() {
        AVCaptureDevice.requestAccess(for: .audio) { (granted) in
            
        }
    }
    
    // 线路规划
    class public func routePlanning(_ body: [String: Any]) {
        // 纬度 Latitude  经度 Longitude  南京北纬32 东经119
        let startLatStr: String = body["startLat"]! as! String
        let startLonStr: String = body["startLon"]! as! String
        let startLat: Double = Double(startLatStr)!
        let startLon: Double = Double(startLonStr)!
        
        let endLatStr: String = body["endLat"]! as! String
        let endLonStr: String = body["endLon"]! as! String
        let endLat: Double = Double(endLatStr)!
        let endLon: Double = Double(endLonStr)!
        
        let startAddressStr: String = body["startAddress"]! as! String
        let endAddressStr: String = body["endAddress"]! as! String
        
        let urlString: String = String(format: "iosamap://path?sourceApplication=applicationName&sid=&slat=%.6f&slon=%.6f&sname=%@&did=&dlat=%.6f&dlon=%.6f&dname=%@&dev=0&t=0", startLat, startLon, startAddressStr, endLat, endLon, endAddressStr)
        
        // swift 必须要有这个
        let urlSSSS = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: urlSSSS!)
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        } else {
            
        }
    }
}



extension UIViewController {

    public func sg_present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        viewControllerToPresent.modalPresentationStyle = UIModalPresentationStyle.fullScreen
        self.present(viewControllerToPresent, animated: flag, completion: completion)
    }
}

extension Date {
    public func format(_ dateFormat: String, localId: String = "zh_CN") -> String {
        let df = DateFormatter()
        df.locale = Locale(identifier: localId)
        df.dateFormat = dateFormat
        let str = df.string(from: self)
        return str
    }
    
    /// 根据本地时区转换
    public func getCurrentZoneDateFromAnyDate() -> Date {
        
        //设置源日期时区
        let sourceTimeZone = NSTimeZone(abbreviation: "UTC")
        //或GMT
        //设置转换后的目标日期时区
        let destinationTimeZone = NSTimeZone.local as NSTimeZone
        //得到源日期与世界标准时间的偏移量
        var sourceGMTOffset: Int? = nil
        sourceGMTOffset = sourceTimeZone?.secondsFromGMT(for: self)
        //目标日期与本地时区的偏移量
        var destinationGMTOffset: Int? = nil
        destinationGMTOffset = destinationTimeZone.secondsFromGMT(for: self)
        //得到时间偏移量的差值
        let interval = TimeInterval((destinationGMTOffset ?? 0) - (sourceGMTOffset ?? 0))
        //转为现在时间
        var destinationDateNow: Date? = nil
        destinationDateNow = Date(timeInterval: interval, since: self)
        return destinationDateNow!
    }
}

extension UIView {
    // 截屏
    public func screenshot() -> UIImage? {
        
        guard let window = UIApplication.shared.keyWindow else { return nil }
        // 用下面这行而不是UIGraphicsBeginImageContext()，因为前者支持Retina
        UIGraphicsBeginImageContextWithOptions(window.bounds.size, false, 0.0)
        window.drawHierarchy(in: window.frame, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}
