//
//  ZGLiveProtocol.swift
//  ZhangGuang
//
//  Created by 叁拾叁 on 2020/7/20.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit

protocol ZGLiveProtocol: class {// 将protocol限制在class内 这样才能使用weak，因为swift大都是struct类型，另一种方法是加@objc
    
    func getGPSSuccess(latitude: Double, longitude: Double)
    
    func getGPSFailure(error: Error)
    
}
