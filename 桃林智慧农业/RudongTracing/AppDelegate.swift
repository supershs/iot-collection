//
//  AppDelegate.swift
//  RudongTracing
//
//  Created by JAY on 2023/5/17.

import UIKit
//let kWebUrl = "http://114.115.238.249:9096/#/"
let kWebUrl = "http://114.115.238.249:9097/#/"

//let kWebUrl = "http://192.168.0.80:8080/#/"
let GOTOHOME = "pages/base-index/index?data="

//账号登录
let accountUrl = "http://114.115.238.249:9057/api/1.0/SysLogin/password/login"
//验证码登录
let codeLoginUrl = "http://114.115.238.249:9057/api/1.0/SysLogin/message/push"
//参数url
let paraUrl = "124.70.188.151:9095"

//忘记密码
let forgetUrl  = "pages/base-login/resetPassword"
 

//忘记密码验证码
let CodeURL = "http://114.115.238.249:9060/api/1.0/SysLogin/message/push"
//忘记密码修改成功
let ChangePasswordUrl = "http://114.115.238.249:9060/api/1.0/SysLogin/forget/password"

//注册验证码
let registerURL = "http://114.115.148.40:9040/api/1.0/AppLogin/message/push/register"
//注册成功
let successfulURL = "http://114.115.148.40:9040/api/1.0/AppLogin/app/register"

let agreementURL = "pages/base-login/userAgreement"
@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var webVC = HSWebViewController(path: kWebUrl)
    var canAllButUpsideDown: Bool = false
    var loginVC = LogOnViewController() 
    var record :  Bool = false
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
//        //设置用户授权显示通知
//        if #available(iOS 10.0, *) {
//            UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.badge,.carPlay,.sound], completionHandler: { (success, error) in
//                print("通知授权" + (success ? "成功" : "失败"))
//            })
//        }
        
        window = UIWindow(frame: UIScreen.main.bounds)
        let nav = ZXNavigationController (rootViewController: loginVC)
        self.window?.rootViewController = nav
        window!.makeKeyAndVisible()
        
        
        // 强制关闭暗黑模式
        if #available(iOS 13.0, *) {
            self.window?.overrideUserInterfaceStyle = .light
        }
        #if DEBUG // 判断是否在测试环境下
        print("当前环境：DEBUG")
        #else
        print("当前环境：RELEASE")
        #endif
        return true
    }
    

}

