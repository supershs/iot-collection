//
//  SYRequestConstant.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2020/8/6.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit

class SYRequestConstant: NSObject {
    
    #if DEBUG
    
    @objc static let api = "https://www.map8.com/"
    
    #else
    
    @objc static let api = "https://www.map8.com/"
    
    #endif
    
    /// 检查app的最新版本号（仅限正式服）
    static let APP_DETECTION_VERSION = "static/appDetectionVersion"
}
