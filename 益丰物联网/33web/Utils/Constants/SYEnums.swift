//
//  VGEnums.swift
//  vgbox
//
//  Created by 宋海胜 on 2019/6/26.
//  Copyright © 2019 Swift Xcode. All rights reserved.
//  公共枚举

import UIKit

///人物属性
enum Gender: String {
    // MARK: - sign
    /// 男性
    case man = "01"
    /// 女性
    case woman = "02"
    
    // MARK: - description
    /// 男性
    case man_Des = "男"
    /// 女性
    case woman_Des = "女"
    
    static func getGenderFrom(_ sign: String) -> String {
        switch sign {
        case man.rawValue: return man_Des.rawValue
        default: return woman_Des.rawValue
        }
    }
    
    static func getGenderSignBy(_ genderDes: String) -> String {
        switch genderDes {
        case man_Des.rawValue: return man.rawValue
        case woman_Des.rawValue: return woman.rawValue
        default: return ""
        }
    }
}

////操作结果枚举
//enum PhotoAlbumUtilResult {
//    case success, error, denied
//}
