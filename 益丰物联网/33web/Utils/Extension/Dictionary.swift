//
//  Dictionary.swift
//  vgbox
//
//  Created by 宋海胜 on 2019/6/25.
//  Copyright © 2019 Swift Xcode. All rights reserved.
//

import Foundation

extension Dictionary {
    
    // 获取一个随机值
    func random() -> Value? {
        return Array(values).randomElement()
    }
    
    /// 是否存在该key
    func has(_ key: Key) -> Bool {
        return index(forKey: key) != nil
    }
    
    /// 将不存在的键值对加进来。已存在的进行值得更新
    public func union(_ dictionaries: Dictionary...) -> Dictionary {
        var result = self
        dictionaries.forEach { (dictionary) -> Void in
            dictionary.forEach { (key, value) -> Void in
                result[key] = value
            }
        }
        return result
    }
    
    ///
    public func difference(_ dictionaries: [Key: Value]...) -> [Key: Value] {
        var result = self
        for dictionary in dictionaries {
            for (key, value) in dictionary {
                if result.has(key) && result[key] == value as! _OptionalNilComparisonType {
                    result.removeValue(forKey: key)
                }
            }
        }
        return result
    }
    
    /// 转换字典键值对的类型
    public func map<K, V>(_ map: (Key, Value) -> (K, V)) -> [K: V] {
        var mapped: [K: V] = [:]
        forEach {
            let (_key, _value) = map($0, $1)
            mapped[_key] = _value
        }
        return mapped
    }
    
    
    /// 将JSONString转换成Dictionary
    public static func constructFromJSON (json: String) -> Dictionary? {
        if let data = (try? JSONSerialization.jsonObject(
            with: json.data(using: String.Encoding.utf8,
                            allowLossyConversion: true)!,
            options: JSONSerialization.ReadingOptions.mutableContainers)) as? Dictionary {
            return data
        } else {
            return nil
        }
    }
    
    /// 转换成JSON
    func formatJSON() -> String? {
        if let jsonData = try? JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions()) {
            let jsonStr = String(data: jsonData, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
            return String(jsonStr ?? "")
        }
        return nil
    }
    
}

extension Dictionary {//where Key: ExpressibleByStringLiteral, Value: Any {
    
  var showJsonString: String {
        do {
//            var dic: [String: Any] = [String: Any]()
//            for (key, value) in self {
//                dic["\(key)"] = value
//            }
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)

            if let data = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String? {
                return data
            } else {
                return "{}"
            }
        } catch {
            return "{}"
        }
    }
}

extension Double {
    
    /// 小数点后如果只是0，显示整数，如果不是，显示原来的值
    var cleanZero : String {
        if self.truncatingRemainder(dividingBy: 1) == 0 {
            return String(format: "%.0f", self)
        } else {
            return String(self)
        }
    }
    
    var saveTwo: Double {
        
        // 先保留2位小数
        let tempSelf = String(format: "%.2f", self)
        return tempSelf.doubleValue
    }
}
