//
//  NSMutableAttributedString.swift
//  vgbox
//
//  Created by 宋海胜 on 2019/6/26.
//  Copyright © 2019 Swift Xcode. All rights reserved.
//

import Foundation


/// 改变字体大小颜色的方法 如果不改变传nil
func getAttrituteText(text: String, font: CGFloat?, color: UIColor?, range: NSRange) -> NSMutableAttributedString {
    
    let attristr = NSMutableAttributedString(string: text)
    
    if let f = font {
        attristr.addAttributes(convertToNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont.systemFont(ofSize: f)]), range: range)
    }
    
    if let c = color {
        attristr.addAttributes(convertToNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : c]), range: range)
    }
    return attristr
}

///字体颜色 + 加粗
func getAttrituteBoldText(text: String, font: CGFloat?, color: UIColor?, range: NSRange) -> NSMutableAttributedString {
    
    let attristr = NSMutableAttributedString(string: text)
    
    if let f = font {
        attristr.addAttributes(convertToNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont.boldSystemFont(ofSize: f)]), range: range)
    }
    
    if let c = color {
        attristr.addAttributes(convertToNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : c]), range: range)
    }
    return attristr
}


///改变部分字体大小
func getAttributeFont(text:String, font: CGFloat, range: NSRange) -> NSMutableAttributedString {
    
    let attristr = NSMutableAttributedString(string: text)
    
    attristr.addAttributes(convertToNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont.systemFont(ofSize: font)]), range: range)
    
    return attristr
}

///改变部分字体颜色
func getAttributeColor(text: String, color: UIColor, range: NSRange) -> NSMutableAttributedString {
    let attristr = NSMutableAttributedString(string: text)
    attristr.addAttributes(convertToNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): color]), range: range)
    
    return attristr
}

///部分文字加粗
func getAttributeBold(text: String, font: CGFloat, range: NSRange) -> NSMutableAttributedString {
    let attristr = NSMutableAttributedString(string: text)
    attristr.addAttributes(convertToNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.font): UIFont.boldSystemFont(ofSize: font)]), range: range)
    
    return attristr
}

///改变字体间距 设置字体间距 比如 “你好” 间距为12 他的距离是 “你 好 ” 或者 " 你 好" 需要设置其余空间的宽度
func getAttributeSpacing(text: String, spacing: CGFloat, range: NSRange) -> NSMutableAttributedString {
    let attristr = NSMutableAttributedString(string: text)
    attristr.addAttributes(convertToNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.kern): spacing]), range: range)
    return attristr
}

//改变字体行间距
func getAtributelinespacing(text:String,linespacing:CGFloat,range:NSRange) -> NSMutableAttributedString {
    let attristr = NSMutableAttributedString(string:text)
    let style = NSMutableParagraphStyle()
    style.lineSpacing = linespacing
    attristr.addAttributes(convertToNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.paragraphStyle) : style]), range: range)
    return attristr
}




// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToNSAttributedStringKeyDictionary(_ input: [String: Any]) -> [NSAttributedString.Key: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}
