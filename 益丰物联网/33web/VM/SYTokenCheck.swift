//
//  SYTokenCheck.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2021/3/29.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit

class SYTokenCheck: NSObject {
    
    class func toLoginVC() {
        if UserInstance.accessToken == "" {
            return
        }
        UserInstance.userLogout()
        let loginVC = HSWebViewController(path: WEBIP)
        if let currentVC = currentViewController() {
            if !currentVC.isKind(of: HSWebViewController.self) {
                DispatchQueue.main.async {
                    currentVC.vg_present(loginVC, animated: true)
                }
            }
        }
    }
    
    
    class func toIdcardVC() {
        UserInstance.userLogout()
//        let loginVC = SYIdcardRecognizeViewController()
//        if let currentVC = currentViewController() {
//            if !currentVC.isKind(of: SYIdcardRecognizeViewController.self) {
//                DispatchQueue.main.async {
//                    currentVC.vg_present(loginVC, animated: true)
//                }
//            }
//        }
    }
    

    class func currentViewController() -> (UIViewController?) {
       var window = UIApplication.shared.keyWindow
       if window?.windowLevel != UIWindow.Level.normal{
         let windows = UIApplication.shared.windows
         for  windowTemp in windows{
           if windowTemp.windowLevel == UIWindow.Level.normal{
              window = windowTemp
              break
            }
          }
        }
       let vc = window?.rootViewController
       return currentViewController(vc)
    }


    class func currentViewController(_ vc :UIViewController?) -> UIViewController? {
       if vc == nil {
          return nil
       }
       if let presentVC = vc?.presentedViewController {
          return currentViewController(presentVC)
       }
       else if let tabVC = vc as? UITabBarController {
          if let selectVC = tabVC.selectedViewController {
              return currentViewController(selectVC)
           }
           return nil
        }
        else if let naiVC = vc as? UINavigationController {
           return currentViewController(naiVC.visibleViewController)
        }
        else {
           return vc
        }
     }
}
