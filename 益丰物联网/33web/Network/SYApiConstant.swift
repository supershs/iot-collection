//
//  Constant_API.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2020/8/11.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit

let VERSION = "/api/1.0/"

var WEBIP = "http://114.115.166.192:9112/#/"
//var WEBIP =  "http://192.168.2.147:8081/#/"
var IMGIP = "http://114.115.166.192:9110/api/1.0"
var IP = "http://114.115.166.192:9110"//
var sysTenantDTOURL = ""

let testMP3 = "http://downsc.chinaz.net/Files/DownLoad/sound1/201906/11582.mp3"
let testMP4 = "http://static.tripbe.com/videofiles/20121214/9533522808.f4v.mp4"

let REQUEST_DIC = "requestDictionary"
let GOTOHOME = "pages/wisdomIot/tabBar/homePage"

// MARK: - tabbar
// 登录
let LOGIN = "AppLogin/password/login"
//忘记

//忘记密码获取验证码
let CODE = "AppLogin/message/push/login"
//忘记密码登录
let FORGETPASSWORD = "SysUser/sc/password/byCode"
//注册忘记密码获取验证吗
let  REGISTERCODE = "AppLogin/message/push/register"
//注册
let REGIStER = "AppLogin/app/register"

let CODELOGIN = "SysLogin/message/login"


// 身份识别
let SHENFEN_RECOGNIZE = "SysUser/app/certification"

// MARK: - 病虫害
// 病害图像识别接口
let SHIBIE_BING = "DiseaseDiscernLog/sc/detect/plant"

// 虫害图像识别接口
let SHIBIE_CHONG = "DiseaseDiscernLog/sc/detect/pest"

// 病虫害查询我的识别历史
let SHIBIE_LISHI = "DiseaseDiscernLog/app/myself"
