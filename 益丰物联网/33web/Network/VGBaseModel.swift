//
//  VGBaseListModel.swift
//  vgbox
//
//  Created by 宋海胜 on 2019/6/30.
//  Copyright © 2019 宋海胜. All rights reserved.
//  列表页基础类 - data数组中 model类

import UIKit
import HandyJSON

struct SGBaseModel<T: HandyJSON>: HandyJSON {
    
    var status: String?
    var code: Int?
    var message: String?
    var date: String?
    var token: String?
    var data: T?

}

struct SGBaseNoPageListModel<T: HandyJSON>: HandyJSON {
    
    var status: String?
    var code: Int?
    var message: String?
    var date: String?
    var data: [T]?

}

struct SGBaseListModel<T: HandyJSON>: HandyJSON {
    
    var status: String?
    var code: Int?
    var message: String?
    var date: String?
    var data: SGPagerModel<T>?

}

struct SGPayModel: HandyJSON {
    
    var status: String?
    var code: Int?
    var message: String?
    var date: String?
    var token: String?
    var data: String?

}

struct SGPagerModel<T: HandyJSON>: HandyJSON {

    var size: Int?
    var current: Int?
    var pages: Int?
    var total: Int?
    var records: [T]?
}

struct SGPagerTestModel: HandyJSON, Equatable
{
    var test: String?
    var id: Int = 0
}


struct SGSimpleModel: HandyJSON, Equatable
{
    
    var id: String?
    var imgUrl: String?
}



