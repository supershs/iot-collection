//
//  SYObservable+Toast.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2020/8/25.
//  Copyright © 2020 叁拾叁. All rights reserved.
//
#if DEBUG
    import Foundation
#endif
import UIKit
import RxSwift

class SYObservable_Toast: NSObject {

}


 extension ObservableType {
     /**
      Subscribes an event handler to an observable sequence.
      
      - parameter on: Action to invoke for each event in the observable sequence.
      - returns: Subscription object used to unsubscribe from the observable sequence.
      */
     public func sy_subscribe(_ on: @escaping (Event<Element>) -> Void)
         -> Disposable {
            
            self.subscribe(on)
     }
     
 }


