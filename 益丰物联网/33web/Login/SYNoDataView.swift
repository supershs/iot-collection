//
//  SYNoDataView.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2020/9/1.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit
import YYText

class SYNoDataView: SYBaseView {

    var imgHeight: CGFloat = 150
    let imgView: UIImageView = {
        let v = UIImageView()
        v.sy_name("no_data")
        return v
    }()
    
    let tipText: UILabel = {
        let v = UILabel()
        v.text = "这里什么都没有哦"
        v.font = UIFont.systemFont(ofSize: 18)
        v.textColor = UIColor(hex: 0x6F6F6F)
        return v
    }()
    
    let subTipText: UILabel = {
        let v = UILabel()
        v.text = "去首页看看有没有你感兴趣的吧"
        v.font = UIFont.systemFont(ofSize: 14)
        v.textColor = UIColor(hex: 0x808080)
        return v
    }()
    
    let btn: UIButton = {
        let v = UIButton()
        v.backgroundColor = .yellow
        v.setTitle("刷新", for: .normal)
        v.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        v.setTitleColor(.white, for: .normal)
        v.layer.cornerRadius = 5
        v.layer.masksToBounds = true
        return v
    }()
    
    override func initViews() {
        
        addSubview(imgView)
        addSubview(tipText)
        addSubview(subTipText)
        addSubview(btn)
        imgView.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(imgHeight)
        }
        tipText.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(imgView.snp.bottom).offset(10)
            make.left.right.equalToSuperview()
            make.height.equalTo(20)
        }
        subTipText.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(tipText.snp.bottom).offset(10)
            make.left.right.equalToSuperview()
            make.height.equalTo(20)
        }
        btn.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(subTipText.snp.bottom).offset(10)
            make.width.equalTo(200)
            make.height.equalTo(40)
        }
    }

}


import UIKit
import RxSwift

var dayLastTime: String = "15:59:59"

class SYBaseView: UIView {
    
    let disposeBag = DisposeBag()
    
    public var clickedClosure: ((Int) -> Void)?
    
    public var passParamsClosure: ((Any) -> Void)?

    weak var currentVC: SYBaseViewController!
    
    var dayLastTime: String = "15:59:59"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    func initViews() {
        
    }
    
    public var lineView: UIView = {
        let v: UIView = UIView()
        v.backgroundColor = UIColor(hex: 0xE6E6E6)
        return v
    }()
    
    func setPriceLb(_ price: String?) -> NSMutableAttributedString {
        let d: Double = Double(price ?? "0") ?? 0.0
        let priceStr = String(format: "%.2f", d)
        let firStr = "￥"
        let totalStr = "￥\(priceStr)"
        let attString = NSMutableAttributedString(string: totalStr)
        attString.yy_setColor(UIColor(hex: 0xF55035), range: NSRange(location: 0, length: firStr.count))
        attString.yy_setFont(13.autoFontSize(), range: NSRange(location: 0, length:firStr.count))
        attString.yy_setColor(UIColor(hex: 0xF55035), range: NSRange(location: firStr.count, length: totalStr.count - firStr.count))
        attString.yy_setFont(16.autoFontSize(), range: NSRange(location: firStr.count, length: totalStr.count - firStr.count))
        return attString
    }
    
    func setPriceLb(_ price: String?, _ priceFont: UIFont?) -> NSMutableAttributedString {
        let d: Double = Double(price ?? "0") ?? 0.0
        let priceStr = String(format: "%.2f", d)
        let firStr = "￥"
        let totalStr = "￥\(priceStr)"
        let attString = NSMutableAttributedString(string: totalStr)
        attString.yy_setColor(UIColor(hex: 0xF55035), range: NSRange(location: 0, length: firStr.count))
        attString.yy_setFont(13.autoFontSize(), range: NSRange(location: 0, length:firStr.count))
        attString.yy_setColor(UIColor(hex: 0xF55035), range: NSRange(location: firStr.count, length: totalStr.count - firStr.count))
        attString.yy_setFont(priceFont, range: NSRange(location: firStr.count, length: totalStr.count - firStr.count))
        return attString
    }
    
    func getWeekStr(index: Int) -> String {
        
        var str = ""
        switch index {
        case 1:
            str = "周日"
        case 2:
            str = "周一"
        case 3:
            str = "周二"
        case 4:
            str = "周三"
        case 5:
            str = "周四"
        case 6:
            str = "周五"
        case 7:
            str = "周六"
        default:
            break
        }
        return str
    }
    
    var weekDays: [String] = ["日", "一", "二", "三", "四", "五", "六"]
    
    func isTodayOrTomorrow(_ date:Date) -> String {

        if date.isToday {
            return "今天"
        } else if date.isTomorrow {
            return "明天"
        } else {
            return ""
        }
    }
}
