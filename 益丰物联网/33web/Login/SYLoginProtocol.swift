//
//  SYLoginProtocol.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2020/8/21.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit

protocol SYLoginProtocol {
    var loginType: LoginEnum { get }
    
    func login(loginName: String, password: String) 
}

extension SYLoginProtocol {
    func thirdLogin() {}
}

enum LoginEnum {
    case account
    case verifiCode
    case register
}
