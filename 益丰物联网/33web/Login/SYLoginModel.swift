//
//  SYLoginModel.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/1/27.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit
import HandyJSON


struct SYLoginModel : HandyJSON,Equatable {

    /// <#泛型#>
    var password: String?
    /// <#泛型#>
    var updateBy: String?
    /// <#泛型#>
    var updateDate: String?
    /// <#泛型#>
    var deptPostVOList: String?
    /// <#泛型#>
    var createBy: String?
    /// <#泛型#>
    var salt: String?
    /// <#泛型#>
    var appTenantUserVO: String?
    /// <#泛型#>
    var nickName: String?
    /// <#泛型#>
    var createDate: String?
    /// 陆风
    var trueName: String?
    /// <#泛型#>
    var tenant: String?
    /// 18697753680
    var mobile: String?
    /// <#泛型#>
    var sort: String?
    /// <#泛型#>
    var remarks: String?
    /// <#泛型#>
    var sysDeptPostVO: String?
    /// <#泛型#>
    var sex: String?
    /// <#泛型#>
    var loginTime: String?
    /// <#泛型#>
    var age: String?
    /// <#泛型#>
    var sysPostVO: String?
    /// <#泛型#>
    var educationVOS: String?
    /// <#泛型#>
    var userProfileVO: String?
    /// <#泛型#>
    var id: String?
    ///
    var headImg: String?
    /// <#泛型#>
    var deptVO: String?
    /// <#泛型#>
    var uuid: String?
    /// <#泛型#>
    var birthDayStr: String?
    /// <#泛型#>
    var tenantMenuVO: String?
    /// <#泛型#>
    var sysTenantUserVO: String?
    /// <#泛型#>
    var menuRoleVO: String?
    /// <#泛型#>
    var status: String?
    /// <#泛型#>
    var version: String?
    /// <#泛型#>
    var tenantId: String?
    /// <#泛型#>
    var recentOnLineTime: String?
    /// <#泛型#>
    var loginName: String?
    ///
    var token: String?
}
