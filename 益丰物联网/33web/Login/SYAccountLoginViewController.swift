//
//  SYAccountLoginViewController.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2020/8/20.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import SnapKit
import Then

class SYAccountLoginViewController: SYBaseViewController, SYLoginProtocol {

    
    
    
    var loginType: LoginEnum {
        return .account
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.navigationController?.navigationBar.isHidden = true
        
        let loginView = SYLoginView(frame: CGRect.zero, loginProtocol: self, viewController: self)
        self.view.addSubview(loginView)
        loginView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
    }
    
    /// SYLoginProtocol
    func thirdLogin() {
        
    }
    
    func login(loginName: String, password: String) {
        
        let requestVM: RYNongchangListVM = RYNongchangListVM()
        requestVM.baseRequest(disposeBag: dispose, type: .login(loginName: loginName, password: password), modelClass: SGBaseModel<SYLoginModel>.self) {[weak self] (res) in
            if let `self` = self {
                let m = res.data
                UserInstance.userId = m?.id
                UserInstance.accessToken = m?.token
                let urlStr = GOTOHOME + "?data=" + (m?.toJSONString() ?? "")
                print(urlStr.getEncodeString,m?.toJSONString())
                self.sy_pushWebVC(urlStr.getEncodeString)
            }
            
        } Error: {
            
        }
    }

}
