//
//  HSWebViewController.swift
//  33web
//
//  Created by 宋海胜 on 2020/11/25.
//

import UIKit
import WebKit
import SwiftWeb
import MapKit

import RxSwift
import RxAlamofire
import HandyJSON
import Toast_Swift

enum JsMethod {
    case passGPSInfo
    case passVersion
    case passVoiceContent
    case passPayResult
}

// 自定义编辑
class HSWebViewController: BaseWebViewController, XYVoiceRecognizeProtocol {

    
    public var backClosure: ((Int) -> Void)?

    fileprivate var location = LocationManager()
    fileprivate var isOnece = false
    fileprivate var configManager = Tool()
    fileprivate var backImgView = UIImageView()
    fileprivate var urlInputView: InputView!
    fileprivate var voiceView: XYVoiceRecognizeManager?
    internal var liveView = ZGLiveView()
    public var canAllButUpsideDown = false
    let disposeBag = DisposeBag()
    
    deinit {
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "getGPSInfo")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "saveUnreadMsgCnt")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "getVersion")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "passVideoParams")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "leaveVideoPage")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "startVoice")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "stopVoice")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "toMiniProgram")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "toGaodeApp")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "toPay")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "toShare")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "toDownLoad")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "saveImage")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "webGoBack")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "loginOut")


    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 萤石云视频全屏
        liveView.currentVC = self
        liveView.frame = CGRect(x: 0, y: kStatusBarHeight, width: kScreenWidth, height: kScreenHeight-kStatusBarHeight)
        
        // 启动图复制图片、web未加载完成时覆盖页面
//        self.backImgView = self.configManager.getLanuchboardImage()
//        self.backImgView.frame = self.view.bounds
//        self.view.addSubview(self.backImgView)
        
        // 顶部状态栏高度问题解决
        self.configManager.topMangain(webView: webView, vc: self)
        
        // 网络判断，重新加载网页
        judgeNetwork()
        // 经纬度
        location.locationProtocol = self
        
        // 语音听写、播报
        self.voiceView = XYVoiceRecognizeManager(currentVC: self)
        self.voiceView!.delegate = self
        
        #if DEBUG // 判断是否在测试环境下
        // 输入框
        urlInputView = InputView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 100), currentVC: self)
//        view.addSubview(urlInputView)
        #else
        
        #endif
        
        self.webView.frame = CGRect(x: 0, y: kStatusBarHeight, width: kScreenWidth, height: kScreenHeight-kStatusBarHeight)
//        VersionCheck.hasUpdateVersion(self, disposeBag)
    }
    
    override func injectSwiftMethod(_ config: WKWebViewConfiguration) {
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "getGPSInfo")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "saveUnreadMsgCnt")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "getVersion")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "passVideoParams")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "leaveVideoPage")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "startVoice")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "stopVoice")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "toMiniProgram")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "toGaodeApp")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "toPay")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "toShare")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "toDownLoad")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "saveImage")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "webGoBack")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "loginOut")


    }
    
    override func jsAction() {
        
    }
    
    // 在viewDidLoad中模态不行，此时页面还没加载完成
    fileprivate func judgeNetwork() {
        
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
        //
        //            if self.configManager.judgeNetworkState(self) {
        //                self.loadWeb()
        //            }
        //        }
                
                self.loadWeb()
                let myAppdelegate = UIApplication.shared.delegate as! AppDelegate
                myAppdelegate.networkAuthStatus(stateClosure: {[weak self] state in
                    if let `self` = self {
                        if state {
                            DispatchQueue.main.async {
                                HUDUtil.showHud()
                            }
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                DispatchQueue.main.async {
                                    HUDUtil.hideHud()
                                }
                                self.loadWeb()
                            }
                        }
                    }
                })
            }
    
    fileprivate func jsAction(param: String, type: JsMethod) {
        
        var javascript: String = ""
        
        switch type {
            
        case .passGPSInfo:
            javascript = "javascript:passGPSInfo(\"\(param)\")"
            
        case .passVersion:
            javascript = "javascript:passVersion(\"\(param)\")"
            
        case .passVoiceContent:
            javascript = "javascript:passVoiceContent(\"\(param)\")"
            
        case .passPayResult:
            javascript = "javascript:payResult(\"\(param)\")"
        }
        self.webView.evaluateJavaScript(javascript) { (res, error) in
            print("swift向js传值\n  param: \(param)\n  response: \(res ?? "nil")\n  error: \(error?.localizedDescription ?? "nil")")
        }
    }
    
    override func swiftAction(_ name: String, _ body: Dictionary<String, Any>) {
        
        switch name {
        case "webGoBack":
            if webView.canGoBack {
                webView.goBack()
            } else {
                self.sy_popVC()
            }
        case "getGPSInfo":
            gpsService()
            
        case "saveUnreadMsgCnt":
            let num = body["notReadNum"] as! NSNumber
            UIApplication.shared.applicationIconBadgeNumber = num.intValue > 99 ? 99 : num.intValue
            
        case "getVersion":
            let infoDic = Bundle.main.infoDictionary
            let appVersion = infoDic!["CFBundleShortVersionString"] as! String
            jsAction(param: appVersion, type: .passVersion)
            
        case "passVideoParams":
            setVideoPage(body)
            
        case "leaveVideoPage":
            liveView.leaveVideoPage()
        case "startVoice":
            print(name, "+", body)
            self.voiceView!.startIFlySpeechRecognize()
            
        case "stopVoice":
            self.voiceView!.stopIFlySpeechRecognize()
            
        case "toMiniProgram":
            let appId = body["appId"] as? String ?? ""
            let path = body["path"] as? String ?? ""
//            WXManager.toMiniProgram(appId, path, self)
        case "loginOut":
//            UserInstance.userLogout()
            self.backClosure?(0)
            self.navigationController?.popViewController(animated: false)
            
        case "toGaodeApp":
            Tool.routePlanning(body)
            
        case "toPay":
            Utils.toPay(body)
            
//        case "toShare":
//            XYShareManger.toShare(body, self)
            
        case "toDownLoad":
            let url = body["url"] as? String ?? ""
            UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
            
        case "callSomeOne":
            let phoneNum = body["phoneNum"] as? String ?? ""
            self.configManager.callSomeOne(phoneNum)
            
        case "saveImage":
            let urlStr = body["url"] as? String ?? ""
            let subStr = urlStr.components(separatedBy: "base64,").last ?? ""
            self.saveImgs(base64EncodedStr: subStr)
        default:
            break
        }
    }
    
    override func getH5Url(_ url: String) {
        
        if (url.contains("tel://")) {
            let phoneUrl = url.components(separatedBy: "tel://").last
            self.configManager.callSomeOne(phoneUrl!)
        }
    }
    
    override func loadSuccess() {
        
        UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseInOut, animations: {
            self.backImgView.alpha = 0
        }) { (finish) in
            self.backImgView.isHidden = true
        }
    }
    
    override func loadFail() {
        backImgView.isHidden = false
        self.configManager.alertToReload(self) {
            self.loadWeb()
        }
    }
    
    // 定位
    fileprivate func gpsService() {
        
        //只获取一次
        isOnece = true
        self.location.startLocation()
    }
    
    open override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        if size.width > size.height {// 横屏
            liveView.blanner!.frame = CGRect(x: 0, y: 0, width: kScreenHeight, height: kScreenWidth)
            liveView.showView(isFullScreen: true)
        } else {
            liveView.blanner!.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenWidth*9/16)
            liveView.hideAboveViews()
            self.view.hideToastActivity()
            liveView.showView(isFullScreen: false)
        }
    }
    
    func setVideoPage(_ params: Dictionary<String, Any>) {
        
        Tool.requestAuthorizationPhotoLibary()
        Tool.requestMicroPhoneAuth()
        self.canAllButUpsideDown = true
        if let startTime = params["startTime"] as? String, startTime != "" {
//            self.liveView.configureSDKPlayback(params)
        } else {
            self.liveView.configureSDK(params)
        }
        self.liveView.fullScreenAction()
    }
    
    func voiceContent(content: String) {
        self.jsAction(param: content, type: .passVoiceContent)
    }
    
    func payResult() {
        self.jsAction(param: "", type: .passPayResult)
    }
    
    func saveImgs(urlStr: String) {
        if let url = URL(string: urlStr) {
            do {
                let data = try Data(contentsOf: url)
                if let image = UIImage(data: data) {
                    let infoDictionary: Dictionary = Bundle.main.infoDictionary!
                    let kAppDisplayName = infoDictionary["CFBundleDisplayName"] as! String

                    ZGCustomPhoto.saveImageInAlbum(image: image, albumName: String(format: "%@APP", kAppDisplayName)) { (result) in
                        
                        DispatchQueue.main.async {
                            switch result{
                            case .success:
                                self.view.makeToast("已保存至相册")
                            case .denied:
                                self.view.makeToast("保存被拒绝")
                            case .error:
                                self.view.makeToast("保存失败")
                            }
                        }
                    }
                } else {
                    self.view.makeToast("data to image error")
                }
            } catch {
                self.view.makeToast(error.localizedDescription)
            }
           
        } else {
            self.view.makeToast("urlStr to URL error")
        }
    }
    
    func saveImgs(base64EncodedStr: String) {
        
        if let data = Data(base64Encoded: base64EncodedStr) {
            if let image = UIImage(data: data) {
                ZGCustomPhoto.saveImageInAlbum(image: image, albumName: "安国物联app") { (result) in
                    
                    DispatchQueue.main.async {
                        switch result{
                        case .success:
                            self.view.makeToast("已保存至相册")
                        case .denied:
                            self.view.makeToast("保存被拒绝")
                        case .error:
                            self.view.makeToast("保存失败")
                        }
                    }
                }
            } else {
                self.view.makeToast("data to image error")
            }
        } else {
            self.view.makeToast("base64EncodedStr to data error")
        }
        
    }
}

extension HSWebViewController: LocationProtocol {

    func getGPSAuthorizationFailure() {
        let alert = UIAlertController(title: "提示", message: "请打开定位，以便获取您的位置信息", preferredStyle: .alert)

        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)

        self.present(alert, animated: true, completion: nil)
    }

    func getGPSSuccess(latitude: Double, longitude: Double) {
        location.stopLocation()
        if (self.isOnece) {
            print("lng: \(longitude) lat: \(latitude)")
            let param = "\(longitude),\(latitude)"
            self.jsAction(param: param, type: .passGPSInfo)
            self.isOnece = false
        }
    }

    func getLocationSuccess(_ area: String, _ locality: String, _ subLocality: String, _ thoroughfare: String, _ name: String) {
        
    }
    func getGPSFailure(error: Error) {
        self.isOnece = false
        print("getMoLocation error: \(error.localizedDescription)")
        if (!self.isOnece) {
            location.stopLocation()
        }
    }
}
