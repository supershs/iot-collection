//
//  AppDelegate.swift
//  33web
//
//  Created by 宋海胜 on 2020/11/25.
//

import UIKit
import IQKeyboardManagerSwift
import CallKit
import CoreTelephony

enum SCREEN_SET {
    case set_port
    case set_land
    case set_auto
}

// 蒲公英的appkey 每个app都不一样，用来控制版本更新提示
let kPGYAppKey = "5e06c9686c0777771c1bdc744e8b78b2"
// 蒲公英账号api（不变）
let kPGYApiKey = "0fd319b2151be6ebd41031a8493a79db"
// 版本更新提示次数
let kMaxShowUpdateTipCount = 3

// 萤石云控制接口
let kVideoStartPTZ = "https://open.ys7.com/api/lapp/device/ptz/start"
let kVideoStopPTZ = "https://open.ys7.com/api/lapp/device/ptz/stop"
let kGetAccessToken = "https://open.ys7.com/api/lapp/token/get"
// 图片上传接口（暂无此功能）
let kImageApi = "http://119.3.169.92:9488"
let kImageUpload = "\(kImageApi)/sysFile/batchUploadImg"
let kImageInfoUpload = "\(kImageApi)/api/1.1/CityCapture/savePic"

// 友盟
let kUMAppKey = "5f8d054180455950e4ae04ed"//"5e9d880ddbc2ec07ad295685"
// 微信
let kWechatAppId = "wxe315c4d7125111e6"
let kWechatSecrectKey = "5424cea7cb28690d3ad4e7f680b236ea"
let kWechatLinks = "https://help.wechat.com/XuYi/"
@main

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var webVC = HSWebViewController(path: WEBIP)
    var loginVC = SYAccountLoginViewController()
    var screen_set: SCREEN_SET = .set_auto

//    var wxDelegate = WXDelegate()
    let myAppdelegate = UIApplication.shared.delegate as? AppDelegate

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
//        //设置用户授权显示通知
//        if #available(iOS 10.0, *) {
//            UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.badge,.carPlay,.sound], completionHandler: { (success, error) in
//                print("通知授权" + (success ? "成功" : "失败"))
//            })
//        }
        IQKeyboardManager.shared.enable = true

        window = UIWindow(frame: UIScreen.main.bounds)
        
//        window!.rootViewController = webVC
        window?.rootViewController = UINavigationController(rootViewController: loginVC)
//        window!.rootViewController = loginVC
        window!.makeKeyAndVisible()
//        configUM(application, launchOptions)
        IFlySpeechUtility.createUtility("appid=\(APPID_VALUE)");
//        wxDelegate.currentVC = webVC
        
        // 强制关闭暗黑模式
        if #available(iOS 13.0, *) {
            self.window?.overrideUserInterfaceStyle = .light
        }
        #if DEBUG // 判断是否在测试环境下
        print("当前环境：DEBUG")
        #else
        print("当前环境：RELEASE")
        #endif
        return true
    }

    func configUM(_ application: UIApplication, _ launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
//        UMConfigure.initWithAppkey(kUMAppKey, channel: "App Store")
//        UMSocialManager.default()?.setPlaform(UMSocialPlatformType.wechatSession, appKey: kWechatAppId, appSecret: kWechatSecrectKey, redirectURL: kWechatLinks)
////        WXApi.registerApp(kWechatAppId, universalLink: kWechatLinks)
//        WXApi.registerApp(kWechatAppId)
//        self.youmengPush(application, launchOptions)
    }

    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        // 在此方法中做如下判断，因为还有可能有其他的支付，如支付宝就是@"safepay"
        if url.host == "pay" {
        } else if url.host == "safepay"{
//            AliPayDelegate.handleAlipayUrl(url, webVC)
        }
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        // 在此方法中做如下判断，因为还有可能有其他的支付，如支付宝就是@"safepay"
        if url.host == "pay" {
        } else if url.host == "safepay"{
//            AliPayDelegate.handleAlipayUrl(url, webVC)
        }
        return true
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        if webVC.canAllButUpsideDown {
            return UIInterfaceOrientationMask.allButUpsideDown
        } else {
            return UIInterfaceOrientationMask.portrait
        }
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        webVC.liveView.stopLiveVideo()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        webVC.liveView.startLiveVideo()
    }
    


    
    // 电话状态监听
//    func callObserver(_ callObserver: CXCallObserver, callChanged call: CXCall) {
//
//        print("电话状态监听: \n      call.isOutgoing: \(call.isOutgoing) \n      call.isOnHold: \(call.isOnHold) \n      call.hasEnded: \(call.hasEnded) \n      call.hasConnected: \(call.hasConnected) \n      call.uuid: \(call.uuid)")
//
//        if call.isOutgoing {// 拨打
//            if call.hasConnected {
//                if call.hasEnded {
//                    print("watchCallStatus: 拨打->接通->结束")
//                    webVC.liveView.startLiveVideo()
//                } else {
//                    print("watchCallStatus: 拨打->接通")
//                }
//            } else if call.hasEnded {
//                print("watchCallStatus: 拨打->结束")
//                webVC.liveView.startLiveVideo()
//            } else {
//                print("watchCallStatus: 拨打")
//            }
//        } else if call.isOnHold {// 待接通
//            print("watchCallStatus: 待接通")
//        } else if call.hasConnected {// 接通
//            if call.hasEnded {// 结束
//                webVC.liveView.startLiveVideo()
//                print("watchCallStatus: 接通->结束")
//            } else {
//                print("watchCallStatus: 接通")
//            }
//        } else if call.hasEnded {// 结束
//            webVC.liveView.startLiveVideo()
//            print("watchCallStatus: 结束")
//        } else {
//            print("watchCallStatus: \(call.uuid)")
//            webVC.liveView.stopLiveVideo()
//        }
//    }
    
    func networkAuthStatus(stateClosure: @escaping ((Bool) -> Void)) {
        let cellularData = CTCellularData()
        cellularData.cellularDataRestrictionDidUpdateNotifier = {(state) in
            if (state == .restricted) {
                //拒绝
                if let t = UserDefaults.standard.value(forKey: "isFirstInApp") as? Bool, !t {
//                    self.networkSettingAlert()
                }
                stateClosure(false)
            } else if (state == .notRestricted) {
                //允许
                stateClosure(true)
            } else {
                //未知
                if let t = UserDefaults.standard.value(forKey: "isFirstInApp") as? Bool, !t {
//                    self.unknownNetwork()
                }
                stateClosure(false)
            }
        }
    }
}


