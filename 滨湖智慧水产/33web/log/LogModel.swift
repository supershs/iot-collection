//
//  LogModel.swift
//  33web
//
//  Created by JAY on 2023/5/9.
//

import UIKit

import HandyJSON


struct NSRootModel<T: HandyJSON> : HandyJSON{
    ///
    var date: String?
    ///
    var data: T?
    /// SUCCESS
    var status: String?
    /// 10000
    var code: Int = 0
    /// 登录成功
    var message: String?

}


struct LoginModel : HandyJSON {

    ///
    var token: String?
    ///
    var userInfo: NSUserInfoModel?

}


struct NSUserInfoModel : HandyJSON {

    /// ADMIN
    var userType: String?
    ///
    var updateDate: String?
    ///
    var salt: String?
    /// 1
    var tenantId: String?
    /// <#泛型#>
    var deptId: String?
    ///
//    var menuList: [Any]?
    /// <#泛型#>
    var brithday: String?
    /// admin
    var loginName: String?
    /// <#泛型#>
    var createBy: String?
    ///
    var deptName: String?
    ///
    var password: String?
    /// <#泛型#>
    var createDate: String?
    ///
    var remarks: String?
    /// <#泛型#>
    var version: String?
    /// 1
    var sex: Int = 0
    /// 1
    var status: Int = 0
    /// <#泛型#>
    var monitorId: String?
    /// 0
    var sort: Int = 0
    ///
    var uuid: String?
    /// 管理员
    var trueName: String?
    /// <#泛型#>
    var registerDate: String?
    ///
    var phone: String?
    ///
    var email: String?
    /// 16806
    var jobNumber: String?
    /// <#泛型#>
    var deviceId: String?
    /// 强丰农场
    var roleName: String?
    ///
    var updateBy: String?
    /// PC
    var type: String?
    /// <#泛型#>
    var lastLoginTime: String?
    ///
    var mobile: String?
    /// id
    var id: String?
    /// <#泛型#>
    var idCard: String?
    ///
    var headImg: String?

 
}
