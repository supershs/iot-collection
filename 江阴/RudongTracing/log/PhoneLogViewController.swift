//
//  PhoneLogViewController.swift
//  33web
//
//  Created by JAY on 2023/5/10.
//
import UIKit
import SnapKit
import RxSwift
import RxAlamofire
import HandyJSON
import Alamofire
import SwiftyJSON

class PhoneLogViewController: RootViewController,UITextViewDelegate,UITextFieldDelegate{
    let disposBag = DisposeBag()
    ///点击类型
    enum ClickLinkType {
        ///用户协议
        case userProtocol
        ///隐私条款
        case privacyPolicy
    }
    
    var showAll : Bool!
    var btnImage : Bool!
    
    ///点击事件
    var clickHandle:((_ clickType:ClickLinkType)->())?
    
    public var imageV : UIImageView = {
        let iconV = UIImageView()
        iconV.image = UIImage(named: "login_bg")
        return iconV
    }()
    
    public var logoV : UIImageView = {
        
        let iconV = UIImageView()
        iconV.image = UIImage(named: "lg")
        return iconV
        
    }()
    
    public var logoLb : UILabel = {
        
        let lb = UILabel()
        lb.textColor = UIColor.black
        lb.font = UIFont.boldSystemFont(ofSize: 25)
        lb.text = "江阴高标准农田灌溉系统"
        lb.numberOfLines = 0
        lb.textAlignment = .left
        return lb
        
    }()
    
    
    public var accountView : UIView = {
        let v = UIView()
        v.backgroundColor = UIColor(hex: "f7f7fb")
        v.layer.cornerRadius = 5
        return v
    }()
    
    public var passwordView : UIView = {
        let v = UIView()
        v.backgroundColor = UIColor(hex: "f7f7fb")
        v.layer.cornerRadius = 5
        return v
    }()
    
    public var accountLb : UILabel = {
        let lb = UILabel()
        lb.font = UIFont.systemFont(ofSize: 12.0)
        lb.textColor = UIColor.black
        lb.text = "手机号"
        lb.numberOfLines = 0
        lb.textAlignment = .left
        return lb
    }()
    public var passwordLb : UILabel = {
        let lb = UILabel()
        lb.font = UIFont.systemFont(ofSize: 12.0)
        lb.textColor = UIColor.black
        lb.text = "验证码"
        lb.numberOfLines = 0
        lb.textAlignment = .left
        return lb
    }()
    
    public var codeBtn : UIButton = {
        let btn = UIButton()
        btn.setTitle("获取验证码", for: .normal)
        btn.setTitleColor( .black, for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        btn.contentHorizontalAlignment = .right
        return btn
        
    }()
    
    var usernameTF: UITextField = {
        let tf = UITextField()
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.textColor = .black
       
        tf.placeholder = "请输入手机号"
        return tf
    }()
    

    var passwordTF: UITextField = {
        let tf = UITextField()
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.textColor = .black
        tf.placeholder = "请输入验证码"
        tf.isSecureTextEntry = true
        return tf
    }()
    
    
    public var logBtn : UIButton = {
        let btn = UIButton()
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        btn.setTitleColor(.white, for: .normal)
        btn.contentHorizontalAlignment = .center
        btn.setTitle("登录", for: .normal)
        btn.setBackgroundImage(UIImage(named: "login_btn"), for: .normal)
       
        return btn
    }()
    
    public var delegateButton : UIButton = {
        let bt = UIButton()
        bt.setImage(UIImage(named: "select_false"), for: .normal)
        return bt
    }()
    
    ///同意View
    private lazy var agreeTextView : UITextView = {
        let textStr = "登录既代表您已同意《用户协议》和《隐私条款》"
        let textView = UITextView()
        textView.delegate = self
        textView.font =  UIFont.systemFont(ofSize: 12, weight: .regular)
        textView.textColor = UIColor(hex:"666666")
        textView.textAlignment = .center
 
        ///设为true 在代理里面禁掉所有的交互事件
        textView.isEditable = true
         
        textView.autoresizingMask =  UIView.AutoresizingMask.flexibleHeight
        textView.isScrollEnabled = false
        let attStr = NSMutableAttributedString(string: textStr)
         
        //点击超链接
        attStr.addAttribute(NSAttributedString.Key.link, value: "userProtocol://", range: (textStr as NSString).range(of: "《用户协议》和《隐私条款》"))
        textView.attributedText = attStr
        ///只能设置一种颜色
        textView.linkTextAttributes =  [
            NSAttributedString.Key.foregroundColor: UIColor(hex: "00B85F")
        ] 
         
        return textView
    }()
    
    public var phoneBtn : UIButton = {
        let btn = UIButton()
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        btn.setTitleColor(UIColor(hex: "c9c5c5"), for: .normal)
        btn.contentHorizontalAlignment = .center
        btn.setTitle("账号密码登录", for: .normal)
        return btn
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showAll = false
        btnImage = false
        
        view.backgroundColor = .white
        view.addSubview(imageV)
        view.addSubview(logoV)
        view.addSubview(logoLb)
        
        view.addSubview(accountView)
        view.addSubview(passwordView)
        accountView.addSubview(accountLb)
        passwordView.addSubview(passwordLb)
        accountView.addSubview(usernameTF)
        passwordView.addSubview(passwordTF)
        passwordView.addSubview(codeBtn)
        
        view.addSubview(logBtn)
        view.addSubview(delegateButton)
        view.addSubview(agreeTextView)
        view.addSubview(phoneBtn)
        
        passwordTF.delegate = self
        usernameTF.delegate = self
        
        codeBtn.addTarget(self, action: #selector(codeClick), for: .touchUpInside)
        delegateButton.addTarget(self, action: #selector(delegateClick), for: .touchUpInside)
        phoneBtn.addTarget(self, action: #selector(phoneClick), for: .touchUpInside)
        logBtn.addTarget(self, action: #selector(btnClick), for: .touchUpInside)
        imageV.snp.makeConstraints { make in
            make.top.bottom.right.left.equalTo(view)
        }
        logoV.snp.makeConstraints { make in
            make.top.equalTo(view).offset(120)
            make.left.equalTo(view).offset(15)
            make.width.equalTo(60)
            make.height.equalTo(60)
        }
        logoLb.snp.makeConstraints { make in
            make.top.equalTo(logoV)
            make.left.equalTo(logoV.snp.right).offset(10)
            make.width.equalTo(100)
            make.height.equalTo(60)
        }
        
        accountView.snp.makeConstraints { make in
            make.top.equalTo(logoV.snp.bottom).offset(50)
            make.left.equalTo(imageV).offset(30)
            make.right.equalTo(imageV).offset(-30)
            make.height.equalTo(60)
        }
        passwordView.snp.makeConstraints { make in
            make.top.equalTo(accountView.snp.bottom).offset(20)
            make.left.equalTo(imageV).offset(30)
            make.right.equalTo(imageV).offset(-30)
            make.height.equalTo(60)
        }
        accountLb.snp.makeConstraints { make in
            make.top.equalTo(accountView).offset(5)
            make.left.equalTo(accountView).offset(10)
            make.right.equalTo(accountView).offset(-10)
            make.height.equalTo(25)
        }
        passwordLb.snp.makeConstraints { make in
            make.top.equalTo(passwordView).offset(5)
            make.left.equalTo(passwordView).offset(10)
            make.right.equalTo(passwordView).offset(-10)
            make.height.equalTo(25)
        }
        
        usernameTF.snp.makeConstraints { make in
            make.top.equalTo(accountLb.snp.bottom).offset(0)
            make.left.equalTo(accountView).offset(10)
            make.right.equalTo(accountView).offset(-10)
            make.bottom.equalTo(accountView)
        }
        
        passwordTF.snp.makeConstraints { make in
            make.top.equalTo(passwordLb.snp.bottom).offset(0)
            make.left.equalTo(passwordView).offset(10)
            make.right.equalTo(passwordView).offset(-10)
            make.bottom.equalTo(passwordView)
        }
        
        codeBtn.snp.makeConstraints { make in
            make.width.equalTo(100)
            make.height.equalTo(30)
            make.right.equalTo(passwordView).offset(-10)
            make.centerY.equalTo(passwordView)
        }
        
        
        logBtn.snp.makeConstraints { make in
            make.top.equalTo(passwordView.snp.bottom).offset(20)
            make.right.equalTo(view).offset(-45)
            make.left.equalTo(view).offset(45)
            make.height.equalTo(50)
        }
        
        delegateButton.snp.makeConstraints { make in
            make.top.equalTo(logBtn.snp.bottom).offset(22)
            make.left.equalTo(logBtn).offset(20)
            make.width.equalTo(15)
            make.height.equalTo(15)
        }
        
        agreeTextView.snp.makeConstraints { make in
            make.centerY.equalTo(delegateButton)
            make.left.equalTo(delegateButton.snp.right).offset(5)
            make.width.equalTo(300)
            make.height.equalTo(30)
        }
        
        
        phoneBtn.snp.makeConstraints { make in
            make.top.equalTo(agreeTextView.snp.bottom).offset(20)
            make.centerX.equalTo(view)
            make.width.equalTo(200)
            make.height.equalTo(30)
        }
        
        
        
//
    }
    //    MARK: -  发验证码
    @objc func codeClick(){
        
        var countdown = 60 // 倒计时的秒数
        if usernameTF.text!.isEmpty {
            TSProgressHUD.ts_showWarningWithStatus("请输入手机号")
        }else{
            let sysTenantDTO = ["url":paraUrl]
            let parameters  = ["mobile":usernameTF.text,"sysTenantDTO":sysTenantDTO] as [String : Any]
            RxAlamofire.requestJSON(.post, URL(string: CodeURL)!, parameters: parameters, encoding: JSONEncoding.default, headers: ["Content-Type":"application/json;charset=UTF-8"])
                .debug()
                .subscribe(onNext: { [self] (r, json) in
                    
                    let JSONDictory = JSON(json ?? " ")
                    let code = JSONDictory["code"].intValue
                    let message = JSONDictory["message"].string
                    print("code====",code)
                    print("CodeURL====",CodeURL)
                    print("parameters====",parameters)
                    if code == 10000{
                        self.codeBtn.isEnabled = false
                        let timer = DispatchSource.makeTimerSource(queue: DispatchQueue.global())
                        timer.schedule(deadline: .now(), repeating: .seconds(1))
                        timer.setEventHandler { [weak self] in
                            guard let self = self else { return }
                            countdown -= 1
                            DispatchQueue.main.async { [self] in
                                self.codeBtn.setTitle("\(countdown)秒后再试", for: .normal)
                            }
                            if countdown <= 0 {
                                timer.cancel()
                                DispatchQueue.main.async {
                                    self.codeBtn.setTitle("获取验证码", for: .normal)
                                    self.codeBtn.isEnabled = true
                                }
                            }
                        }
                        timer.resume()
                    }else{
                        TSProgressHUD.ts_showWarningWithStatus(message ?? "系统出错")
                    }
                    
                    
                }, onError: { (error) in
                    print(error,"=====error")
                    print("parameters == ",parameters)
                })
                .disposed(by: disposBag)
            
        }
    }
    //    MARK: -  账号密码登录
    @objc func phoneClick(){
        
        self.navigationController?.popViewController(animated: false)
       
    }
    //    MARK: -  登录
    @objc func btnClick(){
        
        if usernameTF.text!.isEmpty {
            TSProgressHUD.ts_showWarningWithStatus("请输入手机号")
        }else if passwordTF.text!.isEmpty{
            TSProgressHUD.ts_showWarningWithStatus("请输入密码")
        }else if btnImage == false{
            TSProgressHUD.ts_showWarningWithStatus("请选择协议")
        }else{
            goLogin(loginName: "", password: "")
            
        }
       
    }
    
    func goLogin(loginName: String, password: String){
        
        let sysTenantDTO = ["url":paraUrl]
        let parameters  = ["mobile":usernameTF.text!,"code":passwordTF.text!,"sysTenantDTO":sysTenantDTO] as [String : Any]
        RxAlamofire.requestJSON(.post, URL(string: codeLoginUrl)!, parameters: parameters, encoding: JSONEncoding.default, headers: ["Content-Type":"application/json;charset=UTF-8"])
                  .debug()
        
        
                  .subscribe(onNext: { (r, json) in
                    if let dict = json as? [String: AnyObject] {
                     
                        let modelA = NSRootModel<LoginModel>.deserialize(from: dict)
                        if modelA?.status == "SUCCESS"  {
                            print(">>>>>>>>>>>>>>>>>>>自己的打印>>>>>>>>>>>>>>>>>>>>>>")
                            let urlStr =  GOTOHOME  + (modelA?.data?.toJSONString() ?? "")
                            self.navigationController?.pushViewController(HSWebViewController(path: kWebUrl + urlStr.getEncodeString), animated: true)

                        }else{
                           
//                            HUDUtil.showBlackTextView(text: modelA?.message, detailText:"", delay: 1.5) {
                            }

                        }
                      
                  }, onError: { (error) in
                        print(error,"=====error")
                      print("parameters == ",parameters)
                  })
                  .disposed(by: disposBag)
    }
    
    //    MARK: -  勾选协议
    @objc func delegateClick(){

        if btnImage{
            btnImage = false
            delegateButton.setImage(UIImage(named: "select_false"), for: .normal)
        }else{
            btnImage = true
            
            delegateButton.setImage(UIImage(named: "select_true"), for: .normal)
        }
       
    }
    
    //textField点击return关闭键盘
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view?.endEditing(false)
        return true
    }
    
    
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        return false
    }
     
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if URL.scheme  ==  "userProtocol"{
            self.clickHandle?(.userProtocol)
            print("00000")
            return false
        }else if URL.scheme == "privacyPolicy"{
            self.clickHandle?(.privacyPolicy)
            print("111111111")
            return false
        }
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
        
    }
                                     
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
        
    }
    
}

