//
//  RegisterViewController.swift
//  studyDay01
//
//  Created by JAY on 2023/3/24.
//

import UIKit
import SnapKit
import RxSwift
import RxAlamofire
import HandyJSON
import Alamofire
import SwiftyJSON
class RegisterViewController: RootViewController {
    let disposBag = DisposeBag()
    var showAll : Bool!
    var showRepeat : Bool!
    var backIngV : UIImageView!
    var wisdomIngV : UIImageView!
    
    //手机号
    var backView : UIView!
    var beginLb : UILabel!
    var phoneF : UITextField!
    var lineView : UIView!
    
    //设置密码
    var backsetUpView : UIView!
    var phonesetUpIngV : UIImageView!
    var phonesetUpF : UITextField!
    var linesetUpView : UIView!
    var setUpBtn : UIButton!
    var looksetUpBtn : UIButton! //查看密码
    
    //再次输入密码
    var backRepeatView : UIView!
    var phoneRepeatIngV : UIImageView!
    var phoneRepeatF : UITextField!
    var lineRepeatView : UIView!
    var setRepeatBtn : UIButton!
    var lookRepeatBtn : UIButton! //查看密码
    
    //验证码
    var backTwoView : UIView!
    var phoneTwoIngV : UIImageView!
    var phoneTwoF : UITextField!
    var lineTwoView : UIView!
    var btn : UIButton!
    var lookBtn : UIButton! //查看密码
    
    //登录
    var signBtn : UIButton!
    //账号登录
    var accountBtn : UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "忘记密码"
        showAll = false
        showRepeat = false
        view.backgroundColor = .white
        setupUI()
    }
    
    func setupUI(){
        
        //view
        backView = UIView()
        backView.backgroundColor = UIColor.white
        view.addSubview(backView)
        backView.snp.makeConstraints { make in
            make.right.equalTo(view).offset(-15)
            make.left.equalTo(view).offset(15)
            make.top.equalTo(view).offset(20)
            make.height.equalTo(60)
        }
        
        beginLb = UILabel()
        beginLb.font = UIFont.systemFont(ofSize: 14.0)
        beginLb.textColor = UIColor.black
        beginLb.text = "+86"
        beginLb.numberOfLines = 0
        beginLb.textAlignment = .left
        backView.addSubview(beginLb)
        beginLb.snp.makeConstraints { make in
            make.left.equalTo(backView).offset(20)
            make.centerY.equalTo(backView)
            make.height.equalTo(21)
            make.width.equalTo(30)
        }
        
      
        
        //手机号输入框
        phoneF = UITextField()
        phoneF.borderStyle = .none //除边框样式
        phoneF.placeholder = "请输入手机号"
        phoneF.clearButtonMode = .whileEditing
        backView.addSubview(phoneF)
        phoneF.snp.makeConstraints { make in
            make.left.equalTo(beginLb.snp.right).offset(10)
            make.right.bottom.top.equalTo(backView)
        }
        
        //底线
        lineView = UIView()
        lineView.backgroundColor = UIColor(hex: "d6d6d6")
        backView.addSubview(lineView)
        
        lineView.snp.makeConstraints { make in
            make.top.equalTo(phoneF.snp.bottom).offset(0)
            make.right.equalTo(backView).offset(-20)
            make.left.equalTo(backView).offset(15)
            make.height.equalTo(1)
        }
        
        //view设置密码
        backsetUpView = UIView()
        backsetUpView.backgroundColor = UIColor.white
        view.addSubview(backsetUpView)
        backsetUpView.snp.makeConstraints { make in
            make.right.equalTo(view).offset(-15)
            make.left.equalTo(view).offset(15)
            make.top.equalTo(backView.snp.bottom).offset(2)
            make.height.equalTo(60)
        }
        
        //密码输入框
        phonesetUpF = UITextField()
        phonesetUpF.borderStyle = .none //除边框样式
        phonesetUpF.placeholder = "请设置密码"
        phonesetUpF.isSecureTextEntry = true
        backsetUpView.addSubview(phonesetUpF)
        phonesetUpF.snp_makeConstraints { make in
            make.left.equalTo(backsetUpView).offset(15)
            make.right.bottom.top.equalTo(backsetUpView)
        }
        
        //底线
        linesetUpView = UIView()
        linesetUpView.backgroundColor = UIColor(hex: "d6d6d6")
        backsetUpView.addSubview(linesetUpView)
        linesetUpView.snp_makeConstraints { make in
            make.top.equalTo(phonesetUpF.snp.bottom).offset(0)
            make.right.equalTo(backsetUpView).offset(-20)
            make.left.equalTo(backsetUpView).offset(15)
            make.height.equalTo(1)
        }
        
        //查看设置密码
        looksetUpBtn = UIButton()
        looksetUpBtn.setImage(UIImage(named: "yan"), for: .normal)
        looksetUpBtn.addTarget(self, action: #selector(looksetUpBtnClick), for: .touchUpInside)
        backsetUpView.addSubview(looksetUpBtn)
        looksetUpBtn.snp.makeConstraints { make in
            make.centerY.equalTo(backsetUpView)
            make.right.equalTo(backsetUpView).offset(-10)
            make.width.equalTo(25)
            make.height.equalTo(25)
        }
        
        //view再次输入密码
        backRepeatView = UIView()
        backRepeatView.backgroundColor = UIColor.white
        view.addSubview(backRepeatView)
        backRepeatView.snp.makeConstraints { make in
            make.right.equalTo(view).offset(-15)
            make.left.equalTo(view).offset(15)
            make.top.equalTo(backsetUpView.snp_bottom).offset(2)
            make.height.equalTo(60)
        }
        
        //密码输入框
        phoneRepeatF = UITextField()
        phoneRepeatF.borderStyle = .none //除边框样式
        phoneRepeatF.placeholder = "请再次输入密码"
        phoneRepeatF.isSecureTextEntry = true
        backRepeatView.addSubview(phoneRepeatF)
        phoneRepeatF.snp_makeConstraints { make in
            make.left.equalTo(backRepeatView).offset(15)
            make.right.bottom.top.equalTo(backRepeatView)
        }
        
        //底线
        lineRepeatView = UIView()
        lineRepeatView.backgroundColor = UIColor(hex: "d6d6d6")
        backRepeatView.addSubview(lineRepeatView)
        lineRepeatView.snp_makeConstraints { make in
            make.top.equalTo(phoneRepeatF.snp_bottom).offset(0)
            make.right.equalTo(backRepeatView).offset(-20)
            make.left.equalTo(backRepeatView).offset(15)
            make.height.equalTo(1)
        }
        
        //查看设置密码
        lookRepeatBtn = UIButton()
        lookRepeatBtn.setImage(UIImage(named: "yan"), for: .normal)
        lookRepeatBtn.addTarget(self, action: #selector(lookRepeatBtnClick), for: .touchUpInside)
        backRepeatView.addSubview(lookRepeatBtn)
        lookRepeatBtn.snp_makeConstraints { make in
            make.centerY.equalTo(backRepeatView)
            make.right.equalTo(backRepeatView).offset(-10)
            make.width.equalTo(25)
            make.height.equalTo(25)
        }
        
        
        //view验证码
        backTwoView = UIView()
        backTwoView.backgroundColor = UIColor.white
        view.addSubview(backTwoView)
        backTwoView.snp.makeConstraints { make in
            make.right.equalTo(view).offset(-15)
            make.left.equalTo(view).offset(15)
            make.top.equalTo(backRepeatView.snp.bottom).offset(2)
            make.height.equalTo(60)
        }
        
        //验证码输入框
        phoneTwoF = UITextField()
        phoneTwoF.borderStyle = .none //除边框样式
        phoneTwoF.placeholder = "请输入验证码"
        backTwoView.addSubview(phoneTwoF)
        phoneTwoF.snp.makeConstraints { make in
            make.left.equalTo(backTwoView).offset(15)
            make.right.bottom.top.equalTo(backTwoView)
        }
        
        btn = UIButton()
        btn.setTitle("获取验证码", for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        btn.setTitleColor(UIColor(hex: "2cd4a0"), for: .normal)
        btn.setBackgroundImage(UIImage(named: "cheek"), for: .normal)
        btn.addTarget(self, action: #selector(codeClick), for: .touchUpInside)
        backTwoView.addSubview(btn)
        
        btn.snp_makeConstraints { make in
            make.centerY.equalTo(backTwoView)
            make.right.equalTo(backTwoView).offset(-20)
            make.width.equalTo(80)
            make.height.equalTo(25)
        }
        
        
        //底线
        lineTwoView = UIView()
        lineTwoView.backgroundColor = UIColor(hex: "d6d6d6")
        backTwoView.addSubview(lineTwoView)
        
        lineTwoView.snp_makeConstraints { make in
            make.top.equalTo(phoneTwoF.snp_bottom).offset(0)
            make.right.equalTo(backTwoView).offset(-20)
            make.left.equalTo(backTwoView).offset(10)
            make.height.equalTo(1)
        }
        
        //登录按钮
        signBtn = UIButton()
        signBtn.setTitle("修改", for: .normal)
        signBtn.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        signBtn.setTitleColor(UIColor.white, for: .normal)
        signBtn.setBackgroundImage(UIImage(named: "login_btn"), for: .normal)
        signBtn.addTarget(self, action: #selector(modifyClick), for: .touchUpInside)
        view.addSubview(signBtn)
        signBtn.snp_makeConstraints { make in
            make.top.equalTo(backTwoView.snp_bottom).offset(50)
            make.right.equalTo(backTwoView).offset(-20)
            make.left.equalTo(backTwoView).offset(20)
            make.height.equalTo(60)
        }
        
        //账号密码登录按钮
        accountBtn = UIButton()
        accountBtn.setTitle("", for: .normal)
        accountBtn.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        accountBtn.setTitleColor(UIColor.black, for: .normal)
        accountBtn.addTarget(self, action: #selector(accountClick), for: .touchUpInside)
        signBtn.setBackgroundImage(UIImage(named: "login_btn"), for: .normal)
        view.addSubview(accountBtn)
        accountBtn.snp_makeConstraints { make in
            make.top.equalTo(signBtn.snp_bottom).offset(0)
            make.right.equalTo(backTwoView).offset(-20)
            make.left.equalTo(backTwoView).offset(20)
            make.height.equalTo(45)
        }
        
        
        
    }
    
    //    MARK: -  去登录
    @objc func accountClick(){
       
//        self.navigationController?.popViewController(animated: false)
        
    }
    
    //    MARK: -  查看设置密码
    @objc func looksetUpBtnClick(){
       
        if showAll{
            showAll = false
            phonesetUpF.isSecureTextEntry = true
        }else{
            showAll = true
            phonesetUpF.isSecureTextEntry = false
        }
        
    }
    
    //    MARK: -  查看设置密码
    @objc func lookRepeatBtnClick(){
       
        if showRepeat{
            showRepeat = false
            phoneRepeatF.isSecureTextEntry = true
        }else{
            showRepeat = true
            phoneRepeatF.isSecureTextEntry = false
        }
    
    }
    //    MARK: -  验证码
    @objc func codeClick(){
        var countdown = 60 // 倒计时的秒数
        if phoneF.text!.isEmpty {
            TSProgressHUD.ts_showWarningWithStatus("请输入手机号")
        }else{
            
            let sysTenantDTO = ["url":paraUrl]
            let parameters  = ["mobile":phoneF.text,"sysTenantDTO":sysTenantDTO] as [String : Any]
            
            RxAlamofire.requestJSON(.post, URL(string: CodeURL)!, parameters: parameters, encoding: JSONEncoding.default, headers: ["Content-Type":"application/json;charset=UTF-8"])
                .debug()
                .subscribe(onNext: { [self] (r, json) in
                    
                    let JSONDictory = JSON(json ?? " ")
                    let code = JSONDictory["code"].intValue
                    let message = JSONDictory["message"].string
                    print("code====",code)
                    print("CodeURL====",CodeURL)
                    print("parameters====",parameters)
                    if code == 10000{
                        self.btn.isEnabled = false
                        let timer = DispatchSource.makeTimerSource(queue: DispatchQueue.global())
                        timer.schedule(deadline: .now(), repeating: .seconds(1))
                        timer.setEventHandler { [weak self] in
                            guard let self = self else { return }
                            countdown -= 1
                            DispatchQueue.main.async { [self] in
                                self.btn.setTitle("\(countdown)秒后再试", for: .normal)
                            }
                            if countdown <= 0 {
                                timer.cancel()
                                DispatchQueue.main.async {
                                    self.btn.setTitle("获取验证码", for: .normal)
                                    self.btn.isEnabled = true
                                }
                            }
                        }
                        timer.resume()
                    }else{
                        TSProgressHUD.ts_showWarningWithStatus(message ?? "系统出错")
                    }
                    
                    
                }, onError: { (error) in
                    print(error,"=====error")
                    print("parameters == ",parameters)
                })
                .disposed(by: disposBag)
            
        }
    }
    
    @objc func modifyClick(){
        
        if phoneF.text!.isEmpty{
            TSProgressHUD.ts_showWarningWithStatus("请输入手机号")
        }else if phonesetUpF.text!.isEmpty{
            TSProgressHUD.ts_showWarningWithStatus("请输入密码")
        }else if phoneRepeatF.text!.isEmpty{
            TSProgressHUD.ts_showWarningWithStatus("请输入再次密码")
        }else if phoneTwoF.text!.isEmpty{
            TSProgressHUD.ts_showWarningWithStatus("请输入验证码")
        }else{
            let parameters  = ["mobile":phoneF.text!,"code": phoneTwoF.text!,"password2":phonesetUpF.text!,"password":AESCode.MD5(codeString: phonesetUpF.text!),"confirmPassword":AESCode.MD5(codeString: phoneRepeatF.text!)]
            
            AF.request(ChangePasswordUrl, method: .post, parameters:parameters, encoder: JSONParameterEncoder.default).responseData {[self]response in
                let json = try? JSONSerialization.jsonObject(with: response.value!, options: JSONSerialization.ReadingOptions.allowFragments) as? [String:Any]
                let JSONDictory = JSON(json ?? " ")
                let code = JSONDictory["code"].intValue
                let message = JSONDictory["message"].string
                if code == 10000{
                    TSProgressHUD.ts_showWarningWithStatus(message ?? "系统出错")
                    self.navigationController?.popViewController(animated: false)
                }else{
                    TSProgressHUD.ts_showWarningWithStatus(message ?? "系统出错")
                }
            }
            
            
        }
    }
}
