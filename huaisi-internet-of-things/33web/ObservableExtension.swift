//
//  ObservableExtension.swift
//  33web
//
//  Created by 宋海胜 on 2020/12/4.
//

import HandyJSON
import RxSwift

//数据映射错误
public enum RxMapModelError: Error {
    case parsingError
}

//扩展Observable：增加模型映射方法
public extension Observable where Element:Any {
    
    //将JSON数据转成对象
    public func mapModel<T>(type:T.Type) -> Observable<T> where T:HandyJSON {
        return self.map { (element) -> T in
            guard let parsedElement = T.deserialize(from: element as? Dictionary) else {
                throw RxMapModelError.parsingError
            }
            
            return parsedElement
        }
    }
    
    //将JSON数据转成数组
    public func mapModels<T>(type:T.Type) -> Observable<[T]> where T:HandyJSON {
        return self.map { (element) -> [T] in
            guard let parsedArray = [T].deserialize(from: element as? [Any]) else {
                throw RxMapModelError.parsingError
            }
            
            return parsedArray as! [T]
        }
    }
}
extension String {
    public var hexInt: Int? {
        let scanner = Scanner(string: self)
        var value: UInt64 = 0
        guard scanner.scanHexInt64(&value) else { return nil }
        return Int(value)
    }
}


extension UIColor {
    public convenience init(redIn255: Int, greenIn255: Int, blueIn255: Int, alphaIn100: Int = 100) {
        self.init(red: CGFloat(redIn255)/255, green: CGFloat(greenIn255)/255, blue: CGFloat(blueIn255)/255, alpha: CGFloat(alphaIn100)/100)
    }
}


extension UIColor {
    public convenience init?(hex: String) {
        var str = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        let startIndex = str.startIndex
        if str.hasPrefix("#") {
            let indexOffsetBy1 = str.index(startIndex, offsetBy: 1)
            str = String(str[indexOffsetBy1...])
        }
        
        guard str.count == 6 else { return nil }
        
        let indexOffsetBy2 = str.index(startIndex, offsetBy: 2)
        let indexOffsetBy4 = str.index(startIndex, offsetBy: 4)

        var red = String(str[..<indexOffsetBy2])
        var green = String(str[indexOffsetBy2..<indexOffsetBy4])
        var blue = String(str[indexOffsetBy4...])
            
        guard let redIn255 = red.hexInt else { return nil }
        guard let greenIn255: Int = green.hexInt else { return nil }
        guard let blueIn255: Int = blue.hexInt else { return nil }
        
        self.init(redIn255: redIn255, greenIn255: greenIn255, blueIn255: blueIn255)
    }
}
