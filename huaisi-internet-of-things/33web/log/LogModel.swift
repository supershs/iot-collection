//
//  LogModel.swift
//  33web
//
//  Created by JAY on 2023/5/9.
//

import UIKit

import HandyJSON


struct NSRootModel<T: HandyJSON> : HandyJSON{
    ///
    var date: String?
    ///
    var data: T?
    /// SUCCESS
    var status: String?
    /// 10000
    var code: Int = 0
    /// 登录成功
    var message: String?

}

struct LoginModel : HandyJSON {

    ///
       var token: String?
       ///
       var userInfo: NSUserInfoModel?

}
struct NSUserInfoModel : HandyJSON {
    
    /// 16806
       var jobNumber: String?
       /// ADMIN
       var userType: String?
       /// updateBy
       var updateBy: Int = 0
       ///
       var mobile: String?
       ///
       var remarks: String?
       /// <#泛型#>
       var idCard: String?
       /// 前端开发
       var roleName: String?
       /// <#泛型#>
       var lastLoginTime: String?
       ///
       var email: String?
       ///
       var salt: String?
       /// 1
       var sex: Int = 0
       /// id
       var id: String?
       /// PC
       var type: String?
       ///
       var deptName: String?
       /// <#泛型#>
       var createDate: String?
       ///
       var updateDate: String?
       /// <#泛型#>
       var monitorId: String?
       /// <#泛型#>
       var version: String?
       /// <#泛型#>
       var registerDate: String?
       ///
       var headImg: String?
       /// 0
       var sort: Int = 0
       ///
       var uuid: String?
       ///
//       var menuList: [Any]?
       /// <#泛型#>
       var deptId: String?
       ///
       var phone: String?
       /// <#泛型#>
       var deviceId: String?
       /// <#泛型#>
       var brithday: String?
       /// 管理员
       var trueName: String?
       /// admin
       var loginName: String?
       /// 1
       var tenantId: Int = 0
       ///
       var password: String?
       /// 1
       var status: Int = 0
       /// <#泛型#>
       var createBy: String?

}
