//
//  RootViewController.swift
//  studyDay01
//
//  Created by JAY on 2023/2/2.
//

import UIKit

class RootViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
// UIImage(named:"navigationbar_friendattention"),style:UIBarButtonItemStyle.plain,target:self,action:#selector(HomeViewController.KK))
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "qian"), style: .plain, target: self, action: #selector(clickUp))
        

    }
    
    @objc func clickUp(){
        
        self.navigationController?.popViewController(animated: true)
    }
}
