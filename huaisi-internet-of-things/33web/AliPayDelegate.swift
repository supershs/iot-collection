//
//  AliPayDelegate.swift
//  XuYiLobster
//
//  Created by 宋海胜 on 2020/11/2.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit

class AliPayDelegate: NSObject {
    
    class func alipay(_ orderString: String) {
        AlipaySDK.defaultService().payOrder(orderString, fromScheme: "XuYiLobsterScheme"){
            result in
            print(result)
        }
    }

    // 支付宝回调
    class func handleAlipayUrl(_ url: URL, _ currentVC: HSWebViewController) {
        AlipaySDK.defaultService().processOrder(withPaymentResult: url){
            value in
            let code = value!
            let resultStatus = code["resultStatus"] as!String
            var content = ""
            switch resultStatus {
            case "9000":
                content = "支付成功"
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "aliPaySucceess"), object: content)
                self.toAliPayResultPage(currentVC)
            case "8000":
                content = "订单正在处理中"
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "aliPayUnknowStatus"), object: content)
            case "4000":
                content = "支付失败"
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "aliPayDefeat"), object: content)
                self.toAliPayResultPage(currentVC)
            case "5000":
                content = "重复请求"
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "aliPayDefeat"), object: content)
            case "6001":
                content = "中途取消"
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "aliPayDefeat"), object: content)
            case "6002":
                content = "网络连接出错"
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "aliPayDefault"), object: content)
            case "6004":
                content = "支付结果未知"
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "aliPayUnknowStatus"), object: content)
            default:
                content = "支付失败"
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "aliPayDefeat"), object: content)
                self.toAliPayResultPage(currentVC)
                break
            }
        }
    }
    
    class func toAliPayResultPage(_ currentVC: HSWebViewController) {
        currentVC.payResult()
    }
}
