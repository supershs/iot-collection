//
//  LocationManager.swift
//  XUYIProject
//
//  Created by 叁拾叁 on 2020/7/16.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit
import MapKit

public class LocationManager: NSObject, CLLocationManagerDelegate {
    
    public let manager = CLLocationManager()
    public weak var locationProtocol: LocationProtocol?
    public static let Shared = LocationManager()
    public override init() {
        super.init()
        
        
        manager.delegate = self
        //控制定位精度,越高耗电量越
        manager.desiredAccuracy = kCLLocationAccuracyBest
        
        if #available(iOS 8.0, *) {
            manager.requestWhenInUseAuthorization()
            manager.requestAlwaysAuthorization()
        }
    }
    
    public func startLocation()  {
        
        let status  = CLLocationManager.authorizationStatus()
        
        if status == .notDetermined {
            manager.requestWhenInUseAuthorization()
            return
        }
        
        if status == .denied || status == .restricted {
            if let p = locationProtocol, let vc = p as? UIViewController {
                let alert = UIAlertController(title: "提示", message: "获取定位权限失败，请在设置中打开定位", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                    
                }))
                vc.sg_present(alert, animated: true, completion: nil)
            } else {
                locationProtocol?.getGPSAuthorizationFailure()
            }
            return
        }
        manager.stopUpdatingLocation()
        manager.startUpdatingLocation()
    }
    
    public func stopLocation() {
        manager.stopUpdatingLocation()
    }
    
    // CLLocationManagerDelegate
    // 每隔一段时间就会调用
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        for (_, loc) in locations.enumerated() {
            let l: CLLocationCoordinate2D = loc.coordinate
            let lat = l.latitude
            let lng = l.longitude
            locationProtocol?.getGPSSuccess(latitude: lat, longitude: lng)
        }
        
        
        let location = locations.last ?? CLLocation()
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
            
            if error != nil {
                return
            }
            
            if let place = placemarks?[0]{
                
                // 国家 省  市  区  街道  名称  国家编码  邮编
                //                let country = place.country ?? ""
                let administrativeArea = place.administrativeArea ?? ""
                let locality = place.locality ?? ""
                let subLocality = place.subLocality ?? ""
                let thoroughfare = place.thoroughfare ?? ""
                let name = place.name ?? ""
                
                //                let isoCountryCode = place.isoCountryCode ?? ""
                //                let postalCode = place.postalCode ?? ""
                
                let addressLines =  administrativeArea + locality + subLocality + thoroughfare + name
                self.locationProtocol?.getLocationSuccess(administrativeArea,locality,subLocality,thoroughfare,name)
            } else {
                
                self.locationProtocol?.getLocationSuccess("","","","","")
            }
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        print("无法获取位置信息 \(error.localizedDescription)")
        locationProtocol?.getGPSFailure(error: error)
    }
    
}
