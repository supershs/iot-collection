//
//  JDLoginViewController.swift
//  33web
//
//  Created by iOS开发 on 2023/3/9.
//

import UIKit
import SnapKit
import RxSwift
import RxAlamofire
import HandyJSON
import Toast
import SwiftyJSON
import Alamofire
  
class JDLoginViewController: UIViewController,UITextFieldDelegate {
//    let disposeBag = DisposeBag()
    let disposBag = DisposeBag()
    var showAll : Bool!
    let minUsernameLength = 1
    let maxUsernameLength = 11
    let minPasswordLength = 1
    let maxPasswordLength = 30
    
    
    var usernameTF: UITextField = {
        let tf = UITextField()
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.textColor = .black
       
        tf.placeholder = "请输入账号"
        return tf
    }()
    

    var passwordTF: UITextField = {
        let tf = UITextField()
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.textColor = .black
        tf.placeholder = "请输入密码"
        tf.isSecureTextEntry = true
        return tf
    }()
    
  
    var loginButton: UIButton = {
        let v = UIButton()
        v.backgroundColor = UIColor.white
        v.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        v.setTitleColor( .white, for: .normal)
        v.backgroundColor = UIColor(hex: "#1683e9")
        v.setBackgroundImage(UIImage(named: "login_btn"), for: .normal)
        v.setTitle("登录", for: .normal)
        v.layer.cornerRadius = 5
        v.layer.masksToBounds = true
        return v
    }()
    
    var eyeButton: UIButton = {
        let v = UIButton()
        v.setBackgroundImage(UIImage(named: "yan"), for: .normal)
        return v
    }()
    
    var logoImgView: UIImageView = {
        let v = UIImageView()
        v.image = UIImage(named: "login_head")
        return v
    }()
    var accountImgView: UIImageView = {
        let v = UIImageView()
        v.image = UIImage(named: "account")

        return v
    }()
    var passwordImgView: UIImageView = {
        let v = UIImageView()
        v.image = UIImage(named: "password")
        return v
    }()
    var accountSepView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.init(hex: "f5f5f5")
        return v
    }()
    var passwordSepView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.init(hex: "f5f5f5")
        return v
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showAll = false
        view.addSubview(logoImgView)
        view.addSubview(accountImgView)
        view.addSubview(passwordImgView)
        view.addSubview(passwordTF)
        view.addSubview(usernameTF)
        view.addSubview(accountSepView)
        view.addSubview(passwordSepView)
        view.addSubview(loginButton)
        view.addSubview(eyeButton)
        usernameTF.delegate = self
        passwordTF.delegate = self
        loginButton.addTarget(self, action: #selector(logOnClick), for: .touchUpInside)
        eyeButton.addTarget(self, action: #selector(lookBtnClick), for: .touchUpInside)
        
        view.backgroundColor = .white
        logoImgView.snp.makeConstraints { make in
            make.top.left.right.equalToSuperview()
            make.height.equalTo(323)
        }

        accountImgView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(25)
            make.top.equalTo(logoImgView.snp.bottom).offset(51)
            make.width.equalTo(18)
            make.height.equalTo(22)
        }
      
        usernameTF.snp.makeConstraints { make in
            make.left.equalTo(accountImgView.snp.right).offset(10)
            make.top.equalTo(logoImgView.snp.bottom).offset(43)
            make.right.equalToSuperview().offset(-25)
            make.height.equalTo(40)
        }
    
        accountSepView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(25)
            make.top.equalTo(usernameTF.snp.bottom).offset(1)
            make.right.equalToSuperview().offset(-25)
            make.height.equalTo(1)
        }
        
        passwordImgView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(25)
            make.top.equalTo(accountSepView.snp.bottom).offset(60)
            make.width.equalTo(18)
            make.height.equalTo(20)
        }
        
        passwordTF.snp.makeConstraints { make in
            make.left.equalTo(passwordImgView.snp.right).offset(10)
            make.top.equalTo(accountSepView.snp.bottom).offset(50)
            make.right.equalToSuperview().offset(-25)
            make.height.equalTo(40)
        }
        
        eyeButton.snp.makeConstraints { make in
            make.right.equalTo(passwordTF.snp.right).offset(10)
            make.centerY.equalTo(passwordTF)
            make.height.equalTo(12)
            make.width.equalTo(20)
        }
     
        passwordSepView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(25)
            make.top.equalTo(passwordTF.snp.bottom).offset(1)
            make.right.equalToSuperview().offset(-25)
            make.height.equalTo(1)
        }
        loginButton.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(25)
            make.top.equalTo(passwordSepView.snp.bottom).offset(50)
            make.right.equalToSuperview().offset(-25)
            make.height.equalTo(44)
        }
    }
    
//    //    MARK: - 登录
    @objc func logOnClick(){

        if usernameTF.text!.isEmpty {
            TSProgressHUD.ts_showWarningWithStatus("请输入手机号")
        }else if passwordTF.text!.isEmpty {
            TSProgressHUD.ts_showWarningWithStatus("请输入密码")
        }else{

            goLogin(loginName: "", password: "")

        }
    }
    
    
    func goLogin(loginName: String, password: String){
//        let parameters  = ["loginName":usernameTF.text!,"password":passwordTF.text!,"url":appURL]
        let parameters  = ["loginName":usernameTF.text!,"password":AESCode.endcode_AES_CBC(strToEncode: passwordTF.text!),"url":appURL] as [String : Any]
        RxAlamofire.requestJSON(.post, URL(string: logoURL)!, parameters: parameters, encoding: JSONEncoding.default, headers: ["Content-Type":"application/json;charset=UTF-8"])
                  .debug()
                                                                                                        
                  .subscribe(onNext: { (r, json) in
                    if let dict = json as? [String: AnyObject] {
                        
                        print(">>>>>>>>>>>>>>>>>>>dict>>>>>>>>>>>>>>>>>>>>>>",json)
                     
                        let modelA = NSRootModel<LoginModel>.deserialize(from: dict)
                        if modelA?.status == "SUCCESS"  {
                            print(">>>>>>>>>>>>>>>>>>>自己的打印>>>>>>>>>>>>>>>>>>>>>>")
                            let urlStr =  GOTOHOME  + (modelA?.data?.toJSONString() ?? "")
                            self.navigationController?.pushViewController(HSWebViewController(path: kWebUrl + urlStr.getEncodeString), animated: true)

                        }else{
//                            HUDUtil.showBlackTextView(text: modelA?.message, detailText:"", delay: 1.5) {
                            }

                        }
                      
                  }, onError: { (error) in
                        print(error,"=====error")
                  })
                  .disposed(by: disposBag)
    }
    
    //    MARK: -  查看密码
    @objc func lookBtnClick(){
       
        if showAll{
            showAll = false
            passwordTF.isSecureTextEntry = true
        }else{
            showAll = true
            passwordTF.isSecureTextEntry = false
        }
        
    }
    
    
    //textField点击return关闭键盘   
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view?.endEditing(false)
        return true
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
        
    }
                                     
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
        
    }

}

extension String {
    /// String转encode
        var getEncodeString: String {
            guard self.count != 0 else { return ""}
            if let u = self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                return u
            }
            return ""
        }
}

extension String {
    public var hexInt: Int? {
        let scanner = Scanner(string: self)
        var value: UInt64 = 0
        guard scanner.scanHexInt64(&value) else { return nil }
        return Int(value)
    }
}


extension UIColor {
    public convenience init(redIn255: Int, greenIn255: Int, blueIn255: Int, alphaIn100: Int = 100) {
        self.init(red: CGFloat(redIn255)/255, green: CGFloat(greenIn255)/255, blue: CGFloat(blueIn255)/255, alpha: CGFloat(alphaIn100)/100)
    }
}


extension UIColor {
    public convenience init?(hex: String) {
        var str = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        let startIndex = str.startIndex
        if str.hasPrefix("#") {
            let indexOffsetBy1 = str.index(startIndex, offsetBy: 1)
            str = String(str[indexOffsetBy1...])
        }
        
        guard str.count == 6 else { return nil }
        
        let indexOffsetBy2 = str.index(startIndex, offsetBy: 2)
        let indexOffsetBy4 = str.index(startIndex, offsetBy: 4)

        var red = String(str[..<indexOffsetBy2])
        var green = String(str[indexOffsetBy2..<indexOffsetBy4])
        var blue = String(str[indexOffsetBy4...])
            
        guard let redIn255 = red.hexInt else { return nil }
        guard let greenIn255: Int = green.hexInt else { return nil }
        guard let blueIn255: Int = blue.hexInt else { return nil }
        
        self.init(redIn255: redIn255, greenIn255: greenIn255, blueIn255: blueIn255)
    }
}
