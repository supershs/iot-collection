//
//  LogModel.swift
//  33web
//
//  Created by JAY on 2023/5/9.
//

import UIKit

import HandyJSON


struct NSRootModel<T: HandyJSON> : HandyJSON{
    ///
    var date: String?
    ///
    var data: T?
    /// SUCCESS
    var status: String?
    /// 10000
    var code: Int = 0
    /// 登录成功
    var message: String?
    


}

struct LoginModel : HandyJSON {
    ///
    var userInfo: NSUserInfoModel?
    ///
    var token: String?
  

}
struct NSUserInfoModel : HandyJSON {
    /// <#泛型#>
    var lastLoginTime: String?
    /// <#泛型#>
    var topDeptId: String?
    /// 管理员
    var roleName: String?
    /// <#泛型#>
    var brithday: String?
    /// <#泛型#>
    var menuList: String?
    /// <#泛型#>
    var mobile: String?
    /// <#泛型#>
    var topDeptName: String?
    ///
    var password: String?
    /// <#泛型#>
    var roleList: String?
    /// admin
    var loginName: String?
    /// 农业农村局
    var deptName: String?
    ///
    var headImg: String?
    /// <#泛型#>
    var phone: String?
    /// <#泛型#>
    var enterId: String?
    /// <#泛型#>
    var sex: String?
    /// <#泛型#>
    var idCard: String?
    /// 1
    var roleId: String?
    /// <#泛型#>
    var deptId: String?
    /// false
    var authentication: Bool = false
    ///
    var userToken: String?
    /// <#泛型#>
    var tenantUserId: String?
    /// 1
    var status: Int = 0
    ///
    var salt: String?
    /// 1
    var userId: String?
    /// 165DAA
    var mainColor: String?
    /// <#泛型#>
    var email: String?
    /// 1
    var tenantId: String?
    /// ADMIN
    var userType: String?
    /// <#泛型#>
    var registerDate: String?
    /// 1
    var id: String?
    /// 管理员
    var trueName: String?
    /// 游客16250387
    var nickName: String?
}
