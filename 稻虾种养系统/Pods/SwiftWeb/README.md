# SwiftWeb

[![CI Status](https://img.shields.io/travis/叁拾叁/SwiftWeb.svg?style=flat)](https://travis-ci.org/叁拾叁/SwiftWeb)
[![Version](https://img.shields.io/cocoapods/v/SwiftWeb.svg?style=flat)](https://cocoapods.org/pods/SwiftWeb)
[![License](https://img.shields.io/cocoapods/l/SwiftWeb.svg?style=flat)](https://cocoapods.org/pods/SwiftWeb)
[![Platform](https://img.shields.io/cocoapods/p/SwiftWeb.svg?style=flat)](https://cocoapods.org/pods/SwiftWeb)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SwiftWeb is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'SwiftWeb'
```

## Author

叁拾叁, danzelwm@126.com

## License

SwiftWeb is available under the MIT license. See the LICENSE file for more info.
