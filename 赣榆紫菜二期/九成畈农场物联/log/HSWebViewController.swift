//
//  HSWebViewController.swift
//  33web
//
//  Created by 宋海胜 on 2020/11/25.
//

import UIKit
import SwiftWeb
import WebKit

class HSWebViewController: BaseWebViewController {

    fileprivate var backImgView = UIImageView()
    fileprivate var configManager = Tool()
    
    deinit {
      
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "loginOut")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "webGoBack")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        judgeNetwork()
        self.backImgView = self.configManager.getLanuchboardImage()
        self.backImgView.frame = self.view.bounds
        self.view.addSubview(self.backImgView)
        self.configManager.topMangain(webView: webView, vc: self)
        self.webView.frame = CGRect(x: 0, y: kStatusBarHeight, width: kScreenWidth, height: kScreenHeight - kStatusBarHeight)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func injectSwiftMethod(_ config: WKWebViewConfiguration) {
     
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "loginOut")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "webGoBack") 
        
    }
     
    open override func swiftAction(_ name: String, _ body: Dictionary<String, Any>) {
        
        switch name {
                
            case "loginOut":
            //            UserInstance.userLogout()
                        self.navigationController?.popToRootViewController(animated: false)
                
            case "webGoBack":
            //            UserInstance.userLogout()
                        self.navigationController?.popToRootViewController(animated: false)
            
              
        default:
            break
        }
    }
    
    
    // 在viewDidLoad中模态不行，此时界面还没加载完成
    fileprivate func judgeNetwork() {
        self.loadWeb()
        let myAppdelegate = UIApplication.shared.delegate as! AppDelegate
        myAppdelegate.networkAuthStatus(stateClosure: {[weak self] state in
            if let `self` = self {
                if state {
                    DispatchQueue.main.async {
//                        HUDUtil.showHud()
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        DispatchQueue.main.async {
//                            HUDUtil.hideHud()
                        }
                        self.loadWeb()
                    }
                }
            }
        })
    }
    override func loadSuccess() {
        
        UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseInOut, animations: {
            self.backImgView.alpha = 0
        }) { (finish) in
            self.backImgView.isHidden = true
        }
    }
}
