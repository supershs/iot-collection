//
//  LogModel.swift
//  33web
//
//  Created by JAY on 2023/5/9.
//

import UIKit

import HandyJSON


struct NSRootModel<T: HandyJSON> : HandyJSON{
    ///
    var date: String?
    ///
    var data: T?
    /// SUCCESS
    var status: String?
    /// 10000
    var code: Int = 0
    /// 登录成功
    var message: String?

}

struct LoginModel : HandyJSON {
 
    /// <#泛型#>
       var roleName: Any?
       /// <#泛型#>
       var sysTenantUserVO: Any?
       /// <#泛型#>
       var recentOnLineTime: Any?
       /// <#泛型#>
       var sysPostVO: Any?
       /// <#泛型#>
       var tenantMenuVO: Any?
       /// <#泛型#>
       var createDate: Any?
       /// 超级管理员
       var trueName: String?
       /// <#泛型#>
       var sort: Any?
       /// <#泛型#>
       var updateDate: Any?
       /// <#泛型#>
       var sysDeptPostVO: Any?
       /// <#泛型#>
       var roleId: Any?
       /// <#泛型#>
       var postAll: Any?
       /// <#泛型#>
       var loginTime: Any?
       /// <#泛型#>
       var tenant: Any?
       /// 0
       var isEnter: Int = 0
       ///
       var token: String?
       /// <#泛型#>
       var status: Any?
       /// 29
       var age: Int = 0
       /// <#泛型#>
       var version: Any?
       /// <#泛型#>
       var userProfileVO: Any?
       /// 男
       var sex: String?
       /// <#泛型#>
       var menuRoleVO: Any?
       /// <#泛型#>
       var birthDayStr: Any?
       /// <#泛型#>
       var remarks: Any?
       /// <#泛型#>
       var tenantId: Any?
       /// <#泛型#>
       var uuid: Any?
       /// <#泛型#>
       var appTenantUserVO: Any?
       /// <#泛型#>
       var salt: Any?
       /// 30
       var id: String?
       /// 13382330000
       var mobile: String?
       /// <#泛型#>
       var deptPostVOList: Any?
       /// <#泛型#>
       var updateBy: Any?
       /// <#泛型#>
       var deptVO: Any?
       /// 超级管理员
       var nickName: String?
       /// <#泛型#>
       var loginName: Any?
       /// <#泛型#>
       var createBy: Any?
       /// <#泛型#>
       var password: Any?
       /// <#泛型#>
       var educationVOS: Any?
       ///
       var headImg: String?

     

    
}
