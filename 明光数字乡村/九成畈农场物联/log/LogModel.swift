//
//  LogModel.swift
//  33web
//
//  Created by JAY on 2023/5/9.
//

import UIKit

import HandyJSON


struct NSRootModel<T: HandyJSON> : HandyJSON{
    ///
    var date: String?
    ///
    var data: T?
    /// SUCCESS
    var status: String?
    /// 10000
    var code: Int = 0
    /// 登录成功
    var message: String?

}

struct LoginModel : HandyJSON {
   
    // 张庭
       var nickName: String?
       /// 17712905327
       var loginName: String?
       /// <#泛型#>
       var locked: Any?
       ///
       var userId: String?
       /// <#泛型#>
       var headImg: Any?
       /// <#泛型#>
       var educationVOS: Any?
       /// <#泛型#>
       var age: Any?
       ///
       var certificationId: String?
       /// USER
       var userType: String?
       /// <#泛型#>
       var birthDayStr: Any?
       ///
       var token: String?
       /// 张庭
       var trueName: String?
       /// <#泛型#>
       var sex: Any?
       /// 1
       var tenantId: Int = 0
       /// <#泛型#>
       var sysTenantUserVO: Any?
       /// <#泛型#>
       var recentOnLineTime: Any?
       /// <#泛型#>
       var userProfileVO: Any?
       /// <#泛型#>
       var mobile: Any?
       /// enterprise
       var kindType: String?



}
