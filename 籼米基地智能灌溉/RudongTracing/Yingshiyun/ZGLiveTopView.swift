//
//  ZGLiveTopView.swift
//  ZhangGuang
//
//  Created by 叁拾叁 on 2020/7/20.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit
import SwiftWeb

class ZGLiveTopView: UIView {

    var backClosure: (() -> Void)?
    var beisuClosure: (() -> Void)?
    
    let topView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor(white: 0, alpha: 0.5)
        return v
    }()
    
    let backImgView: UIImageView = {
        let v = UIImageView()
        v.image = UIImage(named: "back")
        return v
    }()
    
    let backBt: UIButton = {
        let v = UIButton()
        v.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        v.titleEdgeInsets = UIEdgeInsets(top: 1, left: 20, bottom: 0, right: 0)
        return v
    }()
    
    let beisuBt: UIButton = {
        let v = UIButton()
        v.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        v.setTitle("倍速", for: .normal)
        v.setTitleColor(.white, for: .normal)
        v.isHidden = true
        return v
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    fileprivate func initViews() {
        
        self.addSubview(topView)
        topView.addSubview(backBt)
        topView.addSubview(beisuBt)
        backBt.addSubview(backImgView)
        backBt.addTarget(self, action: #selector(backBtAction), for: .touchUpInside)
        beisuBt.addTarget(self, action: #selector(beisuBtAction), for: .touchUpInside)
        
        topView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.height.equalToSuperview()
        }
        
        backBt.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(10)
            make.left.equalToSuperview().offset(15)
            make.width.equalTo(100)
            make.height.equalTo(40)
        }
        
        beisuBt.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(25)
            make.width.equalTo(100)
            make.height.equalTo(40)
        }
        
        backImgView.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview()
            make.width.equalTo(8)
            make.height.equalTo(14)
        }
    }
    
    @objc func backBtAction() {
        if let b = backClosure {
            b()
        }
    }
    
    @objc func beisuBtAction() {
        if let b = beisuClosure {
            b()
        }
    }
}


class ZGLiveBeisuChoiceView: UIView, UITableViewDelegate, UITableViewDataSource {
    
    var titles: [String] = ["1/8", "1/4", "1/2", "1", "2", "4", "8"]
//    var titles: [String] = ["1/8", "1/4", "1/2", "1"]
    var tableView: UITableView!
    var beisuChoiceClosure: ((Int) -> Void)?
    var lastBeisu: Int = 3
    let backBt: UIButton = {
        let v = UIButton()
        return v
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    fileprivate func initViews() {
        
        self.backgroundColor = .clear
        tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.separatorStyle = .none
        self.tableView.layer.cornerRadius = 5
        self.tableView.layer.masksToBounds = true
        self.tableView.backgroundColor = UIColor(white: 0, alpha: 0.5)
        self.tableView!.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        backBt.addTarget(self, action: #selector(backBtAction), for: .touchUpInside)
        self.addSubview(backBt)
        self.addSubview(tableView!)
        backBt.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        tableView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(55)
            make.left.equalToSuperview().offset(kScreenHeight - 150)
            make.width.equalTo(100)
            make.height.equalTo(kScreenWidth - 100)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        cell?.textLabel?.text = titles[indexPath.row]
        cell?.textLabel?.font = UIFont.systemFont(ofSize: 14)
        if lastBeisu == indexPath.row {
            cell?.textLabel?.textColor = .green
        } else {
            cell?.textLabel?.textColor = .white
        }
        
        cell?.backgroundColor = .clear
        cell?.selectionStyle = .none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if lastBeisu == indexPath.row {
            return
        }
        lastBeisu = indexPath.row
        if let b = beisuChoiceClosure {
            b(indexPath.row)
            tableView.reloadData()
            self.isHidden = true
        }
    }
    
    @objc func backBtAction() {
        self.isHidden = true
    }
}
