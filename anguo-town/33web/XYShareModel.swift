//
//  XYShareModel.swift
//  XuYiLobster
//
//  Created by 叁拾叁 on 2020/8/5.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit

enum ShareContentType {
    case image
    case url
}

class XYShareModel: NSObject {

//    var sharePlatformType: UMSocialPlatformType?
    var shareContentType: ShareContentType?
    var shareImage: UIImage?
    var shareTitle: String?
    var shareDescr: String?
    var shareUrl: String?
    var currentVC: UIViewController?
}
