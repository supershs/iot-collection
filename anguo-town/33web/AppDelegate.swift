//
//  AppDelegate.swift
//  33web
//
//  Created by 宋海胜 on 2020/11/25.
//

import UIKit
import AFNetworking
import CallKit

#if DEBUG // 判断是否在测试环境下
let kWebUrl = "http://114.115.166.192:8104/#/"
//let kWebUrl = "http://192.168.0.196:8080/#/"
#else
let kWebUrl = "http://114.115.166.192:8104/#/"
//let kWebUrl = "http://192.168.0.196:8080/#/"
#endif

let logoURL = "http://114.115.166.192:8103/api/1.0/Login/app/checkLogin" //登录接口
let GOTOHOME = "pages/tabBar/homePage?data="
let appURL = "33app.33iot.com"


// 萤石云控制接口
let kVideoStartPTZ = "https://open.ys7.com/api/lapp/device/ptz/start"
let kVideoStopPTZ = "https://open.ys7.com/api/lapp/device/ptz/stop"
let kGetAccessToken = "https://open.ys7.com/api/lapp/token/get"
// 图片上传接口（暂无此功能）
let kImageApi = "http://119.3.169.92:9488"
let kImageUpload = "\(kImageApi)/sysFile/batchUploadImg"
let kImageInfoUpload = "\(kImageApi)/api/1.1/CityCapture/savePic"

// 友盟
let kUMAppKey = "5f8d054180455950e4ae04ed"//"5e9d880ddbc2ec07ad295685"
// 微信
let kWechatAppId = "wxe315c4d7125111e6"
let kWechatSecrectKey = "5424cea7cb28690d3ad4e7f680b236ea"
let kWechatLinks = "https://help.wechat.com/XuYi/"

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var canAllButUpsideDown: Bool = false
    var window: UIWindow?
    var webVC = HSWebViewController(path: kWebUrl)
    var logVC = JDLoginViewController()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //设置用户授权显示通知
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.badge,.carPlay,.sound], completionHandler: { (success, error) in
                print("通知授权" + (success ? "成功" : "失败"))
            })
        }
        
        window = UIWindow(frame: UIScreen.main.bounds)
        let nav = ZXNavigationController (rootViewController: logVC)
        self.window?.rootViewController = nav
        window!.makeKeyAndVisible()
        
        // 强制关闭暗黑模式
        if #available(iOS 13.0, *) {
            self.window?.overrideUserInterfaceStyle = .light
        }
        #if DEBUG // 判断是否在测试环境下
        print("当前环境：DEBUG")
        #else
        print("当前环境：RELEASE")
        #endif
        return true
    }


    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        if canAllButUpsideDown {
            return UIInterfaceOrientationMask.allButUpsideDown
        } else {
            return UIInterfaceOrientationMask.portrait
        }
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        webVC.liveView.stopLiveVideo()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        webVC.liveView.startLiveVideo()
    }
    
    func watchNetwork() {

        AFNetworkReachabilityManager.shared().startMonitoring()
        AFNetworkReachabilityManager.shared().setReachabilityStatusChange { (status) in
            switch status {
            case .unknown:
                print("watchNetwork: 未知")
                self.webVC.liveView.stopLiveVideo()
                
            case .notReachable:
                print("watchNetwork: 未连接")
                self.webVC.liveView.stopLiveVideo()
                
            case .reachableViaWiFi:
                print("watchNetwork: wifi连接")
                self.webVC.liveView.reloadVideo()
                
            case .reachableViaWWAN:
                print("watchNetwork: wwan连接")
                self.webVC.liveView.reloadVideo()
            }
        }
    }
    
    // 电话状态监听
    func callObserver(_ callObserver: CXCallObserver, callChanged call: CXCall) {
        
        print("电话状态监听: \n      call.isOutgoing: \(call.isOutgoing) \n      call.isOnHold: \(call.isOnHold) \n      call.hasEnded: \(call.hasEnded) \n      call.hasConnected: \(call.hasConnected) \n      call.uuid: \(call.uuid)")
        
        if call.isOutgoing {// 拨打
            if call.hasConnected {
                if call.hasEnded {
                    print("watchCallStatus: 拨打->接通->结束")
                    webVC.liveView.startLiveVideo()
                } else {
                    print("watchCallStatus: 拨打->接通")
                }
            } else if call.hasEnded {
                print("watchCallStatus: 拨打->结束")
                webVC.liveView.startLiveVideo()
            } else {
                print("watchCallStatus: 拨打")
            }
        } else if call.isOnHold {// 待接通
            print("watchCallStatus: 待接通")
        } else if call.hasConnected {// 接通
            if call.hasEnded {// 结束
                webVC.liveView.startLiveVideo()
                print("watchCallStatus: 接通->结束")
            } else {
                print("watchCallStatus: 接通")
            }
        } else if call.hasEnded {// 结束
            webVC.liveView.startLiveVideo()
            print("watchCallStatus: 结束")
        } else {
            print("watchCallStatus: \(call.uuid)")
            webVC.liveView.stopLiveVideo()
        }
    }
}


