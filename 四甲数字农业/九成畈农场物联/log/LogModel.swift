//
//  LogModel.swift
//  33web
//
//  Created by JAY on 2023/5/9.
//

import UIKit

import HandyJSON


struct NSRootModel<T: HandyJSON> : HandyJSON{
    ///
    var date: String?
    ///
    var data: T?
    /// SUCCESS
    var status: String?
    /// 10000
    var code: Int = 0
    /// 登录成功
    var message: String?

}

struct LoginModel : HandyJSON {

    /// <#泛型#>
    var companyInfo: Any?
    /// 1
    var userId: String?
    ///
    var token: String?
    /// 管理员
    var trueName: String?
    /// 1
    var tenantId: Int = 0
    /// 管理员
    var nickName: String?
    ///
    var sysDeptPostVO: NSSysDeptPostVOModel?
    ///
    var headImg: String?
    ///
    var sysTenantUserVO: NSSysTenantUserVOModel?
    /// ADMIN
    var userType: String?
    /// admin
    var loginName: String?
    /// <#泛型#>
    var registrationId: Any?
    /// false
    var outsourceCode: Bool = false

}

struct NSSysDeptPostVOModel : HandyJSON {

    /// 1
       var deptId: String?
       ///
       var updateDate: String?
       /// <#泛型#>
       var sysPostVO: Any?
       /// 1
       var tenantId: String?
       /// <#泛型#>
       var deptName: Any?
       /// 1
       var status: Int = 0
       /// 1
       var createBy: String?
       /// <#泛型#>
       var remarks: Any?
       /// <#泛型#>
       var dockPostId: Any?
       /// <#泛型#>
       var sysDeptVO: Any?
       /// id
       var id: String?
       /// true
       var principal: Bool = false
       /// <#泛型#>
       var dockDeptId: Any?
       /// <#泛型#>
       var postName: Any?
       /// <#泛型#>
       var companyPoint: Any?
       ///
       var uuid: String?
       /// <#泛型#>
       var sysUserDTO: Any?
       ///
       var createDate: String?
       /// 1
       var version: Int = 0
       /// 1
       var organiId: String?
       /// <#泛型#>
       var dockUserId: Any?
       /// <#泛型#>
       var deptLevel: Any?
       /// <#泛型#>
       var operationPrincipal: Any?
       /// 1
       var userId: String?
       /// 1
       var postId: String?
       /// <#泛型#>
       var outsourceCode: Any?
       /// 1
       var updateBy: String?
       /// 1
       var sort: Int = 0

      


}


struct NSSysTenantUserVOModel : HandyJSON {

    /// <#泛型#>
       var profileVO: Any?
       /// 1
       var userId: String?
       /// 1
       var createBy: String?
       ///
       var updateDate: String?
       /// 1
       var status: Int = 0
       /// 1
       var sort: Int = 0
       /// false
       var locked: Bool = false
       /// 1
       var tenantId: String?
       /// <#泛型#>
       var tenantVO: Any?
       /// 31
       var version: Int = 0
       ///
       var loginTime: String?
       /// <#泛型#>
       var userVO: Any?
       /// ADMIN
       var userType: String?
       /// <#泛型#>
       var uuid: Any?
       /// <#泛型#>
       var remarks: Any?
       /// 1
       var updateBy: String?
       /// SYS_PASSWORD
       var loginType: String?
       /// 1
       var id: String?
       /// 165DAA
       var mainColor: String?
       /// <#泛型#>
       var registrationId: Any?
       /// <#泛型#>
       var createDate: Any?

      


  
}
