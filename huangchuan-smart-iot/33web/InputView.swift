//
//  InputView.swift
//  33web
//
//  Created by 宋海胜 on 2020/12/3.
//

import UIKit
import SwiftWeb

class InputView: UIView {

    fileprivate var urlTf: UITextField!
    fileprivate var loadBtn: UIButton!
    fileprivate var currentVC:HSWebViewController!
    
    init(frame: CGRect, currentVC: HSWebViewController) {
        super.init(frame: frame)
        self.currentVC = currentVC
        initViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initViews() {
        
        urlTf = UITextField(frame: CGRect(x: 0, y: 50, width: kScreenWidth - 100, height: 50))
        urlTf.placeholder = "请输入新地址"
        urlTf.textColor = .black
        urlTf.font = UIFont.systemFont(ofSize: 14)
        urlTf.backgroundColor = UIColor.white
        urlTf.layer.borderWidth = 1
        urlTf.layer.borderColor = UIColor.lightGray.cgColor
        self.addSubview(urlTf)
        loadBtn = UIButton()
        loadBtn.frame = CGRect(x: kScreenWidth - 100, y: 50, width: 100, height: 50)
        loadBtn.backgroundColor = .purple
        loadBtn.addTarget(self, action: #selector(loadBtnAction), for: .touchUpInside)
        loadBtn.setTitle("加载", for: .normal)
        loadBtn.setTitleColor(.white, for: .normal)
        self.addSubview(loadBtn)
    }
    
    @objc func loadBtnAction() {
        if let url = URL(string: self.urlTf.text!) {
            currentVC.webView.load(URLRequest(url: url))//load需在addsubview之后
            self.isHidden = true
        } else {
            currentVC.view.makeToast("请输入正确地址")
        }
    }
}
