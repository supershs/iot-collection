//
//  AppDelegate.swift
//  九成畈农场物联
//
//  Created by JAY on 2023/6/21.
//

import UIKit
import CallKit
import CoreTelephony
import IQKeyboardManagerSwift
import WebKit

//线上
let IP = "http://123.60.179.174:9030/"
let VERSION = "api/1.0/"
// 线上服务器
let kWebUrl = "http://123.60.179.174:9032/#/"
// 本地
//let kWebUrl = "http://192.168.0.53:8081/#/"
//账号登录
let accountUrl = IP + VERSION + "auth/app/login"
//参数线上
let paraUrl = "http://192.168.0.188:9002"


//验证码登录
let codeLoginUrl = IP + VERSION + "SysLogin/message/login"
//忘记密码验证码
let CodeURL = IP + VERSION + "auth/message/push"
//忘记密码修改成功
let ChangePasswordUrl = IP + VERSION + "auth/resetPassword"

let GOTOHOME = "pages/index/iotManage?data="
//协议
let agreementURL = "pages/base-login/userAgreement"


// 萤石云控制接口
let kVideoStartPTZ = "https://open.ys7.com/api/lapp/device/ptz/start"
let kVideoStopPTZ = "https://open.ys7.com/api/lapp/device/ptz/stop"
let kGetAccessToken = "https://open.ys7.com/api/lapp/token/get"
// 图片上传接口（暂无此功能）
let kImageApi = "http://119.3.169.92:9488"
let kImageUpload = "\(kImageApi)/sysFile/batchUploadImg"
let kImageInfoUpload = "\(kImageApi)/api/1.1/CityCapture/savePic"

// 友盟
let kUMAppKey = "5f8d054180455950e4ae04ed"//"5e9d880ddbc2ec07ad295685"
// 微信
let kWechatAppId = "wxe315c4d7125111e6"
let kWechatSecrectKey = "5424cea7cb28690d3ad4e7f680b236ea"
let kWechatLinks = "https://help.wechat.com/XuYi/"

@main
class AppDelegate: UIResponder, UIApplicationDelegate {


    var isFullScreen: Bool = false
    var window: UIWindow?
    var webVC = HSWebViewController(path: kWebUrl)
//    var wxDelegate = WXDelegate()
    var canAllButUpsideDown: Bool = false
    var logVC = LogOnViewController()
    var record: String = "0"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        window = UIWindow(frame: UIScreen.main.bounds)
        window?.backgroundColor = .white
        UIApplication.shared.applicationSupportsShakeToEdit = false
        let nav = ZXNavigationController (rootViewController: logVC)
        self.window?.rootViewController = nav
        window!.makeKeyAndVisible()
        IQKeyboardManager.shared.enable = true

        // 强制关闭暗黑模式
        if #available(iOS 13.0, *) {
            self.window?.overrideUserInterfaceStyle = .light
        }
        #if DEBUG // 判断是否在测试环境下
        print("当前环境：DEBUG")
        #else
        print("当前环境：RELEASE")
        #endif
        return true
    }

    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        if isFullScreen {
            if #available(iOS 16.0, *) {
                // 16 及以上能够做到依据屏幕方向适配横屏
                return .allButUpsideDown
            } else {
                // 16 以下不方便做, 所以咱们是强制 右横屏
                return .landscapeRight
            }
        }
        return .portrait
    }

    func networkAuthStatus(stateClosure: @escaping ((Bool) -> Void)) {
        let cellularData = CTCellularData()
        cellularData.cellularDataRestrictionDidUpdateNotifier = {(state) in
            if (state == .restricted) {
                //拒绝
                if let t = UserDefaults.standard.value(forKey: "isFirstInApp") as? Bool, !t {
                    //                    self.networkSettingAlert()
                }
                stateClosure(false)
            } else if (state == .notRestricted) {
                //允许
                stateClosure(true)
            } else {
                //未知
                if let t = UserDefaults.standard.value(forKey: "isFirstInApp") as? Bool, !t {
                    //                    self.unknownNetwork()
                }
                stateClosure(false)
            }
        }
    }

}

