//
//  LogModel.swift
//  33web
//
//  Created by JAY on 2023/5/9.
//

import UIKit

import HandyJSON


struct NSRootModel<T: HandyJSON> : HandyJSON{
    ///
    var date: String?
    ///
    var data: T?
    /// SUCCESS
    var status: String?
    /// 10000
    var code: Int = 0
    /// 登录成功
    var message: String?

}
struct LoginModel : HandyJSON {
 
    /// 1
        var userId: String?
        /// <#泛型#>
        var age: Any?
        /// <#泛型#>
        var locked: Any?
        /// 1
        var tenantId: Int = 0
        /// <#泛型#>
        var birthDayStr: Any?
        /// admin
        var loginName: String?
        /// 吴家俊
        var trueName: String?
        /// <#泛型#>
        var userProfileVO: Any?
        /// <#泛型#>
        var sex: Any?
        /// 18352603216
        var mobile: String?
        /// ADMIN
        var userType: String?
        /// <#泛型#>
        var educationVOS: Any?
        /// <#泛型#>
        var recentOnLineTime: Any?
        /// 管理员
        var nickName: String?
        ///
        var sysTenantUserVO: NSSysTenantUserVOModel?
        ///
        var headImg: String?
        ///
        var token: String?


    
}
struct NSTenantModel : HandyJSON {

    /// <#泛型#>
    var name: Any?
    /// <#泛型#>
    var remarks: Any?
    /// <#泛型#>
    var createBy: Any?
    /// <#泛型#>
    var updateDate: Any?
    /// <#泛型#>
    var updateBy: Any?
    /// <#泛型#>
    var hasApp: Any?
    /// <#泛型#>
    var createDate: Any?
    /// <#泛型#>
    var sort: Any?
    /// ADMIN
    var type: String?
    /// <#泛型#>
    var version: Any?
    /// <#泛型#>
    var current: Any?
    /// <#泛型#>
    var id: Any?
    /// <#泛型#>
    var info: Any?
    /// <#泛型#>
    var url: Any?
    /// <#泛型#>
    var tenantMenuList: Any?
    /// <#泛型#>
    var appTenantMenuVOS: Any?
    /// <#泛型#>
    var menuTree: Any?
    /// <#泛型#>
    var uuid: Any?
    /// <#泛型#>
    var status: Any?
    /// <#泛型#>
    var applyId: Any?
    /// <#泛型#>
    var urlType: Any?
    /// <#泛型#>
    var activate: Any?
    /// <#泛型#>
    var adminUserList: Any?
    /// <#泛型#>
    var tenantId: Any?

}

struct NSSysTenantUserVOModel : HandyJSON {

    /// <#泛型#>
       var createDate: Any?
       /// 1
       var userId: String?
       /// <#泛型#>
       var remarks: Any?
       /// false
       var locked: Bool = false
       /// <#泛型#>
       var supTrueName: Any?
       /// <#泛型#>
       var status: Any?
       ///
       var loginTime: String?
       /// 1
       var tenantId: String?
       /// <#泛型#>
       var userVO: Any?
       /// <#泛型#>
       var updateBy: Any?
       /// ADMIN
       var userType: String?
       /// <#泛型#>
       var expField: Any?
       /// <#泛型#>
       var createBy: Any?
       /// 1
       var id: String?
       /// <#泛型#>
       var sort: Any?
       /// <#泛型#>
       var updateDate: Any?
       /// <#泛型#>
       var uuid: Any?
       /// SYS_PASSWORD
       var loginType: String?
       /// 165DAA
       var mainColor: String?
       /// <#泛型#>
       var tenantVO: Any?
       /// <#泛型#>
       var version: Any?



}

