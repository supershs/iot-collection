//
//  HSWebViewController.swift
//  33web
//
//  Created by 宋海胜 on 2020/11/25.
//
 
import UIKit
import SwiftWeb
import WebKit
import CoreLocation

enum JsMethod {
    case passGPSInfo
    case passVersion
    case passVoiceContent
    case passPayResult
}

class HSWebViewController: BaseWebViewController, LocationProtocol,CLLocationManagerDelegate {
    
    func getGPSSuccess(latitude: Double, longitude: Double) {
        location.stopLocation()
        if (self.isOnece) {
            print("lng: \(longitude) lat: \(latitude)")
            let param = "\(longitude),\(latitude)"
            self.jsAction(param: param, type: .passGPSInfo)
            self.isOnece = false
        }
    }
    
    
    
    func getGPSFailure(error: Error) {
        self.isOnece = false
        print("getMoLocation error: \(error.localizedDescription)")
        if (!self.isOnece) {
            location.stopLocation()
        }
    }
    
    func getGPSAuthorizationFailure() {
        let alert = UIAlertController(title: "提示", message: "请打开定位，以便获取您的位置信息", preferredStyle: .alert)
                     
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    

    fileprivate var backImgView = UIImageView()
    fileprivate var configManager = Tool()

    public var location :LocationManager = LocationManager()
    fileprivate var isOnece = false
    
    deinit {
      
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "loginOut")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "webGoBack")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "getGPSInfo")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        judgeNetwork()
        
        
        location.locationProtocol = self
//        self.location.startLocation()
        self.backImgView = self.configManager.getLanuchboardImage()
        self.backImgView.frame = self.view.bounds
        self.view.addSubview(self.backImgView)
        self.configManager.topMangain(webView: webView, vc: self)
        
        self.webView.frame = CGRect(x: 0, y: kStatusBarHeight, width: kScreenWidth, height: kScreenHeight - kStatusBarHeight)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func injectSwiftMethod(_ config: WKWebViewConfiguration) {
     
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "loginOut")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "webGoBack")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "getGPSInfo")
        
    }
    
    fileprivate func jsAction(param: String, type: JsMethod) {
        
        
        var javascript: String = ""
        
        switch type {
            
        case .passGPSInfo:
            javascript = "javascript:passGPSInfo(\"\(param)\")"
            
        case .passVersion:
            javascript = "javascript:passVersion(\"\(param)\")"
            
            
        case .passVoiceContent:
            javascript = "javascript:passVoiceContent(\"\(param)\")"
                
            
        case .passPayResult:
            javascript = "javascript:payResult(\"\(param)\")"
        }
        self.webView.evaluateJavaScript(javascript) { (res, error) in
            print("swift向js传值\n  param: \(param)\n  response: \(res ?? "nil")\n  error: \(error?.localizedDescription ?? "nil")")
            
        }
    }
    
     
    open override func swiftAction(_ name: String, _ body: Dictionary<String, Any>) {
        
        switch name {
                
            case "loginOut":
            //            UserInstance.userLogout()
                        self.navigationController?.popToRootViewController(animated: false)
                
            case "webGoBack":
            //            UserInstance.userLogout()
                        self.navigationController?.popToRootViewController(animated: false)
            
            case "getGPSInfo":
                gpsService()
                
        default:
            break
        }
    }
    
    // 定位
    fileprivate func gpsService() {
        
        //只获取一次
        isOnece = true
        self.location.startLocation()
    }
    
    
    // 在viewDidLoad中模态不行，此时页面还没加载完成
    fileprivate func judgeNetwork() {
        self.loadWeb()
        let myAppdelegate = UIApplication.shared.delegate as! AppDelegate
        myAppdelegate.networkAuthStatus(stateClosure: {[weak self] state in
            if let `self` = self {
                if state {
                    DispatchQueue.main.async {
//                        HUDUtil.showHud()
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        DispatchQueue.main.async {
//                            HUDUtil.hideHud()
                        }
                        self.loadWeb()
                    }
                }
            }
        })
    }
    override func loadSuccess() {
        
        UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseInOut, animations: {
            self.backImgView.alpha = 0
        }) { (finish) in
            self.backImgView.isHidden = true
        }
    }
}
