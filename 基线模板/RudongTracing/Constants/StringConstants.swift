//
//  StringConstants.swift
//  vgbox
//
//  Created by 宋海胜 on 2019/6/26.
//  Copyright © 2019 Swift Xcode. All rights reserved.
//  字符串常量

import UIKit

struct Constant_Share {
    
    // MARK: not install
    static let sinaWeiBoNotInstall: String = "未安装新浪微博客户端"
    
    static let qqNotInstall: String = "未安装QQ客户端"
    
    static let timNotInstall: String = "未安装TIM客户端"
    
    static let weChatNotInstall: String = "未安装微信客户端"
    
    // MARK: not support
    static let sinaWeiBoNotSupport: String = "当前新浪微博客户端不支持此次分享"
    
    static let qqNotSupport: String = "当前QQ客户端不支持此次分享"
    
    static let timNotSupport: String = "当前TIM客户端不支持此次分享"
    
    static let weChatNotSupport: String = "当前微信客户端不支持此次分享"
    
}

/// int转中文小写数字
struct Constant_ZHNum {
    
    static func IntToZH(_ index: Int) -> String {
        switch index {
        case 0:
            return "零"
        case 1:
            return "一"
        case 2:
            return "二"
        case 3:
            return "三"
        case 4:
            return "四"
        case 5:
            return "五"
        case 6:
            return "六"
        case 7:
            return "七"
        case 8:
            return "八"
        case 9:
            return "九"
        case 10:
            return "十"
        default:
            break
        }
        return ""
    }
}

struct Constant_UserDefault {
    
    // 智能海报下载、分享次数统计
//    static let posterShareNumKey: String = String(format: "%@_%@_shareNum", UserRepository.instance.getuser!.user.uid, RequestUrl.APP_MAP_SAVE_FESPOSSHARE)
}

struct Constant_Toast {
    
    static let loadText: String = "正在加载"
    
    static let loadingText: String = "正在加载..."
    
    static let loadFailed: String = "加载失败"
    
    static let loginIn: String = "正在登录..."
    
    static let loginOut: String = "正在退出..."
    
    static let smsCodeRequ: String = "验证码已发送，注意查收"
    
    static let accountChanging: String = "正在切换..."
    
    static let accountChangeSuccess: String = "切换成功"
    
    static let publishing: String = "正在发布"
    
    static let publishSuccess: String = "发布成功"
    
    static let searching: String = "正在搜索"
    
    static let comingSoon: String = "敬请期待~"
    
    static let commiting: String = "正在提交"
    
    static let saving: String = "正在保存"
    
    static let saveSuccess: String = "保存成功"
    
    static let saveFailed: String = "保存失败"
    
    static let deleting: String = "正在删除"
    
    static let deleteSuccess: String = "删除成功"
    
    static let deleteFailed: String = "删除失败"
    
    static let screening: String = "正在屏蔽"
    
    static let screenSuccess: String = "屏蔽成功"
    
    static let requestFailed: String = "请求失败"

    static let editing: String = "正在编辑"

    static let editSuccess: String = "编辑成功"
    
    static let editFailed: String = "编辑失败"
    
    static let adding: String = "正在添加"
    
    static let addSuccess: String = "添加成功"
    
    static let addFailed: String = "添加失败"

    static let changeSuccess: String = "修改成功"

    static let changeFailed: String = "修改失败"
    
    static let ry_buy_max: String = "已是最大数量"

    static let uploadError: String = "上传失败"
    
    static let fabiaoSuccess: String = "发布成功，等待审核"
    static let bianjiSuccess: String = "编辑成功，等待审核"

}

struct Constant_Placeholder {
    
    static let inputText: String = "请输入文字"
    
    static let enroManaDIYPl: String = "请输入（20字以内）"
    
    static let enroManaEditPl: String = "自定义来源渠道（10字以内）"
    
    static let courseCustomStu: String = "请输入学员姓名"
}
