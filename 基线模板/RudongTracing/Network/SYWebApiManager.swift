//
//  SYWebApiManager.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2021/2/2.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit
// 病虫害识别结果
var SY_SHIBIE_RESULT = "#/pages/insectPest/recognitionResults?id="
var SY_SHIBIEYU_RESULT = "#/pages/service-fishInfo/recognize?url="


public protocol WebTargetType {

    /// The target's base `URL`.
    var baseURL: String { get }

    /// The path to be appended to `baseURL` to form the full `URL`.
    var path: String { get }

}


enum SYWebApiManager {
    case bingchongShibie(id: String)
    case bingchongShibieyu(url: String)
}

extension SYWebApiManager: WebTargetType {
    
    var baseURL: String {
        
//        switch self {
//        case .login:
//            return WEBIP
//        default:
            return WEBIP
//        }
    }
    
    var path: String {
        
        switch self {
        
    
        case .bingchongShibie(let id): return String(format: "%@%@", SY_SHIBIE_RESULT, id)
        case .bingchongShibieyu(let url): return String(format: "%@%@", SY_SHIBIEYU_RESULT,url)
        }
    }
}
