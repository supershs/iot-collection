//
//  SaomaViewController.swift
//  33web
//
//  Created by 宋海胜 on 2021/6/22.
//

import UIKit
import swiftScan
import RxSwift

class SaomaViewController: LBXScanViewController {

    let dispose = DisposeBag()
    var requestVM: RYNongchangListVM = RYNongchangListVM()
    
    var code: String!
    
    var jilongLb: UILabel = {
        let v: UILabel = UILabel()
        v.font = UIFont.boldSystemFont(ofSize: 17)
        v.textColor = .white
        v.text = "编号："
        return v
    }()
    
    
    let backBt: UIButton = {
        let v: UIButton  = UIButton()
        v.setTitleColor(UIColor.white, for: .normal)
        v.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        v.backgroundColor = .gray
        v.setImage(UIImage(named: "nav_back"), for: .normal)
        v.layer.cornerRadius = 60
        v.tag = 99
        return v
    }()
    
    let clearBt: UIButton = {
        let v: UIButton  = UIButton()
        v.setTitle("重扫", for: .normal)
        v.setTitleColor(UIColor.white, for: .normal)
        v.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        v.backgroundColor = UIColor(hex: 0x666666)
        v.layer.cornerRadius = 5
        v.tag = 100
        return v
    }()
    
    let sureBt: UIButton = {
        let v: UIButton  = UIButton()
        v.setTitle("确定", for: .normal)
        v.setTitleColor(UIColor.white, for: .normal)
        v.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        v.backgroundColor = UIColor(hex: 0x367FF6)
        v.layer.cornerRadius = 5
        v.tag = 101
        return v
    }()
    
    var toastLb: UILabel = {
        let v: UILabel = UILabel()
        v.font = UIFont.boldSystemFont(ofSize: 17)
        v.textColor = .white
        v.backgroundColor = UIColor(white: 0, alpha: 0.7)
        v.isHidden = true
        v.layer.cornerRadius = 7
        v.layer.masksToBounds = true
        return v
    }()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        initViews()
        refreshBtns(0)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func initViews() {
        
        clearBt.addTarget(self, action: #selector(danAction), for: .touchUpInside)
        backBt.addTarget(self, action: #selector(danAction), for: .touchUpInside)
        sureBt.addTarget(self, action: #selector(danAction), for: .touchUpInside)
        qRScanView?.addSubview(jilongLb)
        qRScanView?.addSubview(backBt)

        qRScanView?.addSubview(clearBt)
        qRScanView?.addSubview(sureBt)
        qRScanView?.addSubview(toastLb)
        
        jilongLb.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-BOTTOM_SAFE_HEIGHT - 200)
            make.left.equalToSuperview().offset(30)
            make.height.equalTo(20)
        }
        
    
        backBt.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(15+STATUSBAR_HEIGHT)
            make.left.equalToSuperview().offset(12)
            make.width.equalTo(11)
            make.height.equalTo(20)
        }
        clearBt.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-BOTTOM_SAFE_HEIGHT - 60)
            make.left.equalToSuperview().offset(15.autoWidth())
            make.width.equalTo(120)
            make.height.equalTo(44)
        }
        sureBt.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-BOTTOM_SAFE_HEIGHT - 60)
            make.right.equalToSuperview().offset(-40)
            make.width.equalTo(120)
            make.height.equalTo(44)
        }
        toastLb.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.height.equalTo(50)
        }
    }
    
    @objc func danAction(sender: UIButton) {
        if sender.tag == 99{
            self.navigationController?.popViewController(animated: true)
        }
        if sender.tag == 100 {
            refreshBtns(0)
            self.startScan()
        } else if sender.tag == 101 {
            refreshBtns(1)
            self.startScan()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "scanResultNotification"), object: nil, userInfo:["code":self.code ?? ""])
            self.navigationController?.popViewController(animated: true)
        }
        self.jilongLb.text = "编号："
        self.code = ""
    }
    
    func refreshBtns(_ index: Int) {
        
//        clearBt.backgroundColor = .gray
//        sureBt.backgroundColor = .gray
        
//        if index == 0 {
//            clearBt.backgroundColor = UIColor(hex: 0x666666)
//        } else if index == 1 {
//            sureBt.backgroundColor = UIColor(hex: 0x367FF6)
//
//        }
    }
    
    /**
     覆盖父类 扫码成功后的操作
     
     处理扫码结果，如果是继承本控制器的，可以重写该方法,作出相应地处理，或者设置delegate作出相应处理
     */
    override func handleCodeResult(arrayResult: [LBXScanResult]) {
        guard let delegate = scanResultDelegate else {
            fatalError("you must set scanResultDelegate or override this method without super keyword")
        }
        
        if !isSupportContinuous {
//            navigationController?.popViewController(animated: true)
        }
        
        var res:LBXScanResult!
        if let result = arrayResult.first {
            res = result
            delegate.scanFinished(scanResult: result, error: nil)
        } else {
            let result = LBXScanResult(str: nil, img: nil, barCodeType: nil, corner: nil)
            res = result
            delegate.scanFinished(scanResult: result, error: "no scan result")
        }
        
        if let c = res.strScanned {
            code = c
            jilongLb.text = "编号：\(c)"
            clearBt.backgroundColor = UIColor(hex: 0x666666)
            sureBt.backgroundColor = UIColor(hex: 0x367FF6)
        } else {
            HUDUtil.showBlackTextView(text: "未能识别条形码")
            return
        }
        
    }
 

    func passEggInfo() {
//        requestVM.baseRequest(disposeBag: dispose, type: .eggInfo(code: code, eggStatus: eggStatus), modelClass: SGBaseModel<SGSimpleModel>.self) {[weak self] (res) in
//            if let `self` = self {
//                DispatchQueue.main.async {
////                    HUDUtil.showBlackTextView(text: res.message ?? "")
//                    self.toastLb.text = "  \(res.message ?? "")  "
//                    self.toastLb.isHidden = false
//                }
                self.refreshBtns(0)
                self.jilongLb.text = "鸡笼编号："
                self.startScan()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    self.toastLb.isHidden = true
                }
//            }
//        } Error: {
//
//        }
    }
}
