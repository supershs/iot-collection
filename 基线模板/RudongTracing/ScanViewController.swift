//
//  ViewController.swift
//  33web
//
//  Created by 宋海胜 on 2020/11/25.
//

import UIKit
import SwiftWeb
import swiftScan
import RxSwift


class ScanViewController: UIViewController, LBXScanViewControllerDelegate {

    
    var isSupportContinuous = false;
    let saomaVC = SaomaViewController()
    
    var bianjiBt: UIButton = {
        let v = UIButton(frame: CGRect(x: 100, y: 100, width: 100, height: 50))
        v.backgroundColor = .blue
        v.setTitle("扫一扫", for: .normal)
        v.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        v.setTitleColor(UIColor.white, for: .normal)
        v.layer.cornerRadius = 3
        return v
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.title = "添加设备"
        notSquare()
//        bianjiBt.addTarget(self, action: #selector(btAction), for: .touchUpInside)
//        view.addSubview(bianjiBt)
//        bianjiBt.snp.makeConstraints { (make) in
//            make.top.equalToSuperview().offset(80+NAV_HEIGHT)
//            make.centerX.equalToSuperview()
//            make.height.equalTo(40)
//            make.width.equalTo(100)
//        }
    }
    
    @objc func btAction() {
        
        notSquare()
        
    }
    
    // MARK: - ------条形码扫码界面 ---------
    func notSquare() {
        //设置扫码区域参数
        //设置扫码区域参数
        var style = LBXScanViewStyle()

        style.centerUpOffset = 44
        style.photoframeAngleStyle = LBXScanViewPhotoframeAngleStyle.Inner
        style.photoframeLineW = 4
        style.photoframeAngleW = 28
        style.photoframeAngleH = 16
        style.isNeedShowRetangle = false

        style.anmiationStyle = LBXScanViewAnimationStyle.LineStill

        style.animationImage = createImageWithColor(color: UIColor.red)
        //非正方形
        //设置矩形宽高比
        style.whRatio = 4.3/4.3

        //离左边和右边距离
        style.xScanRetangleOffset = 30

        saomaVC.scanResultDelegate = self
        saomaVC.scanStyle = style
//        self.navigationController?.pushViewController(saomaVC, animated: true)

    }
    
    func createImageWithColor(color: UIColor) -> UIImage {
        let rect=CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        let theImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return theImage!
    }
    
    func scanFinished(scanResult: LBXScanResult, error: String?) {
        
        if !isSupportContinuous {
            showDeleteAlert(scanResult.strScanned)
        }
    }
    
    func showDeleteAlert(_ strScanned: String?) {
//        if let c = strScanned {
//            code = c
//            saomaVC.jilongLb.text = "鸡笼编号：\(c)"
//        } else {
//            HUDUtil.showBlackTextView(text: "未能识别条形码")
//            return
//        }
        
//        let alert = QualityAlert()
//        alert.clickedClosure = { [weak self] index in
//            if let `self` = self {
//                // 1-有蛋，0-无， 2- 坏
//                if index == 0 {
//                    self.eggStatus = "1"
//                } else if index == 1 {
//                    self.eggStatus = "0"
//                } else {
//                    self.eggStatus = "2"
//                }
//                self.passEggInfo()
//            }
//
//        }
//        let window = UIApplication.shared.keyWindow
//        window?.addSubview(alert)
//        alert.snp.makeConstraints { (make) in
//            make.edges.equalToSuperview()
//        }
    }
}



