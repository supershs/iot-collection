//
//  UITextField+Expand.h
//  vgbox
//
//  Created by ZTS-lcg on 2019/8/8.
//  Copyright © 2019 Swift Xcode. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^LimitBlock)(void);

NS_ASSUME_NONNULL_BEGIN

@interface UITextField (Expand)
@property (nonatomic , copy)LimitBlock limitBlock;

- (void)lengthLimit:(void (^)(void))limit;

@end

NS_ASSUME_NONNULL_END
