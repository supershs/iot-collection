//
//  LogModel.swift
//  33web
//
//  Created by JAY on 2023/5/9.
//

import UIKit

import HandyJSON


struct NSRootModel<T: HandyJSON> : HandyJSON{
    ///
    var date: String?
    ///
    var data: T?
    /// SUCCESS
    var status: String?
    /// 10000
    var code: Int = 0
    /// 登录成功
    var message: String?

}

struct LoginModel : HandyJSON {
    /// 1
      var tenantId: Int = 0
      /// ADMIN
      var userType: String?
      /// <#泛型#>
      var companyInfo: Any?
      /// 管理员
      var trueName: String?
      ///
      var headImg: String?
      ///
      var token: String?
      /// 1
      var userId: String?
      /// admin
      var loginName: String?
      /// <#泛型#>
      var registrationId: Any?
      ///
      var sysTenantUserVO: NSSysTenantUserVOModel?
      /// 管理员
      var nickName: String?
      /// false
      var outsourceCode: Bool = false
      


}


struct NSSysDeptPostVOModel : HandyJSON {

    /// 1
       var userId: String?
       /// 1
       var postId: String?
       ///
       var createDate: String?
       /// <#泛型#>
       var deptName: Any?
       /// <#泛型#>
       var dockDeptId: Any?
       /// 1
       var id: String?
       /// <#泛型#>
       var operationPrincipal: Any?
       /// <#泛型#>
       var remarks: Any?
       ///
       var uuid: String?
       /// 1
       var deptId: String?
       /// <#泛型#>
       var postName: Any?
       /// 1
       var status: Int = 0
       /// id
       var itemId: String?
       /// 1
       var createBy: String?
       ///
       var updateDate: String?
       /// 1
       var sort: Int = 0
       /// <#泛型#>
       var sysDeptVO: Any?
       /// <#泛型#>
       var companyPoint: Any?
       /// 1
       var updateBy: String?
       /// <#泛型#>
       var dockPostId: Any?
       /// 1
       var organiId: String?
       /// 1
       var version: Int = 0
       /// <#泛型#>
       var deptLevel: Any?
       /// <#泛型#>
       var sysUserDTO: Any?
       /// true
       var principal: Bool = false
       /// <#泛型#>
       var dockUserId: Any?
       /// <#泛型#>
       var outsourceCode: Any?
       /// <#泛型#>
       var sysPostVO: Any?



}


struct NSSysTenantUserVOModel : HandyJSON {

    /// <#泛型#>
    /// ADMIN
      var userType: String?
      /// <#泛型#>
      var profileVO: Any?
      /// 1
      var tenantId: String?
      /// 1
      var createBy: String?
      /// <#泛型#>
      var tenantVO: Any?
      ///
      var updateDate: String?
      /// 1
      var sort: Int = 0
      /// 1
      var userId: String?
      ///
      var loginTime: String?
      /// 1
      var id: String?
      /// false
      var locked: Bool = false
      /// <#泛型#>
      var createDate: Any?
      /// <#泛型#>
      var registrationId: Any?
      /// 1
      var updateBy: String?
      /// <#泛型#>
      var remarks: Any?
      /// 1
      var status: Int = 0
      /// 31
      var version: Int = 0
      /// SYS_PASSWORD
      var loginType: String?
      /// 165DAA
      var mainColor: String?
      /// <#泛型#>
      var userVO: Any?
      /// <#泛型#>
      var uuid: Any?

     


}
