//
//  RegisterNowViewController.swift
//  RudongTracing
//
//  Created by JAY on 2023/8/21.
//

import UIKit
import SnapKit
import RxSwift
import RxAlamofire
import HandyJSON
import Alamofire
import SwiftyJSON
class RegisterNowViewController: RootViewController {
    
    let disposBag = DisposeBag()
    
    //手机号
    var backView : UIView!
    var beginLb : UILabel!
    var phoneF : UITextField!
    var lineView : UIView!
    
    //验证码
    var backTwoView : UIView!
    var phoneTwoIngV : UIImageView!
    var phoneTwoF : UITextField!
    var lineTwoView : UIView!
    var btn : UIButton!
    var lookBtn : UIButton! //查看密码
    
    //设置密码
    var backsetUpView : UIView!
    var phonesetUpIngV : UIImageView!
    var phonesetUpF : UITextField!
    var linesetUpView : UIView!
    var setUpBtn : UIButton!
    var looksetUpBtn : UIButton! //查看密码
    var backsetLb : UILabel!
    
    //登录
    var signBtn : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "注册"
        view.backgroundColor = .white
        setupUI()
    }
    
    func setupUI(){
        
        //view
        backView = UIView()
        backView.backgroundColor = UIColor.white
        view.addSubview(backView)
        backView.snp.makeConstraints { make in
            make.right.equalTo(view).offset(-15)
            make.left.equalTo(view).offset(15)
            make.top.equalTo(view).offset(20)
            make.height.equalTo(60)
        }
        
        beginLb = UILabel()
        beginLb.font = UIFont.systemFont(ofSize: 14.0)
        beginLb.textColor = UIColor.black
        beginLb.text = "+86"
        beginLb.numberOfLines = 0
        beginLb.textAlignment = .left
        backView.addSubview(beginLb)
        beginLb.snp.makeConstraints { make in
            make.left.equalTo(backView).offset(20)
            make.centerY.equalTo(backView)
            make.height.equalTo(21)
            make.width.equalTo(30)
        }
        
      
        
        //手机号输入框
        phoneF = UITextField()
        phoneF.borderStyle = .none //除边框样式
        phoneF.placeholder = "请输入手机号"
        phoneF.clearButtonMode = .whileEditing
        backView.addSubview(phoneF)
        phoneF.snp.makeConstraints { make in
            make.left.equalTo(beginLb.snp.right).offset(10)
            make.right.bottom.top.equalTo(backView)
        }
        
        //底线
        lineView = UIView()
        lineView.backgroundColor = UIColor(hex: "d6d6d6")
        backView.addSubview(lineView)
        
        lineView.snp.makeConstraints { make in
            make.top.equalTo(phoneF.snp.bottom).offset(0)
            make.right.equalTo(backView).offset(-20)
            make.left.equalTo(backView).offset(15)
            make.height.equalTo(1)
        }
        
        //view验证码
        backTwoView = UIView()
        backTwoView.backgroundColor = UIColor.white
        view.addSubview(backTwoView)
        backTwoView.snp.makeConstraints { make in
            make.right.equalTo(view).offset(-15)
            make.left.equalTo(view).offset(15)
            make.top.equalTo(backView.snp.bottom).offset(2)
            make.height.equalTo(60)
        }
        
        //验证码输入框
        phoneTwoF = UITextField()
        phoneTwoF.borderStyle = .none //除边框样式
        phoneTwoF.placeholder = "请输入验证码"
        backTwoView.addSubview(phoneTwoF)
        phoneTwoF.snp.makeConstraints { make in
            make.left.equalTo(backTwoView).offset(15)
            make.right.bottom.top.equalTo(backTwoView)
        }
        
        btn = UIButton()
        btn.setTitle("获取验证码", for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        btn.setTitleColor(UIColor(hex: "2cd4a0"), for: .normal)
        btn.setBackgroundImage(UIImage(named: "cheek"), for: .normal)
        btn.addTarget(self, action: #selector(codeClick), for: .touchUpInside)
        backTwoView.addSubview(btn)
        
        btn.snp_makeConstraints { make in
            make.centerY.equalTo(backTwoView)
            make.right.equalTo(backTwoView).offset(-20)
            make.width.equalTo(80)
            make.height.equalTo(25)
        }
        
        
        //底线
        lineTwoView = UIView()
        lineTwoView.backgroundColor = UIColor(hex: "d6d6d6")
        backTwoView.addSubview(lineTwoView)
        
        lineTwoView.snp_makeConstraints { make in
            make.top.equalTo(phoneTwoF.snp_bottom).offset(0)
            make.right.equalTo(backTwoView).offset(-20)
            make.left.equalTo(backTwoView).offset(10)
            make.height.equalTo(1)
        }
        
        //view设置密码
        backsetUpView = UIView()
        backsetUpView.backgroundColor = UIColor.white
        view.addSubview(backsetUpView)
        backsetUpView.snp.makeConstraints { make in
            make.right.equalTo(view).offset(-15)
            make.left.equalTo(view).offset(15)
            make.top.equalTo(backTwoView.snp.bottom).offset(2)
            make.height.equalTo(60)
        }
        
        backsetLb = UILabel()
        backsetLb.font = UIFont.systemFont(ofSize: 14.0)
        backsetLb.textColor = UIColor.black
        backsetLb.text = "初始密码: 123456a"
        backsetLb.numberOfLines = 0
        backsetLb.textAlignment = .left
        backsetUpView.addSubview(backsetLb)
        backsetLb.snp.makeConstraints { make in
            make.left.equalTo(backsetUpView).offset(20)
            make.centerY.equalTo(backsetUpView)
            make.height.equalTo(21)
            make.width.equalTo(300)
        }
        
        //底线
        linesetUpView = UIView()
        linesetUpView.backgroundColor = UIColor(hex: "d6d6d6")
        backsetUpView.addSubview(linesetUpView)
        linesetUpView.snp_makeConstraints { make in
            make.bottom.equalTo(backsetUpView.snp.bottom).offset(0)
            make.right.equalTo(backsetUpView).offset(-20)
            make.left.equalTo(backsetUpView).offset(15)
            make.height.equalTo(1)
        }
        
        
        //登录按钮
        signBtn = UIButton()
        signBtn.setTitle("注册", for: .normal)
        signBtn.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        signBtn.setTitleColor(UIColor.white, for: .normal)
        signBtn.setBackgroundImage(UIImage(named: "login_btn"), for: .normal)
        signBtn.addTarget(self, action: #selector(modifyClick), for: .touchUpInside)
        view.addSubview(signBtn)
        signBtn.snp_makeConstraints { make in
            make.top.equalTo(linesetUpView.snp_bottom).offset(50)
            make.right.equalTo(backTwoView).offset(-20)
            make.left.equalTo(backTwoView).offset(20)
            make.height.equalTo(60)
        }
        
        
        //注册点击事件
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
      
        
    }
    
    //点击空白处关闭键盘方法
    @objc func handleTap(sender: UITapGestureRecognizer) {
           if sender.state == .ended {
               print("收回键盘")
               self.phoneF.resignFirstResponder()//username放弃第一响应者
               self.phoneTwoF.resignFirstResponder()//password放弃第一响应者
           }
           sender.cancelsTouchesInView = false
       }
    
    //    MARK: -  验证码
    @objc func codeClick(){
        var countdown = 60 // 倒计时的秒数
        if phoneF.text!.isEmpty {
            TSProgressHUD.ts_showWarningWithStatus("请输入手机号")
        }else{
           
            let sysTenantDTO = ["url":paraUrl]
            let parameters  = ["mobile":phoneF.text]
            
            RxAlamofire.requestJSON(.post, URL(string: registerURL)!, parameters: parameters, encoding: JSONEncoding.default, headers: ["Content-Type":"application/json;charset=UTF-8"])
                .debug()
                .subscribe(onNext: { [self] (r, json) in
                    
                    let JSONDictory = JSON(json ?? " ")
                    let code = JSONDictory["code"].intValue
                    let message = JSONDictory["message"].string
                    print("code====",code)
                    print("CodeURL====",registerURL)
                    print("parameters====",parameters)
                    if code == 10000{
                        self.btn.isEnabled = false
                        let timer = DispatchSource.makeTimerSource(queue: DispatchQueue.global())
                        timer.schedule(deadline: .now(), repeating: .seconds(1))
                        timer.setEventHandler { [weak self] in
                            guard let self = self else { return }
                            countdown -= 1
                            DispatchQueue.main.async { [self] in
                                self.btn.setTitle("\(countdown)秒后再试", for: .normal)
                            }
                            if countdown <= 0 {
                                timer.cancel()
                                DispatchQueue.main.async {
                                    self.btn.setTitle("获取验证码", for: .normal)
                                    self.btn.isEnabled = true
                                }
                            }
                        }
                        timer.resume()
                    }else{
                        TSProgressHUD.ts_showWarningWithStatus(message ?? "系统出错")
                    }
                    
                    
                }, onError: { (error) in
                    print(error,"=====error")
                    print("parameters == ",parameters)
                })
                .disposed(by: disposBag)
            
        }
    }
    
    @objc func modifyClick(){
        
        if phoneF.text!.isEmpty{
            TSProgressHUD.ts_showWarningWithStatus("请输入手机号")
        }else if phoneTwoF.text!.isEmpty{
            TSProgressHUD.ts_showWarningWithStatus("请输入验证码")
        }else{
            let parameters  = ["mobile":phoneF.text!,"code": phoneTwoF.text!,"tenantUrl":paraUrl]
            
            AF.request(successfulURL, method: .post, parameters:parameters, encoder: JSONParameterEncoder.default).responseData {[self]response in
                let json = try? JSONSerialization.jsonObject(with: response.value!, options: JSONSerialization.ReadingOptions.allowFragments) as? [String:Any]
                let JSONDictory = JSON(json ?? " ")
                let code = JSONDictory["code"].intValue
                let message = JSONDictory["message"].string
                if code == 10000{
                    TSProgressHUD.ts_showWarningWithStatus(message ?? "系统出错")
                    self.navigationController?.popViewController(animated: false)
                }else{
                    TSProgressHUD.ts_showWarningWithStatus(message ?? "系统出错")
                }
            }
            
            
        }
    }

}
