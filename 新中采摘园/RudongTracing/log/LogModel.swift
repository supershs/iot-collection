//
//  LogModel.swift
//  33web
//
//  Created by JAY on 2023/5/9.
//

import UIKit

import HandyJSON


struct NSRootModel<T: HandyJSON> : HandyJSON{
    ///
    var date: String?
    ///
    var data: T?
    /// SUCCESS
    var status: String?
    /// 10000
    var code: Int = 0
    /// 登录成功
    var message: String?

}

struct LoginModel : HandyJSON {
    /// <#泛型#>
      var remarks: String?
      /// <#泛型#>
      var version: String?
      /// <#泛型#>
      var createBy: String?
      /// <#泛型#>
      var loginTime: String?
      ///
      var sysTenantUserVO: NSSysTenantUserVOModel?
      /// <#泛型#>
      var tenantMenuVO: String?
      /// <#泛型#>
      var recentOnLineTime: String?
      /// <#泛型#>
      var age: String?
      /// <#泛型#>
      var deptVO: String?
      /// <#泛型#>
      var status: String?
      /// <#泛型#>
      var educationVOS: String?
      /// <#泛型#>
      var subject: String?
      ///
      var headImg: String?
      /// <#泛型#>
      var createDate: String?
      /// <#泛型#>
      var sysPostVO: String?
      /// admin
      var loginName: String?
      /// <#泛型#>
      var deptPostVOList: String?
      /// <#泛型#>
      var nickName: String?
      /// <#泛型#>
      var uuid: String?
      /// <#泛型#>
      var updateBy: String?
      /// <#泛型#>
      var password: String?
      /// 13382330001
      var mobile: String?
      ///
      var token: String?
      /// <#泛型#>
      var tenantId: String?
      /// <#泛型#>
      var salt: String?
      /// <#泛型#>
      var menuRoleVO: String?
      /// 超级管理员
      var trueName: String?
      /// <#泛型#>
      var userProfileVO: String?
      /// <#泛型#>
      var updateDate: String?
      /// <#泛型#>
      var birthDayStr: String?
      /// 1
      var id: String?
      ///
//      var tenant: NSTenantModel?
      /// <#泛型#>
      var appTenantUserVO: String?
      /// <#泛型#>
      var sort: String?
      /// <#泛型#>
      var sex: String?
      /// <#泛型#>
      var sysDeptPostVO: String?





}
struct NSTenantModel : HandyJSON {
    /// <#泛型#>
       var urlType: String?
       /// <#泛型#>
       var updateDate: String?
       /// <#泛型#>
       var menuTree: String?
       /// ADMIN
       var type: String?
       /// <#泛型#>
       var url: String?
       /// <#泛型#>
       var applyId: String?
       /// <#泛型#>
       var createBy: String?
       /// <#泛型#>
       var activate: String?
       /// <#泛型#>
       var name: String?
       /// <#泛型#>
       var tenantId: String?
       /// <#泛型#>
       var current: String?
       /// <#泛型#>
       var updateBy: String?
       /// <#泛型#>
       var tenantMenuList: String?
       /// <#泛型#>
       var remarks: String?
       /// <#泛型#>
       var createDate: String?
       /// <#泛型#>
       var version: String?
       /// <#泛型#>
       var adminUserList: String?
       /// <#泛型#>
       var sort: String?
       /// <#泛型#>
       var id: String?
       /// <#泛型#>
       var status: String?
       /// <#泛型#>
       var info: String?
       /// <#泛型#>
       var uuid: String?
       /// <#泛型#>
       var hasApp: String?
       /// <#泛型#>
       var appTenantMenuVOS: String?


}

struct NSSysTenantUserVOModel : HandyJSON {
    /// <#泛型#>
        var updateDate: String?
        /// <#泛型#>
        var userVO: String?
        /// <#泛型#>
        var profileVO: String?
        /// <#泛型#>
        var createBy: String?
        /// <#泛型#>
        var tenantId: String?
        /// ADMIN
        var userType: String?
        /// <#泛型#>
        var locked: String?
        /// <#泛型#>
        var updateBy: String?
        /// 1
        var itemId: String?
        /// <#泛型#>
        var remarks: String?
        /// <#泛型#>
        var createDate: String?
        /// <#泛型#>
        var userId: String?
        /// <#泛型#>
        var loginType: String?
        /// <#泛型#>
        var version: String?
        /// <#泛型#>
        var status: String?
        /// <#泛型#>
        var tenantVO: String?
        /// <#泛型#>
        var uuid: String?
        /// <#泛型#>
        var sort: String?
        /// <#泛型#>
        var loginTime: String?
        /// 165DAA
        var mainColor: String?
        /// <#泛型#>
        var menuVOS: String?


}

