//
//  BaseWebViewController.swift
//  vgbox
//
//  Created by 宋海胜 on 2019/4/3.
//  Copyright © 2019年 Swift Xcode. All rights reserved.
//  web基础类

import UIKit
import WebKit

open class BaseWebViewController: UIViewController {
    
    public var path: String = ""
    public var navTitle: String = ""
    public var progressView: UIProgressView = {
        let progress: UIProgressView = UIProgressView(frame: CGRect(x: 0, y: kNavigationHeight, width: kScreenWidth, height: 1))
        progress.tintColor = .orange
        progress.trackTintColor = UIColor.clear
        return progress
    }()
    
    public var webView: WKWebView!
    
    public init(path: String)
    {
        self.path = path
        super.init(nibName: nil, bundle: nil)
        print("weburl: \(self.path)")
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    deinit {
        if nil == self.webView {
            return
        }
//        webView.removeObserver(self, forKeyPath: "title")
//        webView.removeObserver(self, forKeyPath: "estimatedProgress")
        print("BaseWebViewController: \(self.path) 释放啦")
    }
    
    open override func viewDidLoad() {
        
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.view.backgroundColor = .white
        
        setupWebView()
        
        self.view.addSubview(self.progressView)
    }
    
    open func setupWebView() {
        
        let config: WKWebViewConfiguration = WKWebViewConfiguration()
        let prefer: WKPreferences = WKPreferences()
        prefer.javaScriptEnabled = true
        config.preferences = prefer
        config.userContentController = WKUserContentController()
        config.selectionGranularity = WKSelectionGranularity.character
        self.injectSwiftMethod(config)
        webView = WKWebView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight), configuration:config)
        webView.scrollView.bounces = false
        webView.uiDelegate = self
        webView.navigationDelegate = self
        webView.addObserver(self, forKeyPath: "title", options: .new, context: nil)
        webView.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil)
        webView.backgroundColor = .white
        self.view.addSubview(self.webView)
    }
    
    // 捕获title 和进度
    open override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "title"
        {
            self.navTitle = self.webView.title!
            self.webView.removeObserver(self, forKeyPath: "title")
        }
        else if keyPath == "estimatedProgress"
        {
            self.progressView.alpha = 1.0
            self.progressView.setProgress(Float(self.webView.estimatedProgress), animated: true)
            if self.webView.estimatedProgress >= 1.0
            {
                UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseOut, animations: {
                    self.progressView.alpha = 0
                }) { (finish) in
                    self.progressView.setProgress(0.0, animated: false)
                    
                    self.webView.removeObserver(self, forKeyPath: "estimatedProgress")
                }
                loadSuccess()
            }
        }
    }
    
    open func loadWeb(){
        webView.load(URLRequest(url: URL(string: self.path)!))//load需在addsubview之后
    }
    
    // 注入方法名
    open func injectSwiftMethod(_ config: WKWebViewConfiguration) {
        
    }
    
    // js需要执行的swift方法
    open func swiftAction(_ name: String, _ body: Dictionary<String, Any>) {

    }
    
    // swift需要执行的js方法
    open func jsAction() {
        
    }
    
    //关闭加载方法
    open func getH5Url(_ url: String){
        
    }
    
    open func loadSuccess() {
        
    }
    
    open func loadFail() {
        
    }
    
}

extension BaseWebViewController: WKUIDelegate
{
    // 显示的一些拦截 alert等

    // 监听通过JS调用警告框
    public func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            completionHandler()
        }))
        self.sg_present(alert, animated: true, completion: nil)
    }
    
    // 监听通过JS调用提示框
    public func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (Bool) -> Void) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            completionHandler(true)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
            completionHandler(false)
        }))
        self.sg_present(alert, animated: true, completion: nil)
    }
    
    // 监听JS调用输入框
    public func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (String?) -> Void) {
        // 类似上面两个方法
    }
}

extension BaseWebViewController: WKScriptMessageHandler
{
    // 在此js向swift传值
    public func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        print("js向swift传值:\n  message.name: \(message.name)\n  message.body: \(message.body)")
        let body = message.body as! [String: Any]
        swiftAction(message.name, body)
    }
}


extension BaseWebViewController: WKNavigationDelegate
{
    // 在此可调用js方法
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.jsAction()
    }
    
    // 拦截url
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let url = navigationAction.request.url?.absoluteString
        {
            getH5Url(url)
        }
        decisionHandler(.allow)
    }
    
    public func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        loadFail()
    }
    
    public func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        // 如果是拨打电话的url拦截
        let e = error as NSError
        if let value = e.userInfo["NSErrorFailingURLStringKey"] {
            if let vs = value as? String, vs.contains("tel://") {
                return
            }
        }
        loadFail()
    }
}


// 防止循环引用控制器
open class WeakScriptMessageDelegate: NSObject, WKScriptMessageHandler {
    
    weak var scriptDelegate: WKScriptMessageHandler?
    public init(scriptDelegate: WKScriptMessageHandler)
    {
        self.scriptDelegate = scriptDelegate
        super.init()
    }
    open func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        scriptDelegate?.userContentController(userContentController, didReceive: message)
    }
    deinit {
        print("WeakScriptMessageDelegate 释放啦")
    }
}

