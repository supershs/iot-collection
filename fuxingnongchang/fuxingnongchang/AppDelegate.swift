//
//  AppDelegate.swift
//  fuxingnongchang
//
//  Created by JAY on 2023/5/29.
//

import UIKit

let kWebUrl = "http://121.37.178.216:9065/#/"
//let kWebUrl = "http://192.168.0.9:8081/#/"

let logoURL = "http://121.37.178.216:9066/api/1.0/Login/check" //登录接口
let GOTOHOME = "home?data="
let appURL = "33app.33iot.com"



@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var canAllButUpsideDown: Bool = false
    var window: UIWindow?
    var webVC = HSWebViewController(path: kWebUrl)
    var logVC = JDLoginViewController()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //设置用户授权显示通知
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.badge,.carPlay,.sound], completionHandler: { (success, error) in
                print("通知授权" + (success ? "成功" : "失败"))
            })
        }
        
        window = UIWindow(frame: UIScreen.main.bounds)
        let nav = ZXNavigationController (rootViewController: logVC)
        self.window?.rootViewController = nav
        window!.makeKeyAndVisible()
        
        // 强制关闭暗黑模式
        if #available(iOS 13.0, *) {
            self.window?.overrideUserInterfaceStyle = .light
        }
        #if DEBUG // 判断是否在测试环境下
        print("当前环境：DEBUG")
        #else
        print("当前环境：RELEASE")
        #endif
        return true
    }

}

