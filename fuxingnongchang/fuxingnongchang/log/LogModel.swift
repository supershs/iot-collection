//
//  LogModel.swift
//  33web
//
//  Created by JAY on 2023/5/9.
//

import UIKit

import HandyJSON


struct NSRootModel<T: HandyJSON> : HandyJSON{
    ///
    var date: String?
    ///
    var data: T?
    /// SUCCESS
    var status: String?
    /// 10000
    var code: Int = 0
    /// 登录成功
    var message: String?
    


}

struct LoginModel : HandyJSON {
    ///
      var userInfo: NSUserInfoModel?
      ///
      var token: String?
      ///
      var Authorization: String?

  

}
struct NSUserInfoModel : HandyJSON {
    /// <#泛型#>
        var traceNumber: String?
        /// ADMIN
        var userType: String?
        /// admin
        var loginName: String?
        ///
        var updateDate: String?
        /// 1
        var sex: Int = 0
        ///
        var headImg: String?
        /// <#泛型#>
        var version: String?
        ///
        var phone: String?
        /// 系统管理员
        var trueName: String?
        ///
        var email: String?
        /// <#泛型#>
        var deptId: String?
        /// <#泛型#>
        var type: String?
        ///
        var updateBy: String?
        /// 0
        var sort: Int = 0
        /// <#泛型#>
        var idCard: String?
        /// id
        var id: String?
        /// 1
        var status: Int = 0
        /// <#泛型#>
        var brithday: String?
        ///
//        var menuList: [String]?
        /// <#泛型#>
        var registerDate: String?
        /// <#泛型#>
        var lastLoginTime: String?
        ///
        var uuid: String?
        ///
        var deptName: String?
        ///
        var remarks: String?
        /// 16806
        var jobNumber: String?
        /// <#泛型#>
        var createDate: String?
        /// <#泛型#>
        var createBy: String?
        /// 前端开发
        var roleName: String?
        ///
        var mobile: String?
        /// <#泛型#>
        var deviceId: String?
        /// 1
        var tenantId: String?
        ///
        var password: String?
        ///
        var salt: String?

}
