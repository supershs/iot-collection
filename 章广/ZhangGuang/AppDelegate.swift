//
//  AppDelegate.swift
//  ZhangGuang
//
//  Created by 叁拾叁 on 2020/6/5.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit
import Alamofire
import AFNetworking
import CallKit

let kImageApi = "http://119.3.169.92:9488"
let kImageUpload = "\(kImageApi)/sysFile/batchUploadImg"
let kImageInfoUpload = "\(kImageApi)/api/1.1/CityCapture/savePic"


//线上pad 端
let padUrl = "https://zhangguang.jsnj33.com:9099/#/"
//phone 端
let kAppLoginUrl = "https://zhangguang.jsnj33.com:9099/#/"
//let kAppLoginUrl = "http://192.168.0.177:8081/#/"
/**
 判断是否是ipad
 */
let isPad = UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad
//忘记密码
let forgetUrl  = "forgetPassWd"

//获取验证码
let CodeURL = "https://zhangguang.jsnj33.com:9488/api/updatePhoneCode"
//测试
//let kAppLoginUrl = "http://192.168.0.65:8082/#/"
let logoURL = "https://zhangguang.jsnj33.com:9488/api/1.1/Login/check" //登录接口
let GOTOHOME = "firstList?data="
let GOTOH = "helloWorld?id=0&data="
let appURL = "33app.33iot.com"

let kVideoStartPTZ = "https://open.ys7.com/api/lapp/device/ptz/start"
let kVideoStopPTZ = "https://open.ys7.com/api/lapp/device/ptz/stop"
let kGetAccessToken = "https://open.ys7.com/api/lapp/token/get"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CXCallObserverDelegate {
    var canAllButUpsideDown: Bool = false
    var window: UIWindow?
    var webVC = ZGWebViewController(path: kAppLoginUrl)
    var logVC = JDLoginViewController()
    var padLogVC = PadLoginViewController()
    
    var record: String = "0"
    
    ///记录电话 打来中断
    var callob: CXCallObserver = CXCallObserver()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.watchNetwork()
        self.callob.setDelegate(self, queue: DispatchQueue.main)
//        window = UIWindow(frame: UIScreen.main.bounds)
//        window!.rootViewController = webVC
//        window!.makeKeyAndVisible()
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        if isPad{
            print("pad")
            let nav = ZXNavigationController (rootViewController: logVC)
            self.window?.rootViewController = nav
        }else{
            print("phone")
            let nav = ZXNavigationController (rootViewController: padLogVC)
            self.window?.rootViewController = nav
        }
        window!.makeKeyAndVisible()
        return true
    }

    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        if canAllButUpsideDown {
            return UIInterfaceOrientationMask.allButUpsideDown
        } else {
            return UIInterfaceOrientationMask.portrait
        }
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        webVC.liveView.stopLiveVideo()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        webVC.liveView.startLiveVideo()
    }
    
    func watchNetwork() {

        AFNetworkReachabilityManager.shared().startMonitoring()
        AFNetworkReachabilityManager.shared().setReachabilityStatusChange { (status) in
            switch status {
            case .unknown:
                print("watchNetwork: 未知")
                self.webVC.liveView.stopLiveVideo()
                
            case .notReachable:
                print("watchNetwork: 未连接")
                self.webVC.liveView.stopLiveVideo()
                
            case .reachableViaWiFi:
                print("watchNetwork: wifi连接")
                self.webVC.liveView.reloadVideo()
                
            case .reachableViaWWAN:
                print("watchNetwork: wwan连接")
                self.webVC.liveView.reloadVideo()
            }
        }
    }
    
    // 电话状态监听
    func callObserver(_ callObserver: CXCallObserver, callChanged call: CXCall) {
        
        print("电话状态监听: \n      call.isOutgoing: \(call.isOutgoing) \n      call.isOnHold: \(call.isOnHold) \n      call.hasEnded: \(call.hasEnded) \n      call.hasConnected: \(call.hasConnected) \n      call.uuid: \(call.uuid)")
        
        if call.isOutgoing {// 拨打
            if call.hasConnected {
                if call.hasEnded {
                    print("watchCallStatus: 拨打->接通->结束")
                    webVC.liveView.startLiveVideo()
                } else {
                    print("watchCallStatus: 拨打->接通")
                }
            } else if call.hasEnded {
                print("watchCallStatus: 拨打->结束")
                webVC.liveView.startLiveVideo()
            } else {
                print("watchCallStatus: 拨打")
            }
        } else if call.isOnHold {// 待接通
            print("watchCallStatus: 待接通")
        } else if call.hasConnected {// 接通
            if call.hasEnded {// 结束
                webVC.liveView.startLiveVideo()
                print("watchCallStatus: 接通->结束")
            } else {
                print("watchCallStatus: 接通")
            }
        } else if call.hasEnded {// 结束
            webVC.liveView.startLiveVideo()
            print("watchCallStatus: 结束")
        } else {
            print("watchCallStatus: \(call.uuid)")
            webVC.liveView.stopLiveVideo()
        }
    }
  
}

