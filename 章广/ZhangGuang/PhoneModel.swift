//
//  PhoneModel.swift
//  ZhangGuang
//
//  Created by JAY on 2023/12/7.
//  Copyright © 2023 叁拾叁. All rights reserved.
//

import UIKit
import HandyJSON

struct NSRootModel1<T: HandyJSON> : HandyJSON{
    ///
    var date: String?
    ///
    var data: T?
    /// SUCCESS
    var status: String?
    /// 10000
    var code: Int = 0
    /// 登录成功
    var message: String?
    
    
}
struct PhoneModel : HandyJSON {

    ///
    var token: String?
    ///
    var userInfo: NSUserInfoModel1?

}


struct NSUserInfoModel1 : HandyJSON {
    
    /// 章广镇
     var deptName: String?
     /// <#泛型#>
     var topDeptName: Any?
     ///
     var headImg: String?
     /// app管理员
     var roleName: String?
     ///
     var remarks: String?
     ///
//     var menuList: [Any]?
     /// <#泛型#>
     var lastLoginTime: Any?
     /// <#泛型#>
     var type: Any?
     /// 管理员
     var trueName: String?
     ///
     var createDate: String?
     /// <#泛型#>
     var brithday: Any?
     /// 1
     var status: Int = 0
     /// 0
     var sort: Int = 0
     /// id
     var id: String?
     ///
     var phone: String?
     /// 0
     var sex: Int = 0
     ///
     var salt: String?
     /// 1891@168.com
     var email: String?
     /// 8
     var version: Int = 0
     /// <#泛型#>
     var registerDate: Any?
     ///
     var idCard: String?
     /// 1
     var createBy: Int = 0
     /// 1
     var tenantId: Int = 0
     /// 18914801234
     var mobile: String?
     /// <#泛型#>
     var deptId: Any?
     /// 2
     var updateBy: Int = 0
     ///
     var password: String?
     /// <#泛型#>
     var topDeptId: Any?
     /// USER
     var userType: String?
     ///
     var updateDate: String?
     ///
     var uuid: String?
     /// appAdmin
     var loginName: String?
     /// 3411030008
     var jobNumber: String?

    
}
