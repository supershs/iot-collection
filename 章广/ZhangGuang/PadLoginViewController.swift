//
//  PadLoginViewController.swift
//  ZhangGuang
//
//  Created by JAY on 2023/12/7.
//  Copyright © 2023 叁拾叁. All rights reserved.
//

import UIKit
import SnapKit
import HandyJSON
import Toast
import RxAlamofire
import RxSwift
import SwiftyJSON
import Alamofire
import KeychainAccess
class PadLoginViewController: UIViewController,UITextFieldDelegate {
//    let disposeBag = DisposeBag()
    let disposBag = DisposeBag()

    let minUsernameLength = 1
    let maxUsernameLength = 11
    let minPasswordLength = 1
    let maxPasswordLength = 30
    
    var TSKeychain: Keychain! = Keychain()
    
    var showAll : Bool!
    var usernameTF: UITextField = {
        let tf = UITextField()
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.textColor = .black
       
        tf.placeholder = "请输入账号"
        return tf
    }()
    

    var passwordTF: UITextField = {
        let tf = UITextField()
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.textColor = .black
        tf.placeholder = "请输入密码"
        tf.isSecureTextEntry = true
        return tf
    }()
    
    //验证码登录 文字为空 方便后面展开
    var codeButton: UIButton = {
        let v = UIButton()
        v.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        v.setTitleColor(UIColor(hex: "#1ecca7"), for: .normal)
        v.setTitle("", for: .normal)
        v.layer.masksToBounds = true
        return v
    }()
    
    
  
    var loginButton: UIButton = {
        let v = UIButton()
        v.backgroundColor = UIColor.white
        v.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        v.setTitleColor( .white, for: .normal)
        v.backgroundColor = UIColor(hex: "#1ecca7")
//        v.setBackgroundImage(UIImage(named: "login_btn"), for: .normal)
        v.setTitle("登录", for: .normal)
        v.layer.cornerRadius = 5
        v.layer.masksToBounds = true
        return v
    }()
    
    
    
    

    
    var logoImgView: UIImageView = {
        let v = UIImageView()
        v.image = UIImage(named: "loginBg")
        return v
    }()
    var accountImgView: UIImageView = {
        let v = UIImageView()
        v.image = UIImage(named: "yh")

        return v
    }()
    var passwordImgView: UIImageView = {
        let v = UIImageView()
        v.image = UIImage(named: "dun")
        return v
    }()
    var accountSepView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.init(hex: "1ecca7")
        return v
    }()
    var passwordSepView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.init(hex: "1ecca7")
        return v
    }()
    
    var forgotButton: UIButton = {
        let v = UIButton()
        v.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        v.setTitleColor(UIColor(hex:"#888888"), for: .normal)
        v.setTitle("忘记密码", for: .normal)
        v.layer.masksToBounds = true
        return v
    }()
    
    var verticalLineView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.init(hex: "333333")
        return v
    }()
    
    public var eyeBtn : UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named:"yan"), for: .normal)
        return btn
        
    }()
    
    public var rememberBtn : UIButton = {
        let btn = UIButton()
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        btn.setTitleColor(UIColor(hex: "4bca8a"), for: .normal)
        btn.contentHorizontalAlignment = .right
        btn.setTitle("记住密码 ", for: .normal)
        return btn
    }()
    
    public var rememberButton : UIButton = {
        let bt = UIButton()
        bt.setImage(UIImage(named: "select_false"), for: .normal)
        return bt
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showAll = false
       
        
        view.addSubview(logoImgView)
        view.addSubview(accountImgView)
        view.addSubview(passwordImgView)
        view.addSubview(passwordTF)
        view.addSubview(usernameTF)
        view.addSubview(accountSepView)
        view.addSubview(passwordSepView)
        view.addSubview(forgotButton)
        view.addSubview(eyeBtn)
        view.addSubview(verticalLineView)
        
        view.addSubview(codeButton)
        view.addSubview(rememberBtn)
        view.addSubview(rememberButton)
        view.addSubview(loginButton)
        
        
        usernameTF.delegate = self
        passwordTF.delegate = self
        loginButton.addTarget(self, action: #selector(logOnClick), for: .touchUpInside)
        codeButton.addTarget(self, action: #selector(codeClick), for: .touchUpInside)
        forgotButton.addTarget(self, action: #selector(forgotClick), for: .touchUpInside)
        eyeBtn.addTarget(self, action: #selector(eyeClick), for: .touchUpInside)
        rememberButton.addTarget(self, action: #selector(rememberBtnClick), for: .touchUpInside)
        
        
        //注册点击事件
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))

        
        view.backgroundColor = .white
        logoImgView.snp.makeConstraints { make in
            make.top.left.right.equalToSuperview()
            make.height.equalTo(323)
        }

        accountImgView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(25)
            make.top.equalTo(logoImgView.snp.bottom).offset(51)
            make.width.equalTo(18)
            make.height.equalTo(22)
        }
      
        usernameTF.snp.makeConstraints { make in
            make.left.equalTo(accountImgView.snp.right).offset(10)
            make.top.equalTo(logoImgView.snp.bottom).offset(43)
            make.right.equalToSuperview().offset(-25)
            make.height.equalTo(40)
        }
    
        accountSepView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(25)
            make.top.equalTo(usernameTF.snp.bottom).offset(1)
            make.right.equalToSuperview().offset(-25)
            make.height.equalTo(1)
        }
        
        passwordImgView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(25)
            make.top.equalTo(accountSepView.snp.bottom).offset(60)
            make.width.equalTo(18)
            make.height.equalTo(20)
        }
        
        forgotButton.snp.makeConstraints { make in
            make.top.equalTo(accountSepView.snp.bottom).offset(50)
            make.right.equalToSuperview().offset(-25)
            make.height.equalTo(21)
            make.width.equalTo(100)
        }
        
        
        
        passwordTF.snp.makeConstraints { make in
        make.right.equalTo(forgotButton.snp.left).offset(10)
        make.top.equalTo(accountSepView.snp.bottom).offset(50)
        make.left.equalTo(passwordImgView.snp.right).offset(10)
        make.height.equalTo(40)
        }
        
        passwordSepView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(25)
            make.top.equalTo(passwordTF.snp.bottom).offset(1)
            make.right.equalToSuperview().offset(-25)
            make.height.equalTo(1)
        }
        
    
        

        verticalLineView.snp.makeConstraints { make in
            make.centerY.equalTo(forgotButton)
//            make.top.equalTo(passwordTF.snp.bottom).offset(1)
            make.right.equalTo(forgotButton.snp_left).offset(0)
            make.height.equalTo(15)
            make.width.equalTo(1)
        }
        
        eyeBtn.snp.makeConstraints { make in
            make.width.equalTo(20)
            make.height.equalTo(20)
            make.right.equalTo(verticalLineView.snp_left).offset(-10)
            make.centerY.equalTo(forgotButton)
        }
        
        rememberBtn.snp.makeConstraints { make in
            make.top.equalTo(passwordSepView.snp.bottom).offset(10)
            make.left.equalTo(passwordSepView.snp.left).offset(5)
            make.width.equalTo(70)
            make.height.equalTo(30)
        }
        
        rememberButton.snp.makeConstraints { make in
            make.centerY.equalTo(rememberBtn)
            make.right.equalTo(rememberBtn.snp.left).offset(10)
            make.width.equalTo(15)
            make.height.equalTo(15)
        }
        
        
        codeButton.snp.makeConstraints { make in
            make.centerX.equalTo(view)
            make.top.equalTo(view.snp_bottom).offset(-100)
            make.height.equalTo(44)
            make.width.equalTo(200)
        }
        

        loginButton.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(25)
            make.bottom.equalTo(codeButton.snp.top).offset(-20)
            make.right.equalToSuperview().offset(-25)
            make.height.equalTo(44)
        }
        
        
        
    }
    
    //点击空白处关闭键盘方法
    @objc func handleTap(sender: UITapGestureRecognizer) {
           if sender.state == .ended {
               print("收回键盘")
               self.usernameTF.resignFirstResponder()//username放弃第一响应者
               self.passwordTF.resignFirstResponder()//password放弃第一响应者
           }
           sender.cancelsTouchesInView = false
       }
    
    //    MARK: - 登录
    @objc func logOnClick(){
        
        if usernameTF.text!.isEmpty {
            TSProgressHUD.ts_showWarningWithStatus("请输入账号")
        }else if passwordTF.text!.isEmpty {
            TSProgressHUD.ts_showWarningWithStatus("请输入密码")
        }else{
            
            goLogin(loginName: "", password: "")
            
        }
    }
    
    //    MARK: - 验证码登录
    @objc func codeClick(){
//        let regVC = PhoneViewController()
//        self.navigationController?.pushViewController(regVC, animated: false)
    }
    //    MARK: - 忘记密码
    @objc func forgotClick(){
        print("忘记密码kWebUrl===",padUrl + forgetUrl)
        self.navigationController?.pushViewController(ZGWebViewController(path: kAppLoginUrl + forgetUrl.getEncodeString), animated: true)
    }
    
    //    MARK: -  查看密码
    @objc func eyeClick(){
        
        if showAll{
            showAll = false
            passwordTF.isSecureTextEntry = true
        }else{
            showAll = true
            passwordTF.isSecureTextEntry = false
        }
       
    }
    
    //    MARK: -  记住密码
    @objc func rememberBtnClick(){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        print("appDelegate === ",appDelegate.record)
        if appDelegate.record == "1"{
            print("忘记密码灰色",appDelegate.record)
            appDelegate.record = "0"
            rememberButton.setImage(UIImage(named: "select_false"), for: .normal)
             
        }else{
            print("忘记密码亮色",appDelegate.record)
            appDelegate.record = "1"
            rememberButton.setImage(UIImage(named: "select_true"), for: .normal)
            
        }
    }
    

    func goLogin(loginName: String, password: String){
        let parameters  = ["loginName":usernameTF.text!,"password":AESCode.endcode_AES_CBC(strToEncode: passwordTF.text!),"url":"33app.33iot.com"]

        RxAlamofire.requestJSON(.post, URL(string: logoURL)!, parameters: parameters, encoding: JSONEncoding.default, headers: ["Content-Type":"application/json;charset=UTF-8"])
                  .debug()
                                                                                                        
                  .subscribe(onNext: { (r, json) in
                    if let dict = json as? [String: AnyObject] {
                     
                        let modelA = NSRootModel1<PhoneModel>.deserialize(from: dict)
                    
                        if modelA?.status == "SUCCESS"  {
                            
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            print("appDelegate.record === ",appDelegate.record)
                            if appDelegate.record == "0"{
                                
                                do {
                                    try self.TSKeychain.removeAll()
                                } catch {
                                    print("An error occurred: \(error)")
                                }
                                
                                self.TSKeychain["username"] = self.usernameTF.text!
                            }else{
                                // 将值存储到 Keychain 中
//                                try TSKeychain.removeAll()
                                self.TSKeychain["username"] = self.usernameTF.text!
                                self.TSKeychain["password"] = self.passwordTF.text!
                                print("将值存储到 Keychain 中")
                            }
                            UserDefaults.standard.set(modelA?.data?.token, forKey: "token")
                            UserDefaults.standard.set(self.usernameTF.text!, forKey: "username")
                            UserDefaults.standard.set(self.passwordTF.text!, forKey: "password")
                            print(">>>>>>>>>>>>>>>>>>>自己的打印>>>>>>>>>>>>>>>>>>>>>>")
                            print(">>>>>>>>>>>>>>>>>>>自己的打印>>>>>>>>>>>>>>>>>>>>>>",modelA?.data?.userInfo?.jobNumber)
                            let urlStr =  GOTOH + (modelA?.data?.toJSONString() ?? "")
                            
                            self.navigationController?.pushViewController(ZGWebViewController(path: kAppLoginUrl + urlStr.getEncodeString), animated: true)
                            
                        }else{
                            TSProgressHUD.ts_showWarningWithStatus(modelA?.message ?? "")
                            }

                        }
                      
                  }, onError: { (error) in
                        print(error,"=====error")
                  })
                  .disposed(by: disposBag)
    }
    
    
    //textField点击return关闭键盘
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view?.endEditing(false)
        return true
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
                      // 从 Keychain 中检索值
                      if let username = TSKeychain["username"] {
                          print("Username: \(username)")
                          usernameTF.text = username
                      } else {
                          usernameTF.text = ""
                          print("No username stored in Keychain.")
                      }
                      
                      if let password = TSKeychain["password"] {
                          print("Username: \(password)")
                          passwordTF.text = password
              //            rememberButton.setImage(UIImage(named: "select_true"), for: .normal)
                          let appDelegate = UIApplication.shared.delegate as! AppDelegate
                          appDelegate.record = "0"
                          passwordTF.text = password
                      } else {
                          let appDelegate = UIApplication.shared.delegate as! AppDelegate
                          
                          if appDelegate.record == "0"{
                              appDelegate.record = "0"
                          }else{
                              appDelegate.record = "1"
                          }
              //            let username = TSKeychain["username"]
              //            let password = TSKeychain["password"]
                          passwordTF.text = ""
              //            rememberButton.setImage(UIImage(named: "select_false"), for: .normal)
                          print("No username stored in Keychain.")
                      }
        
    }
                                     
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
        
    }

}

//extension String {
//    /// String转encode
//        var getEncodeString: String {
//            guard self.count != 0 else { return ""}
//            if let u = self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
//                return u
//            }
//            return ""
//        }
//}

