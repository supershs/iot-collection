//
//  ZGWebViewController.swift
//  vgbox
//
//  Created by 宋海胜 on 2019/9/15.
//  Copyright © 2019 Swift Xcode. All rights reserved.
//

import UIKit
import WebKit
import SwiftWeb
import MapKit

enum JsMethod {
    case passGPSInfo
    case passOther
}

// 自定义编辑
open class ZGWebViewController: BaseWebViewController, CLLocationManagerDelegate {
    
    fileprivate let location = LocationManager()
    fileprivate var jsType: NSNumber = NSNumber()
    fileprivate var isOnece = false
    fileprivate var configManager = Tool()
    fileprivate var backImgView = UIImageView()
    internal var liveView = ZGLiveView()
    public var canAllButUpsideDown = false
    
    deinit {
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "getGPSInfo")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "passVideoParams")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "leaveVideoPage")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "loginOut")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "webGoBack")
    }
    
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        liveView.currentVC = self
        
        self.webView.frame = CGRect(x: 0, y: kStatusBarHeight, width: kScreenWidth, height: kScreenHeight - kBottomSafeHeight-kStatusBarHeight)
        self.backImgView = self.configManager.getLanuchboardImage()
        self.backImgView.frame = self.view.bounds
        self.view.addSubview(self.backImgView)
        self.configManager.topMangain(webView: webView, vc: self)
        
        judgeNetwork()
        location.manager.delegate = self
        location.locationProtocol = self
        
        liveView.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
    }
    
    open override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        if size.width > size.height {// 横屏
            liveView.blanner!.frame = CGRect(x: 0, y: 0, width: kScreenHeight, height: kScreenWidth)
            liveView.showView(isFullScreen: true)
        } else {
            liveView.blanner!.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenWidth*9/16)
            liveView.hideAboveViews()
            self.view.hideToastActivity()
            liveView.showView(isFullScreen: false)
        }
    }
    
    open override func injectSwiftMethod(_ config: WKWebViewConfiguration) {
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "getGPSInfo")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "passVideoParams")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "leaveVideoPage")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "loginOut")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "webGoBack")
        
    }
    
    open override func jsAction() {
        
    }
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    // 在viewDidLoad中模态不行，此时页面还没加载完成
    fileprivate func judgeNetwork() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {

            if self.configManager.judgeNetworkState(self) {
                self.loadWeb()
            }
        }
    }
    
  
    
    fileprivate func jsAction(param: String, type: JsMethod) {
        
        var javascript: String = ""
        
        switch type {
            
        case .passGPSInfo:
            javascript = "javascript:passGPSInfo(\"\(param)\")"
        case .passOther:
            print("passOther")
        }
        self.webView.evaluateJavaScript(javascript) { (res, error) in
            print("swift向js传值\n  param: \(param)\n  response: \(res ?? "nil")\n  error: \(error?.localizedDescription ?? "nil")")
        }
    }
    
    open override func swiftAction(_ name: String, _ body: Dictionary<String, Any>) {
        
        switch name {
                
            case "loginOut":
            //            UserInstance.userLogout()
                        self.navigationController?.popToRootViewController(animated: false)
            case "webGoBack":
                //            UserInstance.userLogout()
                self.navigationController?.popToRootViewController(animated: false)
                
        case "getGPSInfo":
            let type = body["type"] as! NSNumber
            gpsService(type)
            
        case "passVideoParams":
            setVideoPage(body)
            
        case "leaveVideoPage":
            liveView.leaveVideoPage()
            
        case "toDownLoad":
            let url = body["url"] as? String ?? ""
            UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
            
        case "callSomeOne":
            let phoneNum = body["phoneNum"] as? String ?? ""
            self.configManager.callSomeOne(phoneNum)
            
        case "toGaodeApp":
            Tool.routePlanning(body)
            
        default:
            break
        }
    }
    
    open override func getH5Url(_ url: String) {
        
        if (url.contains("tel://")) {
            let phoneUrl = url.components(separatedBy: "tel://").last
            self.configManager.callSomeOne(phoneUrl!)
        }
    }
    
    open override func loadSuccess() {
        
        UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseInOut, animations: {
            self.backImgView.alpha = 0
        }) { (finish) in
            self.backImgView.isHidden = true
        }
    }
    
    open override func loadFail() {
        backImgView.isHidden = false
        self.configManager.alertToReload(self) {
            self.loadWeb()
        }
    }
    
    // 定位
    fileprivate func gpsService(_ type: NSNumber) {
        
        //只获取一次
        isOnece = true
        self.jsType = type
        self.location.startLocation()
    }
    
    func setVideoPage(_ params: Dictionary<String, Any>) {
        if liveView.player != nil {
            self.liveView.fullScreenAction()
            return
        }
        Tool.requestAuthorizationPhotoLibary()
        Tool.requestMicroPhoneAuth()
        self.canAllButUpsideDown = true
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.canAllButUpsideDown = true
        self.liveView.configureSDK(params)
        self.liveView.fullScreenAction()
    }
    
    // CLLocationManagerDelegate
    // 每隔一段时间就会调用
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        for (_, loc) in locations.enumerated() {
            let l: CLLocationCoordinate2D = loc.coordinate
            let lat = l.latitude
            let lng = l.longitude
            self.getGPSSuccess(latitude: lat, longitude: lng)
        }
    }

    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {

        print("无法获取位置信息 \(error.localizedDescription)")
        self.getGPSFailure(error: error)
    }
}

extension ZGWebViewController: LocationProtocol {
    public func getLocationSuccess(_ area: String, _ locality: String, _ subLocality: String, _ thoroughfare: String, _ name: String) {
        
    }
    public func getGPSAuthorizationFailure() {
        let alert = UIAlertController(title: "提示", message: "请打开定位，以便获取您的位置信息", preferredStyle: .alert)
                     
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
                     
        self.present(alert, animated: true, completion: nil)
    }
    
    
    public func getGPSSuccess(latitude: Double, longitude: Double) {
        location.stopLocation()
        if (self.isOnece) {
            print("lng: \(longitude) lat: \(latitude) type: \(self.jsType)")
            let param = "\(longitude),\(latitude),\(self.jsType)"
            self.jsAction(param: param, type: .passGPSInfo)
            self.isOnece = false
        }
    }
    
    public func getGPSFailure(error: Error) {
        self.isOnece = false
        print("getMoLocation error: \(error.localizedDescription)")
        if (!self.isOnece) {
            location.stopLocation()
        }
    }
}

