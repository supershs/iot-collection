//
//  YQBaseTableViewController.m
//  YiQuNew
//
//  Created by Nasy on 2017/6/8.
//  Copyright © 2017年 Nasy. All rights reserved.
//

#import "YQBaseTableViewController.h"
@interface YQBaseTableViewController ()<UINavigationControllerDelegate,UITableViewDelegate,UITableViewDataSource>


@end

@implementation YQBaseTableViewController

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    //navigation标题文字颜色
         NSDictionary *dic = @{NSForegroundColorAttributeName :UIColorFromHex(0xffffff),
                               NSFontAttributeName : [UIFont systemFontOfSize:18]};
      
         if (@available(iOS 13.0, *)) {
             UINavigationBarAppearance *barApp = [UINavigationBarAppearance new];
             barApp.backgroundColor = [UIColor whiteColor];
             #//基于backgroundColor或backgroundImage的磨砂效果
             barApp.backgroundImage = [UIImage imageNamed:@"nav"];
             barApp.backgroundEffect = nil;
             #//阴影颜色（底部分割线），当shadowImage为nil时，直接使用此颜色为阴影色。如果此属性为nil或clearColor（需要显式设置），则不显示阴影。
             barApp.shadowColor = [UIColor whiteColor];
             //标题文字颜色
             barApp.titleTextAttributes = dic;
             self.navigationController.navigationBar.scrollEdgeAppearance = barApp;
             self.navigationController.navigationBar.standardAppearance = barApp;
         }else {
            self.navigationController.navigationBar.titleTextAttributes = dic;
            UIImage *bgImage = [[UIImage imageNamed:@"nav"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch];
                [self.navigationController.navigationBar  setBackgroundImage : bgImage forBarMetrics:UIBarMetricsDefault];
         }
      //透明设置
      self.navigationController.navigationBar.translucent = NO;
      //navigationItem控件的颜色
      self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"back_arrow"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    //隐藏默认的返回箭头
       self.navigationController.navigationBar.backIndicatorImage = [UIImage new];
       self.navigationController.navigationBar.backIndicatorTransitionMaskImage = [UIImage new];

    

    
    [SVProgressHUD dismiss];
    
}

-(void)back{
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;


}
-(void)viewWillDisappear:(BOOL)animated{
    
    [SVProgressHUD dismiss];
    
    [super viewWillDisappear:animated];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return   UITableViewAutomaticDimension;
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//
//- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
//                                  animationControllerForOperation:(UINavigationControllerOperation)operation
//                                               fromViewController:(UIViewController*)fromVC
//                                                 toViewController:(UIViewController*)toVC
//{
//    if (operation != UINavigationControllerOperationNone) {
//        return [AMWaveTransition transitionWithOperation:operation andTransitionType:AMWaveTransitionTypeBounce];
//    }
//    return nil;
//}


- (void)dealloc
{
    [self.navigationController setDelegate:nil];
}




//#pragma mark - Table view data source
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//
//    return 0;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return 0;
//}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
