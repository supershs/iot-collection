//
//  ErrorView.m
//  suyuanAPP
//
//  Created by 叁拾叁 on 2021/11/5.
//  Copyright © 2021 江苏南京叁拾叁. All rights reserved.
//

#import "ErrorView.h"

@interface ErrorView ()



@property(nonatomic,strong)UIImageView * iconImg;
@property(nonatomic,strong)UILabel *messageLabel;
@property(nonatomic,strong)UIButton *messageBtn;


@end

@implementation ErrorView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		self.backgroundColor = UIColorFromHex(0xf8f8f8);
		[self addSubview:self.iconImg];
		[self addSubview:self.messageLabel];
		[self addSubview:self.messageBtn];
		[self setupUI];
		
	}
	return self;
}

-(UIImageView *)iconImg{
	
	if (_iconImg == nil) {
		_iconImg = [[UIImageView alloc]init];
		_iconImg.image = [UIImage imageNamed:@"errorIcon"];
	}
	return  _iconImg;
}

- (UILabel *)messageLabel{
	
	if (_messageLabel == nil) {
		_messageLabel = [[UILabel alloc]init];
		_messageLabel.font = [UIFont systemFontOfSize:14];
		_messageLabel.textColor = UIColorFromHex(0x999999);
		_messageLabel.text = @"抱歉，暂无内容";
		_messageLabel.textAlignment = NSTextAlignmentCenter;
	}
	return _messageLabel;
}

- (UIButton *)messageBtn{
	if (_messageBtn == nil) {
		
		_messageBtn = [[UIButton alloc]init];
		[_messageBtn setHidden:YES];
		[_messageBtn setTitle:@"刷新数据" forState:UIControlStateNormal];
		_messageBtn.titleLabel.font = [UIFont systemFontOfSize:12];
		_messageBtn.layer.borderColor =  UIColorFromHex(0xCCCCCC).CGColor;
		_messageBtn.layer.borderWidth = 0.5;
		_messageBtn.layer.cornerRadius = 2;
		_messageBtn.layer.masksToBounds = YES;
		
	}
	return  _messageBtn;
}

-(void)setupUI{
	
	self.iconImg.frame = CGRectMake((self.frame.size.width - 184)/2, 90, 184, 196);
	self.messageLabel.frame = CGRectMake(15, 295, (self.frame.size.width - 30), 20);
	self.messageBtn.frame = CGRectMake((self.frame.size.width - 95)/2, 345, 95, 30);
}
@end
