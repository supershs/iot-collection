//
//  YQNTabBar.m
//  YiQuNew
//
//  Created by Nasy on 2017/6/7.
//  Copyright © 2017年 Nasy. All rights reserved.
//

#import "YQNTabBar.h"


@interface YQNTabBar()

/**
 *  按钮背景
 */
@property (nonatomic, strong) UIButton *bgBtn;


/**
 *  判断两次选择是否一致
 */
@property (nonatomic, strong) UIControl *control;

@end

@implementation YQNTabBar


#pragma mark - Lifecycle

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        [self config];
    }
    
    return self;
}

#pragma mark - Private Method

- (void)config {
    
    //去掉tabbar上面自带的线
    CGRect rect = CGRectMake(0, 0, kWidth, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [[UIColor clearColor] CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self setBackgroundImage:img];
    [self setShadowImage:img];
    
    self.layer.shadowColor=RGBA(0, 0, 0, 0.35).CGColor;
    self.layer.shadowOffset=CGSizeMake(0, 0 );
    self.layer.shadowOpacity=1;
    self.layer.shadowRadius=15;
    
    
    //给tabar加上自定义的线
//    UIView * view=[[UIView  alloc]initWithFrame:CGRectMake(0, 0, kWidth, 0.5)];
//    view.backgroundColor=[UIColor colorWithRed:205/255.0 green:205/255.0 blue:205/255.0 alpha:0.8];
//    [self addSubview:view];
    
    //或者
    //    self.backgroundColor = [UIColor whiteColor];
    //    UIImageView *topLine = [[UIImageView alloc] initWithFrame:CGRectMake(0, -5,self.width, 5)];
    //    topLine.image = [UIImage imageNamed:@"tapbar_top_line"];
    //    [self addSubview:topLine];
    
    
   
    //设置每个item的属性
    for (int i = 0 ; i < self.items.count; i++) {
        UITabBarItem *child = self.items[i];
        
        
    
        [child setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: UIColorFromHex(0x666666),NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize:12],NSFontAttributeName, nil] forState:UIControlStateNormal];
        [child setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromHex(0x3757D8),NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize:12],NSFontAttributeName, nil] forState:UIControlStateSelected];
        //设置子控制器的图片（storyBoard已经设置）这里不再覆盖设置


        
    }
    
  
}


-(void)awakeFromNib{
    [super awakeFromNib];
    
    [self config];

}

//缩放动画
- (void)addScaleAnimationOnView:(UIView *)animationView repeatCount:(float)repeatCount {
    //需要实现的帧动画，这里根据需求自定义
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animation];
    animation.keyPath = @"transform.scale";
    animation.values = @[@1.0,@1.3,@0.9,@1.15,@0.95,@1.02,@1.0];
    animation.duration = 1;
    animation.repeatCount = repeatCount;
    animation.calculationMode = kCAAnimationCubic;
    [animationView.layer addAnimation:animation forKey:nil];
}
//旋转动画
- (void)addRotateAnimationOnView:(UIView *)animationView {
    // 针对旋转动画，需要将旋转轴向屏幕外侧平移，最大图片宽度的一半
    // 否则背景与按钮图片处于同一层次，当按钮图片旋转时，转轴就在背景图上，动画时会有一部分在背景图之下。
    // 动画结束后复位
    animationView.layer.zPosition = 65.f / 2;
    [UIView animateWithDuration:0.32 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        animationView.layer.transform = CATransform3DMakeRotation(M_PI, 0, 1, 0);
    } completion:nil];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.70 delay:0 usingSpringWithDamping:1 initialSpringVelocity:0.2 options:UIViewAnimationOptionCurveEaseOut animations:^{
            animationView.layer.transform = CATransform3DMakeRotation(2 * M_PI, 0, 1, 0);
        } completion:nil];
    });
}

-(void)layoutSubviews
{
    //不能删，一定要调用，等布局完我们再覆盖行为
    [super layoutSubviews];
    
    
    //覆盖排布
    
    //1.设置自定义按钮的位置
    self.bgBtn.centerX = kWidth*0.5;
    self.bgBtn.centerY = -2;
        
}

 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }

@end

