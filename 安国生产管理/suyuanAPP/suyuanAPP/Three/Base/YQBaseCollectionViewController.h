//
//  YQBaseCollectionViewController.h
//  CompanyAPP_Obj
//
//  Created by 杨琴 on 2019/5/16.
//  Copyright © 2019 YQ. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQBaseCollectionViewController : UICollectionViewController

@end

NS_ASSUME_NONNULL_END
