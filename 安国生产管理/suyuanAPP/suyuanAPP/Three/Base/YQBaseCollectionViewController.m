//
//  YQBaseCollectionViewController.m
//  CompanyAPP_Obj
//
//  Created by 杨琴 on 2019/5/16.
//  Copyright © 2019 YQ. All rights reserved.
//

#import "YQBaseCollectionViewController.h"

@interface YQBaseCollectionViewController ()

@end

@implementation YQBaseCollectionViewController

static NSString * const reuseIdentifier = @"Cell";


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

         NSDictionary *dic = @{NSForegroundColorAttributeName :UIColorFromHex(0xffffff),
                               NSFontAttributeName : [UIFont systemFontOfSize:18]};
      
         if (@available(iOS 13.0, *)) {
             UINavigationBarAppearance *barApp = [UINavigationBarAppearance new];
             barApp.backgroundColor = [UIColor whiteColor];
             #//基于backgroundColor或backgroundImage的磨砂效果
             barApp.backgroundImage = [UIImage imageNamed:@"nav_other"];
             
             barApp.backgroundEffect = nil;
             #//阴影颜色（底部分割线），当shadowImage为nil时，直接使用此颜色为阴影色。如果此属性为nil或clearColor（需要显式设置），则不显示阴影。
             barApp.shadowColor = [UIColor whiteColor];
             //标题文字颜色
             barApp.titleTextAttributes = dic;
             self.navigationController.navigationBar.scrollEdgeAppearance = barApp;
             self.navigationController.navigationBar.standardAppearance = barApp;
             barApp.shadowImage = [UIImage imageNamed:@"nav_other"];
             barApp.shadowColor=nil;//分割线去除
//                      [UINavigationBar appearance].standardAppearance= barApp;
//                      [UINavigationBar appearance].scrollEdgeAppearance=[UINavigationBar appearance].standardAppearance;//重新赋值
         }else {
            self.navigationController.navigationBar.titleTextAttributes = dic;
            UIImage *bgImage = [[UIImage imageNamed:@"nav_other"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch];
                 [self.navigationController.navigationBar  setBackgroundImage : bgImage forBarMetrics:UIBarMetricsDefault];
         }
      //透明设置
      self.navigationController.navigationBar.translucent = NO;
      //navigationItem控件的颜色
      self.navigationController.navigationBar.tintColor = [UIColor blackColor];

}


-(void)viewWillDisappear:(BOOL)animated{
    [SVProgressHUD dismiss];

    [super viewWillDisappear:animated];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];

    UIImage *bgImage = [[UIImage imageNamed:@"nav_other"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch];
    [self.navigationController.navigationBar  setBackgroundImage : bgImage forBarMetrics:UIBarMetricsDefault];

  
    NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName,[UIFont systemFontOfSize:16],NSFontAttributeName,nil];
    [self.navigationController.navigationBar setTitleTextAttributes:attributes];
    
    if ([self.collectionView     respondsToSelector:@selector(setContentInsetAdjustmentBehavior:)]) {
        if (@available(iOS 11.0, *)) {
            self.collectionView.contentInsetAdjustmentBehavior =     UIScrollViewContentInsetAdjustmentNever;
        } else {
            // Fallback on earlier versions
        }
    }
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"nav_other"] forBarMetrics:UIBarMetricsDefault];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
}
/// 统一设置导航栏外观
 - (void)setNavigationBarStyle
 {
     UINavigationBar *navBar = [UINavigationBar appearance];
     /** 设置导航栏背景图片 */
     [navBar setBackgroundImage:[UIImage imageNamed:@"nav"] forBarMetrics:UIBarMetricsDefault];
     /** 设置导航栏标题文字颜色 */
     NSDictionary *dict = @{
                            NSForegroundColorAttributeName : [UIColor whiteColor]
                            };
     [navBar setTitleTextAttributes:dict];
 }
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 0;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    // Configure the cell
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
    return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
    
}
*/

@end
