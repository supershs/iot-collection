//
//  YQBaseViewController.m
//  YiQuStaff
//
//  Created by 杨琴 on 2017/11/21.
//  Copyright © 2017年 yiquStaff. All rights reserved.
//

#import "YQBaseViewController.h"

@interface YQBaseViewController ()

@end

@implementation YQBaseViewController
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //navigation标题文字颜色
         NSDictionary *dic = @{NSForegroundColorAttributeName :UIColorFromHex(0xffffff),
                               NSFontAttributeName : [UIFont systemFontOfSize:18]};
      
         if (@available(iOS 13.0, *)) {
             UINavigationBarAppearance *barApp = [UINavigationBarAppearance new];
             barApp.backgroundColor = [UIColor whiteColor];
             #//基于backgroundColor或backgroundImage的磨砂效果
             barApp.backgroundImage = [UIImage imageNamed:@"nav"];
             barApp.backgroundEffect = nil;
             #//阴影颜色（底部分割线），当shadowImage为nil时，直接使用此颜色为阴影色。如果此属性为nil或clearColor（需要显式设置），则不显示阴影。
             barApp.shadowColor = [UIColor whiteColor];
             //标题文字颜色
             barApp.titleTextAttributes = dic;
             self.navigationController.navigationBar.scrollEdgeAppearance = barApp;
             self.navigationController.navigationBar.standardAppearance = barApp;
         }else {
            self.navigationController.navigationBar.titleTextAttributes = dic;
            UIImage *bgImage = [[UIImage imageNamed:@"nav"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch];
                [self.navigationController.navigationBar  setBackgroundImage : bgImage forBarMetrics:UIBarMetricsDefault];
         }
      //透明设置
      self.navigationController.navigationBar.translucent = NO;
      //navigationItem控件的颜色
      self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"back_arrow"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    //隐藏默认的返回箭头
       self.navigationController.navigationBar.backIndicatorImage = [UIImage new];
       self.navigationController.navigationBar.backIndicatorTransitionMaskImage = [UIImage new];

    

    
    [SVProgressHUD dismiss];
    
}

-(void)back{
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [SVProgressHUD dismiss];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
