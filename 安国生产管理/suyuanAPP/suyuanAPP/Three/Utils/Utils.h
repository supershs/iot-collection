//
//  Utils.h
//  TestDemoT
//
//  Created by 杨琴 on 2017/7/29.
//  Copyright © 2017年 Yq. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CommonCrypto/CommonCryptor.h>

//@class YQBaseModel;


@interface Utils : NSObject


+ (NSString*) getUUIDString;

#pragma mark 判断字符串是否为空，null，nil等...(YES：为空)
+ (BOOL)valString:(NSString *)string;

#pragma mark 判断字符串是否为空，空返回@""不为空返回本身
+ (NSString *)stringTurnString:(NSString *)string;

#pragma mark 判断字符串是否是数字 是返回本身不是返回0
+ (NSString *)stringTurnNumber:(NSString *)string;

#pragma mark 判断空对象

+(BOOL) isNullObject:(id)object;


#pragma mark 字符串数组转数组
+(NSMutableArray *)toArrayOrNSDictionary:(NSString *)jsonString;

#pragma mark 字典转字符串
+ (NSString*)dictionaryToJson:(NSDictionary *)dic;

#pragma mark - 将数组转json字符串
+ (NSString *)arrayToJSONString:(NSArray *)array;
#pragma mark 字符串字典转字典
+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString;

#pragma mark data转字典
+ (NSDictionary *)dictionaryWithContentsOfData:(NSData *)data ;

#pragma mark 提取字符串中的数字
+ (NSString *)getNumberFromStr:(NSString *)str;



#pragma mark 验证身份证号码
+ (BOOL)IsIdentityCard:(NSString *)IDCardNumber;

#pragma mark 验证手机号
+ (BOOL) IsPhoneNumber:(NSString *)number;

#pragma mark 验证银行卡
+ (BOOL) IsBankCard:(NSString *)cardNumber;

#pragma mark a中文数字 转 阿拉伯数字
+ (NSString *)turnUppercaseLowercaseWithString:(NSString *)vioceString;

#pragma mark 将#000000颜色转化成RGB
+ (UIColor *) colorFromHexCode:(NSString *)hexString;

#pragma mark 倒计时
+(void)openCountdown:(UIButton *)authCodeBtn;
#pragma mark - 获取当前时间的 时间戳
+(NSInteger)getNowTimestamp;

#pragma mark - 将某个时间转化成 时间戳
+(NSInteger)timeSwitchTimestamp:(NSString *)formatTime andFormatter:(NSString *)format;

#pragma mark - 将某个时间戳转化成 时间
+(NSString *)timestampSwitchTime:(NSString *)timestamp andFormatter:(NSString *)format;

#pragma mark 方便字典保存到nsdatefa
+(NSObject *)objectRemoveEmptyValue:(NSObject *)obj;

#pragma  mark 字典转URl字符串
+(NSString *)dictionaryToString:(NSDictionary *)dict;

+ (NSString *)setupRequestMonth;

//集合转字符串
+(NSString *)stringWithContentsOfModelArray:(NSArray *)ary;

//字符串拆分为数组
+(NSArray *)stringAppcomdAry:(NSString *)images;


+(NSArray *)newTwomonth:(NSInteger)Index;


//+(void)showReasonWithItem:(YQBaseModel *)item Error:(NSError *)error;

+(NSString *)firstCharactor:(NSString *)aString;

//yymodel 自己转数组

+ (UIWindow *)mainWindow;
//按比例缩放图片
+ (UIImage *) scaleFromImage: (UIImage *) image toSize: (CGSize) targetSize;
//固定宽度g缩放f图片

+(UIImage *) imageCompressForWidth:(UIImage *)sourceImage targetWidth:(CGFloat)defineWidth;

/**base64加密*/
+(NSString *)stringBase64:(NSString *)string64;
//写入本地plist
+ (void)saveUserInfotoLocalWithDic:(NSDictionary *)dic;
+ (NSDictionary *)getUserInfofromLocal:(NSString *)namePlist;

+(NSString *)code:(NSInteger)code;

+(NSString *)unit:(NSInteger)code;

+(NSString *)type:(NSInteger)code;

+(NSString *)title:(NSInteger)code;

+(NSString *)pestControlTargets:(NSInteger)code;

@end
