//
//  YQDataManager.h
//  YiQuStaff
//
//  Created by 杨琴 on 2017/11/14.
//  Copyright © 2017年 yiquStaff. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking/AFNetworking.h"



///请求返回状态定义
typedef NS_ENUM(NSInteger, YQResponseCode) {
    ///成功
    YQResponseCode_Success = 10000,
    ///保存失败
    YQResponseCode_SaveError = 20000
    
};

#define PageSize 10

#pragma mark - 数据model基类
@interface YQListBaseModel :NSObject
@property (nonatomic, assign) NSInteger size;
@property (nonatomic, assign) NSInteger pageSize;
@property (nonatomic, assign) NSInteger pages;// 总页数
@property (nonatomic, assign) NSInteger current;//当前页
@property (nonatomic, assign) NSInteger total;
@property (nonatomic, assign) NSInteger navigateFirstPage;
@property (nonatomic, assign) NSInteger endRow;
@property (nonatomic, assign) BOOL hasPreviousPage;
@property (nonatomic, assign) NSInteger prePage;
@property (nonatomic, assign) NSInteger lastPage;
@property (nonatomic, assign) NSInteger firstPage;
@property (nonatomic, assign) BOOL isFirstPage;
@property (nonatomic, assign) NSInteger navigatePages;
@property (nonatomic, assign) NSInteger pageNum;
@property (nonatomic, assign) BOOL isLastPage;
@property (nonatomic, assign) NSInteger nextPage;
@property (nonatomic, assign) BOOL hasNextPage;
@property (nonatomic, assign) BOOL searchCount;
@property (nonatomic, strong) NSArray *navigatepageNums;


@end

@interface YQDataBaseModel :NSObject
@property (nonatomic, strong) NSString *updateDate;
@property (nonatomic, assign) double sort;
@property (nonatomic, assign) double version;
@property (nonatomic, assign) double createBy;
@property (nonatomic, assign) double updateBy;
@property (nonatomic, strong) NSString *createDate;
@property (nonatomic, strong) NSString *remarks;
@property (nonatomic, assign) double status;

@end

@interface YQBaseModel  : NSObject
@property (nonatomic ,assign) YQResponseCode code;
@property (nonatomic ,assign) id data;
@property (nonatomic ,strong) NSString *date;
@property (nonatomic ,strong) NSString *message;
@property (nonatomic ,strong) NSString *status;
@end

#pragma mark - 网络请求基类

/**
 *  数据请求返回的block定义
 *
 *  @param info   若为请求成功，则为以JSonModel基类的对应model，
 *                若请求失败，而为NSError
 */
typedef void(^YQNetworkResult)(BOOL statue, YQBaseModel * item, id info);

/**
 *  AFNCLient 请求回调的块声明
 *  基本的上传及下载进度、请求完成、请求失败， Block回调
 */
typedef void (^KKProgressBlock)(float progress);
typedef void (^KKCompletedBlock)(id JSON,NSString *stringData,int statusCode);
typedef void (^KKFailedBlock)(NSError *error);


/**
 *上传文件
 进度条返回
 
 */
typedef void (^YQProgressBlock)(float progress);

@interface YQDataManager : AFHTTPSessionManager

//单例模式
+ (YQDataManager *)manager;

/**
 *  GET方式请求数据
 *
 *  @param url        请求url
 *  @param params    请求参数
 *  @param modelClass 返回结果的model类，如果为空，则返回结果不做model化处理
 *  @param respBlock  返回结果的block
 */
+ (void)getDataWithUrl:(NSString *)url
                Params:(NSDictionary *)params
            modelClass:(Class)modelClass
         responseBlock:(YQNetworkResult)respBlock;
/**
 *  POST方式入参Json请求数据
 *
 *  @param url        请求url
 *  @param params    请求参数
 *  @param modelClass 返回结果的model类，如果为空，则返回结果不做model化处理
 *  @param respBlock  返回结果的block
 */
+ (void)postDataJsonRequestWithUrl:(NSString *)url
                            Params:(NSDictionary *)params
                        modelClass:(Class)modelClass
                     responseBlock:(YQNetworkResult)respBlock;

/**
 *  POST方式入参Http请求数据
 *
 *  @param url        请求url
 *  @param params    请求参数
 *  @param modelClass 返回结果的model类，如果为空，则返回结果不做model化处理
 *  @param respBlock  返回结果的block
 */
+ (void)postDataHttpRequestWithUrl:(NSString *)url
                 Params:(NSDictionary *)params
             modelClass:(Class)modelClass
          responseBlock:(YQNetworkResult)respBlock;



#pragma mark - Utils

/**
 *  根据错误描述信息构造NSError
 *
 *  @param errorMsg 错误描述内容
 *  @param code 错误码
 *  @return NSError
 */
+ (NSError *)errorWithDescription:(NSString *)errorMsg cede:(NSInteger)code;



/**
 *  退出登录清除本地个人数据
 */
+(void)logOutToClearLocalData;


+ (void)postUpdataHttpRequestWithUrl:(NSString *)url
                              params:(NSDictionary *)aParams
                              images:(NSArray<UIImage *> *)images
                                name:(NSString *)name
                            mimeType:(NSString *)mimeType
                            fileName:(NSString *)fileName
                          modelClass:(Class)modelClass
                       responseBlock:(YQNetworkResult)respBlock;

@end

