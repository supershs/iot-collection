//
//  APiMacros.h
//  suyuanAPP
//
//  Created by 杨琴 on 2019/10/16.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#ifndef APiMacros_h
#define APiMacros_h
//登录加密
#define CBC_KEY  @"1234567890123456"
#define CBC_IV  @"1234567890123456"
#define CBC_SALT  @"1234567890123456"

//接口名称相关
#define API_BASE_URL_STRING     @"http://114.115.166.192:8100/api/1.1/"
#define API_Image_URL     @"http://114.115.166.192:8100/"
#define API_URL @"114.115.166.192:8100"



/*图片识别    参数：images */
#define pictureKnowURl [NSString stringWithFormat:@"%@Detection/classify",API_BASE_URL_STRING]
/*图片上传*/
#define UrlFile @"sysFile/uploadImgs"
/*登录*/
#define LoginUrl @"Login/check"

/*生产管理列表 固定传值 "status": 10018*/
//#define ProductionListPost @"TraceProductionArchi/list"
#define ProductionListPost @"TraceProductionArchi/listApp"


/*养殖投入品列表 */
#define ProductionInputPost @"TraceInput/list"
/*畜禽投入品列表 */
#define ProductionLivestockInputPost @"TraceLivestockInput/list"
/*渔业投入品列表 */
#define ProductionFishbreedInputPost @"TraceFishbreedInput/list"
/* 新增单个农事管理表详情
 taskPic 图片id(多图片逗号分隔 例如：15516,13131)；
farmingCode：农事类别；（100100；\r\n播种：100101\r\n灌溉：100102\r\n施肥：100103\r\n用药：100104\r\n除草：100105\r\n插秧：100106\r\n翻地：100107\r\n其他：100108）
archiId 生产档案id(对应生产档案列表)；
product：投入品（对应投入品列表 */
#define ProductionAddPost @"TraceProductionFarming/addAppProductFarming"
/* 新增单个畜禽养殖管理表详情
 请求参数：Integer breedCode；String taskPic；String product；
 传参示例：{"breedCode":"1020101",
 "taskPic":"1234556,44566,4456",
 "product":"[{'id':'11568571462975488','inputConsum':'5','inputUnit':'10030'}]"}
 enterNumber 禽畜养殖类别（禽畜养殖类别（1020100禽畜养殖类别；1020101投喂； 1020102清洁； 1020103防疫;1020104 消毒；1020105投药；1020106投幼崽；1020107其它）；）*/
#define ProductionAddLivestockBreedPost @"TraceProductionLivestockBreed/addAppProductionLivestockBreed"
/* 新增单个渔业管理表详情
 请求参数："fishbreedCode": "300106",
         "taskDate": "2019-12-13 00:00:00",
        fishbreedCode 投喂:300101 增氧:300102 加水:300103 清淤:300104
        @疫苗:300105 @渔药:300106 @鱼苗:300107 其他:300108
 */
#define ProductionAddFishbreedPost @"TraceProductionFishbreed/addAppProductFishbreed"

/*事记录列表sort 传1 代表农事生产(其余则不传)farmingCode：100102 肥料，100103农药 100100种子/ {pageNum}/{pageSize} */
#define ProductionFarmListPost @"TraceProductionFarming/getAppProductionFarmingist"
/*渔业记录列表 是否有投入品（0无，1有）sort   personId */
#define   ProductionFishbreedListPost @"TraceProductionFishbreed/getAppProductionFishbreedList"
/*农事记录详情/{id}*/
#define   ProductionFarmDetailPost @"TraceProductionFarming/getAppProductionFarmingInfoById"
/*渔业记录详情/{id}*/
#define   ProductionFishbreedInfoGet @"TraceProductionFishbreed/getAppProductionFishbreedInfoById"
/**畜禽记录列表  （带分页）*/
#define postAppProductionLivestockBreedList @"TraceProductionLivestockBreed/getAppProductionLivestockBreedList"
/**畜禽记录详情  根据id查询养殖记录详情 */
#define getAppProductionLivestockBreedInfoById @"TraceProductionLivestockBreed/getAppProductionLivestockBreedInfoById"

/*肥料、种子、农药记录*/
#define   Production_FarmDetail_Post @"TraceProductionInput/applist/page"
/*肥料、种子、农药记录详情/{id}*/
#define   ProductionFarmDetailGet @"TraceProductionInput/getinput"
/*APP通过条件获取畜禽生产投入品表分页列表
 Long personId（登录返回的参数里有）
 Integer breedCode（禽畜养殖类别1020100禽畜养殖类别；1020101投喂； 1020102清洁； 1020103防疫*;1020104 消毒；1020105投药*；1020106投幼崽*；1020107其它；）（标记*对应app需要的三个页面）
 反参释义：投入品ID livestockInputId;
         投入品名称 inputName;
        投入品用量 inputConsum;
 计量单位(10030克(g）；10031千克(kg)；10032毫升(ML)；10033升(L)；10034吨；10035其他  Integer inputUnit;*/
#define   appProductionLivestockInputlistPOST @"TraceProductionLivestockInput/appProductionLivestockInputlist/page"
/*获取生产投入品表详情 url:/api/${version}/TraceProductionLivestockInput/appGetLivestockinput/{id} */
#define   appGetLivestockinput @"TraceProductionLivestockInput/appGetLivestockinput"
/*渔业记录*/
#define   Production_FishInput_Post @"TraceProductionFishInput/appList/page"
/*渔业记录详情/{id}*/
#define   ProductionFishInputGet @"TraceProductionFishInput/getInput"

/*农事首页接口*/
#define HomeDataUrl @"TraceProductionFarming/getAppPrincipal"
/*畜禽生产主页*/
#define HomeLivestockUrl @"TraceProductionLivestockBreed/getAppLivestockPrincipal"
/*渔业生产主页*/
#define HomeFishUrl @"TraceProductionFishbreed/getAppPrincipal"
/*农事管理地块列表*/
#define Area_nongye_List @"TraceProductionMassif/getProductionMassifList/1/1000"
/*栋舍（畜禽养殖）管理地块列表*/
#define Area_xuqin_List @"TraceProductionLivestockField/getProductionLivestockFieldList/page/1/1000"
/*渔业管理地块列表*/
#define Area_yuye_List @"TraceProductionTangkou/getProductionTangkouList/1/1000"





#endif /* APiMacros_h */
