//
//  YQDataManager.m
//  YiQuStaff
//
//  Created by 杨琴 on 2017/11/14.
//  Copyright © 2017年 yiquStaff. All rights reserved.
//

#import "YQDataManager.h"

#define USERNAME [[NSUserDefaults standardUserDefaults] objectForKey:@"loginName"]
#define PASSWORD  [[NSUserDefaults standardUserDefaults] objectForKey:@"passWord"]


#pragma mark - 数据model基类
@implementation YQListBaseModel
+ (NSDictionary *)modelCustomPropertyMapper
{
	return @{};
}
@end

@implementation YQDataBaseModel

+ (NSDictionary *)modelCustomPropertyMapper
{
	return @{};
}

@end

@implementation YQBaseModel
+ (NSDictionary *)modelCustomPropertyMapper
{
	return @{};
}

@end


#pragma mark - 网络请求基类
///请求类型类型
typedef NS_ENUM(NSUInteger, YQNetWorkType) {
	
	YQNetWorkTypeGet,//get请求
	
	YQNetWorkTypeHttpPost,//入参Http格式Post请求body
	
	YQNetWorkTypeJsonPost,//入参json格式Post请求
	YQNetWorkTypeJsonUP,
};

/**
 *  请求重试计数器
 */
@interface RetryCounter : NSObject

//重试次数，默认30次
@property (nonatomic, assign) NSInteger retryCount;
//已重试的次数
@property (nonatomic, assign) NSInteger retryTime;
//重试时间间隔，默认100毫秒
@property (nonatomic, assign) NSTimeInterval retryInterval;
//计算器id
@property (nonatomic, assign) NSInteger counterId;

- (instancetype)initWithHash:(NSUInteger)hash;
- (BOOL)canRetry;

@end

@implementation RetryCounter

- (instancetype)initWithHash:(NSUInteger)hash
{
	self = [super init];
	//set default value
	self.retryCount = 1;
	self.retryInterval = 0.1;
	self.counterId = hash;
	
	return self;
}

/**
 *  是否可以重试，如果是，则返回true，并且重试次数自增1.
 *
 *  @return 是否可以重试
 */
- (BOOL)canRetry {
	BOOL result = (_retryTime < _retryCount);
	
	if (result) {
		_retryTime++;
	}
	
	return result;
}
@end

@interface YQDataManager ()

@property (nonnull, nonatomic, strong) NSMutableArray *retryCounterList;

@end
@implementation YQDataManager

/**
 *  创建单例
 *
 *  @return return value description
 */
+ (YQDataManager *)manager {
	static YQDataManager *sharedManager = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
		configuration.timeoutIntervalForRequest = 8.0;
		configuration.timeoutIntervalForResource = 8.0;
		/* 创建网络请求对象 */
		sharedManager = [[YQDataManager alloc] initWithBaseURL:[NSURL URLWithString:@""] sessionConfiguration:configuration];
		/* 设置请求和接收的数据编码格式 */
		
		sharedManager.responseSerializer = [AFJSONResponseSerializer serializer]; // 设置接收数据为 JSON 数据
		//        [sharedManager.requestSerializer setValue:@"iot_91c8e4da23ee44b698778748426d2ee8" forHTTPHeaderField:@"appId"];
		sharedManager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
		// MARK: - allowInvalidCertificates 是否允许无效证书（也就是自建的证书），默认为NO
		// MARK: - 如果是需要验证自建证书，需要设置为YES
		sharedManager.securityPolicy.allowInvalidCertificates = YES;
		// MARK: - 假如证书的域名与你请求的域名不一致，需把该项设置为NO
		// MARK: - 主要用于这种情况：客户端请求的是子域名，而证书上的是另外一个域名。因为SSL证书上的域名是独立的，假如证书上注册的域名是www.google.com，那么mail.google.com是无法验证通过的；当然，有钱可以注册通配符的域名*.google.com，但这个还是比较贵的。
		sharedManager.securityPolicy.validatesDomainName = NO;
		// 修改 解析数据格式 能够接受的内容类型 － 官方推荐的做法，民间做法：直接修改 AFN 的源代码
		sharedManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/json",@"text/javascript", @"text/html",nil];
		
		sharedManager.retryCounterList = [NSMutableArray array];
	});
	return sharedManager;
}

#pragma mark - helper

- (RetryCounter*)retryCounterForHash:(NSUInteger)hash
{
	RetryCounter *target = nil;
	for (RetryCounter *counter in _retryCounterList) {
		if (counter.counterId == hash) {
			target = counter;
			break;
		}
	}
	
	return target;
}

- (RetryCounter*)insertCounterWithHash:(NSUInteger)hash
{
	RetryCounter *counter = [[RetryCounter alloc] initWithHash:hash];
	[[YQDataManager manager].retryCounterList addObject:counter];
	
	return counter;
}

- (void)removeCounterForHash:(NSUInteger)hash
{
	RetryCounter *counter = [self retryCounterForHash:hash];
	
	if (counter != nil) {
		[_retryCounterList removeObject:counter];
	}
}


#pragma mark - http process


/**
 *  重试的方法
 */
+ (void)networkMananger:(YQDataManager *)manager
				 method:(YQNetWorkType)httpMethod
		   retryWithUrl:(NSString*)url
				 params:(NSDictionary*)param
				success:(void(^)(id responseData))successBlock
				failure:(void(^)(NSError *error))failureBlock {
	
	RetryCounter *currentCounter = [[YQDataManager manager] retryCounterForHash:param.hash];
	
	if (currentCounter == nil) {
		//add to retry counter list
		currentCounter = [[YQDataManager manager] insertCounterWithHash:param.hash];
	}
	
	BOOL retryFlag = [currentCounter canRetry];
	//wait for 10ms
	NSDate *endDate = [NSDate dateWithTimeIntervalSinceNow:currentCounter.retryInterval];
	[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:endDate];
	
	if (httpMethod == YQNetWorkTypeGet) {
		
		AFJSONRequestSerializer *privateSerializer = [AFJSONRequestSerializer serializer];
		
//        [privateSerializer setValue:UserId forHTTPHeaderField:@"userId"];
//        [privateSerializer setValue:@"app" forHTTPHeaderField:@"type"];
		manager.requestSerializer = privateSerializer;
			   [privateSerializer setValue:[Utils stringBase64:[NSString stringWithFormat:@"%@@%@",USERNAME,PASSWORD]] forHTTPHeaderField:@"mobileToken"];
		
		[manager GET:url parameters:param headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
			successBlock(responseObject);
			[[YQDataManager manager] removeCounterForHash:param.hash];
		} failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
			if (retryFlag) {
				//retry
				[YQDataManager networkMananger:manager method:httpMethod retryWithUrl:url params:param success:successBlock failure:failureBlock];
			} else {
				failureBlock(error);
				//remove counter
				[[YQDataManager manager] removeCounterForHash:param.hash];
			}
		}];
		
//        [manager GET:url parameters:param progress:nil success:^(NSURLSessionDataTask *task, id responseData) {
//            successBlock(responseData);
//            //remove counter
//            [[YQDataManager manager] removeCounterForHash:param.hash];
//        } failure:^(NSURLSessionDataTask *task, NSError *error) {
//            if (retryFlag) {
//                //retry
//                [YQDataManager networkMananger:manager method:httpMethod retryWithUrl:url params:param success:successBlock failure:failureBlock];
//            } else {
//                failureBlock(error);
//                //remove counter
//                [[YQDataManager manager] removeCounterForHash:param.hash];
//            }
//
//        }];
	}else if (httpMethod == YQNetWorkTypeJsonPost){
		manager.requestSerializer = [AFJSONRequestSerializer serializer]; // 设置请求数据为 JSON 数据
		
		AFJSONRequestSerializer *privateSerializer = [AFJSONRequestSerializer serializer];
//        [privateSerializer setValue:UserId forHTTPHeaderField:@"userId"];
//        [privateSerializer setValue:@"app" forHTTPHeaderField:@"type"];
//        NSLog(@"UserId==%@",UserId);

			   [privateSerializer setValue:[Utils stringBase64:[NSString stringWithFormat:@"%@@%@",USERNAME,PASSWORD]] forHTTPHeaderField:@"mobileToken"];
		manager.requestSerializer = privateSerializer;
		
		[manager POST:url parameters:param headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
			successBlock(responseObject);
			//remove counter
			[[YQDataManager manager] removeCounterForHash:param.hash];
		} failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
			if (retryFlag) {
				//retry
				[YQDataManager networkMananger:manager method:httpMethod retryWithUrl:url params:param success:successBlock failure:failureBlock];
			} else {
				failureBlock(error);
				//remove counter
				[[YQDataManager manager] removeCounterForHash:param.hash];
			}
		}];
		
//        [manager POST:url parameters:param progress:nil success:^(NSURLSessionDataTask *task, id responseData) {
//            successBlock(responseData);
//            //remove counter
//            [[YQDataManager manager] removeCounterForHash:param.hash];
//        } failure:^(NSURLSessionDataTask *task, NSError *error) {
//            if (retryFlag) {
//                //retry
//                [YQDataManager networkMananger:manager method:httpMethod retryWithUrl:url params:param success:successBlock failure:failureBlock];
//            } else {
//                failureBlock(error);
//                //remove counter
//                [[YQDataManager manager] removeCounterForHash:param.hash];
//            }
//        }];
	}else{
		manager.requestSerializer = [AFHTTPRequestSerializer serializer];
		AFJSONRequestSerializer *privateSerializer = [AFJSONRequestSerializer serializer];
//        [privateSerializer setValue:UserId forHTTPHeaderField:@"userId"];
//        NSLog(@"UserId==%@",UserId);
//        [privateSerializer setValue:@"app" forHTTPHeaderField:@"type"];
			   [privateSerializer setValue:[Utils stringBase64:[NSString stringWithFormat:@"%@@%@",USERNAME,PASSWORD]] forHTTPHeaderField:@"mobileToken"];
		manager.requestSerializer = privateSerializer;
		
		[manager POST:url parameters:param headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
						successBlock(responseObject);
						//remove counter
						[[YQDataManager manager] removeCounterForHash:param.hash];
		} failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
						if (retryFlag) {
				   //retry
				   [YQDataManager networkMananger:manager method:httpMethod retryWithUrl:url params:param success:successBlock failure:failureBlock];
			   } else {
				   failureBlock(error);
				   //remove counter
				   [[YQDataManager manager] removeCounterForHash:param.hash];
			   }
		}];
		
//        [manager POST:url parameters:param progress:nil success:^(NSURLSessionDataTask *task, id responseData) {
//            successBlock(responseData);
//            //remove counter
//            [[YQDataManager manager] removeCounterForHash:param.hash];
//        } failure:^(NSURLSessionDataTask *task, NSError *error) {
//            if (retryFlag) {
//                //retry
//                [YQDataManager networkMananger:manager method:httpMethod retryWithUrl:url params:param success:successBlock failure:failureBlock];
//            } else {
//                failureBlock(error);
//                //remove counter
//                [[YQDataManager manager] removeCounterForHash:param.hash];
//            }
//        }];
	}
}

/**
 *  网络请求的底层方法
 */
+ (void)networkProcessWithUrl:(NSString *)url
					   params:(NSDictionary *)aParams
					   method:(YQNetWorkType )httpMethod
				   modelClass:(Class)modelClass
				responseBlock:(YQNetworkResult)respBlock
{
	YQDataManager *manager = [YQDataManager manager];
//    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:USERNAME password:PASSWORD];
	NSString *fullPath = [NSString stringWithFormat:@"%@%@",manager.baseURL,url];
	if (aParams) {
		NSError *error;
		NSData *jsonData = [NSJSONSerialization dataWithJSONObject:aParams options:0 error:&error];
		
		NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
		NSLog(@"url : %@, param ==== %@", fullPath, jsonString);
	}else{
		NSLog(@"url : %@, param ==== %@", fullPath, aParams);
	}
	
	
	if (httpMethod == YQNetWorkTypeGet) {
		
		manager.requestSerializer = [AFJSONRequestSerializer serializer]; // 设置请求数据为 JSON 数据
		AFJSONRequestSerializer *privateSerializer = [AFJSONRequestSerializer serializer];
//        [privateSerializer setValue:UserId forHTTPHeaderField:@"userId"];
//        [privateSerializer setValue:@"app" forHTTPHeaderField:@"type"];

		[privateSerializer setValue:[Utils stringBase64:[NSString stringWithFormat:@"%@@%@",USERNAME,PASSWORD]] forHTTPHeaderField:@"mobileToken"];


		manager.requestSerializer = privateSerializer;
		
		[manager GET:fullPath parameters:aParams headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
			[YQDataManager successProcessWithModelClass:modelClass result:responseObject block:respBlock];

		} failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
			[YQDataManager networkMananger:manager method:httpMethod  retryWithUrl:url params:aParams success:^(id respData) {
				[YQDataManager successProcessWithModelClass:modelClass result:respData block:respBlock];
			} failure:^(NSError *error) {
				[YQDataManager failureProcess:error block:respBlock];
			}];
		}];
		
//        [manager GET:fullPath parameters:aParams progress:nil success:^(NSURLSessionDataTask *task, id responseData) {
//
//            [YQDataManager successProcessWithModelClass:modelClass result:responseData block:respBlock];
//
//        } failure:^(NSURLSessionDataTask *task, NSError *error) {
//            [YQDataManager networkMananger:manager method:httpMethod  retryWithUrl:url params:aParams success:^(id respData) {
//                [YQDataManager successProcessWithModelClass:modelClass result:respData block:respBlock];
//            } failure:^(NSError *error) {
//                [YQDataManager failureProcess:error block:respBlock];
//            }];
//        }];
		
	}
	else if (httpMethod == YQNetWorkTypeJsonPost) {
		
		
		manager.requestSerializer = [AFJSONRequestSerializer serializer]; // 设置请求数据为 JSON 数据
		AFJSONRequestSerializer *privateSerializer = [AFJSONRequestSerializer serializer];
//        [privateSerializer setValue:UserId forHTTPHeaderField:@"userId"];
//        [privateSerializer setValue:@"app" forHTTPHeaderField:@"type"];
//        NSLog(@"UserId==%@",UserId);
		[privateSerializer setValue:[Utils stringBase64:[NSString stringWithFormat:@"%@@%@",USERNAME,PASSWORD]] forHTTPHeaderField:@"mobileToken"];
		manager.requestSerializer = privateSerializer;
		
		[manager POST:fullPath parameters:aParams headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
			[YQDataManager successProcessWithModelClass:modelClass result:responseObject block:respBlock];

		} failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
			
			[YQDataManager networkMananger:manager method:httpMethod  retryWithUrl:url params:aParams success:^(id respData) {
				[YQDataManager successProcessWithModelClass:modelClass result:respData block:respBlock];
			} failure:^(NSError *error) {
				[YQDataManager failureProcess:error block:respBlock];
			}];
		}];
		
//        [manager POST:fullPath parameters:aParams progress:nil success:^(NSURLSessionDataTask *task, id responseData) {
//
//            [YQDataManager successProcessWithModelClass:modelClass result:responseData block:respBlock];
//
//        } failure:^(NSURLSessionDataTask *task, NSError *error) {
//
//            [YQDataManager networkMananger:manager method:httpMethod  retryWithUrl:url params:aParams success:^(id respData) {
//                [YQDataManager successProcessWithModelClass:modelClass result:respData block:respBlock];
//            } failure:^(NSError *error) {
//                [YQDataManager failureProcess:error block:respBlock];
//            }];
//        }];
	}else{
		
		manager.requestSerializer = [AFHTTPRequestSerializer serializer];
		AFJSONRequestSerializer *privateSerializer = [AFJSONRequestSerializer serializer];
//        [privateSerializer setValue:UserId forHTTPHeaderField:@"userId"];
//        [privateSerializer setValue:@"app" forHTTPHeaderField:@"type"];
//        NSLog(@"UserId==%@",UserId);
		[privateSerializer setValue:[Utils stringBase64:[NSString stringWithFormat:@"%@@%@",USERNAME,PASSWORD]] forHTTPHeaderField:@"mobileToken"];

		manager.requestSerializer = privateSerializer;
		
		[manager POST:fullPath parameters:aParams headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
			[YQDataManager successProcessWithModelClass:modelClass result:responseObject block:respBlock];

		} failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
			
			[YQDataManager networkMananger:manager method:httpMethod  retryWithUrl:url params:aParams success:^(id respData) {
				[YQDataManager successProcessWithModelClass:modelClass result:respData block:respBlock];
			} failure:^(NSError *error) {
				[YQDataManager failureProcess:error block:respBlock];
			}];
			
		}];
		
//        [manager POST:fullPath parameters:aParams progress:nil success:^(NSURLSessionDataTask *task, id responseData) {
//
//            [YQDataManager successProcessWithModelClass:modelClass result:responseData block:respBlock];
//
//        } failure:^(NSURLSessionDataTask *task, NSError *error) {
//
//            [YQDataManager networkMananger:manager method:httpMethod  retryWithUrl:url params:aParams success:^(id respData) {
//                [YQDataManager successProcessWithModelClass:modelClass result:respData block:respBlock];
//            } failure:^(NSError *error) {
//                [YQDataManager failureProcess:error block:respBlock];
//            }];
//
//        }];
	}
}

/**
 *  接口请求成功返回
 */
+ (void)successProcessWithModelClass:(Class)modelClass
							  result:(id)responseData
							   block:(YQNetworkResult)block
{
	NSData *jsonData = [NSJSONSerialization dataWithJSONObject:responseData options:NSJSONWritingPrettyPrinted error:nil];
	NSString *jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
	NSLog(@"jsonStr====%@",jsonStr);
	if (!block) {
		
		[SVProgressHUD showErrorWithStatus:@"未知错误"];
		[SVProgressHUD dismissWithDelay:0.5];
		
		return;
	}
	//model
	if (modelClass == nil) {
		if (block) {
			block(YES, nil, responseData);
		}
	} else {
		
		YQBaseModel *item = [YQBaseModel mj_objectWithKeyValues:responseData];
		if ([item.status isEqualToString:@"SUCCESS"]) {//返回成功状态
			if ([item.data isKindOfClass:[NSDictionary class]]) {

				id model = [modelClass mj_objectWithKeyValues:item.data];

				if (block) {
					block(YES, item, model);
				}
			}else{
				if (item.data) {
					NSDictionary *josnDic = @{@"data":item.data };
				
				
					id model = [modelClass mj_objectWithKeyValues:josnDic];
					if (block) {
						block(YES, item, model);
					}
				}else{
					if (block) {
						block(YES, item, nil);
					}
				}
			}
		}else{//返回失败状态
			if (block) {
				if ([Utils valString: item.message ]) {
					item.message=@"网络错误";
					
				}
				NSError *error = [YQDataManager errorWithDescription:item.message cede:item.code];
				block(NO, item, error);
			}
		}
	}
}
/**
 *  接口请求失败无返回
 */
+ (void)failureProcess:(NSError *)error block:(YQNetworkResult)block
{
	NSLog(@"error: %@,userInfo = %@",error, [error userInfo]);
	
	if (block) {
		block(NO, nil, error);
	}
}

/**
 *  get方式请求调用方法
 */
+ (void)getDataWithUrl:(NSString *)url
				Params:(NSDictionary *)params
			modelClass:(Class)modelClass
		 responseBlock:(YQNetworkResult)respBlock
{
	[YQDataManager networkProcessWithUrl:url params:params method:YQNetWorkTypeGet modelClass:modelClass responseBlock:respBlock];
}

/**
 *  post 请求调用方式 入参Json请求数据
 */
+ (void)postDataJsonRequestWithUrl:(NSString *)url
							Params:(NSDictionary *)params
						modelClass:(Class)modelClass
					 responseBlock:(YQNetworkResult)respBlock
{
	[YQDataManager networkProcessWithUrl:url params:params method:YQNetWorkTypeJsonPost modelClass:modelClass responseBlock:respBlock];
}

/**
 *  post 请求调用方法 入参Http请求数据
 */
+ (void)postDataHttpRequestWithUrl:(NSString *)url
							Params:(NSDictionary *)params
						modelClass:(Class)modelClass
					 responseBlock:(YQNetworkResult)respBlock
{
	[YQDataManager networkProcessWithUrl:url params:params method:YQNetWorkTypeHttpPost modelClass:modelClass responseBlock:respBlock];
}
/**
 *postUpload 上传文件方法
 */
/**
 *  网络请求的底层方法
 */
+ (void)postUpdataHttpRequestWithUrl:(NSString *)url
					   params:(NSDictionary *)aParams
					   images:(NSArray<UIImage *> *)images
						 name:(NSString *)name
					 mimeType:(NSString *)mimeType
					 fileName:(NSString *)fileName
				   modelClass:(Class)modelClass
			responseBlock:(YQNetworkResult)respBlock
{
	YQDataManager *manager = [YQDataManager manager];
//    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:USERNAME password:PASSWORD];
	NSString *fullPath = [NSString stringWithFormat:@"%@%@",manager.baseURL,url];
	if (aParams) {
		NSError *error;
		NSData *jsonData = [NSJSONSerialization dataWithJSONObject:aParams options:0 error:&error];
		
		NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
		NSLog(@"url : %@, param ==== %@", fullPath, jsonString);
	}else{
		NSLog(@"url : %@, param ==== %@", fullPath, aParams);
	}
  
		
		manager.requestSerializer = [AFHTTPRequestSerializer serializer];
		AFJSONRequestSerializer *privateSerializer = [AFJSONRequestSerializer serializer];
//        [privateSerializer setValue:UserId forHTTPHeaderField:@"userId"];
//        [privateSerializer setValue:@"app" forHTTPHeaderField:@"type"];
		
		manager.requestSerializer = privateSerializer;
	[privateSerializer setValue:[Utils stringBase64:[NSString stringWithFormat:@"%@@%@",USERNAME,PASSWORD]] forHTTPHeaderField:@"mobileToken"];
	
	[manager POST:fullPath parameters:aParams headers:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
				//压缩-添加-上传图片
				[images enumerateObjectsUsingBlock:^(UIImage * _Nonnull image, NSUInteger idx, BOOL * _Nonnull stop) {
					NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
					[formData appendPartWithFileData:imageData name:name fileName:fileName mimeType:[NSString stringWithFormat:@"image/%@",mimeType?mimeType:@"jpeg"]];
				}];
	} progress:^(NSProgress * _Nonnull uploadProgress) {
		NSLog(@"%lld",uploadProgress.totalUnitCount);
	} success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
		[YQDataManager successProcessWithModelClass:modelClass result:responseObject block:respBlock];
	} failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
		[YQDataManager failureProcess:error block:respBlock];
	}];
	
//    [manager POST:fullPath parameters:aParams constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
//
//        //压缩-添加-上传图片
//        [images enumerateObjectsUsingBlock:^(UIImage * _Nonnull image, NSUInteger idx, BOOL * _Nonnull stop) {
//            NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
//            [formData appendPartWithFileData:imageData name:name fileName:fileName mimeType:[NSString stringWithFormat:@"image/%@",mimeType?mimeType:@"jpeg"]];
//        }];
//    } progress:^(NSProgress * _Nonnull uploadProgress) {
//
//
//    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//
//        [YQDataManager successProcessWithModelClass:modelClass result:responseObject block:respBlock];
//
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//
//        [YQDataManager failureProcess:error block:respBlock];
//    }];
	
	
}

#pragma mark - Utils

/**
 *  请求成功返回失败状态时创建错误返回
 */
+ (NSError*)errorWithDescription:(NSString *)errorMsg cede:(NSInteger)code
{
	NSString *msg = errorMsg;
	if (msg == nil) {
		msg = @"未知错误";
		code = 22222;
	}
	
	NSDictionary *userInfo = @{NSLocalizedDescriptionKey: msg};
	NSError *error = [NSError errorWithDomain:@"33OA" code:code userInfo:userInfo];
	
	return error;
}
/**
 *  清除本地个人数据
 */
+(void)logOutToClearLocalData{
	//    [UserConfig shareConfig].user=nil;
	[[NSUserDefaults standardUserDefaults] removeObjectForKey:@"login"];
}





@end
