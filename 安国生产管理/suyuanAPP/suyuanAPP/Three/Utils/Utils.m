//
//  Utils.m
//  TestDemoT
//
//  Created by 杨琴 on 2017/7/29.
//  Copyright © 2017年 Yq. All rights reserved.
//1. In file included from /Users/yangqin/Documents/APP/CompanyAPP_Obj/CompanyAPP_Obj/Three/Utils/Utils.m:9:

#import "Utils.h"
#import <Foundation/Foundation.h>
@implementation Utils

+ (NSString*) getUUIDString
{
    
    CFUUIDRef uuidObj = CFUUIDCreate(nil);
    
    NSString *uuidString = (__bridge_transfer NSString*)CFUUIDCreateString(nil, uuidObj);
    
    CFRelease(uuidObj);
    
    return uuidString;
    
}
#pragma mark 判断字符串是否为空，null，nil等...(YES：为空)
+ (BOOL)valString:(NSString *)string {
    NSString *toString = [NSString stringWithFormat:@"%@",string];
    if (toString == nil || toString == NULL) {
        return YES;
    }
    
    if ([toString isKindOfClass:[NSNull class]]) {
        return YES;
    }
    
    if ([toString isEqualToString:@""]) {
        return YES;
    }
    
    if ([toString isEqualToString:@"(null)"]) {
        return YES;
    }
    
    if ([toString isEqualToString:@"<null>"]) {
        return YES;
    }
    
    if ([toString stringByReplacingOccurrencesOfString:@" " withString:@""].length<=0) {
        return YES;
    }
    
    return NO;
}

+(NSString *)toString:(NSObject *) str{
    if ( [str isKindOfClass:[NSString class]] && [((NSString *) str) isEqualToString:@""] ) {
        return @"";
    }
    return [[NSString alloc] initWithFormat:@"%@" ,str ];
}

#pragma mark 判断字符串是否是数字 是返回本身不是返回0
+ (NSString *)stringTurnNumber:(NSString *)string {
    
    NSString *toString = [[NSString stringWithFormat:@"%@",string] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([self isNumText:toString]) {
        return toString;
    }else{
        return @"0";
    }
}

//判断空对象

+(BOOL) isNullObject:(id)object

{
    
    if (object == nil || [object isEqual:[NSNull class]]) {
        
        return YES;
        
    }else if ([object isKindOfClass:[NSNull class]])
        
    {
        
        if ([object isEqualToString:@""]) {
            
            return YES;
            
        }else
            
        {
            
            return NO;
            
        }
        
    }else if ([object isKindOfClass:[NSNumber class]])
        
    {
        
        if ([object isEqualToNumber:@0]) {
            
            return YES;
            
        }else
            
        {
            
            return NO;
            
        }
        
    }
    
    return NO;
    
}
#pragma mark 判断字符串是否为空，空返回@""不为空返回本身
+ (NSString *)stringTurnString:(NSString *)string {
    if ([self valString:string]) {
        return @"";
    }else{
        return [NSString stringWithFormat:@"%@",string];
    }
}
#pragma mark 是否是纯数字
+ (BOOL)isNumText:(NSString *)str {
    str = [NSString stringWithFormat:@"%@",str];
    NSScanner* scan = [NSScanner scannerWithString:str];
    double val;
    return[scan scanDouble:&val] && [scan isAtEnd];
}

// 将JSON串转化为字典或者数组
+(NSMutableArray *)toArrayOrNSDictionary:(NSString *)jsonString{
    
    NSData *jsonData= [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData
                                                    options:NSJSONReadingAllowFragments
                                                      error:&error];
    
    if (jsonObject != nil && error == nil){
        
        NSLog(@"jsonObject%@",jsonObject);
        
        return jsonObject;
    }else{
        // 解析错误
        return nil;
    }
    
}

//字典转json格式字符串：
+ (NSString*)dictionaryToJson:(NSDictionary *)dic
{

    
    NSString *p1Str = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:dic options:0 error:nil] encoding:NSUTF8StringEncoding];

    NSMutableString *mutStr = [NSMutableString stringWithString:p1Str];
    
    NSRange range = {0,p1Str.length};
    
    //去掉字符串中的空格
    
    [mutStr replaceOccurrencesOfString:@" " withString:@"" options:NSLiteralSearch range:range];
    
    NSRange range2 = {0,mutStr.length};
    
    //去掉字符串中的换行符
    
    [mutStr replaceOccurrencesOfString:@"\n" withString:@"" options:NSLiteralSearch range:range2];
    
    return mutStr;
}


+ (NSString *)arrayToJSONString:(NSArray *)array

 {
    NSError *error = nil;
//    NSMutableArray *muArray = [NSMutableArray array];
//    for (NSString *userId in array) {
//        [muArray addObject:[NSString stringWithFormat:@"\"%@\"", userId]];
//    }
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//    NSString *jsonTemp = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
//    NSString *jsonResult = [jsonTemp stringByReplacingOccurrencesOfString:@" " withString:@""];
//    NSLog(@"json array is: %@", jsonResult);
    return jsonString;
}

+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString
{
    if ([Utils valString:jsonString] ) {
        
        return nil;
    }
    
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err)
    {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}

+ (NSDictionary *)dictionaryWithContentsOfData:(NSData *)data {
   

//    return [NSJSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
    
    return nil;
}


//提取字符中的数字

+ (NSString *)getNumberFromStr:(NSString *)str{
    
    
    
    NSCharacterSet *nonDigitCharacterSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
      
    return[[str componentsSeparatedByCharactersInSet:nonDigitCharacterSet] componentsJoinedByString:@""];
    

    
}


+ (BOOL) IsIdentityCard:(NSString *)IDCardNumber
{
    if (IDCardNumber.length <= 0) {
        return NO;
    }
    NSString *regex2 = @"^(\\d{14}|\\d{17})(\\d|[xX])$";
    NSPredicate *identityCardPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex2];
    return [identityCardPredicate evaluateWithObject:IDCardNumber];
}

+ (BOOL) IsPhoneNumber:(NSString *)number
{
    NSString *phoneRegex1=@"1[34578]([0-9]){9}";
    
    NSPredicate *phoneTest1 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex1];
    
    return  [phoneTest1 evaluateWithObject:number];
}
+ (BOOL) IsBankCard:(NSString *)cardNumber
{
    if(cardNumber.length==0)
    {
        return NO;
    }
    NSString *digitsOnly = @"";
    char c;
    for (int i = 0; i < cardNumber.length; i++)
    {
        c = [cardNumber characterAtIndex:i];
        if (isdigit(c))
        {
            digitsOnly =[digitsOnly stringByAppendingFormat:@"%c",c];
        }
    }
    int sum = 0;
    int digit = 0;
    int addend = 0;
    BOOL timesTwo = false;
    for (NSInteger i = digitsOnly.length - 1; i >= 0; i--)
    {
        digit = [digitsOnly characterAtIndex:i] - '0';
        if (timesTwo)
        {
            addend = digit * 2;
            if (addend > 9) {
                addend -= 9;
            }
        }
        else {
            addend = digit;
        }
        sum += addend;
        timesTwo = !timesTwo;
    }
    int modulus = sum % 10;
    return modulus == 0;
}

- (NSString *)turnUppercaseLowercaseWithString:(NSString *)vioceString {
    NSString *string = vioceString;
    string = [string stringByReplacingOccurrencesOfString:@"一" withString:@"1"];
    string = [string stringByReplacingOccurrencesOfString:@"二" withString:@"2"];
    string = [string stringByReplacingOccurrencesOfString:@"三" withString:@"3"];
    string = [string stringByReplacingOccurrencesOfString:@"四" withString:@"4"];
    string = [string stringByReplacingOccurrencesOfString:@"五" withString:@"5"];
    string = [string stringByReplacingOccurrencesOfString:@"六" withString:@"6"];
    string = [string stringByReplacingOccurrencesOfString:@"七" withString:@"7"];
    string = [string stringByReplacingOccurrencesOfString:@"八" withString:@"8"];
    string = [string stringByReplacingOccurrencesOfString:@"九" withString:@"9"];
    string = [string stringByReplacingOccurrencesOfString:@"零" withString:@"0"];
    return string;
}


+ (UIColor *) colorFromHexCode:(NSString *)hexString {
    NSString *cleanString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    if([cleanString length] == 3) {
        cleanString = [NSString stringWithFormat:@"%@%@%@%@%@%@",
                       [cleanString substringWithRange:NSMakeRange(0, 1)],[cleanString substringWithRange:NSMakeRange(0, 1)],
                       [cleanString substringWithRange:NSMakeRange(1, 1)],[cleanString substringWithRange:NSMakeRange(1, 1)],
                       [cleanString substringWithRange:NSMakeRange(2, 1)],[cleanString substringWithRange:NSMakeRange(2, 1)]];
    }
    if([cleanString length] == 6) {
        cleanString = [cleanString stringByAppendingString:@"ff"];
    }
    
    unsigned int baseValue;
    [[NSScanner scannerWithString:cleanString] scanHexInt:&baseValue];
    
    float red = ((baseValue >> 24) & 0xFF)/255.0f;
    float green = ((baseValue >> 16) & 0xFF)/255.0f;
    float blue = ((baseValue >> 8) & 0xFF)/255.0f;
    float alpha = ((baseValue >> 0) & 0xFF)/255.0f;
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}


// 开启倒计时效果
+(void)openCountdown:(UIButton *)authCodeBtn{
    
    __block NSInteger time = 59; //倒计时时间
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    
    dispatch_source_set_event_handler(_timer, ^{
        
        if(time <= 0){ //倒计时结束，关闭
            
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                
                //设置按钮的样式
                [authCodeBtn setTitle:@"重新发送" forState:UIControlStateNormal];
                [authCodeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                authCodeBtn.userInteractionEnabled = YES;
            });
            
        }else{
            
            int seconds = time % 60;
            dispatch_async(dispatch_get_main_queue(), ^{
                
                //设置按钮显示读秒效果
                [authCodeBtn setTitle:[NSString stringWithFormat:@"重新发送(%.2d)", seconds] forState:UIControlStateNormal];
                authCodeBtn.titleLabel.font=[UIFont systemFontOfSize:12];
                [authCodeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                authCodeBtn.userInteractionEnabled = NO;
            });
            time--;
        }
    });
    dispatch_resume(_timer);
}


//字典
+(NSObject *)objectRemoveEmptyValue:(NSObject *)obj{
    
    
    if ([obj isKindOfClass:[NSArray class]]) {
        
        NSMutableArray *array=[[NSMutableArray alloc]initWithArray:(NSArray *)obj];
        [self removeEmptyValue:array];
        return array;
        
    }else if ([obj isKindOfClass:[NSDictionary class]]){
        
        NSMutableDictionary *dictionary=[[NSMutableDictionary alloc]initWithDictionary:(NSDictionary *)obj];
        [self removeEmptyValue:dictionary];
        return dictionary;
        
    }
    return obj;
    
}
#pragma mark - 获取当前时间的 时间戳

+(NSInteger)getNowTimestamp{
    
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"]; // ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    
    //设置时区,这个对于时间的处理有时很重要
    
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];
    
    [formatter setTimeZone:timeZone];
    
    NSDate *datenow = [NSDate date];//现在时间
    
    
    
    NSLog(@"设备当前的时间:%@",[formatter stringFromDate:datenow]);
    
    //时间转时间戳的方法:
    
    
    
    NSInteger timeSp = [[NSNumber numberWithDouble:[datenow timeIntervalSince1970]] integerValue];
    
    
    
    NSLog(@"设备当前的时间戳:%ld",(long)timeSp); //时间戳的值
    
    
    
    return timeSp;
    
}



//将某个时间转化成 时间戳

#pragma mark - 将某个时间转化成 时间戳

+(NSInteger)timeSwitchTimestamp:(NSString *)formatTime andFormatter:(NSString *)format{
    
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    [formatter setDateFormat:format]; //(@"YYYY-MM-dd hh:mm:ss") ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    
    
    
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];
    
    [formatter setTimeZone:timeZone];
    
    
    
    NSDate* date = [formatter dateFromString:formatTime]; //------------将字符串按formatter转成nsdate
    
    //时间转时间戳的方法:
    
    NSInteger timeSp = [[NSNumber numberWithDouble:[date timeIntervalSince1970]] integerValue];
    
    
    
    NSLog(@"将某个时间转化成 时间戳&&&&&&&timeSp:%ld",(long)timeSp); //时间戳的值
    
    
    
    return timeSp;
    
}



//将某个时间戳转化成 时间

#pragma mark - 将某个时间戳转化成 时间

+(NSString *)timestampSwitchTime:(NSString *)timestamp andFormatter:(NSString *)format{
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    [formatter setDateFormat:format]; // （@"YYYY-MM-dd hh:mm:ss"）----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];
    
    [formatter setTimeZone:timeZone];
    NSDate *confromTimesp;
    
    if (timestamp.length== 13) {
       confromTimesp = [NSDate dateWithTimeIntervalSince1970:[timestamp integerValue]/1000];

    }else{
        confromTimesp = [NSDate dateWithTimeIntervalSince1970:[timestamp integerValue]];
    }
    NSLog(@"1296035591   = %@",confromTimesp);
    
    
    
    NSString *confromTimespStr = [formatter stringFromDate:confromTimesp];
    
    
    
    //NSLog(@"&&&&&&&confromTimespStr = : %@",confromTimespStr);
    
    
    
    return confromTimespStr;
    
}



+(void)removeEmptyValue:(NSObject *)object{
    if ([object isKindOfClass:[NSArray class]]) {
        
        NSMutableArray *array=(NSMutableArray *)object;
        [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj isKindOfClass:[NSNull class]]|| obj==nil) {
                [array removeObject:obj];
                
                
            }else if ([obj isKindOfClass:[NSArray class]]){
                
                NSMutableArray *subArray=[[NSMutableArray alloc]initWithArray:obj];
                [array replaceObjectAtIndex:idx withObject:subArray];
                [self removeEmptyValue:subArray];
                
            }else if ([obj isKindOfClass: [NSDictionary class]]){
                NSMutableDictionary *subDictionary=[[NSMutableDictionary alloc]initWithDictionary:obj];
                [array replaceObjectAtIndex:idx withObject:subDictionary];
                [self removeEmptyValue:subDictionary];
                
            }
        }];
        
        
    }else if ([object isKindOfClass:[NSDictionary class]]){
        
        NSMutableDictionary *dictionary=(NSMutableDictionary *)object;
        [dictionary enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            if ([obj isKindOfClass:[NSNull class]] || obj==nil) {
                
                [dictionary removeObjectForKey:key];
                
            }else if ([obj isKindOfClass:[NSArray class]]){
                NSMutableArray *subArray=[[NSMutableArray alloc]initWithArray:obj];
                [dictionary setObject:subArray forKey:key];
                [self removeEmptyValue:subArray];
                
                
            }else if ( [obj isKindOfClass:[NSArray class]]){
                NSMutableDictionary *subDictionary=[[NSMutableDictionary alloc]initWithDictionary:obj];
                [dictionary setObject:subDictionary forKey:key];
                [self removeEmptyValue:subDictionary];
                
                
            }
        }];
        
    }
    
}


+(NSString *)dictionaryToString:(NSDictionary *)dict {
    
    
    if ([dict count]==0 ) {
        
        return@"";
        
    }
    NSArray *allKeyArray = [dict allKeys];
    NSArray *afterSortKeyArray = [allKeyArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        NSComparisonResult resuest = [obj1 compare:obj2];
        return resuest;
    }];
    //通过排列的key值获取value
    NSMutableArray *valueArray = [NSMutableArray array];
    for (NSString *sortsing in afterSortKeyArray) {
        NSString *valueString = [dict objectForKey:sortsing];
        [valueArray addObject:valueString];
    }
    //    NSLog(@"valueArray:%@",valueArray);
    NSMutableArray *signArray = [NSMutableArray array];
    for (int i = 0 ; i < afterSortKeyArray.count; i++) {
        NSString *keyValue = [NSString stringWithFormat:@"%@=%@",afterSortKeyArray[i],[Utils stringTurnString:valueArray[i]]];
        [signArray addObject:keyValue];
    }
    NSString *signString = [signArray componentsJoinedByString:@"&"];
    return signString;
}


+ (NSString *)setupRequestMonth
{
    NSDate *currentDate = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM"];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *lastMonthComps = [[NSDateComponents alloc] init];
    //    [lastMonthComps setYear:1]; // year = 1表示1年后的时间 year = -1为1年前的日期，month day 类推
    NSDate *newdate = [calendar dateByAddingComponents:lastMonthComps toDate:currentDate options:0];
    NSString *dateStr = [formatter stringFromDate:newdate];
    NSLog(@"date str = %@", dateStr);
    return dateStr;
    
}

+(NSString *)stringWithContentsOfModelArray:(NSArray *)ary{
    NSData *data = [NSJSONSerialization dataWithJSONObject:ary
                                                   options:NSJSONReadingMutableLeaves | NSJSONReadingAllowFragments
                                                     error:nil];
    
    if (data == nil) {
        return nil;
    }
    
    NSString *string = [[NSString alloc] initWithData:data
                                             encoding:NSUTF8StringEncoding];
    
    return string;
}
//字符串拆分为数组
+(NSArray *)stringAppcomdAry:(NSString *)images{

    if ([[self class]valString:images]) {
        
        return [NSArray array];
        
    }
    NSArray *aryImg=[images componentsSeparatedByString:@","];
    
    return aryImg;
    
    
}



+(NSArray *)newTwomonth:(NSInteger)Index{
    NSMutableArray *aryAdd=[NSMutableArray array];
    //得到当前的时间
    NSDate * mydate = [NSDate date];
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM"];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *comps = nil;
    
    comps = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitMonth fromDate:mydate];
    
    NSDateComponents *adcomps = [[NSDateComponents alloc] init];
    
    [adcomps setYear:0];
    
    [adcomps setMonth:-2];
    
    [adcomps setDay:0];
    for (int i=0; i<Index; i++) {
        
        [adcomps setYear:0];
        
        [adcomps setMonth:-i];
        
        [adcomps setDay:0];
        NSDate *newdate = [calendar dateByAddingComponents:adcomps toDate:mydate options:0];
        NSString *beforDate = [dateFormatter stringFromDate:newdate];
        
        NSLog(@"%@",beforDate);
        
        [aryAdd addObject:beforDate];
    }
    return [aryAdd copy];
    
}
//
//+(void)showReasonWithItem:(YQBaseModel *)item Error:(NSError *)error{
//    if (item) {
//        [SVProgressHUD showErrorWithStatus:item.message];
//    }else{
//        if ([[error userInfo] objectForKey:NSLocalizedDescriptionKey]) {
//            [SVProgressHUD showErrorWithStatus:[[error userInfo] objectForKey:NSLocalizedDescriptionKey]];
//        }else{
//            [SVProgressHUD showErrorWithStatus:[[error userInfo] objectForKey:NSDebugDescriptionErrorKey]];
//        }
//    }
//}


//汉子转拼音首字母

+(NSString *)firstCharactor:(NSString *)aString

{
    
    
    //转成了可变字符串
    
    NSMutableString *str = [NSMutableString stringWithString:aString];
    
    //先转换为带声调的拼音
    
    CFStringTransform((CFMutableStringRef)str,NULL, kCFStringTransformMandarinLatin,NO);
    
    //再转换为不带声调的拼音
    
    CFStringTransform((CFMutableStringRef)str,NULL, kCFStringTransformStripDiacritics,NO);
    
    //转化为大写拼音
    
    NSString *pinYin = [str capitalizedString];
    
    //获取并返回首字母
    
    return [pinYin substringToIndex:1];
    
}

/**yymodel 自己转数组**/
//+ (NSArray *)jsonsToModelsWithJsons:(NSArray *)jsons {
//    NSMutableArray *models = [NSMutableArray array];
//    for (NSDictionary *json in jsons) {
//        id model = [[self class] yy_modelWithJSON:json];
//        if (model) {
//            [models addObject:model];
//        }
//    }
//    return models;
//}
//获取当前window
 + (UIWindow *)mainWindow
 {
         UIApplication *app = [UIApplication sharedApplication];
         if ([app.delegate respondsToSelector:@selector(window)])
            {
                   return [app.delegate window];
                 }
         else
           {
                  return [app keyWindow];
                }
    
 }

+ (UIImage *) scaleFromImage: (UIImage *) image toSize: (CGSize) targetSize

{
    CGFloat width = image.size.width;
    CGFloat height = image.size.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    if (CGSizeEqualToSize(image.size, targetSize) == NO)
    {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        if (widthFactor > heightFactor){
            scaleFactor = widthFactor; // scale to fit height
            
        }else{
            scaleFactor = heightFactor; // scale to fit width
            
        }
        scaledWidth= width * scaleFactor;
        scaledHeight = height * scaleFactor;
        // center the image
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
            
        }
        else if (widthFactor < heightFactor)
        {
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
            
        }
        
    }

    UIGraphicsBeginImageContext(targetSize); // this will crop
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width= scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    [image drawInRect:thumbnailRect];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;

}

+(UIImage *) imageCompressForWidth:(UIImage *)sourceImage targetWidth:(CGFloat)defineWidth{
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = defineWidth;
    CGFloat targetHeight = height / (width / targetWidth);
    CGSize size = CGSizeMake(targetWidth, targetHeight);
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0, 0.0);
    if(CGSizeEqualToSize(imageSize, size) == NO){
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        if(widthFactor > heightFactor){
            scaleFactor = widthFactor;
        }
        else{
            scaleFactor = heightFactor;
        }
        scaledWidth = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        if(widthFactor > heightFactor){
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }else if(widthFactor < heightFactor){
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }
    UIGraphicsBeginImageContext(size);
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil){
        NSLog(@"scale image fail");
    }
    
    UIGraphicsEndImageContext();
    return newImage;
}


/**base64加密*/
+(NSString *)stringBase64:(NSString *)string64{
    
    NSData *basicAuthCredentials = [string64 dataUsingEncoding:NSUTF8StringEncoding];

    NSString *base64AuthCredentials = [basicAuthCredentials base64EncodedStringWithOptions:(NSDataBase64EncodingOptions)0];

    return base64AuthCredentials;
    
}


//
+ (void)saveUserInfotoLocalWithDic:(NSDictionary *)dic {
    NSString *documentPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *dicPath = [documentPath stringByAppendingPathComponent:@"Industry.plist"];
    NSFileManager* fm = [NSFileManager defaultManager];
    if (![fm fileExistsAtPath:dicPath]) {
       BOOL rrr = [fm createFileAtPath:dicPath contents:nil attributes:nil];
        if (rrr) {
            NSLog(@"创建文件成功");
        } else {
            NSLog(@"创建文件失败");
        }
    }
    NSLog(@"%@",dicPath);
    
    // 序列化，把字典存入plist文件
    BOOL ret = [dic writeToFile:dicPath atomically:YES];
    if (!ret) {
        NSLog(@"写入本地失败");
    }
}

+ (NSDictionary *)getUserInfofromLocal:(NSString *)namePlist{
    NSString *documentPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *dicPath = [documentPath stringByAppendingPathComponent:namePlist];
    // 反序列化，把plist文件数据读取出来，转为字典
    NSDictionary *userInfoDic = [NSDictionary dictionaryWithContentsOfFile:dicPath];
    return userInfoDic;
}





/*
农事类别：100100
播种：100101;
灌溉：100102;
施肥：100103;
用药：100104;
除草：100105;
插秧：100106
翻地：100107;
其他：100108)"
*/
+(NSString *)code:(NSInteger)code{
    
    NSString *typeCode;
    
    switch (code) {
        case 100101:
            typeCode=@"播种";
            break;
        case 100102:
            typeCode=@"灌溉";
            break;
        case 100103:
            typeCode=@"施肥";
            break;
        case 100104:
            typeCode=@"用药";
            break;
        case 100105:
            typeCode=@"除草";
            break;
        case 100106:
            typeCode=@"插秧";
            break;
        case 100107:
            typeCode=@"翻地";
            break;
        case 100108:
            typeCode=@"其他";
            break;
        case 1020101:
            typeCode=@"投喂";
            break;
        case 1020102:
            typeCode=@"清洁";
            break;
        case 1020103:
            typeCode=@"防疫";
            break;
        case 1020104:
            typeCode=@"消毒";
            break;
        case 1020105:
            typeCode=@"投药";
            break;
        case 1020106:
            typeCode=@"投幼崽";
            break;
        case 1020107:
            typeCode=@"其他";
            break;
        case 300101:
            typeCode=@"投喂";
            break;
        case 300102:
            typeCode=@"增氧";
            break;
        case 300103:
            typeCode=@"加水";
            break;
        case 300104:
            typeCode=@"清淤";
            break;
        case 300105:
            typeCode=@"疫苗";
            break;
        case 300106:
            typeCode=@"渔药";
            break;
        case 300107:
            typeCode=@"鱼苗";
            break;
        case 300108:
            typeCode=@"其他";
            break;
        default:
            break;
    }
    return typeCode;
    
}

+(NSString *)unit:(NSInteger)code{
    
    NSString *typeCode;
    
    switch (code) {
        case 10030:
            typeCode=@"克(g)";
            break;
        case 10031:
            typeCode=@"千克(Kg)";
            break;
        case 10032:
            typeCode=@"毫升(mL)";
            break;
        case 10033:
            typeCode=@"升(L)";
            break;
        case 10034:
            typeCode=@"吨";
            break;
        case 10035:
            typeCode=@"其他";
            break;
        case 10036:
            typeCode=@"只";
            break;
        case 10037:
            typeCode=@"头";
            break;
        case 10038:
            typeCode=@"支";
            break;
        case 10039:
            typeCode=@"袋";
            break;
        case 10040:
            typeCode=@"尾";
            break;
        default:
            typeCode=@"";
            break;
    }
    return typeCode;
    
}
// 111111农事生产
//10026农药
//10027肥料
//10028种子
+(NSString *)type:(NSInteger)code{
    
    NSString *typeCode;
    
    switch (code) {
        case 100101:
            typeCode=@"种子记录";
            break;
        case 100102:
            typeCode=@"农事生产记录";
            break;
        case 100103:
            typeCode=@"肥料记录";
            break;
        case 100104:
            typeCode=@"农药记录";
            break;
        case 100105:
            typeCode=@"农事生产记录";
            break;
        case 100106:
            typeCode=@"农事生产记录";
            break;
        case 100107:
            typeCode=@"农事生产记录";
            break;
        case 100108:
            typeCode=@"农事生产记录";
            break;
        case 300101:
            typeCode=@"投喂记录";
            break;
        case 300102:
            typeCode=@"养殖记录";
            break;
        case 300103:
            typeCode=@"养殖记录";
            break;
        case 300104:
            typeCode=@"养殖记录";
            break;
        case 300105:
            typeCode=@"养殖记录";
            break;
        case 300106:
            typeCode=@"鱼药记录";
            break;
        case 300107:
            typeCode=@"鱼苗记录";
            break;
        case 300108:
            typeCode=@"其他记录";
            break;
        case 1020101:
            typeCode=@"投喂记录";
            break;
        case 1020102:
            typeCode=@"养殖记录";
            break;
        case 1020103:
            typeCode=@"养殖记录";
            break;
        case 1020104:
            typeCode=@"养殖记录";
            break;
        case 1020105:
            typeCode=@"投药记录";
            break;
        case 1020106:
            typeCode=@"幼崽记录";
            break;
        case 1020107:
            typeCode=@"养殖记录";
        default:
            typeCode=@"";
            break;
    }
    return typeCode;
    
}


+(NSString *)title:(NSInteger)code{
    
    NSString *title;
    
    switch (code) {
        case 10007:
            title=@"溯源系统-农业";
            break;
        case 10008:
            title=@"溯源系统-畜禽业";
            break;
        case 10009:
            title=@"溯源系统-渔业";
            break;
        case 10010:
            title=@"溯源系统-其他";
            break;
        default:
            title=@"";
            break;
    }
    return title;
}

+(NSString *)pestControlTargets:(NSInteger)code{
    
    NSString *typeCode;
    
    switch (code) {
        case 10040:
            typeCode=@"杀虫剂";
            break;
        case 10041:
            typeCode=@"杀菌剂";
            break;
        case 10042:
            typeCode=@"杀螨剂";
            break;
        case 10043:
            typeCode=@"杀线虫剂";
            break;
        case 10044:
            typeCode=@"杀鼠剂";
            break;
        case 10045:
            typeCode=@"除草剂";
            break;
        case 10046:
            typeCode=@"脱叶剂";
            break;
        case 10047:
            typeCode=@"植物生成调节剂";
            break;
        default:
            typeCode=@"";
            break;
    }
    return typeCode;
    
}

@end
