//
//  AppDelegate.h
//  suyuanAPP
//
//  Created by mc on 2019/9/26.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

