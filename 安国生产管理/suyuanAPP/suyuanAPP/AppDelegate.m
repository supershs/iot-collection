//
//  AppDelegate.m
//  suyuanAPP
//
//  Created by mc on 2019/9/26.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import "AppDelegate.h"
#import "loginViewController.h"
#import "AgriculturalTableViewController.h"
#import "HomeCollectionViewController.h"
#import "loginViewController.h"
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [[UITextField appearance] setTintColor:[UIColor blackColor]];

    NSLog(@"bug%d",DEBUG);
    
    if (@available(iOS 13.0, *)) {
        UIUserInterfaceStyle mode = UITraitCollection.currentTraitCollection.userInterfaceStyle;
        if (mode == UIUserInterfaceStyleDark) {
            NSLog(@"深色模式");
            
        } else if (mode == UIUserInterfaceStyleLight) {
            NSLog(@"浅色模式");
        } else {
            NSLog(@"未知模式");
        }
    }
    self.window.backgroundColor=[UIColor whiteColor];

    NSLog(@"%@",[UserConfig shareConfig].user.loginName);
    
    
    if (![Utils valString:[UserConfig shareConfig].user.loginName ]) {
        HomeCollectionViewController *login=GetSbVc(@"Main", @"HomeCollectionViewController");
        UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:login];
        self.window.rootViewController=nav;
        
    }else{

        loginViewController *login=GetSbVc(@"Main", @"loginViewController");
        UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:login];
        self.window.rootViewController=nav;
        
    }

    [self.window makeKeyAndVisible];
    
//    #if TARGET == 1//纯溯源
//        NSLog(@"target：纯溯源");
//    #elif TARGET == 2//射阳溯源
//        NSLog(@"target：射阳溯源");
//    #elif TARGET == 3//广陵溯源
//        NSLog(@"target：广陵溯源");
//    #elif TARGET == 4//盱眙溯源
//        NSLog(@"target：盱眙溯源");
//    #endif
    
    
    return YES;
}





@end
