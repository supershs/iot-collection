//
//  loginViewController.m
//  suyuanAPP
//
//  Created by mc on 2019/9/26.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import "loginViewController.h"
#import "HomeCollectionViewController.h"

#import "NSData+CommomCryptor.h"
#import "NSData+CustomPadding.h"
#import "NSData+HMAC.h"
@interface loginViewController ()


@property (weak, nonatomic) IBOutlet UIImageView *logoImg;
@property (weak, nonatomic) IBOutlet UILabel *labTitle;

@property (strong, nonatomic) IBOutlet UIView *viewMoble;
@property (strong, nonatomic) IBOutlet UITextField *tfMobile;
@property (strong, nonatomic) IBOutlet UIView *viewPw;
@property (strong, nonatomic) IBOutlet UITextField *tfPW;
@property (nonatomic,strong) NSArray *IndustryAry;


@end

@implementation loginViewController


-(void)viewWillAppear:(BOOL)animated{
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.hidesBackButton = YES;
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    
}

-(void)viewWillDisappear:(BOOL)animated{
    
    self.navigationController.navigationBarHidden = NO;


}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];
    
    if (TARGET ==2) {
        
        _logoImg.image =[UIImage imageNamed:@"login_head"];
        
        _labTitle.text =@"射阳溯源系统";
        
    } else if (TARGET == 4) {
        _logoImg.image =[UIImage imageNamed:@"logo_xuyi"];
        
        _labTitle.text =@"盱眙溯源系统";
    }
    _viewMoble.layer.cornerRadius=5;
    _viewPw.layer.cornerRadius=5;


    

}
- (IBAction)login:(id)sender {
    
    if ([Utils valString:_tfMobile.text]) {
        [SVProgressHUD showErrorWithStatus:@"请输入账号"];
        
        return;
        
    }
    if ([Utils valString:_tfPW.text]) {
        [SVProgressHUD showErrorWithStatus:@"请输入密码"];
           return;
       }
    [SVProgressHUD show];
    
    NSData *sourceData = [self.tfPW.text dataUsingEncoding:NSUTF8StringEncoding];
    // Data -> AESEncrypt
    NSData *ansix923Data = [sourceData cc_encryptUsingAlgorithm:CcCryptoAlgorithmAES key:CBC_KEY InitializationVector:CBC_IV Mode:CcCryptorCBCMode Padding:CcCryptorZeroPadding];
    NSString *ansix923String = [ansix923Data base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    NSLog(@"%@",ansix923String);
    
    // API_URL 仅仅在此用 可能并无作用
    [YQDataManager postDataJsonRequestWithUrl:[NSString stringWithFormat:@"%@%@",API_BASE_URL_STRING,LoginUrl] Params:@{@"loginName":_tfMobile.text,@"password":ansix923String,@"url":API_URL} modelClass:[YQInfoData class] responseBlock:^(BOOL statue, YQBaseModel *item, id info) {
        
        if (statue) {
            
            [[UserConfig shareConfig] clearSignData];
            
            YQInfoData *dataInfo =info;
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",self.tfMobile.text] forKey:@"loginName"];
            [[NSUserDefaults standardUserDefaults] setObject:self.tfPW.text forKey:@"passWord"];

            [[NSUserDefaults standardUserDefaults] synchronize];
            [UserConfig shareConfig].user = dataInfo.userInfo;

            [[UserConfig shareConfig] saveUserInfoToFile];
            
       
             HomeCollectionViewController *home=GetSbVc(@"Main", @"HomeCollectionViewController");
             
             [self.navigationController pushViewController:home animated:YES];
             
            [SVProgressHUD dismiss];
            
        }else{
            
            [SVProgressHUD showErrorWithStatus:item.message];
            
        }
        
    }];
 
     
}

////MARK:key
//    OC中的AES加密是通过key的bytes数组位数来进行AES128/192/256加密
//    key -> 加密方式
//    16  -> AES128
//    24  -> AES192
//    32  -> AES256
////MARK: iv
//    iv:偏移量，加密过程中会按照偏移量进行循环位移
//    //MARK:OC
//    1. 引入头文件 #include <CommonCrypto/CommonCrypto.h>
 
- (NSData *)aesEncryptWithData:(NSData *)enData andKey:(NSData *)key iv:(NSData *)iv {
    if (key.length != 16 && key.length != 24 && key.length != 32) {
        return nil;
    }
    if (iv.length != 16 && iv.length != 0) {
        return nil;
    }
    
    NSData *result = nil;
    size_t bufferSize = enData.length + kCCBlockSizeAES128;
    void *buffer = malloc(bufferSize);
    if (!buffer) return nil;
    size_t encryptedSize = 0;
    CCCryptorStatus cryptStatus = CCCrypt(kCCEncrypt,
    kCCAlgorithmAES128,
    kCCOptionPKCS7Padding,//填充方式
    key.bytes,
    key.length,
    iv.bytes,
    enData.bytes,
    enData.length,
    buffer,
    bufferSize,
    &encryptedSize);
    if (cryptStatus == kCCSuccess) {
        result = [[NSData alloc]initWithBytes:buffer length:encryptedSize];
        free(buffer);
        return result;
    } else {
        free(buffer);
        return nil;
    }
    }
    - (NSData *)aesDecryptWithData:(NSData *)deData andKey:(NSData *)key iv:(NSData *)iv {
        if (key.length != 16 && key.length != 24 && key.length != 32) {
            return nil;
        }
        if (iv.length != 16 && iv.length != 0) {
            return nil;
        }
        
        NSData *result = nil;
        size_t bufferSize = deData.length + kCCBlockSizeAES128;
        void *buffer = malloc(bufferSize);
        if (!buffer) return nil;
        size_t encryptedSize = 0;
        CCCryptorStatus cryptStatus = CCCrypt(kCCDecrypt,
        kCCAlgorithmAES128,
        kCCOptionPKCS7Padding,//填充方式
        key.bytes,
        key.length,
        iv.bytes,
        deData.bytes,
        deData.length,
        buffer,
        bufferSize,
        &encryptedSize);
        if (cryptStatus == kCCSuccess) {
            result = [[NSData alloc]initWithBytes:buffer length:encryptedSize];
            free(buffer);
            return result;
        } else {
            free(buffer);
            return nil;
        }
}
  
NSData *AES256Encrypt(NSString *dataStr) {
    char keyPtr[kCCKeySizeAES256 + 1]; // room for terminator (unused)
    bzero(keyPtr, sizeof(keyPtr)); // fill with zeroes (for padding)
  
    // fetch key data
    [CBC_KEY getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];
  
    NSData *plainTextData = [dataStr dataUsingEncoding:NSUTF8StringEncoding];
    NSUInteger dataLength = [plainTextData length];
  
    //See the doc: For block ciphers, the output size will always be less than or
    //equal to the input size plus the block size minus one.
    //That's because we need one block of padding.
    size_t bufferSize = dataLength + kCCBlockSizeAES128;
    void *buffer = malloc(bufferSize);
  
    size_t numBytesEncrypted = 0;
    CCCryptorStatus cryptStatus = CCCrypt(kCCEncrypt, kCCAlgorithmAES128, kCCOptionPKCS7Padding | kCCOptionECBMode,
                                           keyPtr, kCCKeySizeAES256,
                                           NULL /* initialization vector (optional) */,
                                           [plainTextData bytes], dataLength, /* input */
                                       buffer, bufferSize, /* output */
                                           &numBytesEncrypted);
    if (cryptStatus == kCCSuccess) {
        //the returned NSData takes ownership of the buffer and will free it on deallocation
        return [NSData dataWithBytesNoCopy:buffer length:numBytesEncrypted];
    }
  
    free(buffer); //free the buffer;
    return nil;
}

@end
