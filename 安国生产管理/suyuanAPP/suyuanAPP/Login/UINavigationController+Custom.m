//
//  UINavigationController+Custom.m
//  suyuanAPP
//
//  Created by 叁拾叁 on 2022/4/2.
//  Copyright © 2022 江苏南京叁拾叁. All rights reserved.
//

#import "UINavigationController+Custom.h"

@implementation UINavigationController (Custom)

- (instancetype)init{
	UIImage *backgroundImage = [UIImage imageNamed:@"gzr1.jpg"];

	UIImage *symbolImage = [UIImage imageNamed:@"symbol"];

	NSString *titleString = @"XXXXXXXXXX";

	UIImage *combineImage = [self addImage:backgroundImage byImage:symbolImage title:titleString];

if (@available(iOS 15.0, *)) {

	UINavigationBarAppearance *barApp = [UINavigationBarAppearance new];

	[barApp setBackgroundImage:backImage];

	self.navigationBar.scrollEdgeAppearance = barApp;

	self.navigationBar.standardAppearance = barApp;

}else{

	[self.navigationBar setBackgroundImage:backImage forBarMetrics:UIBarMetricsDefault];

}
	
}
@end
