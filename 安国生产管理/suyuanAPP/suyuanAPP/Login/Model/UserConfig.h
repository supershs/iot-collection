//
//  UserConfig.h
//  YiQuNew
//
//  Created by Nasy on 2017/6/16.
//  Copyright © 2017年 Nasy. All rights reserved.
//

#import <Foundation/Foundation.h>


@class YQUserInfo;

@interface UserConfig : NSObject

@property(nonatomic,strong)  YQUserInfo *user;

+(instancetype)shareConfig;
///保存用户数据
- (void)saveUserInfoToFile;
- (void)clearSignData;
@end
