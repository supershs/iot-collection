//
//  uerInfo.m
//  CompanyAPP_Obj
//
//  Created by 杨琴 on 2019/7/11.
//  Copyright © 2019 YQ. All rights reserved.
//

#import "YQUserInfo.h"

@implementation YQUserInfo

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"userInfoIdentifier": @"id"};
}
#pragma mark - NSCoding Methods
//重写以下几个方法
- (void)encodeWithCoder:(NSCoder*)aCoder {
    [self mj_encode:aCoder];
}

- (id)initWithCoder:(NSCoder*)aDecoder
{
    if (self = [super init]) {
        
    [self mj_decode:aDecoder];
        
    }
    
    return self;
    
}

@end

@implementation YQInfoData

@end

