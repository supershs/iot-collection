//
//  UserConfig.m
//  YiQuNew
//
//  Created by Nasy on 2017/6/16.
//  Copyright © 2017年 Nasy. All rights reserved.
//

#import "UserConfig.h"


@implementation UserConfig
/**
 *  线程安全 防止数据脏 （数据不值得信任）
 NSLock
 dispatch_semaphore_t
 NSConditionLock
 @synchronized (self) {
 
 }
 
 内存区块 5
 
 栈区   （临时变量）
 堆区    （程序员手动分配空间，释放时也是程序员释放，alloc，malloc，calloc，ARC（自动管理）防止循环引用就可以  MRC releace  Cocoa底层框架开发 CFRelease 手动管理内存 ）
 全局区  （静态变量，全局变量）  （编译时分配，app结束时由系统释放）
 常量区   （常量）  （编译时分配，app结束时由系统释放）
 代码区
 
 单例模式 （整个类只有一个实例）
 外部环境不可控 （在内部进行控制，防止生成多个对象）
 注意：1.单例没有子类，扩展性降低；
      2.单例过多，也不好，权利过大，一直占用着内存；
      3.容易定位问题；
      4.高频率的生成一个对象，然后又释放，可以考虑用单列；
      5.app内不希望crash，即使外部想创建多个对象；
 */

static UserConfig *_userConfig;

/**
 *  文件加载的时候被执行（被动执行）main方法之前执行  加载时运行环境不稳定，关于其他方面的逻辑写在这容易找不到哦 一般不在这里处理逻辑
 */
+(void)load{
//    [UserConfig shareConfig];
}

/**
 *  某个类第一次调用的时候执行（被动执行）
 */
+(void)initialize{
    [UserConfig shareConfig];
}


+(instancetype)shareConfig{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *path = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:@"UserInfo.plist"];
        NSLog(@"path==%@",path);
        if (!_userConfig.user) {
            _userConfig.user = [[YQUserInfo alloc]init];
        }
        _userConfig = [[super allocWithZone:NULL] init];

        if([[NSFileManager defaultManager] fileExistsAtPath:path]){
            
            _userConfig.user = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
            
        }
    });
    return _userConfig;
}




#pragma mark - 保存用户数据
- (void)saveUserInfoToFile {
    
    [NSKeyedArchiver archiveRootObject:self.user toFile:[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:@"UserInfo.plist"]];
}



#pragma mark - 退出登录
- (void)clearSignData {
    
    //清除用户数据
    [UserConfig shareConfig].user=nil;
    
    
    [[UserConfig shareConfig ] saveUserInfoToFile];
    
    //创建通知
    NSNotification *notification =[NSNotification notificationWithName:@"loginOut" object:nil userInfo:nil];
    //通过通知中心发送通知
    [[NSNotificationCenter defaultCenter] postNotification:notification];
}

+(instancetype)init{
    if (_userConfig) {
        return _userConfig;
    }
    return [super init];
}

@end
