//
//  uerInfo.h
//  CompanyAPP_Obj
//
//  Created by 杨琴 on 2019/7/11.
//  Copyright © 2019 YQ. All rights reserved.
//

#import "YQDataManager.h"

NS_ASSUME_NONNULL_BEGIN



@interface YQUserInfo : YQDataBaseModel<NSCoding>

@property (nonatomic, strong) NSString *loginName;/* 登录名 */
@property (nonatomic, strong) NSString *userType;/* 用户类型 */
@property (nonatomic, strong) NSString *jobNumber;/* 工号 */
@property (nonatomic, strong) NSString *trueName;/* 真实姓名 */
@property (nonatomic, strong) NSString *password;/* 密码 */
@property (nonatomic, strong) NSString *salt;/* 盐值 */
@property (nonatomic, strong) NSString *sex;/* 性别（10男，20女） */
@property (nonatomic, strong) NSString *brithday;/* 生日 */
@property (nonatomic, strong) NSString *idCard;/* 身份证号码 */
@property (nonatomic, strong) NSString *email;/* 电子邮箱 */
@property (nonatomic, strong) NSString *mobile;/* 手机号 */
@property (nonatomic, strong) NSString *phone;/* 固定电话 */
@property (nonatomic, strong) NSString *headImg;/* 头像 */
@property (nonatomic, strong) NSString *registerDate;/* 注册日期 */
@property (nonatomic, strong) NSString *lastLoginTime;/* 上次登录时间 */
@property (nonatomic, strong) NSString *menuList;/* 可用菜单 */
@property (nonatomic, strong) NSString *roleName;
@property (nonatomic, strong) NSString *deptName;
@property (nonatomic, strong) NSString *traceFlag;/* 溯源主体（管理员ADMIN，企业ENTER） */
@property (nonatomic, strong) NSString *traceNumber;/* 生产经营主体编码 */
@property (nonatomic, assign) double tracePeasantId;/* 农户ID*/
@property (nonatomic,strong)NSString *traceIndustry;/*etraceIndustry 所属行业(10007种植业；10008畜牧业；10009渔业；10010其他)**/
@property (nonatomic, strong) NSString *userInfoIdentifier;
@property (nonatomic, assign) double tenantId;
@property (nonatomic, strong) NSString *uuid;

@end

@interface YQInfoData : YQDataBaseModel
@property (nonatomic,strong)YQUserInfo *userInfo;
@property (nonatomic, strong) NSString *token;
@end


NS_ASSUME_NONNULL_END
