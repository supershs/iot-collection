//
//  NSLayoutConstraint+BSIBDesignable.h
//  SuKen
//
//  Created by 杨琴 on 2020/3/5.
//  Copyright © 2020 AYQ. All rights reserved.
// storyboard  间距 s根据屏幕自适应  按照375 



#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSLayoutConstraint (BSIBDesignable)
@property(nonatomic, assign) IBInspectable BOOL adapterScreen;

@end

NS_ASSUME_NONNULL_END
