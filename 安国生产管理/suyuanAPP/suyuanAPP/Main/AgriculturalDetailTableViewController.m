//
//  AgriculturalDetailTableViewController.m
//  suyuanAPP
//
//  Created by mc on 2019/9/30.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import "AgriculturalDetailTableViewController.h"
#import "AgriculturalDetailTableViewCell.h"
@interface AgriculturalDetailTableViewController ()

@property (nonatomic,retain) UIScrollView *imageSrcollView;

@property (nonatomic,retain)NSMutableArray *arrData;

@property (nonatomic,copy)NSMutableArray *imageArr;

@property (nonatomic,retain)UILabel *countPageLabel;

@end

@implementation AgriculturalDetailTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.rowHeight=50;
    self.tableView.tableHeaderView=self.imageSrcollView;
    self.tableView.separatorColor = RGB(220, 220, 220);
    self.tableView.tableFooterView=[[UIView alloc] initWithFrame:CGRectZero];
    [self createNavAndClick];
}
-(void)createNavAndClick{
    UIButton *Btn=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [Btn setImage:[UIImage imageNamed:@"back_arrow"] forState:UIControlStateNormal];
    [Btn addTarget:self action:@selector(addPro) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backBtnItem=[[UIBarButtonItem alloc]initWithCustomView:Btn];
    self.navigationItem.leftBarButtonItem=backBtnItem;
}

-(void)addPro{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self getDetailData];
}

-(void)getDetailData{
    
    NSString *urlStr;
    
//    etraceIndustry 所属行业(10007种植业；10008畜牧业；10009渔业；10010其他)**/
    NSMutableDictionary *dic =[[NSMutableDictionary alloc]initWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"Property List" ofType:@"plist"]];
    if ([self.type isEqualToString:[[[dic objectForKey:[UserConfig shareConfig].user.traceIndustry] objectAtIndex:4] objectForKey:@"farmingCode"]]) {
        if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10007"]) {
            urlStr=[NSString stringWithFormat:@"%@%@/%@",API_BASE_URL_STRING,ProductionFarmDetailPost,_ids];
        }else if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10008"]){
            urlStr=[NSString stringWithFormat:@"%@%@/%@",API_BASE_URL_STRING,getAppProductionLivestockBreedInfoById,_ids];
        }else if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10009"]){
            urlStr=[NSString stringWithFormat:@"%@%@/%@",API_BASE_URL_STRING,ProductionFishbreedInfoGet,_ids];
        }
    }else{
        if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10007"]) {
            urlStr=[NSString stringWithFormat:@"%@%@/%@",API_BASE_URL_STRING,ProductionFarmDetailGet,_ids];
        }else if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10008"]){
            urlStr=[NSString stringWithFormat:@"%@%@/%@",API_BASE_URL_STRING,appGetLivestockinput,_ids];
        }else if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10009"]){
            urlStr=[NSString stringWithFormat:@"%@%@/%@",API_BASE_URL_STRING,ProductionFishInputGet,_ids];
        }
    }
    [YQDataManager getDataWithUrl:[urlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] Params:@{} modelClass:[YQBaseModel class] responseBlock:^(BOOL statue, YQBaseModel *item, id info) {
       
        if (statue) {
        
            [self dataManager:item.data];
            
        }else{
            [SVProgressHUD showWithStatus:@"网络错误"];
            [SVProgressHUD dismissWithDelay:1];
        }
        
    }];
}

-(void)dataManager:(id)data{
    
    if ([self.type isEqualToString:@"100100"]) {
        
        detailModel *model=[detailModel mj_objectWithKeyValues:data];
        
        _imageArr=[[NSMutableArray alloc] initWithArray:model.filePath];
        
        self.arrData =[NSMutableArray arrayWithObjects:
                        @{@"name":@"时间",@"value":model.createDate},
                        @{@"name":@"上传者",@"value":model.personName},
                        @{@"name":@"产品名称",@"value":[Utils valString:model.productName]?@"":model.productName},
                        @{@"name":@"生产档案号",@"value":[Utils valString:model.archiId]?@"":model.archiId},
					   @{@"name":@"农事类型",@"value":[Utils code:model.farmingCode]},
					   @{@"name":@"作业大棚",@"value":[Utils stringTurnString:model.massifName]}, nil];
        
    }else if([self.type isEqualToString:@"100101"]){
        
        ProductionInputModel *model=[ProductionInputModel mj_objectWithKeyValues:data];
        
        _imageArr=[[NSMutableArray alloc] initWithArray:model.filePath];
        
        NSString *count=[NSString stringWithFormat:@"%@%@",[Utils stringTurnString:model.inputConsum],[Utils unit:model.inputUnit]];
        self.arrData =[NSMutableArray arrayWithObjects:
                        @{@"name":@"时间",@"value":model.createDate},
                        @{@"name":@"上传者",@"value":model.personName},
                        @{@"name":@"产品名称",@"value":[Utils valString:model.productName]?@"":model.productName},
                        @{@"name":@"生产档案号",@"value":[Utils valString:model.archiId]?@"":model.archiId},
                        @{@"name":@"种子品种",@"value":model.inputName},
					   @{@"name":@"投入量",@"value":count},
					   @{@"name":@"作业大棚",@"value":[Utils stringTurnString:model.massifName]},nil];
        
    }else if([self.type isEqualToString:@"100103"]){
        
        ProductionInputModel *model=[ProductionInputModel mj_objectWithKeyValues:data];
        
        _imageArr=[[NSMutableArray alloc] initWithArray:model.filePath];
        
        NSString *count=[NSString stringWithFormat:@"%@%@",[Utils stringTurnString:model.inputConsum],[Utils unit:model.inputUnit]];
        self.arrData =[NSMutableArray arrayWithObjects:
                        @{@"name":@"时间",@"value":model.createDate},
                        @{@"name":@"上传者",@"value":model.personName},
                        @{@"name":@"产品名称",@"value":[Utils valString:model.productName]?@"":model.productName},
                        @{@"name":@"生产档案号",@"value":[Utils valString:model.archiId]?@"":model.archiId},
                        @{@"name":@"肥料名称",@"value":model.inputName},
					   @{@"name":@"使用量",@"value":count},
					   @{@"name":@"作业大棚",@"value":[Utils stringTurnString:model.massifName]},nil];
        
    }else if([self.type isEqualToString:@"100104"]){
        
        ProductionInputModel *model=[ProductionInputModel mj_objectWithKeyValues:data];
        
        _imageArr=[[NSMutableArray alloc] initWithArray:model.filePath];
        
        NSString *count=[NSString stringWithFormat:@"%@%@",[Utils stringTurnString:model.inputConsum],[Utils unit:model.inputUnit]];
        self.arrData =[NSMutableArray arrayWithObjects:
                        @{@"name":@"时间",@"value":model.createDate},
                        @{@"name":@"上传者",@"value":model.personName},
                        @{@"name":@"产品名称",@"value":[Utils valString:model.productName]?@"":model.productName},
                        @{@"name":@"生产档案号",@"value":[Utils valString:model.archiId]?@"":model.archiId},
                        @{@"name":@"农药名称",@"value":model.inputName},
                        @{@"name":@"使用量",@"value":count},
					   @{@"name":@"作业大棚",@"value":[Utils stringTurnString:model.massifName]},
//                        @{@"name":@"防治对象",@"value":[Utils pestControlTargets:model.pestControlTargets]},
                       nil];
        
    }else if([self.type isEqualToString:@"1020100"]){
        
        detailModel *model=[detailModel mj_objectWithKeyValues:data];
        
        _imageArr=[[NSMutableArray alloc] initWithArray:model.filePath];
        
        self.arrData =[NSMutableArray arrayWithObjects:
                        @{@"name":@"时间",@"value":model.createDate},
                        @{@"name":@"上传者",@"value":model.personName},
                        @{@"name":@"产品名称",@"value":[Utils valString:model.productName]?@"":model.productName},
                        @{@"name":@"生产档案号",@"value":[Utils valString:model.archiId]?@"":model.archiId},
					   @{@"name":@"作业类型",@"value":[Utils code:model.breedCode]},
					   @{@"name":@"作业大棚",@"value":[Utils stringTurnString:model.massifName]},nil];
        
    }else if([self.type isEqualToString:@"1020101"]){
        
        ProductionInputModel *model=[ProductionInputModel mj_objectWithKeyValues:data];
        
        _imageArr=[[NSMutableArray alloc] initWithArray:model.filePath];
        
        NSString *count=[NSString stringWithFormat:@"%@%@",[Utils stringTurnString:model.inputConsum],[Utils unit:model.inputUnit]];
        self.arrData =[NSMutableArray arrayWithObjects:
                        @{@"name":@"时间",@"value":model.createDate},
                        @{@"name":@"上传者",@"value":model.personName},
                        @{@"name":@"产品名称",@"value":[Utils valString:model.productName]?@"":model.productName},
                        @{@"name":@"生产档案号",@"value":[Utils valString:model.archiId]?@"":model.archiId},
                        @{@"name":@"饲料名称",@"value":model.inputName},
					   @{@"name":@"使用量",@"value":count},
					   @{@"name":@"作业大棚",@"value":[Utils stringTurnString:model.massifName]},nil];
        
    }else if([self.type isEqualToString:@"1020105"]){
        
        ProductionInputModel *model=[ProductionInputModel mj_objectWithKeyValues:data];
        
        _imageArr=[[NSMutableArray alloc] initWithArray:model.filePath];
        
        NSString *count=[NSString stringWithFormat:@"%@%@",[Utils stringTurnString:model.inputConsum],[Utils unit:model.inputUnit]];
        self.arrData =[NSMutableArray arrayWithObjects:
                        @{@"name":@"时间",@"value":model.createDate},
                        @{@"name":@"上传者",@"value":model.personName},
                        @{@"name":@"产品名称",@"value":[Utils valString:model.productName]?@"":model.productName},
                        @{@"name":@"生产档案号",@"value":[Utils valString:model.archiId]?@"":model.archiId},
                        @{@"name":@"兽药名称",@"value":model.inputName},
					   @{@"name":@"使用量",@"value":count},
					   @{@"name":@"作业大棚",@"value":[Utils stringTurnString:model.massifName]},nil];
        
    }else if([self.type isEqualToString:@"1020106"]){
        
        ProductionInputModel *model=[ProductionInputModel mj_objectWithKeyValues:data];
        
        _imageArr=[[NSMutableArray alloc] initWithArray:model.filePath];
        
        NSString *count=[NSString stringWithFormat:@"%@%@",[Utils stringTurnString:model.inputConsum],[Utils unit:model.inputUnit]];
        self.arrData =[NSMutableArray arrayWithObjects:
                        @{@"name":@"时间",@"value":model.createDate},
                        @{@"name":@"上传者",@"value":model.personName},
                        @{@"name":@"产品名称",@"value":[Utils valString:model.productName]?@"":model.productName},
                        @{@"name":@"生产档案号",@"value":[Utils valString:model.archiId]?@"":model.archiId},
					   @{@"name":@"养殖品种",@"value":model.inputName},
					   @{@"name":@"作业大棚",@"value":[Utils stringTurnString:model.massifName]},
                        @{@"name":@"养殖数量",@"value":count},nil];
        
    }else if([self.type isEqualToString:@"300100"]){
        
        detailModel *model=[detailModel mj_objectWithKeyValues:data];
        
        _imageArr=[[NSMutableArray alloc] initWithArray:model.filePath];
        
        self.arrData =[NSMutableArray arrayWithObjects:
                        @{@"name":@"时间",@"value":model.createDate},
                        @{@"name":@"上传者",@"value":model.personName},
                        @{@"name":@"产品名称",@"value":[Utils valString:model.productName]?@"":model.productName},
                        @{@"name":@"生产档案号",@"value":[Utils valString:model.archiId]?@"":model.archiId},
					   @{@"name":@"作业类型",@"value":[Utils code:model.fishbreedCode]},
					   @{@"name":@"作业大棚",@"value":[Utils stringTurnString:model.massifName]}, nil];
        
    }else if([self.type isEqualToString:@"300101"]){
        
        ProductionInputModel *model=[ProductionInputModel mj_objectWithKeyValues:data];
        
        _imageArr=[[NSMutableArray alloc] initWithArray:model.filePath];
        
        NSString *count=[NSString stringWithFormat:@"%@%@",[Utils stringTurnString:model.inputConsum],[Utils unit:model.inputUnit]];
        self.arrData =[NSMutableArray arrayWithObjects:
                        @{@"name":@"时间",@"value":model.createDate},
                        @{@"name":@"上传者",@"value":model.personName},
                        @{@"name":@"产品名称",@"value":[Utils valString:model.productName]?@"":model.productName},
                        @{@"name":@"生产档案号",@"value":[Utils valString:model.archiId]?@"":model.archiId},
                        @{@"name":@"饲料名称",@"value":model.inputName},
					   @{@"name":@"使用量",@"value":count},
					   @{@"name":@"作业大棚",@"value":[Utils stringTurnString:model.massifName]},nil];
    }else if([self.type isEqualToString:@"300107"]){
        
        ProductionInputModel *model=[ProductionInputModel mj_objectWithKeyValues:data];
        
        _imageArr=[[NSMutableArray alloc] initWithArray:model.filePath];
        
        NSString *count=[NSString stringWithFormat:@"%@%@",[Utils stringTurnString:model.inputConsum],[Utils unit:model.inputUnit]];
        self.arrData =[NSMutableArray arrayWithObjects:
                        @{@"name":@"时间",@"value":model.createDate},
                        @{@"name":@"上传者",@"value":model.personName},
                        @{@"name":@"产品名称",@"value":[Utils valString:model.productName]?@"":model.productName},
                        @{@"name":@"生产档案号",@"value":[Utils valString:model.archiId]?@"":model.archiId},
                        @{@"name":@"养殖品种",@"value":model.inputName},
					   @{@"name":@"养殖数量",@"value":count},
					   @{@"name":@"作业大棚",@"value":[Utils stringTurnString:model.massifName]},nil];
    }else if([self.type isEqualToString:@"300106"]){
        
        ProductionInputModel *model=[ProductionInputModel mj_objectWithKeyValues:data];
        
        _imageArr=[[NSMutableArray alloc] initWithArray:model.filePath];
        
        NSString *count=[NSString stringWithFormat:@"%@%@",[Utils stringTurnString:model.inputConsum],[Utils unit:model.inputUnit]];
        self.arrData =[NSMutableArray arrayWithObjects:
                        @{@"name":@"时间",@"value":model.createDate},
                        @{@"name":@"上传者",@"value":model.personName},
                        @{@"name":@"产品名称",@"value":[Utils valString:model.productName]?@"":model.productName},
                        @{@"name":@"生产档案号",@"value":[Utils valString:model.archiId]?@"":model.archiId},
                        @{@"name":@"鱼药名称",@"value":model.inputName},
					   @{@"name":@"使用量",@"value":count},
					   @{@"name":@"作业大棚",@"value":[Utils stringTurnString:model.massifName]},nil];
    }
    self.imageSrcollView.contentSize=CGSizeMake(kWidth*_imageArr.count, kWidth/2);
    for (int i=0; i<_imageArr.count; i++) {
        UIImageView *UpImgeView=[[UIImageView alloc] initWithFrame:CGRectMake(i*kWidth, 0, kWidth, kWidth/2)];
        NSString *imagePath=[NSString stringWithFormat:@"%@%@",API_Image_URL,_imageArr[i]];
        UpImgeView.contentMode=UIViewContentModeScaleAspectFill;
        [UpImgeView sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[UIImage imageNamed:@"image"]];
        [self.imageSrcollView addSubview:UpImgeView];
    }
    if (self.imageArr.count>1) {
        [self.view addSubview:self.countPageLabel];
        _countPageLabel.text=[NSString stringWithFormat:@"1/%ld",_imageArr.count];
    }
    [self.tableView reloadData];
    
}

#pragma mark ---图片的滑动代理方法
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    //    显示当前页
    _countPageLabel.text=[NSString stringWithFormat:@"%.f/%ld",scrollView.contentOffset.x/kWidth+1,_imageArr.count];

}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.arrData.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"AgriculturalDetailTableViewCell";
    //改为以下的方法
    AgriculturalDetailTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell==nil) {
        cell=[[AgriculturalDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.userInteractionEnabled=NO;
    if (self.arrData.count) {
        [cell inserDataAgriculturalDetailCell:self.arrData[indexPath.row]];
    }
    return cell;
}

-(UILabel *)countPageLabel{
    if (!_countPageLabel) {
        _countPageLabel=[[UILabel alloc] initWithFrame:CGRectMake(_imageSrcollView.right-50, _imageSrcollView.bottom-35, 40, 25)];
        _countPageLabel.backgroundColor=RGBA(0, 0, 0, 0.4);
        _countPageLabel.layer.masksToBounds=YES;
        _countPageLabel.layer.cornerRadius=3;
        _countPageLabel.textAlignment=NSTextAlignmentCenter;
        _countPageLabel.font=[UIFont systemFontOfSize:12];
        _countPageLabel.textColor=RGB(255,255,255);
    }
    return _countPageLabel;
}

-(UIScrollView *)imageSrcollView{
    if (!_imageSrcollView) {
        _imageSrcollView=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, kWidth, kWidth/2)];
        _imageSrcollView.pagingEnabled=YES;
        _imageSrcollView.bounces=YES;
        _imageSrcollView.delegate=self;
        _imageSrcollView.showsHorizontalScrollIndicator=NO;
    }
    return _imageSrcollView;
}

@end
