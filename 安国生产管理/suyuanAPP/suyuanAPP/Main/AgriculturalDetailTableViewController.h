//
//  AgriculturalDetailTableViewController.h
//  suyuanAPP
//
//  Created by mc on 2019/9/30.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AgriculturalDetailTableViewController : UITableViewController

@property (nonatomic,copy)NSString *ids;

@property (nonatomic,copy)NSString *type;

@end

NS_ASSUME_NONNULL_END
