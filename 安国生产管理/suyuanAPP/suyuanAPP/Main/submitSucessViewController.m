//
//  submitSucessViewController.m
//  suyuanAPP
//
//  Created by mc on 2019/9/30.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import "submitSucessViewController.h"
#import "SuccessView.h"
#import "HomeCollectionViewController.h"
@interface submitSucessViewController ()<UIScrollViewDelegate>

@property (nonatomic,retain)SuccessView *topView;

@end

@implementation submitSucessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createNavAndClick];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.view.backgroundColor=RGB(235, 235, 235);
    [self.view addSubview:self.topView];
    [_topView setDataForsucessView:self.dict andTitle:self.inputCategory];
}

-(void)createNavAndClick{
    UIButton *Btn=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [Btn setImage:[UIImage imageNamed:@"back_arrow"] forState:UIControlStateNormal];
    [Btn addTarget:self action:@selector(addPro) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backBtnItem=[[UIBarButtonItem alloc]initWithCustomView:Btn];
    self.navigationItem.leftBarButtonItem=backBtnItem;
}

-(void)addPro{
    
    for (UIViewController *vc in self.navigationController.viewControllers) {
        
        if ([vc isKindOfClass:[HomeCollectionViewController class]]) {
            [self.navigationController popToViewController:vc animated:YES];

            
        }
    }
    
    
}

-(SuccessView *)topView{
    if (!_topView) {
        _topView=[[SuccessView alloc] initWithFrame:CGRectMake(0, 0, kWidth, kHeight)];
    }
    return _topView;
}



@end
