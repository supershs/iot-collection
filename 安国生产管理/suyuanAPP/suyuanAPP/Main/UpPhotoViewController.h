//
//  UpPhotoViewController.h
//  suyuanAPP
//
//  Created by mc on 2019/10/12.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import "YQBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface UpPhotoViewController : YQBaseViewController

@property (nonatomic,strong)NSMutableDictionary *dicAddFarm;// 方便最后调用接口
@property (nonatomic,copy)NSString *inputCategory;
@property (nonatomic,assign)BOOL hasArea;
@end

NS_ASSUME_NONNULL_END
