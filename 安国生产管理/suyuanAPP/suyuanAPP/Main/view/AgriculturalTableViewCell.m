//
//  AgriculturalTableViewCell.m
//  suyuanAPP
//
//  Created by mc on 2019/9/29.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import "AgriculturalTableViewCell.h"


@interface AgriculturalTableViewCell ()

@property (nonatomic,retain)UIView *backgroudView;

@property (nonatomic,retain)UIImageView *leftImageView;

@property (nonatomic,retain)UILabel *titleLabel;

@property (nonatomic,retain)UILabel *lineLabel;

@property (nonatomic,retain)UIImageView *bannarImageView;

@property (nonatomic,retain)UILabel *workTypeLabel;

@property (nonatomic,retain)UILabel *workTimeLabel;

@property (nonatomic,retain)UILabel *workType;

@property (nonatomic,retain)UILabel *workTime;


@end

@implementation AgriculturalTableViewCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self createAgriculturalCell];
    
    }
    return self;
}


-(void)createAgriculturalCell{
    
    [self.contentView addSubview:self.backgroundView];
    [self.backgroundView addSubview:self.leftImageView];
    [self.backgroundView addSubview:self.titleLabel];
    [self.backgroundView addSubview:self.lineLabel];
    [self.backgroundView addSubview:self.bannarImageView];
    [self.backgroundView addSubview:self.workTypeLabel];
    [self.backgroundView addSubview:self.workType];
    [self.backgroundView addSubview:self.workTimeLabel];
    [self.backgroundView addSubview:self.workTime];
    
}


-(void)inserDataCells:(detailModel *)model{
    
    NSString *imagePath=[NSString stringWithFormat:@"%@%@",API_Image_URL,[Utils stringTurnString:model.path]];
    [_bannarImageView sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[UIImage imageNamed:@"image"]];
    _workTime.text=[Utils stringTurnString:model.createDate];
    _titleLabel.text=[NSString stringWithFormat:@"上传人：%@",[Utils stringTurnString:model.personName]];//@"使用土地：008";
    if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10007"]) {
        _workType.text=[Utils code:model.farmingCode];
    }else if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10008"]){
        _workType.text=[Utils code:model.breedCode];
    }else if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10009"]){
        _workType.text=[Utils code:model.fishbreedCode];
    }
    
    
}



-(UIView *)backgroundView{
    if (!_backgroudView) {
        _backgroudView=[[UIView alloc] initWithFrame:CGRectMake(10, 10, kWidth-20, 140)];
        _backgroudView.layer.cornerRadius=5;
        _backgroudView.backgroundColor=[UIColor whiteColor];
        _backgroudView.layer.shadowColor = RGB(170, 170, 170).CGColor;
        _backgroudView.layer.shadowOffset = CGSizeMake(2, 5);
        _backgroudView.layer.shadowOpacity = 0.5;
        _backgroudView.layer.shadowRadius = 5;
    }
    return _backgroudView;
}
-(UIImageView *)leftImageView{
    if (!_leftImageView) {
        _leftImageView=[[UIImageView alloc] initWithFrame:CGRectMake(15,15,15,10)];
        _leftImageView.image=[UIImage imageNamed:@"dikuai"];
    }
    return _leftImageView;
}
-(UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(_leftImageView.right+10, _leftImageView.y-5, kWidth-100, 20)];
        _titleLabel.font=[UIFont systemFontOfSize:12];
        _titleLabel.textColor=RGB(100, 100, 100);
    }
    return _titleLabel;
}
-(UILabel *)lineLabel{
    if (!_lineLabel) {
        _lineLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, _titleLabel.bottom+10, _backgroudView.width, 1)];
        _lineLabel.backgroundColor=RGB(240, 240, 240);
    }
    return _lineLabel;
}

-(UIImageView *)bannarImageView{
    if (!_bannarImageView) {
        _bannarImageView=[[UIImageView alloc] initWithFrame:CGRectMake(10, _lineLabel.bottom+10, 80, 80)];
        _bannarImageView.contentMode=UIViewContentModeScaleAspectFill;
        _bannarImageView.layer.masksToBounds=YES;
    }
    return _bannarImageView;
}

-(UILabel *)workTimeLabel{
    if (!_workTimeLabel) {
        _workTimeLabel=[[UILabel alloc] initWithFrame:CGRectMake(_workTypeLabel.left, _workTypeLabel.bottom, _workTypeLabel.width, _workTypeLabel.height)];
        _workTimeLabel.text=@"作业时间";
        _workTimeLabel.textAlignment=NSTextAlignmentRight;
        _workTimeLabel.textColor=RGB(102, 102, 102);
        _workTimeLabel.font=[UIFont systemFontOfSize:14];
    }
    return _workTimeLabel;
}
-(UILabel *)workTypeLabel{
    if (!_workTypeLabel) {
        _workTypeLabel=[[UILabel alloc] initWithFrame:CGRectMake(_bannarImageView.right+10, _bannarImageView.y+20, 60, 20)];
        _workTypeLabel.text=@"作业类型";
        _workTypeLabel.textAlignment=NSTextAlignmentRight;
        _workTypeLabel.textColor=RGB(102, 102, 102);
        _workTypeLabel.font=[UIFont systemFontOfSize:14];
    }
    return _workTypeLabel;
}
-(UILabel *)workTime{
    if (!_workTime) {
        _workTime=[[UILabel alloc] initWithFrame:CGRectMake(_workType.left, _workType.bottom, _workType.width, _workType.height)];
        _workTime.textColor=RGB(51, 51, 51);
        _workTime.font=[UIFont systemFontOfSize:14];
    }
    return _workTime;
}
-(UILabel *)workType{
    if (!_workType ) {
        _workType=[[UILabel alloc] initWithFrame:CGRectMake(_workTypeLabel.right+10, _workTypeLabel.y, _backgroudView.width-_workTimeLabel.right-10, _workTypeLabel.height)];
        
        _workType.textColor=RGB(51, 51, 51);
        _workType.font=[UIFont systemFontOfSize:14];
    }
    return _workType;
}


@end
