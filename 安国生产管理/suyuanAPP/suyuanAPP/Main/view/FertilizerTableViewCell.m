//
//  FertilizerTableViewCell.m
//  suyuanAPP
//
//  Created by mc on 2019/9/29.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import "FertilizerTableViewCell.h"

@interface FertilizerTableViewCell ()

@property (nonatomic,retain)UIView *backgroudView;

@property (nonatomic,retain)UIImageView *leftImageView;

@property (nonatomic,retain)UILabel *titleLabel;

@property (nonatomic,retain)UILabel *lineLabel;

@property (nonatomic,retain)UIImageView *bannarImageView;

@property (nonatomic,retain)UILabel *FertilizerNameLabel;

@property (nonatomic,retain)UILabel *countLabel;

@property (nonatomic,retain)UILabel *typeLabel;

@property (nonatomic,retain)UILabel *useTimeLabel;

@property (nonatomic,retain)UILabel *FertilizerName;

@property (nonatomic,retain)UILabel *count;

@property (nonatomic,retain)UILabel *type;

@property (nonatomic,retain)UILabel *useTime;



@end

@implementation FertilizerTableViewCell


-(void)inserDataCell:(NSString *)cellTypeStr andModel:(ProductionInputModel *)model{
    
    if ([cellTypeStr isEqualToString:@"100104"]) {
        _FertilizerNameLabel.text=@"农药名称";
        _countLabel.text=@"使用量";
        _useTimeLabel.text=@"使用时间";
    }else if ([cellTypeStr isEqualToString:@"100101"]){
        _FertilizerNameLabel.text=@"种子品种";
        _countLabel.text=@"投入量";
        _useTimeLabel.text=@"使用时间";
    }else if ([cellTypeStr isEqualToString:@"100103"]){
        _FertilizerNameLabel.text=@"肥料名称";
        _countLabel.text=@"使用量";
        _useTimeLabel.text=@"使用时间";
    }else if ([cellTypeStr isEqualToString:@"1020101"]){
        _FertilizerNameLabel.text=@"饲料名称";
        _countLabel.text=@"使用量";
        _useTimeLabel.text=@"使用时间";
    }else if ([cellTypeStr isEqualToString:@"1020105"]){
        _FertilizerNameLabel.text=@"兽药名称";
        _countLabel.text=@"使用量";
        _useTimeLabel.text=@"使用时间";
    }else if ([cellTypeStr isEqualToString:@"1020106"]){
        _FertilizerNameLabel.text=@"养殖品种";
        _countLabel.text=@"养殖数量";
        _useTimeLabel.text=@"投入时间";
    }else if ([cellTypeStr isEqualToString:@"300101"]){
        _FertilizerNameLabel.text=@"饲料名称";
        _countLabel.text=@"使用量";
        _useTimeLabel.text=@"使用时间";
    }else if ([cellTypeStr isEqualToString:@"300107"]){
        _FertilizerNameLabel.text=@"鱼苗名称";
        _countLabel.text=@"养殖数量";
        _useTimeLabel.text=@"投入时间";
    }else if ([cellTypeStr isEqualToString:@"300106"]){
        _FertilizerNameLabel.text=@"鱼药名称";
        _countLabel.text=@"使用量";
        _useTimeLabel.text=@"使用时间";
    }
    
    _useTime.text=[Utils stringTurnString:model.createDate];
    _FertilizerName.text=[Utils stringTurnString:model.inputName];
    _count.text=[NSString stringWithFormat:@"%@%@",[Utils stringTurnString:model.inputConsum],[Utils unit:model.inputUnit]];
    NSString *imagePath=[NSString stringWithFormat:@"%@%@",API_Image_URL,[Utils stringTurnString:model.path]];
    _titleLabel.text=[NSString stringWithFormat:@"上传人：%@",[Utils stringTurnString:model.personName]];
    [_bannarImageView sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[UIImage imageNamed:@"image"]];
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self createFertilizerCell];
    
    }
    return self;
}


-(void)createFertilizerCell{
    
    [self.contentView addSubview:self.backgroundView];
    [self.backgroundView addSubview:self.leftImageView];
    [self.backgroundView addSubview:self.titleLabel];
    [self.backgroundView addSubview:self.lineLabel];
    [self.backgroundView addSubview:self.bannarImageView];
    [self.backgroundView addSubview:self.FertilizerNameLabel];
    [self.backgroundView addSubview:self.FertilizerName];
    [self.backgroundView addSubview:self.countLabel];
    [self.backgroundView addSubview:self.count];
//    [self.backgroundView addSubview:self.typeLabel];
//    [self.backgroundView addSubview:self.type];
    [self.backgroundView addSubview:self.useTimeLabel];
    [self.backgroundView addSubview:self.useTime];
}


-(UIView *)backgroundView{
    if (!_backgroudView) {
        _backgroudView=[[UIView alloc] initWithFrame:CGRectMake(10, 10, kWidth-20, 140)];
        _backgroudView.layer.cornerRadius=5;
        _backgroudView.backgroundColor=[UIColor whiteColor];
        _backgroudView.layer.shadowColor = RGB(170, 170, 170).CGColor;
        _backgroudView.layer.shadowOffset = CGSizeMake(4, 6);
        _backgroudView.layer.shadowOpacity = 0.5;
        _backgroudView.layer.shadowRadius = 5;
    }
    return _backgroudView;
}
-(UIImageView *)leftImageView{
    if (!_leftImageView) {
        _leftImageView=[[UIImageView alloc] initWithFrame:CGRectMake(15,15,15,10)];
        _leftImageView.image=[UIImage imageNamed:@"dikuai"];
    }
    return _leftImageView;
}
-(UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(_leftImageView.right+10, _leftImageView.y-5, kWidth-100, 20)];
        _titleLabel.font=[UIFont systemFontOfSize:12];
        _titleLabel.textColor=RGB(100, 100, 100);
    }
    return _titleLabel;
}
-(UILabel *)lineLabel{
    if (!_lineLabel) {
        _lineLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, _titleLabel.bottom+10, _backgroudView.width, 1)];
        _lineLabel.backgroundColor=RGB(240, 240, 240);
    }
    return _lineLabel;
}

-(UIImageView *)bannarImageView{
    if (!_bannarImageView) {
        _bannarImageView=[[UIImageView alloc] initWithFrame:CGRectMake(10, _lineLabel.bottom+10, 80, 80)];
        _bannarImageView.contentMode=UIViewContentModeScaleAspectFill;
        _bannarImageView.layer.masksToBounds=YES;
    }
    return _bannarImageView;
}
-(UILabel *)FertilizerNameLabel{
    if (!_FertilizerNameLabel) {
        _FertilizerNameLabel=[[UILabel alloc] initWithFrame:CGRectMake(_bannarImageView.right+10, _bannarImageView.y, 60, 25)];
        _FertilizerNameLabel.textAlignment=NSTextAlignmentRight;
        _FertilizerNameLabel.textColor=RGB(102, 102, 102);
        _FertilizerNameLabel.font=[UIFont systemFontOfSize:14];
    }
    return _FertilizerNameLabel;
}
-(UILabel *)FertilizerName{
    if (!_FertilizerName) {
        _FertilizerName=[[UILabel alloc] initWithFrame:CGRectMake(_FertilizerNameLabel.right+10, _FertilizerNameLabel.y,  _backgroudView.width-_FertilizerNameLabel.right-20, _FertilizerNameLabel.height)];
        _FertilizerName.textColor=RGB(51, 51, 51);
        _FertilizerName.font=[UIFont systemFontOfSize:14];
    }
    return _FertilizerName;
}
-(UILabel *)countLabel{
    if (!_countLabel) {
        _countLabel=[[UILabel alloc] initWithFrame:CGRectMake(_FertilizerNameLabel.left, _FertilizerNameLabel.bottom, _FertilizerNameLabel.width, _FertilizerNameLabel.height)];
        _countLabel.textAlignment=NSTextAlignmentRight;
        _countLabel.textColor=RGB(102, 102, 102);
        _countLabel.font=[UIFont systemFontOfSize:14];
    }
    return _countLabel;
}
-(UILabel *)count{
    if (!_count) {
        _count=[[UILabel alloc] initWithFrame:CGRectMake(_FertilizerName.left, _FertilizerName.bottom, _FertilizerName.width, _FertilizerName.height)];
        _count.textColor=RGB(51, 51, 51);
        _count.font=[UIFont systemFontOfSize:14];
    }
    return _count;
}
//-(UILabel *)typeLabel{
//    if (!_typeLabel) {
//        _typeLabel=[[UILabel alloc] initWithFrame:CGRectMake(_countLabel.left, _countLabel.bottom, _countLabel.width, _countLabel.height)];
//        _typeLabel.textAlignment=NSTextAlignmentRight;
//        _typeLabel.textColor=RGB(102, 102, 102);
//        _typeLabel.font=[UIFont systemFontOfSize:14];
//    }
//    return _typeLabel;
//}
//
//-(UILabel *)type{
//    if (!_type) {
//        _type=[[UILabel alloc] initWithFrame:CGRectMake(_count.left, _count.bottom, _count.width, _count.height)];
//        _type.textColor=RGB(51, 51, 51);
//        _type.font=[UIFont systemFontOfSize:14];
//    }
//    return _type;
//}
-(UILabel *)useTimeLabel{
    if (!_useTimeLabel) {
//        _useTimeLabel=[[UILabel alloc] initWithFrame:CGRectMake(_typeLabel.left, _typeLabel.bottom, _typeLabel.width, _typeLabel.height)];
        _useTimeLabel=[[UILabel alloc] initWithFrame:CGRectMake(_countLabel.left, _countLabel.bottom, _countLabel.width, _countLabel.height)];
        
        _useTimeLabel.textAlignment=NSTextAlignmentRight;
        _useTimeLabel.textColor=RGB(102, 102, 102);
        _useTimeLabel.font=[UIFont systemFontOfSize:14];
    }
    return _useTimeLabel;
}
-(UILabel *)useTime{
    if (!_useTime ) {
//        _useTime=[[UILabel alloc] initWithFrame:CGRectMake(_type.left, _type.bottom, _type.width, _type.height)];
        _useTime=[[UILabel alloc] initWithFrame:CGRectMake(_count.left, _count.bottom, _count.width, _count.height)];
        
        _useTime.textColor=RGB(51, 51, 51);
        _useTime.font=[UIFont systemFontOfSize:14];
    }
    return _useTime;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
