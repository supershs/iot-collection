//
//  BreedTableViewCell.h
//  suyuanAPP
//
//  Created by mc on 2019/9/30.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BreedTableViewCell : UITableViewCell

-(void)inserDataCell:(NSString *)cellTypeStr andModel:(ProductionInputModel *)model;

@end

NS_ASSUME_NONNULL_END
