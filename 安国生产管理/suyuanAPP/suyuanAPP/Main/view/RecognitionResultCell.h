//
//  RecognitionResultCell.h
//  suyuanAPP
//
//  Created by mc on 2019/10/25.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^deleteBtnClick)(NSInteger tag);

NS_ASSUME_NONNULL_BEGIN

@interface RecognitionResultCell : UITableViewCell


@property (nonatomic,copy) deleteBtnClick deleteClick;

-(void)insertImagePhoto:(UIImage *)image andRow:(NSInteger)row;


@end

NS_ASSUME_NONNULL_END
