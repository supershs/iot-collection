//
//  AgriculturalDetailTableViewCell.h
//  suyuanAPP
//
//  Created by mc on 2019/9/29.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AgriculturalDetailTableViewCell : UITableViewCell

-(void)inserDataAgriculturalDetailCell:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
