//
//  RecognitionResultCell.m
//  suyuanAPP
//
//  Created by mc on 2019/10/25.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import "RecognitionResultCell.h"


@interface RecognitionResultCell ()

@property (nonatomic,retain)UIImageView *PhotosImageView;

@property (nonatomic,retain)UIButton *deleteBtn;

//@property (nonatomic,retain)UIImageView *PhotosImageView;
//
//@property (nonatomic,retain)UIImageView *PhotosImageView;
//
//@property (nonatomic,retain)UIImageView *PhotosImageView;



@end

@implementation RecognitionResultCell



-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        
        [self.contentView addSubview:self.PhotosImageView];
        [self.PhotosImageView addSubview:self.deleteBtn];
        
    }
    
    return self;
}



-(void)insertImagePhoto:(UIImage *)image andRow:(NSInteger)row{
    
    self.PhotosImageView.image=image;
    _deleteBtn.tag=row+1;
    
}

-(void)deleteBtnClick:(UIButton *)btn{
    
    self.deleteClick(btn.tag-1);
    
}

-(UIButton *)deleteBtn{
    
    if (!_deleteBtn) {
        _deleteBtn=[[UIButton alloc] initWithFrame:CGRectMake(_PhotosImageView.width-30, 0, 30, 30)];
        [_deleteBtn setBackgroundImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];
        [_deleteBtn addTarget:self action:@selector(deleteBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _deleteBtn;
}

-(UIImageView *)PhotosImageView{
    
    if (!_PhotosImageView) {
        _PhotosImageView=[[UIImageView alloc] initWithFrame:CGRectMake(10, 10, kWidth-20, (kWidth-20)/355*216)];
        _PhotosImageView.contentMode=UIViewContentModeScaleAspectFill;
        _PhotosImageView.backgroundColor=[UIColor whiteColor];
        _PhotosImageView.userInteractionEnabled=YES;
        _PhotosImageView.layer.masksToBounds=YES;
        _PhotosImageView.layer.cornerRadius=5;
        _PhotosImageView.layer.shadowColor = RGB(170, 170, 170).CGColor;
        _PhotosImageView.layer.shadowOffset = CGSizeMake(4, 6);
        _PhotosImageView.layer.shadowOpacity = 0.5;
        _PhotosImageView.layer.shadowRadius = 5;
    }
    return _PhotosImageView;
}





- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
