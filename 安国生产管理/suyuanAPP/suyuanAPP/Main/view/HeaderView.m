

//
//  HeaderView.m
//  suyuanAPP
//
//  Created by mc on 2019/10/12.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import "HeaderView.h"

@interface HeaderView ()

@property (nonatomic,retain)UIView *headerView;

@property (nonatomic,retain)UIButton *addPicBtn;

@end

@implementation HeaderView


-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self=[super initWithFrame:frame]) {
        
        self.userInteractionEnabled=YES;
        [self createHeadView];
    }
    return self;
}


-(void)createHeadView{
    
    [self addSubview:self.headerView];
    [self.headerView addSubview:self.addPicBtn];
    
}

-(void)addPicBtnClick{
    
    self.addClicks();
}

-(UIButton *)addPicBtn{
    if (!_addPicBtn) {
        _addPicBtn=[[UIButton alloc] initWithFrame:CGRectMake(_headerView.width/2-17, _headerView.height/2-14, 34, 28)];
        [_addPicBtn setBackgroundImage:[UIImage imageNamed:@"addPic"] forState:UIControlStateNormal];
        [_addPicBtn addTarget:self action:@selector(addPicBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _addPicBtn;
}

-(UIView *)headerView{
    if (!_headerView) {
        _headerView=[[UIView alloc] initWithFrame:CGRectMake(10, 10, kWidth-20, 216)];
        _headerView.backgroundColor=[UIColor whiteColor];
         _headerView.userInteractionEnabled=YES;
        _headerView.layer.cornerRadius=5;
        _headerView.layer.masksToBounds=YES;
    }
    return _headerView;
}


@end
