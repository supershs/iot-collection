
//
//  SuccessView.m
//  suyuanAPP
//
//  Created by mc on 2019/10/8.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import "SuccessView.h"

@interface SuccessView()<UIScrollViewDelegate,MAMapViewDelegate>

@property (nonatomic,retain)UIView *TopView;

@property (nonatomic,retain)UIView *DetailView;

@property (nonatomic,retain)UIImageView *YESLabel;

@property (nonatomic,retain)UILabel *titleLabel;

@property (nonatomic,retain)UILabel *subTitleLabel;

@property (nonatomic,retain)UILabel *countPageLabel;

@property (nonatomic,retain)UILabel *detailLabel;

@property (nonatomic,retain)UILabel *timeLabel;

@property (nonatomic,retain) UIScrollView *imageSrcollView;

@property (nonatomic,retain)MAMapView *mapView;

@property (nonatomic,retain)NSMutableArray *array;

@property (nonatomic,retain)NSMutableArray *traceProductionInputList;

@property (nonatomic,retain) UIScrollView *mySrcollView;



@end



@implementation SuccessView

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self==[super initWithFrame:frame]) {
        
        [self addSubview:self.mySrcollView];
        [self createSucessView];
        
    }
    return self;
    
}

-(void)createSucessView{
    
    [self.mySrcollView addSubview:self.TopView];
    [self.mySrcollView addSubview:self.DetailView];
    
    [self.TopView addSubview:self.YESLabel];
    [self.TopView addSubview:self.titleLabel];
    [self.DetailView addSubview:self.subTitleLabel];
    [self.DetailView addSubview:self.imageSrcollView];
    [self.DetailView addSubview:self.countPageLabel];
    [self.DetailView addSubview:self.detailLabel];
    [self.DetailView addSubview:self.timeLabel];
//    [MAMapView updatePrivacyShow:AMapPrivacyShowStatusDidShow privacyInfo:AMapPrivacyInfoStatusDidContain];
//    [MAMapView updatePrivacyAgree:AMapPrivacyAgreeStatusDidAgree];
    
    [self.DetailView addSubview:self.mapView];
        
}

-(void)setDataForsucessView:(NSDictionary *)dict andTitle:(NSString *)title{
    
    _array=[[NSMutableArray alloc]initWithArray:dict[@"filePath"]];
    
    if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10007"]) {
        _traceProductionInputList=[[NSMutableArray alloc] initWithArray:[ProductionInputModel mj_objectArrayWithKeyValuesArray:dict[@"traceProductionInputList"]]];
        _subTitleLabel.text=[NSString stringWithFormat:@"您今天完成的%@",[Utils type:[dict[@"farmingCode"] integerValue]]];
    }else if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10008"]){
        _traceProductionInputList=[[NSMutableArray alloc] initWithArray:[ProductionInputModel mj_objectArrayWithKeyValuesArray:dict[@"traceProductionLivestockInputList"]]];
        _subTitleLabel.text=[NSString stringWithFormat:@"您今天完成的%@",[Utils type:[dict[@"breedCode"] integerValue]]];
    }else{
        _traceProductionInputList=[[NSMutableArray alloc] initWithArray:[ProductionInputModel mj_objectArrayWithKeyValuesArray:dict[@"traceProductionFishInputList"]]];
        _subTitleLabel.text=[NSString stringWithFormat:@"您今天完成的%@",[Utils type:[dict[@"fishbreedCode"] integerValue]]];
    }
    
    self.imageSrcollView.contentSize=CGSizeMake(236*_array.count, 140);
    for (int i=0; i<_array.count; i++) {
        UIImageView *UpImgeView=[[UIImageView alloc] initWithFrame:CGRectMake(i*236, 0, 236, 140)];
        NSString *imagePath=[NSString stringWithFormat:@"%@%@",API_Image_URL,_array[i]];
        UpImgeView.contentMode=UIViewContentModeScaleAspectFill;
        UpImgeView.layer.masksToBounds=YES;
        [UpImgeView sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[UIImage imageNamed:@"image"]];
        [self.imageSrcollView addSubview:UpImgeView];
    }
    _timeLabel.text=[NSString stringWithFormat:@"时间：%@",[Utils stringTurnString:dict[@"createDate"]]];
    _countPageLabel.text=[NSString stringWithFormat:@"1/%ld",_array.count];
    
    if (_traceProductionInputList.count>0) {
        
        self.detailLabel.hidden=YES;
        
        for (int i=0; i<_traceProductionInputList.count; i++) {
            
            ProductionInputModel *model=_traceProductionInputList[i];
            UILabel  *Label=[[UILabel alloc] initWithFrame:CGRectMake(10, _imageSrcollView.bottom+15+i*40+15, self.width-40, 30)];
            Label.backgroundColor=RGB(234, 250, 240);
            Label.layer.masksToBounds=YES;
            Label.layer.cornerRadius=3;
            Label.textAlignment=NSTextAlignmentCenter;
            Label.font=[UIFont systemFontOfSize:14];
            Label.textColor=RGB(12,162,67);
            Label.text=[NSString stringWithFormat:@"%@       %@%@",model.inputName,[Utils stringTurnString:model.inputConsum],[Utils unit:model.inputUnit]];
            [self.DetailView addSubview:Label];
        }
        _DetailView.height=440+_traceProductionInputList.count*55;
        _timeLabel.frame=CGRectMake(_detailLabel.left, _detailLabel.bottom+20+(_traceProductionInputList.count-1)*55, _detailLabel.width, 15);
        _mapView.frame=CGRectMake(15, self.timeLabel.bottom+15, self.DetailView.width-30, 145);
        self.mySrcollView.contentSize=CGSizeMake(kWidth, self.DetailView.bottom);
        
        
    }else{
        
        self.detailLabel.hidden=NO;
        if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10007"]) {
            _detailLabel.text=[Utils code:[dict[@"farmingCode"] integerValue]];
        }else if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10008"]){
            _detailLabel.text=[Utils code:[dict[@"breedCode"] integerValue]];
        }else{
            _detailLabel.text=[Utils code:[dict[@"fishbreedCode"] integerValue]];
        }
        self.mySrcollView.contentSize=CGSizeMake(kWidth, self.DetailView.bottom);
    }
    
}


#pragma mark ---图片的滑动代理方法
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    //    显示当前页
    _countPageLabel.text=[NSString stringWithFormat:@"%.f/%ld",scrollView.contentOffset.x/236+1,_array.count];

}

-(UILabel *)subTitleLabel{
    if (!_subTitleLabel) {
        _subTitleLabel=[[UILabel alloc] initWithFrame:CGRectMake(self.TopView.width/2-120,15, 240, 15)];
        _subTitleLabel.textAlignment=NSTextAlignmentCenter;
        _subTitleLabel.font=[UIFont systemFontOfSize:15];
        _subTitleLabel.textColor=RGB(51, 51, 51);
    }
    return _subTitleLabel;
}
-(UILabel *)countPageLabel{
    if (!_countPageLabel) {
        _countPageLabel=[[UILabel alloc] initWithFrame:CGRectMake(_subTitleLabel.right-50, _imageSrcollView.bottom-35, 40, 25)];
        _countPageLabel.backgroundColor=RGBA(0, 0, 0, 0.4);
        _countPageLabel.layer.masksToBounds=YES;
        _countPageLabel.layer.cornerRadius=3;
        _countPageLabel.textAlignment=NSTextAlignmentCenter;
        _countPageLabel.font=[UIFont systemFontOfSize:12];
        _countPageLabel.textColor=RGB(255,255,255);
    }
    return _countPageLabel;
}
-(UILabel *)detailLabel{
    if (!_detailLabel) {
        _detailLabel=[[UILabel alloc] initWithFrame:CGRectMake(_subTitleLabel.left, _imageSrcollView.bottom+15, _subTitleLabel.width, 30)];
        _detailLabel.backgroundColor=RGB(234, 250, 240);
        _detailLabel.layer.masksToBounds=YES;
        _detailLabel.layer.cornerRadius=3;
        _detailLabel.textAlignment=NSTextAlignmentCenter;
        _detailLabel.font=[UIFont systemFontOfSize:14];
        _detailLabel.textColor=RGB(12,162,67);
    }
    return _detailLabel;
}
-(UILabel *)timeLabel{
    if (!_timeLabel) {
        _timeLabel=[[UILabel alloc] initWithFrame:CGRectMake(_detailLabel.left, _detailLabel.bottom+20, _detailLabel.width, 15)];
        _timeLabel.textAlignment=NSTextAlignmentCenter;
        _timeLabel.font=[UIFont systemFontOfSize:14];
        _timeLabel.textColor=RGB(128, 128, 128);
    }
    return _timeLabel;
}

-(UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(self.TopView.width/2-40, self.YESLabel.bottom+10, 80, 18)];
        _titleLabel.text=@"提交成功!";
        _titleLabel.textAlignment=NSTextAlignmentCenter;
        _titleLabel.font=[UIFont systemFontOfSize:18];
        _titleLabel.textColor=RGB(38, 38, 38);
    }
    return _titleLabel;
}

-(UIImageView *)YESLabel{
    if (!_YESLabel) {
        _YESLabel=[[UIImageView alloc] initWithFrame:CGRectMake(self.TopView.width/2-30, 20, 60, 60)];
        _YESLabel.image=[UIImage imageNamed:@"image_s"];
    }
    return _YESLabel;
}

-(UIView *)TopView{
    if (!_TopView) {
        _TopView=[[UIView alloc] initWithFrame:CGRectMake(10,10, kWidth-20, 135)];
        _TopView.backgroundColor=[UIColor whiteColor];
        _TopView.layer.masksToBounds=YES;
        _TopView.layer.cornerRadius=5;
    }
    return _TopView;
}
-(UIView *)DetailView{
    if (!_DetailView) {
        _DetailView=[[UIView alloc] initWithFrame:CGRectMake(_TopView.left,_TopView.bottom+10, kWidth-20, 440)];
        _DetailView.backgroundColor=[UIColor whiteColor];
        _DetailView.layer.masksToBounds=YES;
        _DetailView.layer.cornerRadius=5;
        
    }
    return _DetailView;
}

-(UIScrollView *)imageSrcollView{
    if (!_imageSrcollView) {
        _imageSrcollView=[[UIScrollView alloc] initWithFrame:CGRectMake(_DetailView.width/2-118, _subTitleLabel.bottom+15, 236, 140)];
        _imageSrcollView.pagingEnabled=YES;
        _imageSrcollView.bounces=YES;
        _imageSrcollView.delegate=self;
        _imageSrcollView.showsHorizontalScrollIndicator=NO;
    }
    return _imageSrcollView;
}

-(MAMapView *)mapView{
    if (!_mapView) {
        _mapView=[[MAMapView alloc]initWithFrame:CGRectMake(15, self.timeLabel.bottom+15, self.DetailView.width-30, 145)];
        _mapView.userTrackingMode=MAUserTrackingModeFollow;
        _mapView.zoomLevel=15;
        _mapView.showsUserLocation=YES;
        _mapView.delegate=self;
        _mapView.showsCompass=NO;
        _mapView.showsScale=NO;
    }
    return _mapView;
}
-(UIScrollView *)mySrcollView{
    if (!_mySrcollView) {
        _mySrcollView=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, kWidth, kHeight)];
        _mySrcollView.pagingEnabled=YES;
        _mySrcollView.bounces=YES;
        _mySrcollView.delegate=self;
        _mySrcollView.showsHorizontalScrollIndicator=NO;
    }
    return _mySrcollView;
}

@end
