//
//  HeaderView.h
//  suyuanAPP
//
//  Created by mc on 2019/10/12.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void(^addBtnClick)(void);

NS_ASSUME_NONNULL_BEGIN

@interface HeaderView : UIView

@property (nonatomic,copy)addBtnClick addClicks;

@end

NS_ASSUME_NONNULL_END
