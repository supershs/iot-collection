//
//  SuccessView.h
//  suyuanAPP
//
//  Created by mc on 2019/10/8.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SuccessView : UIView

-(void)setDataForsucessView:(NSDictionary *)dict andTitle:(NSString *)title;

@end

NS_ASSUME_NONNULL_END
