//
//  AgriculturalDetailTableViewCell.m
//  suyuanAPP
//
//  Created by mc on 2019/9/29.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import "AgriculturalDetailTableViewCell.h"

@interface AgriculturalDetailTableViewCell ()

@property (nonatomic,retain)UILabel *titleLabel;

@property (nonatomic,retain)UILabel *subTitleLabel;


@end

@implementation AgriculturalDetailTableViewCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self createAgriculturalDetailCell];
    
    }
    return self;
}


-(void)createAgriculturalDetailCell{
    
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.subTitleLabel];
}


-(void)inserDataAgriculturalDetailCell:(NSDictionary *)dict{
    
    self.titleLabel.text=[Utils stringTurnString:dict[@"name"]];
    self.subTitleLabel.text=[Utils stringTurnString:dict[@"value"]];//dict[@"value"];
    
}


-(UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(15, 15, 80, 20)];
        _titleLabel.text=@"上传时间";
        _titleLabel.textColor=RGB(102, 102, 102);
        _titleLabel.font=[UIFont systemFontOfSize:14];
    }
    return _titleLabel;
}
-(UILabel *)subTitleLabel{
    if (!_subTitleLabel) {
        _subTitleLabel=[[UILabel alloc] initWithFrame:CGRectMake(_titleLabel.right+10, _titleLabel.y, kWidth-_titleLabel.right-25, _titleLabel.height)];
        _subTitleLabel.text=@"2019-10-25 14:25";
        _subTitleLabel.textAlignment=NSTextAlignmentRight;
        _subTitleLabel.textColor=RGB(64, 64, 64);
        _subTitleLabel.font=[UIFont systemFontOfSize:14];
    }
    return _subTitleLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
