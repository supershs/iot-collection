//
//  AreaModel.m
//  suyuanAPP
//
//  Created by 叁拾叁 on 2020/8/28.
//  Copyright © 2020 江苏南京叁拾叁. All rights reserved.
//

#import "AreaData.h"


@implementation AreaData

+ (NSDictionary *)objectClassInArray {
     return @{@"records" : [AreaRecords class]};
}

@end

@implementation AreaRecords

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
     return @{@"idField":@"id",};
}

@end
