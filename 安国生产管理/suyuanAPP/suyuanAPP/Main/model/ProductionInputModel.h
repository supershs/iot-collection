//
//  ProductionInputModel.h
//  suyuanAPP
//
//  Created by mc on 2019/10/21.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProductionInputModel : YQListBaseModel

@property (nonatomic, retain) NSString *enterNumber;
@property (nonatomic, retain) NSString *enterName;
@property (nonatomic, retain) NSString *createDate;
@property (nonatomic, retain) NSString *inputName;
@property (nonatomic, retain) NSString *inputConsum;
@property (nonatomic, assign) NSInteger inputUnit;
@property (nonatomic, retain) NSString *updateDate;
@property (nonatomic, assign) NSInteger pestControlTargets;//防治对象
@property (nonatomic, retain) NSString *remarks;
@property (nonatomic, retain) NSString *sort;
@property (nonatomic, retain) NSString *path;
@property (nonatomic, retain) NSArray *filePath;
@property (nonatomic, retain) NSString *personName;
@property (nonatomic, retain) NSString *ids;
@property (nonatomic, retain) NSString *archiId;// 生产档案
@property (nonatomic, retain) NSString *productName;
@property (nonatomic, retain) NSString *massifName;/*地块名称*/

@end

NS_ASSUME_NONNULL_END
