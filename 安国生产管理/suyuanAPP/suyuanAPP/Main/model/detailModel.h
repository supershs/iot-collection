//
//  detailModel.h
//  suyuanAPP
//
//  Created by mc on 2019/10/17.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


@interface detailModel : YQListBaseModel

/*
 农事类别：100100
 播种：100101;
 灌溉：100102;
 施肥：100103;
 用药：100104;
 除草：100105;
 插秧：100106
 翻地：100107;
 其他：100108)"
 */
@property (nonatomic, assign) NSInteger farmingCode;/*农事类别()*/
@property (nonatomic, retain) NSString *enterNumber;/*生产经营主体编号*/
@property (nonatomic, retain) NSString *enterName;/*生产经营主体编号*/
@property (nonatomic, assign) NSInteger massifId;/*地块ID*/
@property (nonatomic, retain) NSString *massifName;/*地块名称*/
@property (nonatomic, assign) NSInteger massifArea;/*地块面积*/
@property (nonatomic, retain) NSString *massifUnit;/*地块单位(10016亩；10017平方）*/
@property (nonatomic, assign) NSInteger personId;/*作业人员ID*/
@property (nonatomic, retain) NSString *personName;/*作业人员姓名*/
@property (nonatomic, retain) NSString *taskDate;/*作业时间*/
@property (nonatomic, retain) NSString *taskPic;/*作业图片*/
@property (nonatomic, retain) NSString *createDate;
@property (nonatomic, retain) NSArray *filePath;
@property (nonatomic, retain) NSString *path;
@property (nonatomic, retain) NSString *ids;
@property (nonatomic, assign) NSInteger breedCode;/*农事类别()*/
@property (nonatomic, retain) NSString *createBy;
@property (nonatomic, retain) NSString *fieldArea;/*地块面积*/
@property (nonatomic, retain) NSString *fieldId;/*地块ID*/
@property (nonatomic, retain) NSString *fieldName;/*地块名称*/
@property (nonatomic, retain) NSString *fieldUnit;/*地块单位(10016亩；10017平方）*/
@property (nonatomic, retain) NSString *productName;
@property (nonatomic, retain) NSString *remarks;
@property (nonatomic, retain) NSString *status;
@property (nonatomic, retain) NSString *traceProductionLivestockInputList;
@property (nonatomic, retain) NSString *updateDate;
@property (nonatomic, assign) NSInteger fishbreedCode;
@property (nonatomic, retain) NSString *archiId;// 生产档案

@end



NS_ASSUME_NONNULL_END
