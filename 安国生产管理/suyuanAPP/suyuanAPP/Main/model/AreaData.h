//
//  AreaModel.h
//  suyuanAPP
//
//  Created by 叁拾叁 on 2020/8/28.
//  Copyright © 2020 江苏南京叁拾叁. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class AreaData,AreaRecords;


@interface AreaData : NSObject

@property (nonatomic, strong) NSArray <AreaRecords *> * records;
@property (nonatomic, assign) NSInteger current;
@property (nonatomic, assign) BOOL searchCount;
@property (nonatomic, assign) NSInteger pages;
@property (nonatomic, assign) NSInteger total;
@property (nonatomic, assign) NSInteger size;

@end

@interface AreaRecords : NSObject

@property (nonatomic, strong) NSObject * remarks;
@property (nonatomic, copy) NSString * uuid;
@property (nonatomic, copy) NSString * massifName;
@property (nonatomic, assign) NSInteger massifArea;
@property (nonatomic, copy) NSString * idField;
@property (nonatomic, strong) NSObject * sort;
@property (nonatomic, assign) NSInteger updateBy;
@property (nonatomic, assign) NSInteger createBy;
@property (nonatomic, assign) NSInteger massifUnit;
@property (nonatomic, strong) NSObject * archiId;
@property (nonatomic, strong) NSObject * tenantId;
@property (nonatomic, copy) NSString * enterNumber;
@property (nonatomic, copy) NSString * updateDate;
@property (nonatomic, assign) NSInteger version;
@property (nonatomic, assign) NSInteger massifId;
@property (nonatomic, assign) NSInteger status;
@property (nonatomic, copy) NSString * createDate;

@end


NS_ASSUME_NONNULL_END
