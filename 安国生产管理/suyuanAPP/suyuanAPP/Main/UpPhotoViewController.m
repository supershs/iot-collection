//
//  UpPhotoViewController.m
//  suyuanAPP
//
//  Created by mc on 2019/10/12.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import "UpPhotoViewController.h"
#import "PhotosTableViewCell.h"
#import "AgriculturalDetailTableViewController.h"
#import "submitSucessViewController.h"
#import "HeaderView.h"
#import <MOFSPickerView.h>
#import <MOFSPickerManager/MOFSPickerManager.h>
#import "AreaData.h"

@interface UpPhotoViewController ()<UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic,retain)UITableView *photoTableView;

@property (nonatomic,retain)NSMutableArray *imageArr;

@property (nonatomic,retain)HeaderView *headerView;

@property (nonatomic,retain)UIButton *submmitBtn;

@property (nonatomic,retain)UIButton *choiceBtn;

@property (nonatomic,strong)UIImageView *xialaImgView;

@property (nonatomic,strong)UILabel *placeholdLb;

@property (strong, nonatomic) UIImagePickerController *picker;

@property (nonatomic,strong) AreaData *areaModel;

@property (nonatomic,strong) AreaRecords *selectArea;

@property (nonatomic,strong) UITapGestureRecognizer *areaLbTapGes;

@property (nonatomic,strong) UITapGestureRecognizer *areaImgViewTapGes;



@end

@implementation UpPhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=RGB(230, 230, 230);
    [self.view addSubview:self.photoTableView];
    [self.view addSubview:self.submmitBtn];
    [self.view addSubview:self.choiceBtn];
    [self.choiceBtn addSubview:self.xialaImgView];
    [self.choiceBtn addSubview:self.placeholdLb];
    self.areaLbTapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(choiceBtnClick)];
    self.areaImgViewTapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(choiceBtnClick)];
    [self.xialaImgView addGestureRecognizer:self.areaImgViewTapGes];
    [self.placeholdLb addGestureRecognizer:self.areaLbTapGes];
    typeof(self) __weak weakSelf = self;
    self.headerView.addClicks = ^{//添加图片点击事件
        [weakSelf createChoose];
    };
	[self getAreaList];

	NSLog(@"%@===10007",[UserConfig shareConfig].user.traceIndustry);
}
//-(void)setHasArea:(BOOL)hasArea{
//	self.showDK = hasArea;
//	[self getAreaList];
//
//
//}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (self.imageArr.count>2) {
        return [[UIView alloc] initWithFrame:CGRectZero];
    }else{
         return  self.headerView;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (self.imageArr.count>2) {
        return 10;
    }else{
         return self.headerView.height;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1;
}

#pragma mark  ------上传图片

-(void)submmitBtnClick{
    
    if ([self.imageArr count]==0) {
        [SVProgressHUD showErrorWithStatus:@"请选择图片"];
        [SVProgressHUD dismissWithDelay:1.0];
        return;
    }
    
//    if (self.showDK) {
        if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10007"]) {
            if ([Utils isNullObject:self.selectArea]) {
                [SVProgressHUD showErrorWithStatus:@"请选择大棚"];
                [SVProgressHUD dismissWithDelay:1.0];
                return;
            }
        }
//    }
      
      [YQDataManager postUpdataHttpRequestWithUrl:[NSString stringWithFormat:@"%@%@",API_Image_URL,UrlFile] params:nil images:self.imageArr name:@"files" mimeType:@"jpg/png/jpeg" fileName:@"fileName" modelClass:[YQBaseModel class] responseBlock:^(BOOL statue, YQBaseModel *item, id info) {
          NSLog(@"%@____%@",info,item.message)
          
          if (statue) {
              [SVProgressHUD showWithStatus:@"请求成功"];

              [self getCommit:[Utils stringTurnString:item.message]];
              [SVProgressHUD dismissWithDelay:1.0];
              
          }else{
              [SVProgressHUD showErrorWithStatus:item.message];
              [SVProgressHUD dismissWithDelay:1.0];
          }
      }];
      
      [self.dicAddFarm setObject:@"" forKey:@"taskPic"];
    
}

-(void)getAreaList {
    NSMutableString *ApiUrl =[NSMutableString stringWithString:API_BASE_URL_STRING];
    
//    if (self.showDK) {
        if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10007"]) {
                [ApiUrl appendFormat:Area_nongye_List];
                NSDictionary *dic = @{@"archiId": self.dicAddFarm[@"archiId"]};
                WeakSelf;
                [YQDataManager postDataJsonRequestWithUrl:[ApiUrl copy] Params:dic modelClass:[AreaData class] responseBlock:^(BOOL statue, YQBaseModel *item, id info) {
                    if (statue) {
                        weakSelf.areaModel = info;
                    }
                }];
            }else if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10008"]) {
        //        [ApiUrl appendFormat:Area_xuqin_List];  暂时不做该功能
                self.photoTableView.frame = CGRectMake(0, 0, kWidth, kHeight-kTopHeight-55);
                [self.choiceBtn setHidden:YES];
            }else if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10009"]) {
        //        [ApiUrl appendFormat:Area_yuye_List];  暂时不做该功能
                self.photoTableView.frame = CGRectMake(0, 0, kWidth, kHeight-kTopHeight-55);
                [self.choiceBtn setHidden:YES];
//            }
    } else {
        self.photoTableView.frame = CGRectMake(0, 0, kWidth, kHeight-kTopHeight-55);
        [self.choiceBtn setHidden:YES];
    }
}

-(void)choiceBtnClick{
    
    WeakSelf;
    [[MOFSPickerManager shareManger]showPickerViewWithCustomDataArray:self.areaModel.records keyMapper:@"massifName" title:nil cancelTitle:@"取消" commitTitle:@"确定" commitBlock:^(id model) {
        
        weakSelf.selectArea = model;
        weakSelf.placeholdLb.text = @"";
        [weakSelf.choiceBtn setTitle:[NSString stringWithFormat:@"作业大棚：  %@", weakSelf.selectArea.massifName] forState:UIControlStateNormal];
        
        NSLog(@"model==%@",model);
        
    } cancelBlock:^{
        
    }];
}


-(void)getCommit:(NSString *)taskPic{
    
    [self.dicAddFarm setObject:taskPic forKey:@"taskPic"];
    NSNumber *unitNum = [NSNumber numberWithInteger:self.selectArea.massifUnit];
    NSNumber *areaNum = [NSNumber numberWithInteger:self.selectArea.massifArea];
    
     NSMutableString *ApiUrl =[NSMutableString stringWithString:API_BASE_URL_STRING];
     if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10007"]) {
//         if (self.showDK) {
             
             // 地块参数
             [self.dicAddFarm setObject:self.selectArea.idField forKey:@"massifId"];// 地块ID
             [self.dicAddFarm setObject:self.selectArea.massifName forKey:@"massifName"];// 地块名称
             [self.dicAddFarm setObject:unitNum forKey:@"massifUnit"];// 地块单位(10016亩；10017平方）
             [self.dicAddFarm setObject:areaNum forKey:@"massifArea"];// 地块面积
//         }
         
         [ApiUrl appendFormat:ProductionAddPost];
     }else if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10008"]){
         [self.dicAddFarm setObject:[self.dicAddFarm objectForKey:@"farmingCode"] forKey:@"breedCode"];
         [self.dicAddFarm removeObjectForKey:@"farmingCode"];
         
         // 地块参数
//         [self.dicAddFarm setObject:self.selectArea.idField forKey:@"field_id"];//栋舍ID
//         [self.dicAddFarm setObject:self.selectArea.massifName forKey:@"field_name"];// 栋舍名称
//         [self.dicAddFarm setObject:unitNum forKey:@"field_unit"];// 地块单位(10016亩；10017平方）
//         [self.dicAddFarm setObject:areaNum forKey:@"field_area"];// 栋舍面积
         
         [ApiUrl appendFormat:ProductionAddLivestockBreedPost];
     }else if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10009"]){
         [self.dicAddFarm setObject:[self.dicAddFarm objectForKey:@"farmingCode"] forKey:@"fishbreedCode"];
         [self.dicAddFarm removeObjectForKey:@"farmingCode"];
         
         // 地块参数
//         [self.dicAddFarm setObject:self.selectArea.idField forKey:@"tangkou_id"];// 塘口ID
//         [self.dicAddFarm setObject:self.selectArea.massifName forKey:@"tangkou_name"];// 塘口名称
//         [self.dicAddFarm setObject:unitNum forKey:@"tangkou_unit"];// 鱼塘单位(10016亩；10017平方）
//         [self.dicAddFarm setObject:areaNum forKey:@"tangkou_area"];// 塘口面积
         
         [ApiUrl appendFormat:ProductionAddFishbreedPost];
     }
      [YQDataManager postDataJsonRequestWithUrl:[ApiUrl copy] Params:[self.dicAddFarm copy] modelClass:[YQBaseModel class] responseBlock:^(BOOL statue, YQBaseModel *item, id info) {
          if (statue) {
              submitSucessViewController *subMit =[[submitSucessViewController alloc]init];
              subMit.inputCategory=self.inputCategory;
              subMit.dict=item.data;
              subMit.title=@"提交成功";
              [self.navigationController pushViewController:subMit animated:YES];
          }
      }];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.imageArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"PhotosTableViewCell";
    //改为以下的方法
    PhotosTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell==nil) {
        cell=[[PhotosTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.backgroundColor=RGB(230, 230, 230);

    if (self.imageArr.count) {
        [cell insertImagePhoto:self.imageArr[indexPath.row] andRow:indexPath.row];
        typeof(self) __weak weakSelf = self;
        cell.deleteClick = ^(NSInteger tag) {
            [weakSelf.imageArr removeObjectAtIndex:tag];
            [weakSelf.photoTableView reloadData];
        };
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
//    AgriculturalDetailTableViewController *VC=[[AgriculturalDetailTableViewController alloc] init];
//    [self.navigationController pushViewController:VC animated:YES];
}
-(void)createChoose{
    
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *CamaroAction = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            self.picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            self.modalPresentationStyle=UIModalPresentationFullScreen;
            [self presentViewController:self.picker animated:YES completion:nil];
        }
    }];
    UIAlertAction* photoAction = [UIAlertAction actionWithTitle:@"从相册选取" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        self.picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        self.modalPresentationStyle=UIModalPresentationFullScreen;
        [self presentViewController:self.picker animated:YES completion:nil];
    }];
    UIAlertAction *cancelAction=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:CamaroAction];
    [alert addAction:photoAction];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info{
    UIImage *image = info[UIImagePickerControllerEditedImage];//获取图片
    [self.imageArr insertObject:image atIndex:0];//添加到数组中
    [self.photoTableView reloadData];
    [picker dismissViewControllerAnimated:YES completion:nil];//获取图片后返回
}
//按取消按钮时候的功能
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(NSMutableArray *)imageArr{
    if (!_imageArr) {
        _imageArr=[[NSMutableArray alloc] init];
    }
    return _imageArr;
}
 
-(HeaderView *)headerView{
    if (!_headerView) {
        _headerView=[[HeaderView alloc] initWithFrame:CGRectMake(0, 0, kWidth, 236)];
    }
    return _headerView;
}

- (UIImagePickerController *)picker{
    if (!_picker) {
        _picker = [[UIImagePickerController alloc]init];
        _picker.delegate = self;
        _picker.allowsEditing=YES;
    }
    return _picker;
}

-(UIButton *)submmitBtn{
    if (!_submmitBtn) {
        _submmitBtn=[[UIButton alloc] initWithFrame:CGRectMake(10, kHeight-kTopHeight-55, kWidth-20, 45)];
        _submmitBtn.titleLabel.font=[UIFont systemFontOfSize:18];
        _submmitBtn.layer.masksToBounds=YES;
        _submmitBtn.layer.cornerRadius=4;
        [_submmitBtn setBackgroundImage:[UIImage imageNamed:@"nav"] forState:UIControlStateNormal];
        [_submmitBtn setTitle:@"提交" forState:UIControlStateNormal];
        [_submmitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_submmitBtn addTarget:self action:@selector(submmitBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _submmitBtn;
}

-(UIButton *)choiceBtn{
    if (!_choiceBtn) {
        _choiceBtn=[[UIButton alloc] initWithFrame:CGRectMake(10, 5, kWidth-20, 45)];
        _choiceBtn.titleLabel.font=[UIFont systemFontOfSize:18];
        _choiceBtn.layer.masksToBounds=YES;
        _choiceBtn.layer.cornerRadius=4;
        _choiceBtn.backgroundColor = [UIColor whiteColor];
        _choiceBtn.layer.cornerRadius = 5;
        _choiceBtn.layer.masksToBounds = YES;
        [_choiceBtn setTitle:@"作业大棚：" forState:UIControlStateNormal];
        _choiceBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        _choiceBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        [_choiceBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_choiceBtn addTarget:self action:@selector(choiceBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _choiceBtn;
}

-(UIImageView *)xialaImgView {
    if (!_xialaImgView) {
        _xialaImgView = [[UIImageView alloc] initWithFrame:CGRectMake(kWidth - 50, 18.5, 15, 8)];
        _xialaImgView.image = [UIImage imageNamed:@"xiala"];
        _xialaImgView.userInteractionEnabled= YES;
    }
    return _xialaImgView;
}

-(UILabel *)placeholdLb {
    if (!_placeholdLb) {
        _placeholdLb = [[UILabel alloc] initWithFrame:CGRectMake(100, 0, kWidth-110, 45)];
        _placeholdLb.userInteractionEnabled= YES;
        _placeholdLb.text = @"请选择大棚";
        _placeholdLb.textColor = [UIColor lightGrayColor];
        _placeholdLb.font = [UIFont systemFontOfSize:16];
    }
    return _placeholdLb;
}

-(UITableView *)photoTableView{
    if (!_photoTableView) {
        _photoTableView =[[UITableView alloc] initWithFrame:CGRectMake(0, 45, kWidth, kHeight-kTopHeight-55) style:UITableViewStyleGrouped];
        _photoTableView.delegate=self;
        _photoTableView.dataSource=self;
        _photoTableView.rowHeight=(kWidth-20)/355*216+10;
        _photoTableView.backgroundColor=RGB(230, 230, 230);
        _photoTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        _photoTableView.tableHeaderView=[[UIView alloc] initWithFrame:CGRectZero];
    }
    return _photoTableView;
}


@end
