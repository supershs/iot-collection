//
//  AgriculturalTableViewController.m
//  suyuanAPP
//
//  Created by mc on 2019/9/29.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import "AgriculturalTableViewController.h"
#import "AgriculturalDetailTableViewController.h"
#import "AgriculturalTableViewCell.h"
#import "FertilizerTableViewCell.h"
#import "BreedTableViewCell.h"
#import "ProductionViewController.h"
#import "HomeCollectionViewController.h"
#import "ErrorView.h"
@interface AgriculturalTableViewController ()

@property (nonatomic,retain)NSMutableArray *dataSourceArr;
@property (nonatomic,copy)NSString *farmingCode;
@property (nonatomic,copy)NSString *sort;
@property (nonatomic ,assign) NSInteger currentPage;
@property (nonatomic ,assign) NSInteger pages;
@property (nonatomic, strong) ErrorView *errorView;

@end

@implementation AgriculturalTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    
    [self createNavAndClick];
    self.title=self.dict[@"name"];
    self.view.backgroundColor=RGB(240, 240, 240);
    self.tableView.frame=CGRectMake(0, 0, kWidth, kHeight-30);
    self.tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
	self.errorView = [[ErrorView alloc]initWithFrame:self.tableView.frame] ;
	[self.errorView setHidden: YES];
	[self.tableView addSubview:self.errorView];
	
	
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [self loadNewData];
        }];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [self loadMoreData];
    }];
    

}

-(void)backs{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)createNavAndClick{
    
    UIButton *Btns=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [Btns setImage:[UIImage imageNamed:@"back_arrow"] forState:UIControlStateNormal];
    [Btns addTarget:self action:@selector(backs) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backBtnItems=[[UIBarButtonItem alloc]initWithCustomView:Btns];
    self.navigationItem.leftBarButtonItem=backBtnItems;
    
    
    UIButton *Btn=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [Btn setImage:[UIImage imageNamed:@"add_item"] forState:UIControlStateNormal];
    [Btn addTarget:self action:@selector(addPro) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backBtnItem=[[UIBarButtonItem alloc]initWithCustomView:Btn];
    self.navigationItem.rightBarButtonItem=backBtnItem;
}

-(void)addPro{
    
    ProductionViewController * vc =GetSbVc(@"Main", @"ProductionViewController");
    vc.dicAddFarm =self.dict;
    vc.inputCategory=self.inputCategory;
    [self.navigationController pushViewController:vc animated:YES];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _currentPage = 1;
    [self requestClassfyScoreListIsHud:YES];
}

-(void)loadNewData{
    _currentPage = 1;
    [self.tableView.mj_footer resetNoMoreData];
    [self requestClassfyScoreListIsHud:NO];
}

-(void)loadMoreData{
    if (_currentPage < _pages) {
        _currentPage++;
        [self requestClassfyScoreListIsHud:NO];
    }else{
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }
}

// sort 传1 代表农事生产(其余则不传)
//farmingCode：100102 肥料，100103农药 100100种子
-(void)requestClassfyScoreListIsHud:(BOOL)isHud{
    
    if (isHud) {
        [SVProgressHUD show];
    }
    __weak __typeof(self)weakSelf = self;
//    etraceIndustry 所属行业(10007种植业；10008畜牧业；10009渔业；10010其他)**/
    NSMutableDictionary *dic =[[NSMutableDictionary alloc]initWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"Property List" ofType:@"plist"]];
    
    if ([self.dict[@"name"] isEqualToString:[[[dic objectForKey:[UserConfig shareConfig].user.traceIndustry] objectAtIndex:4] objectForKey:@"name"]]) {
        
        NSString *url;
        NSDictionary *dict;
        if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10007"]) {
            url=[NSString stringWithFormat:@"%@%@/%ld/%d",API_BASE_URL_STRING,ProductionFarmListPost,weakSelf.currentPage,10];
            dict=@{@"sort":@"0"};
        }else if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10008"]){
            url=[NSString stringWithFormat:@"%@%@/%ld/%d",API_BASE_URL_STRING,postAppProductionLivestockBreedList,weakSelf.currentPage,10];
            dict=@{@"sort":@"0",@"personId":@([UserConfig shareConfig].user.tracePeasantId)};
        }else if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10009"]){
            url=[NSString stringWithFormat:@"%@%@/%ld/%d",API_BASE_URL_STRING,ProductionFishbreedListPost,weakSelf.currentPage,10];
            dict=@{@"sort":@"0",@"personId":@([UserConfig shareConfig].user.tracePeasantId)};
        }
        
        [YQDataManager postDataHttpRequestWithUrl:url Params:dict modelClass:nil responseBlock:^(BOOL statue, YQBaseModel *item, id info) {
            if (statue) {
                weakSelf.pages=[info[@"data"][@"pages"] intValue];
                if (weakSelf.currentPage==1) {
                    [weakSelf.dataSourceArr removeAllObjects];
                }
                [weakSelf.dataSourceArr addObjectsFromArray:[detailModel mj_objectArrayWithKeyValuesArray:info[@"data"][@"records"]]];
                [weakSelf.tableView reloadData];
				if (weakSelf.dataSourceArr.count == 0 ) {
					[self.errorView setHidden: NO];
				}else{
					[self.errorView setHidden:YES];
				}
                [SVProgressHUD dismiss];
                [weakSelf.tableView.mj_header endRefreshing];
                [weakSelf.tableView.mj_footer endRefreshing];
            }
        }];
        
    }else{
        
        NSString *url;
        NSDictionary *dict;
        if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10007"]) {
            url=[NSString stringWithFormat:@"%@%@/%ld/%d",API_BASE_URL_STRING,Production_FarmDetail_Post,weakSelf.currentPage,10];
            dict=@{@"farmingCode":self.dict[@"farmingCode"]};
        }else if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10008"]){
            url=[NSString stringWithFormat:@"%@%@/%ld/%d",API_BASE_URL_STRING,appProductionLivestockInputlistPOST,weakSelf.currentPage,10];
            dict=@{@"breedCode":self.dict[@"farmingCode"],@"personId":@([UserConfig shareConfig].user.tracePeasantId)};
        }else if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10009"]){
            url=[NSString stringWithFormat:@"%@%@/%ld/%d",API_BASE_URL_STRING,Production_FishInput_Post,weakSelf.currentPage,10];
            dict=@{@"fishbreedCode":self.dict[@"farmingCode"],@"personId":@([UserConfig shareConfig].user.tracePeasantId)};
        }
        
        [YQDataManager postDataHttpRequestWithUrl:url Params:dict modelClass:nil responseBlock:^(BOOL statue, YQBaseModel *item, id info) {
            if (statue) {
                weakSelf.pages=[info[@"data"][@"pages"] intValue];
                if (weakSelf.currentPage==1) {
                    [weakSelf.dataSourceArr removeAllObjects];
                }
                [weakSelf.dataSourceArr addObjectsFromArray:[ProductionInputModel mj_objectArrayWithKeyValuesArray:info[@"data"][@"records"]]];
                [weakSelf.tableView reloadData];
				if (weakSelf.dataSourceArr.count == 0 ) {
					[self.errorView setHidden: NO];
				}else{
					[self.errorView setHidden:YES];
				}
			
                [SVProgressHUD dismiss];
                [weakSelf.tableView.mj_header endRefreshing];
                [weakSelf.tableView.mj_footer endRefreshing];
            }

        }];
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 150;
}
#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourceArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSMutableDictionary *dic =[[NSMutableDictionary alloc]initWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"Property List" ofType:@"plist"]];
    if ([self.dict[@"name"] isEqualToString:[[[dic objectForKey:[UserConfig shareConfig].user.traceIndustry] objectAtIndex:4] objectForKey:@"name"]]) {
        static NSString *CellIdentifier = @"AgriculturalTableViewCell";
        //改为以下的方法
        AgriculturalTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        if (cell==nil) {
            cell=[[AgriculturalTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        cell.backgroundColor=RGB(240, 240, 240);
        if (self.dataSourceArr.count) {
            [cell inserDataCells:self.dataSourceArr[indexPath.row]];
        }
        return cell;
        
    }else{
        
        static NSString *CellIdentifier = @"FertilizerTableViewCell";
        //改为以下的方法
        FertilizerTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        if (cell==nil) {
            cell=[[FertilizerTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        cell.backgroundColor=RGB(240, 240, 240);
        if (self.dataSourceArr.count) {
            [cell inserDataCell:self.dict[@"farmingCode"] andModel:self.dataSourceArr[indexPath.row]];
        }
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    AgriculturalDetailTableViewController *detail=[[AgriculturalDetailTableViewController alloc] init];
    if (self.dataSourceArr.count) {
        detail.type=self.dict[@"farmingCode"];
        detail.title=[NSString stringWithFormat:@"%@详情",self.dict[@"name"]];
        detail.ids=[self.dataSourceArr[indexPath.row] ids];
        [self.navigationController pushViewController:detail animated:YES];
    }    
}


-(NSMutableArray *)dataSourceArr{
    if (!_dataSourceArr) {
        _dataSourceArr=[NSMutableArray new];
    }
    return _dataSourceArr;
}


@end
