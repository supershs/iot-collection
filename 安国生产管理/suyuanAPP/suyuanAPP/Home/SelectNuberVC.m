//
//  SelectNuberVC.m
//  suyuanAPP
//
//  Created by 杨琴 on 2019/10/9.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import "SelectNuberVC.h"
#import "SelectViewController.h"
#import "Model/ProductionModel.h"
#import "Cell/SelectOneTableViewCell.h"
#import "PhotoTableViewController.h"
#import "UpPhotoViewController.h"
#import <MOFSPickerView.h>
#import <MOFSPickerManager/MOFSPickerManager.h>
@interface SelectNuberVC ()<UITableViewDataSource,UITableViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *btnAdd;//添加按钮
@property (nonatomic,strong)NSMutableArray *aryData;//data数据
@property (nonatomic,strong)NSMutableArray *selectAry;// 网络请求数据
@property (nonatomic,strong)NSMutableDictionary *typeDic;//临时字典
@property (strong, nonatomic) UIImagePickerController *pickers;
@property (nonatomic,strong)NSArray *aryUnit;
@property (strong, nonatomic) IBOutlet UILabel *labContent;
@property (nonatomic,strong)AddManageModel *model;

@end

@implementation SelectNuberVC

- (void)viewDidLoad {
    [super viewDidLoad];
    NSMutableDictionary *dic =[[NSMutableDictionary alloc]initWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"Property List" ofType:@"plist"]];
 self.aryUnit =[NSMutableArray arrayWithArray:[[[dic objectForKey:[UserConfig shareConfig].user.traceIndustry]objectAtIndex:1]objectForKey:@"seledList"]];
    self.labContent.text =[NSString stringWithFormat:@"点击下拉选择投入品名称。如需要选择多个投入品名称，请点击下方的添加按钮，方可添加多个投入品名称。"];
    
    self.view.backgroundColor=UIColorFromHex(0xf2f2f2);
    
    self.btnAdd.layer.cornerRadius=3;
    
    self.selectAry =[NSMutableArray array];
    self.aryData =[NSMutableArray array];
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    
    [self postData];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"扫一扫" style:UIBarButtonItemStyleDone target:self action:@selector(btnPic)];
    self.navigationItem.rightBarButtonItem.tintColor=[UIColor whiteColor];
    

}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

-(void)btnPic{
    
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *CamaroAction = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            self.pickers.sourceType = UIImagePickerControllerSourceTypeCamera;
            self.navigationController.modalPresentationStyle=UIModalPresentationFullScreen;
            [self presentViewController:self.pickers animated:YES completion:nil];
        }
    }];
    UIAlertAction* photoAction = [UIAlertAction actionWithTitle:@"从相册选取" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        self.pickers.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        self.navigationController.modalPresentationStyle=UIModalPresentationFullScreen;
        [self presentViewController:self.pickers animated:YES completion:nil];
    }];
    UIAlertAction *cancelAction=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:CamaroAction];
    [alert addAction:photoAction];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info{
    UIImage *image = info[UIImagePickerControllerEditedImage];//获取图片
    [self uploadImage:image];
    [picker dismissViewControllerAnimated:YES completion:nil];//获取图片后返回
}
//按取消按钮时候的功能
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}
-(void)uploadImage:(UIImage *)image{
    
    
    //图片上传识别接口
	[YQDataManager postUpdataHttpRequestWithUrl:pictureKnowURl params:nil images:[[NSArray alloc] initWithObjects:image, nil] name:@"images" mimeType:@"jpg/png/jpeg" fileName:@"fileName" modelClass:[YQBaseModel class] responseBlock:^(BOOL statue, YQBaseModel *item, id info) {

		if (statue) {
			
			self.model=nil;
			
			if ([item.data[@"detection"][@"comprehensiveType"] isEqualToString:@"1"]&&[self.inputCategory isEqualToString:@"10026"]) {//comprehensiveType（1农药，2肥料，3种子）
				self.model = [AddManageModel mj_objectWithKeyValues:item.data];
				[self postData];
			}else if ([item.data[@"detection"][@"comprehensiveType"] isEqualToString:@"2"]&&[self.inputCategory isEqualToString:@"10027"]){
				self.model = [AddManageModel mj_objectWithKeyValues:item.data];
				[self postData];
			}else if ([item.data[@"detection"][@"comprehensiveType"] isEqualToString:@"3"]&&[self.inputCategory isEqualToString:@"10028"]){
				self.model = [AddManageModel mj_objectWithKeyValues:item.data];
				[self postData];
			}
			[SVProgressHUD showSuccessWithStatus:[NSString stringWithFormat:@"识别成功,%@",item.data[@"inputName"]]];
			[SVProgressHUD dismissWithDelay:2.0];
			
		}else{
			
			[SVProgressHUD showWithStatus:item.message];
			[SVProgressHUD dismissWithDelay:1.5];
		}

	}];
    
}

//提交

- (IBAction)CommitPush:(id)sender {
    
    NSMutableArray *aryTemp=[NSMutableArray array];
    
    if ([self.selectAry count]==0) {

        [SVProgressHUD showErrorWithStatus:@"没有投入品可添加"];
        
        return;
        
    }
    for (NSDictionary *dic in self.aryData) {
        
        if ([Utils valString:[dic objectForKey:@"tfNumber"]] ) {
        
            
            [SVProgressHUD showErrorWithStatus:@"请输入数量"];
            
            [SVProgressHUD dismissWithDelay:1.0];
            
            return;
            
        }else{
            AddManageModel*model= [AddManageModel mj_objectWithKeyValues:[dic objectForKey:@"model"]];
            
            NSDictionary *dicTemp =@{@"id": model.dataIdentifier,@"inputUnit":[dic objectForKey:@"inputUnit"],@"inputConsum":[dic objectForKey:@"tfNumber"]};
            
            [aryTemp addObject:dicTemp];
            
            
        }
    }
    
//    PhotoTableViewController *photo =GetSbVc(@"Main", @"PhotoTableViewController");

    UpPhotoViewController *photo=[[UpPhotoViewController alloc] init];
	photo.hasArea = YES;
    photo.title=@"请拍照上传";
	[self.dicAddFarm setObject:[Utils arrayToJSONString:[aryTemp copy]] forKey:@"product"];
    photo.inputCategory=self.inputCategory;
    photo.dicAddFarm=self.dicAddFarm;
    
    [self.navigationController pushViewController:photo animated:YES];
    
    
}
//数据请求
-(void)postData{
    
    
    
    [self.aryData removeAllObjects];
    
     NSMutableString *ApiUrl=[NSMutableString stringWithString:API_BASE_URL_STRING] ;
     
     if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10007"]) {
         
         [ApiUrl appendFormat:ProductionInputPost];
         
     }else if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10008"]){
         [ApiUrl appendFormat:ProductionLivestockInputPost];
         
     }else if([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10009"]){
         [ApiUrl appendFormat:ProductionFishbreedInputPost];
         
         
     }else{
         
     }
    
    
    [YQDataManager postDataJsonRequestWithUrl:[ApiUrl copy] Params:@{@"status": @"10018",@"inputCategory":self.inputCategory} modelClass:[YQBaseModel class] responseBlock:^(BOOL statue, YQBaseModel *item, id info) {

        if (statue) {

            YQBaseModel *mode=info;

            self.selectAry=[AddManageModel mj_objectArrayWithKeyValuesArray:mode.data];
            
            
            if ([self.selectAry count]>0) {
                //第一次赋值
                NSDictionary *adModel;
                
                if (self.model) {
                     adModel=self.model.mj_keyValues;
                    self.typeDic =[NSMutableDictionary dictionary];
                    [self.typeDic setObject:adModel forKey:@"model"];
                    [self.typeDic setObject:@"" forKey:@"tfNumber"];
                }else{
                    adModel=[[self.selectAry objectAtIndex:0] mj_keyValues];
                    self.typeDic =[NSMutableDictionary dictionary];
                    [self.typeDic setObject:adModel forKey:@"model"];
                    [self.typeDic setObject:@"" forKey:@"tfNumber"];
                }
                
                for (NSDictionary *dicUnit in self.aryUnit) {
                    
                    if ([[dicUnit objectForKey:@"typeCode"] isEqualToString:[adModel objectForKey:@"inputUnit"]]) {
                        [self.typeDic setObject:[dicUnit objectForKey:@"typeName"] forKey:@"inputName"];
                        [self.typeDic setObject:[dicUnit objectForKey:@"typeCode"] forKey:@"inputUnit"];

                    }
                }
                [self.aryData addObject:self.typeDic];
                
            }
            
            [self.tableView reloadData];

            [SVProgressHUD dismiss];
            
            

        }else{

            [SVProgressHUD showErrorWithStatus:@"网络错误"];

        }



    }];

}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    
    return [_aryData count];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
 
        
        SelectOneTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"SelectOneTableViewCell"];

    
    if ([_aryData count]-1 == indexPath.section) {
          [cell setModel:[AddManageModel mj_objectWithKeyValues:[[self.aryData objectAtIndex:indexPath.section]objectForKey:@"model"]] tfNumber:[[self.aryData objectAtIndex:indexPath.section]objectForKey:@"tfNumber"] typeName:[[self.aryData objectAtIndex:indexPath.section] objectForKey:@"inputName" ] typeIndex:0];
    }else{
        
        [cell setModel:[AddManageModel mj_objectWithKeyValues:[[self.aryData objectAtIndex:indexPath.section]objectForKey:@"model"]] tfNumber:[[self.aryData objectAtIndex:indexPath.section]objectForKey:@"tfNumber"] typeName:[[self.aryData objectAtIndex:indexPath.section] objectForKey:@"inputName" ] typeIndex:1];

    }
  
    
    
    cell.seletView = ^(UIView * _Nonnull viewTap) {
        
        if (viewTap.tag ==10000) {
        
            if ([self.aryData count]==0) {
                
                [SVProgressHUD showErrorWithStatus:@"没有更多的了"];
                
                return ;
            }
            
            [[MOFSPickerManager shareManger]showPickerViewWithCustomDataArray:[self.selectAry copy] keyMapper:@"inputName" title:nil cancelTitle:@"取消" commitTitle:@"确定" commitBlock:^(id model) {
                
                
                
                NSDictionary *dic =[self.aryData objectAtIndex:indexPath.section];
                
                    [dic setValue:[model mj_keyValues] forKey:@"model"];
                    
                for (NSDictionary *dicUnit in self.aryUnit) {
                            
                            if ([[dicUnit objectForKey:@"typeCode"] isEqualToString:[[dic objectForKey:@"model"] objectForKey:@"inputUnit"]]) {
                                
                                [self.typeDic setObject:[dicUnit objectForKey:@"typeName"] forKey:@"inputName"];
                                [self.typeDic setObject:[dicUnit objectForKey:@"typeCode"] forKey:@"inputUnit"];

                            }
                        }
                
                
                    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
                
                NSLog(@"model==%@",model);
                
            } cancelBlock:^{
                
            }];
             
        }else{
            
            [[MOFSPickerManager shareManger]showPickerViewWithCustomDataArray:self.aryUnit keyMapper:@"typeName" title:nil cancelTitle:@"取消" commitTitle:@"确定" commitBlock:^(id model) {
                
                [[self.aryData objectAtIndex:indexPath.section] setObject:[model objectForKey:@"typeCode"] forKey:@"inputUnit"];
                [[self.aryData objectAtIndex:indexPath.section] setObject:[model objectForKey:@"typeName"] forKey:@"inputName"];

                [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];

                
                NSLog(@"model==%@",model);
                
            } cancelBlock:^{
                
            }];
            
            
        }
//        [self goModelSelet:indexPath];
        
    };
    
    cell.tfNumberBlock = ^(UITextField * _Nonnull tf){
        
        
        [[self.aryData objectAtIndex:indexPath.section] objectForKey:@"tfNumber"];
        
        
      NSDictionary *dic=[self.aryData objectAtIndex:indexPath.section];
        
            [dic setValue:tf.text forKey:@"tfNumber"];
        

        
    };
    
    cell.btnDelet = ^(UIButton * _Nonnull btn) {
        
        
        [self.aryData removeObjectAtIndex:indexPath.section];
        
        [self.tableView reloadData];
        
        
    };
    
    
    
    
        return cell;
   
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == [self.aryData count]-1) {
        
        return 178;
    }
    
    return 214;
    
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
 
 
}


//添加项
- (IBAction)btnAdd:(UIButton *)sender {
    
    
  
    if ([self.selectAry count]>0) {
        NSDictionary *adModel =[[self.selectAry objectAtIndex:0] mj_keyValues];

            self.typeDic =[NSMutableDictionary dictionary];
            
            [self.typeDic setObject:adModel forKey:@"model"];
            [self.typeDic setObject:@"" forKey:@"tfNumber"];
        
        for (NSDictionary *dicUnit in self.aryUnit) {
                    
                    if ([[dicUnit objectForKey:@"typeCode"] isEqualToString:[adModel objectForKey:@"inputUnit"]]) {
                        [self.typeDic setObject:[dicUnit objectForKey:@"typeName"] forKey:@"inputName"];
                        [self.typeDic setObject:[dicUnit objectForKey:@"typeCode"] forKey:@"inputUnit"];

                    }
                }
        
        [self.aryData addObject:_typeDic];
        
        
        
    }else{
        
        [SVProgressHUD showErrorWithStatus:@"没有投入品可添加"];
        
    }
    

    
    
    [self.tableView reloadData];
    
    
    
    
}

- (UIImagePickerController *)pickers{
    if (!_pickers) {
        _pickers = [[UIImagePickerController alloc]init];
        _pickers.delegate = self;
        _pickers.allowsEditing=YES;
    }
    return _pickers;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
