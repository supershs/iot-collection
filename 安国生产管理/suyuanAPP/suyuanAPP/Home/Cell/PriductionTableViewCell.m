//
//  PriductionTableViewCell.m
//  suyuanAPP
//
//  Created by 杨琴 on 2019/10/14.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import "PriductionTableViewCell.h"
#import "ProductionModel.h"

@interface PriductionTableViewCell ()
@property (strong, nonatomic) IBOutlet UILabel *labNumber;//编号
@property (strong, nonatomic) IBOutlet UILabel *labName;//名称
@property (strong, nonatomic) IBOutlet UILabel *labCont;//批次
@property (strong, nonatomic) IBOutlet UILabel *labTime;
@property (strong, nonatomic) IBOutlet UILabel *labPreson;//负责人

@property (strong, nonatomic) IBOutlet UIView *style;

@end
@implementation PriductionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    //Base style for 矩形 4 拷贝 3
    self.style.layer.cornerRadius = 3;
    self.style.layer.shadowColor = UIColorFromHex(0xe3e3e3).CGColor;
    
    self.style.layer.shadowOffset = CGSizeMake(0,4);
    self.style.layer.shadowOpacity = 0.4;
    self.style.layer.shadowRadius=3;
    self.style.layer.masksToBounds = YES;
    self.style.clipsToBounds = NO;


}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setModel:(ProductionModel *)model{
    if (model) {
        
        self.labNumber.text =[NSString stringWithFormat:@"%.f",model.archiNumber];
        self.labName.text =[NSString stringWithFormat:@"%@",[Utils stringTurnString:model.productName]];
        self.labCont.text=[NSString stringWithFormat:@"%.f",model.archiBatch];
        self.labTime.text =[NSString stringWithFormat:@"%@",[Utils stringTurnString:model.productDate]];
        self.labPreson.text=[NSString stringWithFormat:@"%@",[Utils stringTurnString:model.personCharge]];
        

    }
    
    
}
@end
