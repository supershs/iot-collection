//
//  PriductionTableViewCell.h
//  suyuanAPP
//
//  Created by 杨琴 on 2019/10/14.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class ProductionModel;

@interface PriductionTableViewCell : UITableViewCell

@property (nonatomic,strong)ProductionModel *model;
@end

NS_ASSUME_NONNULL_END
