//
//  AddHomeCell.m
//  suyuanAPP
//
//  Created by 杨琴 on 2019/10/8.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import "AddHomeCell.h"

@interface AddHomeCell ()

@property (strong, nonatomic) IBOutlet UIImageView *bgImg;
@property (strong, nonatomic) IBOutlet UILabel *labTitle;

@end

@implementation AddHomeCell

- (void)awakeFromNib {
  
    
    self.bgImg .layer.borderWidth=1;
    
    self.bgImg.layer.borderColor=(__bridge CGColorRef _Nullable)([UIColor clearColor]);
    self.bgImg.layer.cornerRadius=3;
    [super awakeFromNib];
    
}

-(void)setTitle:(NSString *)strTitle index:(NSIndexPath *)indexPath{
    self.labTitle.text =strTitle;
    
    self.bgImg.image =[UIImage imageNamed:[NSString stringWithFormat:@"%ld",indexPath.item]];
    
}
@end
