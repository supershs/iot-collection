//
//  AddManagementCell.m
//  suyuanAPP
//
//  Created by 杨琴 on 2019/10/8.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import "AddManagementCell.h"

@interface AddManagementCell ()
@property (strong, nonatomic) IBOutlet UIView *bgView;//背景view
@property (strong, nonatomic) IBOutlet UILabel *labContent;//内容
@property (strong, nonatomic) IBOutlet UIImageView *iconImg;//图标


@end

@implementation AddManagementCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    

//        self.bgView.layer.cornerRadius = 3;
//        _bgView.backgroundColor=[UIColor whiteColor];
//        _bgView.layer.shadowColor = RGB(170, 170, 170).CGColor;
//        _bgView.layer.shadowOffset = CGSizeMake(4, 6);
//        _bgView.layer.shadowOpacity = 0.5;
//        _bgView.layer.shadowRadius = 5;
    
    self.bgView.layer.cornerRadius = 3;
    self.bgView.layer.shadowColor = UIColorFromHex(0xe3e3e3).CGColor;
    
    self.bgView.layer.shadowOffset = CGSizeMake(0,4);
    self.bgView.layer.shadowOpacity = 0.4;
    self.bgView.layer.shadowRadius=3;
    self.bgView.layer.masksToBounds = YES;
    self.bgView.clipsToBounds = NO;
}


-(void)setModel:(NSDictionary *)model{
    
    if (model) {
        
        self.labContent.text =[model objectForKey:@"contentName"];
        self.iconImg.image = [UIImage imageNamed:[model objectForKey:@"imgName"]];
        
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
