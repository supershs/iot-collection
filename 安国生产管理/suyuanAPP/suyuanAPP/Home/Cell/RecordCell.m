//
//  RecordCell.m
//  suyuanAPP
//
//  Created by 杨琴 on 2019/10/8.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import "RecordCell.h"


@interface RecordCell()




@property (strong, nonatomic) IBOutlet UIView *labBg;
@property (strong, nonatomic) IBOutlet UIImageView *iconImg;
@property (strong, nonatomic) IBOutlet UILabel *labTitle;


@end


@implementation RecordCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self createInfoCell];
    
    // Initialization code
}



-(void)createInfoCell{

        //Base style for 矩形 6 拷贝 2
        self.labBg.layer.cornerRadius = 3;
        self.labBg.layer.borderColor = [[UIColor colorWithRed:66.0f/255.0f green:205.0f/255.0f blue:94.0f/255.0f alpha:1.0f] CGColor];
        self.labBg.layer.borderWidth = 1;
        self.labBg.layer.backgroundColor = [[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0f] CGColor];
        self.labBg.alpha = 1;

}


-(void)setTitle:(NSString *)strTitle ImgName:(NSString *)imgName{
    self.iconImg.image =[UIImage imageNamed:imgName];
    self.labTitle.text =[Utils stringTurnString:strTitle];
    

}



@end
