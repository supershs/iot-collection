//
//  SelectOneTableViewCell.h
//  suyuanAPP
//
//  Created by 杨琴 on 2019/10/9.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//选择列表

#import <UIKit/UIKit.h>

@class AddManageModel;




NS_ASSUME_NONNULL_BEGIN

@interface SelectOneTableViewCell : UITableViewCell
@property (copy, nonatomic) void (^seletView)(UIView *viewTap);
@property (nonatomic,strong)AddManageModel *model;

@property (copy, nonatomic) void (^btnDelet)(UIButton *btn);


@property (copy, nonatomic) void (^tfNumberBlock)(UITextField *tf);

-(void)setModel:(AddManageModel *)model tfNumber:(NSString *)tfNumber typeName:(NSString *) typeName typeIndex:(NSInteger)typeIndex;


@end



NS_ASSUME_NONNULL_END
