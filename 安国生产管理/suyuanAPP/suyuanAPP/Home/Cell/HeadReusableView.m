//
//  HeadReusableView.m
//  suyuanAPP
//
//  Created by 杨琴 on 2019/10/8.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import "HeadReusableView.h"
#import "HomeModel.h"
@interface HeadReusableView ()
@property (strong, nonatomic) IBOutlet UILabel *labName;//用户名
@property (strong, nonatomic) IBOutlet UIButton *btnPhoto;

@property (strong, nonatomic) IBOutlet UIView *bgView;
@property (strong, nonatomic) IBOutlet UILabel *labBaseNum;//基地名
@property (strong, nonatomic) IBOutlet UILabel *labNowPlot;//z农事生产次数
@property (strong, nonatomic) IBOutlet UILabel *labVarie;//投入品次数
@property (strong, nonatomic) IBOutlet UILabel *labProduction;//c生产次数
@property (strong, nonatomic) IBOutlet UILabel *labIputNuben;//投入品次数

@property (strong, nonatomic) IBOutlet UILabel *labInteger;//积分


@end
@implementation HeadReusableView

-(void)setModel:(HomeModel *)model{
    
    self.labName.text =[Utils stringTurnString:model.personName];
    self.labBaseNum.text =[Utils stringTurnString:model.enterName];
    self.labNowPlot.text =[NSString stringWithFormat:@"%@次",[Utils stringTurnNumber:model.framCount]];
    self.labVarie.text =[NSString stringWithFormat:@"%@次",[Utils stringTurnNumber:model.productCount]];
    if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10008"]) {
        self.labNowPlot.text =[NSString stringWithFormat:@"%@次",[Utils stringTurnNumber:model.breedCount]];

    }else if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10009"]){
                self.labNowPlot.text =[NSString stringWithFormat:@"%@次",[Utils stringTurnNumber:model.fishCount]];

    }

    
}

-(void)awakeFromNib{
    
    //Base style for 圆角矩形 1
    self.bgView.layer.cornerRadius = 3;
  
    
    [super awakeFromNib];
    
}

@end
