//
//  PhotoTableViewCell.m
//  suyuanAPP
//
//  Created by 杨琴 on 2019/10/11.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import "PhotoTableViewCell.h"

@interface PhotoTableViewCell ()

@property (strong, nonatomic) IBOutlet UIImageView *img;
@property (strong, nonatomic) IBOutlet UIButton *btn;

@end


@implementation PhotoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setModel:(UIImage *)model imgType:(NSString *)type{
 
    
    NSLog(@"%@",type);
    
    if ([Utils valString:type]) {
        
        [self.img setImage:model];
        _btn.hidden=NO;
        
    }else{
        
        _btn.hidden=YES;
        self.img.image =[UIImage imageNamed:@"添加图片"];
        
        
    }
        
        
    
}
- (IBAction)btnDelect:(UIButton *)sender {
    
    self.deletPhoto(sender);
    
    
    
    
    
}
@end
