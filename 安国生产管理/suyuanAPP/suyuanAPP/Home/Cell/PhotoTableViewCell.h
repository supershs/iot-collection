//
//  PhotoTableViewCell.h
//  suyuanAPP
//
//  Created by 杨琴 on 2019/10/11.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


@interface PhotoTableViewCell : UITableViewCell

@property (copy, nonatomic) void (^deletPhoto)(UIButton *btn);


-(void)setModel:(UIImage *)model imgType:(NSString *)type;
@end

NS_ASSUME_NONNULL_END
