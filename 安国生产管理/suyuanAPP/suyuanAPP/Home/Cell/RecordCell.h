//
//  RecordCell.h
//  suyuanAPP
//
//  Created by 杨琴 on 2019/10/8.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RecordCell : UICollectionViewCell

-(void)setTitle:(NSString *)strTitle ImgName:(NSString *)imgName;

@end

NS_ASSUME_NONNULL_END
