//
//  SelectOneTableViewCell.m
//  suyuanAPP
//
//  Created by 杨琴 on 2019/10/9.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import "SelectOneTableViewCell.h"
#import "ProductionModel.h"

@interface SelectOneTableViewCell ()<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UIView *bgView;
@property (strong, nonatomic) IBOutlet UILabel *labNuber;//投入品名称
@property (strong, nonatomic) IBOutlet UIImageView *OpenImg;
@property (strong, nonatomic) IBOutlet UITextField *tfNumber;// 投入品数量

@property (strong, nonatomic) IBOutlet UILabel *labUnit;//投入品单位
@property (strong, nonatomic) IBOutlet UIView *unitView;
@property (strong, nonatomic) IBOutlet UIButton *btnDelect;

@end


@implementation SelectOneTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.bgView.layer.cornerRadius=3;
    _tfNumber.delegate=self;
    
//    [_tfNumber addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];

    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapView:)];
    

    self.bgView.tag=10000;
    
    
    [ self.bgView addGestureRecognizer:tapGesture];

    UITapGestureRecognizer * tapUtil = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapView:)];
    self.unitView.tag=10001;
    
    [ self.unitView addGestureRecognizer:tapUtil];

   
}

-(void)setModel:(AddManageModel *)model tfNumber:(NSString *)tfNumber typeName:(NSString *) typeName typeIndex:(NSInteger)typeIndex{

    if (model) {
        
    
        if (typeIndex==0) {
            self.btnDelect.frame =CGRectMake(0, 0, 0, 0);
            self.btnDelect.hidden=YES;
            
        }else{
            self.btnDelect.hidden=NO;

            
        }
        _tfNumber.placeholder =[NSString stringWithFormat:@"请输入%@使用量",[Utils stringTurnString:model.inputName]];
        
        
      _labNuber.text =[Utils stringTurnString:model.inputName];
      _tfNumber.text =[Utils stringTurnString:tfNumber];
        _labUnit.text=[Utils stringTurnString:typeName];
        
    }
  
    
    
}

-(void)tapView:(UITapGestureRecognizer *)tapView{
    
    NSLog(@"点击了");
   
    
  
    
    self.seletView(tapView.view);
    
    
    
}

//删除按钮
- (IBAction)DelegateClick:(UIButton *)sender {
    
    
    
    self.btnDelet(sender);
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    // 使contentTextField聚焦变成第一响应者
    [self.tfNumber becomeFirstResponder];

}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField
{

    
    self.tfNumberBlock(textField);
    

}

//-(void)textFieldDidChange:(UITextField *)textField{
//
//
//}
//


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

