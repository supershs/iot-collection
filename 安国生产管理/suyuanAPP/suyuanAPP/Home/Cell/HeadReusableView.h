//
//  HeadReusableView.h
//  suyuanAPP
//
//  Created by 杨琴 on 2019/10/8.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class HomeModel;

@interface HeadReusableView : UICollectionReusableView
@property (nonatomic,strong)HomeModel *model;

@end

NS_ASSUME_NONNULL_END
