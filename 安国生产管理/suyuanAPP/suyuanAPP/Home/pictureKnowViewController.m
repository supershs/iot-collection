//
//  pictureKnowViewController.m
//  CompanyAPP_Obj
//
//  Created by mc on 2019/11/28.
//  Copyright © 2019 YQ. All rights reserved.
//

#import "pictureKnowViewController.h"

@interface pictureKnowViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic,retain)UIImageView *PhotosImageViews;

@property (nonatomic,retain)UILabel *titleLabel;

@property (nonatomic,retain)UIImageView *resultImages;

@property (nonatomic,retain)UILabel *resultLabel;


@end

@implementation pictureKnowViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"OCR识别结果";
    self.view.backgroundColor=RGB(230, 230, 230);
    [self createUI];
    
}

-(void)createUI{
    
    [self.view addSubview:self.PhotosImageViews];
    [self.view addSubview:self.titleLabel];
    [self.view addSubview:self.resultImages];
    [self.view addSubview:self.resultLabel];
    self.titleLabel.text=[NSString stringWithFormat:@"识别结果：%@-%@",self.strName,self.strType];
    self.resultLabel.text=self.strResult;
    NSString *strUrl=[NSString stringWithFormat:@"%@%@",API_Image_URL,self.strImageUrl];
    [self.PhotosImageViews sd_setImageWithURL:[NSURL URLWithString:strUrl]];
    
    
}


-(UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(_PhotosImageViews.left, _PhotosImageViews.bottom+10, _PhotosImageViews.width-85, 20)];
        _titleLabel.textColor=RGB(38, 38, 38);
        _titleLabel.font=[UIFont systemFontOfSize:15];
    }
    return _titleLabel;
}

-(UIImageView *)resultImages{
    if (!_resultImages) {
        _resultImages=[[UIImageView alloc] initWithFrame:CGRectMake(_titleLabel.right+5, _titleLabel.y, 20, 20)];
        [_resultImages setImage:[UIImage imageNamed:@"result"]];
    }
    return _resultImages;
}

-(UILabel *)resultLabel{
    if (!_resultLabel) {
        _resultLabel=[[UILabel alloc] initWithFrame:CGRectMake(_resultImages.right, _titleLabel.y, 70, 20)];
        _resultLabel.textColor=RGB(64,205,94);
        _resultLabel.textAlignment=NSTextAlignmentCenter;
        _resultLabel.font=[UIFont systemFontOfSize:15];
    }
    return _resultLabel;
}

-(UIImageView *)PhotosImageViews{
    
    if (!_PhotosImageViews) {
        _PhotosImageViews=[[UIImageView alloc] initWithFrame:CGRectMake(10, 10, kWidth-20, (kWidth-20)/355*216)];
        _PhotosImageViews.contentMode=UIViewContentModeScaleAspectFill;
        _PhotosImageViews.backgroundColor=[UIColor whiteColor];
        _PhotosImageViews.userInteractionEnabled=YES;
        _PhotosImageViews.layer.masksToBounds=YES;
        _PhotosImageViews.layer.cornerRadius=5;
        _PhotosImageViews.layer.shadowColor = RGB(170, 170, 170).CGColor;
        _PhotosImageViews.layer.shadowOffset = CGSizeMake(4, 6);
        _PhotosImageViews.layer.shadowOpacity = 0.5;
        _PhotosImageViews.layer.shadowRadius = 5;
    }
    return _PhotosImageViews;
}

@end
