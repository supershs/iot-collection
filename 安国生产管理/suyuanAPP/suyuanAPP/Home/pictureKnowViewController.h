//
//  pictureKnowViewController.h
//  CompanyAPP_Obj
//
//  Created by mc on 2019/11/28.
//  Copyright © 2019 YQ. All rights reserved.
//

#import "YQBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface pictureKnowViewController : YQBaseViewController

@property (nonatomic,copy)NSString *strImageUrl;

@property (nonatomic,copy)NSString *strName;

@property (nonatomic,copy)NSString *strType;

@property (nonatomic,copy)NSString *strResult;

@end

NS_ASSUME_NONNULL_END
