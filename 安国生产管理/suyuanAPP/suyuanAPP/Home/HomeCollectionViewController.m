//
//  HomeCollectionViewController.m
//  suyuanAPP
//
//  Created by 杨琴 on 2019/10/8.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//首页


#import "HomeCollectionViewController.h"
#import "Cell/AddHomeCell.h"
#import "Cell/RecordCell.h"
#import "Cell/HeadReusableView.h"
#import "ProductionViewController.h"
#import "AgriculturalTableViewController.h"
#import "loginViewController.h"
#import "HomeModel.h"
#import "EZAddByQRCodeViewController.h"
@interface HomeCollectionViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *flowLayout;

@property (nonatomic,strong)NSMutableArray *aryData;
@property (nonatomic,strong)HomeModel *model;
@end
static NSString *HeaderID = @"headerID";

@implementation HomeCollectionViewController

static NSString * const addCell = @"AddHomeCell";
static NSString * const recoderCell = @"RecordCell";


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.title=[Utils title:[[UserConfig shareConfig].user.traceIndustry integerValue]];
    
    NSMutableDictionary *dic =[[NSMutableDictionary alloc]initWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"Property List" ofType:@"plist"]];
    self.aryData =[NSMutableArray arrayWithArray:[NSMutableArray arrayWithArray:[dic objectForKey:[UserConfig shareConfig].user.traceIndustry]]];
    
    self.navigationItem.rightBarButtonItem =[[UIBarButtonItem alloc] initWithTitle:@"退出" style:UIBarButtonItemStyleDone target:self action:@selector(btnExit)];
    self.navigationItem.rightBarButtonItem.tintColor=[UIColor whiteColor];
		self.navigationItem.hidesBackButton = YES;
    [self getHomeData];
	
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.collectionView .mj_header =[MJRefreshHeader headerWithRefreshingBlock:^{
        [self getHomeData];

    }];
	UIImage *bgImage = [[UIImage imageNamed:@"nav_other"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch];

	if (@available(iOS 13.0, *)) {
		UINavigationBarAppearance *barApp = [UINavigationBarAppearance new];
		[barApp setBackgroundImage:bgImage];
	self.navigationController.navigationBar.scrollEdgeAppearance = barApp;
	self.navigationController.navigationBar.standardAppearance = barApp;
	} else {
		// Fallback on earlier versions
	}
        
}

-(void)btnExit{
    
    [[UserConfig shareConfig] clearSignData];
    
    loginViewController *login =GetSbVc(@"Main", @"loginViewController");
    
    [self.navigationController pushViewController:login animated:YES];
    
}



-(void)getHomeData{
    
    [SVProgressHUD show];
    
    
    NSLog(@"%@__%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"loginName"],[[NSUserDefaults standardUserDefaults] objectForKey:@"passWord"]);
    
    
    NSMutableString *ApiUrl=[NSMutableString stringWithString:API_BASE_URL_STRING] ;
    
    if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10007"]) {
        
        [ApiUrl appendFormat:HomeDataUrl];
        
    }else if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10008"]){
        [ApiUrl appendFormat:HomeLivestockUrl];
        
    }else if([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10009"]){
        [ApiUrl appendFormat:HomeFishUrl];
        
        
    }else{
        
    }
    
    [YQDataManager getDataWithUrl:[ApiUrl copy] Params:nil modelClass:[HomeModel class] responseBlock:^(BOOL statue, YQBaseModel *item, id info) {
        if (statue) {
            
            
            self.model =info;
            
//            [self.collectionView.mj_header endRefreshing];
            [self.collectionView reloadData];
            
            [SVProgressHUD dismiss];
            
        }else{
            
        [SVProgressHUD showErrorWithStatus:item.message];
            
        }
        [self.collectionView.mj_header endRefreshing];

        [SVProgressHUD dismiss];
        [self.collectionView reloadData];

        
    }];
    
//    [SVProgressHUD dismiss];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.aryData count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.item <4) {
        
        AddHomeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:addCell forIndexPath:indexPath];
        
        [cell setTitle:[[self.aryData objectAtIndex:indexPath.item] objectForKey:@"name"] index:indexPath];
        
        return cell;
    }else{
        
        
        RecordCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:recoderCell forIndexPath:indexPath];
        [cell setTitle:[[self.aryData objectAtIndex:indexPath.item] objectForKey:@"name"] ImgName:[NSString stringWithFormat:@"%ld",indexPath.item]];
        
        // Configure the cell
        
        return cell;
    }
   
}

#pragma mark <UICollectionViewDelegate>
// 会自动的在每一组的头部和尾部加上这个视图
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
  
        HeadReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:HeaderID forIndexPath:indexPath];
    
    [headerView setModel:self.model];
    
        return headerView;
  
    
}


// 定义每个Cell的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((kWidth-30)/2, 80);
}



// 定义每个Section的四边间距
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    // UIEdgeInsets insets = {top, left, bottom, right};
    return UIEdgeInsetsMake(1, 10, 10, 10);
}

//退出登录

//- (IBAction)ExitCommit:(id)sender {
//    [[UserConfig shareConfig] clearSignData];
//
//    loginViewController *login =GetSbVc(@"Main", @"loginViewController");
//
//    [self.navigationController pushViewController:login animated:YES];
//
//
//}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.item>3) {
        
        AgriculturalTableViewController *Avc=[[AgriculturalTableViewController alloc] init];
        NSMutableDictionary *dic =[NSMutableDictionary dictionaryWithDictionary:@{@"farmingCode":[[self.aryData objectAtIndex:indexPath.item] objectForKey:@"farmingCode"],@"name":[[self.aryData objectAtIndex:indexPath.item] objectForKey:@"name"],@"product":@"",@"archiId":@"",@"taskPic":@""}];
        Avc.dict =dic;
        Avc.inputCategory=[[self.aryData objectAtIndex:indexPath.item] objectForKey:@"inputCategory"];
        [self.navigationController pushViewController:Avc animated:YES];
        
    }else{
        ProductionViewController * vc =GetSbVc(@"Main", @"ProductionViewController");
                
        NSMutableDictionary *dic =[NSMutableDictionary dictionaryWithDictionary:@{@"farmingCode":[[self.aryData objectAtIndex:indexPath.item] objectForKey:@"farmingCode"],@"product":@"",@"archiId":@"",@"taskPic":@""}];
        
            vc.dicAddFarm =dic;
        
        vc.inputCategory=[[self.aryData objectAtIndex:indexPath.item] objectForKey:@"inputCategory"];
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    NSLog(@"%ld",indexPath.item);
    
}



/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

@end
