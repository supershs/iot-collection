//
//  SelectViewController.h
//  suyuanAPP
//
//  Created by 杨琴 on 2019/10/17.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import "YQBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
@class AddManageModel;


@interface SelectViewController : YQBaseViewController

@property (copy, nonatomic) void (^AddManageBlock)( AddManageModel*model);
@property (nonatomic,strong)NSMutableArray *selectAry;


@end

NS_ASSUME_NONNULL_END
