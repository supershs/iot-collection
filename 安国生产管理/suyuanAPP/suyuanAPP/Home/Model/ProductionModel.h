//
//  ProductionModel.h
//  suyuanAPP
//
//  Created by 杨琴 on 2019/10/16.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import "YQDataManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProductionModel : YQDataBaseModel
@property (nonatomic, strong) NSString *testNumber;//监测次数
@property (nonatomic, assign) double productCode;//生产编码
@property (nonatomic, assign) double archiBatch;// 生产批次
@property (nonatomic, strong) NSString *enterName;//主体名称
@property (nonatomic, strong) NSString *personCharge;//负责人
@property (nonatomic, strong) NSString *uuid;
@property (nonatomic, strong) NSString *dataIdentifier;
@property (nonatomic, assign) double productNumber;//产品备案b号
@property (nonatomic, strong) NSString *productDate;//生产日期
@property (nonatomic, strong) NSString *tenantId;
@property (nonatomic, strong) NSString *enterNumber;//生产经营主体编号
@property (nonatomic, strong) NSString *productName;//农产品名称
@property (nonatomic, assign) double archiNumber;// 生产档案
@property (nonatomic, strong) NSString *quantity;//养殖数量(value="10018已启用，10019已停用")

@end


@interface AddManageModel : YQDataBaseModel

@property (nonatomic, strong) NSString *dataIdentifier;
@property (nonatomic, strong) NSString *uuid;
@property (nonatomic, strong) NSString *enterNumber;
@property (nonatomic, strong) NSString *inputName;
@property (nonatomic, strong) NSString *tenantId;
@property (nonatomic, strong) NSString *inputUnit;
@property (nonatomic, assign) NSInteger inputId;
@property (nonatomic, assign) double inputConsum;


@end


NS_ASSUME_NONNULL_END
