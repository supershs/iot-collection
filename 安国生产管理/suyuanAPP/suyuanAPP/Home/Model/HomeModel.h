//
//  HomeModel.h
//  suyuanAPP
//
//  Created by 杨琴 on 2019/10/23.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import "YQDataManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeModel : YQBaseModel

@property (nonatomic,strong)NSString *enterName;//所在基地名称
@property (nonatomic,strong)NSString *productCount;//投入品次数
@property (nonatomic,strong)NSString *personName;//z作业人员姓名
@property (nonatomic,strong)NSString *framCount;// 农事c生产i次数
@property (nonatomic,strong)NSString *breedCount;//畜禽生产次数
@property (nonatomic,strong)NSString *fishCount;//c渔业生产次数

@end

NS_ASSUME_NONNULL_END
