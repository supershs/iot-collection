//
//  AddManagementVC.m
//  suyuanAPP
//
//  Created by 杨琴 on 2019/10/8.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import "AddManagementVC.h"
#import "Cell/AddManagementCell.h"
#import "SelectNuberVC.h"
//#import "PhotoTableViewController.h"
#import "ProductionModel.h"
#import "ProductionViewController.h"
#import "UpPhotoViewController.h"
@interface AddManagementVC ()<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic,strong)NSMutableArray *aryData;

@end

@implementation AddManagementVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10007"]) {
        
        self.title =@"添加农事管理";
        
    }else if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10008"]){
        self.title =@"添加畜禽管理";

    }else if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10009"]){
        
        self.title =@"添加渔业管理";
        
    }
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    self.tableView.delegate=self;
    
    self.tableView.dataSource=self;

    
    NSMutableDictionary *dic =[[NSMutableDictionary alloc]initWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"Property List" ofType:@"plist"]];
    
    self.aryData =[NSMutableArray arrayWithArray:[[[dic objectForKey:[UserConfig shareConfig].user.traceIndustry]objectAtIndex:0]objectForKey:@"addList"]];


}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
}




-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [self.aryData count];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return  64;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    AddManagementCell *cell=[tableView dequeueReusableCellWithIdentifier:@"AddManagementCell"];
    
    cell.model =[_aryData objectAtIndex:indexPath.row];
    
    return cell;
    
    
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
        
    UpPhotoViewController *vc=[[UpPhotoViewController alloc] init];
    vc.title=@"请拍照上传";
	vc.hasArea = YES;
	[self.dicAddFarm setObject:[[self.aryData objectAtIndex:indexPath.row] objectForKey:@"farmingCode"] forKey:@"farmingCode"];
    vc.dicAddFarm =self.dicAddFarm;
    if (TARGET == 1) {
        vc.hasArea = false;
    } else if (TARGET == 2) {
        vc.hasArea = false;
    } else if (TARGET == 3) {
        vc.hasArea = true;
    } else if (TARGET == 4) {
        vc.hasArea = false;
    }
    
    vc.inputCategory=self.dicAddFarm[@"inputCategory"];
    [self.navigationController pushViewController:vc animated:YES];
   
}

//提交


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
