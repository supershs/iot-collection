//
//  ScanViewController.h
//  suyuanAPP
//
//  Created by 杨琴 on 2019/10/29.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//二维码扫描界面

#import "YQBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ScanViewController : YQBaseViewController

@end

NS_ASSUME_NONNULL_END
