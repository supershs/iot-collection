//
//  ProductionViewController.h
//  suyuanAPP
//
//  Created by 杨琴 on 2019/10/14.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//生产管理列表


#import "YQBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProductionViewController : YQBaseViewController


@property (nonatomic,strong)NSMutableDictionary *dicAddFarm;// 方便最后调用接口
@property (nonatomic,strong)NSString *inputCategory;
@end

NS_ASSUME_NONNULL_END
