//
//  PhotoTableViewController.m
//  suyuanAPP
//
//  Created by 杨琴 on 2019/10/11.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import "PhotoTableViewController.h"
#import "PhotoTableViewCell.h"
#import "PhotoModel.h"
#import "HXCustomNavigationController.h"
#import "HXPhotoPicker.h"
#import "submitSucessViewController.h"
@interface PhotoTableViewController ()<UITableViewDelegate,UITableViewDataSource,HXCustomNavigationControllerDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableview;

@property (nonatomic,strong)NSMutableArray *aryPhoto;//照片数组
@property (nonatomic,strong)HXPhotoManager *manager;
@property (nonatomic,strong)HXCustomNavigationController *nav;
@end

@implementation PhotoTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];
    
    self.tableview.delegate=self;
    self.tableview.dataSource=self;
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if ([_aryPhoto count]>2) {
        
        return [_aryPhoto count];
    }
    return [_aryPhoto count]+1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PhotoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PhotoTableViewCell" forIndexPath:indexPath];
   
    
    if (indexPath.row >[_aryPhoto count]-1 || [_aryPhoto count]==0) {
        
        [cell setModel:[UIImage new] imgType: @"1"];
        [self updateFocusIfNeeded];
        
    }else{
        HXPhotoModel *model =[self.aryPhoto objectAtIndex:indexPath.row];
        
        [cell setModel:model.thumbPhoto imgType: @""];

        cell.deletPhoto = ^(UIButton * _Nonnull btn) {
            
          NSLog(@"%@",[self.aryPhoto objectAtIndex:indexPath.row])  ;

            [self.manager afterSelectedListdeletePhotoModel:model];

            [self.aryPhoto removeObjectAtIndex:indexPath.row];
                        
            [self.tableview reloadData];
            
            
        };
        

    }
   
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // 照片选择控制器
//    self.modalPresentationStyle=UIModalPresentationFullScreen;
    
//    [self presentViewController:self.nav animated:YES completion:nil];
    
    
    [self hx_presentSelectPhotoControllerWithManager:self.manager didDone:^(NSArray<HXPhotoModel *> *allList, NSArray<HXPhotoModel *> *photoList, NSArray<HXPhotoModel *> *videoList, BOOL isOriginal, UIViewController *viewController, HXPhotoManager *manager) {
            NSSLog(@"block - all - %@",allList);
            NSSLog(@"block - photo - %@",photoList);
        

        for (HXPhotoModel *model in allList) {
            
            NSLog(@"____model=====%@",model.thumbPhoto);

        }

        self.aryPhoto =[NSMutableArray arrayWithArray:allList];
        
        [self.tableview reloadData];
        
    } cancel:^(UIViewController *viewController, HXPhotoManager *manager) {
            NSSLog(@"block - 取消了");
        }];
    
  
}
- (HXPhotoManager *)manager {
    if (!_manager) {
        _manager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhoto];
        _manager.configuration.deleteTemporaryPhoto = NO;
        _manager.configuration.maxNum=3;
        
    }
    
    return _manager;
}


//提交
- (IBAction)btnCommit:(id)sender {
    
    
    
    if ([self.aryPhoto count]==0) {
          
          [SVProgressHUD showErrorWithStatus:@"请选择图片"];
          
          [SVProgressHUD dismissWithDelay:1.0];
          
          
          return;
      }
          
          
     
      
    
    NSMutableArray  *ary=[NSMutableArray array];
    
    for (HXPhotoModel *model in self.aryPhoto) {
        
        
        [ary addObject:model.thumbPhoto];
        
        
    }
    [YQDataManager postUpdataHttpRequestWithUrl:[NSString stringWithFormat:@"%@",UrlFile] params:nil images:ary name:@"files" mimeType:@"jpg/png/jpeg" fileName:@"fileName" modelClass:[YQBaseModel class] responseBlock:^(BOOL statue, YQBaseModel *item, id info) {
        NSLog(@"%@____%@",info,item.message)
        
        if (statue) {
            
        
            [SVProgressHUD showWithStatus:@"请求成功"];

            [self getCommit:[Utils stringTurnString:item.message]];
            
            [SVProgressHUD dismissWithDelay:1.0];
            
        }else{
            
            [SVProgressHUD showErrorWithStatus:item.message];
            [SVProgressHUD dismissWithDelay:1.0];

        }
    }];
    
    
  
    
    
    [self.dicAddFarm setObject:@"" forKey:@"taskPic"];
    
 
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 214;
    
}

-(void)getCommit:(NSString *)taskPic{
    
    [self.dicAddFarm setObject:taskPic forKey:@"taskPic"];
    
    NSMutableString *ApiUrl =[NSMutableString stringWithString:API_BASE_URL_STRING];
    if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10007"]) {
        
        [ApiUrl appendString:ProductionAddPost];
        
    }else if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10008"]){
        [ApiUrl appendString:ProductionAddLivestockBreedPost];
    }else if ([[UserConfig shareConfig].user.traceIndustry isEqualToString:@"10009"]){
        [ApiUrl appendString:ProductionAddFishbreedPost];
    }else{
        
    }
    
    
      [YQDataManager postDataJsonRequestWithUrl:[ApiUrl copy] Params:[self.dicAddFarm copy] modelClass:[YQBaseModel class] responseBlock:^(BOOL statue, YQBaseModel *item, id info) {
          
          if (statue) {
              
              submitSucessViewController *subMit =[[submitSucessViewController alloc]init];
              
              subMit.title=@"提交成功";
              
              [self.navigationController pushViewController:subMit animated:YES];
          }
          
          
      }];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
