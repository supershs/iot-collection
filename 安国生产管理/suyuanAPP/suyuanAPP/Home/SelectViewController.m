//
//  SelectViewController.m
//  suyuanAPP
//
//  Created by 杨琴 on 2019/10/17.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import "SelectViewController.h"
#import "ProductionModel.h"

@interface SelectViewController ()<UITableViewDelegate,UITableViewDataSource>


@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong)NSMutableArray *aryData;
@end

@implementation SelectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    self.tableView.layer.cornerRadius=5;
    self.tableView.tableFooterView =[UIView new];
    

}




-(NSMutableArray *)aryData{
    
    if (!_aryData) {
        self.aryData =[NSMutableArray array];

        _aryData=self.selectAry;
    

    }
    
    return _aryData;
    
}



-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1 ;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [self.aryData  count];
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"sltCell"];
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"sltCell"];
        
    }
    AddManageModel *modle =[self.aryData objectAtIndex:indexPath.row];
    
    cell.textLabel.text =[Utils stringTurnString:modle.inputName];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return 44;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    AddManageModel *modle =[self.aryData objectAtIndex:indexPath.row];

    
    self.AddManageBlock(modle);
    
    
    [self  dismissViewControllerAnimated:YES completion:nil];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
