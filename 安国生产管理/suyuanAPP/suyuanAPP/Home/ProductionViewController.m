//
//  ProductionViewController.m
//  suyuanAPP
//
//  Created by 杨琴 on 2019/10/14.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import "ProductionViewController.h"
#import "Cell/PriductionTableViewCell.h"
#import "ProductionModel.h"
#import "AddManagementVC.h"
#import "SelectNuberVC.h"
@interface ProductionViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *tableview;

@property (nonatomic,strong)NSMutableArray *aryData;
@end

@implementation ProductionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor= UIColorFromHex(0xf2f2f2);
    self.tableview.backgroundColor=[UIColor clearColor];
    self.tableview.dataSource=self;
    self.tableview.delegate=self;
    
    self.tableview.tableFooterView=[UIView new];
    
    
    
    self.tableview .mj_header =[MJRefreshHeader headerWithRefreshingBlock:^{
        
        [self postData];
        
    }];
    
    
    

    
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self postData];

    [super viewWillAppear:animated];
    
}

-(void)postData{
    
    [SVProgressHUD show];
    
    NSString *trueName = [UserConfig shareConfig].user.trueName;
    [YQDataManager postDataJsonRequestWithUrl:[NSString stringWithFormat:@"%@%@",API_BASE_URL_STRING,ProductionListPost] Params:@{@"status": @10018, @"personCharge": trueName} modelClass:[YQBaseModel class] responseBlock:^(BOOL statue, YQBaseModel *item, id info) {
        
        NSLog(@"info%@ item==%@",info,item.data);
        
        if (statue) {
            
            YQBaseModel *mode=info;
            
            self.aryData=[ProductionModel mj_objectArrayWithKeyValuesArray:mode.data];
            [self.tableview reloadData];
            [self.tableview.mj_header endRefreshing];
            [SVProgressHUD dismiss];
            [SVProgressHUD dismissWithDelay:1.0];

            
        }else{
            
            [SVProgressHUD showErrorWithStatus:@"网络错误"];
            [SVProgressHUD dismissWithDelay:1.0];
            
            
        }
        
        
        
    }];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PriductionTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"PriductionTableViewCell"];
  cell.model=[_aryData objectAtIndex:indexPath.row];
    
    return cell;
    
    
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.aryData count];
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ProductionModel * model=[_aryData objectAtIndex:indexPath.row];
      
    [self.dicAddFarm setObject:[Utils stringTurnString:model.dataIdentifier]  forKey:@"archiId"];
    
    if ([[self.dicAddFarm objectForKey:@"farmingCode"] isEqualToString:@"100100"]||[[self.dicAddFarm objectForKey:@"farmingCode"] isEqualToString:@"1020100"]||[[self.dicAddFarm objectForKey:@"farmingCode"] isEqualToString:@"300100"]) {
        
      
          AddManagementVC *vc=GetSbVc(@"Main", @"AddManagementVC");
                 vc.dicAddFarm =self.dicAddFarm;

                 [self.navigationController pushViewController:vc animated:YES];
    }else{
        
        SelectNuberVC *vc=GetSbVc(@"Main", @"SelectNuberVC");
       
              vc.dicAddFarm =self.dicAddFarm;
        vc.inputCategory =self.inputCategory;
        
              [self.navigationController pushViewController:vc animated:YES];
    }

    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */




@end
