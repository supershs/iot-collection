//
//  HomeCollectionViewController.h
//  suyuanAPP
//
//  Created by 杨琴 on 2019/10/8.
//  Copyright © 2019 江苏南京叁拾叁. All rights reserved.
//

#import "YQBaseCollectionViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeCollectionViewController : YQBaseCollectionViewController

@end

NS_ASSUME_NONNULL_END
