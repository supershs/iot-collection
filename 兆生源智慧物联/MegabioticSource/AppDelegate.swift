//
//  AppDelegate.swift
//  MegabioticSource
//
//  Created by JAY on 2023/5/26.
//

import UIKit
import AFNetworking
import CallKit
import CallKit
import CoreTelephony

#if DEBUG // 判断是否在测试环境下
let kWebUrl = "http://124.71.189.139:9212/#/"
//let kWebUrl = "http://192.168.0.9:8081/#/"
#else
let kWebUrl = "http://124.71.189.139:9212/#/"
//let kWebUrl = "http://192.168.0.9:8081/#/"
#endif

let logoURL = "http://124.71.189.139:9211/api/1.0/Login/checkLogin" //登录接口
let GOTOHOME = "pages/tabBar/homePage?data="
let appURL = "33app.33iot.com"


// 图片上传接口（暂无此功能）
let kImageApi = "http://119.3.169.92:9488"
let kImageUpload = "\(kImageApi)/sysFile/batchUploadImg"
let kImageInfoUpload = "\(kImageApi)/api/1.1/CityCapture/savePic"

// 友盟
let kUMAppKey = "5f8d054180455950e4ae04ed"//"5e9d880ddbc2ec07ad295685"
// 微信
let kWechatAppId = "wxe315c4d7125111e6"
let kWechatSecrectKey = "5424cea7cb28690d3ad4e7f680b236ea"
let kWechatLinks = "https://help.wechat.com/XuYi/"


@main
class AppDelegate: UIResponder, UIApplicationDelegate {


    var canAllButUpsideDown: Bool = false
    var window: UIWindow?
    var webVC = HSWebViewController(path: kWebUrl)
    var logVC = JDLoginViewController()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //设置用户授权显示通知
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.badge,.carPlay,.sound], completionHandler: { (success, error) in
                print("通知授权" + (success ? "成功" : "失败"))
            })
        }
        
        window = UIWindow(frame: UIScreen.main.bounds)
        let nav = ZXNavigationController (rootViewController: logVC)
        self.window?.rootViewController = nav
        window!.makeKeyAndVisible()
        
        // 强制关闭暗黑模式
        if #available(iOS 13.0, *) {
            self.window?.overrideUserInterfaceStyle = .light
        }
        #if DEBUG // 判断是否在测试环境下
        print("当前环境：DEBUG")
        #else
        print("当前环境：RELEASE")
        #endif
        return true
    }


    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        if canAllButUpsideDown {
            return UIInterfaceOrientationMask.allButUpsideDown
        } else {
            return UIInterfaceOrientationMask.portrait
        }
    }

    func networkAuthStatus(stateClosure: @escaping ((Bool) -> Void)) {
            let cellularData = CTCellularData()
            cellularData.cellularDataRestrictionDidUpdateNotifier = {(state) in
                if (state == .restricted) {
                    //拒绝
                    if let t = UserDefaults.standard.value(forKey: "isFirstInApp") as? Bool, !t {
                        //                    self.networkSettingAlert()
                    }
                    stateClosure(false)
                } else if (state == .notRestricted) {
                    //允许
                    stateClosure(true)
                } else {
                    //未知
                    if let t = UserDefaults.standard.value(forKey: "isFirstInApp") as? Bool, !t {
                        //                    self.unknownNetwork()
                    }
                    stateClosure(false)
                }
            }
        }

}

