//
//  Debug.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2020/8/6.
//  Copyright © 2020 叁拾叁. All rights reserved.
//


import UIKit

class Debug: NSObject {
    
    class func print(obj: Any...) {
        ///这里大写
        #if DEBUG
            debugPrint(obj)
        #endif
        
    }
    
    class func DebugUrlLinkString(url: String, params: [String: String]) -> Void {
        #if DEBUG
            var urllinkString = url + "?" //url链接
            for (key, value) in params {
                urllinkString += key + "=" + value + "&"
            }
            urllinkString.remove(at: urllinkString.index(before: urllinkString.endIndex))
            print(obj: "urllink：" + urllinkString)
            //print("urllinkString==\(urllinkString)")
        #endif
    }
}
