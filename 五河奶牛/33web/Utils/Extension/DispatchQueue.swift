//
//  DispatchQueue.swift
//  vgbox
//
//  Created by 单琦 on 2019/10/24.
//  Copyright © 2019 Swift Xcode. All rights reserved.
//

import UIKit

extension DispatchQueue {

    func after(_ delay: TimeInterval, execute: @escaping () -> Void) {
        asyncAfter(deadline: .now() + delay, execute: execute)
    }
}
