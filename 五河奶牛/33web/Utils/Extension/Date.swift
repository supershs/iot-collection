//
//  Date.swift
//  vgbox
//
//  Created by 宋海胜 on 2019/6/25.
//  Copyright © 2019 Swift Xcode. All rights reserved.
//

import Foundation
import SwiftDate

extension Date {
    
    // 秒转分秒
    static func secondsToMinSec(second: CGFloat) -> String {
        
        var sec: CGFloat = 0
        var min: CGFloat = 0
        var secStr: String = ""
        var minStr: String = ""
        
        if second <= 3600 && 60 < second {
            min = second / 60
            sec = second.truncatingRemainder(dividingBy: 60)
            minStr = min > 9 ? String(format: "%d", Int(min)) : String(format: "0%d", Int(min))
            secStr = sec > 9 ? String(format: "%.f", sec) : String(format: "0%.f", sec)
            return String(format: "%@:%@", minStr, secStr)
        } else if second <= 60 {
            sec = second.truncatingRemainder(dividingBy: 60)
            return sec > 9 ? String(format: "00:%.f", sec) : String(format: "00:0%.f", sec)
        }
        return "00:00"
    }
    
    func sy_toString(format: String) -> String {
        // 东八区
        let regin = Region(zone: Zones.asiaShanghai)
        // date 转string
        let dateStr = self.convertTo(region: regin).toFormat(format)
        return dateStr
    }
    
    /// 根据本地时区转换
        static func getNowDateFromatAnDate(_ anyDate: Date?) -> Date {
            //设置源日期时区
            let sourceTimeZone = NSTimeZone(abbreviation: "UTC")
            //或GMT
            //设置转换后的目标日期时区
            let destinationTimeZone = NSTimeZone.local as NSTimeZone
            //得到源日期与世界标准时间的偏移量
            var sourceGMTOffset: Int? = nil
            if let aDate = anyDate {
                sourceGMTOffset = sourceTimeZone?.secondsFromGMT(for: aDate)
            }
            //目标日期与本地时区的偏移量
            var destinationGMTOffset: Int? = nil
            if let aDate = anyDate {
                destinationGMTOffset = destinationTimeZone.secondsFromGMT(for: aDate)
            }
            //得到时间偏移量的差值
            let interval = TimeInterval((destinationGMTOffset ?? 0) - (sourceGMTOffset ?? 0))
            //转为现在时间
            var destinationDateNow: Date? = nil
            if let aDate = anyDate {
                destinationDateNow = Date(timeInterval: interval, since: aDate)
            }
            return destinationDateNow!
        }
    ///间隔天数
    static func sy_days(startDate:Date,endDate:Date) -> Int{
        
        let components = NSCalendar.current.dateComponents([.day], from: startDate, to: endDate)
        //如果需要返回月份间隔，分钟间隔等等，只需要在dateComponents第一个参数后面加上相应的参数即可，示例如下：
    //    let components = NSCalendar.current.dateComponents([.month,.day,.hour,.minute], from: date1, to: date2)
        return components.day!
    }
}
