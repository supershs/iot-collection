//
//  Int.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/4/28.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit

extension Int {
    
    var weekStr: String {
        
        var str = ""
        switch self {
        case 1:
            str = "周日"
        case 2:
            str = "周一"
        case 3:
            str = "周二"
        case 4:
            str = "周三"
        case 5:
            str = "周四"
        case 6:
            str = "周五"
        case 7:
            str = "周六"
        default:
            break
        }
        return str
    }
    
    var toZhongwen: String {
        var str = ""
        switch self {
        case 1:
            str = "一"
        case 2:
            str = "二"
        case 3:
            str = "三"
        case 4:
            str = "四"
        case 5:
            str = "五"
        case 6:
            str = "六"
        case 7:
            str = "七"
        default:
            break
        }
        return str
    }
}
