//
//  UIWindow.swift
//  vgbox
//
//  Created by zts on 2019/7/24.
//  Copyright © 2019 Swift Xcode. All rights reserved.
//  copy from IQUIWindow+Hierarchy.swift(old)

import UIKit
import IQKeyboardManagerSwift

extension UIWindow{

     func TopMostController() -> UIViewController? {

        var topController = rootViewController

        while let presentedController = topController?.presentedViewController {
            topController = presentedController
        }

        return topController
    }

     func CurrentViewController() -> UIViewController? {

        var currentViewController = TopMostController()

        while currentViewController != nil && currentViewController is UINavigationController && (currentViewController as! UINavigationController).topViewController != nil {
            currentViewController = (currentViewController as! UINavigationController).topViewController
        }

        return currentViewController
    }


    /// https://www.jianshu.com/p/8c44251babed
    func frontWindow() -> UIWindow? {

        guard IQKeyboardManager.shared.keyboardShowing else{
            return UIApplication.shared.keyWindow
        }

        for window in UIApplication.shared.windows.reversed() {

            guard window.screen == UIScreen.main else {
                continue
            }
            guard !window.isHidden && window.alpha > 0 else {
                continue
            }
            guard window.windowLevel >= .normal else {
                continue
            }
            guard !window.description.hasPrefix("<UIRemoteKeyboardWindow") || IQKeyboardManager.shared.keyboardShowing else {
                continue
            }
            return window

        }
        return nil

    }

}
