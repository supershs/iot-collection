//
//  RYBaseNavView.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/4/30.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit

class RYBaseNavView: SYBaseView {

    let navView: SYBaseView = SYBaseView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: NAV_HEIGHT))
    let title: UILabel = UILabel()
    var baseCurrentVC: MBWebViewController!

    override func initViews() {
        creatBiaoqian()
    }
    
    func creatBiaoqian() {
        
        navView.backgroundColor = .white
        self.addSubview(navView.lineView)
        navView.lineView.frame = CGRect(x: 0, y: NAV_HEIGHT - 0.5, width: SCREEN_WIDTH, height: 0.5)
        navView.lineView.backgroundColor = UIColor(hex: 333333)
        let back: UIButton = UIButton()
        back.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        back.setImage(UIImage(named: "back_nav"), for: .normal)
        self.addSubview(navView)
        self.addSubview(back)
        title.textAlignment = .center
        self.addSubview(title)
        title.textColor = UIColor(hex: 333333)
        title.backgroundColor = .clear
        title.snp.makeConstraints { (make) in
            make.top.equalTo(STATUSBAR_HEIGHT)
            make.centerX.equalToSuperview()
            make.left.equalToSuperview().offset(40)
            make.right.equalToSuperview().offset(-40)
            make.bottom.equalToSuperview()
        }
        back.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.top.equalToSuperview().offset(STATUSBAR_HEIGHT)
            make.height.equalTo(48.autoWidth())
            make.width.equalTo(30.autoWidth())
        }
    }
    
    @objc func backAction() {
        if (self.baseCurrentVC != nil) {
            self.baseCurrentVC.navigationController?.popViewController(animated: true)
        }else{
            self.currentVC.navigationController?.popViewController(animated: true)

        }
    }
}
