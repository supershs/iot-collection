//
//  JPUSHRegisterDelegate.swift
//  33web
//
//  Created by 叁拾叁 on 2022/11/3.
//

import UIKit


let APPKEY  = "84301bdf909246ba2952d3d7"

var pathUrl  = ""
// 极光推送设置
extension AppDelegate: JPUSHRegisterDelegate {
    
    func jpushInit(_ application: UIApplication, _ launchOptions: [UIApplication.LaunchOptionsKey: Any]? , appDelegate: AppDelegate) {
        
        JPUSHService.setup(withOption: launchOptions as [AnyHashable : Any]?, appKey: APPKEY, channel: "App Store", apsForProduction: false, advertisingIdentifier: nil)
        //推送代码
        let entity = JPUSHRegisterEntity()
        entity.types = 1 << 0 | 1 << 1 | 1 << 2
        JPUSHService.register(forRemoteNotificationConfig: entity, delegate: appDelegate)
    }
    
    func jpushNotificationCenter(_ center: UNUserNotificationCenter!, openSettingsFor notification: UNNotification!) {
        let userInfo = notification.request.content.userInfo
        JPUSHService.handleVoipNotification(userInfo)
    }
    
    func jpushNotificationAuthorization(_ status: JPAuthorizationStatus, withInfo info: [AnyHashable : Any]!) {
        JPUSHService.handleRemoteNotification(info)
    }
    
    @available(iOS 10.0, *)
    func jpushNotificationCenter(_ center: UNUserNotificationCenter!, willPresent notification: UNNotification!, withCompletionHandler completionHandler: ((Int) -> Void)!) {
        
        let userInfo = notification.request.content.userInfo
        if notification.request.trigger is UNPushNotificationTrigger {
            print("带有完成处理程序===",userInfo)
            JPUSHService.handleRemoteNotification(userInfo)
        }
        JPush.shared().willPresent(notification)
        // 需要执行这个方法，选择是否提醒用户，有Badge、Sound、Alert三种类型可以选择设置
        completionHandler(Int(UNNotificationPresentationOptions.alert.rawValue))
    }
    
    @available(iOS 10.0, *)
    //应用程序退出后进入前台
    func jpushNotificationCenter(_ center: UNUserNotificationCenter!, didReceive response: UNNotificationResponse!, withCompletionHandler completionHandler: (() -> Void)!) {
        let userInfo = response.notification.request.content.userInfo
        self.userInfo(dictionary: userInfo)
        if response.notification.request.trigger is UNPushNotificationTrigger {
            JPUSHService.handleRemoteNotification(userInfo)
        }
        // 系统要求执行这个方法
        completionHandler()
        JPush.shared().didReceive(response)
    }
    
    
    func userInfo(dictionary:[AnyHashable: Any]){
    
        if let extraString = dictionary[AnyHashable("extra")] as? String,
           let data = extraString.data(using: .utf8),
           let extraDict = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any],
           var url = extraDict["url"] as? String {
            print(extraString,"extraString",extraDict[""] ?? "")
            if url.length != 0 {
                let id = extraDict["id"] as? String
                let returnUrl = extraDict["returnUrl"] as? String
                print(id ?? "","id====")
                    url.removeFirst()
                let path = String(format: "%@?id=%@&returnUrl=%@",url,id!,returnUrl!)
                    pathUrl = path
                    CESHIURL = path
                    NotificationCenter.default.post(name: .Noti_goUrl, object: ["url": url,"id": id])
            }

        } else {
            // 提取失败
            print("无法提取url")
            NotificationCenter.default.post(name: .Noti_goUrl, object: ["url": GOTOHOME,"id": ""])

        }
        
        

    }
    //点推送进来执行这个方法
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        JPUSHService.handleRemoteNotification(userInfo)
        CESHIURL = "99999"
        completionHandler(UIBackgroundFetchResult.newData)
        
    }
    //系统获取Token
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print(deviceToken,"======deviceToken")
        JPUSHService.registerDeviceToken(deviceToken)
    }
    //获取token 失败
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) { //可选
        print("注册远程通知失败，出现错误: \(error)")
    }
}

extension Notification.Name {
    
    /// 有推送消息
    static let Noti_PushJpush = Notification.Name("Noti_PushMsg")
    
}
