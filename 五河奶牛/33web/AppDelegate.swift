//
//  AppDelegate.swift
//  33web
//
//  Created by 宋海胜 on 2020/11/25.
//

import UIKit
import CallKit
import CoreTelephony
import IQKeyboardManagerSwift

// 微信
let kWechatAppId = "wxe315c4d7125111e6"
let kWechatSecrectKey = "5424cea7cb28690d3ad4e7f680b236ea"
let kWechatLinks = "https://help.wechat.com/XuYi/"
let kMapKey  = "2f0c5271515bc2dda854bdba2b66d7cc"
@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        let nav = UINavigationController.init(rootViewController:  SYAccountLoginViewController())
        nav.isNavigationBarHidden = true
        window?.rootViewController = nav
      
#if DEBUG // 判断是否在测试环境下
print("当前环境：DEBUG")
#else
print("当前环境：RELEASE")
#endif
        // 强制关闭暗黑模式
        if #available(iOS 13.0, *) {
            self.window?.overrideUserInterfaceStyle = .light
        }
        //设置用户授权显示通知
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.badge,.carPlay,.sound], completionHandler: { (success, error) in
                print("通知授权" + (success ? "成功" : "失败"))
            })
        }
        IQKeyboardManager.shared.enable = true
        JPUSHService.resetBadge()
        self.jpushInit(application, launchOptions, appDelegate: self)
        JPush.shared().initOthers(APPKEY, launchOptions: launchOptions)
        self.applicationDidBecomeActive(application)        

        return true
    }
    func applicationDidBecomeActive(_ application: UIApplication) {
        // MARK: 角标重置
        application.applicationIconBadgeNumber = -1
        // MARK: 移除所有远程通知
        // 即将收到的
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        // 已经收到的
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        JPUSHService.setBadge(0)
        
    }
    
    func networkAuthStatus(stateClosure: @escaping ((Bool) -> Void)) {
        let cellularData = CTCellularData()
        cellularData.cellularDataRestrictionDidUpdateNotifier = {(state) in
            if (state == .restricted) {
                //拒绝
                if let t = UserDefaults.standard.value(forKey: "isFirstInApp") as? Bool, !t {
                    //                    self.networkSettingAlert()
                }
                stateClosure(false)
            } else if (state == .notRestricted) {
                //允许
                stateClosure(true)
            } else {
                //未知
                if let t = UserDefaults.standard.value(forKey: "isFirstInApp") as? Bool, !t {
                    //                    self.unknownNetwork()
                }
                stateClosure(false)
            }
        }
    }
}


