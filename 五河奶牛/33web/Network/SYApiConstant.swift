//
//  Constant_API.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2020/8/11.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit


let VERSION = "/api/1.0/"
//#if DEBUG // 判断是否在测试环境下

//var WEBIP = "http://192.168.0.84:8080/#/"
var WEBIP = "http://114.115.238.249:9022/#/"
//var IP = "http://114.115.238.249:9020"

//var WEBIP = "http://192.168.0.126:8080/#/"
var IP = "http://192.168.0.92:9020"

//var IP = "http://114.115.166.192:9910"
//114.115.166.192:9910

//#else
//var WEBIP = "http://192.168.0.84:8080/#/"
////var WEBIP = "http://114.115.166.192:9902/#/"
////var IP = "http://192.168.0.45:9900"
//var IP = "http://114.115.166.192:9900"
//#endif




var IMGIP = IP + VERSION
var sysTenantDTOURL = "114.115.238.249:9020"

let testMP3 = "http://downsc.chinaz.net/Files/DownLoad/sound1/201906/11582.mp3"
let testMP4 = "http://static.tripbe.com/videofiles/20121214/9533522808.f4v.mp4"

let REQUEST_DIC = "requestDictionary"
let GOTOHOME = "pages/base-index/index"
let USERAGREEMENT = "pages/base-login/agreement"
let RESETPASSWORDURL = "pages/base-login/resetPassword"
let MOBILELOGINURL = "pages/base-login/register"
//let UGOTOHOME = "pages/enterprise/index"
// MARK: - tabbar
// 登录
let LOGIN = "SysLogin/password/login"
//忘记

//忘记密码获取验证码
let CODE = "auth/message/push"
//忘记密码登录
let FORGETPASSWORD = "SysLogin/forget/password"
//注册获取验证吗
let  REGISTERCODE = "auth/message/push"
//手机号验证码登录
let REGIStER = "auth/mobile/login"
let CODELOGIN = "auth/mobile/login"
let USEREDITPERMISSION = "TraceSubject/pc/userEditPermission"
let RETALIAS = "auth/app/resetDeviceAlias"
let RETALIASREID = "auth/app/resetDeviceAlias"
// 身份识别
let SHENFEN_RECOGNIZE = "SysUser/app/certification"

// MARK: - 病虫害
// 病害图像识别接口
let SHIBIE_BING = "DiseaseDiscernLog/sc/detect/plant"

// 虫害图像识别接口
let SHIBIE_CHONG = "DiseaseDiscernLog/sc/detect/pest"

// 病虫害查询我的识别历史
let SHIBIE_LISHI = "DiseaseDiscernLog/app/myself"

var CESHIURL = "12345"
