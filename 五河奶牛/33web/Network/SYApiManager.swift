//
//  SYApiManager.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2020/8/11.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import Foundation
import Moya
import HandyJSON
import RxSwift



let ApiManagerProvider = MoyaProvider<SYApiManager>(endpointClosure: endpointMapping, requestClosure: requestTimeoutClosure, plugins:[])

public func endpointMapping<Target: TargetType>(target: Target) -> Endpoint {
    print("\n************************************************** separate *********************************************\n\n请求链接：\(target.baseURL)\(target.path)\n方法：\(target.method.rawValue)\n请求头：\(target.headers ?? [:])\n参数：\(String(describing: target.task.self))")
    //在这里设置你的HTTP头部信息
    return MoyaProvider.defaultEndpointMapping(for: target).adding(newHTTPHeaderFields: target.headers ?? [:])
}

//设置请求超时时间
private let requestTimeoutClosure = { (endpoint: Endpoint, done: @escaping MoyaProvider<SYApiManager>.RequestResultClosure) in
    do {
        var request = try endpoint.urlRequest()
        request.timeoutInterval = 60
print(request)
        done(.success(request))
    } catch {
        return
    }
}

enum SYApiManager {
    
    case resetDeviceAlias(registrationId: String)
    case resetDeviceRegistrationId(registrationId: String)
    case login(loginName: String, password: String,registrationId: String)
    case verifiCode(mobile: String)
    case forgetPassword(mobile: String,code: String,passwd: String,passwd2:String,confirmPassword: String)
    case registerCode(mobile: String)
    case registerLogin(mobile: String,code: String,passwd: String,passwd2:String)
    case codeLogin(mobile: String, code: String,registrationID: String)
    case loginOut(userId: String)
    case shenfenShibie(imgs: [UIImage],dic:[String: String])
    case shibieBing(category: String, position: String, img: UIImage)
    case shibieChong(img: UIImage)
    case shibieLishi(page:Int,size:Int)
    case userEditPermission
}

extension SYApiManager: TargetType {

    
    var headers: [String : String]? {
        let token: String = UserInstance.accessToken ?? ""
        return ["Token": token, "Authorization": token]
    }
    
    var baseURL: URL {
        switch self {
        case .login:
            return URL(string: IP+VERSION)!
//        case .homepageWeather(let cityName):
//            // 在path中设置的url中的 ？ 会被转义，故放于此
//            return  URL(string: String(format: "%@%@%@?cityName=%@", IP, VERSION, HOMEPAGE_WEATHER, cityName.getEncodeString))!
        default:
            return URL(string: IP+VERSION)!
        }
    }
    
    var path: String {
        switch self {
        case .resetDeviceAlias(let stringId): return  String(format: "%@/%@", RETALIAS,stringId)
        case .resetDeviceRegistrationId(_): return String(format: "%@", RETALIASREID)
        case .codeLogin(_,_,_): return CODELOGIN
        case .login(_, _,_): return LOGIN
        case .registerCode(_): return REGISTERCODE
        case .registerLogin(_, _, _, _): return REGIStER
        case .verifiCode(_): return CODE
        case .forgetPassword(_, _, _,_,_): return FORGETPASSWORD
        case .loginOut(let userId): return "/api/\(userId)"
        case .shenfenShibie(_,_): return SHENFEN_RECOGNIZE
        case .shibieBing(let category, let position, _): return String(format: "%@/%@/%@", SHIBIE_BING,category,position)
        case .shibieChong(_): return SHIBIE_CHONG
        case .shibieLishi(let page, let size): return String(format: "%@/%ld/%ld", SHIBIE_LISHI,page,size)
        case .userEditPermission: return USEREDITPERMISSION
        
        }

    }
    
    var method: Moya.Method {
        switch self {

        case .resetDeviceRegistrationId,.login,.verifiCode,.codeLogin,.registerCode,.registerLogin,.forgetPassword,.shenfenShibie, .shibieLishi, .shibieChong, .shibieBing:
            return .post
//        case .forgetPassword:
//            return .put
//
//        case .deleteMy:
//            return .patch
//        case .dy_jingquPeopleDelete:
//            return .delete
        default:
            return .get 
        }
    }
    
    var task: Task {
        switch self {
            
        case .resetDeviceRegistrationId(let registrationId):
            return .requestParameters(parameters: ["registrationId":registrationId], encoding: JSONEncoding.default)
        case .login(let loginName, let password, let registrationId):
            return .requestParameters(parameters: ["loginName": loginName, "password": AESCode.endcode_AES_CBC(strToEncode: password),"sysTenantDTO":["url":sysTenantDTOURL],"registrationId": registrationId], encoding: JSONEncoding.default)
        case .verifiCode(let mobile):
            return .requestParameters(parameters: ["mobile":mobile,"sysTenantDTO":["url":sysTenantDTOURL]], encoding: JSONEncoding.default)
        case .codeLogin(let mobile, let code, let registrationID):
            return .requestParameters(parameters: ["mobile":mobile,"code":code,"sysTenantDTO":["url":sysTenantDTOURL]], encoding: JSONEncoding.default)
        case .forgetPassword(let mobile,let code, let passwd, let passwd2, let confirmPassword):
            return .requestParameters(parameters: ["mobile":mobile,"code": code,"password":  AESCode.endcode_AES_CBC(strToEncode:passwd),"confirmPassword":AESCode.endcode_AES_CBC(strToEncode:passwd2),"confirmPassword":AESCode.endcode_AES_CBC(strToEncode:confirmPassword)], encoding: JSONEncoding.default)
        case .registerCode(let mobile):
            return .requestParameters(parameters: ["mobile":mobile,"sysTenantDTO":["url": sysTenantDTOURL]], encoding: JSONEncoding.default)
        case .registerLogin(let mobile,  let code, let passwd, let passwd2):
            return .requestParameters(parameters: ["mobile":mobile,"code": code,"password":  AESCode.endcode_AES_CBC(strToEncode:passwd),"passwd2":AESCode.endcode_AES_CBC(strToEncode:passwd2) ,], encoding: JSONEncoding.default)

            
            // chao "13915999715"
        // 蜘蛛侠 13382336993
        // 钢铁侠 13382336992
//            return .requestParameters(parameters: ["loginName": "13382336992", "password": "123456"], encoding: JSONEncoding.default) // post请求
        //            return .requestParameters(parameters: ["memberName": "zhaozhiyun", "password": "123456"], encoding: URLEncoding.default) // post请求
        
        //        case .getReward(let type, let cursor, let limit):
        //            return .requestParameters(parameters: ["type": type], encoding: URLEncoding.default) // 其它请求
        case .shibieBing(_, _, let img):
            return .uploadMultipart(getImagesData(imgs: [img]))
            
        case .shibieChong(let img):
            return .uploadMultipart(getImagesData(imgs: [img]))
        case .shibieLishi(_,_):
            return .requestParameters(parameters: [:], encoding: JSONEncoding.default)
        case .shenfenShibie(let imgs, let dic):
            return .uploadCompositeMultipart((getImagesData(imgs: imgs)), urlParameters: dic)
//        case .downloadAudio(let url):
//            return .downloadDestination { (url, res) -> (destinationURL: URL, options: DownloadRequest.Options) in
//
//            }
            
            
        default:
            return .requestPlain
        }
    }
    
    var validate: Bool {
        return false
    }
    var sampleData: Data {
        return "".data(using: String.Encoding.utf8)!
    }
    
    func getImagesData(imgs: [UIImage]) -> [MultipartFormData] {

        var formDataAry:[MultipartFormData] = [MultipartFormData]()
        for (index,image) in imgs.enumerated() {
            //图片转成Data
            let data:Data = image.jpegData(compressionQuality: 0.7)!
            //根据当前时间设置图片上传时候的名字
            let now = Date()
            var dateStr: String = now.sy_toString(format: "yyyy-MM-dd HH:mm:ss")
            //别忘记这里给名字加上图片的后缀哦
            dateStr = dateStr.appendingFormat("-%i.jpeg", index)
            // MARK: - 对应服务端，这里的name必须为 "file"，fileName和mineType必须有值，headers只需要token，不然都会报错，提示未上传文件
            let formData: MultipartFormData = MultipartFormData(provider: .data(data), name: "file", fileName: dateStr, mimeType: "image/jpeg")
        
            formDataAry.append(formData)
        }
      
        return formDataAry
    }
    
    
    
    
    func getImagesDataID(imgs: [UIImage], trueName: NSString, idCard: NSString,address: NSString) -> [MultipartFormData] {

        var formDataAry:[MultipartFormData] = [MultipartFormData]()
        for (index,image) in imgs.enumerated() {
            //图片转成Data
            let data:Data = image.jpegData(compressionQuality: 0.7)!
            //根据当前时间设置图片上传时候的名字
            let now = Date()
            var dateStr: String = now.sy_toString(format: "yyyy-MM-dd HH:mm:ss")
            //别忘记这里给名字加上图片的后缀哦
            dateStr = dateStr.appendingFormat("-%i.jpeg", index)
            // MARK: - 对应服务端，这里的name必须为 "file"，fileName和mineType必须有值，headers只需要token，不然都会报错，提示未上传文件
            
            let trueData = String(trueName).data(using: String.Encoding.utf8, allowLossyConversion: true)
            let idCardData = String(idCard).data(using: String.Encoding.utf8, allowLossyConversion: true)
            let addressData = String(address).data(using: String.Encoding.utf8, allowLossyConversion: true)
            
            
            let formData: MultipartFormData = MultipartFormData(provider: .data(data), name: "file", fileName: dateStr, mimeType: "image/jpeg")

            formDataAry.append(formData)
        }
      
        return formDataAry
    }
}



// MARK: 取消所有请求
func cancelAllRequest() {
    //    ApiManagerProvider.manager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
    //        dataTasks.forEach { $0.cancel() }
    //        uploadTasks.forEach { $0.cancel() }
    //        downloadTasks.forEach { $0.cancel() }
    //    }
    //
    //    ApiManagerProvider.manager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
    //        dataTasks.forEach { $0.cancel() }
    //        uploadTasks.forEach { $0.cancel() }
    //        downloadTasks.forEach { $0.cancel() }
    //    }
}


