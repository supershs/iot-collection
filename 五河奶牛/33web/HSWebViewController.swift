//
//  HSWebViewController.swift
//  33web
//
//  Created by 宋海胜 on 2020/11/25.
//

import WebKit
import SwiftWeb
import RxSwift
import RxAlamofire
import HandyJSON
import AMapSearchKit

enum JsMethod {
    case passGPSInfo
    case passVersion
    case passVoiceContent
    case passPayResult
    case passAddress
}

// 自定义编辑
class HSWebViewController: BaseWebViewController {
    
    
    let requestVM: RYNongchangListVM = RYNongchangListVM()
    let dispose = DisposeBag()
    
    fileprivate var location = LocationManager()
    fileprivate var isOnece = false
    fileprivate var configManager = Tool()
    fileprivate var backImgView = UIImageView()
    fileprivate var urlInputView: InputView!
    public var canAllButUpsideDown = false
    let disposeBag = DisposeBag()
    let search = AMapSearchAPI()
    deinit {
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "getGPSInfo")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "saveUnreadMsgCnt")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "getVersion")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "passVideoParams")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "leaveVideoPage")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "startVoice")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "stopVoice")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "toMiniProgram")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "toGaodeApp")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "toPay")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "toShare")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "toDownLoad")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "saveImage")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "webGoBack")
        webView.configuration.userContentController.removeScriptMessageHandler(forName: "loginOut")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        // 启动图复制图片、web未加载完成时覆盖页面
        self.backImgView = self.configManager.getLanuchboardImage()
        self.backImgView.frame = self.view.bounds
        self.view.addSubview(self.backImgView)
        // 顶部状态栏高度问题解决
        self.configManager.topMangain(webView: webView, vc: self)
        // 网络判断，重新加载网页
        judgeNetwork()
        // 经纬度
        location.locationProtocol = self
        self.webView.frame = CGRect(x: 0, y: kStatusBarHeight, width: kScreenWidth, height: kScreenHeight - kStatusBarHeight)
        
        if CESHIURL == "12345"{
        }else{
            let token: String? = UserInstance.accessToken
           var  path = WEBIP + CESHIURL + "&type=app"
            if !(token ?? "").isEmpty {
                path = WEBIP + CESHIURL + "&type=app&token=" + (token ?? "")
            }
            print(" path.getEncodeString====",path.getEncodeString)
            self.sy_push(MBWebViewController(path: path.getEncodeString))
        }
        NotificationCenter.default.addObserver(self, selector: #selector(networkDidReceiveMessage), name: .Noti_goUrl, object: nil)

        let btn = UIButton.init(frame: CGRect(x: 20, y: 120, width: 230, height: 44))
        btn.setTitle("www", for: .normal)
        btn.backgroundColor = .red
//        self.view.addSubview(btn)
        btn.addTarget(self, action: #selector(goWeb), for: .touchUpInside)
    }
    
    @objc func goWeb(){
        location.startLocation()
    }

    @objc func networkDidReceiveMessage(notification:Notification){
        let dic: [String: String] = notification.object as! [String : String]
        let token: String? = UserInstance.accessToken
        print(dic,"tongzhitiaozhuan")
        var path = WEBIP + GOTOHOME
//        if !(token ?? "").isEmpty {
//          path = WEBIP + CESHIURL + "&type=app&token=" + (token ?? "")
//        }

        self.sy_push(MBWebViewController(path:  path))

    }
    
    override func injectSwiftMethod(_ config: WKWebViewConfiguration) {
        config.allowsInlineMediaPlayback = true;
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "getGPSInfo")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "saveUnreadMsgCnt")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "getVersion")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "passVideoParams")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "leaveVideoPage")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "startVoice")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "stopVoice")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "toMiniProgram")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "toGaodeApp")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "toPay")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "toShare")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "toDownLoad")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "saveImage")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "webGoBack")
        config.userContentController.add(WeakScriptMessageDelegate(scriptDelegate: self), name: "loginOut")
        
    }
    
    override func jsAction() {
        
    }
    
    // 在viewDidLoad中模态不行，此时页面还没加载完成
    fileprivate func judgeNetwork() {
        self.loadWeb()
        let myAppdelegate = UIApplication.shared.delegate as! AppDelegate
        myAppdelegate.networkAuthStatus(stateClosure: {[weak self] state in
            if let `self` = self {
                if state {
                    DispatchQueue.main.async {
//                        HUDUtil.showHud()
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        DispatchQueue.main.async {
                            HUDUtil.hideHud()
                        }
                        self.loadWeb()
                    }
                }
            }
        })
    }
    
    fileprivate func jsAction(param: String, type: JsMethod) {
        
        var javascript: String = ""
        
        switch type {
            
        case .passGPSInfo:
            javascript = "javascript:passGPSInfo(\"\(param)\")"
            
        case .passVersion:
            javascript = "javascript:passVersion(\"\(param)\")"
            
        case .passVoiceContent:
            javascript = "javascript:passVoiceContent(\"\(param)\")"
            
        case .passPayResult:
            javascript = "javascript:payResult(\"\(param)\")"
        case .passAddress:
            javascript = "javascript:passAddress(\"\(param)\")"
        }
        self.webView.evaluateJavaScript(javascript) { (res, error) in
            print("swift向js传值\n  param: \(param)\n  response: \(res ?? "nil")\n  error: \(error?.localizedDescription ?? "nil")")
        }
    }
    
    override func swiftAction(_ name: String, _ body: Dictionary<String, Any>) {
        
        switch name {
        case "webGoBack":
            if webView.canGoBack {
                webView.goBack()
            } else {
                self.sy_popVC()
            }
        case "loginOut":
            UserInstance.userLogout()
            resetDeviceRegistrationId()
            if let navigationController = self.navigationController {
//                let rootViewController = navigationController.viewControllers.first as ? SYAccountLoginViewController
                if let rootViewController = UIApplication.shared.keyWindow?.rootViewController as? SYAccountLoginViewController {
                    print("是")
                    self.navigationController?.popToRootViewController(animated: true)
                }else{
                    print("否")
                    self.sy_push(SYAccountLoginViewController())
                }
            }else {
                let nav = UINavigationController(rootViewController: SYAccountLoginViewController())
                nav.isNavigationBarHidden = true
                UIApplication.shared.keyWindow?.rootViewController = nav

            }
//            if let rootViewController = UIApplication.shared.keyWindow?.rootViewController as? SYAccountLoginViewController {
//                // rootViewController 是 LoginViewController
//
//            } else {
//                // rootViewController 不是 LoginViewController
//            }
//            print(UserInstance.accessToken ?? "ddddd")
//            self.navigationController?.popToRootViewController(animated: true)
        
        case "getGPSInfo":
            gpsService()
            
        case "saveUnreadMsgCnt":
            let num = body["notReadNum"] as! NSNumber
            UIApplication.shared.applicationIconBadgeNumber = num.intValue > 99 ? 99 : num.intValue
            
        case "getVersion":
            let infoDic = Bundle.main.infoDictionary
            let appVersion = infoDic!["CFBundleShortVersionString"] as! String
            jsAction(param: appVersion, type: .passVersion)

        case "toGaodeApp":
            Tool.routePlanning(body)
        case "toDownLoad":
            let url = body["url"] as? String ?? ""
            UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
            
        case "callSomeOne":
            let phoneNum = body["phoneNum"] as? String ?? ""
            self.configManager.callSomeOne(phoneNum)
            
        case "saveImage":
            let urlStr = body["url"] as? String ?? ""
            let subStr = urlStr.components(separatedBy: "base64,").last ?? ""
            self.saveImgs(base64EncodedStr: subStr)
        default:
            break
        }
    }
    
    override func getH5Url(_ url: String) {
        
        if (url.contains("tel://")) {
            let phoneUrl = url.components(separatedBy: "tel://").last
            self.configManager.callSomeOne(phoneUrl!)
        }
    }
    
    override func loadSuccess() {
        
        UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseInOut, animations: {
            self.backImgView.alpha = 0
        }) { (finish) in
            self.backImgView.isHidden = true
        }
    }
    
    override func loadFail() {
//        backImgView.isHidden = false
//        self.configManager.alertToReload(self) {
//            self.loadWeb()
//        }
    }
    
    // 定位
    fileprivate func gpsService() {
        
        //只获取一次
        isOnece = true
        self.location.startLocation()
    }
    
 
    
    func voiceContent(content: String) {
        self.jsAction(param: content, type: .passVoiceContent)
    }
    
    func payResult() {
        self.jsAction(param: "", type: .passPayResult)
    }
    
    func saveImgs(urlStr: String) {
        if let url = URL(string: urlStr) {
            do {
                let data = try Data(contentsOf: url)
                if let image = UIImage(data: data) {
                    let infoDictionary: Dictionary = Bundle.main.infoDictionary!
                    let kAppDisplayName = infoDictionary["CFBundleDisplayName"] as! String

                    ZGCustomPhoto.saveImageInAlbum(image: image, albumName:kAppDisplayName) { (result) in
                        
                        DispatchQueue.main.async {
                            switch result{
                            case .success:
                                self.view.makeToast("已保存至相册")
                            case .denied:
                                self.view.makeToast("保存被拒绝")
                            case .error:
                                self.view.makeToast("保存失败")
                            }
                        }
                    }
                } else {
                    self.view.makeToast("data to image error")
                }
            } catch {
                self.view.makeToast(error.localizedDescription)
            }
           
        } else {
            self.view.makeToast("urlStr to URL error")
        }
    }
    
    func saveImgs(base64EncodedStr: String) {
        
        if let data = Data(base64Encoded: base64EncodedStr) {
            if let image = UIImage(data: data) {
                let infoDictionary: Dictionary = Bundle.main.infoDictionary!
                let kAppDisplayName = infoDictionary["CFBundleDisplayName"] as! String

                ZGCustomPhoto.saveImageInAlbum(image: image, albumName: kAppDisplayName) { (result) in
                    
                    DispatchQueue.main.async {
                        switch result{
                        case .success:
                            self.view.makeToast("已保存至相册")
                        case .denied:
                            self.view.makeToast("保存被拒绝")
                        case .error:
                            self.view.makeToast("保存失败")
                        }
                    }
                }
            } else {
                self.view.makeToast("data to image error")
            }
        } else {
            self.view.makeToast("base64EncodedStr to data error")
        }
        
    }
    
    func resetDeviceRegistrationId(){
        var  stringRegistr = ""
        if let key = UserDefaults.standard.object(forKey: "registrationID"){
            print(key)
            stringRegistr = UserDefaults.standard.object(forKey: "registrationID") as! String
        }
//        requestVM.baseRequest(disposeBag: dispose, type: .resetDeviceRegistrationId(registrationId: stringRegistr), modelClass: SGBaseModel<SYLoginModel>.self) {[weak self] (res) in
//            if let `self` = self {
//                let m = res.data
//               
//            }
//            
//        } Error: {
//            
//        }
    }
    

}

extension HSWebViewController: LocationProtocol {
    func getGPSAuthorizationFailure() {
        
        print("getGPSAuthorizationFailure")
    }
    
    func getLocationSuccess(_ area: String, _ locality: String, _ subLocality: String, _ thoroughfare: String, _ name: String) {
        
        print(area,locality,subLocality,thoroughfare,name)
        let param: String = locality + "," + subLocality + thoroughfare
        if locality.isEmpty != nil && thoroughfare.isEmpty != nil {
            self.jsAction(param: param, type: .passAddress)
        }
    }

    func getGPSSuccess(latitude: Double, longitude: Double) {
        location.stopLocation()
        if (self.isOnece) {
            print("lng: \(longitude) lat: \(latitude)")
            let param = "\(longitude),\(latitude)"
            self.jsAction(param: param, type: .passGPSInfo)
            self.isOnece = false
        }
    }
    
    func getGPSFailure(error: Error) {
        self.isOnece = false
        print("getMoLocation error: \(error.localizedDescription)")
        if (!self.isOnece) {
            location.stopLocation()
        }
    }
}
