//
//  SYForgetViewController.swift
//  33web
//
//  Created by iOS开发 on 2023/2/23.
//

import UIKit
import RxCocoa
import RxSwift
import SnapKit
import SwiftWeb

class SYForgetViewController: SYBaseViewController {
    
    let requestVM: RYNongchangListVM = RYNongchangListVM()
    let minUsernameLength = 1
    let maxUsernameLength = 11
    let minPasswordLength = 1
    let maxPasswordLength = 30
    let disposBag = DisposeBag()
    var usernameTF: UITextField = {
        let tf = UITextField()
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.textColor = .black
        tf.placeholder = "请输入手机号"
        return tf
    }()
    var accountSepView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.init(hex: 0xf2f2f2)
        return v
    }()
    var usercodeTF: UITextField = {
        let tf = UITextField()
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.textColor = .black
        tf.placeholder = "请输入验证码"
        return tf
    }()
    var codeButton: UIButton = {
        let v = UIButton()
        v.backgroundColor = UIColor.white
        v.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        v.setTitleColor(UIColor.init(hex: 0x4BCA8A), for: .normal)
        v.setTitle("获取验证码", for: .normal)
//        v.titleLabel?.textAlignment = .right
//        [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        v.contentHorizontalAlignment = .right
        return v
    }()
    var accountSepView1: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.init(hex: 0xf2f2f2)
        return v
    }()
    var passwordTF: UITextField = {
        let tf = UITextField()
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.textColor = .black
        tf.placeholder = "请输入密码"
        tf.isSecureTextEntry = true
        
        return tf
    }()
    var accountSepView2: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.init(hex: 0xf2f2f2)
        return v
    }()
    var newpasswordTF: UITextField = {
        let tf = UITextField()
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.textColor = .black
        tf.placeholder = "请输入确认密码"
        tf.isSecureTextEntry = true
        return tf
    }()
    var accountSepView3: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.init(hex: 0xf2f2f2)
        return v
    }()
    var loginButton: UIButton = {
        let v = UIButton()
        v.backgroundColor = UIColor.white
//        v.layer.cornerRadius = 22.5
//        v.layer.masksToBounds = true
        v.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        v.setTitleColor(UIColor.init(hex: 0xffffff), for: .normal)
        v.setBackgroundImage(UIImage(named: "login_btn"), for: .normal)
        v.setTitle("登录", for: .normal)
        return v
    }()
    
    public var typeTitle: String = ""
    public init(typeTitle: String)
    {
        self.typeTitle = typeTitle
        super.init(nibName: nil, bundle: nil)
        print("类型 \(self.typeTitle)")
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = self.typeTitle
        self.navigationController?.isNavigationBarHidden = false
        self.view.backgroundColor = .white
        self.initView()
        if self.typeTitle == "忘记密码"{
            self.loginButton.setTitle("修改", for: .normal)
            
        }else
        {
            self.loginButton.setTitle("注册", for: .normal)
        }

        let usernameValid = usernameTF.rx.text
            .map { $0!.count >= self.minUsernameLength && $0!.count <= self.maxUsernameLength }
            .share(replay: 1)
        
        codeButton.rx.tap
            .subscribe(onNext: { [weak self] in
             
                
                if self?.typeTitle == "忘记密码"{
                    self?.getCode(mobile: self?.usernameTF.text ?? "")

                }else{
                    self?.getResiterCode(mobile: self?.usernameTF.text ?? "")

                }

                
            })
            .disposed(by: disposBag)
        
        
        let passwordValid = passwordTF.rx.text
            .map { $0!.count >= self.minPasswordLength && $0!.count < self.maxPasswordLength }
            .share(replay: 1)
        let passwordValid2new = newpasswordTF.rx.text
            .map { $0!.count >= self.minPasswordLength && $0!.count < self.maxPasswordLength }
            .share(replay: 1)
        
        let everythingValid = Observable.combineLatest(usernameValid, passwordValid,passwordValid2new) { (usernameValid, passwordValid,passwordValid2new) -> Bool in
            usernameValid && passwordValid && passwordValid2new
        }
        
        everythingValid
            .bind(to: loginButton.rx.isEnabled) // 用户名密码都通过验证，才可以点击按钮
            .disposed(by: disposBag)
        
        loginButton.rx.tap.subscribe(onNext: { [weak self] in
            
            if self?.typeTitle == "忘记密码" {
                self?.forgetPassword(mobile: self?.usernameTF.text ?? "" , code: self?.usercodeTF.text ?? "", passwrod: self?.passwordTF.text ?? "", newPassword: self?.newpasswordTF.text ?? "")
                
            }else {
                self?.getResiter(mobile: self?.usernameTF.text ?? "" , code: self?.usercodeTF.text ?? "", passwrod: self?.passwordTF.text ?? "", newPassword: self?.newpasswordTF.text ?? "")

            }
         
        })
        .disposed(by: disposBag)
        
    }
    
    func getCode(mobile: String){
        requestVM.baseRequest(disposeBag: dispose, type: .verifiCode(mobile: mobile), modelClass: SGBaseModel<SYLoginModel>.self) {[weak self] (res) in
            if let `self` = self {
                let m = res.data
                Util.dispatchTimer(timeInterval: 1, repeatCount: 60) { timer, index in
                    self.codeButton.isUserInteractionEnabled = false
                    self.codeButton.setTitle("重新发送\(index)s", for: .normal)
                    if index == 0  {
                        self.codeButton.setTitle("获取验证码", for: .normal)
                        self.codeButton.isUserInteractionEnabled = true
                    }
                }
            }
            
        } Error: {
            
        }
    }
    
    func getResiterCode(mobile: String){
        requestVM.baseRequest(disposeBag: dispose, type: .registerCode(mobile: mobile), modelClass: SGBaseModel<SYLoginModel>.self) {[weak self] (res) in
            if let `self` = self {
                let m = res.data
                Util.dispatchTimer(timeInterval: 1, repeatCount: 60) { timer, index in
                    self.codeButton.isUserInteractionEnabled = false
                    self.codeButton.setTitle("重新发送\(index)s", for: .normal)
                    if index == 0  {
                        self.codeButton.setTitle("获取验证码", for: .normal)
                        self.codeButton.isUserInteractionEnabled = true
                    }
                }
            }
            
        } Error: {
            
        }
    }
    
    func getResiter(mobile: String,code: String,passwrod: String, newPassword: String){
        requestVM.baseRequest(disposeBag: dispose, type: .registerLogin(mobile: mobile, code: code, passwd: passwrod, passwd2: newPassword), modelClass: SGBaseModel<SYLoginModel>.self) {[weak self] (res) in
            if let `self` = self {
                let m = res.data
//                UserInstance.userId = m?.id
                UserInstance.accessToken = m?.token
                self.sy_popVC()
            }
            
        } Error: {
            
        }
        
    }
    func forgetPassword(mobile: String,code: String,passwrod: String, newPassword: String){
        requestVM.baseRequest(disposeBag: dispose, type: .forgetPassword(mobile: mobile, code: code, passwd: passwrod, passwd2: newPassword, confirmPassword: ""), modelClass: SGBaseModel<SYLoginModel>.self) {[weak self] (res) in
            if let `self` = self {
                let m = res.data
//                UserInstance.userId = m?.id
                UserInstance.accessToken = m?.token
                self.sy_popVC()
            }
            
        } Error: {
            
        }
        
    }
  func  initView(){
        view.addSubview(usernameTF)
        view.addSubview(accountSepView)
        view.addSubview(usercodeTF)
        view.addSubview(codeButton)
        view.addSubview(accountSepView1)
        view.addSubview(passwordTF)
        view.addSubview(accountSepView2)
        view.addSubview(newpasswordTF)
        view.addSubview(accountSepView3)
        view.addSubview(loginButton)
        usernameTF.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(NAV_HEIGHT+49.autoWidth())
            make.left.equalToSuperview().offset(30.autoWidth())
            make.right.equalToSuperview().offset(-30.autoWidth())
        }
        accountSepView.snp.makeConstraints { make in
            make.top.equalTo(usernameTF.snp.bottom).offset(22.autoWidth())
            make.left.equalToSuperview().offset(30.autoWidth())
            make.right.equalToSuperview().offset(-30.autoWidth())
            make.height.equalTo(1)
        }
        
        usercodeTF.snp.makeConstraints { make in
            make.top.equalTo(accountSepView.snp.bottom).offset(22.autoWidth())
            make.left.equalToSuperview().offset(30.autoWidth())
            make.right.equalToSuperview().offset(-160.autoWidth())
            
        }
        codeButton.snp.makeConstraints { make in
            make.centerY.equalTo(usercodeTF.snp.centerY)
            make.right.equalToSuperview().offset(-32.autoWidth())
            make.width.equalTo(130.autoWidth())
        }
        accountSepView1.snp.makeConstraints { make in
            
            make.top.equalTo(usercodeTF.snp.bottom).offset(22.autoWidth())
            make.left.equalToSuperview().offset(30.autoWidth())
            make.right.equalToSuperview().offset(-30.autoWidth())
            make.height.equalTo(1)

        }
        
        passwordTF.snp.makeConstraints { make in
            make.top.equalTo(accountSepView1.snp.bottom).offset(22.autoWidth())
            make.left.equalToSuperview().offset(30.autoWidth())
            make.right.equalToSuperview().offset(-30.autoWidth())
            
        }
        
        accountSepView2.snp.makeConstraints { make in
            
            make.top.equalTo(passwordTF.snp.bottom).offset(22.autoWidth())
            make.left.equalToSuperview().offset(30.autoWidth())
            make.right.equalToSuperview().offset(-30.autoWidth())
            make.height.equalTo(1)

        }
        
        newpasswordTF.snp.makeConstraints { make in
            make.top.equalTo(accountSepView2.snp.bottom).offset(22.autoWidth())
            make.left.equalToSuperview().offset(30.autoWidth())
            make.right.equalToSuperview().offset(-30.autoWidth())
            
        }
        
        accountSepView3.snp.makeConstraints { make in
            
            make.top.equalTo(newpasswordTF.snp.bottom).offset(22.autoWidth())
            make.left.equalToSuperview().offset(30.autoWidth())
            make.right.equalToSuperview().offset(-30.autoWidth())
            make.height.equalTo(1)

        }
        
        loginButton.snp.makeConstraints { (make) in
            make.top.equalTo(accountSepView3.snp.bottom).offset(120.autoWidth())
            make.left.equalToSuperview().offset(33)
            make.right.equalToSuperview().offset(-33)
            make.height.equalTo(70)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
