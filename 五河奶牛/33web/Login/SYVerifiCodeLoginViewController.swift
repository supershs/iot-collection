//
//  SYVerifiCodeLoginViewController.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2020/8/20.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import SnapKit
class SYVerifiCodeLoginViewController: SYBaseViewController, SYLoginProtocol {

    

    var loginType: LoginEnum {
        return .verifiCode
    }
    var loginView: SYLoginView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        
        initViews()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }

    fileprivate func initViews() {
        loginView = SYLoginView(frame: CGRect.zero, loginProtocol: self, viewController: self)
        self.view.addSubview(loginView)
        loginView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    func getVerifiCode(complete:@escaping (Bool)->()) {
        
        let requestVM: RYNongchangListVM = RYNongchangListVM()
        requestVM.baseRequest(disposeBag: dispose, type: .verifiCode(mobile: self.loginView.usernameTF.text ?? ""), modelClass: SGBaseModel<SYLoginModel>.self) {[weak self] (res) in
            if let `self` = self {
                let m = res.data
                complete(true)
            }
            
        } Error: {
            complete(false)
        }
        
    }
    
 
    func login(loginName: String, password: String) {
        
        var  stringRegistr = ""
        if let key = UserDefaults.standard.object(forKey: "registrationID"){
            print(key)
            stringRegistr = UserDefaults.standard.object(forKey: "registrationID") as! String
        }
        
        let requestVM: RYNongchangListVM = RYNongchangListVM()
        requestVM.baseRequest(disposeBag: dispose, type: .codeLogin(mobile: loginName, code: password,registrationID: stringRegistr), modelClass: SGBaseModel<SYLoginModel>.self) {[weak self] (res) in
            if let `self` = self {
                var m = res.data
//                m?.mobile = loginName
                UserInstance.accessToken = m?.token
                UserInstance.userId = m?.userId
//                print(m?.toJSONString(),"ddddd")
                let urlStr = GOTOHOME + "?data=" + (m?.toJSONString() ?? "")
                self.sy_pushWebVC(urlStr.getEncodeString)
            }
            
        } Error: {
            
        }
    }
    
  
    
}
