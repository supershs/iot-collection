//
//  SYLoginModel.swift
//  SheYangBigData
//
//  Created by 宋海胜 on 2021/1/27.
//  Copyright © 2021 叁拾叁. All rights reserved.
//

import UIKit
import HandyJSON

struct SYLoginModel : HandyJSON {

        ///
        var headImg: String?
        /// false
        var firmSubcategory: Bool = false
        /// false
        var outsourceCode: Bool = false
        ///
        var sysDeptPostVO: NSSysDeptPostVOModel?
        ///
        var registrationId: String?
        /// 王成
        var nickName: String?
        /// wangcheng
        var loginName: String?
        /// 1
        var tenantId: Int = 0
        ///
        var token: String?
        /// USER
        var userType: String?
        ///
        var userId: String?
        /// <#泛型#>
        var compStringInfo: String?
        /// 王成
        var trueName: String?
        ///
        var sysTenantUserVO: NSSysTenantUserVOModel?

    }


    struct NSSysDeptPostVOModel : HandyJSON {

        ///
        var compStringPoint: String?
        ///
        var updateBy: String?
        ///
        var deptId: String?
        ///
        var updateDate: String?
        ///
        var userId: String?
        /// 681
        var dockDeptId: String?
        ///
        var postId: String?
        /// id
        var id: String?
        /// 2
        var deptLevel: Int = 0
        /// <#泛型#>
        var outsourceCode: String?
        /// <#泛型#>
        var sysUserDTO: String?
        /// 409
        var dockUserId: String?
        /// 101190705
        var weatherWarnCode: String?
        /// <#泛型#>
        var operationPrincipal: String?
        /// <#泛型#>
        var indexes: String?
        /// <#泛型#>
        var remarks: String?
        /// 1
        var status: Int = 0
        /// true
        var principal: Bool = false
        /// <#泛型#>
        var postName: String?
        /// 1
        var tenantId: String?
        ///
        var uuid: String?
        /// 320924
        var baiduAreaCode: String?
        ///
        var organiId: String?
        /// 1
        var sort: Int = 0
        /// 23
        var dockPostId: String?
        /// <#泛型#>
        var sysDeptVO: String?
        ///
        var createDate: String?
        /// 1
        var version: Int = 0
        ///
        var createBy: String?
        /// <#泛型#>
        var sysPostVO: String?
        /// <#泛型#>
        var deptName: String?

    
    }


    struct NSSysTenantUserVOModel : HandyJSON {

        ///
        var uuid: String?
        ///
        var updateBy: String?
        /// <#泛型#>
        var registrationId: String?
        ///
        var updateDate: String?
        /// <#泛型#>
        var userVO: String?
        /// <#泛型#>
        var tenantVO: String?
        /// 1
        var version: Int = 0
        ///
        var createBy: String?
        /// 165DAA
        var mainColor: String?
        /// 1
        var tenantId: String?
        /// USER
        var userType: String?
        /// 1
        var status: Int = 0
        /// <#泛型#>
        var loginType: String?
        /// id
        var id: String?
        /// <#泛型#>
        var remarks: String?
        /// <#泛型#>
        var loginTime: String?
        ///
        var userId: String?
        /// false
        var locked: Bool = false
        /// 1
        var sort: Int = 0
        ///
        var createDate: String?
        /// <#泛型#>
        var mobile: String?

      
    }
