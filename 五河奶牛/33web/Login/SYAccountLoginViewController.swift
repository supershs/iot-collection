//
//  SYAccountLoginViewController.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2020/8/20.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import SnapKit
import Then

class SYAccountLoginViewController: SYBaseViewController, SYLoginProtocol {
    let requestVM: RYNongchangListVM = RYNongchangListVM()

    var loginType: LoginEnum {
        return .account
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        let loginView = SYLoginView(frame: CGRect.zero, loginProtocol: self, viewController: self)
        self.view.addSubview(loginView)
        loginView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        let token: String? = UserInstance.accessToken
        
        if !(token ?? "").isEmpty {
            let userId: String = UserInstance.userId ?? ""
            let token: String = UserInstance.accessToken ?? ""
            var  registrationId = ""
            if let key = UserDefaults.standard.object(forKey: "registrationID"){
                print(key)
                registrationId = UserDefaults.standard.object(forKey: "registrationID") as! String
            }
            let path: String = "?userId=" + userId  + "&token=" + token + "&registrationId=" + registrationId
            print(path)
            self.sy_pushWebVC(GOTOHOME + path.getEncodeString)
        }else{
            
//            self.resetDeviceRegistrationId()
        }
    }
    
    /// SYLoginProtocol
    func thirdLogin() {
        
    }
    

    

    
    func login(loginName: String, password: String) {
        
        var  stringRegistr = ""
        if let key = UserDefaults.standard.object(forKey: "registrationID"){
            print(key)
            stringRegistr = UserDefaults.standard.object(forKey: "registrationID") as! String
        }
        requestVM.baseRequest(disposeBag: dispose, type: .login(loginName: loginName, password: password, registrationId: stringRegistr), modelClass: SGBaseModel<SYLoginModel>.self) {[weak self] (res) in
            if let `self` = self {
                let m = res.data
                UserInstance.loginName = loginName
                UserInstance.password = password
                UserInstance.accessToken = m?.token
                UserInstance.userId = m?.userId
                let urlStr = GOTOHOME + "?data=" + (m?.toJSONString() ?? "") + "&registrationId=" + stringRegistr
                print(m?.toJSONString() ?? "")
                self.sy_pushWebVC(urlStr.getEncodeString)
            }
            
        } Error: {
            
        }
    }
}
