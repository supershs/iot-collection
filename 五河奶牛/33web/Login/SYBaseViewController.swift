//
//  SYBaseViewController.swift
//  SheYangBigData
//
//  Created by 叁拾叁 on 2020/8/21.
//  Copyright © 2020 叁拾叁. All rights reserved.
//

import UIKit
import RxSwift
//import MJRefresh
//import SwiftDate

class SYBaseViewController: UIViewController {

    public var backClosure: ((Int) -> Void)?
    public var passParamsClosure: ((Any) -> Void)?
    let dispose = DisposeBag()
    let backItem: UIBarButtonItem = UIBarButtonItem()
    
    var hideNav: Bool = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = hideNav
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.navigationController?.navigationBar.isHidden = false
        self.tabBarController?.tabBar.isHidden = true
        let image: UIImage = UIImage(named: "back_nav")!
//        image.renderingMode = .alwaysOriginal
        self.navigationController?.navigationBar.backIndicatorImage = image
//        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = image
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "back_nav"), style: .done, target: self, action: #selector(goBack))

    }
	
	@objc func goBack() {
		self.navigationController?.popViewController(animated: true)
	}
    func toShouye(_ animated: Bool = true) {
        self.navigationController?.tabBarController?.hidesBottomBarWhenPushed = false
        self.navigationController?.tabBarController?.selectedIndex = 0 // (第一个控制器)
        self.navigationController?.popToRootViewController(animated: animated)
    }
    
    func popVC(_ animated: Bool = true) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
        self.backClosure?(0)
    }
    
    func pushVC(_ vc: UIViewController) {
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func sy_toast(_ message: String, completion: ((_ didTap: Bool) -> Void)? = nil) {
        
        self.view.makeToast(message, duration: 1.5, position: .center, completion: completion)
    }
    func removeVC(_ vcClass: AnyClass) {
        
        self.navigationController?.children.forEach({ vc in
            if vc.isKind(of: vcClass) {
                self.navigationController?.viewControllers.removeValue(vc)
            }
        })
    }
    

    func sy_pushWebVC(_ url: String) {
        let path = String(format: "%@%@", WEBIP, url)
        log.info("\n************************************************** separate *********************************************\n\n webpath: \(path)\n")
        let webvc = HSWebViewController(path: path)
        self.pushVC(webvc)
    }
    
    func backSnp(_ back: UIView) {
        back.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.top.equalToSuperview().offset(STATUSBAR_HEIGHT)
            make.height.equalTo(48.autoWidth())
            make.width.equalTo(30.autoWidth())
        }
    }
    
    func isTodayOrTomorrow(_ date:Date) -> String {

        if date.isToday {
            return "今天"
        } else if date.isTomorrow {
            return "明天"
        } else {
            return ""
        }
    }
    
    func toLoginVC() {
        
        let loginVC = SYAccountLoginViewController()
        self.vg_present(loginVC, animated: true)
    }
    
    func toWebLoginVC(){
        let loginVC = HSWebViewController(path: WEBIP)
        self.vg_present(loginVC, animated: true)
    }
    func addToolBar() -> UIToolbar {
        let toolBar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 35))
        toolBar.backgroundColor = UIColor.gray
        let spaceBtn = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let barBtn = UIBarButtonItem(title: "完成", style: .plain, target: self, action: #selector(doneNum))
        toolBar.items = [spaceBtn, barBtn]
        toolBar.sizeToFit()
        return toolBar
    }

    @objc func doneNum() {
        self.view.endEditing(false)
    }
    
    public func topMangain(_ tableView: UITableView) {
        // 解决Webview未覆盖顶部状态栏
        if #available(iOS 11.0, *) {
            tableView.contentInsetAdjustmentBehavior = UIScrollView.ContentInsetAdjustmentBehavior.never
        } else {
            self.edgesForExtendedLayout = UIRectEdge.bottom
        }
    }
}

class SYBaseTableViewController: SYBaseViewController {
    
    var page = 1
    var size = 10
    var noDataView = SYNoDataView()
    
    var tableView:UITableView!
    
    var isStatusBarHidden = false {
        didSet {
            self.setNeedsStatusBarAppearanceUpdate()
        }
    }
    override var prefersStatusBarHidden: Bool {
        get {
            return isStatusBarHidden
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
}
extension UIViewController {
    
    func vg_present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        viewControllerToPresent.modalPresentationStyle = UIModalPresentationStyle.fullScreen
        self.present(viewControllerToPresent, animated: flag, completion: completion)
        
    }
    
    /// 设置状态栏颜色
    /// - Parameter style: 传入.default是黑色 lightContent是白色
    func setStatubarStyle(style: UIStatusBarStyle) {
        if style == .default{
            if #available(iOS 13.0, *) {
                UIApplication.shared.setStatusBarStyle(.darkContent, animated: true)
            } else {
                UIApplication.shared.setStatusBarStyle(.default, animated: true)
            }
        }else{
            UIApplication.shared.setStatusBarStyle(style, animated: true)
        }
    }
    
    func dismissViewController() {
        if let _ = navigationController {
            navigationController?.popViewController(animated: true)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
    
    func sy_push(_ vc: UIViewController, _ animated: Bool = true) {
        self.navigationController?.pushViewController(vc, animated: animated)
    }
    
    func sy_popVC() {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
}

extension UINavigationController {
    // 用于获取vc数组中需要的vc
    func fetchVCByOffsetCurrent(offset: Int, classType: AnyClass) -> Any? {
        let vcs = self.viewControllers
        let target = vcs[vcs.count - offset - 1]
        if target.isMember(of: classType) {
            return target
        } else {
            return nil
        }
    }
}
